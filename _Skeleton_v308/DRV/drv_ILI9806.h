// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_ILI9806.h
// 	   Version: 3.0
//		  Desc: User Driver for ILI9806 - header file
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
// 

#ifndef __DRV_ILI9806_H_
#define __DRV_ILI9806_H_

#include "Skeleton.h"

// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************

#ifndef CONF_DEBUG_DRV_ILI9806
#define CONF_DEBUG_DRV_ILI9806			0											// default je vypnuty
#endif	



// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************
COMP_PACKED_BEGIN
typedef struct drv_BQ2589x_Ext_API
{
//	uint32_t	(*Read_Batt_Voltage)	( _drv_If_t *pDrvIf);						// Get voltage
//	bool 		(*Preset) 				(_drv_If_t *pDrvIf);						// Fill with presets values
	void 		(*Reload_MirrorRegs)	(_drv_If_t *pDrvIf);						// reload mirror all charger registers
//	void		(*FastMonitor)			(_drv_If_t *pDrvIf);						// reload only AD registers
} drv_ILI9806_Ext_API_t;
COMP_PACKED_END

// ku generic strukture treba este pridat:
COMP_PACKED_BEGIN
typedef struct drv_ILI9806_If_Spec
{
//	const 	CHAL_Signal_t				*pMISO;										// fyzicky Port MISO
			bool 						(*pCallBack_WriteData)(_drv_If_t *pDrvIf, uint8_t DestReg, sys_Buffer_t *pBuff, uint8_t RegsCount);	// handler for data exchange. Must be set from root module!
			uint32_t					(*pCallBack_ReadData) (_drv_If_t *pDrvIf, uint8_t SrcReg, sys_Buffer_t *pBuff, uint8_t RegsCount);	// handler for data exchange. Must be set from root module!
			uint8_t						*pMirrorRegs;
			_drv_If_t					*pCommInterface;							// communication interface
//			SPI_Device_t				SPI_Device;									// device structure
} drv_ILI9806_If_Spec_t;
COMP_PACKED_END
// ************************************************************************************************
// PRIVATE
// ************************************************************************************************
#define	EC_SUCCESS						0
#define EC_ERROR_UNIMPLEMENTED			2
#define EC_ERROR_BUSY					6

#define ILI9806_REG_COUNT      			0x15
/* Registers */
#define ILI9806_REG_INPUT_CURR      	0x00


// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
extern const 							_drv_Api_t drv_ILI9806_API_STD;
//extern const struct 					charger_info *charger_get_info(void);

#endif		// __DRV_ILI9806_H_

