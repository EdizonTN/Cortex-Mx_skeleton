// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_UART.h
// 	   Version: 3.06
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for UART - header file
// ******************************************************************************
// Info: UART driver. 
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
//				2024.09.03	- v3.06 - add WaitForStat function as public
//									- add two new states: DRV_UART_STAT_TXOK, DRV_UART_STAT_RXOK
//									- remove CONF_DRV_UART_TIMEOUT
//									- add CONF_DRV_UART_WRDAT_WAIT_TIMEOUT_CLKS constant
//				2024.02.01	- v3.05 - TxTimeoutCnt fixed
//				2021.06.10	- Conf structure renamed to controller
// 				2020.07.15	- CHAL_UART_Xfer_t renamed to drv_UART_Xfer_t, CHAL_UART_Xfr_Status_t to drv_UART_Xfer_Status_t and their enum flags from CHAL_UART_STAT_xxxx to drv_UART_STAT_xxxx

#if !defined(__DRV_UART_H_) 														// prevent to cyclyc loads
 #if defined(USE_DRV_UART) && (USE_DRV_UART == 1)									// Need to load?
  #if (!defined (_CHIP_UART_COUNT) || _CHIP_UART_COUNT == 0)						// Neeed it, but MCU haven't this type of periphery!
	#error This driver is need to be used, but MCU doesn't have it!
  #else
	// now -> load it.
#define __DRV_UART_H_	
	
	
#include "Skeleton.h"

// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************
#define UART_ACCEPTED_BAUDRATE_ERROR	1											// maximalna chyba pri nastavovani baudrate v percentach

//#ifndef drv_UART_UPCLK	
//#ifdef 	BSP_UART_UPCLK
//	#define	drv_UART_UPCLK 				BSP_UART_UPCLK
//#else
//	#error " Undefined BSP_UART_UPCLK! "											// peripherial clock na UART
//#endif
//// ex.: #define drv_UART_UPCLK			Core.MainClock
//#endif

#ifndef CONF_DEBUG_DRV_UART
#define CONF_DEBUG_DRV_UART				0											// default je vypnuty
#endif	

//#if !defined(CONF_USE_LIB_RINGBUFFER) || (CONF_USE_LIB_RINGBUFFER == 0)
#ifndef CONF_DRV_UART_BUFFERSIZE
 #define CONF_DRV_UART_BUFFERSIZE		100											// velkost buffera prijatych znakov
#endif	

#ifndef CONF_DRV_UART_BUFFERELEMENTSIZE
 #define CONF_DRV_UART_BUFFERELEMENTSIZE 1											// velkost znaku v bufferi
#endif	

//#endif


#ifndef CONF_DRV_UART_WRDAT_WAIT_TIMEOUT_CLKS
#define CONF_DRV_UART_WRDAT_WAIT_TIMEOUT_CLKS	10									// In Write address command - how many UART CLK impulses will wait for timeout
#endif


#ifndef CONF_DRV_UART_EMUL_9THBIT
 #define CONF_DRV_UART_EMUL_9THBIT		0											// emulation of 9-th bit
#endif	


#ifndef UART_USE_IRQ_MANAGER 
#define UART_USE_IRQ_MANAGER 			0											// If IRQ Manager is used, UART can be leaved from IRQ Managaer setting to "0"
#endif	

// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************
typedef enum
{
	DRV_UART_STAT_IDLE					= 0x0000,									// must be zero!
	DRV_UART_STAT_TXBUSY				= 0x0001,									// Tx in progress. wait for ~TXBUSY
	DRV_UART_STAT_RXBUSY				= 0x0002,									// Rx In Progress
	DRV_UART_STAT_TXERR					= 0x0004,									// Tx in progress. wait for ~TXBUSY
	DRV_UART_STAT_RXERR					= 0x0008,									// Rx In Progress

//	DRV_UART_STAT_TXOK					= 0x0010,									// Character was send
//	DRV_UART_STAT_RXOK					= 0x0020,									// Received character

	
	DRV_UART_STAT_TXUNDERRUN			= 0x1000,									// Tx buffer has not enought data!
	DRV_UART_STAT_RXOVERBUFF			= 0x2000,									// Rx buffer is full and new data will losed
	
	DRV_UART_STAT_TIMEOUT				= 0x0040,
} drv_UART_Xfer_Status_t;

typedef struct
{
	sys_Buffer_t						*pTxBuff;									// Pointer to Tx Buffer
	sys_Buffer_t						*pRxBuff;									// Pointer to Rx Buffer
	volatile uint32_t					RxCount;									// total received elements in receive buffer
	uint32_t							TxLenght;									// total elements for send for current transfer buffer
	volatile uint32_t					TxEstimated;								// estimated transfer elements from current TxBuffer
	volatile drv_UART_Xfer_Status_t		Status;										// Status of the current UART transfer
	volatile uint32_t					TxTimeoutCnt;								// counting up to preset value.
} drv_UART_Xfer_t;


typedef enum drv_UART_DumplexMode
{
	HalfDuplex,
	FullDuplex
} drv_UART_DumplexMode_t;

typedef enum drv_UART_FlowControl													// UART definitions - Flow Controls
{
	None,
	CTSRTS
} drv_UART_FlowControl_t;


COMP_PACKED_BEGIN
typedef struct drv_UART_Ext_API
{
	bool   	(*Configure)		( struct _drv_If *pDrvIf);
	bool   	(*ChangeAddress)	( struct _drv_If *pDrvIf, uint8_t NewAddress);
	bool	(*FlowControl) 		( struct _drv_If *pDrvIf, drv_UART_FlowControl_t FlowControl);
	void	(*RxFlush) 			( struct _drv_If *pDrvIf);									// clear and prepare Rx Buffers
	bool	(*WaitForNStat)		( struct _drv_If *pDrvIf, uint32_t WaitFor_nStateMask, uint32_t NumOfTimeOutCLKPulses);
	bool	(*WaitForRxChars)	( struct _drv_If *pDrvIf, uint32_t WaitForChar_Count, uint32_t NumOfTimeOutCLKPulses);
	// dopln set 485 Mode funkciu .....
} drv_UART_Ext_API_t;
COMP_PACKED_END

const typedef struct drv_UART_Controller
{
	drv_UART_DumplexMode_t				DuplexMode;									// Duplex
	uint32_t							BaudRate; 									// Baud Rate value
	uint8_t								DataLen; 									// DataLen
	uint8_t								Parity; 									// Parity
	uint8_t								StopBits; 									// StopBits
	uint8_t								Address;									// Address (9-bit mode) - ak je zadana, konfiguruje sa RS485 !!! Ak je 0xFF, sme ako MASTER!
	drv_UART_FlowControl_t				FlowControl;								// riadenie toku dat
	uint16_t							CLKperBit;									// Num of Clock pulses for one Bit - UPDATED automatically after first use
#if defined(_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
	uint8_t								RxFIFO_Max;									// maximal size for Rx FIFO. Is updated during Init
	uint8_t								TxFIFO_Max;									// maximal size for Tx FIFO. Is updated during Init
#endif	
} drv_UART_Controller_t;

// ku generic strukture treba este pridat:
COMP_PACKED_BEGIN
typedef struct drv_UART_If_Spec
{
	const 	uint8_t						PeriIndex;									// index pouzitej periferie
			_CHIP_UART_T				*pPeri;										// pointer na periferiu	- nacitava sa podla index pri inite

	const 	CHAL_Signal_t				*pRX;										// fyzicky Port RX
	const 	CHAL_Signal_t				*pTX;										// fyzicky Port TX
	const 	CHAL_Signal_t				*pRTS;										// fyzicky Port RTS - pre 4-wire UART mode
	const 	CHAL_Signal_t				*pCTS;										// fyzicky Port CTS - pre 4-wire UART mode
	const 	CHAL_Signal_t				*pDIRDE;									// fyzicky Port DIRection/DataEnable - pre DIR/DE RS485
	const 	CHAL_Signal_t				*pRE;										// fyzicky Port ReceiveEnable - pre RE RS485
	const 	CHAL_Signal_t				*pTERM;										// fyzicky Port Terminator - pre RE RS485
	struct  drv_UART_Controller			Controller;									// pointer na konfiguracnu strukturu
			drv_UART_Xfer_t				XFer_Rec;									// Xfer packet record MCU depends	
} drv_UART_If_Spec_t;
COMP_PACKED_END

// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
extern const _drv_Api_t drv_UART_API_STD;

// ************************************************************************************************
// BIT Definition - INTENCLR register
// ************************************************************************************************
//#define TXRDYCLR						(0x01 << 2)									// Writing 1 clears the corresponding bit in the INTENSET register.


#endif		// defined (_CHIP_UART_COUNT)
#endif		// #if defined(USE_DRV_UART)
#endif		// __DRV_UART_H_
