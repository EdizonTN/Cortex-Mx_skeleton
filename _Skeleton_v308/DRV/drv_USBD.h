// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_USB.h
// 	   Version: 3.2
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Basic Driver for USB - header file
// ******************************************************************************
// Usage:
//			
// ToDo:
//		- USB_HOST + USB_OTG dorobit....
//		- Interny Stack LPC15xx - optimalizuj. Pri jeho inicializacii sa nastuju callbacky a eventy (cdc_vcom.c)- toto sprav uz v USBD_Init.
//
// Changelog:
//				2024.09.24	- v3.2 - Rewrited whole usbd driver 
// 

#if !defined(__DRV_USBD_H_) 															// prevent to cyclyc loads
 #if defined(USE_DRV_USBD) && (USE_DRV_USBD == 1)										// Need to load?
  #if (!defined (_CHIP_USBD_COUNT) || _CHIP_USBD_COUNT == 0)							// Neeed it, but MCU haven't this type of periphery!
	#error This driver is need to be used, but MCU doesn't have it!
  #else
	// now -> load it.
#define __DRV_USBD_H_

#include "Skeleton.h"


// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************
#ifndef CONF_DEBUG_DRV_USBD
#define CONF_DEBUG_DRV_USBD				0											// default je vypnuty
#endif	

//#if !defined(CONF_USE_LIB_RINGBUFFER) || (CONF_USE_LIB_RINGBUFFER == 0)
#ifndef CONF_DRV_USBD_BUFFERSIZE
 #define CONF_DRV_USBD_BUFFERSIZE		100											// velkost buffera prijatych znakov
#endif	

//#ifndef CONF_USE_LIB_LPCUSB 
//#define CONF_USE_LIB_LPCUSB 			0											// pouzivame library LPCUSB
//#endif

//#ifndef CONF_USE_ROMAPI_LPCUSB 
//#define CONF_USE_ROMAPI_LPCUSB 			0											// USB use internal MCU's API
//#endif



/** Available configuration number in a device */
//#define FIXED_NUM_CONFIGURATIONS		1



// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************
typedef enum
{
	CHAL_USB_STAT_OK					= 0x0000,
	CHAL_USB_STAT_ERROR					= 0x8000,
	CHAL_USB_STAT_BUSY					= 0x0001,									// in progress. wait for ~BUSY
	CHAL_USB_STAT_BUFFFULL				= 0x0002,
	CHAL_USB_STAT_BUFFNONE				= 0x0004,	
	CHAL_USB_STAT_TIMEOUT				= 0x0040,
} CHAL_USB_Xfr_Status_t;




COMP_PACKED_BEGIN
typedef struct drv_USBD_Ext_API
{
	bool   	(*Configure)		( struct _drv_If *pDrvIf);
} drv_USBD_Ext_API_t;
COMP_PACKED_END

typedef struct
{
	const uint8_t 						*pTxBuff;									// Pointer to array of bytes to be transmitted
	uint8_t 							*pRxBuff;									// Pointer memory where bytes received from I2C be stored
	uint16_t 							TxSize;										// Number of bytes in transmit array, if 0 only receive transfer will be carried on
	uint16_t 							RxBuffSize;									// Number of bytes for received bytes, if 0 only transmission we be carried on. After received, value is decrement
	volatile CHAL_USB_Xfr_Status_t		Status;										// Status of the current I2C transfer
} CHAL_USB_Xfer_t;



// ku generic strukture treba este pridat:
COMP_PACKED_BEGIN
typedef struct drv_USBD_If_Spec
{
	const 	uint8_t						PeriIndex;									// index pouzitej periferie	
			_CHIP_USB_T					*pPeri;										// pointer na periferiu	- nacitava sa podla index pri inite
	const CHAL_USB_Mode_t				USB_Mode;									// device, host, otg
			bool						Enumerated;									// priznak pripravenosti USB pre App.
			
//	volatile uint8_t					TxCount;									// queue size
//#if defined(CONF_USE_LIB_RINGBUFFER) && (CONF_USE_LIB_RINGBUFFER == 1)	
//			lib_RB_If_t					*pRx_Buff;									// RX Ring Buffer for this USB  - must be pointer !!!!!!!!!
//			lib_RB_If_t					*pTx_Buff;									// TX Ring Buffer
//#else
////    volatile uint8_t					RxBuffer_Wr;								// pointer na posledne zapisane data	
//	volatile uint8_t					*pRx_Buff;									// RX Linear Buffer for this USB  - must be pointer 
//	volatile uint8_t					*pTx_Buff;									// TX Linear Buffer
//	volatile uint32_t					Rx_Count;									// Received char count
//			bool						Tx_Active;									// Priznak ze vysielame	
//			bool 						Rx_Flag;									// Priznak prijatych dat
//#endif	

#if defined(CONF_USE_LIB_LPCUSB) && (CONF_USE_LIB_LPCUSB == 1)
	lib_LPCUSB_If_t						*pXFer_Lib;									// if use lpcUSB_lib Stack
#endif
			CHAL_USB_Xfer_t				XFer_Rec;									// Xfer packet record
			
	const 	CHAL_Signal_t				*pUSB_DP;									// fyzicky Port DP
	const 	CHAL_Signal_t				*pUSB_DN;									// fyzicky Port DN
	const 	CHAL_Signal_t				*pUSB_Connect;								// fyzicky Port SoftConnect - Pull up 1k5 resistor on DP
} drv_USBD_If_Spec_t;
COMP_PACKED_END
























/*
// ************************************************************************************************
// Dependencies
// ************************************************************************************************

// USBD will use lpcUSBlib Stack:
#if defined( CONF_USE_LIB_LPCUSB ) && ( CONF_USE_LIB_LPCUSB == 1)
	#include "LIB\lib_LPCUSB.h"
	#define USBRAM_BUFFER_SIZE				CONF_USBSTACK_SIZE /2					// (4*1024)
#endif

// USBD will use ROM API of Stack and ext. .a library:
#if defined( CONF_USE_ROMAPI_LPCUSB ) &&( CONF_USE_ROMAPI_LPCUSB == 1)
	#include "3rd_STACK\USBD\usbd.h"
	#define USBRAM_BUFFER_SIZE				CONF_USBSTACK_SIZE						// (4*1024)
#endif

*/

// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
extern const _drv_Api_t drv_USBD_API_STD;
//extern void drv_USBD_USB0_Handler_Top (_drv_If_t *pDrvIf);							// read char to buffer
//extern void drv_USBD_USB0_Handler_Bottom (_drv_If_t *pDrvIf);





#endif		// defined (_CHIP_zzz_COUNT)
#endif		// #if defined(USE_DRV_zzz)
#endif		// __DRV_ZZZ_H_
