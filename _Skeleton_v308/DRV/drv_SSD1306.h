// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_SSD1306.h
// 	   Version: 1.01
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: User Driver for SSD1306 - header file
// ******************************************************************************
// Info: 
//
// Notice:
//
// Usage:
//			
// ToDo:	
//
// Changelog:
//				2024.08.28	- v1.01 - change Tx size parameter to uint 32 
//				2024.06.07	- first release

#ifndef __DRV_SSD1306_H_
#define __DRV_SSD1306_H_

#include "Skeleton.h"

// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************

#ifndef CONF_DEBUG_DRV_SSD1306
#define CONF_DEBUG_DRV_SSD1306				0										// default je vypnuty
#endif	

#ifndef CONF_DRV_SSD1306_RETRY_COUNT
#define CONF_DRV_SSD1306_RETRY_COUNT		3										// read/write retry counter if not ack or other error occured3
#endif				


// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************

// ku generic strukture treba este pridat:
COMP_PACKED_BEGIN
typedef struct drv_SSD1306_If_Spec
{
	uint8_t									*pMirrorRegs;
	_drv_If_t 								*pRootDrvIf;							// ptr to Owners driver struct
	const 	CHAL_Signal_t					*pSig_RST;								// hardware reset
	I2C_Device_t							*pI2C_SSD1306_Device;					// connected devices
} drv_SSD1306_If_Spec_t;
COMP_PACKED_END


COMP_PACKED_BEGIN
typedef struct drv_SSD1306_Ext_API
{
	void		(*Reset)					( drv_SSD1306_If_Spec_t *pDrvIf_Spec);	// Reset chip
	bool		(*Init)						(_drv_If_t *pDrvIf, I2C_Device_t *pDevice);	
//	uint32_t	(*Read)						( I2C_Device_t *pDevice, uint8_t SrcReg, sys_Buffer_t *Dst, uint8_t DstInc, uint8_t DataCount);	// read data
	uint32_t	(*Write)					( I2C_Device_t *pDevice, void *Src, uint8_t SrcInc, uint32_t DataCount);	// write data
//	void 		(*Reload_MirrorRegs)		(_drv_If_t *pDrvIf);					// reload mirror all charger registers
} drv_SSD1306_Ext_API_t;
COMP_PACKED_END

// ************************************************************************************************
// PRIVATE
// ************************************************************************************************
#define	EC_SUCCESS						0
#define EC_ERROR_UNIMPLEMENTED			2
#define EC_ERROR_BUSY					6

#define SSD1306_REG_COUNT      			0x15
/* Registers */
#define SSD1306_REG_INPUT_CURR      	0x00

#define	SSD1306_REG_COMMAND				0x00
#define	SSD1306_REG_DATA				0x40


// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
extern const 							_drv_Api_t drv_SSD1306_API_STD;
//extern const struct 					charger_info *charger_get_info(void);

#endif		// __DRV_SSD1306_H_

