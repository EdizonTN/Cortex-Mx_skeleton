// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_ADC.h
// 	   Version: 3.11
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for ADC - header file
// ******************************************************************************
// Info: ADC driver. 
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
//				2023.09.24	-	v3.11	- small upgrades (typo)
// 

#if !defined(__DRV_ADC_H_) 														// prevent to cyclyc loads
 #if defined(USE_DRV_ADC) && (USE_DRV_ADC == 1)									// Need to load?
  #if (!defined (_CHIP_ADC_COUNT) || _CHIP_ADC_COUNT == 0)						// Neeed it, but MCU haven't this type of periphery!
	#error This driver is need to be used, but MCU doesn't have it!
  #else
	// now -> load it.
#define __DRV_ADC_H_

#include "Skeleton.h"

// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************

#ifndef CONF_DEBUG_DRV_ADC
#define CONF_DEBUG_DRV_ADC				0											// default je vypnuty
#endif	

// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************

// Extended API functions for this driver:
//COMP_PACKED_BEGIN
//typedef struct drv_LCD_Ext_API
//{
//	bool	(*SomeFunction)	( _drv_If_t *pDrvIf);									// Some function
//} drv_LCD_Ext_API_t;
//COMP_PACKED_END

COMP_PACKED_BEGIN
typedef struct drv_ADC_If_Channel
{
			uint8_t						ADC_ChannelNum;									// number of selected channel
	volatile uint32_t					ADC_Result;										// AD result (format is specific for desired MCU!)
	
} drv_ADC_If_Channel_t;
COMP_PACKED_END

// Additions for Generic driver structure here:
COMP_PACKED_BEGIN
typedef struct drv_ADC_If_Spec
{
	void								*pPeri;										// pointer for used periphery
	const 	uint8_t						PeriIndex;									// index of used periphery
	const	uint32_t					Ch_Active_Mask[2];							// active channels mask for each sequence
			uint8_t						Ch_Active;									// active channel count
	const	uint8_t						DMA_XFER_CH;								// DMA xfer channel num
//			drv_ADC_If_Channel_t		**Result_CH;								// array of the results of ad conversion for each ACTIVE channel. Finished by NULL. Initialized in the Init
			drv_ADC_If_Channel_t		Result_CH[_CHIP_ADC_CHANNELS_COUNT];
} drv_ADC_If_Spec_t;
COMP_PACKED_END


// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
extern const _drv_Api_t drv_ADC_API_STD;
extern void drv_ADC_Handler_Top(_drv_If_t *pDrvIf);
extern void drv_ADC_Handler_Bottom(_drv_If_t *pDrvIf);

#endif		// defined (_CHIP_ADC_COUNT)
#endif		// #if defined(USE_DRV_ADC)
#endif		// __DRV_ADC_H_
