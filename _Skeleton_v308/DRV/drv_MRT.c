// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_MRT.c
// 	   Version: 3.42
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for Multi Rate MRT
// ******************************************************************************
// Info: MRT driver
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
//				2024.09.30	- v3.42 - add sys_NULL_Function instead a NULL pointer to a function in API structure
//				2023.12.10	- v3.41 - rename _CHIP_MRTx_IRQ_Handler to _Chip_MRTx_IRQ_Handler
//				2022.11.30	- v3.4 - change system of load
//				2022.11.28	- changed interrupt handler bottom
//				2020.06.24	- pAPI_USR deleted!
//				2020.6.16	- while(tmpListRecord) changed
// 

#include "Skeleton.h"

#ifdef __DRV_MRT_H_																	// Header was loaded?

// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool MRT_Init(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool MRT_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool MRT_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
void MRT_Events( struct _drv_If *pDrvIf, _EventType_t EventType);
void MRT_Handler_Top(_drv_If_t *pDrvIf);											// quick handler
//void MRT_Handler_Bottom(_drv_If_t *pDrvIf);										// process handler


// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************

// structure and assign to standard API functions:

// toto su STD volania pre tento driver. Mali by byt ako const. Nesmu sa menit, pretoze na ne moze ukazovat viacero subdriverov.
// ak chces USR volanie, vytvor novu strukturu, skopiruj do nej tuto STD a zmen konkretnu funkciu!
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_MRT_API_STD = 
{
	.Init			=	MRT_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	MRT_Enable,
	.Disable		=	MRT_Disable,
	.Write_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Read_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Events			=	MRT_Events,
	.Ext			= 	NULL
};
#pragma pop
									
const _drv_If_t *MRT_DriverIRQ[_CHIP_MRT_COUNT];
// ------------------------------------------------------------------------------------------------------
// MRT INTERRUPT HANDLER
// ------------------------------------------------------------------------------------------------------
#if defined(_CHIP_MRT_COUNT) && (_CHIP_MRT_COUNT > 0)
void _Chip_MRT0_IRQ_Handler(void)	
{
	MRT_Handler_Top((void *) MRT_DriverIRQ[0]);
	//MRT_Handler_Bottom((void *) MRT_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) MRT_DriverIRQ[0]);
}
#endif

#if defined(_CHIP_MRT_COUNT) && (_CHIP_MRT_COUNT > 1)
void _Chip_MRT1_IRQ_Handler(void)	
{
	MRT_Handler_Top((void *) MRT_DriverIRQ[1]);
	//MRT_Handler_Bottom((void *) MRT_DriverIRQ[1]);
	IRQ_Handler_Bottom((void *) MRT_DriverIRQ[1]);
}
#endif

#if defined(_CHIP_MRT_COUNT) && (_CHIP_MRT_COUNT > 2)
void _Chip_MRT2_IRQ_Handler(void)	
{
	MRT_Handler_Top((void *) MRT_DriverIRQ[2]);
	//MRT_Handler_Bottom((void *) MRT_DriverIRQ[2]);
	IRQ_Handler_Bottom((void *) MRT_DriverIRQ[2]);
}
#endif

// ------------------------------------------------------------------------------------------------------
// Inicializacia interfejsu
// Force - inicializuj aj ked je nastaveny priznak ze uz je init hotovy
bool MRT_Init(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;
	drv_MRT_If_Spec_t* pDrvIf_Spec = (drv_MRT_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_MRT) && (CONF_DEBUG_DRV_MRT == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> ParentMod: %s", pRequestorModIf->Name);
#endif		

	// initialization code here:
	pDrvIf_Spec->pPeri = CHAL_MRT_Init(pDrvIf_Spec->PeriIndex);	
	if(pDrvIf_Spec->pPeri) res = true;

	pDrvIf->Sys.Stat.Initialized  = res;											// Done !
	MRT_DriverIRQ[pDrvIf_Spec->PeriIndex] = pDrvIf;									// set parameter for interrupt routine
	
//gpioinitexit:	
#if defined(CONF_DEBUG_DRV_MRT) && (CONF_DEBUG_DRV_MRT == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif			
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// Generic
// ENABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
bool MRT_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	drv_MRT_If_Spec_t* pDrvIf_Spec = (drv_MRT_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_MRT) && (CONF_DEBUG_DRV_MRT == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> chMask: 0x%.8x", ChannelBitMask);							// print arguments
#endif

	// main code here:
	pDrvIf->Sys.Stat.ChannelStatus &= ~ChannelBitMask;
	pDrvIf->Sys.Stat.LastEvent = EV_NoChange;
	
	res = CHAL_MRT_Enable_CH(pDrvIf_Spec->pPeri, ChannelBitMask, true); 				// enable MRT n
	
	if(res == true) pDrvIf->Sys.Stat.Enabled = true;								// enable was succesfull
	NVIC_EnableIRQ((IRQn_Type) (pDrvIf->IRQ_VecNum));								// Enable MRT n interrupt
		
#if defined(CONF_DEBUG_DRV_MRT) && (CONF_DEBUG_DRV_MRT == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// Generic
// DISABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
bool MRT_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	drv_MRT_If_Spec_t* pDrvIf_Spec = (drv_MRT_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_MRT) && (CONF_DEBUG_DRV_MRT == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> chMask: 0x%.8x", ChannelBitMask);							// print arguments
#endif

	// main code here:
	res = CHAL_MRT_Enable_CH(pDrvIf_Spec->pPeri, ChannelBitMask, false); 				// enable MRT n
	
	if(res == true) pDrvIf->Sys.Stat.Enabled = false;								// enable was succesfull
	NVIC_EnableIRQ((IRQn_Type) (pDrvIf->IRQ_VecNum));								// Enable MRTn interrupt
		
#if defined(CONF_DEBUG_DRV_MRT) && (CONF_DEBUG_DRV_MRT == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}



// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************


// ------------------------------------------------------------------------------------------------------
// Quick handler
void MRT_Handler_Top(_drv_If_t *pDrvIf)												// Quick Handler  - Overrun irq
{
	if(pDrvIf == NULL) return;

	if(pDrvIf->pIRQ_USR_HandlerTop) pDrvIf->pIRQ_USR_HandlerTop(pDrvIf);			// override with USR handler
	else
	{
		if(pDrvIf->Sys.Stat.Initialized == false) return;
		if(pDrvIf->Sys.Stat.Loaded == false) return;

		// Generic interrupt routine:
		NVIC_ClearPendingIRQ( (IRQn_Type) (pDrvIf->IRQ_VecNum));					// Clear pending interrupt
	}
	pDrvIf->Sys.Stat.ChannelStatus = CHAL_MRT_Get_IRQ_Status_CH(((drv_MRT_If_Spec_t*) pDrvIf->pDrvSpecific)->pPeri);
	pDrvIf->Sys.Stat.LastEvent = EV_StateChanged;
	CHAL_MRT_Clear_IRQ_Status_CH(((drv_MRT_If_Spec_t*) pDrvIf->pDrvSpecific)->pPeri, CHAL_MRT_Get_IRQ_Status_CH(((drv_MRT_If_Spec_t*) pDrvIf->pDrvSpecific)->pPeri));
}

//// ------------------------------------------------------------------------------------------------------
//// After handler
//// fire event system
//inline void MRT_Handler_Bottom(_drv_If_t *pDrvIf)									// Process Handler  - Overrun irq
//{
//	if(pDrvIf->pIRQ_USR_HandlerBottom) pDrvIf->pIRQ_USR_HandlerBottom(pDrvIf);		// override with USR handler
//	else
//	{
//		if(pDrvIf->Sys.Stat.Initialized == false) return;
//		if(pDrvIf->Sys.Stat.Loaded == false) return;
//		
//		
////		if((pDrvIf->pAPI_USR) && (pDrvIf->pAPI_USR->Events)) pDrvIf->pAPI_USR->Events( pDrvIf, EV_StateChanged);	// if is set USR event system, call it
////		else
//		{
//			if(pDrvIf->pAPI_STD->Events) pDrvIf->pAPI_STD->Events( pDrvIf, EV_StateChanged);	// if is set STD event system, call it
//		}
//	}
//	
//	CHAL_MRT_Clear_CHIRQ_Status(((drv_MRT_If_Spec_t*) pDrvIf->pDrvSpecific)->pPeri, CHAL_MRT_Get_CHIRQ_Status(((drv_MRT_If_Spec_t*) pDrvIf->pDrvSpecific)->pPeri));
//}




// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************
// ------------------------------------------------------------------------------------------------
// Event system - volane v preruseni !!!
inline void MRT_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{

//	switch(EventType)
//	{
//		case EV_StateChanged:
//		{
//			void* CallBack;		
//			_ColListRecord_t *tmpListRecord = pDrvIf->Sys.ParentModLinked.pFirst;	// temporary pointer to parent module list
//			while(tmpListRecord)
//			{
//				CallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_ChangedState;

//				if(CallBack)		// callback is set?
//				{
//					sys_Task_Create_Item("MRT_Event",CallBack, (sys_TaskArgs_t) {NULL, 0});// create task for data read
//				}

//				if(tmpListRecord->pNext == NULL) break;								// loop while have associadet module
//				tmpListRecord = tmpListRecord->pNext;								// goto next associated module
//			}
//			break;
//		}
//		case EV_NoChange:
//		default: break;
//	}
}

#endif	// ifdef __DRV_MRT_H_
