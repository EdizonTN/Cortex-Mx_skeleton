// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_SSD1306.c
// 	   Version: 1.02
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Driver for OLED/PLED SSD1306 IC
// ******************************************************************************
// Info: uses I2C/SPI driver. 
//			based on: https://github.com/thinkoco/mi-lcd/blob/master/Linux_driver/4.19.x/tinydrm/fb_SSD1306.c
// Notice:
//
// Usage:
//			
// ToDo:	
//
// Changelog:
//				2024.09.30	- v1.02	- add sys_NULL_Function instead a NULL pointer to a function in API structure
//				2024.08.28	- v1.01 - change Tx size parameter to uint 32 
//				2024.06.07	- first release

#include "Skeleton.h"

#if defined(USE_DRV_SSD1306) && (USE_DRV_SSD1306 == 1)


// ******************************************************************************************************
// Functions prototypes
// ******************************************************************************************************
void SSD1306_Reset 							(  drv_SSD1306_If_Spec_t *pDrvIf_Spec);
bool SSD1306_Init 							( _drv_If_t *pDrvIf, I2C_Device_t *pDevice);
//uint32_t SSD1306_Read 					( I2C_Device_t *pDevice, uint8_t SrcReg, sys_Buffer_t *Dst, uint8_t DstInc, uint8_t DataCount);
uint32_t SSD1306_Write						( I2C_Device_t *pDevice, void *Src, uint8_t SrcInc, uint32_t DataCount);
//void SSD1306_MirrorRegs					( _drv_If_t *pDrvIf);
void SSD1306_SetPosXY						( I2C_Device_t *pDevice, uint32_t PosX, uint32_t PosY);
void SSD1306_WriteCMD						( I2C_Device_t *pDevice, unsigned char Command);

// ******************************************************************************************************
// Public Function - specific for this driver
// ******************************************************************************************************
const drv_SSD1306_Ext_API_t	SSD1306_Ext_API 	=
{
	.Reset									= SSD1306_Reset,
//	.Read									= SSD1306_Read,
	.Write									= SSD1306_Write,
	.Init									= SSD1306_Init,
//	.Reload_MirrorRegs						= SSD1306_MirrorRegs,
};


// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************

// structure and assign to standard API functions:
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 	drv_SSD1306_API_STD = 
{
 	.Init			= sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Restart		= sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		= sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			= sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Disable		= sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Write_Data		= sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Read_Data		= sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Events			= sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Ext			= (_drv_Api_t *) &SSD1306_Ext_API,
};
#pragma pop

sys_Buffer_t* 					Buff_SSD1306_Regs;
#define SSD1306_REGS 			((uint8_t *)(Buff_SSD1306_Regs->pDataArray))

// ******************************************************************************************************
// Private Functions
// ******************************************************************************************************


// ******************************************************************************************************
// EXTended Functions
// ******************************************************************************************************
// ------------------------------------------------------------------------------------------------------
// EXT
// Inicialization
bool SSD1306_Init (_drv_If_t *pDrvIf, I2C_Device_t *pDevice)
{
	uint32_t Result;	
	bool res = false;
	
	uint8_t init_array[] = 
	{ // cmd, value:
		0xae | 0x00,																// Set display 0: OFF, 1: ON.
		0xa0 | 0x00, 																// Set Segment Remap (0 / 1). OR-ed Value to command code (MIRROR horizontal)
		0xa8,  0x3f,																// Set multiplex ratio (0 - 63). Next byte is value
		0xd3,  0x00,																// Set display offset (0 - 63). Next byte is value
		0xda,  0x12,																// Set COM HW Config. Next byte is value
		0xa4 | 0x00,																// Entire Display ON (0/1). 0:RESET, 1: ON. OR-ed Value to command code
		0xa6 | 0x01,																// 0: Set normal display, 1: set inverse display
		0xd5,  0x80,																// Set Clock divider (0 - 15). Next byte is value
		0x8d,  0x14,																// Charge Pump 0x10: Disable, 0x14: Enable
		0x81,  0xcf,																// Set Contrast (0 - 255). Next byte is value
		0xae | 0x01,																// Set display 0: OFF, 1: ON.
		0xc0 | 0x00,																// Set COM Output Scan Direction  (0 / 8)
		0xb0 | 0x00,																// Set page start address for page addresing mode. 0-7
		0x00 | 0x00,																// Set lower column start address. 0-15
		0x10 | 0x00,																// Set Higher Column start address. 0-15 
		0x20,  0x02,																// Set Memory Addressing Mode. 0-3
		0x40 | 0x00																	// Set display start line 0-63
	};
	
	
	pDevice->Arg_RegAdr[0] = SSD1306_REG_COMMAND;
	Result = SSD1306_Write( pDevice, &init_array[0], 1, sizeof( init_array ));	// write init sequence

	if(Result == sizeof( init_array )) res = true;

	pDrvIf->Sys.Stat.Initialized = res;
	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// EXT
// Reset SSD1306 device
void SSD1306_Reset (drv_SSD1306_If_Spec_t *pDrvIf_Spec)
{
	// Reset chip
	if(pDrvIf_Spec->pSig_RST != NULL)
	{
		CHAL_Signal_Set_Pin(pDrvIf_Spec->pSig_RST->Std);							// activate reset signal
		Delay_us(5);																// reset pulse delay
		CHAL_Signal_Clear_Pin(pDrvIf_Spec->pSig_RST->Std);							// activate reset signal
		Delay_ms(3);																// after-reset delay
	}
}


// ------------------------------------------------------------------------------------------------------
// EXT
// Read from SSD1306 device
uint32_t SSD1306_Read (I2C_Device_t *pDevice, uint8_t SrcReg, sys_Buffer_t *Dst, uint8_t DstInc, uint8_t DataCount)
{
	uint32_t Result;
	
	((drv_I2C_If_Spec_t* ) pDevice->pHost_If_Spec)->pXFer_Dev = pDevice;			// Link to comm. device
	
	pDevice->TxBuffIncrement = DstInc; 												// save buffer increment value
	
	pDevice->Arg_RegAdr[0] = SrcReg;												// save reader register
	
	uint8_t RetryNum = CONF_DRV_SSD1306_RETRY_COUNT;
	while(Result != DataCount)
	{
		Result = ((drv_I2C_If_Spec_t* ) pDevice->pHost_If_Spec)->pDrvIf->pAPI_STD->Read_Data(NULL, ((drv_I2C_If_Spec_t* ) pDevice->pHost_If_Spec)->pDrvIf, Dst, DataCount, XFer_Blocking);	// Read
		if(RetryNum-- == 0) break;
	}
		
	return(Result);
}

// ------------------------------------------------------------------------------------------------------
// EXT
// Write to SSD1306 device
uint32_t SSD1306_Write(I2C_Device_t *pDevice, void *Src, uint8_t SrcInc, uint32_t DataCount)
{
	uint32_t Result;
	bool	 Res = false;
	
	((drv_I2C_If_Spec_t* ) pDevice->pHost_If_Spec)->pXFer_Dev = pDevice;			// Link to comm. device
	
	pDevice->TxBuffIncrement = SrcInc; 												// save buffer increment value
	
	uint8_t RetryNum = CONF_DRV_SSD1306_RETRY_COUNT;
	while(Res == false)
	{
		Res = ((drv_I2C_If_Spec_t* ) pDevice->pHost_If_Spec)->pDrvIf->pAPI_STD->Write_Data(NULL, ((drv_I2C_If_Spec_t* ) pDevice->pHost_If_Spec)->pDrvIf, Src, DataCount, XFer_Blocking);	// Write
		if(RetryNum-- == 0) break;
	}
		
	Result = DataCount - pDevice->pHost_If_Spec->XFer_Rec.TxSize;
	
	return(Result);
}


// ------------------------------------------------------------------------------------------------
// Read all charger regs 
//void SSD1306_MirrorRegs(_drv_If_t *pDrvIf)
//{
//	SSD1306_read(pDrvIf, 0x00, Buff_SSD1306_Regs, SSD1306_REG_COUNT);				// reread all regs
//}


// ------------------------------------------------------------------------------------------------------
// EXT
// Set cursor position
//void SSD1306_SetPosXY ( I2C_Device_t *pDevice, uint32_t PosX, uint32_t PosY)
//{
//	SSD1306_WriteCMD(pDevice, 0xb0 + PosX);              							// set page address
//	SSD1306_WriteCMD(pDevice, 0x00 + (8 * PosY & 0x0f)); 							// set low col address
//	SSD1306_WriteCMD(pDevice, 0x10 + ((8 * PosY >> 4) & 0x0f)); 					// set high col address	
//}


// ------------------------------------------------------------------------------------------------------
// EXT
// Write command to SSD1306
//void SSD1306_WriteCMD(I2C_Device_t *pDevice, unsigned char Command)
//{
//	pDevice->Arg_RegAdr[0] = SSD1306_REG_COMMAND;

//	SSD1306_Write ( pDevice, Command, true, 1);
//}


// ******************************************************************************************************
// USR Functions
// ******************************************************************************************************


// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// no interrupts in this driver

// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************

// no interrupts in this driver



// ------------------------------------------------------------------------------------------------------
// EXT


#endif	// USE_DRV_SSD1306
