// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_ETH.c
// 	   Version: 1.02
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for ETH
// ******************************************************************************
// Info: ETH driver. 
//
// Notice:
//
// Usage:
//			
// ToDo:
//
// Changelog:
//				2024.09.30	- v1.02 - add sys_NULL_Function instead a NULL pointer to a function in API structure
//				2023.12.10	- v1.01 - rename _CHIP_ETHx_IRQ_Handler to _Chip_ETHx_IRQ_Handler
//									- change bootom irq to 	IRQ_Handler_Bottom((void *) ETH_DriverIRQ[x]);
// 

#include "Skeleton.h"

#if defined(USE_DRV_ETH) && (USE_DRV_ETH == 1)

// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool ETH_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool ETH_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool ETH_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
uint32_t ETH_Write_Data(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, void *pbuff, uint32_t WrLen, _XFerType_t XferType);
uint32_t ETH_Read_Data (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, void *pbuff, uint32_t RdLen, _XFerType_t XferType);
void ETH_Events( struct _drv_If *pDrvIf, _EventType_t EventType);
void ETH_Handler_Top(_drv_If_t *pDrvIf);											// Quick Handler
void ETH_Handler_Bottom(_drv_If_t *pDrvIf);											// Process Handler

// ******************************************************************************************************
// Private
// ******************************************************************************************************
bool ETH_WaitForRead (drv_ETH_If_Spec_t *pDrvIf_Spec, uint32_t WaitFor_State, uint32_t NumOfTimeOutCLKPulses);


// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_ETH_API_STD 		= //__SECTION_DATA 	=
{
 	.Init			=	ETH_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	ETH_Enable,
	.Disable		=	ETH_Disable,
	.Write_Data		=	ETH_Write_Data,
	.Read_Data		=	ETH_Read_Data,
	.Events			=	ETH_Events,
};
#pragma pop

//#if defined (CONF_USE_IRQ_MANAGER) && (CONF_USE_IRQ_MANAGER == 1) && defined(ETH_USE_IRQ_MANAGER) && (ETH_USE_IRQ_MANAGER == 1)
//#else
const _drv_If_t *ETH_DriverIRQ[_CHIP_ETH_COUNT];									// Num of the ETHs

// ------------------------------------------------------------------------------------------------------
// ETH INTERRUPT HANDLER
// ------------------------------------------------------------------------------------------------------

#if defined(_CHIP_ETH_COUNT) && (_CHIP_ETH_COUNT > 0)
void _CHIP_ETH0_IRQ_Handler(void)
{
	ETH_Handler_Top((void *) ETH_DriverIRQ[0]);
	//ETH_Handler_Bottom((void *) ETH_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) ETH_DriverIRQ[0]);
}
#endif

#if defined(_CHIP_ETH_COUNT) && (_CHIP_ETH_COUNT > 1)
void _CHIP_ETH1_IRQ_Handler(void)
{
	ETH_Handler_Top((void *) ETH_DriverIRQ[1]);
	//ETH_Handler_Bottom((void *) ETH_DriverIRQ[1]);
	IRQ_Handler_Bottom((void *) ETH_DriverIRQ[1]);
}
#endif


// ------------------------------------------------------------------------------------------------------
// Generic
// Inicializacia ETH-u
bool ETH_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;
	
	drv_ETH_If_Spec_t* pDrvIf_Spec = (drv_ETH_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_ETH) && (CONF_DEBUG_DRV_ETH == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif

	// initialization code here:

	pDrvIf_Spec->pPeri = CHAL_ETH_Init(pDrvIf_Spec->PeriIndex);
	if(pDrvIf_Spec->pPeri) res = true;

	if(res == true)
	{
		//res = CHAL_ETH_Configure (pDrvIf_Spec->pPeri, pDrvIf_Spec->Conf.Speed_kHz, pDrvIf_Spec->Conf.Mode);
	}
	
	pDrvIf->Sys.Stat.Initialized = res;

	ETH_DriverIRQ[pDrvIf_Spec->PeriIndex] = pDrvIf;
	
#if defined(CONF_DEBUG_DRV_ETH) && (CONF_DEBUG_DRV_ETH == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// Generic
// ENABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
bool ETH_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
	drv_ETH_If_Spec_t* pDrvIf_Spec = (drv_ETH_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_ETH) && (CONF_DEBUG_DRV_ETH == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif

	// main code here:
	res = CHAL_ETH_Enable(pDrvIf_Spec->pPeri, true); 								// enable ETH
	pDrvIf->Sys.Stat.Enabled = res;
	
#if defined(CONF_DEBUG_DRV_ETH) && (CONF_DEBUG_DRV_ETH == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// Generic
// DISABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
bool ETH_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
	drv_ETH_If_Spec_t* pDrvIf_Spec = (drv_ETH_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_ETH) && (CONF_DEBUG_DRV_ETH == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif

	// main code here:
	res = CHAL_ETH_Enable(pDrvIf_Spec->pPeri, false); 								// kompletny disable
	pDrvIf->Sys.Stat.Enabled = res;
	
#if defined(CONF_DEBUG_DRV_ETH) && (CONF_DEBUG_DRV_ETH == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// USR - Write data to ETH
// pbuff - pointer to array of data to write
// BuffByteLen - number of bytes for write to device regs or to device memory
// Before calling ust be set:
// pDrvIf_Spec->Dev_Adr - Adress by SSEL pins. Must be set as ETH device for Xfer or will be used last used
// pDrvIf_Spec->Dev_DstAdr - Destination address in device.
// pDrvIf_Spec->Dev_DstAdrSize - Destination address in device in byte
//	.... If not sets, last used will be used!
// pbuff[0] start of data array to send
 uint32_t ETH_Write_Data( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, void *pbuff, uint32_t WrLen, _XFerType_t XferType)
{
//	bool res = false;
	uint32_t Result = 0;

#if defined(CONF_DEBUG_DRV_ETH) && (CONF_DEBUG_DRV_ETH == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif

//	drv_ETH_If_Spec_t* pDrvIf_Spec = (drv_ETH_If_Spec_t*) pDrvIf->pDrvSpecific;
//	if(pDrvIf_Spec->Dev_Address == 0) return(false);								// Bad Device SSEL address
//	
//	if(pDrvIf_Spec->XFer_Rec.Status == CHAL_ETH_STAT_BUSY) return(false);		// ETH device is occupied by another process
//	
//	// Write Data to ETH device

//	// prepare for send:
//	pDrvIf_Spec->XFer_Rec.pRxBuff = NULL;											// receive not active now
//	pDrvIf_Spec->XFer_Rec.pTxBuff = (uint8_t*) pbuff;								// pointer na pole dat + 1
//	
//	pDrvIf_Spec->XFer_Rec.RxBuffSize = 0;											// without receive data
//	pDrvIf_Spec->XFer_Rec.TxSize = WrLen;											// Xfer data size
//	pDrvIf_Spec->XFer_Rec.DevInstr = CHAL_ETH_CMD_WRITE;								// Instruction - Write cmd
//	pDrvIf_Spec->XFer_Rec.Dev_Address = pDrvIf_Spec->Dev_Address;					// set device addres using SSEL pins
//	if(pDrvIf_Spec->Dev_DstAdrSize)													// destination address
//	{
//		pDrvIf_Spec->XFer_Rec.Dev_RegAdrSize = pDrvIf_Spec->Dev_DstAdrSize;			// Set address size in bytes
//		pDrvIf_Spec->XFer_Rec.pDev_RegAdr = (uint8_t*) &pDrvIf_Spec->Dev_DstAdr;		// write device src/dst address if set
//	}
//	else pDrvIf_Spec->Dev_DstAdrSize = 3;
//	
//	pDrvIf_Spec->XFer_Rec.Status = CHAL_ETH_STAT_BUSY;								// Status to BUSY

//	CHAL_ETH_Enable(pDrvIf_Spec->pPeri, true);										// Enable ETH 
//	
//	// Send
//	CHAL_ETH_ReceiveIgnore(pDrvIf_Spec->pPeri);
//	CHAL_ETH_WriteData(pDrvIf_Spec->pPeri, (uint32_t) pDrvIf_Spec->XFer_Rec.DevInstr, 7, pDrvIf_Spec->XFer_Rec.Dev_Address);// write instruction, 

//	CHAL_ETH_IRQMask_Enable(pDrvIf_Spec->pPeri, CHIP_ETH_INTEN_TXRDY, true);		// enable Tx interrupts
//	CHAL_ETH_IRQ_Enable(pDrvIf_Spec->pPeri, true);									// Enable ETH IRQ

//	// all next transfer is controlled by Interrupt handler
//	// firstly write &pDrvIf_Spec->Dev_RegAdr and repeat &pDrvIf_Spec->Dev_RegSize times
//	// next writes Data from pbuff...

// 		if(XferType == XFer_Blocking) 
//		{
//			
//			res = ETH_WaitForRead( pDrvIf_Spec, CHAL_I2C_STAT_BUSY, 100);			// wait for not BUSY and check timeout

//			pDrvIf_Spec->XFer_Rec.Status = CHAL_ETH_STAT_OK;						// Status to normal
//			
//			Result = pDrvIf_Spec->XFer_Rec.TxSize;									// pDrvIf_Spec->XferRec.TxSize must be Zero.
//			if((Result > 0) || (res == false)) res = false;							// some data was not transferred or timeout was occured!
//				else res = true;
//		}else res = true;															// XFer was started, but not finishet yet.....
	

#if defined(CONF_DEBUG_DRV_ETH) && (CONF_DEBUG_DRV_ETH == 1)
	FN_DEBUG_EXIT(pDrvIf->Name)
	FN_DEBUG_FMTPRINT("Xfer: %d", Result);
#endif
	return(Result);
}


// ------------------------------------------------------------------------------------------------
// USR - Read data from ETH

// pDrvIf_Spec->Dev_Address must be set as I2C device for Xfer
// pDrvIf_Spec->Dev_Reg must be set as I2C device source register/address in device memory for Xfer
// pDrvIf_Spec->Dev_RegSize must be set as I2C device source register/address size in bytes
//	.... If not sets, last used will be used!
// return: res: readed data count. Data will be placed into pbuf from [0]
uint32_t ETH_Read_Data(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, void *pbuff, uint32_t maxlen, _XFerType_t XferType)
{
	//drv_OneWire_If_Spec_t* pDrvIf_Spec = (drv_OneWire_If_Spec_t*) pDrvIf->pDrvSpecific;
	//uint32_t rcount = 0;
	//uint32_t rcount = 0;
	//uint8_t	Byte;
	
#if defined(CONF_DEBUG_DRV_ETH) && (CONF_DEBUG_DRV_ETH == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif
//	drv_ETH_If_Spec_t* pDrvIf_Spec = (drv_ETH_If_Spec_t*) pDrvIf->pDrvSpecific;
	
//	if(pDrvIf_Spec->XFer_Rec.Status == CHAL_ETH_STAT_BUSY) return(0);				// ETH device is occupied by another process
//	if(pDrvIf_Spec->Dev_Address == 0) return(0);									// ETH Address not set
//	// Write Data to ETH device

//	// prepare for send:
//	// firstly ETH send command and src address
//	// next will be read data from Device
//	
//	pDrvIf_Spec->XFer_Rec.pRxBuff = (uint8_t*) pbuff;								// receive not active now
//	pDrvIf_Spec->XFer_Rec.RxBuffSize = maxlen;										// readed bytes
//	pDrvIf_Spec->XFer_Rec.pTxBuff = NULL;											// pointer na pole dat + 1

//	pDrvIf_Spec->XFer_Rec.DevInstr = CHAL_ETH_CMD_READ;								// Instruction - Read cmd
//	pDrvIf_Spec->XFer_Rec.Dev_Address = pDrvIf_Spec->Dev_Address;					// set device addres using SSEL pins
//	if(pDrvIf_Spec->Dev_DstAdrSize)													// destination address
//	{
//		pDrvIf_Spec->XFer_Rec.Dev_RegAdrSize = pDrvIf_Spec->Dev_DstAdrSize;			// Set address size in bytes
//		pDrvIf_Spec->XFer_Rec.pDev_RegAdr = (uint8_t*) &pDrvIf_Spec->Dev_DstAdr;		// write device src/dst address if set
//	}
//	else pDrvIf_Spec->Dev_DstAdrSize = 3;

//	pDrvIf_Spec->XFer_Rec.Status = CHAL_ETH_STAT_BUSY;								// Status to BUSY

//	CHAL_ETH_Enable(pDrvIf_Spec->pPeri, true);										// Enable ETH 
//	
//	// Send
//	//CHAL_ETH_ReceiveIgnore(pDrvIf_Spec->pPeri);
//	CHAL_ETH_WriteData(pDrvIf_Spec->pPeri, (uint32_t) pDrvIf_Spec->XFer_Rec.DevInstr, 7, pDrvIf_Spec->XFer_Rec.Dev_Address);// write instruction, 

//	CHAL_ETH_IRQMask_Enable(pDrvIf_Spec->pPeri, CHIP_ETH_INTEN_TXRDY, true);		// enable Tx interrupts
//	CHAL_ETH_IRQ_Enable(pDrvIf_Spec->pPeri, true);									// Enable ETH IRQ
//	
//	// all next transfer is controlled by Interrupt handler
//	// firstly write &pDrvIf_Spec->Dev_RegAdr and repeat &pDrvIf_Spec->Dev_RegSize times
//	// next Read Data from ETH to pbuff...

//	if(XferType == XFer_Blocking) while(pDrvIf_Spec->XFer_Rec.Status == CHAL_ETH_STAT_BUSY){;}
	
#if defined(CONF_DEBUG_DRV_ETH) && (CONF_DEBUG_DRV_ETH == 1)
	FN_DEBUG_EXIT(pDrvIf->Name)
	FN_DEBUG_FMTPRINT("Xfer: %d", Result);
#endif		
	
	return(0);								// return count of physically readed data
}



//// ------------------------------------------------------------------------------------------------------
//// PRIVATE
//// WaitFor State function. With timeout counting. Timeout_Counter is cleared in each ETH Interrupt.
//// Return: True if State was arrived, False if timeout occured
//COMP_PUSH_OPTIONS
//COMP_OPTIONS_O0
//bool ETH_WaitForRead (drv_ETH_If_Spec_t *pDrvIf_Spec, uint32_t WaitFor_State, uint32_t NumOfTimeOutCLKPulses)
//{
//	uint32_t Timeout_Value;
//	Timeout_Value = (sys_System.Part->CPU_Freq / 1000) / CHAL_ETH_Get_BusSpeed( pDrvIf_Spec->pPeri); // get number of CPU tick per one ETH CLK pulse
//	Timeout_Value = (Timeout_Value * NumOfTimeOutCLKPulses) / 13;					// wait n cycles. 13 is asm instruction in do...while
//		
//	while((pDrvIf_Spec->XFer_Rec.Status & WaitFor_State) == WaitFor_State) 			// wait for end of transfer
//	{ 
//		if(pDrvIf_Spec->Timeout_Counter < Timeout_Value) pDrvIf_Spec->Timeout_Counter ++;
//		else 																		// timout occured !!!!!!!!!
//		{
//			CHAL_ETH_SendEOT(pDrvIf_Spec->pPeri);									// Cancel ETH
//			pDrvIf_Spec->XFer_Rec.Status = (CHAL_ETH_Xfr_Status_t) (CHAL_ETH_STAT_ERROR | CHAL_ETH_STAT_TIMEOUT);
//			return(false);
//		}
//	}	
//	if(pDrvIf_Spec->XFer_Rec.Status & CHAL_ETH_STAT_ERROR) return(false);			// some error occured
//	return(true);
//}
//COMP_POP_OPTIONS




// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Quick handler
inline void ETH_Handler_Top(_drv_If_t *pDrvIf)
{
	
	
#if defined(CONF_IDIOTPROOF) && (CONF_IDIOTPROOF == 1)	
	if(pDrvIf == NULL) return;
#endif
	
	if(pDrvIf->pIRQ_USR_HandlerTop) pDrvIf->pIRQ_USR_HandlerTop(pDrvIf);			// override with USR handler
	else
	{
#if defined(CONF_IDIOTPROOF) && (CONF_IDIOTPROOF == 1)
		if(pDrvIf->Sys.Stat.Initialized == false) return;
		if(pDrvIf->Sys.Stat.Loaded == false) return;
#endif
		
		// Generic interrupt routine:
		
		//uint32_t IRQStatus = CHAL_ETH_Get_IRQ_Status(pDrvIf_Spec->pPeri);

	}	
}
// ------------------------------------------------------------------------------------------------------
// After handler
// fire event system
inline void ETH_Handler_Bottom(_drv_If_t *pDrvIf)
{
	if(pDrvIf->pIRQ_USR_HandlerBottom) pDrvIf->pIRQ_USR_HandlerBottom(pDrvIf);				// override with USR handler
	else
	{
		if(pDrvIf->Sys.Stat.Initialized == false) return;
		if(pDrvIf->Sys.Stat.Loaded == false) return;
		
		// Generic interrupt routine:
		if(pDrvIf->pAPI_STD->Events) pDrvIf->pAPI_STD->Events( pDrvIf, EV_DataReceived);	// ak je nastaveny event system, zavolaj ho
	}
}

// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************
// ------------------------------------------------------------------------------------------------
// Event system - volane v preruseni !!!
inline void ETH_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{

//	switch(EventType)
//	{
//		case EV_DataReceived:
//		case EV_TransmitReady:
//		case EV_TransmitDone:	
//		case EV_NoChange:
//		default: break;
//	}
}


#endif	// USE_DRV_ETH
