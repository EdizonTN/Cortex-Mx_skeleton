// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_DMA.c
// 	   Version: 1.22
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for DMA
// ******************************************************************************
// Info: DMA driver
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
//				2024.09.30	- v1.22 - add sys_NULL_Function instead a NULL pointer to a function in API structure
//									- fix CHAL_DMA_Enable to CHAL_DMA_Enable_CH
//				2023.12.10	- v1.21 - rename _CHIP_DMAx_IRQ_Handler to _Chip_DMAx_IRQ_Handler
//				2022.11.30	- v1.2 - change system of load
//				2022.11.28	- changed interrupt handler bottom


#include "Skeleton.h"

#ifdef __DRV_DMA_H_																	// Header was loaded?

// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool DMA_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool DMA_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool DMA_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
void DMA_Events( struct _drv_If *pDrvIf, _EventType_t EventType);
void DMA_Handler_Top(_drv_If_t *pDrvIf);										// quick handler
//void DMA_Handler_Bottom(_drv_If_t *pDrvIf);										// process handler

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_DMA_API_STD 		= 
{
 	.Init			=	DMA_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	DMA_Enable,
	.Disable		=	DMA_Disable,
	.Write_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Read_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Events			=	DMA_Events,
};
#pragma pop

#if (defined (_CHIP_HAVE_DMA) && _CHIP_HAVE_DMA == 1)
ATTR_ALIGNED(512) drv_DMA_CHDesc_t Sub_DMA_Table[_CHIP_DMA_CHANNELS_COUNT][2] 	__SECTION_NOINIT;	// ToDo: prerob na dynamicku alokaciu !
#endif

const _drv_If_t *DMA_DriverIRQ[_CHIP_DMA_COUNT];
// ------------------------------------------------------------------------------------------------------
// DMA INTERRUPT HANDLER
// ------------------------------------------------------------------------------------------------------
#if defined(_CHIP_DMA_COUNT) && (_CHIP_DMA_COUNT > 0)
void _Chip_DMA0_IRQ_Handler(void)	
{
	DMA_Handler_Top((void *) DMA_DriverIRQ[0]);
	//DMA_Handler_Bottom((void *) DMA_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) DMA_DriverIRQ[0]);
}
#endif

#if defined(_CHIP_DMA_COUNT) && (_CHIP_DMA_COUNT > 1)
void _Chip_DMA1_IRQ_Handler(void)	
{
	DMA_Handler_Top((void *) DMA_DriverIRQ[1]);
	//DMA_Handler_Bottom((void *) DMA_DriverIRQ[1]);
	IRQ_Handler_Bottom((void *) DMA_DriverIRQ[1]);
}
#endif

#if defined(_CHIP_DMA_COUNT) && (_CHIP_DMA_COUNT > 2)
void _Chip_DMA2_IRQ_Handler(void)	
{
	DMA_Handler_Top((void *) DMA_DriverIRQ[2]);
	//DMA_Handler_Bottom((void *) DMA_DriverIRQ[2]);
	IRQ_Handler_Bottom((void *) DMA_DriverIRQ[2]);
}
#endif

#if defined(_CHIP_DMA_COUNT) && (_CHIP_DMA_COUNT > 3)
void _Chip_DMA3_IRQ_Handler(void)	
{
	DMA_Handler_Top((void *) DMA_DriverIRQ[2]);
	//DMA_Handler_Bottom((void *) DMA_DriverIRQ[3]);
	IRQ_Handler_Bottom((void *) DMA_DriverIRQ[3]);
}
#endif

// ------------------------------------------------------------------------------------------------------
// USR
// Inicializacia DMA
// ------------------------------------------------------------------------------------------------------
bool DMA_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;
	
	drv_DMA_If_Spec_t* pDrvIf_Spec = (drv_DMA_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_DMA) && (CONF_DEBUG_DRV_DMA == 1)
    FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif

	// initialization code here:
	pDrvIf_Spec->pPeri = CHAL_DMA_Init(pDrvIf_Spec->PeriIndex);	
	if(pDrvIf_Spec->pPeri) res = true;

	// ToDo: prepis na CHAL funkcie !!
#if defined( CONF_CHIP_ID_LPC1549JBD64 ) || defined ( CONF_CHIP_ID_LPC1549JBD100 ) || defined (CONF_CHIP_ID_LPC1549JBD48)	
	((_CHIP_DMA_T*)pDrvIf_Spec->pPeri)->DMACOMMON[0].SETVALID = 0;					// invalidate all transfer channel
	((_CHIP_DMA_T*)pDrvIf_Spec->pPeri)->DMACOMMON[0].SETTRIG = 0;					// zmaz triggery
			
	((_CHIP_DMA_T*)pDrvIf_Spec->pPeri)->CTRL = 0x01;								// povol DMA peiferiu - kanaly su vsak zakazane
	((_CHIP_DMA_T*)pDrvIf_Spec->pPeri)->SRAMBASE = ((uint32_t) (_Chip_DMA_Table));	// kde mame config ??
	// ------------------------------
#else

#endif		
	
	// periferia je teraz spustena (peri_clock=ON) a pripravena prijat konfiguraciu (v nadradeom module YYY_Write_Config). Naslednym ENABLE sa aktivuje

	DMA_DriverIRQ[pDrvIf_Spec->PeriIndex] = pDrvIf;
	
	pDrvIf->Sys.Stat.Initialized = res;
	
#if defined(CONF_DEBUG_DRV_DMA) && (CONF_DEBUG_DRV_DMA == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// USR
// ENABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
// ------------------------------------------------------------------------------------------------------
bool DMA_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
	drv_DMA_If_Spec_t* pDrvIf_Spec = (drv_DMA_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_DMA) && (CONF_DEBUG_DRV_DMA == 1)
    FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif

	// main code here:
	res = CHAL_DMA_Enable_CH(pDrvIf_Spec->pPeri, ChannelBitMask, true);
	pDrvIf->Sys.Stat.Enabled = res;
	
#if defined(CONF_DEBUG_DRV_DMA) && (CONF_DEBUG_DRV_DMA == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// USR
// DISABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
// ------------------------------------------------------------------------------------------------------
bool DMA_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
	drv_DMA_If_Spec_t* pDrvIf_Spec = (drv_DMA_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_DMA) && (CONF_DEBUG_DRV_DMA == 1)
    FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif

	// main code here:
	res = CHAL_DMA_Enable_CH(pDrvIf_Spec->pPeri, ChannelBitMask, false); 				// disable DMA
	pDrvIf->Sys.Stat.Enabled = res;
	
#if defined(CONF_DEBUG_DRV_DMA) && (CONF_DEBUG_DRV_DMA == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Quick handler
// ------------------------------------------------------------------------------------------------------
inline void DMA_Handler_Top(_drv_If_t *pDrvIf)
{
	if(pDrvIf == NULL) return;	
	
	if(pDrvIf->pIRQ_USR_HandlerTop) pDrvIf->pIRQ_USR_HandlerTop(pDrvIf);			// override with USR handler
	else
	{
		NVIC_ClearPendingIRQ((IRQn_Type) (pDrvIf->IRQ_VecNum));
		if(pDrvIf->Sys.Stat.Initialized == false) return;
		if(pDrvIf->Sys.Stat.Loaded == false) return;
		
		// Generic interrupt routine:
		
		// ToDo: prepis na CHAL !!!!
			// zmaz priznak IRQ
		// --------------------------
		
		
	}
}


//// ------------------------------------------------------------------------------------------------------
//// After handler
//// fire event system
//// ------------------------------------------------------------------------------------------------------
//inline void DMA_Handler_Bottom(_drv_If_t *pDrvIf)
//{
//	if(pDrvIf == NULL) return;
//	
//	if(pDrvIf->pIRQ_USR_HandlerBottom) pDrvIf->pIRQ_USR_HandlerBottom(pDrvIf);		// override with USR handler
//	else
//	{
//		if(pDrvIf->Sys.Stat.Initialized == false) return;
//		if(pDrvIf->Sys.Stat.Loaded == false) return;
//		
//		// Generic interrupt routine:
//		
//		if(pDrvIf->pAPI_STD->Events) pDrvIf->pAPI_STD->Events( pDrvIf, EV_DataReceived);	// ak je nastaveny event system, zavolaj ho
//	}
//}





// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************
inline void DMA_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{

//	switch(EventType)
//	{
//		case EV_DataReceived:
//		case EV_NoChange:
//		default: break;
//	}
}
#endif	// ifdef __DRV_DMA_H_
