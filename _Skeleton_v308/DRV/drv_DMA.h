// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_DMA.h
// 	   Version: 4.0
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for DMA - header file
// ******************************************************************************
// Info: MCU Pheripherial driver - DMA
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
// 

#if !defined(__DRV_DMA_H_) 														// prevent to cyclyc loads
 #if defined(USE_DRV_DMA) && (USE_DRV_DMA == 1)									// Need to load?
  #if (!defined (_CHIP_DMA_COUNT) || _CHIP_DMA_COUNT == 0)						// Neeed it, but MCU haven't this type of periphery!
	#error This driver is need to be used, but MCU doesn't have it!
  #else
	// now -> load it.
#define __DRV_DMA_H_

#include "Skeleton.h"

// ******************************************************************************************************
// CONFIGURATION
// ******************************************************************************************************

#ifndef CONF_DEBUG_DRV_DMA
#define	CONF_DEBUG_DRV_DMA				0											// defaultne vypnuty
#endif

// ******************************************************************************************************
// PUBLIC Defines
// ******************************************************************************************************

// DMA channel source/address/next descriptor
typedef struct 
{
	uint32_t  xfercfg;																// Transfer configuration (only used in linked lists and ping-pong configs)
	uint32_t  source;																// DMA transfer source end address
	uint32_t  dest;																	// DMA transfer desintation end address
	uint32_t  next;																	// Link to next DMA descriptor, must be 16 byte aligned
} drv_DMA_CHDesc_t;

// ku generic strukture treba este pridat:
COMP_PACKED_BEGIN
typedef struct drv_DMA_If_Spec
{
			void						*pPeri;										// union pointrov na Periferiu
	const 	uint8_t						PeriIndex;									// index pouzitej periferie
	const 	uint8_t						Channel;									// pouzity DMA Kanal
} drv_DMA_If_Spec_t;
COMP_PACKED_END



// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
extern drv_DMA_CHDesc_t Sub_DMA_Table[_CHIP_DMA_CHANNELS_COUNT][2];	
extern const _drv_Api_t drv_DMA_API_STD;
extern void drv_DMA_Handler_Top(_drv_If_t *pDrvIf);
extern void drv_DMA_Handler_Bottom(_drv_If_t *pDrvIf);

// ******************************************************************************************************
// PRIVATE Defines
// ******************************************************************************************************


#endif		// defined (_CHIP_zzz_COUNT)
#endif		// #if defined(USE_DRV_zzz)
#endif		// __DRV_ZZZ_H_
