// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_PCA995x.c
// 	   Version: 1.02
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Driver for I2C LED Driver NXP PCA995x
// ******************************************************************************
// Info: I2C Chip PCA995x - LED dimmable/blinkable driver. 
//
// Notice:
//
// Usage:
//			
// ToDo:	
//
// Changelog:
//				2024.09.30	- v1.02 - add sys_NULL_Function instead a NULL pointer to a function in API structure
//				2024.03.03	- v1.01	- add retry count for Read/Write operation if any error occured
//				2023.10.24	- v1.00	- first working version

#include "Skeleton.h"

#ifdef __DRV_PCA995X_H_																	// Header was loaded?

// ******************************************************************************************************
// Functions prototypes
// ******************************************************************************************************
void PCA995x_Reset 		( drv_PCA995x_If_Spec_t *pDrvIf_Spec);
bool PCA995x_Init 		(_drv_If_t *pDrvIf, I2C_Device_t *pDevice, uint8_t ModeReg[2]);
uint32_t PCA995x_Read 	(I2C_Device_t *pDevice, uint8_t SrcReg, sys_Buffer_t *Dst, uint8_t DstInc, uint8_t DataCount);
uint32_t PCA995x_Write	(I2C_Device_t *pDevice, uint8_t DstReg, sys_Buffer_t *Src, uint8_t SrcInc, uint8_t DataCount);
bool PCA995x_Is_Error	(I2C_Device_t *pDevice, drv_PCA995x_LEDError_t *ErrFlagRegs);
	
// ******************************************************************************************************
// Public Function - specific for this driver
// ******************************************************************************************************
const drv_PCA995x_Ext_API_t	PCA995x_Ext_API 	=
{
	.Reset			=	PCA995x_Reset,
	.Read			=	PCA995x_Read,
	.Write			=	PCA995x_Write,
	.Init			=   PCA995x_Init,
	.Is_Error		=   PCA995x_Is_Error,
};

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************
// structure and assign to standard API functions:
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 	drv_PCA995x_API_STD 	= 
{
 	.Init			=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Disable		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Write_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Read_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Events			=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Ext			=	(_drv_Api_t *) &PCA995x_Ext_API,
};
#pragma pop

// ******************************************************************************************************
// Functions prototypes
// ******************************************************************************************************





// ******************************************************************************************************
// Private Functions
// ******************************************************************************************************






// ******************************************************************************************************
// EXTended Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// EXT
// Reset PCA995x device
void PCA995x_Reset (drv_PCA995x_If_Spec_t *pDrvIf_Spec)
{
	//drv_PCA995x_If_Spec_t* pDrvIf_Spec = (drv_PCA995x_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

	// Reset chip
	CHAL_Signal_Set_Pin(pDrvIf_Spec->pSig_RST->Std);								// activate reset signal
	Delay_us(5);																	// reset pulse delay
	CHAL_Signal_Clear_Pin(pDrvIf_Spec->pSig_RST->Std);								// activate reset signal
	Delay_ms(3);																	// after-reset delay
}

// ------------------------------------------------------------------------------------------------------
// EXT
// Read from PCA995x device
uint32_t PCA995x_Read (I2C_Device_t *pDevice, uint8_t SrcReg, sys_Buffer_t *Dst, uint8_t DstInc, uint8_t DataCount)
{
	uint32_t Result;
	
	((drv_I2C_If_Spec_t* ) pDevice->pHost_If_Spec)->pXFer_Dev = pDevice;			// Link to comm. device
	
	pDevice->TxBuffIncrement = DstInc; 												// save buffer increment value
	
	pDevice->Arg_RegAdr[0] = SrcReg;												// save reader register
	
	uint8_t RetryNum = CONF_DRV_PCA995X_RETRY_COUNT;
	while(Result != DataCount)
	{
		Result = ((drv_I2C_If_Spec_t* ) pDevice->pHost_If_Spec)->pDrvIf->pAPI_STD->Read_Data(NULL, ((drv_I2C_If_Spec_t* ) pDevice->pHost_If_Spec)->pDrvIf, Dst, DataCount, XFer_Blocking);	// Read
		if(RetryNum-- == 0) break;
	}
		
	return(Result);
}

// ------------------------------------------------------------------------------------------------------
// EXT
// Write to PCA995x device
uint32_t PCA995x_Write(I2C_Device_t *pDevice, uint8_t DstReg, sys_Buffer_t *Src, uint8_t SrcInc, uint8_t DataCount)
{
	uint32_t Result;
	bool	 Res = false;
	
	((drv_I2C_If_Spec_t* ) pDevice->pHost_If_Spec)->pXFer_Dev = pDevice;			// Link to comm. device
	
	pDevice->TxBuffIncrement = SrcInc; 												// save buffer increment value
	
	pDevice->Arg_RegAdr[0] = DstReg;												// save reder register
	
	uint8_t RetryNum = CONF_DRV_PCA995X_RETRY_COUNT;
	while(Res == false)
	{
		Res = ((drv_I2C_If_Spec_t* ) pDevice->pHost_If_Spec)->pDrvIf->pAPI_STD->Write_Data(NULL, ((drv_I2C_If_Spec_t* ) pDevice->pHost_If_Spec)->pDrvIf, Src, DataCount, XFer_Blocking);	// Write
		if(RetryNum-- == 0) break;
	}
		
	Result = DataCount - pDevice->pHost_If_Spec->XFer_Rec.TxSize;
	
	return(Result);
}

// ------------------------------------------------------------------------------------------------------
// EXT
// Check error status of the pDevice
bool PCA995x_Is_Error(I2C_Device_t *pDevice, drv_PCA995x_LEDError_t *ErrFlagRegs)
{
	uint8_t ErrStat = 0;
	
	PCA995x_Read( pDevice, PCA995X_REG_MODE2, (sys_Buffer_t*) &ErrStat, 1, 1);		// Read MODE2
	
	if(ErrStat & PCA995X_MODE2_ERROR)												// error exists ?
	{
		PCA995x_Read( pDevice, PCA995X_REG_EFLAG0, (sys_Buffer_t*) &ErrFlagRegs->Array8[0], 1, 1);	// Read EFlag 0 register
		PCA995x_Read( pDevice, PCA995X_REG_EFLAG1, (sys_Buffer_t*) &ErrFlagRegs->Array8[1], 1, 1);	// Read EFlag 1 register
		PCA995x_Read( pDevice, PCA995X_REG_EFLAG2, (sys_Buffer_t*) &ErrFlagRegs->Array8[2], 1, 1);	// Read EFlag 2 register
		PCA995x_Read( pDevice, PCA995X_REG_EFLAG3, (sys_Buffer_t*) &ErrFlagRegs->Array8[3], 1, 1);	// Read EFlag 3 register
		ErrStat |= PCA995X_MODE2_CLRERR;											// clear error
		PCA995x_Write(pDevice, PCA995X_REG_MODE2,  (sys_Buffer_t*) &ErrStat, 1, 1);	// Write MODE2 for error clear		
		return(true);
	}
	//else ErrFlagRegs->Mem32 = 0;													// clear errors flags
	return(false);
}

// ------------------------------------------------------------------------------------------------------
// EXT
// Inicialization of the PCA995x
bool PCA995x_Init (_drv_If_t *pDrvIf, I2C_Device_t *pDevice, uint8_t ModeReg[2])
{
	uint32_t Result;	
	bool res = false;
	
	uint8_t init_array[] = 
	{
		0x81, 0x04,                                     							//  0x00, 0x01: MODE1, MODE2
		0xaa, 0xaa, 0xaa, 0xaa,                         							//  0x02 - 0x05: LEDOUT[3:0]
		0x00, 0x00,                                     							//  0x06, 0x07: GRPPWM, GRPFREQ
		0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 							//  0x08, 0x0f: PWM[7:0]
		0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 0xfe, 							//  0x10, 0x17: PWM[15:8]
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 							//  0x18, 0x1f: IREF[7:0]
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 							//  0x20, 0x27: IREF[15:8]
		0xcf,                                   									//  0x28: Ramp_Rate_Grp 0
		0x00, 																		//	0x29: Step_Time_Grp 0
		0x00,																		//	0x2a: Hold_Cntl_Grp 0
		0xff,																		//	0x2b: IREF_GRP 0
		0xcf, 0x00, 0x00, 0xff,														//	0x2c - 0x2f: -//- GRP1
		0xcf, 0x00, 0x00, 0xff,														//	0x30 - 0x33: -//- GRP2
		0xcf, 0x00, 0x00, 0xff,														//	0x34 - 0x37: -//- GRP3
		0x00,																		//	0x38: Grad mode Sel 0
		0x00,																		//	0x39: Grad mode Sel 1
		0x00,																		//	0x3a: Grad Grp Sel 0
		0x00,																		//	0x3b: Grad Grp Sel 1
		0x00,																		//	0x3c: Grad Grp Sel 2
		0x00,																		//	0x3d: Grad Grp Sel 3
		0x00,																		//	0x3e: Grad Cntl
		0x0f,																		//	0x3f: Offset delay
		0x00, 0x00, 0x00,															//	0x40 - 0x42: Sub addr 1-3
		0xe0																		//	0x43: All-Call address
	};	
		
	//((drv_PCA995x_If_Spec_t*) pDrvIf->pDrvSpecific)->pDrvIf = pDrvIf;					// set root driver

	Result = PCA995x_Write( pDevice, PCA995X_REG_MODE1 | PCA995X_MODE1_AIF, &init_array[0], 1, sizeof( init_array ));	// write init sequence

	if(Result == sizeof( init_array )) res = true;

	pDrvIf->Sys.Stat.Initialized = res;
	
	return(res);
}



// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// no interrupts in this driver

// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************

// no events in this driver



#endif	// ifdef __DRV_PCA995X_H_
