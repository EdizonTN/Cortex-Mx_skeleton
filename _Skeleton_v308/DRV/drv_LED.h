// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_LED.h
// 	   Version: 3.0
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for LED - header file
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
// 

#ifndef __DRV_LED_H_
#define __DRV_LED_H_

#include "Skeleton.h"

// ******************************************************************************************************
// CONFIGURATION
// ******************************************************************************************************
#ifndef 	CONF_DEBUG_DRV_LED
#define 	CONF_DEBUG_DRV_LED							0							// default je vypnuty
#endif 

// ******************************************************************************************************
// PUBLIC Defines
// ******************************************************************************************************

// ku generic strukture treba este pridat:
COMP_PACKED_BEGIN
typedef struct drv_LED_If_Spec
{
	const CHAL_Signal_t					*pSigIO;									// IO struktura kompletna
	uint32_t							Value;										// posledna zapisana hodnota
} drv_LED_If_Spec_t;
COMP_PACKED_END

COMP_PACKED_BEGIN
typedef struct drv_LED_Ext_API
{
	void   	(*Change)( struct _drv_If *pDrvIf, uint8_t NewValue);
	void   	(*Toggle)( struct _drv_If *pDrvIf);
} drv_LED_Ext_API_t;
COMP_PACKED_END

COMP_PACKED_BEGIN
typedef struct drv_LED_Ext_API
{
	void   	(*Change)( struct _drv_If *pDrvIf, uint8_t NewValue);
	void   	(*Toggle)( struct _drv_If *pDrvIf);
} drv_LED_Ext_API_t;
COMP_PACKED_END

// ******************************************************************************************************
// PUBLIC 
// ******************************************************************************************************
extern const _drv_Api_t drv_LED_API_STD;


// ******************************************************************************************************
// PRIVATE Defines
// ******************************************************************************************************


#endif // __DRV_LED_H_
