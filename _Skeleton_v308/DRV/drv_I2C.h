// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_I2C.h
// 	   Version: 5.4
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for I2C - header file
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//				2024.02.18	- v5.4	- fix bus speed setting from speed mode
//				2024.02.04	- v5.3	- get I2C Speed from real speed fixed
//				2023.12.21	- v5.2	- add I2C Tx Src Buff TxBuffIncrement (size of step in TxBuff readings. 0 = read same value)
//				2021.03.09	- Add slave mode
//				2020.11.12	- Callback system rewritted
//				2020.11.11	- added CONF_DRV_I2C_ARG_REG_ADR_SIZE constant
// 				2020.08.18	- Moved I2C_Xfer_t struct def from Chip.Hal.h 
//							- pRx_Buff and pTxBuff was changet to sys_Buffer_t type

#if !defined(__DRV_I2C_H_) 															// prevent to cyclyc loads
 #if defined(USE_DRV_I2C) && (USE_DRV_I2C == 1)										// Need to load?
  #if (!defined (_CHIP_I2C_COUNT) || _CHIP_I2C_COUNT == 0)							// Neeed it, but MCU haven't this type of periphery!
	#error This driver is need to be used, but MCU doesn't have it!
  #else
	// now -> load it.
#define __DRV_I2C_H_

#include "Skeleton.h"

// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************

#ifndef CONF_DEBUG_DRV_I2C
#define CONF_DEBUG_DRV_I2C					0										// default is OFF
#endif	

#ifndef CONF_DRV_I2C_BUFFERSIZE
 #define CONF_DRV_I2C_BUFFERSIZE			0x0a									// velkost buffera prenasanych znakov
#endif	

#ifndef CONF_DRV_I2C_BUFFERELEMENTSIZE
 #define CONF_DRV_I2C_BUFFERELEMENTSIZE 	1										// velkost znaku v bufferi
#endif	

#ifndef I2C_USE_IRQ_MANAGER 
#define I2C_USE_IRQ_MANAGER 				0										// If IRQ Manager is used, I2C can be leaved from IRQ Managaer setting to "0"
#endif	

#ifndef CONF_DRV_I2C_PING_WAIT_TIMEOUT_CLKS
#define CONF_DRV_I2C_PING_WAIT_TIMEOUT_CLKS		10									// In Ping command - how manyl CLK impulses will wait for ACK
#endif

#ifndef CONF_DRV_I2C_WRADDR_WAIT_TIMEOUT_CLKS
#define CONF_DRV_I2C_WRADDR_WAIT_TIMEOUT_CLKS	10									// In Write address command - how many CLK impulses will wait for ACK
#endif

#ifndef CONF_DRV_I2C_SCAN_BUS_SPEED_KHZ
#define CONF_DRV_I2C_SCAN_BUS_SPEED_KHZ		10										// Bus Scan command find devica atr this bus frequency. (set to low)
#endif

#ifndef CONF_DRV_I2C_ARG_REG_ADR_SIZE
#define CONF_DRV_I2C_ARG_REG_ADR_SIZE		4										// zis eof the adress register in I2C device
#endif

#ifndef CONF_DRV_I2C_MAX_DATA_SIZE
#define CONF_DRV_I2C_MAX_DATA_SIZE			1										// transfered data size in bytes
#endif

#ifndef CONF_DRV_I2C_MAX_DEV_ON_BUS
#define	CONF_DRV_I2C_MAX_DEV_ON_BUS			10										// maximum devices on one I2C bus - used for array of device size
#endif

#define CONF_DRV_I2C_TIMEOUTCLK_XFERBLOCK	10										// clock pulses for timeout in Blocking transfer

// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************

typedef enum 																		// nominal Bus speed in kHz / 100
{
	I2C_SPEED_MODE_UNKNOWN					= 0,
	I2C_SPEED_MODE_STANDARD					= 1,
	I2C_SPEED_MODE_FULL						= 4,
	I2C_SPEED_MODE_FAST						= 10,
	I2C_SPEED_MODE_HIGH						= 32
} I2C_SpeedMode;


// DEVICE STRUCTURES:
COMP_PACKED_BEGIN
typedef struct
{
	uint32_t								CurrentSpeed_kHz;						// I2C actually preset of Speed in kHz
	I2C_SpeedMode							CurrentSpeedMode;						// I2C Nominal Speed in kHz (100k, 400k, 1MHz, 3.2MHz)
} I2C_Dev_Config_t;
COMP_PACKED_END

COMP_PACKED_BEGIN
typedef struct 
{
	uint8_t									RegAdrSize;								// Device destinastion register/address size in bytes
	CHAL_I2C_Mode_t							XferMode;								// Device working Mode - Master, Slave or Monitor	
	I2C_SpeedMode							SpeedMode; 								// I2C Nominal Speed in kHz (100k, 400k, 1MHz, 3.2MHz)	
}I2C_Dev_Parms_t;
COMP_PACKED_END


typedef struct drv_I2C_If_Spec_t 			drv_I2C_If_Spec_t;

COMP_PACKED_BEGIN
typedef struct	I2C_Device
{
	uint16_t 								Dev_HWAddr;								// device I2C address
	I2C_Dev_Config_t						Dev_Config;								// device configuration - can be changed
	I2C_Dev_Parms_t							Dev_Parms;								// device parameters - constant
	uint8_t 								Arg_RegAdr[CONF_DRV_I2C_ARG_REG_ADR_SIZE];	// Source/Destinastion register/address in Device memory for Xfer
	uint8_t									TxBuffIncrement;						// source Tx Buffer pointer Don't increment flag = send same value
	sys_Buffer_t							*pBuff;									// Pointer to Tx Buffer
	drv_I2C_If_Spec_t						*pHost_If_Spec;							// Pointer to Host controller spec_if structure
}I2C_Device_t;
COMP_PACKED_END


typedef struct	
{
	uint16_t 								DstAdd;									// I2C Slave address
	uint8_t									*pTxDevReg;
	uint8_t									TxDevRegSize;
	sys_Buffer_t 							*pTxBuff;								// Pointer to array of bytes to be transmitted
	sys_Buffer_t							*pRxBuff;								// Pointer memory where bytes received from I2C be stored
	uint16_t 								TxSize;									// Number of bytes in transmit array, if 0 only receive transfer will be carried on
	uint16_t 								RxBuffSize;								// Number of bytes for received bytes, if 0 only transmission we be carried on. After received, value is decrement
	volatile CHAL_I2C_Xfr_Status_t			Status;									// Status of the current I2C transfer
} I2C_Xfer_t;


// Driver specific structure
COMP_PACKED_BEGIN
typedef struct drv_I2C_If_Spec_t
{
	const 	uint8_t							PeriIndex;								// index pouzitej periferie
			_CHIP_I2C_T						*pPeri;									// genric pointer to periphery
	const 	CHAL_Signal_t					*pSig_SDA;								// Port SDA
	const 	CHAL_Signal_t					*pSig_SCL;								// Port SCL
			I2C_Dev_Config_t				BusConf;								// pointer na konfiguracnu strukturu I2C zbernice
			CHAL_I2C_Mode_t					BusMode;								// I2C working Mode - Master, Slave or Monitor
			uint16_t						Address;								// Slave or monitor device address
			_drv_If_t 						*pDrvIf;								// ptr to Owners driver struct
			I2C_Xfer_t						XFer_Rec;								// Xfer packet record
			I2C_Device_t					*pXFer_Dev;								// scr/dst device for transfer
	
#if (CONF_SYS_COLLECTIONS_USED == 1)	
			_ColList_t 						Device_List;							// list of detected devices	
#else
			I2C_Device_t					Device_List[CONF_DRV_I2C_MAX_DEV_ON_BUS];	// list of detected devices	
#endif	
			uint32_t						Timeout_Counter;						// counting up to preset value.

} drv_I2C_If_Spec_t;
COMP_PACKED_END

COMP_PACKED_BEGIN
typedef struct drv_I2C_Ext_API
{
	uint8_t 								(*Scan) (struct _drv_If *pDrvIf, uint16_t MaxAddr, uint16_t MaxDevs, bool CreateDevs);	// Scan I2C IFace and find connected devices
	bool 									(*Ping) (struct _drv_If *pDrvIf, uint16_t Dev_HWAddr);									// check device
	bool									(*Set_BusSpeed) (struct _drv_If *pDrvIf, uint32_t MaxSpeed_kHz);						// Change I2C BUS CLK Speed
	bool 									(*Device_AutoConfigure) (struct _drv_If *pDrvIf, I2C_Device_t *pDevice);				// run autoconfigure of sel. device 
	I2C_Device_t*							(*Get_DeviceByAddr) (struct _drv_If *pDrvIf, uint16_t DevHWAddr, bool Rescan);			// return ptr to device based on HWAddr
	I2C_Device_t* 							(*Create_Device) (struct _drv_If *pDrvIf, uint16_t DevHWAddr);							// create device
	bool 									(*Destroy_Device) (struct _drv_If *pDrvIf, I2C_Device_t* pDevice);						// destroy selected device	
} drv_I2C_Ext_API_t;
COMP_PACKED_END


//#define I2C_GET_MODE_FROM_SPEED(RealSpeed)	( RealSpeed <= (I2C_SPEED_MODE_STANDARD * 100) ? I2C_SPEED_MODE_STANDARD:						\
//											( (RealSpeed <= (I2C_SPEED_MODE_FULL * 100)) && (RealSpeed > (I2C_SPEED_MODE_STANDARD * 100)) ? I2C_SPEED_MODE_FULL:						\
//											( (RealSpeed <= (I2C_SPEED_MODE_FAST * 100)) && (RealSpeed > (I2C_SPEED_MODE_FULL * 100))? I2C_SPEED_MODE_FAST:								\
//											( (RealSpeed <= (I2C_SPEED_MODE_HIGH * 100)) && (RealSpeed > (I2C_SPEED_MODE_FAST| * 100))? I2C_SPEED_MODE_HIGH: I2C_SPEED_MODE_UNKNOWN))))

#define I2C_GET_MODE_FROM_SPEED_KHZ(RealSpeed)( (RealSpeed <= (I2C_SPEED_MODE_STANDARD * 100)) ? I2C_SPEED_MODE_STANDARD:						\
											(((RealSpeed <= (I2C_SPEED_MODE_FULL * 100)) && (RealSpeed > (I2C_SPEED_MODE_STANDARD * 100))) ? I2C_SPEED_MODE_FULL:						\
											(((RealSpeed <= (I2C_SPEED_MODE_FAST * 100)) && (RealSpeed > (I2C_SPEED_MODE_FULL * 100))) ? I2C_SPEED_MODE_FAST:							\
											(((RealSpeed <= (I2C_SPEED_MODE_HIGH * 100)) && (RealSpeed > (I2C_SPEED_MODE_FAST * 100))) ? I2C_SPEED_MODE_HIGH: I2C_SPEED_MODE_UNKNOWN))))


#define I2C_GET_SPEED_KHZ_FROM_MODE(I2C_Mode)(I2C_Mode * 100)

// ************************************************************************************************
// PUBLIC
// ************************************************************************************************

extern const _drv_Api_t 		drv_I2C_API_STD;

#endif		// defined (_CHIP_zzz_COUNT)
#endif		// #if defined(USE_DRV_zzz)
#endif		// __DRV_ZZZ_H_
