// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_UART.c
// 	   Version: 3.22
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for UART
// ******************************************************************************
// Info: UART driver. 
//
// Notice: 	- pri volni s tmp parametrom, napr: modmgr_Write_Data(&mod_Servo_If, &UARTServo_If[CH_TEST], &(uint8_t [5]) {0x00, 0x05, 0xDC}, 3);
//				treba pouzit TX RingBuffer!!! Parameter totiz pominie skor ako sa odvysiela.
//
// Usage:
//			
// ToDo:
//				- add FULL DUPLEX - enable transmit during receive. Based on Parameter in .Controllerig structure. Add this parameter also
//
// Changelog:	
//				2024.09.03	- v3.22 - add WaitFor_nXferStat function as public
//									- add WaitFor_RxChars public function
//									- UART_WaitFor_nXferStat fixed
//									- clearing some flags fixed
//									- fix updating controller.TxTimeoutCLKPuls field
//									- fix wrong Tx finishing !!!
//									- add sys_NULL_Function instead a NULL pointer to a function in API structure
//				2024.02.01	- v3.21 - TxTimeoutCnt fixed
//									- modify to new sys_buffer_create parameter
//				2024.01.26	- v3.20	- add Rx Flush
//				2023.01.03	- v3.11 - modification of the UARTs DE  initialization (may cause DE active before UART init)
//									- modify TXIDLE status in the interrupt
//				2022.11.30	- v3.1 - change system of load
//				2022.11.28	- modify due to clock infor moved to Clock structure
//							- changed interrupt handler bottom
//				2022.11.24	- UART Enable - enable without modifying peri interrupts!
//				2021.06.10	- Controller structure renamed to controller
//				2020.10.01	- modified change address 
//				2020.07.20	- separate IRQ ena/disa in drv Ena/disa
//				2020.07.15	- rename UART_WaitFor to UART_WaitFor_XferStat, and Status const modified
//				2020.07.14	- Add FIFO. Add dynamic Tx FIFO sizing based on size of Tx Buffer.
//				2020.06.18	- enable Rx Interrupt after Init
// 				2019.11.11	- Read/write - pbuff changed to sys_Buffer_t

#include "Skeleton.h"

#ifdef __DRV_UART_H_																	// Header was loaded?


// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool UART_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool UART_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool UART_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool UART_Write_Data (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t WrLen, _XFerType_t XferType);
void UART_Events( struct _drv_If *pDrvIf, _EventType_t EventType);
void UART_Handler_Top(_drv_If_t *pDrvIf);											// quick handler
//void UART_Handler_Bottom(_drv_If_t *pDrvIf);										// process handler
bool UART_Configure( struct _drv_If *pDrvIf);										// write Controllerig structure to UART
bool UART_ChangeAddress( struct _drv_If *pDrvIf, uint8_t NewAddress);
bool UART_FlowControl( struct _drv_If *pDrvIf, drv_UART_FlowControl_t FlowControl);
void UART_Rx_Flush( struct _drv_If *pDrvIf);										// clear and prepare Rx Buffers
bool UART_WaitFor_nXferStat (struct _drv_If *pDrvIf, uint32_t WaitFor_nStateMask, uint32_t NumOfTimeOutCLKPulses);
bool UART_WaitFor_RxChars (_drv_If_t *pDrvIf, uint32_t WaitForChar_Count, uint32_t NumOfTimeOutCLKPulses);

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************
const drv_UART_Ext_API_t	UART_Ext_API_STD 	=  									// Extended (non-standard) API's for this driver
{
	.Configure		=	UART_Configure,												// Configure function
	.FlowControl	=	UART_FlowControl,											// Change Flow Control
	.ChangeAddress 	=	UART_ChangeAddress,											// Configure RS485 mode
	.RxFlush		=	UART_Rx_Flush,												// clearerr and prepare Rx Buffers
	.WaitForNStat	=	UART_WaitFor_nXferStat,										// Wait for UART Status or timeout
	.WaitForRxChars =   UART_WaitFor_RxChars										// Wait for receive n chars
};

// structure and assign to standard API functions:
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_UART_API_STD = 											// Default API's for this driver
{
 	.Init			=	UART_Init,													// Initialization function
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	UART_Enable,												// Enable function
	.Disable		=	UART_Disable,												// Disable function
	.Write_Data		=	UART_Write_Data,											// Write  function
	.Read_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Events			=	UART_Events,												// Event system
	.Ext			=	(_drv_Api_t *) &UART_Ext_API_STD							// link to ext structure
};
#pragma pop

const _drv_If_t *UART_DriverIRQ[_CHIP_UART_COUNT];

// ------------------------------------------------------------------------------------------------------
// UART INTERRUPT HANDLER
// ------------------------------------------------------------------------------------------------------
#if defined(_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
void _Chip_UART0_IRQ_Handler(void)
//void _Chip_FLEXCOMM0_IRQ_Handler(void)
{
	UART_Handler_Top((void *) UART_DriverIRQ[0]);
	//UART_Handler_Bottom((void *) UART_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) UART_DriverIRQ[0]);
}
#endif

#if defined(_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 1)
void _Chip_UART1_IRQ_Handler(void)
{
	UART_Handler_Top((void *) UART_DriverIRQ[1]);									// 5 instr
	//UART_Handler_Bottom((void *) UART_DriverIRQ[1]);
	IRQ_Handler_Bottom((void *) UART_DriverIRQ[1]);
}
#endif

#if defined(_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 2)
void _Chip_UART2_IRQ_Handler(void)
{
	UART_Handler_Top((void *) UART_DriverIRQ[2]);
	//UART_Handler_Bottom((void *) UART_DriverIRQ[2]);
	IRQ_Handler_Bottom((void *) UART_DriverIRQ[2]);
}
#endif

#if defined(_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 3)
void _Chip_UART3_IRQ_Handler(void)
{
	UART_Handler_Top((void *) UART_DriverIRQ[3]);
	//UART_Handler_Bottom((void *) UART_DriverIRQ[3]);
	IRQ_Handler_Bottom((void *) UART_DriverIRQ[3]);
}
#endif

#if defined(_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 4)
void _Chip_UART4_IRQ_Handler(void)
{
	UART_Handler_Top((void *) UART_DriverIRQ[4]);
	//UART_Handler_Bottom((void *) UART_DriverIRQ[4]);
	IRQ_Handler_Bottom((void *) UART_DriverIRQ[4]);
}
#endif

#if defined(_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 5)
void _Chip_UART5_IRQ_Handler(void)
{
	UART_Handler_Top((void *) UART_DriverIRQ[5]);
	//UART_Handler_Bottom((void *) UART_DriverIRQ[5]);
	IRQ_Handler_Bottom((void *) UART_DriverIRQ[5]);
}
#endif

#if defined(_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 6)
void _Chip_UART6_IRQ_Handler(void)
{
	UART_Handler_Top((void *) UART_DriverIRQ[6]);
	//UART_Handler_Bottom((void *) UART_DriverIRQ[6]);
	IRQ_Handler_Bottom((void *) UART_DriverIRQ[6]);
}
#endif

#if defined(_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 7)
void _Chip_UART7_IRQ_Handler(void)
{
	UART_Handler_Top((void *) UART_DriverIRQ[7]);
	//UART_Handler_Bottom((void *) UART_DriverIRQ[7]);
	IRQ_Handler_Bottom((void *) UART_DriverIRQ[7]);
}
#endif

#if defined(_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 8)
void _Chip_UART8_IRQ_Handler(void)
{
	UART_Handler_Top((void *) UART_DriverIRQ[8]);
	//UART_Handler_Bottom((void *) UART_DriverIRQ[8]);
	IRQ_Handler_Bottom((void *) UART_DriverIRQ[8]);
}
#endif

#if defined(_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 9)
void _Chip_UART9_IRQ_Handler(void)
{
	UART_Handler_Top((void *) UART_DriverIRQ[9]);
	//UART_Handler_Bottom((void *) UART_DriverIRQ[9]);
	IRQ_Handler_Bottom((void *) UART_DriverIRQ[9]);
}
#endif


// ------------------------------------------------------------------------------------------------------
// STD
// UART's Initialization routine and full Configure based on filled structures
// ------------------------------------------------------------------------------------------------------
bool UART_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;
	
	drv_UART_If_Spec_t* pDrvIf_Spec = (drv_UART_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer for specific struct 

#if defined(CONF_DEBUG_DRV_UART) && (CONF_DEBUG_DRV_UART == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> ParentMod: %s", pRequestorModIf->Name);
#endif		

//	sys_memset_zero(&pDrvIf_Spec->Status, sizeof(pDrvIf_Spec->Status));				// Clear status structure
	
	// initialization code here:
	if(pDrvIf_Spec->pRX) res |= CHAL_Signal_Init(pDrvIf_Spec->pRX);					// Init RX pin if is Set
	if(pDrvIf_Spec->pTX) res |= CHAL_Signal_Init(pDrvIf_Spec->pTX);					// Init TX pin if is Set
	if(pDrvIf_Spec->pRTS) res |= CHAL_Signal_Init(pDrvIf_Spec->pRTS);				// Init RTS pin if is Set
	if(pDrvIf_Spec->pCTS) res |= CHAL_Signal_Init(pDrvIf_Spec->pCTS);				// Init CTS pin if is Set
	if(pDrvIf_Spec->pRE)  res |= CHAL_Signal_Init(pDrvIf_Spec->pRE);				// Init RE pin if is Set

	if(res == true) pDrvIf_Spec->pPeri = CHAL_UART_Init(pDrvIf_Spec->PeriIndex);	// Init and read pPeri address
	if(pDrvIf_Spec->pPeri == NULL) res = false;

	if(res == true) res = UART_Configure( pDrvIf);									// write Config structure to UART, se com parm, DTS, RTS, ....
	
	if(pDrvIf_Spec->pDIRDE)	res |= CHAL_Signal_Init(pDrvIf_Spec->pDIRDE);			// Init DIRDE pin if is Set


	//if(pDrvIf_Spec->Controller.Address) res = UART_ChangeAddress( pDrvIf, pDrvIf_Spec->Controller.Address);// Write address
	
	 if((pDrvIf_Spec->pCTS) && (pDrvIf_Spec->pRTS)) UART_FlowControl( pDrvIf, CTSRTS); // Set flow mode CTS/RTS
	
	
	// Update: pDrvIf_Spec->Controller.CLKperBit field
	UART_WaitFor_nXferStat( pDrvIf, 0 , 0);

#if defined(_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
	CHAL_UARTFIFO_Set_TxTHLevel(pDrvIf_Spec->pPeri, UINT32_MAX);					// Set threshold to max word
	pDrvIf_Spec->Controller.TxFIFO_Max = CHAL_UARTFIFO_Get_TxTHLevel(pDrvIf_Spec->pPeri); // Read back maximum level of Tx Fifo which periphery is able to set

	CHAL_UARTFIFO_Set_RxTHLevel(pDrvIf_Spec->pPeri, UINT32_MAX);					// Set threshold to max word
	pDrvIf_Spec->Controller.RxFIFO_Max = CHAL_UARTFIFO_Get_RxTHLevel(pDrvIf_Spec->pPeri); // Read back maximum level of Rx Fifo which periphery is able to set
	CHAL_UARTFIFO_Set_RxTHLevel(pDrvIf_Spec->pPeri, 1);					 			// Set threshold to 1 element
#endif


#if defined (CONF_USE_IRQ_MANAGER) && (CONF_USE_IRQ_MANAGER == 1) && defined(UART_USE_IRQ_MANAGER) && (UART_USE_IRQ_MANAGER == 1)
	if(res == true) res = irqmgr_isr_Handler_Install(pDrvIf->IRQ_VecNum, (void (*)(void *Parm))pDrvIf->pIRQ_USR_HandlerTop, (void (*)(void *Parm))pDrvIf->pIRQ_USR_HandlerBottom, pDrvIf->IRQ_Priority, (void *)pDrvIf);
#else
	UART_DriverIRQ[pDrvIf_Spec->PeriIndex] = pDrvIf;								// set parameter for interrupt routine
#endif	

	//pDrvIf_Spec->XFer_Rec.WrCount = 0;
	//pDrvIf_Spec->XFer_Rec.WrLen = 0;
	pDrvIf_Spec->XFer_Rec.TxEstimated = 0;

	CHAL_UART_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_RXRDY | CHAL_UART_IRQSTAT_RXERR, true);	// enable RX Interrupt and RX Error interrupt
	
	
	
	//Create transfer buffers
	if(res == true) 
	{
#if (CONF_SYS_BUFFERS_USED == 1)
		pDrvIf_Spec->XFer_Rec.pRxBuff = sys_Buffer_Create(CONF_DRV_UART_BUFFERSIZE, CONF_DRV_UART_BUFFERELEMENTSIZE, sBuffRing, NULL);
		if(res == true) 
		{
			pDrvIf_Spec->XFer_Rec.pTxBuff = sys_Buffer_Create(CONF_DRV_UART_BUFFERSIZE, CONF_DRV_UART_BUFFERELEMENTSIZE, sBuffRing, NULL);
			if(pDrvIf_Spec->XFer_Rec.pTxBuff == NULL) res = false;
			else res = pDrvIf_Spec->XFer_Rec.pTxBuff->Ready;							// store init flag
		}
			
		if(res == false)																// rollback init
		{
			sys_Buffer_Destroy(pDrvIf_Spec->XFer_Rec.pTxBuff);							// clear Tx Buffer
			sys_Buffer_Destroy(pDrvIf_Spec->XFer_Rec.pRxBuff);							// clear Rx Buffer
		}

#else
		pDrvIf_Spec->XFer_Rec.pRxBuff = sys_malloc_zero(CONF_DRV_UART_BUFFERSIZE * CONF_DRV_UART_BUFFERELEMENTSIZE);		// simply allocate memory
		pDrvIf_Spec->XFer_Rec.pTxBuff = sys_malloc_zero(CONF_DRV_UART_BUFFERSIZE * CONF_DRV_UART_BUFFERELEMENTSIZE);		// simply allocate memory
		if(pDrvIf_Spec->XFer_Rec.pRxBuff == NULL) res = false;
		if(pDrvIf_Spec->XFer_Rec.pTxBuff == NULL) res = false;
#endif	
	}
	
	pDrvIf->Sys.Stat.Initialized = res;												// Driver's flag update
	
#if defined(CONF_DEBUG_DRV_UART) && (CONF_DEBUG_DRV_UART == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif		
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// STD
// ENABLE driver
// If ChannelBitMask == UINT32_MAX enable/disable is valid for all channels
// ------------------------------------------------------------------------------------------------------
bool UART_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
	drv_UART_If_Spec_t* pDrvIf_Spec = (drv_UART_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_UART) && (CONF_DEBUG_DRV_UART == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> chMask: 0x%.8x", ChannelBitMask);
#endif

	// main code here:
	res = CHAL_UART_Enable(pDrvIf_Spec->pPeri, true); 								// enable UART
	//CHAL_UART_Enable_IRQ(pDrvIf_Spec->pPeri, ChannelBitMask, true);					// enable Rx/Tx channels
	if(res) CHAL_UART_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, true);					// enable UART in NVIC
	if(res == true) pDrvIf->Sys.Stat.Enabled = true;								// enable was succesfull
	
#if defined(CONF_DEBUG_DRV_UART) && (CONF_DEBUG_DRV_UART == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// STD
// DISABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
// ------------------------------------------------------------------------------------------------------
bool UART_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
	drv_UART_If_Spec_t* pDrvIf_Spec = (drv_UART_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_UART) && (CONF_DEBUG_DRV_UART == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> chMask: 0x%.8x", ChannelBitMask);
#endif

	// main code here:
	res = CHAL_UART_Enable(pDrvIf_Spec->pPeri, false); 								// kompletny disable
	CHAL_UART_Enable_IRQ(pDrvIf_Spec->pPeri, ChannelBitMask, false);				// disable selected Rx/Tx channels
	if(res) CHAL_UART_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);					// disable UART in NVIC
	if(res == true) pDrvIf->Sys.Stat.Enabled = false;								// disable was succesfull

#if defined(CONF_DEBUG_DRV_UART) && (CONF_DEBUG_DRV_UART == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// USR
// pbuff - pointer na bufer s vysielanymi datami. Musi existvat az do ukoncena transferu!
//		Buffer moze byt staticky alebo dynamicky - podla typu vysielania a dlzky spravy.
// vysielania za sebou su umoznene - buffer sa potom doplna a WrLen sa pridava k XFer.WrLen.
// prve vysielanie sa spusti iba ak nie je aktivny prijem pri Half-DUPLEX.
// ------------------------------------------------------------------------------------------------------
bool UART_Write_Data (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t WrLen, _XFerType_t XferType)
{
	bool res = true;
	//uint32_t Result = 0;	
	uint32_t PeriStatus;
	
#if defined(CONF_DEBUG_DRV_UART) && (CONF_DEBUG_DRV_UART == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pSrc: %p, uffByteLen: %d, XFerType: 0x%.1X", pbuff, WrLen, XferType);
#endif	

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	if(pDrvIf == NULL) res = false;													// Bad pointer
	else res = true;
#endif
	
	drv_UART_If_Spec_t* pDrvIf_Spec = (drv_UART_If_Spec_t*) pDrvIf->pDrvSpecific;

	//	if(pbuff == NULL) pbuff = pDrvIf_Spec->XFer_Rec.pTxBuff;						// if pbiuff wasn't passed, use transfer buffer
	
	//if(pDrvIf_Spec->XFer_Rec.WrCount != pDrvIf_Spec->XFer_Rec.WrLen) {Result = pDrvIf_Spec->XFer_Rec.WrCount; res = false;}// transfer in progress?
	//if(pDrvIf_Spec->XFer_Rec.Status == CHAL_UART_STAT_TXBUSY) res = false;		// UART is occupied by another process
	
	if((pDrvIf_Spec->XFer_Rec.TxEstimated > 0) && !(pDrvIf_Spec->XFer_Rec.Status & DRV_UART_STAT_TXUNDERRUN)) // Transmitt not Done!
		res = false;
	PeriStatus = CHAL_UART_Get_Peri_Status(pDrvIf_Spec->pPeri);	
	if((PeriStatus & CHAL_UART_PERISTAT_TXIDLE) == 0) res = false;					// UART is in transsmit - not in TX IDLE state
	
	// ToDo: add FULL DUPLEX Here:
	if(pDrvIf_Spec->Controller.DuplexMode == HalfDuplex)
		if((PeriStatus & CHAL_UART_PERISTAT_RXIDLE) == 0) res = false;				// UART is in receive - not in RX IDLE state
	
	
	if(res == true)
	{
		// Copy SRC buff to DST Buff:
#if (CONF_SYS_BUFFERS_USED == 1)		
		uint32_t countt;
		uint16_t tmp;		
		
		if(pbuff != pDrvIf_Spec->XFer_Rec.pTxBuff)									// if source and transfer Tx buffer are not identical, copy content to transfer buffer
		{
			SYS_CPUCRIT_START();
			while(WrLen--)
			{
				countt = sys_Buffer_Read_Element(pbuff, (uint8_t*) &tmp, 1);		// read src data
				if(countt) countt = sys_Buffer_Write_Element(pDrvIf_Spec->XFer_Rec.pTxBuff, (uint8_t*) &tmp, countt); // write to TxFBuff
				if(countt == 0) break;												// no data or no free space!
				else pDrvIf_Spec->XFer_Rec.TxEstimated ++;
			}
			SYS_CPUCRIT_END();
		}
		else
			pDrvIf_Spec->XFer_Rec.TxEstimated = pDrvIf_Spec->XFer_Rec.pTxBuff->ElementCount;	// else send whole TXBuffer
		
#else
		if( WrLen > CONF_DRV_UART_BUFFERSIZE * CONF_DRV_UART_BUFFERELEMENTSIZE) WrLen = CONF_DRV_UART_BUFFERSIZE * CONF_DRV_UART_BUFFERELEMENTSIZE;
		sys_memcpy( pDrvIf_Spec->XFer_Rec.pTxBuff, pbuff, WrLen);
		pDrvIf_Spec->XFer_Rec.TxEstimated += WrLen;
#endif
		
		pDrvIf_Spec->XFer_Rec.TxLenght = pDrvIf_Spec->XFer_Rec.TxEstimated;
		
		// Check for CTS if RTS/CTS are applied
		SYS_CPUCRIT_START();
		if(pDrvIf_Spec->pCTS)														// Use CTS also?
		{
			while (CHAL_Signal_Read_Pin(pDrvIf_Spec->pCTS->Std) == true){}			// Wait for inactive CTS
		}

	
		if(pDrvIf_Spec->pDIRDE) CHAL_Signal_Set_Pin(pDrvIf_Spec->pDIRDE->Std);		// Use DE also? Activate if yes.
		
		//uint16_t wrr;
		//sys_Buffer_Read_Element(pDrvIf_Spec->XFer_Rec.pTxBuff, (uint8_t *) &wrr, 1);
		//pDrvIf_Spec->XFer_Rec.TxEstimated --;
		
		
		pDrvIf_Spec->XFer_Rec.Status |= DRV_UART_STAT_TXBUSY;						// Status to TX BUSY

		CHAL_UART_Flush_Tx(pDrvIf_Spec->pPeri);										// Flush Tx
		((_CHIP_UART_T*) pDrvIf_Spec->pPeri)->INTENSET |= (1 << 3);					// Enable TX IDLE
		CHAL_UART_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_TXRDY | CHAL_UART_IRQSTAT_TXERR, true);	// enable TX Interrupt and TX Error interrupt
		CHAL_UARTFIFO_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_UARTFIFO_IRQSTAT_TXRDY | CHAL_UARTFIFO_IRQSTAT_TXERR, true);// enable TX FIFO Interrupt

#if defined(_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
		if(pDrvIf_Spec->XFer_Rec.TxEstimated < pDrvIf_Spec->Controller.TxFIFO_Max) CHAL_UARTFIFO_Set_TxTHLevel(pDrvIf_Spec->pPeri, WrLen); // Set threshold to write len
		else CHAL_UARTFIFO_Set_TxTHLevel(pDrvIf_Spec->pPeri, pDrvIf_Spec->Controller.TxFIFO_Max); // Set threshold to maximum
#endif

		//CHAL_UART_Put_Data(pDrvIf_Spec->pPeri, wrr);								// write char to out reg
		//Result = pDrvIf_Spec->XFer_Rec.TxEstimated;								// vrat pocet zapisanych byte do FIFO
		//Delay_us(1);
		if(pDrvIf_Spec->pRTS) CHAL_Signal_Set_Pin(pDrvIf_Spec->pRTS->Std);			// Set Request To Send signal
		SYS_CPUCRIT_END();
		
		if(XferType == XFer_Blocking) 
		{
			
			//res = UART_WaitFor_nXferStat( pDrvIf, DRV_UART_STAT_TXBUSY | DRV_UART_STAT_TXERR, pDrvIf_Spec->Controller.TxTimeoutCLKPuls);	// wait for clear CHAL_UART_STAT_TXBUSY or ERR and check timeout
			res = UART_WaitFor_nXferStat( pDrvIf, DRV_UART_STAT_TXBUSY | DRV_UART_STAT_TXERR, CONF_DRV_UART_WRDAT_WAIT_TIMEOUT_CLKS);	// wait for clear CHAL_UART_STAT_TXBUSY or ERR and check timeout
			
		}else res = true;															// XFer was started, but not finishet yet.....
	}
	
#if defined(CONF_DEBUG_DRV_UART) && (CONF_DEBUG_DRV_UART == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// PRIVATE
// WaitFor State function. With timeout counting. Timeout_Counter is cleared in each UART Tx Interrupt.
// Return: True if State bits was cleared, False if timeout occured
COMP_PUSH_OPTIONS
COMP_OPTIONS_O0
bool UART_WaitFor_nXferStat (_drv_If_t *pDrvIf, uint32_t WaitFor_State, uint32_t NumOfTimeOutCLKPulses)
{
	drv_UART_If_Spec_t* pDrvIf_Spec = (drv_UART_If_Spec_t*) pDrvIf->pDrvSpecific;

	uint32_t Timeout_Value;
	
	if(pDrvIf_Spec->Controller.CLKperBit == 0)
	{
		pDrvIf_Spec->Controller.CLKperBit = CHAL_UART_Get_Clock_Rate(pDrvIf_Spec->pPeri) / pDrvIf_Spec->Controller.BaudRate; // get number of CPU tick per one pulse
	}
	
	Timeout_Value = (pDrvIf_Spec->Controller.CLKperBit * NumOfTimeOutCLKPulses) / 13;					// wait n cycles. 13 is asm instruction in do...while
	
	while(pDrvIf_Spec->XFer_Rec.Status & WaitFor_State) 			// wait for clear WaitFor_State bits
	{ 
		if(pDrvIf_Spec->XFer_Rec.TxTimeoutCnt < Timeout_Value) pDrvIf_Spec->XFer_Rec.TxTimeoutCnt ++;
		else 																		// timout occured !!!!!!!!!
		{
			pDrvIf_Spec->XFer_Rec.Status |= (drv_UART_Xfer_Status_t) (DRV_UART_STAT_TIMEOUT);
			return(false);
		}
	}	
	return(true);
}
COMP_POP_OPTIONS

COMP_PUSH_OPTIONS
COMP_OPTIONS_O0
// ------------------------------------------------------------------------------------------------------
// PRIVATE
// WaitFor receive WaitFor_Count charcter. With timeout counting. Timeout_Counter is cleared in each UART Tx Interrupt.
// Return: True if State bits was cleared, False if timeout occured
bool UART_WaitFor_RxChars (_drv_If_t *pDrvIf, uint32_t WaitForChar_Count, uint32_t NumOfTimeOutCLKPulses)
{
	drv_UART_If_Spec_t* pDrvIf_Spec = (drv_UART_If_Spec_t*) pDrvIf->pDrvSpecific;

	uint32_t Wait_CountValue;
	
	Wait_CountValue = (sys_System.Clock->CPU_Freq / 1) / pDrvIf_Spec->Controller.BaudRate; // get number of CPU tick per one pulse
	
	Wait_CountValue = (Wait_CountValue * NumOfTimeOutCLKPulses) / 13;							// wait n cycles. 13 is asm instruction in do...while
		
	while(Wait_CountValue) 							// wait for receive num of the characters
	{ 
		if(pDrvIf_Spec->XFer_Rec.RxCount < WaitForChar_Count) Wait_CountValue --;	// stil waiting
		else
		{
			return(true);															// all characters arrived in time
		}
	}	
	return(false);																	// Waiter was counted to finish, characters aren't arrived... 
}
COMP_POP_OPTIONS

// ------------------------------------------------------------------------------------------------------
// EXT
// ------------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------------
// EXT
// Write Controlleriguration structure values into UART's regs - Basic UART Function
// ------------------------------------------------------------------------------------------------------
bool UART_Configure( struct _drv_If *pDrvIf)
{
	bool res = false;
	
#if defined(CONF_DEBUG_DRV_UART) && (CONF_DEBUG_DRV_UART == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
#endif	

	drv_UART_If_Spec_t* pDrvIf_Spec = (drv_UART_If_Spec_t*) pDrvIf->pDrvSpecific;
	
	res |= CHAL_UART_Set_Conf ( pDrvIf_Spec->pPeri, pDrvIf_Spec->Controller.BaudRate, pDrvIf_Spec->Controller.DataLen, pDrvIf_Spec->Controller.Parity, pDrvIf_Spec->Controller.StopBits);
	if(res == false)
		goto exitController;													// return false
//	if(res == true)
//	{
//		pDrvIf_Spec->Status.BaudRate = CHAL_UART_Get_Baud( pDrvIf_Spec->pPeri);			// Read_Data_Handler_t back Baud Rate
//		//pDrvIf_Spec->Status.BaudRate = pDrvIf_Spec->Controller.BaudRate;					// save to Status ....
//		pDrvIf_Spec->Status.DataLen = pDrvIf_Spec->Controller.DataLen;
//		pDrvIf_Spec->Status.Parity = pDrvIf_Spec->Controller.Parity;
//		pDrvIf_Spec->Status.StopBits = pDrvIf_Spec->Controller.StopBits;
//	} else goto exitController;															// return false

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// Additional Settings (9-bit mode, RS485 mode,....

	
	//pDrvIf_Spec->XFer_Rec.RTxDataSize = 1;										// we will transfer 1 byte. 
	// Adress resolution mode (also 9-bit) = RS485 mode (flow control can be used for Direction control)
	if((pDrvIf_Spec->Controller.Address) && (pDrvIf_Spec->Controller.Address != 0xff))
	{
		res |= CHAL_UART_Set_Address (pDrvIf_Spec->pPeri, pDrvIf_Spec->Controller.Address);	// Write New Address
		if(res == false) goto exitController;												// return false
		//pDrvIf_Spec->Status.Address = pDrvIf_Spec->Controller.Address;
	}
	
	if((pDrvIf_Spec->pRTS) && (res == true))										// Configure RTS if is entered
	{
		res = CHAL_UART_Set_RTS_Conf(pDrvIf_Spec->pPeri, pDrvIf_Spec->pRTS);
		if(res == false)
		{
			// create virtual RTS control !
			res = true;
		}
	}
	
	if((pDrvIf_Spec->pCTS) && (res == true))										// Configure CTS if is entered
	{
		res = CHAL_UART_Set_CTS_Conf(pDrvIf_Spec->pPeri, pDrvIf_Spec->pCTS);
		if(res == false)
		{
			// create virtual CTS control !
			res = true;
		}
	}


	if((pDrvIf_Spec->pDIRDE) && (res == true))										// Configure DIR/DE if is entered - activate RS485 mode
	{
		res = CHAL_UART_Set_DIRDE_Conf(pDrvIf_Spec->pPeri, pDrvIf_Spec->pDIRDE);
		if(res == false)
		{
			// create virtual CTS control !
			
			res = true;
		}
		
		if(pDrvIf_Spec->pRE)														// Configure RE if is entered 
		{
			res = CHAL_UART_Set_RE_Conf(pDrvIf_Spec->pPeri, pDrvIf_Spec->pRE);
			if(res == false)
			{
				// create virtual CTS control !
				res = true;
			}
		}
	}

	if((pDrvIf_Spec->pTERM) && (res == true))										// Configure Termination pin if is entered
	{
		res = CHAL_GPIO_Init(pDrvIf_Spec->pTERM->Std.Port, pDrvIf_Spec->pTERM->Std.Pin);
	}	

exitController:	
	
#if defined(CONF_DEBUG_DRV_UART) && (CONF_DEBUG_DRV_UART == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif		
	return (res);
}

// ------------------------------------------------------------------------------------------------------
// EXT
// Configure UART for RS485 communication
// ------------------------------------------------------------------------------------------------------
bool UART_ChangeAddress( struct _drv_If *pDrvIf, uint8_t NewAddress)
{
	bool res = false;
	drv_UART_If_Spec_t* pDrvIf_Spec = (drv_UART_If_Spec_t*) pDrvIf->pDrvSpecific;

	res = CHAL_UART_Set_Address(pDrvIf_Spec->pPeri, NewAddress);
	//pDrvIf_Spec->Controller.Address = NewAddress;
	//pDrvIf_Spec->Status.Address = CHAL_UART_Get_Address(pDrvIf_Spec->pPeri);
	//if(res == true) pDrvIf_Spec->Status.Address = pDrvIf_Spec->Controller.Address;		// Save to status
	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// EXT
// Configure UART for RS485 communication
// ------------------------------------------------------------------------------------------------------
bool UART_FlowControl( struct _drv_If *pDrvIf, drv_UART_FlowControl_t FlowControl)
{
	bool res = false;
	drv_UART_If_Spec_t* pDrvIf_Spec = (drv_UART_If_Spec_t*) pDrvIf->pDrvSpecific;

	switch(FlowControl)
	{
		case CTSRTS:
		{
			res = CHAL_UART_Set_CTS_Conf(pDrvIf_Spec->pPeri, pDrvIf_Spec->pCTS);
			res |= CHAL_UART_Set_RTS_Conf(pDrvIf_Spec->pPeri, pDrvIf_Spec->pRTS);
			//if(res == true) pDrvIf_Spec->Status.FlowControl = pDrvIf_Spec->Controller.FlowControl;// Save to status
			break;
		}
		case None:
		default:
		{
			// ToDo: deConfigure previously Configured pin here!!!!!!!!!!!!
			// pDrvIf_Spec->Status.FlowControl = pDrvIf_Spec->Controller.FlowControl;// Save to status
			res = false;
		}
	}
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// EXT
// Clear and prepare Rx Buffers
// ------------------------------------------------------------------------------------------------------
void UART_Rx_Flush( struct _drv_If *pDrvIf)
{
	drv_UART_If_Spec_t* pDrvIf_Spec = (drv_UART_If_Spec_t*) pDrvIf->pDrvSpecific;
	
	pDrvIf_Spec->XFer_Rec.RxCount = 0;
	pDrvIf_Spec->XFer_Rec.Status &= ~(DRV_UART_STAT_RXBUSY | DRV_UART_STAT_RXERR | DRV_UART_STAT_RXOVERBUFF);	// clear Rx flags
	CHAL_UART_Flush_Rx(pDrvIf_Spec->pPeri);
	
#if (CONF_SYS_BUFFERS_USED == 1)	
	sys_Buffer_Flush( pDrvIf_Spec->XFer_Rec.pRxBuff);
	
#else
	//pDrvIf_Spec->XFer_Rec.pRxBuff[0] = 0;
	//pDrvIf_Spec->XFer_Rec.pRxBuff[1] = 0;
#endif	
}



// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Quick handler
inline void UART_Handler_Top(_drv_If_t *pDrvIf)
{
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	if(pDrvIf == NULL) return;														// UART Device not set
	if(pDrvIf->Sys.Stat.Initialized == false) return;								// Device not initialized
	if(pDrvIf->Sys.Stat.Loaded == false) return;									// Device not loaded
#endif

	
	if(pDrvIf->pIRQ_USR_HandlerTop) 												// override with USR handler ????
	{
		pDrvIf->pIRQ_USR_HandlerTop(pDrvIf);			
		return;
	}
	
	//Standard UART handler:

	drv_UART_If_Spec_t* pDrvIf_Spec = (drv_UART_If_Spec_t*) pDrvIf->pDrvSpecific;
	void* pPeri = pDrvIf_Spec->pPeri;
	uint16_t rddata = 0;
	
#if defined(_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
		uint32_t IRQFIFOStatus = 0;	
		IRQFIFOStatus = CHAL_UARTFIFO_Get_IRQ_Status(pPeri);						// Read FIFO status if exists. Else NULL
#endif
	
	uint32_t IRQStatus = 0;
	IRQStatus = CHAL_UART_Get_IRQ_Status(pPeri);									// Read UART status

	
	
	
		// ******** T R A N S M I T T E R   I S   R E A D Y   F O R   N E X T   C H A R  - for high speed commms.
#if defined(_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)	
	if ((IRQStatus & CHAL_UART_IRQSTAT_TXRDY) || (IRQFIFOStatus & CHAL_UARTFIFO_IRQSTAT_TXRDY))
#else
	if (IRQStatus & CHAL_UART_IRQSTAT_TXRDY)
#endif	
	{																				// Handle transmit ready interrupt if enabled
		// *********** Transmit Next Buffer elements:
		//if((pDrvIf_Spec->XFer_Rec.TxEstimated) && (pDrvIf_Spec->XFer_Rec.pTxBuff->ElementCount))		// if data available and ready to send
		if(pDrvIf_Spec->XFer_Rec.TxEstimated)										// if data available and ready to send
		{
			uint16_t wrdata = 0;
			pDrvIf_Spec->XFer_Rec.Status |= DRV_UART_STAT_TXBUSY;					// Set Status to TX BUSY
				
#if defined(_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
			uint8_t  wrmaxcharlen;			
			CHAL_UARTFIFO_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_TXRDY);
			
			
			// Change Tx FIFO level to optimal size:
			if(pDrvIf_Spec->XFer_Rec.TxEstimated < pDrvIf_Spec->Controller.TxFIFO_Max) wrmaxcharlen = pDrvIf_Spec->XFer_Rec.TxEstimated;
			else wrmaxcharlen = pDrvIf_Spec->Controller.TxFIFO_Max ; 				// change TX FIFO Level to maximum
			
			// if buffer has not enought data:
			if(wrmaxcharlen > pDrvIf_Spec->XFer_Rec.pTxBuff->ElementCount) 
			{
				wrmaxcharlen = pDrvIf_Spec->XFer_Rec.pTxBuff->ElementCount; 
				pDrvIf_Spec->XFer_Rec.Status |= DRV_UART_STAT_TXUNDERRUN;			// No Data!!
			}
			
			CHAL_UARTFIFO_Set_TxTHLevel(pDrvIf_Spec->pPeri, wrmaxcharlen); 			// change TX FIFO Level
			
			// Fill the FIFO:
	
			while(wrmaxcharlen --)
			{
				if(sys_Buffer_Read_Element(pDrvIf_Spec->XFer_Rec.pTxBuff, (uint8_t*) &wrdata, 1) == 1)		// read 1 element?
				{					
					// Stop timeout timer
					if(pDrvIf_Spec->XFer_Rec.TxLenght == pDrvIf_Spec->XFer_Rec.TxEstimated)		// first data to send?
					{
// Call to Tx timeout stop:
						// mod_RS485_USR_STOP_Timeout();
					}
					CHAL_UART_Put_Data(pPeri, wrdata);								// SEND
					pDrvIf_Spec->XFer_Rec.TxEstimated --;
					pDrvIf_Spec->XFer_Rec.TxTimeoutCnt = 0;							// Clear timeout counter
				}
				else
				{
					pDrvIf_Spec->XFer_Rec.Status |= DRV_UART_STAT_TXUNDERRUN;		// No Data!!
					break;															// NO DATA IN Tx Buffer !!!
				}
			}
#else		// UART without FIFO - r/w only one byte per interrupt
			CHAL_UART_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_TXRDY);

#if (CONF_SYS_BUFFERS_USED == 1)				
			if(sys_Buffer_Read_Element(pDrvIf_Spec->XFer_Rec.pTxBuff, (uint8_t*) &wrdata, 1) == 1)
#else
			wrdata = pDrvIf_Spec->XFer_Rec.pTxBuff[pDrvIf_Spec->XFer_Rec.TxLenght - pDrvIf_Spec->XFer_Rec.TxEstimated];
			//pDrvIf_Spec->XFer_Rec.pTxBuff ++;
#endif			
			{
				// Stop timeout timer
				if(pDrvIf_Spec->XFer_Rec.TxEstimated)
				{
					CHAL_UART_Put_Data(pPeri, wrdata);								// SEND
					pDrvIf_Spec->XFer_Rec.TxEstimated --;
					pDrvIf_Spec->XFer_Rec.TxTimeoutCnt = 0;							// Clear timeout counter
				}
				else
				{
					pDrvIf_Spec->XFer_Rec.Status |= DRV_UART_STAT_TXUNDERRUN;		// No Data!!
				}
			}
#endif
			//pDrvIf_Spec->XFer_Rec.Status |= DRV_UART_STAT_TXOK;						// set flag
			//return;																// exit now!
		}
		else 
		{						// Correct End of transmission
			CHAL_UART_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_TXRDY, false);// disable TX Interrupt
			CHAL_UART_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_RXIDLE, true);// enable TX Interrupt from IDLE status
			CHAL_UART_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_TXRDY);

//			CHAL_UARTFIFO_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_UARTFIFO_IRQSTAT_TXRDY | CHAL_UARTFIFO_IRQSTAT_TXERR, false);// disable TX Interrupt
//			CHAL_UARTFIFO_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_UARTFIFO_IRQSTAT_TXRDY);
//			pDrvIf_Spec->XFer_Rec.Status &= ~DRV_UART_STAT_TXBUSY;					// clear Status TX BUSY
//			if(pDrvIf_Spec->pRTS) CHAL_Signal_Clear_Pin(pDrvIf_Spec->pRTS->Std);	// Clear Request To Send signal
		}
		return;																		// exit now!
	}

	// ******** T R A N S M I T T E R   I D L E
	if(IRQStatus & CHAL_UART_IRQSTAT_TXIDLE)										// Transmitter is in IDLE state
	{			
		if(pDrvIf_Spec->XFer_Rec.TxEstimated == 0)									// if we finish the send process
		{
			if(pDrvIf_Spec->pDIRDE) CHAL_Signal_Clear_Pin(pDrvIf_Spec->pDIRDE->Std);	// deactivate DE
			CHAL_UART_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_TXIDLE, false);	// disable TX Interrupt
			
			// Correct End of transmission
//			CHAL_UART_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_RXIDLE, true);// enable TX Interrupt from IDLE status
//			CHAL_UART_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_TXRDY);

			CHAL_UARTFIFO_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_UARTFIFO_IRQSTAT_TXRDY | CHAL_UARTFIFO_IRQSTAT_TXERR, false);// disable TX Interrupt
			CHAL_UARTFIFO_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_UARTFIFO_IRQSTAT_TXRDY);
			if(pDrvIf_Spec->pRTS) CHAL_Signal_Clear_Pin(pDrvIf_Spec->pRTS->Std);	// Clear Request To Send signal			
	

			pDrvIf_Spec->XFer_Rec.Status &= ~DRV_UART_STAT_TXBUSY;						// Change Status
		}
		CHAL_UART_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_TXIDLE);
		return;																		// exit now!
	}
	
	
	
	
	
	
	// ******** R E C E I V E D   A   N E W   C H A R
#if defined(_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
	if (IRQFIFOStatus & CHAL_UARTFIFO_IRQSTAT_RXRDY)
	{
		do
		{
#else		
	if (IRQStatus & CHAL_UART_IRQSTAT_RXRDY)										// RECEIVE
	{
#endif		
			pDrvIf_Spec->XFer_Rec.Status |= DRV_UART_STAT_RXBUSY;
			CHAL_UART_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_RXRDY);
			rddata = CHAL_UART_Get_Data(pPeri);										// read received character
			//pDrvIf_Spec->XFer_Rec.Status |= DRV_UART_STAT_RXOK;						// set flag
#if (CONF_SYS_BUFFERS_USED == 1)			
			sys_Buffer_Write_Element(pDrvIf_Spec->XFer_Rec.pRxBuff, (uint8_t *) &rddata, 1);
			pDrvIf_Spec->XFer_Rec.RxCount = pDrvIf_Spec->XFer_Rec.pRxBuff->ElementCount;
#else
			if(pDrvIf_Spec->XFer_Rec.RxCount == CONF_DRV_UART_BUFFERSIZE) 
				pDrvIf_Spec->XFer_Rec.Status |= DRV_UART_STAT_RXOVERBUFF;
			else 
				pDrvIf_Spec->XFer_Rec.RxCount ++;
			*(pDrvIf_Spec->XFer_Rec.pRxBuff + pDrvIf_Spec->XFer_Rec.RxCount-1) = rddata;
			*(pDrvIf_Spec->XFer_Rec.pRxBuff + pDrvIf_Spec->XFer_Rec.RxCount) = 0xFF;
#endif		
		
#if defined(_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
			CHAL_UARTFIFO_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_UARTFIFO_IRQSTAT_RXRDY);
		}while(CHAL_UARTFIFO_Get_Peri_Status(pDrvIf_Spec->pPeri) & CHAL_UARTFIFO_STAT_RXNOEMPTY); // ... until received characters present in FIFO				
#else		
		
		pDrvIf_Spec->XFer_Rec.Status &= ~DRV_UART_STAT_RXBUSY;
		//return;																	// exit now!
#endif	
		return;																		// exit now!
	}	// end of Rxd event Simple Rx
		

	// ******** R E C E I V E D  I S  I D L E
	if(IRQStatus & CHAL_UART_IRQSTAT_RXIDLE)										// Receiver is in IDLE state
	{			
		CHAL_UART_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_RXIDLE, false);	// disable TX Interrupt
		CHAL_UART_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_RXIDLE); 	// clear flag
		pDrvIf_Spec->XFer_Rec.Status &= ~DRV_UART_STAT_RXBUSY;	// Change Status
		return;																		// exit now!
	}

	
	// ******** T R A N S M I T T E R   E R R O R
#if defined(_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
	if(IRQFIFOStatus &  CHAL_UARTFIFO_IRQSTAT_TXERR)								// FIFO Transmitter - error occured
#else
	if(IRQStatus &  CHAL_UARTFIFO_IRQSTAT_TXERR)									// Transmitter - error occured
#endif	
	{			
		if(pDrvIf_Spec->pDIRDE) CHAL_Signal_Clear_Pin(pDrvIf_Spec->pDIRDE->Std);	// deactivate DE
#if defined(_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
		CHAL_UARTFIFO_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_UARTFIFO_IRQSTAT_TXERR, false);	// disable Interrupt
#else
		CHAL_UART_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_TXERR, false);	// disable Interrupt
#endif		
		pDrvIf_Spec->XFer_Rec.Status |= DRV_UART_STAT_TXERR;
		return;																		// exit now!
	}
	
	
	// ******** R E C E I V E D  E R R O R
#if defined(_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)		
	if(IRQFIFOStatus &  CHAL_UARTFIFO_IRQSTAT_RXERR)								// FIFO Receiver - error occured
#else
	if(IRQStatus &  CHAL_UARTFIFO_IRQSTAT_RXERR)									// Receiver - error occured
#endif	
	{			
		CHAL_UART_Flush_Rx(pDrvIf_Spec->pPeri);
#if defined(_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)		
		CHAL_UARTFIFO_Clear_Peri_Status(pDrvIf_Spec->pPeri, CHAL_UARTFIFO_IRQSTAT_RXERR);
		CHAL_UARTFIFO_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_UARTFIFO_IRQSTAT_RXERR, false);	// disable Interrupt
#else
		CHAL_UART_Clear_Peri_Status(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_RXERR);
		CHAL_UART_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_UART_IRQSTAT_RXERR, false);	// disable Interrupt
#endif		
		pDrvIf_Spec->XFer_Rec.Status |= DRV_UART_STAT_RXERR;
		return;																		// exit now!
	}

}


//// ------------------------------------------------------------------------------------------------------
//// After handler
//// fire event system
//// ------------------------------------------------------------------------------------------------------
//inline void UART_Handler_Bottom(_drv_If_t *pDrvIf)
//{
//	drv_UART_If_Spec_t* pDrvIf_Spec = (drv_UART_If_Spec_t*) pDrvIf->pDrvSpecific;

//	if(pDrvIf->pIRQ_USR_HandlerBottom) pDrvIf->pIRQ_USR_HandlerBottom(pDrvIf);		// override with USR handler
//	else
//	{
//		__nop();
//	}
//	if(pDrvIf_Spec->pRTS) CHAL_Signal_Set_Pin(pDrvIf_Spec->pRTS->Std);				// Povol dalsie vysielanie pre mna
//}



// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************
// ------------------------------------------------------------------------------------------------
// Event system - volane v preruseni !!!
// ------------------------------------------------------------------------------------------------------
inline void UART_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{
//#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
//	SYS_ASSERT( pDrvIf != NULL);													// check
//#endif
//	
//	void* pCallBack = NULL;		
//	_ColListRecord_t *tmpListRecord = pDrvIf->Sys.ParentModLinked.pFirst;			// temporary pointer to parent module list
//	while(tmpListRecord)
//	{
//		switch(EventType)
//		{
//			case EV_StateChanged:
//			{
////				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_ChangedState;
//				break;
//			}
//			case EV_DataReceived:
//			{
////				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_DataReceived;
//				break;
//			}
//			case EV_Transmitted:
//			{
////				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_DataTransmitted;
//				break;
//			}
//			default: break;
//		}
//		
//		if(pCallBack)																// callback is set?
//		{
//#if (CONF_SYS_TASKS_USED == 1)			
//	#if defined(CONF_DEBUG_STRING_NAME_SIZE) && (CONF_DEBUG_STRING_NAME_SIZE > 0) 	
//			sys_Task_Create_Item( pDrvIf->Name, pCallBack, (sys_TaskArgs_t) {NULL, 0});	// create task for data read
//	#else
//			sys_Task_Create_Item( "??", pCallBack, (sys_TaskArgs_t) {NULL, 0});	// create task for data read
//	#endif			
//#else
//			pCallBack();
//#endif			
//		}

//		if(tmpListRecord->pNext == NULL) break;										// loop while have associadet module
//		tmpListRecord = tmpListRecord->pNext;										// goto next associated module
//	}
}


#endif	// ifdef __DRV_UART_H_
