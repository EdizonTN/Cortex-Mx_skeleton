// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_HX711.h
// 	   Version: 1.0
//		  Desc: User Driver for HX711 - header file
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
// 

#ifndef __DRV_HX711_H_
#define __DRV_HX711_H_

#include "Skeleton.h"

// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************

#ifndef CONF_DEBUG_DRV_HX711
#define CONF_DEBUG_DRV_HX711			0											// default je vypnuty
#endif	



// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************
COMP_PACKED_BEGIN
typedef struct drv_HX711_Ext_API
{
	void 		(*Reload_MirrorRegs)	(_drv_If_t *pDrvIf);						// reload mirror all charger registers
} drv_HX711_Ext_API_t;
COMP_PACKED_END

// ku generic strukture treba este pridat:
COMP_PACKED_BEGIN
typedef struct drv_HX711_If_Spec
{
			bool 						(*pCallBack_WriteData)(_drv_If_t *pDrvIf, uint8_t DestReg, sys_Buffer_t *pBuff, uint8_t RegsCount);	// handler for data exchange. Must be set from root module!
			uint32_t					(*pCallBack_ReadData) (_drv_If_t *pDrvIf, uint8_t SrcReg, sys_Buffer_t *pBuff, uint8_t RegsCount);	// handler for data exchange. Must be set from root module!
			uint8_t						*pMirrorRegs;
			_drv_If_t					*pCommInterface;							// communication interface
} drv_HX711_If_Spec_t;
COMP_PACKED_END
// ************************************************************************************************
// PRIVATE
// ************************************************************************************************
#define	EC_SUCCESS						0
#define EC_ERROR_UNIMPLEMENTED			2
#define EC_ERROR_BUSY					6

#define HX711_REG_COUNT      			0x15
/* Registers */
#define HX711_REG_INPUT_CURR      		0x00


// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
extern const 							_drv_Api_t drv_HX711_API_STD;

#endif		// __DRV_HX711_H_

