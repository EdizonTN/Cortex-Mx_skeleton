// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_OTM8009.h
// 	   Version: 1.0
//		  Desc: Driver for OTM8009 IC - TFT LCD Driver
// ******************************************************************************
// Info: 
//			https://github.com/STMicroelectronics/stm32-otm8009a/blob/main/otm8009a.c
// Notice:
//
// Usage:
//			
// ToDo:	nad 100kHz SPI bus speed, sa seka SPI write data!!!
//
// Changelog:
//				2021.05.05 - Initial release 

#ifndef __DRV_OTM8009_H_
#define __DRV_OTM8009_H_

#include "Skeleton.h"

// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************

#ifndef CONF_DEBUG_DRV_OTM8009
#define CONF_DEBUG_DRV_OTM8009			0											// default je vypnuty
#endif	

#ifndef CONF_DRV_OTM8009_I2C_ADDR
#define CONF_DRV_OTM8009_I2C_ADDR		0											// I2C address of internal device
#endif	

#define CONF_DRV_OTM8009_USE_SPI		1											// use SPI interface to configure HiRes LCD
 
 
 
 
 
 
 
 
// ************************************************************************************************
// CHECK
// ************************************************************************************************

#ifndef CONF_DRV_OTM8009_USE_I2C
	#ifndef CONF_DRV_OTM8009_USE_SPI
		#error							"You must define OTM8009communicsation interface I2C/SPI !"
	#endif
#endif	

// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************
COMP_PACKED_BEGIN
typedef struct drv_OTM8009_Ext_API
{
	void 		(*Reload_MirrorRegs)	(_drv_If_t *pDrvIf);						// reload mirror all charger registers
	bool 		(*Write_Reg)			(_drv_If_t *pDrvIf, uint16_t Reg, uint8_t *srcMem, uint8_t BytesCount);  
} drv_OTM8009_Ext_API_t;
COMP_PACKED_END

// ku generic strukture treba este pridat:
COMP_PACKED_BEGIN
typedef struct drv_OTM8009_If_Spec
{
//			bool 						(*pCallBack_WriteData)(_drv_If_t *pDrvIf, uint8_t DestReg, sys_Buffer_t *pBuff, uint8_t RegsCount);	// handler for data exchange. Must be set from root module!
//			uint32_t					(*pCallBack_ReadData) (_drv_If_t *pDrvIf, uint8_t SrcReg, sys_Buffer_t *pBuff, uint8_t RegsCount);	// handler for data exchange. Must be set from root module!
//			uint8_t						*pMirrorRegs;
			_drv_If_t					*pCommInterface;							// communication interface
} drv_OTM8009_If_Spec_t;
COMP_PACKED_END
// ************************************************************************************************
// Definitions
// ************************************************************************************************
#define DEF_DRV_OTM8009_SPI_DEVMODE				CHAL_SPI_Slave
#define DEF_DRV_OTM8009_SPI_BUS_RATE_MAX_HZ		100000								// maximal communication speed in Hz
//#define DEF_DRV_OTM8009_SPI_OPCODE_SIZE      	1									// OpCode: 1 byte

//#define DEF_DRV_OTM8009_REG_ELEMENT_SIZE      	1											// DATA size - 1 byte
//#define DEF_DRV_OTM8009_REG_COUNT      			0x15
//#define DEF_DRV_M8009_REG_INPUT_CURR      	0x00


#define DEF_DRV_OTM8009_REG_DEF_NOP				0x0000								// No Operation
#define DEF_DRV_OTM8009_REG_DEF_SLPOUT			0x1100								// Sleep Out
#define DEF_DRV_OTM8009_REG_DEF_CMD2ENA1		0xFF00								// Enable command 2
#define DEF_DRV_OTM8009_REG_DEF_CMD2ENA2		0xFF80								// Enable command 2
#define DEF_DRV_OTM8009_REG_DEF_DISPON			0x2900								// Display ON

#define DEF_DRV_OTM8009_REG_DEF_WRID1			0xD000								// ID1 Setting
#define DEF_DRV_OTM8009_REG_DEF_WRID2			0xD100								// ID2 + ID3 Setting

#define DEF_DRV_OTM8009_BIT_DEF_RW				0x80								// RW bit
#define DEF_DRV_OTM8009_BIT_DEF_DCX				0x40								// DCX bit
#define DEF_DRV_OTM8009_BIT_DEF_HL				0x20								// HL bit

// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
extern const _drv_Api_t 				drv_OTM8009_API_STD;

#endif		// __DRV_OTM8009_H_

