// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_USB.c
// 	   Version: 1.21
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for USB
// ******************************************************************************
// Info: USB driver. 
//
// Notice:
// 			- pri volni s tmp parametrom, napr: modmgr_Write_Data(&mod_Servo_If, &UARTServo_If[CH_TEST], &(uint8_t [5]) {0x00, 0x05, 0xDC}, 3);
//				treba pouzit TX RingBuffer!!! Parameter totiz pominie skor ako sa odvysiela.
// Usage:
//			
// ToDo:
//		- USB_HOST + USB_OTG dorobit....
//		- Interny Stack LPC15xx - optimalizuj. Pri jeho inicializacii sa nastuju callbacky a eventy (cdc_vcom.c)- toto sprav uz v USBD_Init.
//
// Changelog:
//				2023.12.10	- v1.21 - rename _CHIP_ADCx_IRQ_Handler to _Chip_ADCx_IRQ_Handler
//				2022.11.30	- v1.2 - change system of load
// 				11.11.2019	- Read/write - pbuff changed to sys_Buffer_t


//As USB is host-centric,
//	OUT: host -> device
//	IN: device -> host

#include "Skeleton.h"

#ifdef __DRV_USBD_H_																	// Header was loaded?





// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool USBD_Init(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool USBD_Write_Data (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t WrLen, _XFerType_t XferType);
uint32_t USBD_Read_Data(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t RdLen, _XFerType_t XferType);
void USBD_Events( struct _drv_If *pDrvIf, _EventType_t EventType);
void USBD_Handler_Top(_drv_If_t *pDrvIf);											// quick handler
//void USBD_Handler_Bottom(_drv_If_t *pDrvIf);										// process handler
bool USBD_Configure( struct _drv_If *pDrvIf);										// write Controller structure to USBD


// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************
const drv_USBD_Ext_API_t	USBD_Ext_API_STD 	=  									// Extended (non-standard) API's for this driver
{
	.Configure		=	USBD_Configure,												// Configure function
};


#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_USBD_API_STD 				__SECTION_DATA	= 			// Default API's for this driver
{
	.Init			=	USBD_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,	
	.Enable			=	sys_NULL_Function,
	.Disable		= 	sys_NULL_Function,
	.Write_Data		=	USBD_Write_Data,
	.Read_Data		=	USBD_Read_Data,
	.Events			=	USBD_Events,
	.Ext			=	(_drv_Api_t *) &USBD_Ext_API_STD							// link to ext structure
};
#pragma pop

const _drv_If_t *USBD_DriverIRQ[_CHIP_USBD_COUNT];





// ------------------------------------------------------------------------------------------------------
// USB INTERRUPT HANDLERS
// ------------------------------------------------------------------------------------------------------
#if defined(_CHIP_USBD_COUNT) && (_CHIP_USBD_COUNT > 0)
//void _Chip_USB0_IRQ_Handler(void)
//{
//	USBD_Handler_Top((void *) USBD_DriverIRQ[0]);
//	//USBD_Handler_Bottom((void *) USBD_DriverIRQ[0]);
//	IRQ_Handler_Bottom((void *) USBD_DriverIRQ[0]);
//}
#endif














// ------------------------------------------------------------------------------------------------------
// Generic
// Inicializacia USB
// ------------------------------------------------------------------------------------------------------
bool USBD_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;
	
	drv_USBD_If_Spec_t* pDrvIf_Spec = (drv_USBD_If_Spec_t*) pDrvIf->pDrvSpecific;

#if defined(CONF_DEBUG_DRV_USBD) && (CONF_DEBUG_DRV_USBD == 1)
    FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif	
	
	// initialization code here:
	
	// Signal can or cannot be defined....
	if (pDrvIf_Spec->pUSB_DP != NULL) CHAL_Signal_Init(pDrvIf_Spec->pUSB_DP);		// Configure DP pin
	if (pDrvIf_Spec->pUSB_DN != NULL) CHAL_Signal_Init(pDrvIf_Spec->pUSB_DN);		// Configure DN pin
	if (pDrvIf_Spec->pUSB_Connect != NULL) CHAL_Signal_Init(pDrvIf_Spec->pUSB_Connect); // Configure SoftConnect pin
	if (pDrvIf_Spec->pUSB_Connect) CHAL_Signal_Write_Direction(pDrvIf_Spec->pUSB_Connect->Std, CHAL_IO_Input); // deactivate pin
	
	pDrvIf_Spec->pPeri = CHAL_USB_Init(pDrvIf_Spec->PeriIndex);						// HW init and read pPeri address
	if(pDrvIf_Spec->pPeri != NULL) res = true;
	
	if(res == true) res = CHAL_USB_Configure(pDrvIf_Spec->pPeri, pDrvIf_Spec->USB_Mode);	// Configure Clock and mode



#if defined (CONF_USE_IRQ_MANAGER) && (CONF_USE_IRQ_MANAGER == 1) && defined(UART_USE_IRQ_MANAGER) && (UART_USE_IRQ_MANAGER == 1)
	if(res == true) res = irqmgr_isr_Handler_Install(pDrvIf->IRQ_VecNum, (void (*)(void *Parm))pDrvIf->pIRQ_USR_HandlerTop, (void (*)(void *Parm))pDrvIf->pIRQ_USR_HandlerBottom, pDrvIf->IRQ_Priority, (void *)pDrvIf);
#else
	USBD_DriverIRQ[pDrvIf_Spec->PeriIndex] = pDrvIf;								// set parameter for interrupt routine
#endif		



#if defined( CONF_USE_LIB_LPCUSB ) &&( CONF_USE_LIB_LPCUSB == 1)

	// initialization code here (LIBUSB):
	
	USB_CurrentMode[0] = pDrvIf_Spec->USB_Mode;
    HAL_Reset(pDrvIf_Spec->PeriIndex);		// SW Init
	USB_ResetInterface(pDrvIf_Spec->PeriIndex, pDrvIf_Spec->USB_Mode);				// Call usbController.c:USB_Init_Device
	if(res == true) NVIC_EnableIRQ((IRQn_Type)pDrvIf->IRQ_VecNum);					// enable USB interrupts


	pDrvIf_Spec->pXFer_Lib = &lib_LPCUSB_If;										// pripoj lpcUSBlib stack
	res = ((lib_LPCUSB_Api_t *)((lib_LPCUSB_If_t *) pDrvIf_Spec->pXFer_Lib)->pApi)->Init(pDrvIf_Spec->pXFer_Lib, pDrvIf);			// Init USB Stack

	pDrvIf_Spec->pXFer_Lib->pExStack = &VirtualSerial_CDC_Interface;
	//pDrvIf_Spec->pXFer_Lib->pExStack->State.LineEncoding.BaudRateBPS = 0;

	
	USB_Init(pDrvIf_Spec->pXFer_Lib->pExStack->Config.PortNumber, USB_MODE_Device);
#endif





// Receive buffer initialization
//#if defined(CONF_USE_LIB_RINGBUFFER) && (CONF_USE_LIB_RINGBUFFER == 1)	
//	if(res == true) res = lib_RB_Init(pDrvIf_Spec->pRx_Buff); 						// initialize Rx RingBuffer
//#else
//	pDrvIf_Spec->Rx_Buffer_Wr = 0;													// initialize Rx linear buffers
//	pDrvIf_Spec->Tx_Count = 0;
//	pDrvIf_Spec->Rx_Count = 0;	
//	pDrvIf_Spec->Rx_Flag = false;
//#endif

	
	
	
	if(pDrvIf_Spec->pUSB_Connect)													// needs activate USB Connect signal? (D+ via 1k5 to +VCC)
	{
		CHAL_Signal_Set_Pin(pDrvIf_Spec->pUSB_Connect->Std);						// set Pull Up
		CHAL_Signal_Write_Direction(pDrvIf_Spec->pUSB_Connect->Std, CHAL_IO_Output);// Activate pin
	}

	pDrvIf->Sys.Stat.Loaded = true;
	pDrvIf->Sys.Stat.Initialized  = res;
	
#if defined(CONF_DEBUG_DRV_USBD) && (CONF_DEBUG_DRV_USBD == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif		
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// fyzicky zapis dat do USB
// ------------------------------------------------------------------------------------------------------

bool USBD_Write_Data( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t WrLen, _XFerType_t XferType)
{
	bool res = false;
	uint32_t Result = 0;	
	uint32_t wcount = 0;	
	uint32_t timeout = 5000;
	
#if defined(CONF_DEBUG_DRV_USBD) && (CONF_DEBUG_DRV_USBD == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pSrc: %p, uffByteLen: %d, XFerType: 0x%.1X", pbuff, WrLen, XferType);
#endif	
	
#if defined( CONF_USE_LIB_LPCUSB ) &&( CONF_USE_LIB_LPCUSB == 1)
	drv_USBD_If_Spec_t* pDrvIf_Spec = (drv_USBD_If_Spec_t*) pDrvIf->pDrvSpecific;
#endif	

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	if(pDrvIf == NULL) res = false;													// Bad pointer
	else res = true;
#endif

	if(res == true)
	{
		wcount = 0;
		do
		{

#if defined( CONF_USE_ROMAPI_LPCUSB ) &&( CONF_USE_ROMAPI_LPCUSB == 1)
			//wcount += vcom_write((uint8_t*) pbuff, WrLen);
			//wcount = vcom_write((uint8_t*) sys_Buffer_Get_pRd(pbuff), WrLen);
#elif defined( CONF_USE_LIB_LPCUSB ) &&( CONF_USE_LIB_LPCUSB == 1)
			//wcount = pDrvIf_Spec->pXFer_Lib->pApi->Write_Data(pDrvIf_Spec->pXFer_Lib, (const uint8_t*) pbuff, WrLen);	// zapis data do drivera cez lpcUSBlib
			wcount = pDrvIf_Spec->pXFer_Lib->pApi->Write_Data(pDrvIf_Spec->pXFer_Lib, (const uint8_t*) sys_Buffer_Get_pRd(pbuff), WrLen);	// zapis data do drivera cez lpcUSBlib
#endif		
			if(timeout == 0) {Result = 0; res = false; break; }
			else timeout --;
			Result += wcount;
		}while (Result < WrLen);
	}
	
#if defined(CONF_DEBUG_DRV_USBD) && (CONF_DEBUG_DRV_USBD == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// Citanie dat z USB Rx buffera
// ak maxlen=0 a pbuff=NULL, nastava flush receive buffera - znuluje prijem.
// ------------------------------------------------------------------------------------------------------
uint32_t USBD_Read_Data(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t RdLen, _XFerType_t XferType)
{
	uint32_t Result = RdLen;
	
#if defined(CONF_DEBUG_DRV_USBD) && (CONF_DEBUG_DRV_USBD == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pDst: %p, MaxLen: %d, XFerType: 0x%.1X", pbuff, RdLen, XferType);
#endif	

	drv_USBD_If_Spec_t* pDrvIf_Spec = (drv_USBD_If_Spec_t*) pDrvIf->pDrvSpecific;

//#if defined(CONF_USE_LIB_RINGBUFFER) && (CONF_USE_LIB_RINGBUFFER == 1)	
//	if((RdLen == 0) && (pbuff == NULL))												// FLUSH ?
//	{
//		lib_RB_Init(pDrvIf_Spec->XFer_Rec.pRx_Buff);											// reinit RB
//	}
	uint8_t xfercount = 0;
	while(RdLen)
	{											// ktory RB,           kam?
//		if(lib_RB_Read_Element((lib_RB_If_t*) &pDrvIf_Spec->XFer_Rec.pRx_Buff, pbuff) == true) {Result ++; RdLen --;}
//		else break;																	// uz neni co citat

#if (CONF_SYS_BUFFERS_USED == 1)		
		xfercount = sys_Buffer_Write_Element(pbuff, pDrvIf_Spec->XFer_Rec.pRxBuff, 1);
#else
		sys_memcpy(pbuff + xfercount, pDrvIf_Spec->XFer_Rec.pRxBuff, 1);
		xfercount ++;
#endif		
		Result += xfercount;
		RdLen -= xfercount;
	}
//#else
//	pDrvIf_Spec->RxBuffer_Wr = 0;
//#endif
	

	
#if defined(CONF_DEBUG_DRV_USBD) && (CONF_DEBUG_DRV_USBD == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
	FN_DEBUG_FMTPRINT("Xfer: %d", Result);
#endif	

	return(Result);
}







// ------------------------------------------------------------------------------------------------------
// EXT
// ------------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------------
// Write Controlleriguration structure values into UART's regs - Basic UART Function
bool USBD_Configure( struct _drv_If *pDrvIf)
{
	bool res = false;
	
#if defined(CONF_DEBUG_DRV_USBD) && (CONF_DEBUG_DRV_USBD == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
#endif	

//	drv_USBD_If_Spec_t* pDrvIf_Spec = (drv_USBD_If_Spec_t*) pDrvIf->pDrvSpecific;
			
//exitConfigure:	
	
#if defined(CONF_DEBUG_DRV_USBD) && (CONF_DEBUG_DRV_USBD == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif		
	res = true;
	return (res);
}





// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Quick handler
inline void USBD_Handler_Top(_drv_If_t *pDrvIf)
{
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	if(pDrvIf == NULL) return;														// UART Device not set
	if(pDrvIf->Sys.Stat.Initialized == false) return;								// Device not initialized
	if(pDrvIf->Sys.Stat.Loaded == false) return;									// Device not loaded
#endif

	
	if(pDrvIf->pIRQ_USR_HandlerTop) 												// override with USR handler ????
	{
		pDrvIf->pIRQ_USR_HandlerTop(pDrvIf);			
		return;
	}
	
	//Standard UART handler:

	//drv_USBD_If_Spec_t* pDrvIf_Spec = (drv_USBD_If_Spec_t*) pDrvIf->pDrvSpecific;
	//void* pPeri = pDrvIf_Spec->pPeri;
}


// ------------------------------------------------------------------------------------------------------
// Event system - volane v preruseni !!!
// ------------------------------------------------------------------------------------------------------
inline void USBD_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{

//	switch(EventType)
//	{
//		case EV_DataReceived:
//		{
//			void* CallBack;		
//			
//			_ColListRecord_t *tmpListRecord = NULL;
//			do
//			{
//				tmpListRecord = sys_DynCol_Get_Record(&pDrvIf->Sys.ParentModLinked, tmpListRecord);	// temporary pointer to parent module list
//				if (tmpListRecord == NULL) break;
//				
//				CallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_DataReceived;
//				if(CallBack)		// callback is set?
//				{
//					sys_CreateTask("USB_RX_Data", CallBack, (sys_TaskArgs_t) {NULL, 0});// create task for data read
//				}
//			}while(tmpListRecord);
//			
//			break;
//		}
//		case EV_TransmitReady:
//		case EV_StateChanged:
//		case EV_TransmitDone:	
//		case EV_NoChange:
//		default: break;
//	}
}





















/*


#define LPC_USB0_BASE					LPC_USB_BASE
#define	USB0_IRQn						USB_IRQ_IRQn

#if defined( CONF_USE_ROMAPI_LPCUSB ) &&( CONF_USE_ROMAPI_LPCUSB == 1)
#include "drv_USBD.h"
#include "3rd_STACK\USBD\app_usbd_cfg.h"
#include "3rd_STACK\USBD\cdc_vcom.h"


// ******************************************************************************************************
// Private types/enumerations/variables
// ******************************************************************************************************
 static USBD_HANDLE_T g_hUsb;
 const  USBD_API_T *g_pUsbApi;

 
#endif

#if defined( CONF_USE_LIB_LPCUSB ) && ( CONF_USE_LIB_LPCUSB == 1)
 #include "3rd_STACK\lpcUSBlib\Drivers\USB\USB.h"
#endif


// ******************************************************************************************************
// Functions prototypes
// ******************************************************************************************************

#if defined( CONF_CHIP_ID_LPC1549JBD64 ) || defined ( CONF_CHIP_ID_LPC1549JBD100 ) || defined (CONF_CHIP_ID_LPC1549JBD48)
void USBD_154x_Handler_Top(_drv_If_t *pDrvIf);										// quick handler
void USBD_154x_Handler_Bottom(_drv_If_t *pDrvIf);									// process handler
#else
void USBD_17xx_Handler_Top(_drv_If_t *pDrvIf);										// read char to buffer
void USBD_17xx_Handler_Bottom(_drv_If_t *pDrvIf);									// process handler
#endif


const _drv_If_t *USBD_DriverIRQ[_CHIP_USB_COUNT];	
extern void DcdIrqHandler (uint8_t DeviceID);


// ------------------------------------------------------------------------------------------------------
// USB INTERRUPT HANDLER
// ------------------------------------------------------------------------------------------------------

#if defined(_CHIP_USB_COUNT) && (_CHIP_USB_COUNT > 0)
void _Chip_USB0_IRQ_Handler(void)
{
#if defined( CONF_USE_ROMAPI_LPCUSB ) &&( CONF_USE_ROMAPI_LPCUSB == 1)
	USBD_154x_Handler_Top((void *) USBD_DriverIRQ[0]);
	USBD_154x_Handler_Bottom((void *) USBD_DriverIRQ[0]);
#endif

#if	 defined( CONF_USE_LIB_LPCUSB ) &&( CONF_USE_LIB_LPCUSB == 1)
	USBD_17xx_Handler_Top((void *) USBD_DriverIRQ[0]);
//	USBD_17xx_Handler_Bottom((void *) USBD_DriverIRQ[0]);
	
//	if (USB_CurrentMode[0] == USB_MODE_Host) 
//	{
//#if defined(USB_CAN_BE_HOST)
//	//	HcdIrqHandler(0);
//#endif
//	}

//	if (USB_CurrentMode[0] == USB_MODE_Device) 
//	{
//#if defined(USB_CAN_BE_DEVICE)
//		DcdIrqHandler(0);
//#endif
//	}
#endif		//CONF_USE_LIB_LPCUSB
}
#endif





// ******************************************************************************************************
// Public functions
// ******************************************************************************************************


#if defined( CONF_USE_ROMAPI_LPCUSB ) &&( CONF_USE_ROMAPI_LPCUSB == 1)
USB_INTERFACE_DESCRIPTOR *find_IntfDesc(const uint8_t *pDesc, uint32_t intfClass);

// ------------------------------------------------------------------------------------------------------
// Find the address of interface descriptor for given class type.
// ------------------------------------------------------------------------------------------------------
USB_INTERFACE_DESCRIPTOR *find_IntfDesc(const uint8_t *pDesc, uint32_t intfClass)
{
	USB_COMMON_DESCRIPTOR *pD;
	USB_INTERFACE_DESCRIPTOR *pIntfDesc = 0;
	uint32_t next_desc_adr;

	pD = (USB_COMMON_DESCRIPTOR *) pDesc;
	next_desc_adr = (uint32_t) pDesc;
	while (pD->bLength) 
	{
		if (pD->bDescriptorType == USB_INTERFACE_DESCRIPTOR_TYPE) 					// is it interface descriptor?
		{
			pIntfDesc = (USB_INTERFACE_DESCRIPTOR *) pD;
			if (pIntfDesc->bInterfaceClass == intfClass) 							// did we find the right interface descriptor
			{
				break;
			}
		}
		pIntfDesc = 0;
		next_desc_adr = (uint32_t) pD + pD->bLength;
		pD = (USB_COMMON_DESCRIPTOR *) next_desc_adr;
	}
	return pIntfDesc;
}
#endif








// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

#if defined( CONF_USE_ROMAPI_LPCUSB ) &&( CONF_USE_ROMAPI_LPCUSB == 1)
// ------------------------------------------------------------------------------------------------------
// Okamzite vykonana cast prerusenia
// ------------------------------------------------------------------------------------------------------
inline void USBD_154x_Handler_Top(_drv_If_t *pDrvIf)								// read char to buffer
{
	if(pDrvIf == NULL) return;
	
	if(pDrvIf->pIRQ_USR_HandlerTop) pDrvIf->pIRQ_USR_HandlerTop(pDrvIf);			// override with USR handler
	else
	{
		USBD_API->hw->EnableEvent(g_hUsb, 0, USB_EVT_SOF, 1);
		USBD_API->hw->ISR(g_hUsb);													// obsluha HW prerusenia
		if(g_vCOM.tx_flags & VCOM_TX_CONNECTED) ((drv_USBD_If_Spec_t*) pDrvIf->pDrvSpecific)->Enumerated = true;
	}
}

// ------------------------------------------------------------------------------------------------------
// spracovanie ked sa dostane na nas cas.....
// ------------------------------------------------------------------------------------------------------
inline void USBD_154x_Handler_Bottom (_drv_If_t *pDrvIf)							// process buffers
{
	if(pDrvIf == NULL) return;
	
	if(pDrvIf->pIRQ_USR_HandlerBottom) pDrvIf->pIRQ_USR_HandlerBottom(pDrvIf);		// override with USR handler
	else
	{
		drv_USBD_If_Spec_t* pDrvIf_Spec = (drv_USBD_If_Spec_t*) pDrvIf->pDrvSpecific;
		
		if(pDrvIf->Sys.Stat.Initialized == false) return;
		if(pDrvIf->Sys.Stat.Loaded == false) return;
		
		
		// Data received ?
		if((pDrvIf_Spec->Enumerated == true) && (g_vCOM.rx_count > 0))				// USB je ready a ma pre mna data
		{
			//uint32_t elFree, rdCount = 0;
//#if defined(CONF_USE_LIB_RINGBUFFER) && (CONF_USE_LIB_RINGBUFFER == 1)	
//			do
//			{
//				if(lib_RB_IsFull((lib_RB_If_t*) &pDrvIf_Spec->pRX_RB_Buff))				// have free space in RB ??
//				{
//					// buffer overload !!
//					dbgprint("\r\n.USBD.c - RX Buffer overload !\r\n");
//					break;
//				}
//				elFree = lib_RB_Get_Write_elCountLinear((lib_RB_If_t*) &pDrvIf_Spec->pRX_RB_Buff);	// daj mi linearne volne miesto v RB
//				rdCount = vcom_bread((uint8_t *) pDrvIf_Spec->pRX_RB_Buff->pWrite, elFree/pDrvIf_Spec->pRX_RB_Buff->elSize);	// prenes data z USB. rdCount vravi, kolko data naozaj prislo. freespace je maximum co mozem ulozit.
//				lib_RB_Write_External((lib_RB_If_t*) &pDrvIf_Spec->pRX_RB_Buff, rdCount*pDrvIf_Spec->pRX_RB_Buff->elSize);	// poposuvaj RB pointre

//			}while (rdCount == elFree);												// opakuj pokial ma USB nejake data - nacital som plny pokunany linerany priestor
//			
//#else
//			if(pDrvIf_Spec->RxBuffer_Wr >= sizeof(pDrvIf_Spec->RxBuffer)) pDrvIf_Spec->RxBuffer_Wr = 1;
//			else pDrvIf_Spec->RxBuffer_Wr++;
//			pDrvIf_Spec->RxBuffer[pDrvIf_Spec->RxBuffer_Wr - 1] = (uint8_t) &pSRCBuffer.....;				// Store received character
//#endif	
			
			
			vcom_bread(pDrvIf_Spec->XFer_Rec.pRxBuff, pDrvIf_Spec->XFer_Rec.RxBuffSize);
			//rdCount = vcom_bread((uint8_t *) pDrvIf_Spec->pRX_RB_Buff->pWrite, elFree/pDrvIf_Spec->pRX_RB_Buff->elSize);	// prenes data z USB. rdCount vravi, kolko data naozaj prislo. freespace je maximum co mozem ulozit.



			
			if((pDrvIf->pAPI_USR) && (pDrvIf->pAPI_USR->Events)) pDrvIf->pAPI_USR->Events( pDrvIf, EV_DataReceived);	// if is set USR event system, call it
			else
			{
				if(pDrvIf->pAPI_STD->Events) pDrvIf->pAPI_STD->Events( pDrvIf, EV_DataReceived);	// if is set STD event system, call it
			}			
		}
	}
}
#else









// ------------------------------------------------------------------------------------------------------
// Okamzite vykonana cast prerusenia
// ------------------------------------------------------------------------------------------------------
inline void USBD_17xx_Handler_Top(_drv_If_t *pDrvIf)								// 
{
	drv_USBD_If_Spec_t* pDrvIf_Spec = (drv_USBD_If_Spec_t*) pDrvIf->pDrvSpecific;

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)	
	if(pDrvIf == NULL) return;
#endif	
	
	if(pDrvIf->pIRQ_USR_HandlerTop) pDrvIf->pIRQ_USR_HandlerTop(pDrvIf);			// override with USR handler
	else
	{
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
		if(pDrvIf->Sys.Stat.Initialized == false) return;
		if(pDrvIf->Sys.Stat.Loaded == false) return;
#endif
		
		// Generic interrupt routine:

#if defined(USB_CAN_BE_DEVICE)	
		if (pDrvIf_Spec->USB_Mode == CHAL_USB_DEVICE) 
		{
			DcdIrqHandler(pDrvIf_Spec->PeriIndex);
		}
#endif
	}
}

// ------------------------------------------------------------------------------------------------------
// spracovanie ked sa dostane na nas cas.....
// ------------------------------------------------------------------------------------------------------
inline void USBD_17xx_Handler_Bottom (_drv_If_t *pDrvIf)						// process buffers
{
	drv_USBD_If_Spec_t* pDrvIf_Spec = (drv_USBD_If_Spec_t*) pDrvIf->pDrvSpecific;

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)	
	if(pDrvIf == NULL) return;
#endif

	
	if(pDrvIf->pIRQ_USR_HandlerBottom) pDrvIf->pIRQ_USR_HandlerBottom(pDrvIf);		// override with USR handler
	else
	{
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)	
		if(pDrvIf->Sys.Stat.Initialized == false) return;
		if(pDrvIf->Sys.Stat.Loaded == false) return;
#endif		

		// Generic interrupt routine:
		if(pDrvIf_Spec->pXFer_Lib->USB_State == USB_STATE_BIT_CONFIGURED)
		{
			uint32_t rdCount = 0;
			
			rdCount = CDC_Device_BytesReceived(pDrvIf_Spec->pXFer_Lib->pExStack);
#if defined(CONF_USE_LIB_RINGBUFFER) && (CONF_USE_LIB_RINGBUFFER == 1)	
			if(rdCount)
			{
				while(rdCount)														// opakuj pokial ma USB nejake data 
				{
					if(lib_RB_IsFull((lib_RB_If_t*) &pDrvIf_Spec->pRx_Buff))			// have free space in RB ??
					{
						// buffer overload !!
						dbgprint("\r\n.USBD.c - RX Buffer overload !\r\n");
						//break;
					}
					//freespace = lib_RB_Get_WriteElementCountLinear((lib_RB_If_t*) &pDrvIf_Spec->RX_RB_Buff);	// daj mi linearne volne miesto v RB
					//if(CDC_Device_BytesReceived(pDrvIf_Spec->LPCUSB->pExStack))
					{
						int32_t rddata = CDC_Device_ReceiveByte(pDrvIf_Spec->pXFer_Lib->pExStack);	// read byte from USB
						if(rddata >= 0)				// -1 if no data received
						{
							lib_RB_Write_Element((lib_RB_If_t*) &pDrvIf_Spec->pRx_Buff, (void*) &rddata);
							rdCount --;
						}
					}
					
					//rdCount = vcom_bread((uint8_t *) pDrvIf_Spec->RX_RB_Buff.pHead, freespace);				// prenes data z USB. rdCount vravi, kolko data naozaj prislo. freespace je maximum co mozem ulozit.
					//lib_RB_Write_External((lib_RB_If_t*) &pDrvIf_Spec->RX_RB_Buff, rdCount);	// poposuvaj RB pointre

				}
				//CDC_Device_Flush(pDrvIf_Spec->LPCUSB->pExStack);
#else
			if(rdCount)
			{
				while(rdCount)
				{
					if(pDrvIf_Spec->RxBuffer_Wr >= sizeof(pDrvIf_Spec->RxBuffer)) pDrvIf_Spec->RxBuffer_Wr = 1;
					else pDrvIf_Spec->RxBuffer_Wr++;
					pDrvIf_Spec->RxBuffer[pDrvIf_Spec->RxBuffer_Wr - 1] = CDC_Device_ReceiveByte(pDrvIf_Spec->pXFer_Lib->pExStack); // Store received character
					rdCount --;
				}
#endif	
			
				if((pDrvIf->pAPI_USR) && (pDrvIf->pAPI_USR->Events)) pDrvIf->pAPI_USR->Events( pDrvIf, EV_DataReceived);	// if is set USR event system, call it
				else
				{
					if(pDrvIf->pAPI_STD->Events) pDrvIf->pAPI_STD->Events( pDrvIf, EV_DataReceived);	// if is set STD event system, call it
				}			
			}
		}
	}
}
#endif





#if defined (CONF_USE_ROMAPI_LPCUSB) && (CONF_USE_ROMAPI_LPCUSB == 1)
  // user defined functions 
  ErrorCode_t (*SendEncpsCmd) (USBD_HANDLE_T hCDC, uint8_t* buffer, uint16_t len);
  ErrorCode_t (*GetEncpsResp) (USBD_HANDLE_T hCDC, uint8_t** buffer, uint16_t* len);
  ErrorCode_t (*SetCommFeature) (USBD_HANDLE_T hCDC, uint16_t feature, uint8_t* buffer, uint16_t len);
  ErrorCode_t (*GetCommFeature) (USBD_HANDLE_T hCDC, uint16_t feature, uint8_t** pBuffer, uint16_t* len);
  ErrorCode_t (*ClrCommFeature) (USBD_HANDLE_T hCDC, uint16_t feature);
  ErrorCode_t (*SetCtrlLineState) (USBD_HANDLE_T hCDC, uint16_t state);
  ErrorCode_t (*SendBreak) (USBD_HANDLE_T hCDC, uint16_t state);
  ErrorCode_t (*SetLineCode) (USBD_HANDLE_T hCDC, CDC_LINE_CODING* line_coding);

  // virtual functions
  ErrorCode_t (*CIC_GetRequest)( USBD_HANDLE_T hHid, USB_SETUP_PACKET* pSetup, uint8_t** pBuffer, uint16_t* length); 
  ErrorCode_t (*CIC_SetRequest)( USBD_HANDLE_T hCdc, USB_SETUP_PACKET* pSetup, uint8_t** pBuffer, uint16_t length);
#endif
  
// ******************************************************************************************************
// EVENTS Functions
// ******************************************************************************************************


#if defined( CONF_USE_ROMAPI_LPCUSB ) &&( CONF_USE_ROMAPI_LPCUSB == 1)
// ------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
void EVENT_UsbdCdc_SetLineCode(CDC_LINE_CODING *line_coding)
{
	// Serial_Init(VirtualSerial_CDC_Interface.State.LineEncoding.BaudRateBPS, false);
}
	#include "3rd_STACK\USBD\cdc_vcom.c"												// load API ext. functions, declarations,...
	#include "3rd_STACK\USBD\cdc_desc.c"

#endif










#if defined( CONF_USE_LIB_LPCUSB ) &&( CONF_USE_LIB_LPCUSB == 1)

	// USB Descriptors - fill in :
//		"Application\App_USB_Descriptors.c"
#endif
*/





#endif	// ifdef __DRV_USBD_H_
