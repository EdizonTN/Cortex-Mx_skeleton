// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_PWM.c
// 	   Version: 1.22
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for PWM
// ******************************************************************************
// Info: PWM driver. 
//
// Notice:
//
// Usage:
//			
// ToDo:
//
// Changelog:
//				2024.09.30	- v1.22	- add sys_NULL_Function instead a NULL pointer to a function in API structure
//				2023.12.10	- v1.21 - rename _CHIP_ADCx_IRQ_Handler to _Chip_ADCx_IRQ_Handler
//				2022.11.30	- v1.2 - change system of load
//				2022.11.28	- changed interrupt handler bottom 

#include "Skeleton.h"

#ifdef __DRV_PWM_H_																	// Header was loaded?


// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool PWM_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool PWM_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool PWM_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool PWM_Write_Data( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, void *pbuff, uint32_t BuffByteLen);
void PWM_Events( struct _drv_If *pDrvIf, _EventType_t EventType);
void PWM_Handler_Top(_drv_If_t *pDrvIf);											// quick handler
//void PWM_Handler_Bottom(_drv_If_t *pDrvIf);											// process handler

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************
// structure and assign to standard API functions:
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_PWM_API_STD 		= 
{
 	.Init			=	PWM_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	PWM_Enable,
	.Disable		=	PWM_Disable,
	.Write_Data		=	PWM_Write_Data,
	.Read_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Events			=	PWM_Events,
};
#pragma pop

// ******************************************************************************************************
// Private variables
// ******************************************************************************************************
union parm_t
{
	uint8_t	 _char[4];
	uint16_t _short[2];
	uint32_t _long;
};

#if defined( _CHIP_HAVE_PWM ) && (_CHIP_HAVE_PWM == 1)								// ak ma chip PWM periferoiu		

const _drv_If_t *PWM_DriverIRQ[_CHIP_PWM_COUNT];													// PWM0 - PWM3

// ------------------------------------------------------------------------------------------------------
// PWM INTERRUPT HANDLER
// ------------------------------------------------------------------------------------------------------

#if defined(_CHIP_PWM_COUNT) && (_CHIP_PWM_COUNT > 0)
void _Chip_PWM0_IRQ_Handler(void)	
{
	PWM_Handler_Top((void *) PWM_DriverIRQ[0]);
	//DMA_Handler_Bottom((void *) PWM_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) PWM_DriverIRQ[0]);
}
#endif

#if defined(_CHIP_PWM_COUNT) && (_CHIP_PWM_COUNT > 1)
void _Chip_PWM1_IRQ_Handler(void)	
{
	PWM_Handler_Top((void *) PWM_DriverIRQ[1]);
	//DMA_Handler_Bottom((void *) PWM_DriverIRQ[1]);
	IRQ_Handler_Bottom((void *) PWM_DriverIRQ[1]);
}
#endif

#if defined(_CHIP_PWM_COUNT) && (_CHIP_PWM_COUNT > 2)
void _Chip_PWM2_IRQ_Handler(void)	
{
	PWM_Handler_Top((void *) PWM_DriverIRQ[2]);
	//DMA_Handler_Bottom((void *) PWM_DriverIRQ[2]);
	IRQ_Handler_Bottom((void *) PWM_DriverIRQ[2]);
}
#endif

#if defined(_CHIP_PWM_COUNT) && (_CHIP_PWM_COUNT > 3)
void _Chip_PWM3_IRQ_Handler(void)	
{
	PWM_Handler_Top((void *) PWM_DriverIRQ[0]);
	//DMA_Handler_Bottom((void *) PWM_DriverIRQ[3]);
	IRQ_Handler_Bottom((void *) PWM_DriverIRQ[3]);
}
#endif



// ------------------------------------------------------------------------------------------------------
// vrat hodnotu prescalera podla zadanej frekvencie, pricom hodnota nesmie byt vacsia ako je MaxValueSizeInBytes [2/4]
// UpDn priznak, ze pocitame pocitadlom hore aj dole, takze frekv * 2
// ------------------------------------------------------------------------------------------------------
uint8_t PWM_Get_Pescaler_Value(drv_PWM_If_Spec_t *PWM_If_Spec, uint32_t Freq, bool UpDn)
{
//	uint_fast64_t new_val0; 
//	uint_fast32_t max_Val; 
//	uint8_t Prescaler = 0;
//	uint8_t dev;
//	
//	if (UpDn) dev = 2;
//	else dev = 1;
//	
//	// ToDo: Prepis na CHAL !!
//	if (((_CHIP_PWM_T*)PWM_If_Spec->pPeri)->CONFIG & PWM_CONFIG_32BIT_COUNTER) max_Val = 0xffffffff;	// vygeneruj maximalnu hodnoty, ktoru moze mat new_val
//	else max_Val = 0x0000ffff;
//	// ------------------------
//	
//	// ziadali nas o oba registre alebo len H ?
//	//if((AccessType == 32_bit) | ( AccessType == Counter_H ))
//	//if(!(PWM_If_Spec->uPeri.pPWM->CONFIG | PWM_CONFIG_32BIT_COUNTER))				// bezime ako 2x16-bit Counter? Ak ano, pracujeme aj s H counterom. Inak len s L
//	{
//		// teraz prepocitam najvhodnejsiu hodnotu pre delicku na zaklade zadanej frekvencie
//		//Prescaler.High16 = 0;

//		do
//		{
//			//Prescaler.High16 ++;													// ak budeme mimo, zvys deliaci pomer
//			Prescaler ++;															// zaciname jednotkou neee ?
//			new_val0 = Chip_Clock_GetSystemClockRate();								// daj cip freq
//			//new_val0 = new_val0 / Prescaler.High16 / Freq;						// pokus sa vypocitat hodnotu 
//			new_val0 = new_val0 / Prescaler / Freq / dev;									// pokus sa vypocitat hodnotu 
//			//} while((new_val0 > max_Val) & (Prescaler.High16 < 0x0100)); 			// opakuj pokial hodnota pre counter nie je zmysluplna
//		} while((new_val0 > max_Val) & (Prescaler < 0x80));; 						// opakuj pokial hodnota pre counter nie je zmysluplna a prescaler rmusi byt mensi ako 7f (uklada sa s -1)

//		//Prescaler.High16 --;
//	}

////	// Pri 32b sa L-kovy nacitava vzdy, inak vrat na poziadanie
////	if((AccessType == 32_bit) | ( AccessType == Counter_L ))
////	{
////		Prescaler.Low16 = 1;
////		// teraz prepocitam najvhodnejsiu hodnotu pre delicku na zaklade zadanej frekvencie
////		do	
////		{
////			Prescaler.Low16 ++;														// ak budeme mimo, zvys deliaci pomer
////			new_val0 = Chip_Clock_GetSystemClockRate();
////			new_val0 = new_val0 / Prescaler.Low16 / Freq ;							// pokus sa vypocitat hodnotu pre L counter
////		} while((new_val0 > max_Val) & (Prescaler.Low16 < 0x0100)); 				// opakuj pokial hodnota pre counter nie je zmysluplna
////		Prescaler.Low16 --;
////	}
//	//return(Prescaler.Unified32);													// vrat hodnotu primo zapisovatelnu do CTRL registra
//	return(Prescaler);																// vrat hodnotu prescalera
	return(0);
}


// ------------------------------------------------------------------------------------------------------
// Prepocita zadanu frekvenciu podla zadaneho Prescalera
// vystupom je reload hodnota pre MATCH registre 
// ------------------------------------------------------------------------------------------------------
uint32_t PWM_Get_MATCH_Value(uint32_t Freq, uint8_t Prescaler, bool BiDir)
{
	uint_fast64_t MainFreq;
	uint32_t	Result;
	uint8_t div=1;
	
	
	if(BiDir) div = 2;																// pocitame hore aj dole
	
	MainFreq = Chip_Clock_GetSystemClockRate(); 
	Result = MainFreq / Prescaler / Freq / div;											// vypocitaj hodnotu podla prescalwera

	return (Result);																// vrat vysledok
}
#endif

// ------------------------------------------------------------------------------------------------------
// Generic
// Write data PWM - write desired frequency [Hz] in format:
// When BuffByteLen == 8:
// Buff[0-3] : uint32_t value for U counter
// Buff[4-7] : uint32_t value  - destination PWM channel
// When BuffByteLen == 12:
// Buff[0-3] : uint32_t value for H counter
// Buff[4-7] : uint32_t value for L counter
// Buff[8-11] : uint32_t value  - destination PWM channel
// podla zvoleneho modu pocitadla BiDir, nasobi frekvenciu
// ------------------------------------------------------------------------------------------------------
bool PWM_Write_Data( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, void *pbuff, uint32_t BuffByteLen)
{
//	bool res = false;	
//	
//#if defined( _CHIP_HAVE_PWM ) && (_CHIP_HAVE_PWM == 1)								// ak ma chip PWM periferoiu	
//	union parm_t parm1, parm2, parm3;											
//	uint8_t prescaler;
//	uint32_t NewMatch;
//	bool UpDn = false;
//	
//	drv_PWM_If_Spec_t* pDrvIf_Spec = (drv_PWM_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

//#if defined(CONF_DEBUG_DRV_PWM) && (CONF_DEBUG_DRV_PWM == 1)
//	dbgprint("\r\n("DRV_PWM_OBJECTNAME") ID[%s]: Write Data", pDrvIf->Name);
//#endif		

//	bool SaveEnable = pDrvIf->Sys.Stat.Enabled;										// save old state
//	res = drvmgr_Disable(pRequestorModIf,pDrvIf, NULL);
//	
//	if(((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->CTRL_U & (1 << 4)) UpDn = true;	// pocitadlu hore aj dole
//	prescaler = PWM_Get_Pescaler_Value(pDrvIf_Spec, parm1._long, UpDn);
//	if (prescaler > 0) res = true;
//	
//	// Write data here:
//	if(res == true)
//	{
//		switch (BuffByteLen)
//		{
//			case 8:
//					parm1._long = ((uint32_t*) pbuff)[0];							// load parameter match value.U into union
//					parm3._long = ((uint32_t*) pbuff)[1];							// load parameter dest. channel into union
//					
//					if(((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->CTRL_U & (1 << 4)) UpDn = true;	// pocitadlu hore aj dole
//						
//					prescaler = PWM_Get_Pescaler_Value(pDrvIf_Spec, parm1._long, UpDn);
//					if (prescaler == 0) {res = false; break;}
//					NewMatch = PWM_Get_MATCH_Value(parm1._long, prescaler, UpDn);			// calculate new MATCH value from frequency and PWM prescaler
//					if(NewMatch > 0)
//					{
//						// ToDo: Prepis na CHAL !!!
//						uint32_t OldHalt =  (((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->CTRL_U) & (PWM_CTRL_HALT_L | PWM_CTRL_HALT_H);	// read Halt flags
//						((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->CTRL_U |= (PWM_CTRL_HALT_L | PWM_CTRL_HALT_H);						// stop counting !
//						((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->MATCH[parm3._long].U = NewMatch;
//						((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->MATCHREL[parm3._long].U = NewMatch;
//						((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->CTRL_U |= PWM_CTRL_CLRCTR_L | PWM_CTRL_CLRCTR_H;					// clear counters,
//						((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->CTRL_U &= ~(OldHalt & (PWM_CTRL_HALT_L | PWM_CTRL_HALT_H));			// write old Halt flags
//						// --------------------------

//					}else res = false;
//					break;
//			case 12:
//					parm1._long = ((uint32_t*) pbuff)[0];							// load parameter match value.H into union
//					parm2._long = ((uint32_t*) pbuff)[1];							// load parameter match value.L into union
//					parm3._long = ((uint32_t*) pbuff)[2];							// load parameter dest. channel into union
//				
//					if(((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->CTRL_U & (1 << 4)) UpDn = true;	// pocitadlu hore aj dole
//					prescaler = PWM_Get_Pescaler_Value(pDrvIf_Spec, parm1._long, UpDn);
//			
//					if (prescaler == 0) {res = false; break;}				
//					
//					NewMatch = PWM_Get_MATCH_Value(parm1._short[parm3._long], prescaler, UpDn);		// calculate new MATCH High value from frequency and PWM prescaler
//					if(NewMatch > 0)
//					{
//						//ToDo: Prepis na CHAL!!!
//						uint32_t OldHalt =  (((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->CTRL_U) & (PWM_CTRL_HALT_L | PWM_CTRL_HALT_H);	// read Halt flags
//						((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->CTRL_U |= (PWM_CTRL_HALT_L | PWM_CTRL_HALT_H);						// stop counting !
//						((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->MATCH[parm3._long].H = NewMatch;
//						((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->MATCHREL[parm3._long].H = NewMatch;
//						((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->CTRL_U |= PWM_CTRL_CLRCTR_L | PWM_CTRL_CLRCTR_H;					// clear counters,
//						((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->CTRL_U &= ~(OldHalt & (PWM_CTRL_HALT_L | PWM_CTRL_HALT_H));			// write old Halt flags
//						// -----------------------
//					} else res = false;	

//					
//					if(((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->CTRL_L & (1 << 4)) UpDn = true;	// pocitadlu hore aj dole
//					else UpDn = false;
//					
//					prescaler = PWM_Get_Pescaler_Value(pDrvIf_Spec, parm2._long, UpDn);
//					if (prescaler > 0) {res = false; break;}					
//					NewMatch = PWM_Get_MATCH_Value(parm2._short[1], prescaler, UpDn);		// calculate new MATCH Low value from frequency and PWM prescaler
//					if(NewMatch > 0)
//					{
//						//ToDo: Prepis na CHAL !!
//						uint32_t OldHalt =  (((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->CTRL_U) & (PWM_CTRL_HALT_L | PWM_CTRL_HALT_H);	// read Halt flags
//						((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->CTRL_U |= (PWM_CTRL_HALT_L | PWM_CTRL_HALT_H);						// stop counting !
//						((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->MATCH[parm3._long].L = NewMatch;
//						((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->MATCHREL[parm3._long].L = NewMatch;
//						((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->CTRL_U |= PWM_CTRL_CLRCTR_L | PWM_CTRL_CLRCTR_H;					// clear counters,
//						((_CHIP_PWM_T*)pDrvIf_Spec->pPeri)->CTRL_U &= ~(OldHalt & (PWM_CTRL_HALT_L | PWM_CTRL_HALT_H));			// write old Halt flags
//						// ---------------------------
//					}else res = false;
//					break;
//			default: 
//					res = false; 
//					break;											// wrong parameter format
//		}
//	}
//	if(SaveEnable == true) drvmgr_Enable(pRequestorModIf,pDrvIf, NULL);				// restore state
//	
//	// ----------------------------

//#if defined(CONF_DEBUG_DRV_PWM) && (CONF_DEBUG_DRV_PWM == 1)
//	dbgprint("\r\n("DRV_PWM_OBJECTNAME") ID[%s]: Write Data Done.", pDrvIf->Name);
//#endif		
//#endif			// if ( _CHIP_HAVE_PWM ) && (_CHIP_HAVE_PWM == 1)								// ak ma chip PWM periferoiu
//	return(res);
	return(false);
}

// ------------------------------------------------------------------------------------------------------
// Generic
// Inicializacia PWM-u
// ------------------------------------------------------------------------------------------------------
bool PWM_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;
	drv_PWM_If_Spec_t* pDrvIf_Spec = (drv_PWM_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_PWM) && (CONF_DEBUG_DRV_PWM == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif


//	// initialization code here:
	pDrvIf_Spec->pPeri = CHAL_PWM_Init(pDrvIf_Spec->PeriIndex);
	if(pDrvIf_Spec->pPeri != NULL) res = true;
//	
//	// IO init
//	// Teraz fyzicky prepoj PWM OUT na GPIO Pin ::::::::::::::
//	if(pDrvIf_Spec->pSignal_Out != NULL)
//	{
//		res = CHAL_Signal_Init(pDrvIf_Spec->pSignal_Out);
//	}
//	
#if defined (CONF_USE_IRQ_MANAGER) && (CONF_USE_IRQ_MANAGER == 1) && defined(PWM_USE_IRQ_MANAGER) && (PWM_USE_IRQ_MANAGER == 1)
	if(res == true) res = irqmgr_isr_Handler_Install(pDrvIf->IRQ_VecNum, (void (*)(void *Parm))pDrvIf->pIRQ_USR_HandlerTop, (void (*)(void *Parm))pDrvIf->pIRQ_USR_HandlerBottom, pDrvIf->IRQ_Priority, (void *)pDrvIf);
#else
	PWM_DriverIRQ[pDrvIf_Spec->PeriIndex] = pDrvIf;
#endif	
	
	//res = CHAL_PWM_IRQ_NVIC_Enable(pDrvIf_Spec->pPeri, true);								// enable RX Interrupt

	pDrvIf->Sys.Stat.Initialized = res;													// if enabled is correct, return true
	res = true;

#if defined(CONF_DEBUG_DRV_PWM) && (CONF_DEBUG_DRV_PWM == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif		
//#endif 		// if defined( _CHIP_HAVE_PWM ) && (_CHIP_HAVE_PWM == 1)
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// Generic
// ENABLE function
// If ChannelBitMask == UINT32_MAX, enable/disable whole PWM
// channel in PWM is EVENT.
// ------------------------------------------------------------------------------------------------------
bool PWM_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
//#if defined( _CHIP_HAVE_PWM ) && (_CHIP_HAVE_PWM == 1)								// ak ma chip PWM periferoiu		
//	drv_PWM_If_Spec_t* pDrvIf_Spec = (drv_PWM_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

//#if defined(CONF_DEBUG_DRV_PWM) && (CONF_DEBUG_DRV_PWM == 1)
//	dbgprint("\r\n("DRV_PWM_OBJECTNAME") ID[%s]: Set Enable CH:0x%08x", pDrvIf->Name, ChannelBitMask);
//#endif

//	// main code here:
//	if(ChannelBitMask == UINT32_MAX)
//	{
//		res = CHAL_PWM_Enable(pDrvIf_Spec->pPeri, true); 								// enable PWM
//	}
//	else
//	{
//		res = CHAL_PWM_Enable(pDrvIf_Spec->pPeri, true); 								// enable PWM, each event cannot control...
//	}
	pDrvIf->Sys.Stat.Enabled = true;
	res = true;
//	
//#if defined(CONF_DEBUG_DRV_PWM) && (CONF_DEBUG_DRV_PWM == 1)
//    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
//	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
//#endif	
//#endif		// if defined( _CHIP_HAVE_PWM ) && (_CHIP_HAVE_PWM == 1)
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// Generic
// DISABLE
// If ChannelBitMask == UINT32_MAX, enable/disable whole PWM
// channel in PWM is EVENT.
// ------------------------------------------------------------------------------------------------------
bool PWM_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
//#if defined( _CHIP_HAVE_PWM ) && (_CHIP_HAVE_PWM == 1)								// ak ma chip PWM periferoiu		
//	drv_PWM_If_Spec_t* pDrvIf_Spec = (drv_PWM_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

//#if defined(CONF_DEBUG_DRV_PWM) && (CONF_DEBUG_DRV_PWM == 1)
//	dbgprint("\r\n("DRV_PWM_OBJECTNAME") ID[%s]: Set Disable CH:0x%08x", pDrvIf->Name, ChannelBitMask);
//#endif

//	// main code here:
//	if(ChannelBitMask == UINT32_MAX)
//	{
//		res = CHAL_PWM_Enable(pDrvIf_Spec->pPeri, false); 								// enable PWM
//	}
//	else
//	{
//		res = CHAL_PWM_Enable(pDrvIf_Spec->pPeri, false); 								// enable PWM, each event cannot control...
//	}
	pDrvIf->Sys.Stat.Enabled = false;
	res = true;
//#if defined(CONF_DEBUG_DRV_PWM) && (CONF_DEBUG_DRV_PWM == 1)
//    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
//	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
//#endif	
//#endif 			// if defined( _CHIP_HAVE_PWM ) && (_CHIP_HAVE_PWM == 1)	
	return(res);
}


// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************
#if defined( _CHIP_HAVE_PWM ) && (_CHIP_HAVE_PWM == 1)								// ak ma chip PWM periferoiu		
// ------------------------------------------------------------------------------------------------------
// Quick handler
// ------------------------------------------------------------------------------------------------------
inline void PWM_Handler_Top(_drv_If_t *pDrvIf)
{
	if(pDrvIf == NULL) return;

	if(pDrvIf->pIRQ_USR_HandlerTop) pDrvIf->pIRQ_USR_HandlerTop(pDrvIf);					// override with USR handler
	else
	{
		if(pDrvIf->Sys.Stat.Initialized == false) return;
		if(pDrvIf->Sys.Stat.Loaded == false) return;

		// Generic interrupt routine:
	}
	
}

//// ------------------------------------------------------------------------------------------------------
//// After handler
//// fire event system
//// ------------------------------------------------------------------------------------------------------
//inline void PWM_Handler_Bottom(_drv_If_t *pDrvIf)
//{
//	if(pDrvIf->pIRQ_USR_HandlerBottom) pDrvIf->pIRQ_USR_HandlerBottom(pDrvIf);				// override with USR handler
//	else
//	{
//		if(pDrvIf->Sys.Stat.Initialized == false) return;
//		if(pDrvIf->Sys.Stat.Loaded == false) return;
//		
//		// Generic interrupt routine:
//		if((pDrvIf->pAPI_USR) && (pDrvIf->pAPI_USR->Events)) pDrvIf->pAPI_USR->Events( pDrvIf, EV_StateChanged);	// if is set USR event system, call it
//		else
//		{
//			if(pDrvIf->pAPI_STD->Events) pDrvIf->pAPI_STD->Events( pDrvIf, EV_StateChanged);	// if is set STD event system, call it
//		}
//	}
//}
#endif		// if defined( _CHIP_HAVE_PWM ) && (_CHIP_HAVE_PWM == 1)

// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************
// ------------------------------------------------------------------------------------------------
// Event system - volane v preruseni !!!
// ------------------------------------------------------------------------------------------------------
inline void PWM_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{

//	switch(EventType)
//	{
//		case EV_DataReceived: break;
//		case EV_TransmitReady: break;
//		case EV_TransmitDone: break;	
//		case EV_StateChanged: 
//		{
//			void* CallBack;		
//	
//			_ColListRecord_t *tmpListRecord = NULL;
//			do
//			{
//				tmpListRecord = sys_DynCol_Get_Record(&pDrvIf->Sys.ParentModLinked, tmpListRecord);	// temporary pointer to parent module list
//				if (tmpListRecord == NULL) break;
//				
//				CallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_ChangedState;
//				if(CallBack)		// callback is set?
//				{
//					sys_CreateTask("PWM_Changed", CallBack, (sys_TaskArgs_t) {NULL, 0});// create task for data read
//				}
//			}while(tmpListRecord);
//			
//			break;
//		}
//		case EV_NoChange: break;
//		default: break;
//	}
}

#endif	// ifdef __DRV_PWM_H_
