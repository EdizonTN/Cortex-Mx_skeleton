// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_SPI.c
// 	   Version: 4.23
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for SPI
// ******************************************************************************
// Info: SPI driver. Support Mode 0, 1, 2 and 3 (based on used MCU!)
//
// Notice:
//
// Usage:	SPI Single channel mde
//				Flow: (SSEL) [OpCode] [Adr] [Data] (!SSEL)
//			Opcode size: 8,16,24,32 bits, Adress size: 0,8,16,24,32 bits, Data size: 0-n bits (and array transfer)
// 			One xfer - between SSEL activate and deactivate

//			
// ToDo:	
//			- SPI init withou SCK, MOSI and MISO pins are failed !!
//			- Doesn't MICROWIRE compatible - https://www.ti.com/lit/an/snoa743/snoa743.pdf
//			- Doesn't support SafeSPI - http://safespi.org/
//			- Doesn't support Dual/Quad SPI
//			- No DMA Support
//
// Changelog:
//				2024.09.30	- v4.23	- add sys_NULL_Function instead a NULL pointer to a function in API structure
//				2024.02.02	- v4.22 - modify to new sys_buffer_create parameter
//				2023.12.10	- v4.21 - rename _CHIP_ADCx_IRQ_Handler to _Chip_ADCx_IRQ_Handler
//				2022.11.30	- v4.2 - change system of load
//				2022.11.28	- changed interrupt handler bottom
//				2021.06.16	- Rewrite cmplette driver
//				2020.07.01	- rename Old: CHAL_xxx_IRQ_Enable   ----->  CHAL_xxx_IRQ_NVIC_Enable
// 				2019.11.11	- Read/write - pbuff changed to sys_Buffer_t

#include "Skeleton.h"

#ifdef __DRV_SPI_H_																	// Header was loaded?



// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool SPI_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool SPI_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool SPI_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool SPI_Write_Data (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t WrLen, _XFerType_t XferType);
uint32_t SPI_Read_Data (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t RdLen, _XFerType_t XferType);
void SPI_Events( struct _drv_If *pDrvIf, _EventType_t EventType);
void SPI_Handler_Top(_drv_If_t *pDrvIf);											// Quick Handler
//void SPI_Handler_Bottom(_drv_If_t *pDrvIf);											// Process Handler


// ******************************************************************************************************
// Public Function - specific for this driver
// ******************************************************************************************************
bool SPI_CreateDevice(struct _drv_If *pDrvIf, SPI_Device_t* pDevice, uint16_t SSELID);
bool SPI_DestroyDevice(struct _drv_If *pDrvIf, SPI_Device_t *pDevice);
bool SPI_Set_BusSpeed(struct _drv_If *pDrvIf, uint32_t NewSpeed_kHz);


// ******************************************************************************************************
// Private
// ******************************************************************************************************
bool SPI_WaitForSetStat (drv_SPI_If_Spec_t *pDrvIf_Spec, uint32_t WaitFor_State, uint32_t NumOfTimeOutCLKPulses);

const drv_SPI_Ext_API_t	SPI_Ext_API_STD 	=  										// Extended (non-standard) API's for this driver
{
	.Set_BusSpeed		= SPI_Set_BusSpeed,											// change transfer speed
	.Create_Device		= SPI_CreateDevice,											// create device by SSELID
	.Destroy_Device		= SPI_DestroyDevice											// destroy device by ptr
};

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************
// structure and assign to standard API functions:
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_SPI_API_STD = 
{
 	.Init			=	SPI_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	SPI_Enable,
	.Disable		=	SPI_Disable,
	.Write_Data		=	SPI_Write_Data,
	.Read_Data		=	SPI_Read_Data,
	.Events			=	SPI_Events,
	.Ext			=	(drv_SPI_Ext_API_t*) &SPI_Ext_API_STD
};
#pragma pop

const _drv_If_t *SPI_DriverIRQ[_CHIP_SPI_COUNT];													// SPI0
// ------------------------------------------------------------------------------------------------------
// SPI INTERRUPT HANDLER
// ------------------------------------------------------------------------------------------------------

#if defined(_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
void _Chip_SPI0_IRQ_Handler(void)	
{
	SPI_Handler_Top((void *) SPI_DriverIRQ[0]);
	//SPI_Handler_Bottom((void *) SPI_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) SPI_DriverIRQ[0]);
}
#endif

#if defined(_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 1)
void _Chip_SPI1_IRQ_Handler(void)	
{
	SPI_Handler_Top((void *) SPI_DriverIRQ[1]);
	//SPI_Handler_Bottom((void *) SPI_DriverIRQ[1]);
	IRQ_Handler_Bottom((void *) SPI_DriverIRQ[1]);
}
#endif

#if defined(_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 2)
void _Chip_SPI2_IRQ_Handler(void)	
{
	SPI_Handler_Top((void *) SPI_DriverIRQ[2]);
	//SPI_Handler_Bottom((void *) SPI_DriverIRQ[2]);
	IRQ_Handler_Bottom((void *) SPI_DriverIRQ[2]);
}
#endif

#if defined(_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 3)
void _Chip_SPI3_IRQ_Handler(void)	
{
	SPI_Handler_Top((void *) SPI_DriverIRQ[3]);
	//SPI_Handler_Bottom((void *) SPI_DriverIRQ[3]);
	IRQ_Handler_Bottom((void *) SPI_DriverIRQ[3]);
}
#endif

#if defined(_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 4)
void _Chip_SPI4_IRQ_Handler(void)	
{
	SPI_Handler_Top((void *) SPI_DriverIRQ[4]);
	//SPI_Handler_Bottom((void *) SPI_DriverIRQ[4]);
	IRQ_Handler_Bottom((void *) SPI_DriverIRQ[4]);
}
#endif

#if defined(_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 5)
void _Chip_SPI5_IRQ_Handler(void)	
{
	SPI_Handler_Top((void *) SPI_DriverIRQ[5]);
	//SPI_Handler_Bottom((void *) SPI_DriverIRQ[5]);
	IRQ_Handler_Bottom((void *) SPI_DriverIRQ[5]);
}
#endif

#if defined(_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 6)
void _Chip_SPI6_IRQ_Handler(void)	
{
	SPI_Handler_Top((void *) SPI_DriverIRQ[6]);
	//SPI_Handler_Bottom((void *) SPI_DriverIRQ[6]);
	IRQ_Handler_Bottom((void *) SPI_DriverIRQ[6]);
}
#endif

#if defined(_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 7)
void _Chip_SPI7_IRQ_Handler(void)	
{
	SPI_Handler_Top((void *) SPI_DriverIRQ[7]);
	//SPI_Handler_Bottom((void *) SPI_DriverIRQ[7]);
	IRQ_Handler_Bottom((void *) SPI_DriverIRQ[7]);
}
#endif

#if defined(_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 8)
void _Chip_SPI8_IRQ_Handler(void)	
{
	SPI_Handler_Top((void *) SPI_DriverIRQ[8]);
	//SPI_Handler_Bottom((void *) SPI_DriverIRQ[8]);
	IRQ_Handler_Bottom((void *) SPI_DriverIRQ[8]);
}
#endif

#if defined(_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 9)
void _Chip_SPI9_IRQ_Handler(void)	
{
	SPI_Handler_Top((void *) SPI_DriverIRQ[9]);
	//SPI_Handler_Bottom((void *) SPI_DriverIRQ[9]);
	IRQ_Handler_Bottom((void *) SPI_DriverIRQ[9]);
}
#endif

// ------------------------------------------------------------------------------------------------------
// Generic
// Inicializacia SPI-u
bool SPI_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;
	uint8_t index = 0;

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	SYS_ASSERT( pDrvIf != NULL);													// check
#endif
	
	drv_SPI_If_Spec_t* pDrvIf_Spec = (drv_SPI_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> Peri: 0x%.8x, ParentMod: %s", pDrvIf_Spec->pPeri, pRequestorModIf->Name);
#endif		

	// initialization code here:
	if(pDrvIf_Spec->pSCK) res |= CHAL_Signal_Init(pDrvIf_Spec->pSCK);				// result set to true
	else { res = false; goto exit_SPI_Init; }										// general failure, pin does not exist!
	
	if(pDrvIf_Spec->pMOSI) res &= CHAL_Signal_Init(pDrvIf_Spec->pMOSI);				// and true..
	else { res = false; goto exit_SPI_Init; }										// general failure, pin does not exist!
		
	if(pDrvIf_Spec->pMISO) res &= CHAL_Signal_Init(pDrvIf_Spec->pMISO);				// and true..
	else { res = false; goto exit_SPI_Init; }										// general failure, pin does not exist!
	
	//sys_memset_zero(&pDrvIf_Spec->BusStatus, sizeof(pDrvIf_Spec->BusStatus));				// Clear status structure
	
	while((pDrvIf_Spec->pSSEL[index] != NULL) && (pDrvIf_Spec->pSSEL != NULL))		// cycle all SSEL
	{
		res &= CHAL_Signal_Init(pDrvIf_Spec->pSSEL[index ++]);						// and true..
	}	
	
	if(res == true) pDrvIf_Spec->pPeri = CHAL_SPI_Init(pDrvIf_Spec->PeriIndex);
	if(pDrvIf_Spec->pPeri) res = true;
	else { res = false; goto exit_SPI_Init; }										// pPeri deos not set !

	if(res == true)
	{
		res = CHAL_SPI_Configure (pDrvIf_Spec->pPeri, pDrvIf_Spec->Controller.Speed_Hz, pDrvIf_Spec->Controller.Rel_Mode, CONF_DRV_SPI_CPHA, CONF_DRV_SPI_CPOL);
	}
	

	// try to find FIFO's maximums:
#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)
	CHAL_SPIFIFO_Set_TxTHLevel(pDrvIf_Spec->pPeri, UINT32_MAX);						// Set threshold to max word
	pDrvIf_Spec->Controller.TxFIFO_Max = CHAL_SPIFIFO_Get_TxTHLevel(pDrvIf_Spec->pPeri);	// Read back maximum level of Tx Fifo which periphery is able to set

	CHAL_SPIFIFO_Set_RxTHLevel(pDrvIf_Spec->pPeri, UINT32_MAX);						// Set threshold to max
	pDrvIf_Spec->Controller.RxFIFO_Max = CHAL_SPIFIFO_Get_RxTHLevel(pDrvIf_Spec->pPeri); 	// Read back maximum level of Rx Fifo which periphery is able to set
	CHAL_SPIFIFO_Set_RxTHLevel(pDrvIf_Spec->pPeri, 1);					 			// Set threshold to 1 element
#endif


#if defined (CONF_USE_IRQ_MANAGER) && (CONF_USE_IRQ_MANAGER == 1) && defined(UART_USE_IRQ_MANAGER) && (UART_USE_IRQ_MANAGER == 1)
	if(res == true) res = irqmgr_isr_Handler_Install(pDrvIf->IRQ_VecNum, (void (*)(void *Parm))pDrvIf->pIRQ_USR_HandlerTop, (void (*)(void *Parm))pDrvIf->pIRQ_USR_HandlerBottom, pDrvIf->IRQ_Priority, (void *)pDrvIf);
#else
	SPI_DriverIRQ[pDrvIf_Spec->PeriIndex] = pDrvIf;									// set parameter for interrupt routine
#endif		
	

	
	pDrvIf_Spec->XFer_Rec.pTxBuff = NULL;
	pDrvIf_Spec->XFer_Rec.pRxBuff = NULL;
	pDrvIf_Spec->XFer_Rec.TxDataEstimated = 0;
	pDrvIf_Spec->XFer_Rec.RxBuffSize = 0;
//	pDrvIf_Spec->XFer_Rec.TxOpCodeEstimated = 0;
//	pDrvIf_Spec->XFer_Rec.TxAddrEstimated = 0;
	pDrvIf_Spec->XFer_Rec.Dev_SSEL_ID = 0;
	pDrvIf_Spec->XFer_Rec.Status = CHAL_SPI_STAT_OK;
	pDrvIf_Spec->pXFer_Dev = NULL;													// deselect dest. device
	
	CHAL_SPI_Flush_Rx(pDrvIf_Spec->pPeri);											// Flush Rx
	
#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)					
	CHAL_SPIFIFO_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPIFIFO_IRQEN_RXDONE | CHAL_SPIFIFO_IRQEN_RXOV, true);	// enable FIFO RX Interrupt and RX Error interrupt
#else		
	CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQEN_RXDONE | CHAL_SPI_IRQEN_RXOV, true);	// enable RX Interrupt and RX Error interrupt
#endif	
	
//	if(res == true) 
//	{
//		pDrvIf_Spec->XFer_Rec.pRxBuff = sys_Buffer_Create(CONF_DRV_SPI_BUFFERSIZE, CONF_DRV_SPI_BUFFERELEMENTSIZE, sBuffRing);
//		if(pDrvIf_Spec->XFer_Rec.pRxBuff == NULL) res = false;						// buffer init fail
//		else res = pDrvIf_Spec->XFer_Rec.pRxBuff->Ready;							// store init flag
//	}
//	
//	if(res == true) 
//	{
//		pDrvIf_Spec->XFer_Rec.pTxBuff = sys_Buffer_Create(CONF_DRV_SPI_BUFFERSIZE, CONF_DRV_UART_BUFFERELEMENTSIZE, sBuffRing);
//		if(pDrvIf_Spec->XFer_Rec.pTxBuff == NULL) res = false;
//		else res = pDrvIf_Spec->XFer_Rec.pTxBuff->Ready;							// store init flag
//	}
		

//	if(res == false)																// rollback init
//	{
//		sys_Buffer_Destroy(pDrvIf_Spec->XFer_Rec.pTxBuff);							// clear Tx Buffer
//		sys_Buffer_Destroy(pDrvIf_Spec->XFer_Rec.pRxBuff);							// clear Rx Buffer
//	}
	
	pDrvIf->Sys.Stat.Initialized = res;												// Driver's flag update
	
exit_SPI_Init:
	
#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT("%s, Peri: 0x%.8x", FN_DEBUG_RES_BOOL(res), pDrvIf_Spec->pPeri);	// dbg print result boolean code
#endif		
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// Generic
// ENABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
bool SPI_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	SYS_ASSERT( pDrvIf != NULL);													// check
#endif
	
	drv_SPI_If_Spec_t* pDrvIf_Spec = (drv_SPI_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pPeri: 0x%.8x, chMask: 0x%.8x", pDrvIf_Spec->pPeri, ChannelBitMask);
#endif

	// main code here:
	
	//res = CHAL_SPI_Enable(pDrvIf_Spec->pPeri, false); 							// disable peri
	//CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, UINT32_MAX, false);						// disable all interrupt
	//CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQEN_SSA, true);				// enable SSEL Assert interrupt
	res = CHAL_SPI_Enable(pDrvIf_Spec->pPeri, true); 								// enable peri
	if(res) CHAL_SPI_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, true);						// enable peri in NVIC
	
	
	
//	if(pDrvIf_Spec->Controller.Rel_Mode == CHAL_SPI_Slave) 							// slave SPI mode
//	{
//		res = CHAL_SPISLV_Enable(pDrvIf_Spec->pPeri, true); 
//		if(res) res = CHAL_SPISLV_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, true); 		// enable SPI Slave IRQ
//	}
//	
//	if(pDrvIf_Spec->Controller.Rel_Mode == CHAL_SPI_Slave)							// Monitor SPI
//	{	
//		res = CHAL_SPIMST_Enable(pDrvIf_Spec->pPeri, true); 
//		if(res) res = CHAL_SPIMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, true); 		// enable SPI Master IRQ
//	}
	
	if(res == true) pDrvIf->Sys.Stat.Enabled = true;								// Enable was succesfull

#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// Generic
// DISABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
bool SPI_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	SYS_ASSERT( pDrvIf != NULL);													// check
#endif
	
	drv_SPI_If_Spec_t* pDrvIf_Spec = (drv_SPI_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pPeri: 0x%.8x, chMask: 0x%.8x", pDrvIf_Spec->pPeri, ChannelBitMask);
#endif

	// main code here:
	res = CHAL_SPI_Enable(pDrvIf_Spec->pPeri, false); 								// Disable
	//CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, ChannelBitMask, false);					// disable selected  channels
	if(res) CHAL_SPI_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);					// disable peri in NVIC
	if(res == true) pDrvIf->Sys.Stat.Enabled = false;								// disable was succesfull

#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// USR - Write data to SPI device connected to interface. Speed will be changed, if is out of tolerance CONF_DRV_SPI_SPEED_TOL
// XFer_Rec.DevDestAddr    MUST BE SET! Addres of memory area or register in the destination device.
// pDrvIf_Spec->XFerDev must be set as connected device structure! Xfer structure is updated during transfer.
// if pBuff is not set (NULL) CONF_DRV_SPI_DUMMYDATA data will send
// WrLen - required byte count for read. (One byte is DevDataSize bits!)
// return: true if transfer is started/done, otherwise false
bool SPI_Write_Data( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t WrLen, _XFerType_t XferType)
{
	bool res = false;

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	SYS_ASSERT( pDrvIf != NULL);													// check
#endif
	
	drv_SPI_If_Spec_t* pDrvIf_Spec = (drv_SPI_If_Spec_t*) pDrvIf->pDrvSpecific;

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	SYS_ASSERT( pDrvIf_Spec->pXFer_Dev != NULL);
#endif

	
#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pDev: %p, pSrc: %p, WriteLen: %d, XFerType: 0x%.1X", pDrvIf_Spec->pXFer_Dev, pbuff, WrLen, XferType);
#endif

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	if(pDrvIf == NULL) res = false;													// Bad pointers
	else res = true;
#endif
	
	if(pDrvIf_Spec->XFer_Rec.Status == CHAL_SPI_STAT_BUSY) res = false;				// SPI device is occupied by another process
	else 
	{
		if(pDrvIf_Spec->pXFer_Dev == NULL) res = false;								// target device not assigned!
		else res = true;															// all assigned.
	}
	
	if(res == true)
	{
		res = SPI_Set_BusSpeed(pDrvIf, pDrvIf_Spec->pXFer_Dev->MaxSpeed_kHz);		// Set Bus speed
	}

	if(res == true)
	{
		SYS_CPUCRIT_START();
		// 1. Initialize transfer
		// prepare for send: copy config from device struct to xfer structure.
		if(pbuff == NULL) pDrvIf_Spec->XFer_Rec.pTxBuff = pDrvIf_Spec->pXFer_Dev->pTxBuff; // Set xfrer buffer as device buffer
		else pDrvIf_Spec->XFer_Rec.pTxBuff = pbuff;									// Set destination buffer as user defined Input buffer
		pDrvIf_Spec->XFer_Rec.TxDataEstimated = WrLen;								// Number of write character
		pDrvIf_Spec->XFer_Rec.pRxBuff = NULL;										// empty
		pDrvIf_Spec->XFer_Rec.RxBuffSize = 0;										// nothing read		
		pDrvIf_Spec->XFer_Rec.DevDataBitSize = pDrvIf_Spec->pXFer_Dev->DevDataBitSize;// save data element bits size
		
		//Set activity H/L for SSEL
		pDrvIf_Spec->pPeri->CFG &= ~(SPI_CFG_SPOL0_MASK | SPI_CFG_SPOL1_MASK | SPI_CFG_SPOL2_MASK | SPI_CFG_SPOL3_MASK);
		pDrvIf_Spec->pPeri->CFG |= pDrvIf_Spec->pSSEL[pDrvIf_Spec->pXFer_Dev->Dev_SSEL_ID]->Std.Act << (8 + pDrvIf_Spec->pXFer_Dev->Dev_SSEL_ID);
		
		// 2. Write SSEL ID
		pDrvIf_Spec->XFer_Rec.Dev_SSEL_ID = pDrvIf_Spec->pXFer_Dev->Dev_SSEL_ID;	// save SSELID
		CHAL_SPI_Flush_Rx(pDrvIf_Spec->pPeri);										// Flush Rx
		CHAL_SPI_Flush_Tx(pDrvIf_Spec->pPeri);										// Flush Tx
		
		// 3. Start transfer
		pDrvIf_Spec->XFer_Rec.Status = CHAL_SPI_STAT_BUSY;							// Status to BUSY
		CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, UINT32_MAX, true);					// enable all interrupt
		CHAL_SPIFIFO_Enable_IRQ(pDrvIf_Spec->pPeri, UINT32_MAX, true);
		
#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)
		// Enable FIFO and set threshold level to zero - generate IRQ if TxEmpty
		CHAL_SPIFIFO_Set_TxTHLevel(pDrvIf_Spec->pPeri, 0); 							// Set threshold to empty TxFIFO, next threshold will be set in IRQ Routine
		CHAL_SPIFIFO_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPIFIFO_IRQEN_TXRDY | CHAL_SPIFIFO_IRQEN_RXDONE , true);	// enable TxRx interrupts
#else
		CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQEN_RXDONE | CHAL_SPI_IRQEN_TXRDY, true); // Enable TxRx
#endif
		CHAL_SPI_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, true);							// Enable SPI IRQ
		SYS_CPUCRIT_END();
	
		// wait for done on exit
		if(XferType == XFer_Blocking) 
		{
			res = SPI_WaitForSetStat( pDrvIf_Spec, CHAL_SPI_STAT_OK, CONF_DRV_SPI_TIMEOUTCLK_XFERBLOCK);			// wait for not BUSY and check timeout
#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
			if(res == false) FN_DEBUG_FMTPRINT(" >> Xfer timeout!")
#endif
		}																			// XFer was started, but not finishet yet.....
	}
	
#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT("%s, XFerCount: 0x%.4X", FN_DEBUG_RES_BOOL(res), WrLen - pDrvIf_Spec->XFer_Rec.TxDataEstimated );
#endif		
	return(res);
}


// ------------------------------------------------------------------------------------------------
// USR - Read data from SPI device connected to interface. Speed will be changed, if is out of tolerance CONF_DRV_SPI_SPEED_TOL
// pDrvIf_Spec->XFerDev and XFerRec must be set! Xfer structure is updated during transfer.
// if pRxBuff is not set (NULL) pBuff from device will use
// For send data will be used data from device pTxBuff. If not available, dummy will be used.
// RdLen - required byte count for read. (One byte is DevDataSize bits!)
// return: physically readed data count
uint32_t SPI_Read_Data(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t RdLen, _XFerType_t XferType)
{
	bool res = false;
	uint32_t Result = RdLen;
	
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	SYS_ASSERT( pDrvIf != NULL);													// check
#endif
	
	drv_SPI_If_Spec_t* pDrvIf_Spec = (drv_SPI_If_Spec_t*) pDrvIf->pDrvSpecific;
	
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	SYS_ASSERT( pDrvIf_Spec->pXFer_Dev != NULL);
#endif

	
#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pDev: %p, pDst: %p, ReqLen: %d, XFerType: 0x%.1X", pDrvIf_Spec->pXFer_Dev, pbuff, RdLen, XferType);
#endif	

	if(pDrvIf_Spec->XFer_Rec.Status == CHAL_SPI_STAT_BUSY) res = false;				// SPI device is occupied by another process
	else 
	{
		if(pDrvIf_Spec->pXFer_Dev == NULL) res = false;								// target device not assigned!
		else res = true;															// all assigned.
	}
	
	if(res == true)
	{
		res = SPI_Set_BusSpeed(pDrvIf, pDrvIf_Spec->pXFer_Dev->MaxSpeed_kHz);		// Set Bus speed
	}
	
	if(res == true)																	// input parameters looks OK
	{
		SYS_CPUCRIT_START();
		// 1. Initialize transfer
		// prepare for send: copy config from device struct to xfer structure.
		pDrvIf_Spec->XFer_Rec.RxBuffSize = RdLen;									// readed bytes
		if(pbuff == NULL) pDrvIf_Spec->XFer_Rec.pRxBuff = pDrvIf_Spec->pXFer_Dev->pRxBuff; // Set destination buffer as device buffer
		else pDrvIf_Spec->XFer_Rec.pRxBuff = pbuff;									// Set destination buffer as user defined Input buffer
		
		pDrvIf_Spec->XFer_Rec.pTxBuff = pDrvIf_Spec->pXFer_Dev->pTxBuff;			// Transsmit buffer will be device's buffer
																					// if data not be available, will be used dummy data
		pDrvIf_Spec->XFer_Rec.TxDataEstimated = RdLen;								// write same count of will be read!
		pDrvIf_Spec->XFer_Rec.DevDataBitSize = pDrvIf_Spec->pXFer_Dev->DevDataBitSize;
		
		//Set activity H/L for SSEL
		pDrvIf_Spec->pPeri->CFG &= ~(SPI_CFG_SPOL0_MASK | SPI_CFG_SPOL1_MASK | SPI_CFG_SPOL2_MASK | SPI_CFG_SPOL3_MASK);
		pDrvIf_Spec->pPeri->CFG |= pDrvIf_Spec->pSSEL[pDrvIf_Spec->pXFer_Dev->Dev_SSEL_ID]->Std.Act << (8 + pDrvIf_Spec->pXFer_Dev->Dev_SSEL_ID);
		
		// 2. Write SSEL ID
		//pDrvIf_Spec->XFer_Rec.TxOpCodeEstimated = pDrvIf_Spec->pXFer_Dev->DevOpcodeByteSize; // Set OpCode size in bytes
		pDrvIf_Spec->XFer_Rec.Dev_SSEL_ID = pDrvIf_Spec->pXFer_Dev->Dev_SSEL_ID;	// save SSELID
		CHAL_SPI_Flush_Rx(pDrvIf_Spec->pPeri);										// Flush Rx
		CHAL_SPI_Flush_Tx(pDrvIf_Spec->pPeri);										// Flush Tx

		// 3. Start transfer
		pDrvIf_Spec->XFer_Rec.Status = CHAL_SPI_STAT_BUSY;							// Status to BUSY
		CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, UINT32_MAX, true);					// enable all interrupt
		CHAL_SPIFIFO_Enable_IRQ(pDrvIf_Spec->pPeri, UINT32_MAX, true);
		
#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)
		// Enable FIFO and set threshold level
		CHAL_SPIFIFO_Set_TxTHLevel(pDrvIf_Spec->pPeri, 0); 							// Set threshold to empty TxFIFO, next threshold will be set in IRQ Routine
		if(pDrvIf_Spec->XFer_Rec.RxBuffSize < pDrvIf_Spec->Controller.RxFIFO_Max) CHAL_SPIFIFO_Set_RxTHLevel(pDrvIf_Spec->pPeri, pDrvIf_Spec->XFer_Rec.RxBuffSize); // Set threshold to read len
		else CHAL_SPIFIFO_Set_RxTHLevel(pDrvIf_Spec->pPeri, pDrvIf_Spec->Controller.RxFIFO_Max); // Set threshold to maximum
#else
		CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQEN_RXDONE | CHAL_SPI_IRQEN_TXRDY, true); // Enable TxRx
#endif
		CHAL_SPI_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, true);							// Enable SPI IRQ
		SYS_CPUCRIT_END();
		
		// wait for done on exit
		if(XferType == XFer_Blocking) 
		{
			res = SPI_WaitForSetStat( pDrvIf_Spec, CHAL_SPI_STAT_OK, CONF_DRV_SPI_TIMEOUTCLK_XFERBLOCK);			// wait for not BUSY and check timeout
#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
			if(res == false) FN_DEBUG_FMTPRINT(" >> Xfer timeout!")
#endif
			Result = RdLen - pDrvIf_Spec->XFer_Rec.RxBuffSize;						// pDrvIf_Spec->XferRec.RxBuff Size must be Zero.
		}		
	}	
	
#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT("%s, XFerCount: 0x%.4X", FN_DEBUG_RES_BOOL(res), RdLen - pDrvIf_Spec->XFer_Rec.RxBuffSize );
#endif		
	
	return(Result);																	// return count of physically readed data
}



// ------------------------------------------------------------------------------------------------------
// PRIVATE
// WaitFor set selected State. With timeout counting. Timeout_Counter is cleared in each peri Interrupt.
// Return: True if State was cleared, False if timeout occured
// Used only for Blocked Transfer Type
COMP_PUSH_OPTIONS
COMP_OPTIONS_O0
bool SPI_WaitForSetStat (drv_SPI_If_Spec_t *pDrvIf_Spec, uint32_t WaitFor_State, uint32_t NumOfTimeOutCLKPulses)
{
	uint32_t Timeout_Value;
	
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	SYS_ASSERT( pDrvIf_Spec != NULL);												// check
	SYS_ASSERT( NumOfTimeOutCLKPulses != NULL);										// check
#endif
	
	Timeout_Value = (sys_System.Clock->CPU_Freq ) / pDrvIf_Spec->Controller.Speed_Hz; // ( pDrvIf_Spec->pPeri); // get number of CPU tick per one peri CLK pulse
	Timeout_Value = (Timeout_Value * NumOfTimeOutCLKPulses) / 13;					// wait n cycles. 13 is asm instruction in do...while
	pDrvIf_Spec->XFer_Rec.Timeout_Preset = Timeout_Value;							// Save Preset value
	pDrvIf_Spec->XFer_Rec.Timeout_Countdown = Timeout_Value;							// Preset counter
	while((pDrvIf_Spec->XFer_Rec.Status & WaitFor_State) != WaitFor_State) 			// wait while STAT is occured
	{ 
		if(pDrvIf_Spec->XFer_Rec.Timeout_Countdown) pDrvIf_Spec->XFer_Rec.Timeout_Countdown --;
		else 																		// timout occured !!!!!!!!!
		{
			pDrvIf_Spec->XFer_Rec.Status = (CHAL_SPI_Xfr_Status_t) (CHAL_SPI_STAT_ERROR | CHAL_SPI_STAT_TIMEOUT);
			return(false);															// Timeout Error
		}
		if(pDrvIf_Spec->XFer_Rec.Status & CHAL_SPI_STAT_ERROR) return(false);		// or some error occured
	}	
	if(pDrvIf_Spec->XFer_Rec.Status & CHAL_SPI_STAT_ERROR) return(false);			// some error occured
	return(true);
}
COMP_POP_OPTIONS













//// ------------------------------------------------------------------------------------------------------
//// PRIVATE
//// WaitFor State function. With timeout counting. Timeout_Counter is cleared in each SPI Interrupt.
//// Return: True if State was arrived, False if timeout occured
//COMP_PUSH_OPTIONS
//COMP_OPTIONS_O0
//bool SPI_WaitFor (drv_SPI_If_Spec_t *pDrvIf_Spec, uint32_t WaitFor_State, uint32_t NumOfTimeOutCLKPulses)
//{
//	uint32_t Timeout_Value;
//	Timeout_Value = (sys_System.Part->CPU_Freq / 1000) / CHAL_SPI_Get_BusSpeed( pDrvIf_Spec->pPeri); // get number of CPU tick per one SPI CLK pulse
//	Timeout_Value = (Timeout_Value * NumOfTimeOutCLKPulses) / 13;					// wait n cycles. 13 is asm instruction in do...while
//		
//	while((pDrvIf_Spec->XFer_Rec.Status & WaitFor_State) == WaitFor_State) 			// wait for end of transfer
//	{ 
//		if(pDrvIf_Spec->Timeout_Counter < Timeout_Value) pDrvIf_Spec->Timeout_Counter ++;
//		else 																		// timout occured !!!!!!!!!
//		{
//			CHAL_SPI_SendEOT(pDrvIf_Spec->pPeri);									// Cancel SPI
//			pDrvIf_Spec->XFer_Rec.Status = (CHAL_SPI_Xfr_Status_t) (CHAL_SPI_STAT_ERROR | CHAL_SPI_STAT_TIMEOUT);
//			return(false);
//		}
//	}	
//	if(pDrvIf_Spec->XFer_Rec.Status & CHAL_SPI_STAT_ERROR) return(false);			// some error occured
//	return(true);
//}
//COMP_POP_OPTIONS


// ------------------------------------------------------------------------------------------------------
// PRIVATE
// Create device - if parameter pDevice is set, only assign SSELD and check critical parameters for empty.
//					Otherwise create a device and fill up structure
#pragma diag_suppress 550	
bool SPI_CreateDevice(struct _drv_If *pDrvIf, SPI_Device_t* pDevice, uint16_t SSELID)
{
	bool res = false;

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	SYS_ASSERT( pDrvIf != NULL);													// check
#endif
	
#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pDev: %p, SSELID: 0x%.1X", pDevice, SSELID);
#endif	
	
	if(pDevice == NULL) pDevice = (void *)sys_malloc_zero(sizeof(SPI_Device_t));	// allocate piece of memory and clear it if device doesnt exists
	
	if (pDevice != NULL) 															// device created succesfully?
	{
		res = true;

		pDevice->Dev_SSEL_ID = SSELID;												// set hardware SSELID pin
//		if(pDevice->DevOpcodeByteSize == 0) 
//			pDevice->DevOpcodeByteSize = CONF_DRV_SPI_ARG_OPCODE_SIZEMAX;			// Set Opcode size if doesnt wroted
		
		// check and create transsmit buffer
		if(pDevice->pTxBuff == NULL)
		{
			pDevice->pTxBuff = sys_Buffer_Create(CONF_DRV_SPI_BUFFERSIZE, GET_BYTES(pDevice->DevDataBitSize), sBuffRing, NULL);	// Create Tx comm buffer
			
			if(pDevice->pTxBuff == NULL)											// cannot create buffer ?
			{
#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
				FN_DEBUG_FMTPRINT(" >> Not enought memory for create pTxBuff!");
#endif
				SPI_DestroyDevice(pDrvIf, pDevice);									// destroy device and freeing memory
				res = false;
			}
		}
		
		// check and create receive buffer
		if(pDevice->pRxBuff == NULL)
		{		
			pDevice->pRxBuff = sys_Buffer_Create(CONF_DRV_SPI_BUFFERSIZE, GET_BYTES(pDevice->DevDataBitSize), sBuffRing, NULL);	// Create Rx comm buffer
			
			if(pDevice->pRxBuff == NULL)											// cannot create buffer ?
			{
#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
				FN_DEBUG_FMTPRINT(" >> Not enought memory for create pRxBuff!");
#endif
				SPI_DestroyDevice(pDrvIf, pDevice);									// destroy device and freeing memory
				res = false;
			}



			
		}			
#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
		FN_DEBUG_FMTPRINT(", pTxBuff: %p, pTxBuff: %p", pDevice->pTxBuff, pDevice->pRxBuff);
#endif
	}

#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
	FN_DEBUG_FMTPRINT("%s, pDev: %p", FN_DEBUG_RES_BOOL(res), pDevice);
#endif		

	return(res);
}
#pragma diag_default 550


// ------------------------------------------------------------------------------------------------------
// PRIVATE
// Destroy device - deallocate memory.
#pragma diag_suppress 550
bool SPI_DestroyDevice(struct _drv_If *pDrvIf, SPI_Device_t *pDevice)
{
	bool res = false;
	
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	SYS_ASSERT( pDrvIf != NULL);													// check
	SYS_ASSERT( pDevice != NULL);													// check
#endif
	
#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pDev: %p", pDevice);
#endif	

	if(pDevice) 
	{
		if(pDevice->pTxBuff) sys_Buffer_Destroy(pDevice->pTxBuff);					// clear pTxBuffer
		if(pDevice->pRxBuff) sys_Buffer_Destroy(pDevice->pRxBuff);					// clear pRxBuffer
		sys_free(pDevice);															// clear memory
		res = true;
	}
	
#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif		
	return(res);
}
#pragma diag_default 550


// ------------------------------------------------------------------------------------------------------
// EXT
// Change Bus speed and check change
// Bus speed is in tolerance CONF_DRV_SPI_SPEED_TOL
// Return: True if Change was OK, otherwise False
bool SPI_Set_BusSpeed(struct _drv_If *pDrvIf, uint32_t NewSpeed_Hz)
{
	bool res = false;

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	SYS_ASSERT( pDrvIf != NULL);													// check
#endif
	
	drv_SPI_If_Spec_t* pDrvIf_Spec = (drv_SPI_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie
	uint32_t BusSpeed_Hz;
	
	if((pDrvIf_Spec->Controller.Speed_Hz > NewSpeed_Hz + ( NewSpeed_Hz * (CONF_DRV_SPI_SPEED_TOL / 100) ) ) ||
	(pDrvIf_Spec->Controller.Speed_Hz < NewSpeed_Hz - ( NewSpeed_Hz * (CONF_DRV_SPI_SPEED_TOL / 100) ) ))
	{
#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
		FN_DEBUG_ENTRY(pDrvIf->Name);
		FN_DEBUG_FMTPRINT(" >> Required Speed: %d Hz", NewSpeed_Hz);
#endif	
		SYS_CPUCRIT_START();	
		res = CHAL_SPI_Set_BusSpeed(pDrvIf_Spec->pPeri, NewSpeed_Hz);
		pDrvIf_Spec->Controller.Speed_Hz = CHAL_SPI_Get_BusSpeed(pDrvIf_Spec->pPeri);
		SYS_CPUCRIT_END();	
		
		BusSpeed_Hz = CHAL_SPI_Get_BusSpeed(pDrvIf_Spec->pPeri);
		if((res == true) && (BusSpeed_Hz <= NewSpeed_Hz)) res = true;
		else res = false;
		
		pDrvIf_Spec->Controller.Speed_Hz = BusSpeed_Hz;							// store actual speed in kHz
	}
	
#if defined(CONF_DEBUG_DRV_SPI) && (CONF_DEBUG_DRV_SPI == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
	FN_DEBUG_FMTPRINT("%s, Wroted Speed:: %d Hz", FN_DEBUG_RES_BOOL(res), BusSpeed_Hz);
#endif	
	
	return(res);
}

// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Quick handler
void SPI_Handler_Top(_drv_If_t *pDrvIf)
{
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	SYS_ASSERT( pDrvIf != NULL);													// check
#endif
	
	drv_SPI_If_Spec_t* pDrvIf_Spec = (drv_SPI_If_Spec_t*) pDrvIf->pDrvSpecific;

	// Generic interrupt routine:
	uint32_t IRQStatus = CHAL_SPI_Get_IRQ_Status(pDrvIf_Spec->pPeri);
#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)	
	uint32_t FIFOIRQStatus = CHAL_SPIFIFO_Get_IRQ_Status(pDrvIf_Spec->pPeri);
#endif	


	// SSEL Asserted: .. start of transfer +++++++++++++++++++++++++++++++++++++++++
	if (IRQStatus & CHIP_SPI_IRQSTAT_SSA)											// SPI become a Busy
	{
		//CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQEN_SSA, false);			// disable SSEL assert interrupt
		CHAL_SPI_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_SPI_IRQSTAT_SSA);		// clear interrupt status
	}
	
	
	// SSEL De-Asserted:   .. end of transfer ++++++++++++++++++++++++++++++++++++++
	if (IRQStatus & CHAL_SPI_IRQSTAT_SSD)											// SPI become an Idle
	{
		//CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQEN_SSD, false);		// disable interrupt
		CHAL_SPI_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_SPI_IRQSTAT_SSD);		// clear interrupt status
	}


	
	if (IRQStatus & CHAL_SPI_IRQSTAT_MSTIDLE)										// SPI transsmiter is in the ready state
	{
		if(( pDrvIf_Spec->XFer_Rec.Status == CHAL_SPI_STAT_BUSY) && (pDrvIf_Spec->XFer_Rec.TxDataEstimated == 0))	// trasfer was NOT in progress ???
		{
			pDrvIf_Spec->XFer_Rec.Status = CHAL_SPI_STAT_OK;
			CHAL_SPI_Clear_Peri_Status(pDrvIf_Spec->pPeri, 1 << 7);					// Set EndTransfer bit
		}
		CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQEN_MSTIDLE, false);	// disable interrupt
//			//CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQEN_SSD, false);	// disable interrupt
//			//CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQEN_MSTIDLE, false);	// disable interrupt
//			if(pDrvIf_Spec->XFer_Rec.TxDataEstimated)								// Have I any data to send?
//			{
//				goto SendData;														// start send
//			}
//			//CHAL_SPI_Flush_Rx(pDrvIf_Spec->pPeri);									// Flush Rx
//			//CHAL_SPI_Flush_Tx(pDrvIf_Spec->pPeri);									// Flush Tx
//			//CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQEN_SSA, true);		// enable SSEL Assert interrupt
//			//pDrvIf_Spec->XFer_Rec.Status = CHAL_SPI_STAT_OK;
//		}
//		//CHAL_SPI_Clear_Peri_Status(pDrvIf_Spec->pPeri, CHAL_SPI_IRQSTAT_SSD);		// clear status
		CHAL_SPI_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_SPI_IRQSTAT_MSTIDLE);	// clear interrupt status
	}
	
	
	
	
	
	
	
	
	
	
	
// Receiver error or overflow: ++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)
	if ((FIFOIRQStatus & CHAL_SPIFIFO_IRQSTAT_RXOV) == CHAL_SPIFIFO_IRQSTAT_RXOV)
	//if (CHAL_SPIFIFO_Get_IRQ_Status(pDrvIf_Spec->pPeri) & CHAL_SPIFIFO_IRQSTAT_RXOV)
#else
	if (IRQStatus & CHAL_SPI_IRQSTAT_RXOV)
#endif		
	{
		/* Clear errors */
RXBuffFull:
//		uint32_t rdtmp;
//		rdtmp = CHAL_SPI_Get_Data(pDrvIf_Spec->pPeri);								// read rx for emtying
		CHAL_SPI_Flush_Rx(pDrvIf_Spec->pPeri);										// Flush Rx
		
		pDrvIf_Spec->XFer_Rec.Status = CHAL_SPI_STAT_ERROR;							// Done with error
		pDrvIf_Spec->XFer_Rec.Status |= CHAL_SPI_STAT_BUFFFULL;						// Receive buffer full
		
#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)
		CHAL_SPIFIFO_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPIFIFO_IRQEN_RXOV, false);	// disable Rx interrupt and RX Err
		CHAL_SPIFIFO_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_SPIFIFO_IRQSTAT_RXOV);	// clear interrupt status
#else
		CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQEN_RXOV, false);		// disable Rx interrupt and RX Err
		CHAL_SPI_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_SPI_IRQSTAT_RXOV);		// clear status
#endif
		return;
	}

	
	
	
	
// Transceiver error or underrun: +++++++++++++++++++++++++++++++++++++++++++++++++++
#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)
	if ((FIFOIRQStatus & CHAL_SPIFIFO_IRQSTAT_TXUR) == CHAL_SPIFIFO_IRQSTAT_TXUR)
	//if (CHAL_SPIFIFO_Get_IRQ_Status(pDrvIf_Spec->pPeri) & CHAL_SPIFIFO_IRQSTAT_TXUR)
#else
	if (IRQStatus & CHAL_SPI_IRQSTAT_TXUR)
#endif	
	{
		CHAL_SPI_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);						// Disable SPI IRQ
		
		pDrvIf_Spec->XFer_Rec.Status = CHAL_SPI_STAT_ERROR;							// error !
		pDrvIf_Spec->XFer_Rec.Status |= CHAL_SPI_STAT_BUFFNONE;						// no data in buffer

		
#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)
		
		CHAL_SPIFIFO_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPIFIFO_IRQSTAT_TXUR, false);	// disable TX Err
		CHAL_SPIFIFO_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_SPIFIFO_IRQSTAT_TXUR);	// clear status
#else
		CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQSTAT_TXUR, false);	// disable TX Err
		CHAL_SPI_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_SPI_IRQSTAT_TXUR);		// clear interrupt status
#endif
		return;
	}






	
	// Transceiver Ready: .. ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)					
	if ((FIFOIRQStatus & CHAL_SPIFIFO_IRQSTAT_TXRDY) == CHAL_SPIFIFO_IRQSTAT_TXRDY)
	//if (CHAL_SPIFIFO_Get_IRQ_Status(pDrvIf_Spec->pPeri) & CHAL_SPIFIFO_IRQSTAT_TXRDY)
#else		
	if (IRQStatus & CHAL_SPI_IRQSTAT_TXRDY)
#endif			
	{
		// Fill Tx FIFO if we neet to send data
		if(pDrvIf_Spec->XFer_Rec.TxDataEstimated)									// Have I any data to send?
		{
			uint32_t tmp = 0;
			bool EndWrite;
			
			
			pDrvIf_Spec->XFer_Rec.Status = CHAL_SPI_STAT_BUSY;						// Still in progress
			
			if(pDrvIf_Spec->XFer_Rec.TxDataEstimated > pDrvIf_Spec->Controller.TxFIFO_Max) CHAL_SPIFIFO_Set_TxTHLevel(pDrvIf_Spec->pPeri, pDrvIf_Spec->Controller.TxFIFO_Max); // Set threshold to maximum
			else CHAL_SPIFIFO_Set_TxTHLevel(pDrvIf_Spec->pPeri, pDrvIf_Spec->XFer_Rec.TxDataEstimated); // Set threshold
			
			if(pDrvIf_Spec->XFer_Rec.RxBuffSize)
				CHAL_SPIFIFO_Set_RxTHLevel(pDrvIf_Spec->pPeri, pDrvIf_Spec->XFer_Rec.RxBuffSize); 	// Set Receive threshold to avaiting data  !!
			else 
				CHAL_SPIFIFO_Set_RxTHLevel(pDrvIf_Spec->pPeri, 0); 					// Set Receive threshold to 0 char
			
			do																		// fill FIFO
			{
				if(pDrvIf_Spec->XFer_Rec.pTxBuff)
				{
					if(sys_Buffer_Read_Element(pDrvIf_Spec->XFer_Rec.pTxBuff,(uint8_t *) &tmp, 1) == 1);// read element from buffer was OK ?
					else tmp = CONF_DRV_SPI_DUMMYDATA;								// if not available, use dummy data
				}else tmp = CONF_DRV_SPI_DUMMYDATA;									// if not available, use dummy data
				
				pDrvIf_Spec->XFer_Rec.TxDataEstimated --;
				
				if(pDrvIf_Spec->XFer_Rec.TxDataEstimated)							// Have I next data to send?
				{																	// Yes, next we have a Data for WRITE
					tmp |= (1 << 21);												// Insert EOF
					tmp |= (pDrvIf_Spec->XFer_Rec.DevDataBitSize-1) << 24;			// set datawidth
					EndWrite = false;
				}
				else
				{																	// We havent' data for next WRITE 
					if(pDrvIf_Spec->XFer_Rec.RxBuffSize == 0)						// Will read ?
					{
						tmp |= (1 << 22);											// No, Insert RXIGNORE
					}
					
					tmp |= (1 << 20);												// Insert EOT -- end transfer
					CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQEN_MSTIDLE, true);// enable interrupt
					EndWrite = true;
				}
				
				CHAL_SPI_Put_Data(pDrvIf_Spec->pPeri, tmp, pDrvIf_Spec->XFer_Rec.DevDataBitSize, tmp, pDrvIf_Spec->XFer_Rec.Dev_SSEL_ID);	// write data to peri. 				
				
			}while(!EndWrite);
			if(pDrvIf_Spec->XFer_Rec.Timeout_Preset) pDrvIf_Spec->XFer_Rec.Timeout_Countdown = pDrvIf_Spec->XFer_Rec.Timeout_Preset;	// preset timeout
		}
		else
		{
			if(( pDrvIf_Spec->XFer_Rec.Status == CHAL_SPI_STAT_BUSY) && (pDrvIf_Spec->XFer_Rec.TxDataEstimated == 0))	// trasfer was NOT in progress ???
			{
#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)					
					CHAL_SPIFIFO_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPIFIFO_IRQEN_TXRDY | CHAL_SPIFIFO_IRQEN_TXUR, false);// disable tx fifo interrupt and error int
#else		
					CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQEN_TXRDY | CHAL_SPI_IRQEN_TXUR, false);// disable tx fifo interrupt and error int
#endif				
				if(pDrvIf_Spec->XFer_Rec.RxBuffSize == 0) pDrvIf_Spec->XFer_Rec.Status = CHAL_SPI_STAT_OK; // OK only when we don't wait for receive data!
	//			CHAL_SPI_Clear_Peri_Status(pDrvIf_Spec->pPeri, 1 << 7);				// Set EndTransfer bit
			}
		}
#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)					
		CHAL_SPIFIFO_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_SPIFIFO_IRQSTAT_TXRDY);	// clear interrupt status
#else
		CHAL_SPI_Clear_IRQ_Status(pDrvIf_Spec->pPeri, CHAL_SPI_IRQSTAT_TXRDY);			// clear interrupt status
#endif		
	}

	

// re-readinterrupt status	
	IRQStatus = CHAL_SPI_Get_IRQ_Status(pDrvIf_Spec->pPeri);
#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)	
	FIFOIRQStatus = CHAL_SPIFIFO_Get_IRQ_Status(pDrvIf_Spec->pPeri);
#endif
	
	
	
	// Received character: .. +++++++++++++++++++++++++++++++++++++++++++++++++++++++
#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)					
	if ((FIFOIRQStatus & CHAL_SPIFIFO_IRQSTAT_RXDONE) == CHAL_SPIFIFO_IRQSTAT_RXDONE)
	//if (CHAL_SPIFIFO_Get_IRQ_Status(pDrvIf_Spec->pPeri) & CHAL_SPIFIFO_IRQSTAT_RXDONE)
#else		
	if (IRQStatus & CHAL_SPI_IRQSTAT_RXDONE)
#endif			
	{
#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)					
		uint32_t RxFIFOCount = _Chip_SPIFIFO_Get_Peri_Status(pDrvIf_Spec->pPeri);
#else
		uint32_t RxFIFOCount = _Chip_SPI_Get_Peri_Status(pDrvIf_Spec->pPeri);
#endif		

		RxFIFOCount &= (0x1f << 16);	// separate RXLVL
		RxFIFOCount >>= 16;				// move to 0 bit
		
		do
		{
			uint32_t Readed = CHAL_SPI_Get_Data(pDrvIf_Spec->pPeri);					// data have to read !

			if(pDrvIf_Spec->XFer_Rec.pRxBuff == NULL)									// Receive data buffer ready?
			{
#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)					
				CHAL_SPIFIFO_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPIFIFO_IRQSTAT_RXDONE, false);	// disable Rx interrupt
#else		
				CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQSTAT_RXDONE, false);// disable Rx interrupt
#endif		
				CHAL_SPI_Flush_Rx(pDrvIf_Spec->pPeri);									// Flush Rx
				// data will stolen. We doesn't wait fo it...
				pDrvIf_Spec->XFer_Rec.Status = CHAL_SPI_STAT_OK;						// Done
			}
			else		
			{			// we wait for data !
				if(pDrvIf_Spec->XFer_Rec.pRxBuff->Full == 0)
				{
					Readed &= (1 << pDrvIf_Spec->XFer_Rec.DevDataBitSize) - 1;			// remask valid data only
					
					sys_Buffer_Write_Element(pDrvIf_Spec->XFer_Rec.pRxBuff, (uint8_t*) &Readed, 1);	// save to destination
					
					if(pDrvIf_Spec->XFer_Rec.Timeout_Preset) pDrvIf_Spec->XFer_Rec.Timeout_Countdown = pDrvIf_Spec->XFer_Rec.Timeout_Preset;	// preset timeout
					if(pDrvIf_Spec->XFer_Rec.RxBuffSize) pDrvIf_Spec->XFer_Rec.RxBuffSize --;
					//CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQEN_SSD, true);	// enable SSEL Deassert interrupt
				}
				else																	// buffer full
				{
					goto RXBuffFull;
				}

				if(pDrvIf_Spec->XFer_Rec.RxBuffSize == 0)								// Correct end of receive
				{
					//CHAL_SPI_Flush_Rx(pDrvIf_Spec->pPeri);								// Flush Rx
					
	//#if defined(_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)					
	//				CHAL_SPIFIFO_Clear_Peri_Status(pDrvIf_Spec->pPeri, CHAL_SPIFIFO_IRQSTAT_RXDONE | CHAL_SPIFIFO_IRQSTAT_RXOV);// clear status
	//				CHAL_SPIFIFO_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPIFIFO_IRQSTAT_RXDONE | CHAL_SPIFIFO_IRQEN_RXOV, false);	// disable Rx interrupt and RX Err
	//#else		
	//				CHAL_SPI_Clear_Peri_Status(pDrvIf_Spec->pPeri, CHAL_SPI_IRQSTAT_RXDONE | CHAL_SPI_IRQSTAT_RXOV);// clear status
	//				CHAL_SPI_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_SPI_IRQSTAT_RXRDONE | CHAL_SPI_IRQEN_RXOV, false);	// disable Rx interrupt and RX Err
	//#endif			
					CHAL_SPI_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);				// Disable SPI IRQ
					pDrvIf_Spec->XFer_Rec.Status = CHAL_SPI_STAT_OK;					// Done
				}
			}
		}while(-- RxFIFOCount);
	}
}













// ------------------------------------------------------------------------------------------------------
// After handler
// fire event system
inline void SPI_Handler_Bottom(_drv_If_t *pDrvIf)
{
	
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	SYS_ASSERT( pDrvIf != NULL);													// check
#endif
	
	if(pDrvIf->pIRQ_USR_HandlerBottom) pDrvIf->pIRQ_USR_HandlerBottom(pDrvIf);		// override with USR handler
	else
	{
		if(pDrvIf->Sys.Stat.Initialized == false) return;
		if(pDrvIf->Sys.Stat.Loaded == false) return;
		
		// Generic interrupt routine:
		if(pDrvIf->pAPI_STD->Events) pDrvIf->pAPI_STD->Events( pDrvIf, EV_DataReceived);	// ak je nastaveny event system, zavolaj ho
	}
}




// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************
// ------------------------------------------------------------------------------------------------
// Event system - volane v preruseni !!!
inline void SPI_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{
//#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
//	SYS_ASSERT( pDrvIf != NULL);													// check
//#endif
//	
//	void* pCallBack = NULL;		
//	_ColListRecord_t *tmpListRecord = pDrvIf->Sys.ParentModLinked.pFirst;			// temporary pointer to parent module list
//	while(tmpListRecord)
//	{
//		switch(EventType)
//		{
//			case EV_StateChanged:
//			{
////				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_ChangedState;
//				break;
//			}
//			case EV_DataReceived:
//			{
////				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_DataReceived;
//				break;
//			}
//			case EV_Transmitted:
//			{
////				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_DataTransmitted;
//				break;
//			}
//			default: break;
//		}
//		
//		if(pCallBack)																// callback is set?
//		{
//			sys_Task_Create_Item( pDrvIf->Name, pCallBack, (sys_TaskArgs_t) {NULL, 0});	// create task for data read
//		}

//		if(tmpListRecord->pNext == NULL) break;										// loop while have associadet module
//		tmpListRecord = tmpListRecord->pNext;										// goto next associated module
//	}
}

#endif	// ifdef __DRV_SPI_H_
