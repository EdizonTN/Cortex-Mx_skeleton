// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_OneWire.c
// 	   Version: 1.11
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for OneWire bus
// ******************************************************************************
// Info: OneWire interface driver. OneWire Can scan interface and create list of connected devices. 
//	Creates Handle to devices also!	Each device has UID (ordinal number during scan) and a handle to OneWire_DevData_t structure.
//	Inspired by: https://stm32f4-discovery.net/2014/05/library-12-onewire-library-for-stm43f4xx/
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
//				2024.09.30	- v1.11 - add sys_NULL_Function instead a NULL pointer to a function in API structure
// 				11.11.2019 	- v1.1	- Read/write - pbuff changet to sys_Buffer_t

#include "Skeleton.h"

#if defined(USE_DRV_ONEWIRE) && (USE_DRV_ONEWIRE == 1)


// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool OneWire_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool OneWire_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool OneWire_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
uint32_t OneWire_Write_Data(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t WrLen, _XFerType_t XferType);
uint32_t OneWire_Read_Data (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t RdLen, _XFerType_t XferType);

//-----------------------------------------------
void OneWire_Write_Bit( _drv_If_t *pDrvIf, uint8_t Bit);
uint8_t OneWire_Read_Bit( _drv_If_t *pDrvIf);
void OneWire_Write_Byte( _drv_If_t *pDrvIf, uint8_t byte);
uint8_t OneWire_Read_Byte( _drv_If_t *pDrvIf); 
// ******************************************************************************************************
// Public Function - specific for this driver
// ******************************************************************************************************
uint8_t OneWire_Scan ( struct _drv_If *pDrvIf, uint8_t MaxDevs);
void* OneWire_GetDeviceData (  struct _drv_If *pDrvIf, uint8_t DeviceNum);

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************
const drv_OneWire_Ext_API_t	drv_OneWire_Ext_API_STD 	= 
{
	.Scan			=	OneWire_Scan,
	.GetDeviceData	=	OneWire_GetDeviceData,
};

// structure and assign to standard API functions:
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_OneWire_API_STD 		= 
{
 	.Init			=	OneWire_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	OneWire_Enable,
	.Disable		=	OneWire_Disable,
	.Write_Data		=	OneWire_Write_Data,
	.Read_Data		=	OneWire_Read_Data,
	.Ext			=	(_drv_Api_t *) &drv_OneWire_Ext_API_STD,
};
#pragma pop

// ******************************************************************************************************
// Functions prototypes
// ******************************************************************************************************




// ******************************************************************************************************
// Private Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// PRIVATE
// ------------------------------------------------------------------------------------------------------
uint8_t OneWire_Reset(_drv_If_t *pDrvIf) 
{
	drv_OneWire_If_Spec_t* pDrvIf_Spec = (drv_OneWire_If_Spec_t*) pDrvIf->pDrvSpecific;
	uint8_t res;

#if defined(CONF_DEBUG_DRV_ONEWIRE) && (CONF_DEBUG_DRV_ONEWIRE == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
#endif	
	
	SYS_CPUCRIT_START();

	// now reset onewire - Line low, and wait 480us
	CHAL_Signal_Write_Direction(pDrvIf_Spec->sigComm->Std, CHAL_IO_Output);			// change pin direction to Output
	CHAL_Signal_Set_Pin(pDrvIf_Spec->sigComm->Std);									// Set to Low - Active is in Log 0 !!!
	Delay_us(480);																	// wait 480 usec
	// Release line and wait for 70us
	CHAL_Signal_Write_Direction(pDrvIf_Spec->sigComm->Std, CHAL_IO_Input);			// change pin direction to Input
	Delay_us(70);																	// wait 70 usec
	
	// Check bit value
	if(CHAL_Signal_Read_Pin(pDrvIf_Spec->sigComm->Std) == true) res = 0;			// pin state as result
	else res = 1;
	
	// Delay for 410 us
	Delay_us(410);																	// wait 410 usec
	// end of reset
	
	SYS_CPUCRIT_END();
	
#if defined(CONF_DEBUG_DRV_ONEWIRE) && (CONF_DEBUG_DRV_ONEWIRE == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif		
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// PRIVATE
// ------------------------------------------------------------------------------------------------------
void OneWire_Write_Bit( _drv_If_t *pDrvIf, uint8_t Bit)
{
	drv_OneWire_If_Spec_t* pDrvIf_Spec = (drv_OneWire_If_Spec_t*) pDrvIf->pDrvSpecific;
	
	SYS_CPUCRIT_START();
	
	if(Bit)
	{
		// Set line low
		CHAL_Signal_Write_Direction(pDrvIf_Spec->sigComm->Std, CHAL_IO_Output);		// change pin direction to Output
		CHAL_Signal_Set_Pin(pDrvIf_Spec->sigComm->Std);								// Set to Low
		Delay_us(10);																// wait 10 usec
		// Bit high
		CHAL_Signal_Write_Direction(pDrvIf_Spec->sigComm->Std, CHAL_IO_Input);		// change pin direction to Input
		// Wait for 55 us and release the line
		Delay_us(55);																// wait 55 usec
		CHAL_Signal_Write_Direction(pDrvIf_Spec->sigComm->Std, CHAL_IO_Input);		// change pin direction to Input		
	}
	else
	{
		// Set line low
		CHAL_Signal_Write_Direction(pDrvIf_Spec->sigComm->Std, CHAL_IO_Output);		// change pin direction to Output
		CHAL_Signal_Set_Pin(pDrvIf_Spec->sigComm->Std);								// Set to Low
		Delay_us(65);																// wait 65 usec
		// Bit high
		CHAL_Signal_Write_Direction(pDrvIf_Spec->sigComm->Std, CHAL_IO_Input);		// change pin direction to Input
		// Wait for 5 us and release the line
		Delay_us(5);																// wait 5 usec
		CHAL_Signal_Write_Direction(pDrvIf_Spec->sigComm->Std, CHAL_IO_Input);		// change pin direction to Input
		
	}
	SYS_CPUCRIT_END();
}

// ------------------------------------------------------------------------------------------------------
// PRIVATE
// ------------------------------------------------------------------------------------------------------
uint8_t OneWire_Read_Bit( _drv_If_t *pDrvIf)
{
	drv_OneWire_If_Spec_t* pDrvIf_Spec = (drv_OneWire_If_Spec_t*) pDrvIf->pDrvSpecific;
	uint8_t res = 0;
	
	SYS_CPUCRIT_START();

	// Line low
	CHAL_Signal_Write_Direction(pDrvIf_Spec->sigComm->Std, CHAL_IO_Output);			// change pin direction to Output
	CHAL_Signal_Set_Pin(pDrvIf_Spec->sigComm->Std);									// Set to Low
	Delay_us(3);																	// wait 3 usec
	
	// Release line */
	CHAL_Signal_Write_Direction(pDrvIf_Spec->sigComm->Std, CHAL_IO_Input);			// change pin direction to Input
	Delay_us(10);																	// wait 10 usec
	
	// Read line value */
	if (CHAL_Signal_Read_Pin(pDrvIf_Spec->sigComm->Std))							// pin state as result
	{
		// Bit is LOW */
		res = 0;
	}else res = 1;
	
	// Wait 50us to complete 60us period */
	Delay_us(50);																	// wait 50 usec
	
	SYS_CPUCRIT_END();
	
	return res;																		// Return Value
}

// ------------------------------------------------------------------------------------------------------
// PRIVATE
////	Write 1-byte /8-bits/ to OneWire
// ------------------------------------------------------------------------------------------------------
void OneWire_Write_Byte( _drv_If_t *pDrvIf, uint8_t byte) 
{ 
	SYS_CPUCRIT_START();
	uint8_t i = 8;
	while (i--) 
	{
		OneWire_Write_Bit( pDrvIf, byte & 0x01);									// LSB bit is first
		byte >>= 1;
	}
	SYS_CPUCRIT_END();
}

// ------------------------------------------------------------------------------------------------------
// PRIVATE
//	Read 1-byte /8-bits/ from OneWire
// ------------------------------------------------------------------------------------------------------
uint8_t OneWire_Read_Byte( _drv_If_t *pDrvIf) 
{
	SYS_CPUCRIT_START();
	uint8_t i = 8, byte = 0;
	while (i--) {
		byte >>= 1;
		byte |= (OneWire_Read_Bit( pDrvIf) << 7);
	}
	SYS_CPUCRIT_END();
	return byte;
}


// ------------------------------------------------------------------------------------------------------
// PRIVATE
// ------------------------------------------------------------------------------------------------------
void OneWire_ResetSearch(_drv_If_t *pDrvIf) 
{
	drv_OneWire_If_Spec_t* pDrvIf_Spec = (drv_OneWire_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

	/* Reset the search state */
	pDrvIf_Spec->Priv.LastDiscrepancy = 0;
	pDrvIf_Spec->Priv.LastDeviceFlag = 0;
	pDrvIf_Spec->Priv.LastFamilyDiscrepancy = 0;
}

// ------------------------------------------------------------------------------------------------------
// PRIVATE
// ------------------------------------------------------------------------------------------------------
uint8_t OneWire_Search(_drv_If_t *pDrvIf, uint8_t command) 
{
	drv_OneWire_If_Spec_t* pDrvIf_Spec = (drv_OneWire_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

	uint8_t id_bit_number;
	uint8_t last_zero, rom_byte_number, search_result;
	uint8_t id_bit, cmp_id_bit;
	uint8_t rom_byte_mask, search_direction;

	// Initialize for search
	id_bit_number = 1;
	last_zero = 0;
	rom_byte_number = 0;
	rom_byte_mask = 1;
	search_result = 0;

	// if the last call was not the last one
	if (!pDrvIf_Spec->Priv.LastDeviceFlag) 
	{
		// 1-Wire reset
		if (OneWire_Reset(pDrvIf)) 
		{
			// Reset the search
			pDrvIf_Spec->Priv.LastDiscrepancy = 0;
			pDrvIf_Spec->Priv.LastDeviceFlag = 0;
			pDrvIf_Spec->Priv.LastFamilyDiscrepancy = 0;
			return 0;
		}

		// issue the search command 
		OneWire_Write_Byte(pDrvIf, command);  

		// loop to do the search
		do {
			// read a bit and its complement
			id_bit = OneWire_Read_Bit(pDrvIf);
			cmp_id_bit = OneWire_Read_Bit(pDrvIf);

			// check for no devices on 1-wire
			if ((id_bit == 1) && (cmp_id_bit == 1)) 
			{
				break;
			}
			else 
			{
				// all devices coupled have 0 or 1
				if (id_bit != cmp_id_bit) 
				{
					search_direction = id_bit;  // bit write value for search
				}
				else 
				{
					// if this discrepancy if before the Last Discrepancy on a previous next then pick the same as last time
					if (id_bit_number < pDrvIf_Spec->Priv.LastDiscrepancy) 
					{
						search_direction = ((pDrvIf_Spec->Priv.ROM_NO[rom_byte_number] & rom_byte_mask) > 0);
					} else {
						// if equal to last pick 1, if not then pick 0
						search_direction = (id_bit_number == pDrvIf_Spec->Priv.LastDiscrepancy);
					}
					
					// if 0 was picked then record its position in LastZero
					if (search_direction == 0) 
					{
						last_zero = id_bit_number;

						// check for Last discrepancy in family
						if (last_zero < 9) 
						{
							pDrvIf_Spec->Priv.LastFamilyDiscrepancy = last_zero;
						}
					}
				}

				// set or clear the bit in the ROM byte rom_byte_number with mask rom_byte_mask
				if (search_direction == 1) 
				{
					pDrvIf_Spec->Priv.ROM_NO[rom_byte_number] |= rom_byte_mask;
				}
				else 
				{
					pDrvIf_Spec->Priv.ROM_NO[rom_byte_number] &= ~rom_byte_mask;
				}
				
				// serial number search direction write bit
				OneWire_Write_Bit( pDrvIf, search_direction);

				// increment the byte counter id_bit_number and shift the mask rom_byte_mask
				id_bit_number++;
				rom_byte_mask <<= 1;

				// if the mask is 0 then go to new SerialNum byte rom_byte_number and reset mask
				if (rom_byte_mask == 0) 
				{
					rom_byte_number++;
					rom_byte_mask = 1;
				}
			}
		} while (rom_byte_number < 8);  // loop until through all ROM bytes 0-7

		// if the search was successful then
		if (!(id_bit_number < 65)) 
		{
			// search successful so set LastDiscrepancy,LastDeviceFlag,search_result
			pDrvIf_Spec->Priv.LastDiscrepancy = last_zero;

			// check for last device
			if (pDrvIf_Spec->Priv.LastDiscrepancy == 0) 
			{
				pDrvIf_Spec->Priv.LastDeviceFlag = 1;
			}

			search_result = 1;
		}
	}

	// if no device found then reset counters so next 'search' will be like a first
	if (!search_result || !pDrvIf_Spec->Priv.ROM_NO[0]) 
	{
		pDrvIf_Spec->Priv.LastDiscrepancy = 0;
		pDrvIf_Spec->Priv.LastDeviceFlag = 0;
		pDrvIf_Spec->Priv.LastFamilyDiscrepancy = 0;
		search_result = 0;
	}

	return search_result;
}

// ------------------------------------------------------------------------------------------------------
// PRIVATE
// Calculate CRC
// ------------------------------------------------------------------------------------------------------
uint8_t TM_OneWire_CRC8(uint8_t *addr, uint8_t len) 
{
	uint8_t crc = 0, inbyte, i, mix;
	
	while (len--) 
	{
		inbyte = *addr++;
		for (i = 8; i; i--) 
		{
			mix = (crc ^ inbyte) & 0x01;
			crc >>= 1;
			if (mix) crc ^= 0x8C;
			inbyte >>= 1;
		}
	}
	
	return crc;
}

// ------------------------------------------------------------------------------------------------------
// PRIVATE
// scan 1-wire for connected devices
// ------------------------------------------------------------------------------------------------------
uint8_t OneWire_Get_First(_drv_If_t *pDrvIf) 
{
	OneWire_ResetSearch(pDrvIf);													// Reset search values
	return OneWire_Search(pDrvIf, ONEWIRE_CMD_SEARCHROM);							// Start with searching
}

// ------------------------------------------------------------------------------------------------------
// PRIVATE
// ------------------------------------------------------------------------------------------------------
uint8_t OneWire_Next(_drv_If_t *pDrvIf) 
{
   return OneWire_Search(pDrvIf, ONEWIRE_CMD_SEARCHROM);							// Leave the search state alone
}


// ------------------------------------------------------------------------------------------------------
// PRIVATE
// ------------------------------------------------------------------------------------------------------
uint8_t OneWire_GetROM(struct _drv_If *pDrvIf, uint8_t index) 
{
	drv_OneWire_If_Spec_t* pDrvIf_Spec = (drv_OneWire_If_Spec_t*) pDrvIf->pDrvSpecific;
	
	return (pDrvIf_Spec->Priv.ROM_NO[index]);
}

// ------------------------------------------------------------------------------------------------------
// PRIVATE
// ------------------------------------------------------------------------------------------------------
void OneWire_GetFullROM(struct _drv_If *pDrvIf, uint8_t *firstIndex) 
{
	drv_OneWire_If_Spec_t* pDrvIf_Spec = (drv_OneWire_If_Spec_t*) pDrvIf->pDrvSpecific;

	uint8_t i;
	for (i = 0; i < 8; i++) 
	{
		*(firstIndex + i) = pDrvIf_Spec->Priv.ROM_NO[i];
	}
}


// ------------------------------------------------------------------------------------------------------
// USR
// Inicialization of the OneWire-u
// Remark! Set Signal Active to Log. 1 !!!
// ------------------------------------------------------------------------------------------------------
bool OneWire_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;
	
	drv_OneWire_If_Spec_t* pDrvIf_Spec = (drv_OneWire_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_ONEWIRE) && (CONF_DEBUG_DRV_ONEWIRE == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> ParentMod: %s", pRequestorModIf->Name);
#endif		

	// initialization code here:
	res = CHAL_Signal_Init(pDrvIf_Spec->sigComm);										// Configure IO pin

	if(res == true) OneWire_Reset(pDrvIf);
	
	pDrvIf->Sys.Stat.Initialized = res;
	
#if defined(CONF_DEBUG_DRV_ONEWIRE) && (CONF_DEBUG_DRV_ONEWIRE == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif		
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// USR
// ENABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
// ------------------------------------------------------------------------------------------------------
bool OneWire_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
#if defined(CONF_DEBUG_DRV_ONEWIRE) && (CONF_DEBUG_DRV_ONEWIRE == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
#endif

	// main code here:
	pDrvIf->Sys.Stat.Enabled = true;												// enable was succesfull
	res = true;
#if defined(CONF_DEBUG_DRV_ONEWIRE) && (CONF_DEBUG_DRV_ONEWIRE == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// USR
// DISABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
// ------------------------------------------------------------------------------------------------------
bool OneWire_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;

#if defined(CONF_DEBUG_DRV_ONEWIRE) && (CONF_DEBUG_DRV_ONEWIRE == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
#endif

	// main code here:
	pDrvIf->Sys.Stat.Enabled = false;												// disable was succesfull
	res = true;
	
#if defined(CONF_DEBUG_DRV_ONEWIRE) && (CONF_DEBUG_DRV_ONEWIRE == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// USR
// pbuff - hodnota
// BuffByteLen - pocet znakov na prenesenie (uint8_t)
// ------------------------------------------------------------------------------------------------------
uint32_t OneWire_Write_Data( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t WrLen, _XFerType_t XferType)
{
//	uint32_t wcount = 0;
	uint8_t Byte;
	uint32_t Result = 0;
	
#if defined(CONF_DEBUG_DRV_ONEWIRE) && (CONF_DEBUG_DRV_ONEWIRE == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pSrc: %p, uffByteLen: %d, XFerType: 0x%.1X", pbuff, WrLen, XferType);
#endif	

	// Zapis data
	do
	{
		
		//memcpy(&Byte, &pbuff + wcount, 1);										// toto prepis do normalneho pointroveho tvaru - teraz nevladzem....
		Result += sys_Rd_Buffer(&Byte, pbuff, 1);									// copy data to Byte from pbuff
		OneWire_Write_Byte(pDrvIf, Byte);
		
	}while(--WrLen);
	
#if defined(CONF_DEBUG_DRV_ONEWIRE) && (CONF_DEBUG_DRV_ONEWIRE == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
	FN_DEBUG_FMTPRINT("Xfer: %d", Result);
#endif	
	return(Result);
}

// ------------------------------------------------------------------------------------------------
// Citanie dat z OneWire zbernice
// ak maxlen=0 a pbuff=NULL, nastava flush receive buffera - znuluje prijem.
// ------------------------------------------------------------------------------------------------------
uint32_t OneWire_Read_Data(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t RdLen, _XFerType_t XferType)
{
	uint32_t Result = 0;
	uint8_t	Byte;
	
#if defined(CONF_DEBUG_DRV_ONEWIRE) && (CONF_DEBUG_DRV_ONEWIRE == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pDst: %p, MaxLen: %d, XFerType: 0x%.1X", pbuff, RdLen, XferType);
#endif	

	do
	{
		Byte = OneWire_Read_Byte(pDrvIf);
		//*((uint8_t*) &pbuff + Result) = Byte;
		Result += sys_Wr_Buffer(pbuff, &Byte, 1);									// copy byte to buffer
		
	}while(--RdLen);
	
#if defined(CONF_DEBUG_DRV_ONEWIRE) && (CONF_DEBUG_DRV_ONEWIRE == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
	FN_DEBUG_FMTPRINT("Xfer: %d", Result);
#endif		
	
	return(Result);
}



// ------------------------------------------------------------------------------------------------------
// PRIVATE
// Create device - allocate memory and return handle. Set DevID also.
// ------------------------------------------------------------------------------------------------------
OneWire_DevData_t* OneWire_CreateDevice(struct _drv_If *pDrvIf, uint8_t DevID)
{
	OneWire_DevData_t*	res;
	
	
	res = sys_malloc(sizeof(OneWire_DevData_t));									// allocate piece of memory
	if (res != NULL) res->DeviceID = DevID;
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// PRIVATE
// Destroy device - deallocate memory.
// ------------------------------------------------------------------------------------------------------
void OneWire_DestroyDevice(struct _drv_If *pDrvIf, OneWire_DevData_t *pDev)
{
	if(pDev) sys_free(pDev);					// clear memory
}

// ******************************************************************************************************
// Extended functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// EXT
// scan 1-wire for connected devices. MaxDevs is number of scanned devices.
// return is number of devices found
// ------------------------------------------------------------------------------------------------------
uint8_t OneWire_Scan (struct _drv_If *pDrvIf, uint8_t MaxDevs)
{
	drv_OneWire_If_Spec_t* pDrvIf_Spec = (drv_OneWire_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie
	OneWire_DevData_t *DeviceFound;
	_ColListRecord_t	*tmpRecord;
	uint8_t res, crc;
	uint32_t Result;

#if defined(CONF_DEBUG_DRV_ONEWIRE) && (CONF_DEBUG_DRV_ONEWIRE == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> MaxDevices: %d", MaxDevs);
#endif	
	
	tmpRecord = NULL;
	do
	{
		tmpRecord = sys_DynCol_Get_Record(&pDrvIf_Spec->Dev_List, tmpRecord);		// return pointers to old devices. Needs free memory before scan new ones.
		if(tmpRecord == NULL) break;
		if(tmpRecord->pValue != NULL) OneWire_DestroyDevice(pDrvIf, tmpRecord->pValue);
	}while(DeviceFound);
		
	sys_Clear_DynCol(&pDrvIf_Spec->Dev_List);										// Now, we can clear old device list
	
	res = OneWire_Get_First(pDrvIf);
    Result = 0;
	
	while (res) 																	// repeat while evice found
	{
		DeviceFound = OneWire_CreateDevice(pDrvIf, Result);							// create Device handle
		if(DeviceFound != NULL)
		{
			OneWire_GetFullROM(pDrvIf, &DeviceFound->ROM_NO[0]);					// Read ROM ID	
			crc = TM_OneWire_CRC8(&DeviceFound->ROM_NO[0], 8);
			if (crc == DeviceFound->ROM_NO[7]) DeviceFound->CRC_Check = true;		// check CRC
			else DeviceFound->CRC_Check = false;
				
			res = sys_DynCol_Add_Item (&pDrvIf_Spec->Dev_List, DeviceFound);				// Add device to list
    	}
		res = OneWire_Next(pDrvIf);
		if(Result++ > MaxDevs) break;												// increase number of connected devices
	}

	FN_DEBUG_FMTPRINT("Xfer: %d", Result);	
	return(Result);
}  



// ------------------------------------------------------------------------------------------------------
// EXT
// Read Device memory and return ptr to 8 byte array
// ------------------------------------------------------------------------------------------------------
void* OneWire_GetDeviceData ( struct _drv_If *pDrvIf, uint8_t DeviceNum)
{
	drv_OneWire_If_Spec_t* pDrvIf_Spec = (drv_OneWire_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

	_ColListRecord_t *tmpListRecord = NULL;
	do
	{
		tmpListRecord = sys_DynCol_Get_Record(&pDrvIf_Spec->Dev_List, tmpListRecord);	// temporary pointer to parent module list and get next to tmpListRecord
		if (tmpListRecord == NULL) break;
		if(tmpListRecord->pValue != NULL)											// pValue isn't NULL?
		{
			OneWire_DevData_t *DevData = tmpListRecord->pValue;
			if(DevData->DeviceID == DeviceNum) return (&(DevData->ROM_NO[0]));
		}
	}while(tmpListRecord);
	return(NULL);
}


// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// no interrupts in this driver

// ******************************************************************************************************
// EVENTS Functions
// ******************************************************************************************************

// no events in this driver



#endif	// USE_DRV_ONEWIRE
