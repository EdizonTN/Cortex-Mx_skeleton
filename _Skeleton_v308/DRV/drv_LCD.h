// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_LCD.h
// 	   Version: 3.01
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for LCD - header file
// ******************************************************************************
// Info: LCD driver. 
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
// 

#if !defined(__DRV_LCD_H_) && (defined (_CHIP_LCD_COUNT) && _CHIP_LCD_COUNT > 0)
#define __DRV_LCD_H_

#include "Skeleton.h"

// ******************************************************************************************************
// CONFIGURATION
// ******************************************************************************************************
#ifndef 	CONF_DEBUG_DRV_LCD
 #define 	CONF_DEBUG_DRV_LCD				0										// default is OFF
#endif 

// ******************************************************************************************************
// PUBLIC Defines
// ******************************************************************************************************

// Extended API functions for this driver:
COMP_PACKED_BEGIN
typedef struct drv_LCD_Ext_API
{
	bool	(*Clear)					( _drv_If_t *pDrvIf);						// Clear display
} drv_LCD_Ext_API_t;
COMP_PACKED_END

// Special configuration structure for this driver here:
COMP_PACKED_BEGIN
const typedef struct LCD_Config
{
	uint32_t							ResX;										// Resolution in pixels
	uint32_t							ResY;										// Resolution in pixels
	uint8_t								BPP;										// Bits per Pixel
} LCD_Config_t;
COMP_PACKED_END

// Additions for Generic driver structure here:
COMP_PACKED_BEGIN
typedef struct drv_LCD_If_Spec
{
			_CHIP_LCD_T					*pPeri;										// pointer for used periphery
	const 	uint8_t						PeriIndex;									// index of used periphery
			LCD_Config_t				Conf;										// Configuration
} drv_LCD_If_Spec_t;
COMP_PACKED_END

// ******************************************************************************************************
// PUBLIC 
// ******************************************************************************************************
extern const _drv_Api_t drv_LCD_API_STD;

// ******************************************************************************************************
// PRIVATE Defines
// ******************************************************************************************************


#endif // __DRV_LCD_H_
