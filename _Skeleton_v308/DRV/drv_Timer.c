// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_Timer.c
// 	   Version: 3.42
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for TIMER/CTIMER/MRT periphary
// ******************************************************************************
// Info: Timer driver - CTimer or Timer
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
//				2024.09.30	- v3.42	- add sys_NULL_Function instead a NULL pointer to a function in API structure
//				2023.12.10	- v3.41 - rename _CHIP_Timerx_IRQ_Handler to _Chip_Timerx_IRQ_Handler
//				2022.11.30	- v3.4 - change system of load
//				2022.11.28	- changed interrupt handler bottom
//				2020.10.20	- store event type
//				2020.06.24	- pAPI_USR deleted!
//				2020.06.16	- while(tmpListRecord) changed
// 

#include "Skeleton.h"

#ifdef __DRV_TIMER_H_																	// Header was loaded?


// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool Timer_Init(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool Timer_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool Timer_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
void Timer_Events( struct _drv_If *pDrvIf, _EventType_t EventType);
void Timer_Handler_Top(_drv_If_t *pDrvIf);											// quick handler
//void Timer_Handler_Bottom(_drv_If_t *pDrvIf);										// process handler


// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************
// struktura a priradenie standardnych API funckii driveru:

// structure and assign to standard API functions:
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_Timer_API_STD = 
{
	.Init			=	Timer_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	Timer_Enable,
	.Disable		=	Timer_Disable,
	.Write_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Read_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Events			=	Timer_Events,
};
#pragma pop
									
const _drv_If_t *Timer_DriverIRQ[_CHIP_TIMER_COUNT];
// ------------------------------------------------------------------------------------------------------
// TIMER INTERRUPT HANDLER
// ------------------------------------------------------------------------------------------------------

#if defined(_CHIP_TIMER_COUNT) && (_CHIP_TIMER_COUNT > 0)
void _Chip_CTIMER0_IRQ_Handler(void)	
{
	Timer_Handler_Top((void *) Timer_DriverIRQ[0]);
	//Timer_Handler_Bottom((void *) Timer_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) Timer_DriverIRQ[0]);
}
#endif

#if defined(_CHIP_TIMER_COUNT) && (_CHIP_TIMER_COUNT > 1)
void _Chip_CTIMER1_IRQ_Handler(void)
{
	Timer_Handler_Top((void *) Timer_DriverIRQ[1]);
	//Timer_Handler_Bottom((void *) Timer_DriverIRQ[1]);
	IRQ_Handler_Bottom((void *) Timer_DriverIRQ[0]);
}
#endif

#if defined(_CHIP_TIMER_COUNT) && (_CHIP_TIMER_COUNT > 2)
void _Chip_CTIMER2_IRQ_Handler(void)
{
	Timer_Handler_Top((void *) Timer_DriverIRQ[2]);
	//Timer_Handler_Bottom((void *) Timer_DriverIRQ[2]);
	IRQ_Handler_Bottom((void *) Timer_DriverIRQ[2]);
}
#endif

#if defined(_CHIP_TIMER_COUNT) && (_CHIP_TIMER_COUNT > 3)
void _Chip_CTIMER3_IRQ_Handler(void)
{
	Timer_Handler_Top((void *) Timer_DriverIRQ[3]);
	//Timer_Handler_Bottom((void *) Timer_DriverIRQ[3]);
	IRQ_Handler_Bottom((void *) Timer_DriverIRQ[3]);
}
#endif

// ------------------------------------------------------------------------------------------------------
// Inicializacia interfejsu
// Force - inicializuj aj ked je nastaveny priznak ze uz je init hotovy
bool Timer_Init(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;
	drv_Timer_If_Spec_t* pDrvIf_Spec = (drv_Timer_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_TIMER) && (CONF_DEBUG_DRV_TIMER == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> ParentMod: %s", pRequestorModIf->Name);
#endif		

	// initialization code here:
	pDrvIf_Spec->pPeri = CHAL_Timer_Init(pDrvIf_Spec->PeriIndex);	
	if(pDrvIf_Spec->pPeri) res = true;
//	if(pDrvIf_Spec->pPeri)															// If OK, enable/disable IRQ from this channel
//		res = CHAL_Timer_Enable_IRQ(pDrvIf_Spec->pPeri, pDrvIf_Spec->Channel, false);// default is disabled interrupt request
	
	pDrvIf->Sys.Stat.Initialized  = res;											// Done !
	Timer_DriverIRQ[pDrvIf_Spec->PeriIndex] = pDrvIf;								// set parameter for interrupt routine
	
//gpioinitexit:	
#if defined(CONF_DEBUG_DRV_TIMER) && (CONF_DEBUG_DRV_TIMER == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif			
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// Generic
// ENABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
bool Timer_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	drv_Timer_If_Spec_t* pDrvIf_Spec = (drv_Timer_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_TIMER) && (CONF_DEBUG_DRV_TIMER == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> chMask: 0x%.8x", ChannelBitMask);						// print arguments
#endif

	// main code here:
	res = CHAL_Timer_Enable(pDrvIf_Spec->pPeri, ChannelBitMask, true);				// enable Timer n
	if(res == true) pDrvIf->Sys.Stat.Enabled = true;								// enable was succesfull
	if(res == true) CHAL_Timer_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, true);			// enable Timer in NVIC
		
#if defined(CONF_DEBUG_DRV_TIMER) && (CONF_DEBUG_DRV_TIMER == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// Generic
// DISABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
bool Timer_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	drv_Timer_If_Spec_t* pDrvIf_Spec = (drv_Timer_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_TIMER) && (CONF_DEBUG_DRV_TIMER == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> chMask: 0x%.8x", ChannelBitMask);							// print arguments
#endif

	// main code here:
	res = CHAL_Timer_Enable(pDrvIf_Spec->pPeri, ChannelBitMask, false); 			// enable Timer n
	
	if(res == true) pDrvIf->Sys.Stat.Enabled = false;										// enable was succesfull
	if(res == true) CHAL_Timer_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, true);					// enable Timer in NVIC
		
#if defined(CONF_DEBUG_DRV_TIMER) && (CONF_DEBUG_DRV_TIMER == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************


// ------------------------------------------------------------------------------------------------------
// Quick handler
inline void Timer_Handler_Top(_drv_If_t *pDrvIf)									// Quick Handler  - Overrun irq
{
	if(pDrvIf == NULL) return;
	drv_Timer_If_Spec_t* pDrvIf_Spec = (drv_Timer_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

	if(( CHAL_Timer_Get_CHIRQ_Status( pDrvIf_Spec->pPeri ) & (1 << pDrvIf_Spec->Channel) ) == 0) return;	// not my channel
	
	if(pDrvIf->pIRQ_USR_HandlerTop) pDrvIf->pIRQ_USR_HandlerTop(pDrvIf);			// override with USR handler
	else
	{
		if(pDrvIf->Sys.Stat.Initialized == false) return;
		if(pDrvIf->Sys.Stat.Loaded == false) return;

		// Generic interrupt routine:
		CHAL_Timer_Enable_IRQ_NVIC(((drv_Timer_If_Spec_t*) pDrvIf->pDrvSpecific)->pPeri, true); // re-enable and clear NVIC flag
	}
	CHAL_Timer_Clear_CHIRQ_Status(((drv_Timer_If_Spec_t*) pDrvIf->pDrvSpecific)->pPeri, CHAL_Timer_Get_CHIRQ_Status(((drv_Timer_If_Spec_t*) pDrvIf->pDrvSpecific)->pPeri));
}

//// ------------------------------------------------------------------------------------------------------
//// After handler
//// fire event system
//inline void Timer_Handler_Bottom(_drv_If_t *pDrvIf)									// Process Handler  - Overrun irq
//{
//	if(pDrvIf == NULL) return;
//	drv_Timer_If_Spec_t* pDrvIf_Spec = (drv_Timer_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie
//	if(( CHAL_Timer_Get_CHIRQ_Status( pDrvIf_Spec->pPeri ) & (1 << pDrvIf_Spec->Channel) ) == 0) return;	// not my channel
//	
//	if(pDrvIf->pIRQ_USR_HandlerBottom) pDrvIf->pIRQ_USR_HandlerBottom(pDrvIf);		// override with USR handler
//	else
//	{
//		if(pDrvIf->Sys.Stat.Initialized == false) return;
//		if(pDrvIf->Sys.Stat.Loaded == false) return;

//		pDrvIf->Sys.Stat.LastEvent = EV_StateChanged;								// store event tye
//		if(pDrvIf->pAPI_STD->Events) pDrvIf->pAPI_STD->Events( pDrvIf, EV_StateChanged);	// if is set STD event system, call it

//	}
//	
//	CHAL_Timer_Clear_CHIRQ_Status(((drv_Timer_If_Spec_t*) pDrvIf->pDrvSpecific)->pPeri, CHAL_Timer_Get_CHIRQ_Status(((drv_Timer_If_Spec_t*) pDrvIf->pDrvSpecific)->pPeri));
//}




// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************
// ------------------------------------------------------------------------------------------------
// Event system - volane v preruseni !!!
inline void Timer_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{
//#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
//	SYS_ASSERT( pDrvIf != NULL);													// check
//#endif
//	
//	void* pCallBack = NULL;		
//	_ColListRecord_t *tmpListRecord = pDrvIf->Sys.ParentModLinked.pFirst;			// temporary pointer to parent module list
//	while(tmpListRecord)
//	{
//		switch(EventType)
//		{
//			case EV_StateChanged:
//			{
////				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_ChangedState;
//				break;
//			}
//			case EV_DataReceived:
//			{
////				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_DataReceived;
//				break;
//			}
//			case EV_Transmitted:
//			{
////				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_DataTransmitted;
//				break;
//			}
//			default: break;
//		}
//		
//		if(pCallBack)																// callback is set?
//		{
//			sys_Task_Create_Item( pDrvIf->Name, pCallBack, (sys_TaskArgs_t) {NULL, 0});	// create task for data read
//		}

//		if(tmpListRecord->pNext == NULL) break;										// loop while have associadet module
//		tmpListRecord = tmpListRecord->pNext;										// goto next associated module
//	}
}

#endif	// ifdef __DRV_TIMER_H_
