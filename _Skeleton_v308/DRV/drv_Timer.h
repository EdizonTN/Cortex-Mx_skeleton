// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_TIMER.h
// 	   Version: 3.1
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Driver for TIMER/CTIMER/MRT periphary - header file
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//				2021.07.22	- Rewrite cmplette driver, Added CTIMER/MRT also


#if !defined(__DRV_TIMER_H_) 														// prevent to cyclyc loads
 #if defined(USE_DRV_TIMER) && (USE_DRV_TIMER == 1)									// Need to load?
  #if (!defined (_CHIP_TIMER_COUNT) || _CHIP_TIMER_COUNT == 0)			\
	&& (!defined (_CHIP_CTIMER_COUNT) || (_CHIP_CTIMER_COUNT == 0)) 	\
	&& (!defined (_CHIP_MRT_COUNT) || (_CHIP_MRT_COUNT == 0))						// Neeed it, but MCU haven't this type of periphery!
  
	#error This driver is need to be used, but MCU doesn't have it!
  #else
	// now -> load it.
#define __DRV_TIMER_H_

#include "Skeleton.h"

// ******************************************************************************************************
// CONFIGURATION
// ******************************************************************************************************
#ifndef 	CONF_DEBUG_DRV_TIMER
 #define 	CONF_DEBUG_DRV_TIMER			0											// default is OFF
#endif 

// ******************************************************************************************************
// PUBLIC Defines
// ******************************************************************************************************

// ku generic strukture treba este pridat:
COMP_PACKED_BEGIN
typedef struct drv_Timer_If_Spec
{
			void						*pPeri;										// pointer for used periphery
	const 	uint8_t						PeriIndex;									// index of used periphery
	const	uint8_t						Channel;									// Channel
			uint8_t						ReloadChannel;								// if PWM, this channel are used for reset counting
	const 	CHAL_Signal_t				*pMAT_OUT;									// Physicall pin for Match Out
	const 	CHAL_Signal_t				*pCAP_IN;									// Physicall pin for Capture In

} drv_Timer_If_Spec_t;
COMP_PACKED_END

// ******************************************************************************************************
// PUBLIC 
// ******************************************************************************************************
extern const _drv_Api_t drv_Timer_API_STD;

// ******************************************************************************************************
// PRIVATE Defines
// ******************************************************************************************************


#endif		// defined (_CHIP_zzz_COUNT)
#endif		// #if defined(USE_DRV_zzz)
#endif		// __DRV_ZZZ_H_
