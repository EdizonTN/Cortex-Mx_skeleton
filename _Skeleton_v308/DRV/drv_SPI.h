// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_SPI.h
// 	   Version: 4.1
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for SPI - header file
// ******************************************************************************
// Usage:
// 
// ToDo:
//
// Changelog:
//				2021.06.16	- Rewrite cmplette driver
// 

#if !defined(__DRV_SPI_H_) 															// prevent to cyclyc loads
 #if defined(USE_DRV_SPI) && (USE_DRV_SPI == 1)										// Need to load?
  #if (!defined (_CHIP_SPI_COUNT) || _CHIP_SPI_COUNT == 0)							// Neeed it, but MCU haven't this type of periphery!
	#error This driver is need to be used, but MCU doesn't have it!
  #else
	// now -> load it.
#define __DRV_SPI_H_

#include "Skeleton.h"

// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************

#ifndef CONF_DEBUG_DRV_SPI
#define CONF_DEBUG_DRV_SPI				0											// default je vypnuty
#endif	

#ifndef SPI_USE_IRQ_MANAGER 
#define SPI_USE_IRQ_MANAGER 			0											// If IRQ Manager is used, SPI can be leaved from IRQ Managaer setting to "0"
#endif	

//#ifndef CONF_DRV_SPI_ARG_OPCODE_SIZEMAX
//#define CONF_DRV_SPI_ARG_OPCODE_SIZEMAX	4											// size of the OpCoe SPI device - maximal byte size. Change it only if memory needs!
//#endif

//#ifndef CONF_DRV_SPI_ARG_ADDR_SIZEMAX
//#define CONF_DRV_SPI_ARG_ADDR_SIZEMAX	4											// size of the Addr SPI device - maximal byte size. Change it only if memory needs!
//#endif

#define CONF_DRV_SPI_SPEED_KHZ_STANDARD	10											// default speed rate
#define CONF_DRV_SPI_TIMEOUTCLK_XFERBLOCK	100										// clock pulses for timeout in Blocking transfer
#define CONF_DRV_SPI_SPEED_TOL			10											// speed tolerance in percentage for Set_Bus_Speed fn
#define CONF_DRV_SPI_CPHA				0											// set comm mode [0/1]
#define CONF_DRV_SPI_CPOL				0											// set comm mode [0/1]
#ifndef CONF_DRV_SPI_BUFFERSIZE
 #define CONF_DRV_SPI_BUFFERSIZE		10											// buffer size
#endif	

#ifndef CONF_DRV_SPI_BUFFERELEMENTSIZE
 #define CONF_DRV_SPI_BUFFERELEMENTSIZE 1											// buffer element size in char
#endif	

#define	CONF_DRV_SPI_DUMMYDATA			0x00000000									// dummy data to send if valid data not available
// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************

typedef struct SPI_Controller
{
	uint32_t							Speed_Hz; 									// SPI Speed in kHz
	CHAL_SPI_XferMode_t					Rel_Mode;									// SPI Relations Mode on the bus - Master, Slave or Monitor
	uint8_t								RxFIFO_Max;									// maximal size for Rx FIFO. Is updated during Init. If FIFO doesnt exist, is set to 1
	uint8_t								TxFIFO_Max;									// maximal size for Tx FIFO. Is updated during Init. If FIFO doesnt exist, is set to 1
} SPI_Controller_t;


COMP_PACKED_BEGIN
typedef struct 
{
	uint32_t							MaxSpeed_kHz;								// SPI Speed in kHz
	CHAL_SPI_XferMode_t					Rel_Mode;									// SPI Mode Realtions on the bus - Master, Slave or Monitor
	bool								ReverseBits;								// Flag - Reverse byte order (LSB first). In default is MSB first
//	uint8_t								DevOpcodeByteSize;							// Device - Valid bytes in Opcode field
//	uint8_t								DevDstAddrByteSize;							// Device - Valid bytes in Address field
	uint8_t								DevDataBitSize;								// transfered data size in bits
	uint8_t								SPI_Mode;									// Used SPI mode 0 - 3
	uint8_t 							Dev_SSEL_ID;								// SSel ID
	sys_Buffer_t						*pTxBuff;										// Pointer to Tx Buffer
	sys_Buffer_t						*pRxBuff;										// Pointer to Rx Buffer
//	uint32_t							DevOpcode;									// Trensferred Device OpCode
//	uint32_t							DevDstAddr;									// Device Address array
//	bool								DevDstAddrSend;								// Flag - send address fields also?
	
}SPI_Device_t;
COMP_PACKED_END


COMP_PACKED_BEGIN
typedef struct
{
	sys_Buffer_t 						*pTxBuff;									// Pointer to array of bytes to be transmitted
	sys_Buffer_t						*pRxBuff;									// Pointer memory where bytes received from peri. will be stored
	uint16_t 							RxBuffSize;									// Number of bytes for received bytes, if 0 only transmission we be carried on. After received, value is decrement
	uint8_t								DevDataBitSize;								// transfered data size in bits
	uint8_t 							Dev_SSEL_ID;								// current transfer device address using SSEL0-3 pins
	const CHAL_Signal_t 				*SSel;										// select signal
	volatile uint32_t					TxDataEstimated;							// Estimated data for send	
	uint32_t							Timeout_Countdown;							// counting up to preset value.
	uint32_t							Timeout_Preset;								// counting up to preset value.
	volatile CHAL_SPI_Xfr_Status_t		Status;										// Status of the current SPI transfer
} SPI_Xfer_t;
COMP_PACKED_END

// ku generic strukture treba este pridat:
COMP_PACKED_BEGIN
typedef struct drv_SPI_If_Spec
{
			_CHIP_SPI_T					*pPeri;										// genric pointer to periphery
	const 	uint8_t						PeriIndex;									// index pouzitej periferie
			SPI_Xfer_t					XFer_Rec;									// Xfer packet
			SPI_Device_t				*pXFer_Dev;									// scr/dst device for transfer
			
	const	CHAL_Signal_t				*pSCK;										// fyzicky Port SCK
	const	CHAL_Signal_t				*pMOSI;										// fyzicky Port MOSI
	const	CHAL_Signal_t				*pMISO;										// fyzicky Port MISO
	const	CHAL_Signal_t				**pSSEL;									// fyzicky Port SSEL
			SPI_Controller_t			Controller;									// Controller specific struct
} drv_SPI_If_Spec_t;
COMP_PACKED_END


// Used SPI mode:
//	Mode	CPOL	CPHA
//	0		0		0
//	1		0		1
//	2		1		0
//	3		1		1

// CPHA 0 - Clock Phase Rising Edge, 1 - Clock Phase Falling edge
// CPOL 0 - Clock polarity Active High, 1 - Clock polarity Active Low



COMP_PACKED_BEGIN
typedef struct drv_SPI_Ext_API
{
//	uint8_t 							(*Scan) (struct _drv_If *pDrvIf, uint16_t MaxAddr, uint16_t MaxDevs, bool CreateDevs);	// Scan IFace and find connected devices
//	bool 								(*Ping) (struct _drv_If *pDrvIf, uint16_t Dev_HWAddr);									// check device
	bool								(*Set_BusSpeed) (struct _drv_If *pDrvIf, uint32_t NewSpeed_kHz);						// Change BUS CLK Speed
	bool 								(*Create_Device)  (struct _drv_If *pDrvIf, SPI_Device_t* pDevice, uint16_t SSELID);		// create device
	bool 								(*Destroy_Device) (struct _drv_If *pDrvIf, SPI_Device_t* pDevice);						// destroy selected device
} drv_SPI_Ext_API_t;
COMP_PACKED_END

// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
extern const _drv_Api_t drv_SPI_API_STD;
extern void drv_SPI_Handler_Top(_drv_If_t *pDrvIf);
extern void drv_SPI_Handler_Bottom(_drv_If_t *pDrvIf);



#endif		// defined (_CHIP_zzz_COUNT)
#endif		// #if defined(USE_DRV_zzz)
#endif		// __DRV_ZZZ_H_
