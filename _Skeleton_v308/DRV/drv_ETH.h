// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_ETH.h
// 	   Version: 1.0
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for ETH - header file
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
// 

#if !defined(__DRV_ETH_H_) && (defined (_CHIP_HAVE_ETH) && (_CHIP_HAVE_ETH == 1))
#define __DRV_ETH_H_

#include "Skeleton.h"

// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************

#ifndef CONF_DEBUG_DRV_ETH
#define CONF_DEBUG_DRV_ETH				0											// default je vypnuty
#endif	

#ifndef ETH_USE_IRQ_MANAGER 
#define ETH_USE_IRQ_MANAGER 			0											// If IRQ Manager is used, ETH can be leaved from IRQ Managaer setting to "0"
#endif	

// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************
COMP_PACKED_BEGIN
typedef struct
{
	bool								tmp;
	//uint32_t							CurrentSpeed_kHz;							// I2C actually preset of Speed in kHz
	//ETH_SpeedMode						CurrentSpeedMode;							// I2C Nominal Speed in kHz (100k, 400k, 1MHz, 3.2MHz)
} ETH_Dev_Config_t;
COMP_PACKED_END

COMP_PACKED_BEGIN
typedef struct 
{
	bool								tmp;
	//uint8_t							RegAdrSize;									// Device destinastion register/address size in bytes
	//CHAL_I2C_Mode_t					XferMode;									// Device working Mode - Master, Slave or Monitor	
	//ETH_SpeedMode						SpeedMode; 									// I2C Nominal Speed in kHz (100k, 400k, 1MHz, 3.2MHz)	
}ETH_Dev_Parms_t;
COMP_PACKED_END

COMP_PACKED_BEGIN
typedef struct 
{
	uint16_t 							Dev_HWAddr;									// device address
	ETH_Dev_Config_t					Dev_Config;									// device configuration - can be changed
	ETH_Dev_Parms_t						Dev_Parms;									// device parameters - constant
//	uint8_t 							Arg_RegAdr[4];								// Source/Destinastion register/address in Device memory for Xfer	
}ETH_Device_t;
COMP_PACKED_END

const typedef struct ETH_Config
{
	bool								tmp;
//	uint32_t							Speed_kHz; 									// ETH Speed in kHz
//	CHAL_ETH_Mode_t						Mode;										// ETH Mode - Master, Slave or Monitor
//	bool								ReverseData;								// Flag - Reverse byte order (LSB first). In default is MSB first
} ETH_Config_t;

// ku generic strukture treba este pridat:
COMP_PACKED_BEGIN
typedef struct drv_ETH_If_Spec
{
			uint32_t					*pPeri;										// genric pointer to periphery
	const 	uint8_t						PeriIndex;									// index pouzitej periferie

#if defined(CONF_USE_LIB_RINGBUFFER) && (CONF_USE_LIB_RINGBUFFER == 1)	
			lib_RB_If_t					*pRx_Buff;									// RX Ring Buffer for this USB  - must be pointer !!!!!!!!!
			lib_RB_If_t					*pTx_Buff;									// TX Ring Buffer
#else
//    volatile uint8_t					RxBuffer_Wr;								// pointer na posledne zapisane data	
	volatile uint8_t					*pRx_Buff;									// RX Linear Buffer for this USB  - must be pointer 
	volatile uint8_t					*pTx_Buff;									// TX Linear Buffer
	volatile uint32_t					Rx_Count;									// Received char count
			bool						Tx_Active;									// Priznak ze vysielame	
			bool 						Rx_Flag;									// Priznak prijatych dat
#endif	



//			CHAL_ETH_Xfer_t				XFer_Rec;									// Xfer packet
//			uint8_t 					Dev_Address;								// current transfer device address using SSEL0-3 pins
//			uint32_t 					Dev_DstAdr;									// source/destinastion register/address in device memory for Xfer
//			uint8_t						Dev_DstAdrSize;								// source/destinastion register/address size in bytes
	struct  ETH_Config					Conf;										// pointer na konfiguracnu strukturu
//			uint32_t					Timeout_Counter;							// counting up to preset value.
} drv_ETH_If_Spec_t;
COMP_PACKED_END

// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
extern const _drv_Api_t drv_ETH_API_STD;
extern void drv_ETH_Handler_Top(_drv_If_t *pDrvIf);
extern void drv_ETH_Handler_Bottom(_drv_If_t *pDrvIf);

// ************************************************************************************************
// BIT Definition - INTENCLR register
// ************************************************************************************************
//#define TXRDYCLR						(0x01 << 2)									// Writing 1 clears the corresponding bit in the INTENSET register.


#endif		// __DRV_ETH_H_

