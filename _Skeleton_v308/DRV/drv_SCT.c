// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_SCT.c
// 	   Version: 3.42
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for SC Timer
// ******************************************************************************
// Info: SCT driver. 
//
// Notice:
//
// Usage:
//			
// ToDo:
//
// Changelog:
//				2024.09.30	- v3.42	- add sys_NULL_Function instead a NULL pointer to a function in API structure
//									- fixed SCT for using with NO Buffers
//				2023.12.10	- v3.41 - rename _CHIP_SCTx_IRQ_Handler to _Chip_SCTx_IRQ_Handler
//				2022.11.30	- v3.4 - change system of load
//				2022.11.28	- changed interrupt handler bottom
//				2021.07.21	- rewrite to Skeleton v3
// 				2019.11.11	- Read/write - pbuff changet to sys_Buffer_t

#include "Skeleton.h"

#ifdef __DRV_SCT_H_																	// Header was loaded?


// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool SCT_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool SCT_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool SCT_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool SCT_Write_Data ( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t WrLen, _XFerType_t XferType);
void SCT_Events( struct _drv_If *pDrvIf, _EventType_t EventType);
void SCT_Handler_Top(_drv_If_t *pDrvIf);											// quick handler
//void SCT_Handler_Bottom(_drv_If_t *pDrvIf);											// process handler

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************

// structure and assign to standard API functions:
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_SCT_API_STD 		= 
{
 	.Init			=	SCT_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	SCT_Enable,
	.Disable		=	SCT_Disable,
	.Write_Data		=	SCT_Write_Data,
	.Read_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Events			=	SCT_Events,
};
#pragma pop


// ******************************************************************************************************
// Private variables
// ******************************************************************************************************
union parm_t
{
	uint8_t	 _char[4];
	uint16_t _short[2];
	uint32_t _long;
};


const _drv_If_t *SCT_DriverIRQ[_CHIP_SCT_COUNT];													// SCT0 - SCT3

// ------------------------------------------------------------------------------------------------------
// SCT INTERRUPT HANDLER
// ------------------------------------------------------------------------------------------------------
#if defined(_CHIP_SCT_COUNT) && (_CHIP_SCT_COUNT > 0)
void _Chip_SCT0_IRQ_Handler(void)
{
	SCT_Handler_Top((void *) SCT_DriverIRQ[0]);
	//SCT_Handler_Bottom((void *) SCT_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) SCT_DriverIRQ[0]);
}
#endif

#if defined(_CHIP_SCT_COUNT) && (_CHIP_SCT_COUNT > 1)
void _Chip_SCT1_IRQ_Handler(void)
{
	SCT_Handler_Top((void *) SCT_DriverIRQ[1]);
	//SCT_Handler_Bottom((void *) SCT_DriverIRQ[1]);
	IRQ_Handler_Bottom((void *) SCT_DriverIRQ[1]);
}
#endif

#if defined(_CHIP_SCT_COUNT) && (_CHIP_SCT_COUNT > 2)
void _Chip_SCT2_IRQ_Handler(void)
{
	SCT_Handler_Top((void *) SCT_DriverIRQ[2]);
	//SCT_Handler_Bottom((void *) SCT_DriverIRQ[2]);
	IRQ_Handler_Bottom((void *) SCT_DriverIRQ[2]);
}
#endif

#if defined(_CHIP_SCT_COUNT) && (_CHIP_SCT_COUNT > 3)
void _Chip_SCT3_IRQ_Handler(void)
{
	SCT_Handler_Top((void *) SCT_DriverIRQ[3]);
	//SCT_Handler_Bottom((void *) SCT_DriverIRQ[3]);
	IRQ_Handler_Bottom((void *) SCT_DriverIRQ[3]);
}
#endif


// ------------------------------------------------------------------------------------------------------
// vrat hodnotu prescalera podla zadanej frekvencie, pricom hodnota nesmie byt vacsia ako je MaxValueSizeInBytes [2/4]
// UpDn priznak, ze pocitame pocitadlom hore aj dole, takze frekv * 2
// ------------------------------------------------------------------------------------------------------
uint8_t SCT_Get_Pescaler_Value(drv_SCT_If_Spec_t *SCT_If_Spec, uint32_t Freq, bool UpDn)
{
	uint_fast64_t new_val0; 
	uint_fast32_t max_Val; 
	uint8_t Prescaler = 0;
	uint8_t dev;
	
	if (UpDn) dev = 2;
	else dev = 1;
	
	// ToDo: Prepis na CHAL !!
	if (((_CHIP_SCT_T*)SCT_If_Spec->pPeri)->CONFIG & 0x00000001)					// Operate as 1 32-bit counter ?
		max_Val = 0xffffffff;	// vygeneruj maximalnu hodnoty, ktoru moze mat new_val
	else																			// No, 2x 16-bit counters
		max_Val = 0x0000ffff;
	// ------------------------
	
	// ziadali nas o oba registre alebo len H ?
	//if((AccessType == 32_bit) | ( AccessType == Counter_H ))
	//if(!(SCT_If_Spec->uPeri.pSCT->CONFIG | SCT_CONFIG_32BIT_COUNTER))				// bezime ako 2x16-bit Counter? Ak ano, pracujeme aj s H counterom. Inak len s L
	{
		// teraz prepocitam najvhodnejsiu hodnotu pre delicku na zaklade zadanej frekvencie
		//Prescaler.High16 = 0;

		do
		{
			//Prescaler.High16 ++;													// ak budeme mimo, zvys deliaci pomer
			Prescaler ++;															// zaciname jednotkou neee ?
			//new_val0 = _Chip_Clock_GetSystemClockRate();								// daj cip freq
			new_val0 = SystemCoreClock;
			//new_val0 = new_val0 / Prescaler.High16 / Freq;						// pokus sa vypocitat hodnotu 
			new_val0 = new_val0 / Prescaler / Freq / dev;							// pokus sa vypocitat hodnotu 
			//} while((new_val0 > max_Val) & (Prescaler.High16 < 0x0100)); 			// opakuj pokial hodnota pre counter nie je zmysluplna
		} while((new_val0 > max_Val) & (Prescaler < 0x80));; 						// opakuj pokial hodnota pre counter nie je zmysluplna a prescaler rmusi byt mensi ako 7f (uklada sa s -1)

		//Prescaler.High16 --;
	}

	return(Prescaler);																// vrat hodnotu prescalera
}


// ------------------------------------------------------------------------------------------------------
// Prepocita zadanu frekvenciu podla zadaneho Prescalera
// vystupom je reload hodnota pre MATCH registre 
// ------------------------------------------------------------------------------------------------------
uint32_t SCT_Get_MATCH_Value(uint32_t Freq, uint8_t Prescaler, bool BiDir)
{
	uint_fast64_t MainFreq;
	uint32_t	Result;
	uint8_t div=1;
	
	
	if(BiDir) div = 2;																// pocitame hore aj dole
	
//	MainFreq = Chip_Clock_GetSystemClockRate(); 
	MainFreq = SystemCoreClock;
	//ERR_BREAK;		// Chip_Clock_GetSystemClockRate presun toto do SCT_Config !!!!
	Result = MainFreq / Prescaler / Freq / div;										// vypocitaj hodnotu podla prescalwera

	return (Result);																// vrat vysledok
}

// ------------------------------------------------------------------------------------------------------
// Generic
// Inicializacia SCT-u
// ------------------------------------------------------------------------------------------------------
bool SCT_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;

	drv_SCT_If_Spec_t* pDrvIf_Spec = (drv_SCT_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_SCT) && (CONF_DEBUG_DRV_SCT == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> ParentMod: %s", pRequestorModIf->Name);
#endif		

	// initialization code here:
	pDrvIf_Spec->pPeri = CHAL_SCT_Init(pDrvIf_Spec->PeriIndex);
	if(pDrvIf_Spec->pPeri != NULL) res = true;
	
	// IO init
	// Teraz fyzicky prepoj SCT OUT na GPIO Pin ::::::::::::::
	if(pDrvIf_Spec->pSignal_Out != NULL)
	{
		res = CHAL_Signal_Init(pDrvIf_Spec->pSignal_Out);
	}
	
	SCT_DriverIRQ[pDrvIf_Spec->PeriIndex] = pDrvIf;
#if (CONF_SYS_BUFFERS_USED == 1)
	pDrvIf_Spec->pRWBuff = sys_Buffer_Create(CONF_DRV_SCT_RWBUFFERSIZE, 4, sBuffRing);	// Create R/W buffer
#else
	pDrvIf_Spec->pRWBuff = sys_malloc_zero(CONF_DRV_SCT_RWBUFFERSIZE * 4);				// Create R/W buffer
#endif	
			
	if(pDrvIf_Spec->pRWBuff == NULL)													// cannot create buffer ?
	{
#if defined(CONF_DEBUG_DRV_SCT) && (CONF_DEBUG_DRV_SCT == 1)
		FN_DEBUG_FMTPRINT(" >> Not enought memory for create pTxBuff!");
#endif
		res = false;
	}

	pDrvIf->Sys.Stat.Initialized = res;

#if defined(CONF_DEBUG_DRV_SCT) && (CONF_DEBUG_DRV_SCT == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// Generic
// ENABLE function
// If ChannelBitMask == UINT32_MAX, enable/disable whole SCT
// channel in SCT is EVENT.
// ------------------------------------------------------------------------------------------------------
bool SCT_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	drv_SCT_If_Spec_t* pDrvIf_Spec = (drv_SCT_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_SCT) && (CONF_DEBUG_DRV_SCT == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
#endif

	// main code here:
	if(ChannelBitMask == UINT32_MAX)
	{
		res = CHAL_SCT_Enable(pDrvIf_Spec->pPeri, true); 							// enable SCT
	}
	else
	{
		res = CHAL_SCT_Enable(pDrvIf_Spec->pPeri, true); 							// enable SCT, each event cannot control...
	}
	pDrvIf->Sys.Stat.Enabled = res;
	
#if defined(CONF_DEBUG_DRV_SCT) && (CONF_DEBUG_DRV_SCT == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// Generic
// DISABLE
// If ChannelBitMask == UINT32_MAX, enable/disable whole SCT
// channel in SCT is EVENT.
// ------------------------------------------------------------------------------------------------------
bool SCT_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	drv_SCT_If_Spec_t* pDrvIf_Spec = (drv_SCT_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_SCT) && (CONF_DEBUG_DRV_SCT == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
#endif

	// main code here:
	if(ChannelBitMask == UINT32_MAX)
	{
		res = CHAL_SCT_Enable(pDrvIf_Spec->pPeri, false); 							// enable SCT
	}
	else
	{
		res = CHAL_SCT_Enable(pDrvIf_Spec->pPeri, false); 							// enable SCT, each event cannot control...
	}
	pDrvIf->Sys.Stat.Enabled = res;
#if defined(CONF_DEBUG_DRV_SCT) && (CONF_DEBUG_DRV_SCT == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// Generic
// Write data SCT - write desired frequency [Hz] in format:
// When BuffByteLen == 8:
// Buff[0-3] : uint32_t value for U counter
// Buff[4-7] : uint32_t value  - destination SCT channel
// When BuffByteLen == 12:
// Buff[0-3] : uint32_t value for H counter
// Buff[4-7] : uint32_t value for L counter
// Buff[8-11] : uint32_t value  - destination SCT channel
// podla zvoleneho modu pocitadla BiDir, nasobi frekvenciu
// ------------------------------------------------------------------------------------------------------
bool SCT_Write_Data ( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t WrLen, _XFerType_t XferType)
{
	bool res = false;	

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	SYS_ASSERT( pDrvIf != NULL);													// check
#endif

	drv_SCT_If_Spec_t* pDrvIf_Spec = (drv_SCT_If_Spec_t*) pDrvIf->pDrvSpecific;

	
	union parm_t parm1, parm2, parm3;											
	uint8_t prescaler;
	uint32_t Result = 0;
	uint32_t NewMatch;
	bool UpDn = false;
	bool SaveEnable = pDrvIf->Sys.Stat.Enabled;										// save old state
	

#if defined(CONF_DEBUG_DRV_SCT) && (CONF_DEBUG_DRV_SCT == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pDev: %p, pSrc: %p, WriteLen: %d, XFerType: 0x%.1X", pDrvIf_Spec->pXFer_Dev, pbuff, WrLen, XferType);
#endif		

	SCT_Disable( pRequestorModIf, pDrvIf, NULL);
	//res = drvmgr_Disable(pRequestorModIf, pDrvIf, NULL);
	
	if(((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->CTRL & (1 << 4)) UpDn = true;			// pocitadlu hore aj dole
	prescaler = SCT_Get_Pescaler_Value(pDrvIf_Spec, parm1._long, UpDn);
	if (prescaler > 0) res = true;
	
	// Write data here:
	if(res == true)
	{
		switch (WrLen)
		{
			case 8:
#if (CONF_SYS_BUFFERS_USED == 1)		
					sys_Buffer_Read_Element(pbuff, (uint8_t *) &parm1._long, 1); 	// load parameter match value.U into union
					sys_Buffer_Read_Element(pbuff, (uint8_t *) &parm3._long, 1);	// load parameter dest. channel into union
#else
					sys_memcpy( pbuff, (uint8_t *) &parm1._long, 4); 				// load parameter match value.U into union
					sys_memcpy( pbuff, (uint8_t *) &parm3._long, 4);				// load parameter dest. channel into union			
#endif			
			
					if(((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->CTRL & (1 << 4)) UpDn = true;	// pocitadlu hore aj dole
						
					prescaler = SCT_Get_Pescaler_Value(pDrvIf_Spec, parm1._long, UpDn);
					if (prescaler == 0) {res = false; break;}
					NewMatch = SCT_Get_MATCH_Value(parm1._long, prescaler, UpDn);	// calculate new MATCH value from frequency and SCT prescaler
					if(NewMatch > 0)
					{
						// ToDo: Prepis na CHAL !!!
						uint32_t OldHalt =  (((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->CTRL) & (SCT_CTRL_HALT_L_MASK | SCT_CTRL_HALT_H_MASK);	// read Halt flags
						((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->CTRL |= (SCT_CTRL_HALT_L_MASK | SCT_CTRL_HALT_H_MASK);						// stop counting !
						((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->SCTMATCH[parm3._long] = NewMatch;
						((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->SCTMATCHREL[parm3._long] = NewMatch;
						((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->CTRL |= SCT_CTRL_CLRCTR_L_MASK | SCT_CTRL_CLRCTR_H_MASK;										// clear counters,
						((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->CTRL &= ~(OldHalt & (SCT_CTRL_HALT_L_MASK | SCT_CTRL_HALT_H_MASK));			// write old Halt flags
						// --------------------------

					}else res = false;
					break;
					
			case 12:
#if (CONF_SYS_BUFFERS_USED == 1)		
					sys_Buffer_Read_Element(pbuff, (uint8_t *) &parm1._long, 1);	// load parameter match value.H into union
					sys_Buffer_Read_Element(pbuff, (uint8_t *) &parm2._long, 1);	// load parameter match value.L into union			
					sys_Buffer_Read_Element(pbuff, (uint8_t *) &parm3._long, 1);	// load parameter dest. channel into union
#else
					sys_memcpy(pbuff, (uint8_t *) &parm1._long, 4);					// load parameter match value.H into union
					sys_memcpy(pbuff, (uint8_t *) &parm2._long, 4);					// load parameter match value.L into union			
					sys_memcpy(pbuff, (uint8_t *) &parm3._long, 4);					// load parameter dest. channel into union
#endif			
				
					if(((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->CTRL & (1 << 4)) UpDn = true;	// pocitadlu hore aj dole
					prescaler = SCT_Get_Pescaler_Value(pDrvIf_Spec, parm1._long, UpDn);
			
					if (prescaler == 0) {res = false; break;}				
					
					NewMatch = SCT_Get_MATCH_Value(parm1._short[parm3._long], prescaler, UpDn);		// calculate new MATCH High value from frequency and SCT prescaler
					if(NewMatch > 0)
					{
						//ToDo: Prepis na CHAL!!!
						uint32_t OldHalt =  (((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->CTRL) & (SCT_CTRL_HALT_L_MASK | SCT_CTRL_HALT_H_MASK);	// read Halt flags
						((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->CTRL |= (SCT_CTRL_HALT_L_MASK | SCT_CTRL_HALT_H_MASK);						// stop counting !
						((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->SCTMATCH[parm3._long] = (NewMatch & UINT16_MAX) << 16;
						((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->SCTMATCHREL[parm3._long] = (NewMatch & UINT16_MAX) << 16;
						((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->CTRL |= SCT_CTRL_CLRCTR_L_MASK | SCT_CTRL_CLRCTR_H_MASK;					// clear counters,
						((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->CTRL &= ~(OldHalt & (SCT_CTRL_HALT_L_MASK | SCT_CTRL_HALT_H_MASK));			// write old Halt flags
						// -----------------------
					} else res = false;	

					
					if(((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->CTRL & (1 << 4)) UpDn = true;	// pocitadlu hore aj dole
					else UpDn = false;
					
					prescaler = SCT_Get_Pescaler_Value(pDrvIf_Spec, parm2._long, UpDn);
					if (prescaler > 0) {res = false; break;}					
					NewMatch = SCT_Get_MATCH_Value(parm2._short[1], prescaler, UpDn);		// calculate new MATCH Low value from frequency and SCT prescaler
					if(NewMatch > 0)
					{
						//ToDo: Prepis na CHAL !!
						uint32_t OldHalt =  (((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->CTRL) & (SCT_CTRL_HALT_L_MASK | SCT_CTRL_HALT_H_MASK);	// read Halt flags
						((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->CTRL |= (SCT_CTRL_HALT_L_MASK | SCT_CTRL_HALT_H_MASK);						// stop counting !
						((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->SCTMATCH[parm3._long] = NewMatch & UINT16_MAX;
						((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->SCTMATCHREL[parm3._long] = NewMatch & UINT16_MAX;
						((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->CTRL |= SCT_CTRL_CLRCTR_L_MASK | SCT_CTRL_CLRCTR_H_MASK;					// clear counters,
						((_CHIP_SCT_T*)pDrvIf_Spec->pPeri)->CTRL &= ~(OldHalt & (SCT_CTRL_HALT_L_MASK | SCT_CTRL_HALT_H_MASK));			// write old Halt flags
						// ---------------------------
					}else res = false;
					break;
			default: 
					res = false; 
					break;															// wrong parameter format
		}
	}
	if(SaveEnable == true) SCT_Enable( pRequestorModIf, pDrvIf, NULL);				// restore state
	
	// ----------------------------

#if defined(CONF_DEBUG_DRV_SCT) && (CONF_DEBUG_DRV_SCT == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT("%s, XFerCount: 0x%.4X", FN_DEBUG_RES_BOOL(res), WrLen - pDrvIf_Spec->XFer_Rec.TxDataEstimated );
#endif		

	return(Result);

}

// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Quick handler
// ------------------------------------------------------------------------------------------------------
void SCT_Handler_Top(_drv_If_t *pDrvIf)
{
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	SYS_ASSERT( pDrvIf != NULL);													// check
#endif
	
	if(pDrvIf->pIRQ_USR_HandlerTop) pDrvIf->pIRQ_USR_HandlerTop(pDrvIf);			// override with USR handler
	else
	{
		if(pDrvIf->Sys.Stat.Initialized == false) return;
		if(pDrvIf->Sys.Stat.Loaded == false) return;

		// Generic interrupt routine:
	}
	
}

// ------------------------------------------------------------------------------------------------------
// After handler
// fire event system
// ------------------------------------------------------------------------------------------------------
//inline void SCT_Handler_Bottom(_drv_If_t *pDrvIf)
//{
//#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
//	SYS_ASSERT( pDrvIf != NULL);													// check
//#endif
//	
//	if(pDrvIf->pIRQ_USR_HandlerBottom) pDrvIf->pIRQ_USR_HandlerBottom(pDrvIf);		// override with USR handler
//	else
//	{
//		if(pDrvIf->Sys.Stat.Initialized == false) return;
//		if(pDrvIf->Sys.Stat.Loaded == false) return;
//		
//		// Generic interrupt routine:
//		if(pDrvIf->pAPI_STD->Events) pDrvIf->pAPI_STD->Events( pDrvIf, EV_StateChanged);	// ak je nastaveny event system, zavolaj ho
//	}
//}






// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************
inline void SCT_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{
//#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
//	SYS_ASSERT( pDrvIf != NULL);													// check
//#endif
	
//	void* pCallBack = NULL;		
//	_ColListRecord_t *tmpListRecord = pDrvIf->Sys.ParentModLinked.pFirst;			// temporary pointer to parent module list
//	while(tmpListRecord)
//	{
//		switch(EventType)
//		{
//			case EV_StateChanged:
//			{
//				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_ChangedState;
//				break;
//			}
////			case EV_DataReceived:
////			{
////				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_DataReceived;
////				break;
////			}
////			case EV_Transmitted:
////			{
////				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_DataTransmitted;
////				break;
////			}
//			default: break;
//		}
//		
//		if(pCallBack)																// callback is set?
//		{
//			sys_Task_Create_Item( pDrvIf->Name, pCallBack, (sys_TaskArgs_t) {NULL, 0});	// create task for data read
//		}

//		if(tmpListRecord->pNext == NULL) break;										// loop while have associadet module
//		tmpListRecord = tmpListRecord->pNext;										// goto next associated module
//	}
}



#endif	// ifdef __DRV_SCT_H_
