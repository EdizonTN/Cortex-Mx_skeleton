// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_LED.c
// 	   Version: 3.01
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for LED
// ******************************************************************************
// Info: LED driver (GPIO Output). Dimming and blinking possibilities
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
//				2024.09.30	- v3.01 - add sys_NULL_Function instead a NULL pointer to a function in API structure
// 				2022.10.21	- v3.00	- Read/write - pbuff changed to sys_Buffer_t, Upgraded to v3


#include "Skeleton.h"

#if defined(USE_DRV_LED) && (USE_DRV_LED == 1)


// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool LED_Init(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
void LED_Change( struct _drv_If *pDrvIf, uint8_t NewValue);							// write Value to LED
void LED_Toggle( struct _drv_If *pDrvIf);

// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************

// structure and assign to extended API functions:
const drv_LED_Ext_API_t		LED_Ext_API_STD 	=  									// Extended (non-standard) API's for this driver
{
	.Change		=	LED_Change,														// Easy Write function
	.Toggle		=	LED_Toggle														// Toggle function
};

// structure and assign to standard API functions:
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 	drv_LED_API_STD =
{
	.Init			=	LED_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Disable		=	LCD_Disable,
	.Write_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Read_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Events			=	LCD_Events,
	.Ext			=	(_drv_Api_t *) &LED_Ext_API_STD							// link to ext structure	
};
#pragma pop

									

// ------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------


// ------------------------------------------------------------------------------------------------------
// Interface initialization
bool LED_Init(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;
	drv_LED_If_Spec_t* pDrvIf_Spec = (drv_LED_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_LED) && (CONF_DEBUG_DRV_LED == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> ParentMod: %s", pRequestorModIf->Name);
#endif		

	// initialization code here:
	res = CHAL_Signal_Init (pDrvIf_Spec->pSigIO);									// inicializuj pin
	if( res == false) goto led_exit;												// Init bad - return

	pDrvIf->Sys.Stat.Initialized  = true;											// Done !
	LED_Change (pDrvIf, 0);				// LED_Change Off after Init
	
led_exit:	
#if defined(CONF_DEBUG_DRV_LED) && (CONF_DEBUG_DRV_LED == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_RESULT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif			
	return(res);
}



// ------------------------------------------------------------------------------------------------------
// EXT
// Quick and easy write new value to the LED, without use a sys_Buffer_t
// ------------------------------------------------------------------------------------------------------
void LED_Change( struct _drv_If *pDrvIf, uint8_t NewValue)
{
	drv_LED_If_Spec_t* pDrvIf_Spec = (drv_LED_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

	if(NewValue) CHAL_Signal_Set_Pin( pDrvIf_Spec->pSigIO->Std);
	else CHAL_Signal_Clear_Pin( pDrvIf_Spec->pSigIO->Std);
}

// ------------------------------------------------------------------------------------------------------
// EXT
// Quick and easy write toggle LED state, without use a sys_Buffer_t
// ------------------------------------------------------------------------------------------------------
void LED_Toggle( struct _drv_If *pDrvIf)
{
	drv_LED_If_Spec_t* pDrvIf_Spec = (drv_LED_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

	CHAL_Signal_Toggle_Pin( pDrvIf_Spec->pSigIO->Std);
}


#endif	// USE_DRV_LED
