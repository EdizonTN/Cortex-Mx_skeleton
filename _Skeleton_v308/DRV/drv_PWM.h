// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_PWM.h
// 	   Version: 1.0
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for PWM - header file
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
// 

#if !defined(__DRV_PWM_H_) 															// prevent to cyclyc loads
 #if defined(USE_DRV_PWM) && (USE_DRV_PWM == 1)										// Need to load?
  #if (!defined (_CHIP_PWM_COUNT) || _CHIP_PWM_COUNT == 0)							// Neeed it, but MCU haven't this type of periphery!
	#error This driver is need to be used, but MCU doesn't have it!
  #else
	// now -> load it.
#define __DRV_PWM_H_

#include "Skeleton.h"

// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************

#ifndef CONF_DEBUG_DRV_PWM
#define CONF_DEBUG_DRV_PWM				0											// default is OFF
#endif	

#ifndef PWM_USE_IRQ_MANAGER 
#define PWM_USE_IRQ_MANAGER 			0											// If IRQ Manager is used, PWM can be leaved from IRQ Managaer setting to "0"
#endif	

// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************

												// && (_CHIP_HAVE_PWM == 1)
const typedef struct drv_PWM_Config
{
	uint8_t 	Counter_SizeInBytes;												// pouzivam ako unified ???
} drv_PWM_Config_t;


// ku generic strukture treba este pridat:
COMP_PACKED_BEGIN
typedef struct drv_PWM_If_Spec
{
			void						*pPeri;										// pointer na periferiu	
	const 	uint8_t						PeriIndex;									// index pouzitej periferie	
	const 	CHAL_Signal_t				*pSignal_Out;								// fyzicky Pin vystupu PWM
	const 	uint8_t						PWM_Out;									// cislo vystupu
	const 	uint8_t						EV_Fix_Num;									// ktory event nastavuje OUT
	const 	uint8_t						EV_Var_Num;									// ktory event nastavuje OUT
	const 	uint8_t						EV_DMA_Num;									// ktory event aktivujeDMA prenos
	const 	uint8_t						CNT_Fix_Num;								// ktory MATCH nastavuje sa porovnava s EV_Set
	const 	uint8_t						CNT_Var_Num;								// ktory MATCH nastavuje sa porovnava s EV_Clr
	const 	uint8_t						CNT_DMA_Num;								// ktory MATCH aktivuje novy DMA prenos
//	const 	_If_t						*pDMA_Channel;	
	struct  drv_PWM_Config				Conf;										// pointer na konfiguracnu strukturu
} drv_PWM_If_Spec_t;
COMP_PACKED_END

// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
extern const _drv_Api_t drv_PWM_API_STD;
extern void drv_PWM_Handler_Top(_drv_If_t *pDrvIf);
extern void drv_PWM_Handler_Bottom(_drv_If_t *pDrvIf);

// ************************************************************************************************
// BIT Definition - INTENCLR register
// ************************************************************************************************

#endif		// defined (_CHIP_zzz_COUNT)
#endif		// #if defined(USE_DRV_zzz)
#endif		// __DRV_ZZZ_H_
