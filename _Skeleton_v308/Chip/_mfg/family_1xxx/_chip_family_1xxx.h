// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: _chip_family_1xx.h
// 	   Version: 1.0
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: MCU 1xx Chip description/configuration file. Same for all MCUs of this family.
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//


#ifndef __CHIP_LPC_15XX_H_
#define __CHIP_LPC_15XX_H_

#ifndef LOAD_SCATTER
#if defined     COMP_TYPE_UV
    #include    "LPC15xx.h" 														// Peripheral Access Layer Header File, modified by Keil
#elif defined   COMP_TYPE_ICC
    #include    "LPC15xx.h" 
#elif defined   COMP_TYPE_TSK
    #include    "LPC15xx.h" 
#elif defined   COMP_TYPE_GCC
    #include    "LPC15xx.h" 
#elif defined   COMP_TYPE_XPR
    #include    "LPC15xx.h" 													// Peripheral Access Layer Header File by NXP - LPCOpen
#else
    #error "Unknown Compiler selected!"
#endif
#endif

// ******************************************************************************************************
// PUBLIC Const Values for all LPC15XX MCU's
// ******************************************************************************************************
#define	_CHIP_IRC_FREQUENCY				12000000UL									// Internal Osciltor: 12MHz




// for whole family LPC15xx:
#define  _CHIP_HAVE_GPIO				1											// General purpose IO
#define  _CHIP_HAVE_USB					1											// Universal Serial Bus
#define  _CHIP_HAVE_DMA					1											// Direct Memory Access
#define  _CHIP_HAVE_RTC					1											// Real Time Clock (RTC)
#define  _CHIP_HAVE_RIT					1											// Repetitive Interrupt Timer
#define  _CHIP_HAVE_SCT					1											// State-Configurable Timers
#define  _CHIP_HAVE_SYSTICK				1											// System Tick Timer is an integral part of the Cortex-M3. The System Tick Timer is intended to generate a fixed 10 millisecond interrupt for use by an operating system or other system management software.
#define  _CHIP_HAVE_MRT					1											// Timer - MRT
#define  _CHIP_HAVE_WWDT				1											// Windowed Watchdog Timer (WWDT)
#define  _CHIP_HAVE_QEI					1											// Quadrature Encoder Interface (QEI)
#define  _CHIP_HAVE_UART				1											// Universal asynchronous receiver-transmitter
#define  _CHIP_HAVE_SPI					1											// Serial Peripheral Interface
#define  _CHIP_HAVE_I2C					1											// A typical I2C-bus
#define  _CHIP_HAVE_CAN					1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
#define  _CHIP_HAVE_ADC					1											// Analog-to-Digital Converter (ADC)
#define  _CHIP_HAVE_DAC					1											// Digital-to-Analog Converter (DAC)
#define  _CHIP_HAVE_ACMP				1											// Analog Comparator
#define  _CHIP_HAVE_TEMP				1											// Temperature sensor
#define  _CHIP_HAVE_CRC					1											// Cyclic Redundancy Check (CRC) generator
#define  _CHIP_HAVE_SWMBLOCK			1											// Switch Matrix (SWM)

// Specific parameters for each chip from this series:
#if defined( CONF_CHIP_ID_LPC1517JBD48 )
	// for LPC1517JBD48
    #define  CHIP_ID                    0x00001517                      			// Chip ID wroted in silicone
#elif defined( CONF_CHIP_ID_LPC1517JBD64 )
	// for LPC1517JBD64
    #define  CHIP_ID                    0x00001517                      			// Chip ID wroted in silicone
#elif defined( CONF_CHIP_ID_LPC1518JBD64 )
	// for LPC1518JBD64
    #define  CHIP_ID                    0x00001518                      			// Chip ID wroted in silicone
#elif defined( CONF_CHIP_ID_LPC1518JBD100 )
	// for LPC1518JBD100
    #define  CHIP_ID                    0x00001518                      			// Chip ID wroted in silicone
#elif defined( CONF_CHIP_ID_LPC1519JBD64 )
	// for LPC1519JBD64
    #define  CHIP_ID                    0x00001519                      			// Chip ID wroted in silicone
	#define	 IRAM						0x9000										// size of internal RAM (datasheet value) - 36kB
	#define	 IFLASH						0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 IEEPROM					0x1000										// size of internal EEPROM (datasheet value) - 4kB
	
#elif defined( CONF_CHIP_ID_LPC1519JBD100 )
	// for LPC1519JBD100
    #define  CHIP_ID                    0x00001519                      			// Chip ID wroted in silicone
	#define	 IRAM						0x9000										// size of internal RAM (datasheet value) - 36kB
	#define	 IFLASH						0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 IEEPROM					0x1000										// size of internal EEPROM (datasheet value) - 4kB
	
#elif defined( CONF_CHIP_ID_LPC1547JBD64 )
	// for LPC1547JBD64
    #define  CHIP_ID                    0x00001547                      			// Chip ID wroted in silicone
#elif defined( CONF_CHIP_ID_LPC1548JBD64 )
	// for LPC1548JBD64
    #define  CHIP_ID                    0x00001548                      			// Chip ID wroted in silicone
#elif defined( CONF_CHIP_ID_LPC1548JBD100 )
	// for LPC1548JBD100
    #define  CHIP_ID                    0x00001548                      			// Chip ID wroted in silicone
	
#elif defined( CONF_CHIP_ID_LPC1549JBD48 )
	// for LPC1549JBD48
    #define  CHIP_ID                    0x00001549                      			// Chip ID wroted in silicone
	#define	 IRAM						0x9000										// size of internal RAM (datasheet value) - 36kB
	#define	 IRAMSTART					0x02000000									// Start of RAM	
	#define	 IFLASH						0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 IEEPROM					0x1000										// size of internal EEPROM (datasheet value) - 4kB
	#define	 IEEPROM_START				0x03200000									// internal EEPROM start address
	#define  _CHIP_ADCS					2											// Number of Analog-to-Digital Converters (ADC)
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	
#elif defined( CONF_CHIP_ID_LPC1549JBD64 )
	// for LPC1549JBD64
    #define  CHIP_ID                    0x00001549                     				// Chip ID wroted in silicone
	#define	 IRAM						0x9000										// size of internal RAM (datasheet value) - 36kB
	#define	 IRAMSTART					0x02000000									// Start of RAM	
	#define	 IFLASH						0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 IEEPROM					0x1000										// size of internal EEPROM (datasheet value) - 4kB
	#define	 IEEPROM_START				0x03200000									// internal EEPROM start address
	#define  _CHIP_ADCS					2											// Number of Analog-to-Digital Converters (ADC)
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC

#elif defined( CONF_CHIP_ID_LPC1549JBD100 )
	// for LPC1549JBD100
    #define  CHIP_ID                    0x00001549                     				// Chip ID wroted in silicone
	#define	 IRAM						0x9000										// size of internal RAM (datasheet value) - 36kB
	#define	 IRAMSTART					0x02000000									// Start of RAM
	#define	 IFLASH						0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 IEEPROM					0xFC0										// size of internal EEPROM (datasheet value) - 4kB	
	#define	 IEEPROM_START				0x03200000									// internal EEPROM start address
	#define  _CHIP_ADCS					2											// Number of Analog-to-Digital Converters (ADC)
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
#else
	#error "Unknown selected device!"
#endif

	#define	 IRQHANDLER_16				_CHIP_WDT0_IRQ_Handler
	#define	 IRQHANDLER_17				_CHIP_BOD0_IRQ_Handler
	#define	 IRQHANDLER_18				_CHIP_FLASH0_IRQ_Handler
	#define	 IRQHANDLER_19				_CHIP_EEPROM0_IRQ_Handler
	#define	 IRQHANDLER_20				_CHIP_DMA0_IRQ_Handler
	#define	 IRQHANDLER_21				_CHIP_GINT0_IRQ_Handler
	#define	 IRQHANDLER_22				_CHIP_GINT1_IRQ_Handler
	#define	 IRQHANDLER_23				_CHIP_PINT0_IRQ_Handler
	#define	 IRQHANDLER_24				_CHIP_PINT1_IRQ_Handler
	#define	 IRQHANDLER_25				_CHIP_PINT2_IRQ_Handler
	#define	 IRQHANDLER_26				_CHIP_PINT3_IRQ_Handler
	#define	 IRQHANDLER_27				_CHIP_PINT4_IRQ_Handler
	#define	 IRQHANDLER_28				_CHIP_PINT5_IRQ_Handler
	#define  IRQHANDLER_29				_CHIP_PINT6_IRQ_Handler
	#define	 IRQHANDLER_30				_CHIP_PINT7_IRQ_Handler
	#define	 IRQHANDLER_31				_CHIP_RIT0_IRQ_Handler
	#define	 IRQHANDLER_32				_CHIP_SCT0_IRQ_Handler
	#define	 IRQHANDLER_33				_CHIP_SCT1_IRQ_Handler
	#define	 IRQHANDLER_34				_CHIP_SCT2_IRQ_Handler
	#define	 IRQHANDLER_35				_CHIP_SCT3_IRQ_Handler
	#define	 IRQHANDLER_36				_CHIP_MRT0_IRQ_Handler
	#define	 IRQHANDLER_37				_CHIP_UART0_IRQ_Handler
	#define	 IRQHANDLER_38				_CHIP_UART1_IRQ_Handler
	#define	 IRQHANDLER_39				_CHIP_UART2_IRQ_Handler
	#define	 IRQHANDLER_40				_CHIP_I2C0_IRQ_Handler
	#define	 IRQHANDLER_41				_CHIP_SPI0_IRQ_Handler
	#define	 IRQHANDLER_42				_CHIP_SPI1_IRQ_Handler
	#define	 IRQHANDLER_43				_CHIP_CAN0_IRQ_Handler
	#define	 IRQHANDLER_44				_CHIP_USB0_IRQ_Handler
	#define	 IRQHANDLER_45				_CHIP_USB0_FIQ_IRQ_Handler
	#define	 IRQHANDLER_46				_CHIP_USB0_WAKE_IRQ_Handler
	#define	 IRQHANDLER_47				_CHIP_ADC0_SEQA_IRQ_Handler
	#define	 IRQHANDLER_48				_CHIP_ADC0_SEQB_IRQ_Handler
	#define	 IRQHANDLER_49				_CHIP_ADC0_THCMP_IRQ_Handler
	#define	 IRQHANDLER_50				_CHIP_ADC0_OVR_IRQ_Handler
	#define	 IRQHANDLER_51				_CHIP_ADC1_SEQA_IRQ_Handler
	#define	 IRQHANDLER_52				_CHIP_ADC1_SEQB_IRQ_Handler
	#define	 IRQHANDLER_53				_CHIP_ADC1_THCMP_IRQ_Handler
	#define	 IRQHANDLER_54				_CHIP_ADC1_OVR_IRQ_Handler	
	#define	 IRQHANDLER_55				_CHIP_DAC0_IRQ_Handler
	#define	 IRQHANDLER_56				_CHIP_CMP0_IRQ_Handler
	#define	 IRQHANDLER_57				_CHIP_CMP1_IRQ_Handler
	#define	 IRQHANDLER_58				_CHIP_CMP2_IRQ_Handler
	#define	 IRQHANDLER_59				_CHIP_CMP3_IRQ_Handler
	#define	 IRQHANDLER_60				_CHIP_QEI0_IRQ_Handler
	#define	 IRQHANDLER_61				_CHIP_RTC0_ALARM_IRQ_Handler
	#define	 IRQHANDLER_62				_CHIP_RTC0_WAKE_IRQ_Handler

	#define NR_IRQS 					46
	
#ifndef LOAD_SCATTER

// ******************************************************************************************************
// PUBLIC Values for all LPC15XX MCU's
// ******************************************************************************************************
extern uint32_t SystemCoreClock;													// for IAP - System Clock Frequency (Core Clock)

#define 	CORE_M3					1

#define CPU_NONISR_EXCEPTIONS   	(15)											// Cortex M3 exceptions /without SP!/
#define CPU_IRQ_NUMOF 				(47)											// Vendor and family specific external interrupts. See UM10736, p.17, Table 2


// harmonizing names
// Some manufacturers header files, using different peripherial structure names. Now, we harmonize it into one names.
// now used from chip header file:		Harmonization to:
typedef LPC_GPIO_PORT_Type       		_CHIP_GPIO_T;       
typedef LPC_DMA_T	             		_CHIP_DMA_T;             
typedef LPC_USB_Type             		_CHIP_USB_T;             
typedef LPC_CRC_Type             		_CHIP_CRC_T;             
//typedef LPC_SCT_Type            		_CHIP_SCT_T;            
typedef LPC_SCT_T            			_CHIP_SCT_T;
typedef LPC_ADC0_Type            		_CHIP_ADC_T;
typedef LPC_DAC_Type             		_CHIP_DAC_T;
typedef LPC_ACMP_Type            		_CHIP_ACMP_T;
typedef LPC_INMUX_Type           		_CHIP_INMUX_T;           
typedef LPC_RTC_Type             		_CHIP_RTC_T;
typedef LPC_WWDT_Type            		_CHIP_WWDT_T;            
typedef LPC_SWM_Type             		_CHIP_SWM_T;             
typedef LPC_PMU_Type             		_CHIP_PMU_T;             
typedef LPC_USART0_Type          		_CHIP_UART_T;             
typedef LPC_SPI0_Type            		_CHIP_SPI_T;            
typedef LPC_I2C0_Type            		_CHIP_I2C_T;            
typedef LPC_QEI_Type             		_CHIP_QEI_T;             
typedef LPC_SYSCON_Type          		_CHIP_SYSCON_T;          
typedef LPC_MRT_Type             		_CHIP_MRT_T;
typedef LPC_PINT_Type            		_CHIP_PINT_T;            
typedef LPC_GINT0_Type           		_CHIP_GINT_T;           
typedef LPC_RIT_Type             		_CHIP_RIT_T;
typedef LPC_SCTIPU_Type          		_CHIP_SCTIPU_T;          
typedef LPC_FLASHCTRL_Type       		_CHIP_FLASHCON_T;       
typedef LPC_C_CAN0_Type          		_CHIP_CAN_T;          
typedef LPC_IOCON_Type           		_CHIP_IOCON_T; 
typedef SysTick_Type					_CHIP_SYSTICK_T;


//// remap MCU IRQ Handlers name to unifieds:
//#define DMA_IRQHandler					IRQHANDLER_4
//#define UART0_IRQHandler				IRQHANDLER_21
//#define UART1_IRQHandler				IRQHANDLER_22
//#define UART2_IRQHandler				IRQHANDLER_23
//#define USB_IRQ_IRQHandler				IRQHANDLER_28
//#define ADC0_SEQA_IRQHandler			IRQHANDLER_31
//#define ADC0_SEQB_IRQHandler			IRQHANDLER_32
//#define ADC0_THCMP_IRQHandler			IRQHANDLER_33
//#define ADC0_OVR_IRQHandler				IRQHANDLER_34
//#define ADC1_SEQA_IRQHandler			IRQHANDLER_35
//#define ADC1_SEQB_IRQHandler			IRQHANDLER_36
//#define ADC1_THCMP_IRQHandler			IRQHANDLER_37
//#define ADC1_OVR_IRQHandler				IRQHANDLER_38





//	Load peripherial low-level routines and reg/fun declarations:
#include "romapi_15xx.h"															// ROM API declarations and functions
#include "syscon_15xx.h"															// SysCon regs.
#include "clock_15xx.h"																// CLOCK
#include "iocon_15xx.h"																// IOCON
#include "swm_15xx.h"																// SWM
#include "gpio_15xx.h"																// GPIO
#include "iap_15xx.h"																// IAP
#include "uart_15xx.h"																// UART
#include "adc_15xx.h"																// ADC
#include "sct_15xx.h"																// SCT
#include "dma_15xx.h"																// DMA
#include "i2c_15xx.h"																// I2C



// Public functions exactly for this series of MCU
extern bool _Chip_Read_ID(uint32_t *pDst);
extern bool _Chip_SerialNum_Read(uint32_t *pDst);
extern bool _Chip_Read_BootCodeVersion(uint32_t *pDst);
extern void _Chip_SystemInit(void) __attribute__((weak));							// inserted into reset handler
extern void _Chip_Get_ResetSRC(uint32_t *DstStruct);

// ------------------------------------------------------------------------------------------------
// Call System Init - in reset vector!
inline void _Chip_System_Init(void)													// inserted into SystemInit
{
	LPC_SYSCON->SYSAHBCLKCTRL0 |= 1<<1;												// switch power ON for ROM
	LPC_SYSCON->SYSAHBCLKCTRL0 |= 3<<3;												// switch power ON for SRAM1 a SRAM 2 
	LPC_SYSCON->SYSAHBCLKCTRL0 |= 1<<7;												// switch power ON for Flash
}

// ------------------------------------------------------------------------------------------------
// get reset signal source
static inline void _Chip_Read_ResetSource( uint32_t* DstStruct)						// Get reset sources (Chip_RST_SRC)
{
//	*DstStruct = (CHAL_RST_SRC_t) 0;
//	
//	if(LPC_SYSCON->SYSRSTSTAT & (1 << 0))	*DstStruct |= _RST_POR;
//	if(LPC_SYSCON->SYSRSTSTAT & (1 << 1))	*DstStruct |= _RST_EXT;
//	if(LPC_SYSCON->SYSRSTSTAT & (1 << 2))	*DstStruct |= _RST_WDT;
//	if(LPC_SYSCON->SYSRSTSTAT & (1 << 3))	*DstStruct |= _RST_BOD;
//	if(LPC_SYSCON->SYSRSTSTAT & (1 << 4))	*DstStruct |= _RST_SYS;
}

// SWM
extern bool _Chip_SWM_Enable (void);

// EEPROM
extern void *_Chip_EEPROM_Init(uint8_t PeriIndex);
extern bool _Chip_EEPROM_Enable(void *pPeri, bool NewState);
extern bool _Chip_EEPROM_IRQ_Enable(void *pPeri, bool NewState);
extern bool _Chip_EEPROM_Write(void *pPeri, uint32_t EEDst, uint8_t *pInBuff, size_t byteswrt);
extern bool _Chip_EEPROM_Read(void *pPeri, uint32_t EESrc, uint8_t *pOutBuff, size_t bytesrd);

// DMA
extern void *_Chip_DMA_Init(uint8_t PeriIndex);
extern bool _Chip_DMA_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool _Chip_DMA_IRQ_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool _Chip_DMA_Get_CHINTSTATE(void *pPeri, uint32_t ChannelBitMask);
extern bool _Chip_DMA_Clear_CHINTSTATE(void *pPeri, uint32_t ChannelBitMask);
// ------------------------------------------------------------------------------------------------------
// return pointer to periphery DMA(PeriIndex)
inline uint8_t _Chip_DMA_Get_PeriIndex(void *pPeri)
{
	return(0);																		// this MCU have only one DMA
}

// Timer
extern void* _Chip_Timer_Init(uint8_t PeriIndex);
extern bool _Chip_Timer_Enable (void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool _Chip_Timer_Configure (void *pPeri, uint8_t Channel, uint32_t RateHz, bool RepeatMode);

// SCT
// ------------------------------------------------------------------------------------------------------
// Return Index from pointer
static inline uint8_t _Chip_SCT_Get_PeriIndex(void *pPeri)
{
	switch ((intptr_t) pPeri)
	{
		case (intptr_t)LPC_SCT3: return(3);
		case (intptr_t)LPC_SCT2: return(2);
		case (intptr_t)LPC_SCT1: return(1);
		case (intptr_t)LPC_SCT0: return(0);
		default: return(0);
	}
}
extern void* _Chip_SCT_Init(uint8_t PeriIndex);
extern bool _Chip_SCT_Enable(void *pPeri, bool NewState);
extern bool _Chip_SCT_IRQ_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool _Chip_SCT_Configure(void *pPeri);
	
// UART
extern void *_Chip_UART_Init(uint8_t PeriIndex);
extern bool _Chip_UART_Enable(void *pPeri, bool NewState);
extern bool _Chip_UART_IRQ_Enable(void *pPeri, bool NewState);
extern bool _Chip_UART_Set_Baud(void* pPeri, uint32_t NewBaud);
extern bool _Chip_UART_ConfigData(void* pPeri, uint8_t DataLen, uint8_t Parity, uint8_t StopBits);
extern bool _Chip_UART_RS485_ConfigData(void* pPeri, uint8_t Address);
extern uint32_t _Chip_UART_GET_IRQStatus(void *pPeri);
inline uint8_t _Chip_UART_Get_PeriIndex(void *pPeri)							// Return Index from pointer
{
	switch ((intptr_t) pPeri)
	{
		case (intptr_t)LPC_USART2: return(2);
		case (intptr_t)LPC_USART1: return(1);
		case (intptr_t)LPC_USART0: return(0);
		default: return(0);
	}
}

static inline void _Chip_UART_IRQTX_Enable (void *pPeri, bool NewValue)				// Enable/Disable Tx Interrupts
{
	if(NewValue)
	{
		((_CHIP_UART_T*)pPeri)->INTENSET |= UART_INTEN_TXRDY;						// Enable TX interrupt
	}
	else
	{
		((_CHIP_UART_T*)pPeri)->INTENCLR |= UART_INTEN_TXRDY;						// Disable TX interrupt
	}
}

static inline void _Chip_UART_IRQRX_Enable (void *pPeri, bool NewValue)				// Enable/Disable Rx Interrupts 
{
	if(NewValue)
	{
		((_CHIP_UART_T*)pPeri)->INTENSET |= UART_INTEN_RXRDY;						// Enable RX interrupt
	}
	else
	{
		((_CHIP_UART_T*)pPeri)->INTENCLR |= UART_INTEN_RXRDY;						// Disable RX interrupt
	}
}

// ADC
extern void *_Chip_ADC_Init(uint8_t PeriIndex);
static inline uint8_t _Chip_ADC_Get_PeriIndex(void *pPeri)
{
	switch ((intptr_t) pPeri)
	{
		case (intptr_t)LPC_ADC1: return(1);
		case (intptr_t)LPC_ADC0: return(0);
		default: return(0);
	}
}
extern bool _Chip_ADC_Enable(void *pPeri, bool NewState);
extern bool _Chip_ADC_IRQ_Enable(void *pPeri, bool NewState);

// I2C
extern void *_Chip_I2C_Init(uint8_t PeriIndex);									// init I2C by periphary number. Return is pointer to periphery if success
extern bool _Chip_I2C_Enable(void *pPeri, bool NewState);
extern bool _Chip_I2C_IRQ_Enable(void *pPeri, bool NewState);
extern bool _Chip_I2C_Configure(void *pPeri, uint32_t Speed_kHz, bool MasterMode, bool SlaveMode, bool MonitorMode);
extern bool _Chip_I2CM_Xfer(void *pPeri, uint16_t SlaveAddress, const uint8_t *pTxBuff, uint16_t TxLen, uint8_t *pRxBuff, uint16_t RxLen);

// SPI
extern void *_Chip_SPI_Init(uint8_t PeriIndex);									// init SPI by periphary number. Return is pointer to periphery if success
extern bool _Chip_SPI_Enable(void *pPeri, bool NewState);
extern bool _Chip_SPI_IRQ_Enable(void *pPeri, bool NewState);
extern bool _Chip_SPI_Configure(void *pPeri, uint32_t Speed_kHz, bool MasterMode, bool SlaveMode);

//USB
extern void *_Chip_USB_Init(uint8_t PeriIndex);
extern bool _Chip_USB_Configure(void *pPeri, bool DeviceMode, bool HostMode, bool OTGMode);
extern uint32_t _Chip_USB_Get_IRQStatus(void *pPeri);

// GPIO
COMP_PACKED_BEGIN
typedef struct	_chip_IO_Spec														// Additional GPIO structure based on chip specific requirements */
{
	uint16_t							IOCON_Reg;									// IOCON register hodnota. pouziva sa len 16 bit. Inak IOCON je 32 bitovy	 */
	uint8_t								SWM_Reg;									// SWM - movable function/fixed function: vyber cisla registra ASSIGN/FIXED */
	uint8_t								SWM_Pin;									// SWM - movable function/fixed function: Hodnota registra Assign. Ak 0xFF porom pis do FIXED */
}_chip_IO_Spec_t;
COMP_PACKED_END

extern void *_Chip_GPIO_Init ( uint8_t PeriIndex, uint32_t Pin);
extern bool _Chip_GPIO_DeInit ( uint8_t PeriIndex, uint32_t Pin);
extern void *_Chip_GPIO_GINT_Conf(uint8_t Group, uint32_t Port, uint32_t Pin, uint8_t Sens);
extern void *_Chip_GPIO_PINT_Conf(uint8_t PINTSel, uint32_t Port, uint32_t Pin, uint8_t Sens);
extern bool _Chip_GPIO_IRQ_Enable(void *pPeri, uint32_t Port, uint32_t Pin, bool NewState);
extern void _Chip_GPIO_Wr_Conf_Pin (uint32_t Port, uint32_t Pin, uint8_t Reg_Idx, const _chip_IO_Spec_t *Reg_Val);
static inline void *_Chip_Get_GPIO_Ptr(uint8_t PeriIndex)							// vrat pointer na konkretnu GPIO periferiu - LPC15xx pouziva iba jeten ptr
{
    return(LPC_GPIO_PORT);                                                          // LPC15xx have only one port	
}

#define CHIP_PINSWMUNUSED						0xff, 0xff							// PIN not used - write 0xff into PINENABLE - default state
#define CHIP_PINENABLE(x)						x, 0xff								// write value x into PINENABLE
#define CHIP_PINASSIGN(x,y)						x, y								// write value y into PINASSIGN(x)



// CLOCK
extern uint32_t _Chip_Clock_Get_InpClockFreq(void);									// get clock source frequency
extern uint32_t _Chip_Clock_Get_USBClockFreq(void);
#endif	// LOAD_SCATTER
#endif	//__CHIP_LPC_15XX_H_

