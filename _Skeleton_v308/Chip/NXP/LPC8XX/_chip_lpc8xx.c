// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: _chip_lpc8xx.c
// 	   Version: 3.23
//		  Desc: MCU LPC8xx Chip description/configuration file. Same for all MCUs of this family!
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//				2024.09.12	- v3.23 - add read data and valid flag from ADC
//									- each XXX_Init have to disabled NVIC IRQ !!!!!!
//				2024.08.28	- v3.22	- Fix GPIO 0 and 1 initialization
//									- Fix GPIO init only one time
//									- Fix MRT init only one time
//				2024.02.04	- v3.21 - I2C bus speed calculations corrected
//				2024.01.11	- v3.20 - Rename ADC channels functions
//				2023.11.12	- v3.10	- Finalize LPC82x, 83x and 84x serie
//							- rename handler to sys_xxx_handler
//				2023.11.10	-  fix PINENABLE coding
//              2023.09.24  -  _Chip_I2C_Get_PeriIndex() fixed
//				2020.07.08	-  rename Pld: _Chip_UART_IRQRx_Enable ---> _Chip_UART_IRQ_Peri_Enable
//							-  rename Pld: _Chip_UART_IRQTx_Enable ---> _Chip_UART_IRQ_Peri_Enable, add parameter
//				11.06.2019	-  Optimalization

#include "Skeleton.h"

#if defined ( CONF_CHIP_ID_LPC804M101JBD16 ) ||	\
	defined ( CONF_CHIP_ID_LPC804M101JDH20 ) ||	\
	defined ( CONF_CHIP_ID_LPC804M101JDH24 ) ||	\
	defined ( CONF_CHIP_ID_LPC804M111JDH24 ) ||	\
	defined ( CONF_CHIP_ID_LPC804M101JHI33 ) ||	\
	defined ( CONF_CHIP_ID_LPC810M021FN8   ) ||	\
	defined ( CONF_CHIP_ID_LPC811M001JDH16 ) ||	\
	defined ( CONF_CHIP_ID_LPC812M101JDH16 ) ||	\
	defined ( CONF_CHIP_ID_LPC812M101JDH20 ) ||	\
	defined ( CONF_CHIP_ID_LPC812M101JTB16 ) ||	\
	defined ( CONF_CHIP_ID_LPC822M101JHI33 ) ||	\
	defined ( CONF_CHIP_ID_LPC822M101JDH20 ) ||	\
	defined ( CONF_CHIP_ID_LPC824M201JHI33 ) ||	\
	defined ( CONF_CHIP_ID_LPC824M201JDH20 ) ||	\
	defined ( CONF_CHIP_ID_LPC832M101FDH20 ) || \
	defined ( CONF_CHIP_ID_LPC834M101FHI33 ) || \
	defined	( CONF_CHIP_ID_LPC844M201JBD64 ) || \
 	defined	( CONF_CHIP_ID_LPC844M201JBD48 ) || \
 	defined	( CONF_CHIP_ID_LPC844M201JHI48 ) || \
 	defined	( CONF_CHIP_ID_LPC844M201JHI33 ) || \
 	defined	( CONF_CHIP_ID_LPC845M301JBD64 ) || \
 	defined	( CONF_CHIP_ID_LPC845M301JBD48 ) || \
 	defined	( CONF_CHIP_ID_LPC845M301JHI48 ) || \
 	defined	( CONF_CHIP_ID_LPC845M301JHI33 )
	



#if defined (__REDLIB__)
extern void __main(void);
#endif
extern int main(void);


void _Chip_Default_IRQ_Handler(void) 
{
	dbgprint("\r\n.Unhandlered exception! .\r\n");
}

void sys_Reset_Handler					(void) __WEAK_DEFAULT;
void sys_HardFault_Handler				(void) __WEAK_DEFAULT;


void _Chip_SPI0_IRQ_Handler			(void) __WEAK_DEFAULT;
void _Chip_SPI1_IRQ_Handler			(void) __WEAK_DEFAULT;
void _Chip_DAC0_IRQ_Handler			(void) __WEAK_DEFAULT;
void _Chip_UART0_IRQ_Handler		(void) __WEAK_DEFAULT;
void _Chip_UART1_IRQ_Handler		(void) __WEAK_DEFAULT;
void _Chip_UART2_IRQ_Handler		(void) __WEAK_DEFAULT;
void _Chip_I2C3_IRQ_Handler			(void) __WEAK_DEFAULT;
void _Chip_I2C2_IRQ_Handler			(void) __WEAK_DEFAULT;
void _Chip_I2C1_IRQ_Handler			(void) __WEAK_DEFAULT;
void _Chip_I2C0_IRQ_Handler			(void) __WEAK_DEFAULT;
void _Chip_SCT0_IRQ_Handler			(void) __WEAK_DEFAULT;
void _Chip_MRT0_IRQ_Handler			(void) __WEAK_DEFAULT;
void _Chip_CMP0_IRQ_Handler			(void) __WEAK_DEFAULT;
void _Chip_WDT0_IRQ_Handler			(void) __WEAK_DEFAULT;
void _Chip_BOD0_IRQ_Handler			(void) __WEAK_DEFAULT;
void _Chip_FLASH0_IRQ_Handler		(void) __WEAK_DEFAULT;
void _Chip_WKT0_IRQ_Handler			(void) __WEAK_DEFAULT;
void _Chip_ADC0_SEQA_IRQ_Handler	(void) __WEAK_DEFAULT;
void _Chip_ADC0_SEQB_IRQ_Handler	(void) __WEAK_DEFAULT;
void _Chip_ADC0_THCMP_IRQ_Handler	(void) __WEAK_DEFAULT;
void _Chip_ADC0_OVR_IRQ_Handler		(void) __WEAK_DEFAULT;
void _Chip_DMA0_IRQ_Handler			(void) __WEAK_DEFAULT;
void _Chip_CT32B0_IRQ_Handler		(void) __WEAK_DEFAULT;
void _Chip_PINT0_IRQ_Handler		(void) __WEAK_DEFAULT;
void _Chip_PINT1_IRQ_Handler		(void) __WEAK_DEFAULT;
void _Chip_PINT2_IRQ_Handler		(void) __WEAK_DEFAULT;
void _Chip_PINT3_IRQ_Handler		(void) __WEAK_DEFAULT;
void _Chip_PINT4_IRQ_Handler		(void) __WEAK_DEFAULT;
void _Chip_PINT5_IRQ_Handler		(void) __WEAK_DEFAULT;
void _Chip_PINT6_IRQ_Handler		(void) __WEAK_DEFAULT;
void _Chip_UART3_IRQ_Handler		(void) __WEAK_DEFAULT;
void _Chip_PINT7_IRQ_Handler		(void) __WEAK_DEFAULT;
void _Chip_UART4_IRQ_Handler		(void) __WEAK_DEFAULT;


extern const uint32_t *StackInit;

// define Cortex-M base interrupt vectors 
const cortexm_base_t cortex_vector_base __SECTION_IRQVECTORS  = 
{
	CONF_STACK_START+CONF_STACK_SIZE,
    {
        [ 0] = sys_Reset_Handler,													// entry point of the program
        [ 1] = sys_NMI_Handler,														// [-14] non maskable interrupt handler
        [ 2] = sys_HardFault_Handler,												// [-13] hard fault exception
        [ 3] = _Chip_Default_IRQ_Handler,											//					 [-12]
        [ 4] = _Chip_Default_IRQ_Handler,											//					 [-11]
        [ 5] = _Chip_Default_IRQ_Handler,											//					 [-10]
        [ 6] = _Chip_Default_IRQ_Handler,											//
		[ 7] = _Chip_Default_IRQ_Handler,											//
		[ 8] = _Chip_Default_IRQ_Handler,											//
		[ 9] = _Chip_Default_IRQ_Handler,											//
        [10] = sys_SVC_Handler,														// [-5] SW interrupt, in RIOT used for triggering context switches
        [11] = _Chip_Default_IRQ_Handler,											//					
		[12] = _Chip_Default_IRQ_Handler,											//
        [13] = sys_PendSV_Handler,													// [-2] pendSV interrupt, in RIOT use to do the actual context switch
        [14] = sys_SysTick_Handler,													// [-1] SysTick interrupt, not used in RIOT
    },
	{
		[ 0] = IRQHANDLER_16,														// 
		[ 1] = IRQHANDLER_17,														// 
		[ 2] = IRQHANDLER_18,														// 
		[ 3] = IRQHANDLER_19,														// 
		[ 4] = IRQHANDLER_20,														// 
		[ 5] = IRQHANDLER_21,														//
		[ 6] = IRQHANDLER_22,														// 
		[ 7] = IRQHANDLER_23,														// 
		[ 8] = IRQHANDLER_24,														// 
		[ 9] = IRQHANDLER_25,														// 
		[10] = IRQHANDLER_26,														// 
		[11] = IRQHANDLER_27,														// 
		[12] = IRQHANDLER_28,														// 
		[13] = IRQHANDLER_29,														// 
		[14] = IRQHANDLER_30,														// 
		[15] = IRQHANDLER_31,														// 
		[16] = IRQHANDLER_32,														// 
		[17] = IRQHANDLER_33,														// 
		[18] = IRQHANDLER_34,														// 
		[19] = IRQHANDLER_35,														// 
		[20] = IRQHANDLER_36,														// 
		[21] = IRQHANDLER_37,														// 
		[22] = IRQHANDLER_38,														// 
		[23] = IRQHANDLER_39,														// 
		[24] = IRQHANDLER_40,														// 
		[25] = IRQHANDLER_41,														// 
		[26] = IRQHANDLER_42,														// 
		[27] = IRQHANDLER_43,														// 
		[28] = IRQHANDLER_44,														// 
		[29] = IRQHANDLER_45,														// 
		[30] = IRQHANDLER_46,														// 
		[31] = IRQHANDLER_47,														// 
	}
};



	
	
// ******************************************************************************************************
// PUBLIC Functions
// ******************************************************************************************************
void _Chip_CoreClockUpdate(void);
//void _Chip_SYSCON_PowerUp(uint32_t PowerUpMask);


// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************
volatile uint32_t g_Wdt_Osc_Freq;











// ******************************************************************************************************
// CHIP functions
// ******************************************************************************************************

typedef void (*IAP)(unsigned int [],unsigned int[]);

// ------------------------------------------------------------------------------------------------
// init ITM Cortex Engine
void _Chip_SWO_Init(uint32_t Main_CPU_Clock)
{
// DEBUG INIT:
#if (CONF_DEBUG_ITM == 1) && (__CORTEX_M >= 3)										// debug with ITM support
	ITM_RxBuffer = ITM_RXBUFFER_EMPTY;  											//  CMSIS Debug Input

	SYSCON->ARMTRACECLKDIV = 1;														// ARM trace clock divider register set to 1 - run it.

	CoreDebug->DEMCR = CoreDebug_DEMCR_TRCENA_Msk;									// Enable Trace (Debug Exception and Monitor Control Registe

	TPI->SPPR = kSWO_ProtocolNrz;													// select SWO encoding protocol - 1: SWO Manchester protocol,  2: SWO UART/NRZ protocol
	TPI->ACPR = (Main_CPU_Clock / 2000000)-1;										// TPIU - Set the baud rate to 10MHz
	TPI->FFCR = 0x100;																// Configure FIFO
	
	ITM->LAR = 0xC5ACCE55;															// Enable write to ITM registers
	
	if(~(ITM->LSR & 0x02))															// Accesss unlocked?
	{
		ITM->TPR = 0;																// allow unprivilege access
		ITM->TCR = ITM_TCR_ITMENA_Msk | ITM_TCR_SYNCENA_Msk| ITM_TCR_TraceBusID_Msk | ITM_TCR_DWTENA_Msk
#if defined(BSP_ALLOW_SWO) && (BSP_ALLOW_SWO == 1)
		| ITM_TCR_SWOENA_Msk;
		ITM->TER = 1U << 0;															// ITM Port #0 enable for SWO
#else
		;
		ITM->TER = 0;																// ITM Port #0 disable for SWO
#endif		
		
	}
#endif //CONF_DEBUG_ITM
	
}


// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// Call System Init - in reset vector!
void _Chip_SystemInit(void)											// inserted into SystemInit
{
	SYSCON->SYSAHBCLKCTRL0 |= 1<<1;													// switch power ON for ROM
	SYSCON->SYSAHBCLKCTRL0 |= 1<<2;													// switch power ON for SRAM
	SYSCON->SYSAHBCLKCTRL0 |= 1<<4;													// switch power ON for Flash
}


// ------------------------------------------------------------------------------------------------
// get reset signal sources
//	_RST_NOT = 0,
//	_RST_POR = 1,																	// Power On Reset
//	_RST_EXT = 2,																	// External Reset signal asserted
//	_RST_WDT = 4,																	// WatchDog Reset
//	_RST_BOD = 8,																	// Brown-out reset
//	_RST_SYS = 16,																	// System Reset
// ------------------------------------------------------------------------------------------------
// get reset signal sources
void _Chip_Read_ResetSource(uint32_t *dst)							// Get reset sources (Chip_RST_SRC)
{
 	// if an break occured in the next line, destination isn't align to 4 byte!
	*dst = 0;																		// Clear destination
    if(SYSCON->SYSRSTSTAT & (1<<0)) *dst |= 1 + 8;									// Power On reset and Bruwn out detect
	if(SYSCON->SYSRSTSTAT & (1<<1)) *dst |= 2;										// External Reset
	if(SYSCON->SYSRSTSTAT & (1<<2)) *dst |= 4;										// WatchDog reset
	if(SYSCON->SYSRSTSTAT & (1<<4)) *dst |= 16;										// System Reset
	SYSCON->SYSRSTSTAT |= 31;														// clear all reset flags
}









// ******************************************************************************************************
// IAP functions
// ******************************************************************************************************

#if	defined	( CONF_CHIP_ID_LPC844M201JBD64 ) || \
 	defined	( CONF_CHIP_ID_LPC844M201JBD48 ) || \
 	defined	( CONF_CHIP_ID_LPC844M201JHI48 ) || \
 	defined	( CONF_CHIP_ID_LPC844M201JHI33 ) || \
 	defined	( CONF_CHIP_ID_LPC845M301JBD64 ) || \
 	defined	( CONF_CHIP_ID_LPC845M301JBD48 ) || \
 	defined	( CONF_CHIP_ID_LPC845M301JHI48 ) || \
 	defined	( CONF_CHIP_ID_LPC845M301JHI33 )
	//next functions are available only in the LPC84x MCU's


// ------------------------------------------------------------------------------------------------
// This command is used to obtain a 32-bit signature value of the flash region. See Section 5.5.17 �Read flash signature� and Chapter 6 for more information.
// Remark: See Section 6.5.1.2 �Signature generation� to ensure that the flash signature is generated correctly.
// When CRP1 is enabled, only the signature of the entire flash can be read and no parameters can be passed.
//return 	result[0] : Status Code
bool _Chip_IAP_ReadFlashSign(uint32_t StartFlash, uint32_t EndFlash, uint32_t WaitStates)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_READFLASHSIGN_CMD;
	Command[1] = StartFlash;
	Command[2] = EndFlash;
	Command[3] = WaitStates;
	Command[4] = 0;
	_Chip_IAP_Entry(Command, Result);

	if(Result[0] == 0) return(true);
	return (false);	
}

// ------------------------------------------------------------------------------------------------
// This command reads a given page of FAIM into the memory provided.
//return 	result[0] : Status Code
bool _Chip_IAP_ReadFAIMPage(uint32_t PageNum, uint32_t *ptrFaimMirror)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_READFAIM_CMD;
	Command[1] = PageNum;
	Command[2] = (uint32_t) ptrFaimMirror;
	_Chip_IAP_Entry(Command, Result);

	if(Result[0] == 0) return(true);
	return (false);	
}

// ------------------------------------------------------------------------------------------------
// This command retrieves data from memory at the address given by Param1 and written to the FAIM page.
//return 	result[0] : Status Code
bool _Chip_IAP_WriteFAIMPage(uint32_t PageNum, uint32_t *ptrFaimMirror)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_WRITEFAIM_CMD;
	Command[1] = PageNum;
	Command[2] = (uint32_t) ptrFaimMirror;
	_Chip_IAP_Entry(Command, Result);

	if(Result[0] == 0) return(true);
	return (false);	
}

#endif

// ------------------------------------------------------------------------------------------------
// Prepare sector for write operation - This command must be executed before executing "Copy RAM to flash" or "Erase Sector(s)" command. 
//					Successful execution of the "Copy RAM to flash" or "Erase Sector(s)" command causes relevant sectors to be protected again. 
//					The boot sector can not be prepared by this command. To prepare a single sector use the same "Start" and "End" sector numbers.
//return 	result[0] : Status Code
bool _Chip_IAP_PrepareSector(uint32_t strSector, uint32_t endSector)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_PREWRRITE_CMD;
	Command[1] = strSector;
	Command[2] = endSector;
	_Chip_IAP_Entry(Command, Result);

	if(Result[0] == 0) return(true);
	return (false);	
}


// ------------------------------------------------------------------------------------------------
// Copy RAM to flash - This command is used to program the flash memory. 
//					The affected sectors should be prepared first by calling "Prepare Sector for Write Operation" command. 
//					The affected sectors are automatically protected again once the copy command is successfully executed. 
//					The boot sector can not be written by this command. Also see Section 34.4.3 for the number of bytes that can be written.
//					Param3 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
//					Remark: All user code must be written in such a way that no master accesses the flash while this command is executed and the flash is programmed.
//return 	result[0] : Status Code
bool _Chip_IAP_CopyRamToFlash(uint32_t DstAdd, uint32_t *pSrc, uint32_t byteswrt)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_WRISECTOR_CMD;
	Command[1] = DstAdd;
	Command[2] = (uint32_t) pSrc;
	Command[3] = byteswrt;															// Should be 256 | 512 | 1024 | 4096.
	Command[4] = SystemCoreClock / 1000;
	_Chip_IAP_Entry(Command, Result);

	if(Result[0] == 0) return(true);
	return (false);	
}

// ------------------------------------------------------------------------------------------------
// Erase Page - This command is used to erase a page or multiple pages of on-chip flash memory. 
//				To erase a single page use the same "Start" and "End" page numbers.
//				Param2 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
//				Remark: All user code must be written in such a way that no master accesses the flash while this command is executed and the flash is erased.
//return 	result[0] : Status Code
bool _Chip_IAP_ErasePage(uint32_t strPage, uint32_t endPage)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_ERASE_PAGE_CMD;
	Command[1] = strPage;
	Command[2] = endPage;
	Command[3] = SystemCoreClock / 1000;
	_Chip_IAP_Entry(Command, Result);

	if(Result[0] == 0) return(true);
	return (false);
}

// ------------------------------------------------------------------------------------------------
// Blank check sector - This command is used to blank check a sector or multiple sectors of on-chip flash memory. 
//					To blank check a single sector use the same "Start" and "End" sector numbers.
// Result is True if sector/s is blank. Otherwise false
bool _Chip_IAP_BlankCheckSector(uint32_t strSector, uint32_t endSector)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_BLANK_CHECK_SECTOR_CMD;
	Command[1] = strSector;
	Command[2] = endSector;
	_Chip_IAP_Entry(Command, Result);

	if(Result[0] == 0) return(true);
	return (false);
}

// ------------------------------------------------------------------------------------------------
// Read_MCUChipID
// Read MCU ID 
// Readed ID is saved into memory at address dst
// Result is error code
bool _Chip_IAP_Read_ID(uint32_t *dst)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_REPID_CMD;
	_Chip_IAP_Entry(Command, Result);
	
	if(Result[0] == 0)																// Function call SUCCESS?
	{
		*dst = Result[1];															// save readed value
		return(true);
	}
	else
	{
		*dst = NULL;
		return (false);
	}
}

// ------------------------------------------------------------------------------------------------
// Read_MCUSerialNum
// Read MCU serial Number
// Readed SerialNumber is saved into memory at address dst
// Result is error code
bool _Chip_IAP_Read_SerialNum(uint32_t *dst)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_READ_UID_CMD;
	_Chip_IAP_Entry(Command, Result);
	
	if(Result[0] == 0)																// Function call SUCCESS?
	{
		*dst++ = Result[1];															// save readed value
		*dst++ = Result[2];															// save readed value
		*dst++ = Result[3];															// save readed value
		*dst = Result[4];															// save readed value
		return(true);
	}
	else
	{
		*dst = 0xFFFFFFFF;
		return (false);
	}
}

// ------------------------------------------------------------------------------------------------
// Read BootLoader version in MCU
// Result is success code
bool _Chip_IAP_Read_BootCodeVersion(uint32_t *dst)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_READ_BOOT_CODE_CMD;
	_Chip_IAP_Entry(Command, Result);
	
	if(Result[0] == 0)																// Function call SUCCESS?
	{
		*dst = Result[1];															// save readed value
		return(true);
	}
	else
	{
		*dst = NULL;
		return (false);
	}
}







// ****************************************************************************************************** 
// CLOCK Functions
// ****************************************************************************************************** 

uint32_t  SystemCoreClock;															// for IAP - System Clock Frequency (Core Clock)

#if defined (CONF_CHIP_ID_LPC844M201JHI33) || \
	defined (CONF_CHIP_ID_LPC844M201JBD48) || \
	defined (CONF_CHIP_ID_LPC844M201JHI48) || \
	defined (CONF_CHIP_ID_LPC844M201JBD64) || \
	defined (CONF_CHIP_ID_LPC845M301JHI33) || \
	defined (CONF_CHIP_ID_LPC845M301JBD48) || \
	defined (CONF_CHIP_ID_LPC845M301JHI48) || \
	defined (CONF_CHIP_ID_LPC845M301JBD64)


#ifndef CONF_CHIP_FRORATE
#define CONF_CHIP_FRORATE	0
#endif


// Set FRO oscillator output frequency.
// Initialize the FRO clock to given frequency (18, 24 or 30 MHz).
// param freq Please refer to clock_fro_osc_freq_t definition, frequency must be one of 18000, 24000 or 30000 KHz.
inline void _Chip_IAP_SetFroOscFreq(uint32_t freq_in_khz)
{
    (*((void (*)(uint32_t freq_in_khz))(CLOCK_FRO_SETTING_API_ROM_ADDRESS)))(freq_in_khz);
}


// ------------------------------------------------------------------------------------------------
// "fro frequency"
uint32_t _Chip_Clock_Get_Fro_Rate(void)
{
	uint32_t Result = CONF_CHIP_FRORATE;
	
	switch ((SYSCON->FROOSCCTRL ) & 0x03U) 											// Determine clock frequency according to clock register values
	{
		case 0U:  Result = 18000000; break;
		case 1U:  Result = 24000000; break;
		case 2U:  Result = 30000000; break;
		case 3U:  Result = 30000000; break;
		default:  Result = 0; break;
	}
	
//	if (((SYSCON->FROOSCCTRL >> SYSCON_FROOSCCTRL_FRO_DIRECT_SHIFT) & 0x01) == 0) 
//	{
//		Result = Result >> 1;
//	}

   if (((SYSCON->FROOSCCTRL & SYSCON_FROOSCCTRL_FRO_DIRECT_MASK) >> SYSCON_FROOSCCTRL_FRO_DIRECT_SHIFT) == 0U)
    {
        /* need to check the FAIM low power boot value */
        Result /= ((*((volatile uint32_t *)(_CHIP_CLOCK_FAIM_BASE)) & 0x2U) != 0UL) ? 16U : 2U;
    }
	
	return(Result);
}


#ifndef CONF_CHIP_EXTRATEIN
#define CONF_CHIP_EXTRATEIN			0
#endif


// ------------------------------------------------------------------------------------------------
// "external_clk"
static inline uint32_t _Chip_Clock_Get_External_Clk_Rate(void)
{
    uint32_t Result;

    if (SYSCON->EXTCLKSEL & 1) Result = CONF_CHIP_EXTRATEIN;						// CLK IN freq.
	else Result = CONF_CHIP_OSCRATEIN;												// CONF_CHIP_OSCRATEIN
    
    return(Result);
}


// ------------------------------------------------------------------------------------------------
// "fro_div frequency"
static inline uint32_t _Chip_Clock_Get_FroDiv_Rate(void)
{
	return(_Chip_Clock_Get_Fro_Rate() >> 1);
}

// ------------------------------------------------------------------------------------------------
// "main_clk_pre_pll"
static inline uint32_t _Chip_Clock_Get_MainClkPrePll_Rate(void)
{
    uint32_t Result;

    switch (SYSCON->MAINCLKSEL & 0x03)
    {
        case 0x00: 	Result = _Chip_Clock_Get_Fro_Rate();							// FRO rate
					break;
		case 0x01: 	Result = _Chip_Clock_Get_External_Clk_Rate();					// External clk
					break;
		case 0x02: 	Result = g_Wdt_Osc_Freq;										// WatchDog clk
					break;
		case 0x03:	Result = _Chip_Clock_Get_FroDiv_Rate();							// FRO_Div rate
					break;
		default: 	Result = 0;														// unknown state
    }
    return(Result);
}

// ------------------------------------------------------------------------------------------------
// "sys_pll0_Inp" input freq to system 0 PLL
uint32_t _Chip_Clock_Get_SysPLL0_Inp_Rate(void)
{
	uint32_t Result;

	switch (SYSCON->SYSPLLCLKSEL & 0x03)
	{
        case 0x00: 	Result = _Chip_Clock_Get_Fro_Rate();							// FRO rate
					break;
		case 0x01: 	Result = _Chip_Clock_Get_External_Clk_Rate();					// External clk
					break;
		case 0x02: 	Result = g_Wdt_Osc_Freq;										// WatchDog clk
					break;
		case 0x03:	Result = _Chip_Clock_Get_FroDiv_Rate();							// FRO_Div rate
					break;
		default: 	Result = 0;														// unknown state
	}
    return (Result);
}


// ------------------------------------------------------------------------------------------------
// "sys_pll0_Clk"
uint32_t _Chip_Clock_Get_SysPLL0_Clk_Rate(void)
{
    return ((_Chip_Clock_Get_SysPLL0_Inp_Rate() * (((SYSCON->SYSPLLCTRL & SYSCON_SYSPLLCTRL_MSEL_MASK) >> SYSCON_SYSPLLCTRL_MSEL_SHIFT) + 1)) / 	\
		(1 << ((SYSCON->SYSPLLCTRL & SYSCON_SYSPLLCTRL_PSEL_MASK) >> SYSCON_SYSPLLCTRL_PSEL_SHIFT)));
}


// ------------------------------------------------------------------------------------------------
// "mainclk"
// return clock source for core sysclk
static inline uint32_t _Chip_Clock_Get_MainClk_Rate(void)
{
    uint32_t Result;

    switch (SYSCON->MAINCLKPLLSEL & 0x03)
    {
        case 0x00: 	Result = _Chip_Clock_Get_MainClkPrePll_Rate();					// MainClkPrePll rate
					break;

		case 0x01: 	Result = _Chip_Clock_Get_SysPLL0_Clk_Rate();					// System PLL Out rate
					break;
		case 0x02:	Result = 0;														// none
					break;
		case 0x03: 	Result = 0;														// none
					break;
		default: 	Result = 0;														// unknown state
    }

    return(Result);
}

// ------------------------------------------------------------------------------------------------
// return source clock frequency for core sysclk
uint32_t _Chip_Clock_Get_SourceClk_Rate(void)										// Get input source clock rate.
{
	uint32_t Result;

	
	switch (SYSCON->MAINCLKPLLSEL & 0x03)
    {
        case 0x00:	// main_clk_pre_pll
					switch( SYSCON->MAINCLKSEL & 0x03)
					{
						case 0x00:	Result = _Chip_Clock_Get_Fro_Rate();			// FRO rate
									break;
						case 0x01: 	if(SYSCON->EXTCLKSEL & 0x01) return(CONF_CHIP_OSCRATEIN); // External Crystal
									else return(CONF_CHIP_EXTRATEIN); 				// External Clock input
						case 0x02:	Result = _Chip_Clock_Get_Wdt_Inp_Rate();		// Watchdog oscillator
									break;
						case 0x03:	Result = _Chip_Clock_Get_FroDiv_Rate();			// FRO_Div rate
									break;						
						default:	Result = 0;
					}
					break;
		case 0x01:	// sys_pll0_clk
					switch( SYSCON->SYSPLLCLKSEL & 0x03)
					{
						case 0x00:	Result = _Chip_Clock_Get_Fro_Rate();			// FRO rate
									break;
						case 0x01: 	if(SYSCON->EXTCLKSEL & 0x01) Result = CONF_CHIP_OSCRATEIN; // External Crystal
									else Result = CONF_CHIP_EXTRATEIN; 				// External Clock input
									break;
						case 0x02:	Result = _Chip_Clock_Get_Wdt_Inp_Rate();		// Watchdog oscillator
									break;
						case 0x03:	Result = _Chip_Clock_Get_FroDiv_Rate();			// FRO_Div rate
									break;						
						default:	Result = 0;
					}
					Result = (Result * (((SYSCON->SYSPLLCTRL & SYSCON_SYSPLLCTRL_MSEL_MASK) >> SYSCON_SYSPLLCTRL_MSEL_SHIFT) + 1)) / \
								(1 << ((SYSCON->SYSPLLCTRL & SYSCON_SYSPLLCTRL_PSEL_MASK) >> SYSCON_SYSPLLCTRL_PSEL_SHIFT));
					break;
		case 0x02:	
		case 0x03:
		default: 	Result = 0;														// unknown state
    }
	if(SYSCON->SYSAHBCLKDIV & SYSCON_SYSAHBCLKDIV_DIV_MASK)
	Result = Result * ((SYSCON->SYSAHBCLKDIV & SYSCON_SYSAHBCLKDIV_DIV_MASK) >> SYSCON_SYSAHBCLKDIV_DIV_SHIFT);

	return (Result);
}

// ------------------------------------------------------------------------------------------------
// Get ADC[] Clk
// Return Frequency of ADC
uint32_t _Chip_Clock_Get_ADCClk_Rate(uint32_t ADC_Index)
{
    uint32_t Result = 0;

	switch (SYSCON->ADCCLKSEL & 0x03)
	{
		case 0x00: 	Result = _Chip_Clock_Get_Fro_Rate();							// FRO rate
					break;
		case 0x01: 	Result = _Chip_Clock_Get_SysPLL0_Clk_Rate();					// System PLL Out rate
					break;
	}
		
    return (Result);
}


// ------------------------------------------------------------------------------------------------
// Get FRG0 Clk
// Return FRG0 frequency
uint32_t _Chip_Clock_Get_Frg0_Rate(void)
{
    uint32_t Result = 0;

	switch (SYSCON->FRG[0].FRGCLKSEL & 0x03)
	{
		case 0x00: 	Result = _Chip_Clock_Get_Fro_Rate();							// FRO rate
					break;
		case 0x01: 	Result = _Chip_Clock_Get_MainClk_Rate();						// Main CLK rate
					break;
		case 0x02: 	Result = _Chip_Clock_Get_SysPLL0_Clk_Rate();					// System PLL Out rate
					break;
		default: 	Result = 0;														// unknown state
	}

	if(Result) Result = Result / (1 + (SYSCON->FRG[0].FRGMULT/(SYSCON->FRG[0].FRGDIV + 1)));
	
    return (Result);	
}

// ------------------------------------------------------------------------------------------------
// Get FRG1 Clk
// Return FRG1 frequency
uint32_t _Chip_Clock_Get_Frg1_Rate(void)
{
    uint32_t Result = 0;

	switch (SYSCON->FRG[1].FRGCLKSEL & 0x03)
	{
		case 0x00: 	Result = _Chip_Clock_Get_Fro_Rate();							// FRO rate
					break;
		case 0x01: 	Result = _Chip_Clock_Get_MainClk_Rate();						// Main CLK rate
					break;
		case 0x02: 	Result = _Chip_Clock_Get_SysPLL0_Clk_Rate();					// System PLL Out rate
					break;
		default: 	Result = 0;														// unknown state
	}

	if(Result) Result = Result / (1 + (SYSCON->FRG[1].FRGMULT/(SYSCON->FRG[1].FRGDIV + 1)));
	
    return (Result);	
}

// ------------------------------------------------------------------------------------------------
// Get UART Clk
// Return Input Frequency for UART[n]
uint32_t _Chip_Clock_Get_UARTClk_Rate(uint32_t UART_Index)
{
    uint32_t Result = 0;

	switch (SYSCON->FCLKSEL[UART_Index] & 0x07)										// UART0=FCLK0, UART1=FCLK1,...
	{
		case 0x00: 	Result = _Chip_Clock_Get_Fro_Rate();							// FRO rate
					break;
		case 0x01: 	Result = _Chip_Clock_Get_MainClk_Rate();						// Main CLK rate
					break;
		case 0x02: 	Result = _Chip_Clock_Get_Frg0_Rate();							// FRG0 rate
					break;
		case 0x03: 	Result = _Chip_Clock_Get_Frg1_Rate();							// FRG1 rate
					break;
		case 0x04: 	Result = _Chip_Clock_Get_FroDiv_Rate();							// FRO / 2 rate
					break;		
		default: 	Result = 0;														// unknown state
	}
		
    return (Result);
}


// ------------------------------------------------------------------------------------------------
// Get I2C Clk
// Return Input Frequency for UART[n]
uint32_t _Chip_Clock_Get_I2CClk_Rate(uint32_t I2C_Index)
{
    uint32_t Result = 0;

	switch (SYSCON->FCLKSEL[I2C_Index + 5] & 0x07)									// 5 is Offset for I2C. I2C0=FCLK5, I2C1=FCLK5,...
	{
		case 0x00: 	Result = _Chip_Clock_Get_Fro_Rate();							// FRO rate
					break;
		case 0x01: 	Result = _Chip_Clock_Get_MainClk_Rate();						// Main CLK rate
					break;
		case 0x02: 	Result = _Chip_Clock_Get_Frg0_Rate();							// FRG0 rate
					break;
		case 0x03: 	Result = _Chip_Clock_Get_Frg1_Rate();							// FRG1 rate
					break;
		case 0x04: 	Result = _Chip_Clock_Get_FroDiv_Rate();							// FRO / 2 rate
					break;		
		default: 	Result = 0;														// unknown state
	}
		
    return (Result);
}


// ------------------------------------------------------------------------------------------------
// return clock freq of CLKOUT clock
uint32_t _Chip_Clock_Get_ClkOut_Rate(void)
{
	uint32_t Result;

	switch(SYSCON->CLKOUTSEL & 0x07)
	{
		case 0x00:	Result = _Chip_Clock_Get_Fro_Rate();							// FRO rate
					break;
		case 0x01: 	Result = _Chip_Clock_Get_MainClk_Rate();						// Main CLK rate
					break;
		case 0x02:	Result = _Chip_Clock_Get_SysPLL0_Clk_Rate();					// System PLL Out rate
					break;
		case 0x03:	Result = _Chip_Clock_Get_External_Clk_Rate();					// External clk
					break;
		default: 	Result = 0;														// unknown state
	}

	if((Result) && (SYSCON->CLKOUTDIV & 0x000000ff))  Result = Result / (SYSCON->CLKOUTDIV & 0x000000ff);
	return (Result);
}


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// 845 vs 8xx !!!!!!!!!!!!!
#else	

// ------------------------------------------------------------------------------------------------
// return source clock frequency for core sysclk
uint32_t _Chip_Clock_Get_SourceClk_Rate(void)										// Get input source clock rate.
{
	uint32_t Result;

	
	switch (SYSCON->MAINCLKSEL & SYSCON_MAINCLKSEL_SEL_MASK)
    {
        case 0x00: 	Result = _CHIP_IRC_FREQUENCY;									// Internal RC oscillator 
					break;
		case 0x02:	Result = _Chip_Clock_Get_Wdt_Inp_Rate();						// Watchdog oscillator
					break;
		
		case 0x01: 	
		case 0x03:
					switch( SYSCON->SYSPLLCLKSEL & SYSCON_SYSPLLCLKSEL_SEL_MASK)	// check SYSPLLCLKSEL
					{
						case 0x00: Result = _CHIP_IRC_FREQUENCY;					// Internal RC
									break;
						case 0x01: Result = CONF_CHIP_OSCRATEIN;					// External Crystal
									break;
						case 0x03: Result = CONF_CHIP_EXTRATEIN;					// External clock input
									break;						
						default: Result = 0;
					}
					break;
		default: 	Result = 0;														// unknown state
    }

	return (Result);
}


// ------------------------------------------------------------------------------------------------
// "mainclk"
// return clock source for core sysclk
uint32_t _Chip_Clock_Get_MainClk_Rate(void)
{
    uint32_t Result;

    switch (SYSCON->MAINCLKSEL & 0x03)
    {
        case 0x00: 	Result = _CHIP_IRC_FREQUENCY;									// Internal RC oscillator 
					break;

		case 0x01: 	Result = _Chip_Clock_Get_SysPLL_In_Rate();						// PLLInput = MainCLK
					break;
		case 0x02:	Result = _Chip_Clock_Get_Wdt_Inp_Rate();						// Watchdog oscillator
					break;
		case 0x03: 	Result = _Chip_Clock_Get_SysPLL_Out_Rate();						// PLLOutput = MainCLK
					break;
		default: 	Result = 0;														// unknown state
    }

    return(Result);
}


// ------------------------------------------------------------------------------------------------
// "syspllout"
// return clock source for core sysclk
uint32_t _Chip_Clock_Get_SysPLL_Out_Rate(void)										// Get system pll output rate
{
#if defined (CONF_CHIP_ID_LPC844M201JHI33) || \
	defined (CONF_CHIP_ID_LPC844M201JBD48) || \
	defined (CONF_CHIP_ID_LPC844M201JHI48) || \
	defined (CONF_CHIP_ID_LPC844M201JBD64) || \
	defined (CONF_CHIP_ID_LPC845M301JHI33) || \
	defined (CONF_CHIP_ID_LPC845M301JBD48) || \
	defined (CONF_CHIP_ID_LPC845M301JHI48) || \
	defined (CONF_CHIP_ID_LPC845M301JBD64)
	return((_Chip_Clock_Get_SysPLL_In_Rate() * ((SYSCON->SYSPLLCTRL & 0x1f)+1)) / (1 << ((SYSCON->SYSPLLCTRL >> 5) & 0x03)));
#else
	return((_Chip_Clock_Get_SysPLL_In_Rate() * ((SYSCON->SYSPLLCTRL & 0x1f)+1)));
#endif	
	
	
}


// ------------------------------------------------------------------------------------------------
// "syspllin"
// return clock source for core sysclk
uint32_t _Chip_Clock_Get_SysPLL_In_Rate(void)										// Get system pll input rate
{
	switch(SYSCON->SYSPLLCLKSEL & 0x03)
	{
		case 0x00: return(_CHIP_IRC_FREQUENCY);										// IRC osc as source
		case 0x01: return( CONF_CHIP_OSCRATEIN);									// system oscillator input (SYSOSC)
		case 0x03: return( CONF_CHIP_EXTRATEIN);									// external input (via pin CLKIN)
		default: return(0);															// unknown state
	}
}

// ------------------------------------------------------------------------------------------------
// return clock freq of CLKOUT clock
uint32_t _Chip_Clock_Get_ClkOut_Rate(void)
{
	uint32_t Result;

	switch(SYSCON->CLKOUTSEL & 0x03)
	{
		case 0x00:	Result = _CHIP_IRC_FREQUENCY;									// Internal RC oscillator  
					break;
		case 0x01: 	Result = CONF_CHIP_OSCRATEIN;									// source is system oscillator
					break;
		case 0x02:	Result = _Chip_Clock_Get_Wdt_Inp_Rate();						// source is WatchDog 
					break;
		case 0x03:	Result = _Chip_Clock_Get_MainClk_Rate();						// mainclk
					break;
		default: 	Result = 0;														// unknown state
	}

	return (Result);
}

// ------------------------------------------------------------------------------------------------
// Get ADC Clk
// Return Frequency of ADC
uint32_t _Chip_Clock_Get_ADCClk_Rate(uint32_t ADC_Index)
{
    uint32_t freq = _Chip_Clock_Get_SysClk_Rate();									// ADC input rate is same as system clock

	freq = freq/((ADC0->CTRL & ADC_CTRL_CLKDIV_MASK) + 1U);							// else system_clock as source and apply divider from ADCCTRL
	
    return (freq);
}





// ------------------------------------------------------------------------------------------------
// Get UART Clk
// Return Input Frequency for UART[n] - U_PCLK
uint32_t _Chip_Clock_Get_UARTClk_Rate(uint32_t UART_Index)
{
    uint32_t Result = 0;

	Result = _Chip_Clock_Get_MainClk_Rate() / (SYSCON->UARTCLKDIV & 0x000000FF);	// Get UARTCLKDIV freq.
	
	if(Result) Result = Result / (1 + (SYSCON->UARTFRGMULT/(SYSCON->UARTFRGDIV+1)));
		
    return (Result);
}

// ------------------------------------------------------------------------------------------------
// Get I2C Clk
// Return Input Frequency for I2C[n]
uint32_t _Chip_Clock_Get_I2CClk_Rate(uint32_t I2C_Index)
{
     uint32_t freq = _Chip_Clock_Get_SysClk_Rate();									// I2C input rate is same as system clock

#if _CHIP_I2C_COUNT >= 4
	if(I2C_Index == 3) 
		freq = freq/(((I2C3->CLKDIV & I2C_CLKDIV_DIVVAL_MASK) >> I2C_CLKDIV_DIVVAL_SHIFT) + 1U);	// else system_clock as source and apply divider from I2CDIV
#endif
	
#if _CHIP_I2C_COUNT >= 3
	if(I2C_Index == 2) 
		freq = freq/(((I2C2->CLKDIV & I2C_CLKDIV_DIVVAL_MASK) >> I2C_CLKDIV_DIVVAL_SHIFT) + 1U);	// else system_clock as source and apply divider from I2CDIV
#endif

#if _CHIP_I2C_COUNT >= 2
	if(I2C_Index == 1) 
		freq = freq/(((I2C1->CLKDIV & I2C_CLKDIV_DIVVAL_MASK) >> I2C_CLKDIV_DIVVAL_SHIFT) + 1U);	// else system_clock as source and apply divider from I2CDIV
#endif
	
#if _CHIP_I2C_COUNT >= 1
	if(I2C_Index == 0) 
		freq = freq/(((I2C0->CLKDIV & I2C_CLKDIV_DIVVAL_MASK) >> I2C_CLKDIV_DIVVAL_SHIFT) + 1U);	// else system_clock as source and apply divider from I2CDIV
#endif
	
    return (freq);
}



#endif

















static const uint8_t wdtFreqLookup[16] = {12, 21, 28, 35, 42, 48, 54, 60, 65, 70, 75, 80, 84, 88, 92};

// ------------------------------------------------------------------------------------------------
// return clock frequency for watchdog "wdt_clk"
uint32_t _Chip_Clock_Get_Wdt_Inp_Rate(void)
{
    uint8_t freq_sel, div_sel;
	
	if ((SYSCON->PDRUNCFG & SYSCON_PDRUNCFG_WDTOSC_PD_MASK) != 0UL)
	{
		return 0U;
	}
	else
	{
		div_sel = (uint8_t)((SYSCON->WDTOSCCTRL & SYSCON_WDTOSCCTRL_DIVSEL_MASK) + 1UL) >> SYSCON_WDTOSCCTRL_DIVSEL_SHIFT;
		freq_sel = wdtFreqLookup[((SYSCON->WDTOSCCTRL & SYSCON_WDTOSCCTRL_FREQSEL_MASK) >> SYSCON_WDTOSCCTRL_FREQSEL_SHIFT)];
		return ((uint32_t)freq_sel * 50000U) / ((uint32_t)div_sel);					// step is 50 kHz
	}
}

										  

// ------------------------------------------------------------------------------------------------
// "sysclk"
uint32_t _Chip_Clock_Get_SysClk_Rate(void)											// read system clock
{
	return(_Chip_Clock_Get_MainClk_Rate() / ((SYSCON->SYSAHBCLKDIV & SYSCON_SYSAHBCLKDIV_DIV_MASK) >> SYSCON_SYSAHBCLKDIV_DIV_SHIFT));
}


// ------------------------------------------------------------------------------------------------
// CPU core clock source rate, also called as "cclk" or "cpuclk"					// CPU clock rate
uint32_t _Chip_Clock_Get_CoreClk_Rate(void)
{
	return(_Chip_Clock_Get_SysClk_Rate());											// same as "sysclk"
}


// ------------------------------------------------------------------------------------------------
// Find encoded PDEC value for raw P value, max P = PVALMAX
uint32_t _Chip_Encode_PLL_P(uint32_t P)
{
     switch (P)																		// Find PDec
    {
        case 1U:
            return(0x00);
        case 2U:
            return(0x01);
        case 4U:
            return(0x02);
        case 8U:
            return(0x03);
        default:
            break;
    }
	return(0x04);																	// wrong setting detected
}

// ------------------------------------------------------------------------------------------------
// Find decoded P value for raw PDEC value
uint32_t _Chip_Decode_PLL_P(uint32_t PDEC)
{
    switch (PDEC)																	// Find PDec
    {
        case 0x00:
            return(1U);
        case 0x01:
            return(2U);
        case 0x02:
            return(4U);
        case 0x03:
            return(8U);
        default:
            break;
    }

    return (0L);																	// wrong setting detected
}

// ------------------------------------------------------------------------------------------------
// Set PLL frequency.
// Input clock have to active! 
// All PLL are identical (System, periphery)
//
// UM10736: page 74
// Check that the selected settings meet all of the PLL requirements:
//	� Fin is in the range of 10 MHz to 25 MHz.
//	� Fclkout is max 100 MHz.
//	� The post-divider is either bypassed, or P is in the range of 2 to 16.
//	� M is in the range of 2 to 33.

void _Chip_Clock_Set_PLL_Rate(uint32_t pllM, uint32_t pllP)
{
	if(pllM == 0 ) pllM = 1;
	
	// Set Output 2xP divider and feedback's M multiplier
    SYSCON->SYSPLLCTRL = (_Chip_Encode_PLL_P(pllP) << 5) | (pllM-1) ;
	
	// Wait for Lock - skoncime tu!!!! WHY ????
    while ((SYSCON->SYSPLLSTAT & 0x01) == 0UL)										// Wait for PLL Lock
    {
    }

}
















// ************************************************************************************************
// GPIO Functions 
// ************************************************************************************************


// ------------------------------------------------------------------------------------------------
// Get ptr from index
void *_Chip_Get_GPIO_Ptr(int32_t PeriIndex)											// vrat pointer na konkretnu GPIO periferiu
{
	return(GPIO);																	// LPC8xx have 1 GPIO
}
// ------------------------------------------------------------------------------------------------
// Set Direction for a GPIO port 
void _Chip_GPIO_Set_PinDir(GPIO_Type *pGPIO, uint8_t portNum, uint32_t bitValue, uint8_t out)
{
	if (out) {
		pGPIO->DIR[portNum] |= bitValue;
	}
	else {
		pGPIO->DIR[portNum] &= ~bitValue;
	}
}

// ------------------------------------------------------------------------------------------------
// Read Pin Value
bool _Chip_GPIO_GetPinState(GPIO_Type *pGPIO, uint8_t port, uint8_t pin)
{
	return (bool) pGPIO->B[port][pin];
}

// ------------------------------------------------------------------------------------------------
// Clear Pin
void _Chip_GPIO_SetPinOutLow(GPIO_Type *pGPIO, uint8_t port, uint8_t pin)
{
	pGPIO->CLR[port] = (1 << pin);
}

// ------------------------------------------------------------------------------------------------
// Set Pin
void _Chip_GPIO_SetPinOutHigh(GPIO_Type *pGPIO, uint8_t port, uint8_t pin)
{
	pGPIO->SET[port] = (1 << pin);
}

// ------------------------------------------------------------------------------------------------
// Toggle Pin
void _Chip_GPIO_SetPinToggle(GPIO_Type *pGPIO, uint8_t port, uint8_t pin)
{
	pGPIO->NOT[port] = (1 << pin);
}


// ------------------------------------------------------------------------------------------------
// Enable/Disable PINT IRQ
static bool _Chip_GPIO_PINT_Enable_IRQ_NVIC(void *pPeri, uint32_t Port, uint32_t Pin, bool NewState)
{
	if(NewState == true)
	{
		NVIC_EnableIRQ( (IRQn_Type) (PIN_INT0_IRQn + Pin));							// Enable PINT0 interrupt
	}
	else
	{
		NVIC_DisableIRQ( (IRQn_Type) (PIN_INT0_IRQn + Pin));						// Disable Pin Interrupt	
	}
	return(true);
}


// ------------------------------------------------------------------------------------------------
// HW Initialization
// Initialize port and pin
// ex:  drv_GPIO_Init(sig_STATUS_0)
void *_Chip_GPIO_Init ( int32_t PeriIndex, uint32_t Pin)
{
	void *ResPtr = NULL;

	if((PeriIndex < 0) || (PeriIndex >= _CHIP_GPIO_COUNT)) return(false);		// check or available periphery for this MCU
	

#if _CHIP_GPIO_COUNT >= 2
	if(PeriIndex == 1)
	{
		// Power:
		// nothing to power up/down
			
		// Clock:		
		if((SYSCON->SYSAHBCLKCTRL0 & SYSCON_SYSAHBCLKCTRL0_GPIO1(1)) == 0)			// Enabled ???
		{
			SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_GPIO1(1);				// enable clock to periphery

		// Reset:
			SYSCON->PRESETCTRL0 &= ~(SYSCON_PRESETCTRL0_GPIO1_RST_N(1));__nop();	// generate reset
			SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_GPIO1_RST_N(1);				// release reset
			
		// Feedback?
				// not implemented				                    				// I2C not ready? Missing PULLUP?
			
		// Clear IRQ:
				// use PING or GINT													// clear pending interrupt flag

		// Disable IRQ
				// use PING or GINT													// Enable interrupt
		}
		ResPtr = (void *) GPIO;														// return address of periphery
	}
#endif


#if _CHIP_GPIO_COUNT >= 1
	if(PeriIndex == 0)
	{
		// Power:
		// nothing to power up/down
			
		// Clock:		
		if((SYSCON->SYSAHBCLKCTRL0 & SYSCON_SYSAHBCLKCTRL0_GPIO0(1)) == 0)			// Enabled ???
		{
			SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_GPIO0(1);				// enable clock to periphery
			
		// Reset:
			SYSCON->PRESETCTRL0 &= ~(SYSCON_PRESETCTRL0_GPIO0_RST_N(1));__nop();	// generate reset
			SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_GPIO0_RST_N(1);				// release reset			
			
		// Feedback?
				// not implemented				                    				// I2C not ready? Missing PULLUP?
			
		// Clear IRQ:
				// use PING or GINT													// clear pending interrupt flag

		// Disable IRQ
				// use PING or GINT													// Enable interrupt
		}
		ResPtr = (void *) GPIO;														// return address of periphery
	}
#endif
	
	
	
	// check for this pin is remaped to another peripherial 
#if 0																				// check off - can be disconnected JTAG or SWD !!												
	for(int32_t i = 0; i <= 15; i++)
	{
		for(uint8_t j = 0; j<=(3 - ((i/15)*2)); j++)								// 15 register check for byte 0 and byte 1
		{
			PinAssignReg = *(&SWM->PINASSIGN0+(i));									// read pinassign value for this pin
			if (((PinAssignReg & (0xff << (j*8))) >> (j*8)) == pinpos) 				// bit 7:0
			{
#if defined(DEBUG_HAL) && (DEBUG_HAL == 1)
				__Debug("\r\n\t GPIO p%d.%d Occupied! PINASSIGN[%d]->Byte:%d.\r\n", Port, Pin, i, j);
#endif				
				SYSCON->SYSAHBCLKCTRL |= SYSCON_SYSAHBCLKCTRL0_SWM(1);				// run SWM
				
				*(&SWM->PINASSIGN0+(i*4)) = 0xff;									// release pin
				SYSCON->SYSAHBCLKCTRL &= ~SYSCON_SYSAHBCLKCTRL0_SWM(1);				// run SWM
				sys_Err("GPIO Init - Collision. GPIO p%d.%d Occupied by: PINASSIGN[%d]->Byte:%d.", Port, Pin, i, j);
			}
		}
	}
#endif	
	return(ResPtr);
}

// ------------------------------------------------------------------------------------------------
// Hardware periphery Deinitialization
// ex:  drv_GPIO_DeInit(sig_STATUS_0)
bool _Chip_GPIO_DeInit ( int32_t PeriIndex, uint32_t Pin)
{
	
	return(true);
}


// ------------------------------------------------------------------------------------------------
// Pin configure - for LPCxxx (with SWM Block)
// Index- target register
// 	0	- IOCON : bit 7-0
// 	1	- IOCON : bit 15-8
//	2	- SWM_ENABLE:. SWM_ASSIGN
//  3 	- 
// ex:
//#define CHIP_PINSWMUNUSED						0xff, 0xff							// PIN not used - write 0xff into PINENABLE - default state
//#define CHIP_PINENABLE(x)						x, 0xff								// write value x into PINENABLE
//#define CHIP_PINASSIGN(x,y)					x, y								// write value y into PINASSIGN(x)	- 

void _Chip_GPIO_Wr_Conf_Pin (uint32_t Port, uint32_t Pin, uint8_t Reg_Idx,  _Chip_IO_Spec_t *SWMRegs)	// write to register Reg_Idx value Reg_Val
{
	uint32_t tmp = 0;
	uint8_t Remap[_CHIP_GPIO_LAST_PIN+1] = _CHIP_IOCON_REMAP;
	uint32_t Reg;
	
	SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_IOCON(1);						// run IOCON
	SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_SWM(1);							// run SWM


	tmp = *(&IOCON->PIO[Remap[(Port * 32) + Pin]]);									// read original value of the IOCON
	tmp &= 0xffff0000;																// clear lower 2 bytes
	tmp |= SWMRegs->IOCON_Reg;														// spodny - 0 byte nasav zadanou hodnotou
	
	Reg = Remap[(Port * 32) + Pin];
	IOCON->PIO[Reg] = tmp;															// and write to IOCON

	uint8_t reg1 = SWMRegs->SWM_RegBlock;											// nacitaj prvu SWM hodnotu - register a pripadne aj blok
	volatile uint8_t reg2 = SWMRegs->SWM_PinNum;									// nacitaj druhu SWM hodnotu - cislo GPIO

	if(reg2 == 0xff)						// write to pinenable					// ak je druha hodnota SWM = 0xff, zapisujeme do PIN_ENABLE registra
	{
		if ( reg1 != 0xff)															// ak je neaktivny = 0xff, nerob nic
			_Chip_SWM_EnableFixedPin (reg1);										// ak je pin aktivny - zapis hodnoty do LPC_SWM->PINENABLEx (nuluje zadany pin)
	} 
	else									// pin movable
	{ 
		_Chip_SWM_MovablePinAssign(reg1, reg2);										// premapovanie pinu z Reg_Val[2] na pin z Reg_Val[3]
	}

	SYSCON->SYSAHBCLKCTRL0 &= ~SYSCON_SYSAHBCLKCTRL0_IOCON(1);						// stop IOCON

	SYSCON->SYSAHBCLKCTRL0 &= ~SYSCON_SYSAHBCLKCTRL0_SWM(1);						// stop SWM
	
}

// ------------------------------------------------------------------------------------------------
// Pin configure - interrupt generation conditions - Pin change interrupt
//	Sens (for whole group!):
// 	0x00	- level - low
// 	0x11	- level - high
// 	0x01	- edge - low to high
// 	0x10	- edge - high to low
// return is pointer to PINT
void *_Chip_GPIO_PINT_Conf(uint8_t PINTSel, uint32_t Port, uint32_t Pin, uint8_t Sens)
{
	if(PINTSel > 7) return(NULL);													// LPC15xx has PINTSEL 0 - 7 only
	
	SYSCON->PINTSEL[PINTSel] |= (Port * 32) + Pin;									// select port and pin to input MUX
	
	switch(Sens)
	{
		case 0x00: 						// low level interrupt
			{
				PINT->ISEL |= (1 << PINTSel);										// select LEVEL mode
				PINT->SIENR |= (1 << PINTSel);										// Enable level interrupt
				PINT->CIENF |= (1 << PINTSel);										// select low-level
				break;
			}
		case 0x01:						// Rising edge interrupt
			{
				PINT->ISEL &= ~(1 << PINTSel);										// select rising EDGE mode
				PINT->SIENR |= (1 << PINTSel);										// Enable rising edge interrupt
				PINT->CIENF |= (1 << PINTSel);										// Disable falling edge interrupt
				break;
			}
		case 0x10:						// Falling edge interrupt
			{
				PINT->ISEL &= ~(1 << PINTSel);										// select falling EDGE mode
				PINT->CIENR |= (1 << PINTSel);										// Disable rising edge interrupt
				PINT->SIENF |= (1 << PINTSel);										// Enable falling edge interrupt
				break;
			}
		case 0x22:						// Rising or Falling edge interrupt
			{
				PINT->ISEL &= ~(1 << PINTSel);										// select rising EDGE mode
				PINT->SIENR |= (1 << PINTSel);										// Enable rising edge interrupt
				PINT->SIENF |= (1 << PINTSel);										// Enable falling edge interrupt
				break;
			}
		case 0x11: 						// High level interrupt
			{
				PINT->ISEL |= (1 << PINTSel);										// select LEVEL mode
				PINT->SIENR |= (1 << PINTSel);										// Enable level interrupt
				PINT->SIENF |= (1 << PINTSel);										// select high level
				break;
			}
		default: return(false);
	}
	PINT->RISE |= (1 << PINTSel);													// clear rise flag
	PINT->FALL |= (1 << PINTSel);													// clear fall flag
	NVIC_ClearPendingIRQ((IRQn_Type) ((uint32_t) PIN_INT0_IRQn + PINTSel) );		// clear pending interrupt flag
	return(PINT);
}









// ************************************************************************************************
// I2C Functions 
// ************************************************************************************************

int32_t _Chip_I2C_Get_PeriIndex(void *pPeri)										// Return Index from pointer
{
#if _CHIP_I2C_COUNT == 4
	if((_CHIP_I2C_T*) pPeri == I2C3) return(3);
#endif    
#if _CHIP_I2C_COUNT >= 3	
	if((_CHIP_I2C_T*) pPeri == I2C2) return(2);
#endif    
#if _CHIP_I2C_COUNT >= 2	
	if((_CHIP_I2C_T*) pPeri == I2C1) return(1);
#endif    
#if _CHIP_I2C_COUNT >= 1		
	if((_CHIP_I2C_T*) pPeri == I2C0) return(0);
#endif	
	return(-1);
}




// ------------------------------------------------------------------------------------------------------
// I2C(Num) Initialization
// result is true:successfull otherwise false
void *_Chip_I2C_Init(int32_t PeriIndex)
{
	void *ResPtr = NULL;
	
	// Check:	
	SYS_ASSERT((PeriIndex < _CHIP_I2C_COUNT) && (PeriIndex >= 0));
	
	
	if((PeriIndex < 0) || (PeriIndex >= _CHIP_I2C_COUNT)) return(false);			// check or available periphery for this MCU
	

#if _CHIP_I2C_COUNT >= 4
	if(PeriIndex == 3)
	{
	// Power:
	// nothing to power up/down
		
	// Clock:		
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_I2C3(1); 					// enable I2C clock

	// Reset:
		SYSCON->PRESETCTRL0 &= ~SYSCON_PRESETCTRL0_I2C3_RST_N(1);__nop();			// generate reset
		SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_I2C3_RST_N(1);					// release reset
	
	// Feedback?
	//	if(I2C3->STAT & 0x0001) return(NULL);                    					// I2C not ready? Missing PULLUP?	--------- For LPC845 Must be ignored this test
		
	// Clear IRQ:
		NVIC_ClearPendingIRQ(I2C3_IRQn);											// clear pending interrupt flag

	// Disable IRQ
		NVIC_DisableIRQ(I2C3_IRQn);													// Enable interrupt
		
		ResPtr = (void *) I2C3;														// return address of periphery
	}
#endif
	
#if _CHIP_I2C_COUNT >= 3	
	if(PeriIndex == 2)
	{
	// Power:
	// nothing to power up/down
		
	// Clock:		
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_I2C2(1); 					// enable I2C clock

	// Reset:
		SYSCON->PRESETCTRL0 &= ~SYSCON_PRESETCTRL0_I2C2_RST_N(1);__nop();			// generate reset
		SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_I2C2_RST_N(1);					// release reset
	
	// Feedback?
	//	if(I2C2->STAT & 0x0001) return(NULL);                    					// I2C not ready? Missing PULLUP?	--------- For LPC845 Must be ignored this test
		
	// Clear IRQ:
		NVIC_ClearPendingIRQ(I2C2_IRQn);											// clear pending interrupt flag

	// Disable IRQ
		NVIC_DisableIRQ(I2C2_IRQn);													// Enable interrupt
		
		ResPtr = (void *) I2C2;														// return address of periphery
	}
#endif
	
#if _CHIP_I2C_COUNT >= 2	
	if(PeriIndex == 1)
	{
	// Power:
	// nothing to power up/down
		
	// Clock:		
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_I2C1(1); 					// enable I2C clock

	// Reset:
		SYSCON->PRESETCTRL0 &= ~SYSCON_PRESETCTRL0_I2C1_RST_N(1);__nop();			// generate reset
		SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_I2C1_RST_N(1);					// release reset
	
	// Feedback?
	//	if(I2C1->STAT & 0x0001) return(NULL);                    					// I2C not ready? Missing PULLUP?	--------- For LPC845 Must be ignored this test
		
	// Clear IRQ:
		NVIC_ClearPendingIRQ(I2C1_IRQn);											// clear pending interrupt flag

	// Disable IRQ
		NVIC_DisableIRQ(I2C1_IRQn);													// Enable interrupt

		ResPtr = (void *) I2C1;														// return address of periphery
	}
#endif
	
#if _CHIP_I2C_COUNT >= 1		
	if(PeriIndex == 0)
	{
	// Power:
	// nothing to power up/down
		
	// Clock:		
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_I2C0(1); 					// enable I2C clock

	// Reset:
		SYSCON->PRESETCTRL0 &= ~SYSCON_PRESETCTRL0_I2C0_RST_N(1);__nop();			// generate reset
		SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_I2C0_RST_N(1);					// release reset
	
	// Feedback?
	//	if(I2C0->STAT & 0x0001) return(NULL);                    					// I2C not ready? Missing PULLUP?	--------- For LPC845 Must be ignored this test
		
	// Clear IRQ:
		NVIC_ClearPendingIRQ(I2C0_IRQn);											// clear pending interrupt flag

	// Disable IRQ
		NVIC_DisableIRQ(I2C0_IRQn);													// Enable interrupt

		ResPtr = (void *) I2C0;														// return address of periphery
	}
#endif	
    
	return((uint32_t *) ResPtr);													// return address of periphery thid MCU have only one I2C
}



// ------------------------------------------------------------------------------------------------
// Set SCL Low and High level time
void _Chip_I2CM_SetDutyCycle(void *pPeri, uint16_t sclH, uint16_t sclL)
{
	((_CHIP_I2C_T*)pPeri)->MSTTIME = (((sclH - 2) & 0x07) << 4) | ((sclL - 2) & 0x07);
}


// ------------------------------------------------------------------------------------------------
// Set clock divisor
void _Chip_I2C_SetClockDiv(void *pPeri, uint32_t clkdiv)
{
	if ((clkdiv >= 1) && (clkdiv <= 65536)) ((_CHIP_I2C_T*)pPeri)->CLKDIV = clkdiv - 1;
	else ((_CHIP_I2C_T*)pPeri)->CLKDIV = 0;
}

// ------------------------------------------------------------------------------------------------------
// Read I2C status
uint32_t _Chip_I2C_Get_Peri_Status(void *pPeri)
{
	return(((_CHIP_I2C_T*)pPeri)->STAT);
}

// ------------------------------------------------------------------------------------------------
// Clear I2C status - clear by disable and reenable selected interrupts.
bool _Chip_I2C_Clear_Peri_Status(void *pPeri, uint32_t NewValue)
{
	((_CHIP_I2C_T*)pPeri)->STAT |= NewValue;
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Read I2C MASTER interrupt status
uint32_t _Chip_I2CMST_Get_IRQ_Status(void *pPeri)
{
	uint32_t Result;
	uint32_t rdstat = ((((_CHIP_I2C_T*)pPeri)->INTSTAT) & (((_CHIP_I2C_T *)pPeri)->INTENSET));	// read chip interrupt status
	
	switch (rdstat)																	// recode chip status to CHAL simples status. See UM10470, Table 512
	{
		case (1 << 4): Result |= CHIP_I2C_IRQSTAT_MSARBLOSS; break;					// Master Arbitration Loss Interrupt Status Bit
		case (1 << 6): Result |= CHIP_I2C_IRQSTAT_MSSTSTPERR; break;				// Master Start Stop Error Interrupt Status Bit
		case (1 << 25): Result |= CHIP_I2C_IRQSTAT_SCLTIMEOUT; break;				// SCL Timeout Interrupt Status Bit
		case (1 << 24): Result |= CHIP_I2C_IRQSTAT_EVENTTIMEOUT; break;				// Event Timeout Interrupt Status Bit
		case (1 << 0): Result |= CHIP_I2C_IRQSTAT_MSSTATE_CHANGED; 					// Master Pending Interrupt Status Bit
			{
				uint32_t i2cstat = ((_CHIP_I2C_T*)pPeri)->STAT;						// read chip i2c status
				switch((i2cstat >> 1) & 0x07)
				{
					case 0x01: Result |= CHIP_I2C_MSSTATE_RX; break;				// RX Ready
					case 0x02: Result |= CHIP_I2C_MSSTATE_TX; break;				// TX Ready
					case 0x03: Result |= CHIP_I2C_MSSTATE_NACK_ADR; break;			// Addres non ACK
					case 0x04: Result |= CHIP_I2C_MSSTATE_NACK_DATA; break; 		// Data non ACK
					
				}
				if (i2cstat & (1 << 4)) Result |= CHIP_I2C_IRQSTAT_MSARBLOSS;		// Master Arbitration Loss Interrupt Status Bit
				if (i2cstat & (1 << 6)) Result |= CHIP_I2C_IRQSTAT_MSSTSTPERR;		// Start/Stop error
				break;
			}
		case (1 << 16): Result |= CHIP_I2C_IRQSTAT_MONRDY; break;					// Monitor Ready Interrupt Status Bit
		case (1 << 17): Result |= CHIP_I2C_IRQSTAT_MONOV; break;					// Monitor Overflow Interrupt Status Bit
		case (1 << 19): Result |= CHIP_I2C_IRQSTAT_MONIDLE; break;					// Monitor Idle Interrupt Status Bit
		
		case (1 << 15): Result |= CHIP_I2C_IRQSTAT_SLVDESEL; break;					// Slave Deselect Interrupt Status Bit
		case (1 << 11): Result |= CHIP_I2C_IRQSTAT_SLVNOTSTR; break;				// Slave not stretching Clock Interrupt Status Bit
		case (1 << 8): Result |= CHIP_I2C_IRQSTAT_SLVPENDING; break;				// Slave Pending Interrupt Status Bit
			
		default:  Result = 0;
	}
	return(Result);
}


// ------------------------------------------------------------------------------------------------
// Read I2C SLAVE interrupt status
uint32_t _Chip_I2CSLV_Get_IRQ_Status(void *pPeri)
{
	return(_Chip_I2CMST_Get_IRQ_Status(pPeri));										// use same register
}

// Read I2C MONITOR interrupt status
uint32_t _Chip_I2CMON_Get_IRQ_Status(void *pPeri)
{
	return(_Chip_I2CMST_Get_IRQ_Status(pPeri));										// use same register
}
	
// Read I2C TIMEOUT interrupt status
uint32_t _Chip_I2CTimeout_Get_IRQ_Status(void *pPeri)
{	
	return(_Chip_I2CMST_Get_IRQ_Status(pPeri));										// use same register
}

// ------------------------------------------------------------------------------------------------
// Clear I2C MASTER interrupt status
bool _Chip_I2CMST_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Clear I2C SLAVE interrupt status
bool _Chip_I2CSLV_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Clear I2C MONITOR interrupt status
bool _Chip_I2CMON_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
	return(true);
}


// ------------------------------------------------------------------------------------------------
// Clear I2C Timeout interrupt status
bool _Chip_I2CTimeout_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
	((_CHIP_I2C_T*)pPeri)->STAT |= (1 << 24);										// clear EVENTTIMEOUT status flags
	((_CHIP_I2C_T*)pPeri)->STAT |= (1 << 25);										// clear SCLTIMEOUT status flags
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Enable/disable selected MASTER interrupts
bool _Chip_I2CMST_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_I2C_T*)pPeri)->INTENSET = IRQBitMask & 0x7f;
	else ((_CHIP_I2C_T*)pPeri)->INTENCLR = IRQBitMask  & 0x7f;
	
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Enable/disable selected SLAVE interrupts
bool _Chip_I2CSLV_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_I2C_T*)pPeri)->INTENSET = IRQBitMask & 0x8900;
	else ((_CHIP_I2C_T*)pPeri)->INTENCLR = IRQBitMask  & 0x8900;
	
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Enable/disable selected MONITOR interrupts
bool _Chip_I2CMON_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_I2C_T*)pPeri)->INTENSET = IRQBitMask & 0x000B0000;
	else ((_CHIP_I2C_T*)pPeri)->INTENCLR = IRQBitMask  & 0x000B0000;
	
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Enable/disable selected Timeout interrupts
bool _Chip_I2CTimeout_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_I2C_T*)pPeri)->INTENSET = IRQBitMask & 0x03000000;
	else ((_CHIP_I2C_T*)pPeri)->INTENCLR = IRQBitMask  & 0x03000000;
	
	return(true);
}

// ------------------------------------------------------------------------------------------------
// I2C - write data into I2C
void _Chip_I2CMST_Put_Data(void *pPeri, uint32_t NewValue)
{
	((_CHIP_I2C_T *)pPeri)->MSTDAT = (uint32_t) NewValue;							// write data to I2C
}

// ------------------------------------------------------------------------------------------------
// I2C - Read Data 
uint32_t _Chip_I2CMST_Get_Data(void *pPeri)
{
	return(((_CHIP_I2C_T *)pPeri)->MSTDAT);											// read data from I2C
}

// ------------------------------------------------------------------------------------------------
// I2C - Create START condition 
void _Chip_I2CMST_Set_Start(void *pPeri)											// create start impulse
{
	((_CHIP_I2C_T *)pPeri)->MSTCTL = (1 << 1);
}

// ------------------------------------------------------------------------------------------------
// I2C - Create STOP condition
void _Chip_I2CMST_Set_Stop(void *pPeri)
{
	((_CHIP_I2C_T *)pPeri)->MSTCTL = (1 << 2);										// create stop impulse
}

// ------------------------------------------------------------------------------------------------
// I2C - Master Continue
void _Chip_I2CMST_Set_Cont(void *pPeri)
{
	((_CHIP_I2C_T *)pPeri)->MSTCTL = (1 << 0);										// ACK
}

// ------------------------------------------------------------------------------------------------
// I2C - Reset comm.
void _Chip_I2CMST_Reset(void *pPeri)
{	// LPC546xx has no normal reset. Better solution is disable and reenable I2C periphery.
	((_CHIP_I2C_T *)pPeri)->CFG &= ~(1 << 0);										// Disable I2C
	((_CHIP_I2C_T *)pPeri)->CFG |= (1 << 0);										// Enable I2C
}


// ------------------------------------------------------------------------------------------------------
// Enable/Disable I2C - master mode
// result is true:successfull otherwise false
bool _Chip_I2CMST_Enable(void* pPeri, bool NewState)
{
	SYS_ASSERT(pPeri != NULL);
	
	if(pPeri == NULL ) return(false);
	int32_t PeriIndex = _Chip_I2C_Get_PeriIndex(pPeri);								// return peripherial index
	if((PeriIndex < 0) || (PeriIndex >= _CHIP_I2C_COUNT)) return(false);			// check or available periphery for this MCU

	if(NewState == true) 
	{
		((_CHIP_I2C_T *)pPeri)->CFG |= I2C_CFG_MSTEN(1);							// Turn On
	}
	else
	{		
		((_CHIP_I2C_T *)pPeri)->CFG &= ~I2C_CFG_MSTEN(1);							// Turn Off
	}
	
	return (true);
}

// ------------------------------------------------------------------------------------------------------
// Enable/Disable I2C - slave mode
// result is true:successfull otherwise false
bool _Chip_I2CSLV_Enable(void* pPeri, bool NewState)
{
	SYS_ASSERT(pPeri != NULL);
	
	if(pPeri == NULL ) return(false);
	int32_t PeriIndex = _Chip_I2C_Get_PeriIndex(pPeri);								// return peripherial index
	if((PeriIndex < 0) || (PeriIndex >= _CHIP_I2C_COUNT)) return(false);			// check or available periphery for this MCU

	if(NewState == true) 
	{
		((_CHIP_I2C_T *)pPeri)->CFG |= I2C_CFG_SLVEN(1);							// Turn On
	}
	else
	{		
		((_CHIP_I2C_T *)pPeri)->CFG &= ~I2C_CFG_SLVEN(1);							// Turn Off
	}
	
	return (true);
}



// ------------------------------------------------------------------------------------------------------
// Enable/Disable I2C - monitor mode
// result is true:successfull otherwise false
bool _Chip_I2CMON_Enable(void* pPeri, bool NewState)
{
	SYS_ASSERT(pPeri != NULL);
	
	if(pPeri == NULL ) return(false);
	int32_t PeriIndex = _Chip_I2C_Get_PeriIndex(pPeri);								// return peripherial index
	if((PeriIndex < 0) || (PeriIndex >= _CHIP_I2C_COUNT)) return(false);			// check or available periphery for this MCU

	if(NewState == true) 
	{
		((_CHIP_I2C_T *)pPeri)->CFG |= I2C_CFG_MONEN(1);							// Turn On
	}
	else
	{		
		((_CHIP_I2C_T *)pPeri)->CFG &= ~I2C_CFG_MONEN(1);							// Turn Off
	}
	
	return (true);
}


// ------------------------------------------------------------------------------------------------
// I2C Interrupt - enable/disable
bool _Chip_I2CMST_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	SYS_ASSERT(pPeri != NULL);
	
	if(pPeri == NULL ) return(false);
	int32_t PeriIndex = _Chip_I2C_Get_PeriIndex(pPeri);								// return peripherial index
	if((PeriIndex < 0) || (PeriIndex >= _CHIP_I2C_COUNT)) return(false);			// check or available periphery for this MCU
	
	
	if(NewState == true)
	{
		switch (PeriIndex)
		{
			case 3: {
						NVIC_ClearPendingIRQ (I2C3_IRQn);							// Clear NVIC interrupt flag
						NVIC_EnableIRQ(I2C3_IRQn);									// Enable NVIC interrupt
						break;				
					}
			case 2: {
						NVIC_ClearPendingIRQ (I2C2_IRQn);							// Clear NVIC interrupt flag
						NVIC_EnableIRQ(I2C2_IRQn);									// Enable NVIC interrupt
						break;				
					}			
			case 1: {
						NVIC_ClearPendingIRQ (I2C1_IRQn);							// Clear NVIC interrupt flag
						NVIC_EnableIRQ(I2C1_IRQn);									// Enable NVIC interrupt
						break;				
					}
			case 0: {
						NVIC_ClearPendingIRQ (I2C0_IRQn);							// Clear NVIC interrupt flag
						NVIC_EnableIRQ(I2C0_IRQn);									// Enable NVIC interrupt
						break;				
					}			
			default: return(false);
		}
	}
	else
	{
		switch (PeriIndex)
		{
			case 3: {
						NVIC_DisableIRQ(I2C3_IRQn);									// Disable NVIC interrupt
						break;
					}
			case 2: {
						NVIC_DisableIRQ(I2C2_IRQn);									// Disable NVIC interrupt
						break;				
					}
			case 1: {
						NVIC_DisableIRQ(I2C1_IRQn);									// Disable NVIC interrupt
						break;				
					}
			case 0: {
						NVIC_DisableIRQ(I2C0_IRQn);									// Disable NVIC interrupt
						break;				
					}			
			default: return(false);
		}
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------
// I2C Slave Interrupt - enable/disable
// This chip use one interrupt for MST/SLV/MOV
bool _Chip_I2CSLV_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	
	return(_Chip_I2CMST_Enable_IRQ_NVIC(pPeri, NewState));							// this MCU used same IRQ for all modes
}

// ------------------------------------------------------------------------------------------------
// I2C Monitor Interrupt - enable/disable
// This chip use one interrupt for MST/SLV/MOV
bool _Chip_I2CMON_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	return(_Chip_I2CMST_Enable_IRQ_NVIC(pPeri, NewState));							// this MCU used same IRQ for all modes
}

// ------------------------------------------------------------------------------------------------
// I2C Interrupt - enable/disable
// This chip use one interrupt for MST/SLV/MOV and for timeout also
bool _Chip_I2CTimeout_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	return(_Chip_I2CMST_Enable_IRQ_NVIC(pPeri, NewState));							// this MCU used same IRQ for all modes
}

// ------------------------------------------------------------------------------------------------------
// I2C Set I2C interface speed in kHz
// result is true:successfull otherwise false
bool _Chip_I2C_Set_BusSpeed(void *pPeri, uint32_t Speed_kHz)						// Max Speed: 100kHz, 400kHz, 1MHz
{
	uint32_t scl;
	uint32_t CLKDiv;
	uint32_t PeriCLK;
	
	SYS_ASSERT(pPeri != NULL);
	
	if(pPeri == NULL ) return(false);
	int32_t PeriIndex = _Chip_I2C_Get_PeriIndex(pPeri);								// return peripherial index
	if((PeriIndex < 0) || (PeriIndex >= _CHIP_I2C_COUNT)) return(false);			// check or available periphery for this MCU

	CLKDiv = 1;																		// start at divider = 1
	do
	{	
		((_CHIP_I2C_T*)pPeri)->CLKDIV = CLKDiv - 1;									// set new peripherial divider
		PeriCLK = _Chip_Clock_Get_I2CClk_Rate(PeriIndex);							// reload peripherial clock
		scl = (PeriCLK / (Speed_kHz * 1000)) / CLKDiv;
		scl = scl + 1;															// preset freq. have to < as requested !
		CLKDiv ++;
		if(CLKDiv > 65535) return(false);
	}while((scl < 1) || (scl > 8));													// recalculate while scl is out of range
	
	scl -= 2;																		// 0x00 is 2 clocks....
	((_CHIP_I2C_T*)pPeri)->CLKDIV = (CLKDiv - 1) & 0x0000ffff;
	_Chip_I2CM_SetDutyCycle((_CHIP_I2C_T*)pPeri, (scl / 2), (scl - (scl / 2)));
	//_Chip_I2CM_SetDutyCycle((_CHIP_I2C_T*)pPeri, scl, scl );
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// I2C Get I2C interface speed in kHz
uint32_t _Chip_I2C_Get_BusSpeed(void *pPeri)										// Return Current bus speed
{
	SYS_ASSERT(pPeri != NULL);
	if(pPeri == NULL ) return(false);
	
	int32_t PeriIndex = _Chip_I2C_Get_PeriIndex(pPeri);								// return peripherial index
	if((PeriIndex < 0) || (PeriIndex >= _CHIP_I2C_COUNT)) return(false);			// check or available periphery for this MCU
	
	uint32_t SCLL = (((_CHIP_I2C_T*)pPeri)->MSTTIME & 0x000007) + 2;				// get SCL Low
	uint32_t SCLH = ((((_CHIP_I2C_T*)pPeri)->MSTTIME & 0x000070) >> 4) + 2;			// get SCL High
	uint32_t I2CRate = _Chip_Clock_Get_I2CClk_Rate(PeriIndex);
	I2CRate = I2CRate / (((_CHIP_I2C_T*)pPeri)->CLKDIV + 1);
	I2CRate = I2CRate / (SCLL + SCLH) / 1000;
	return( I2CRate ); //return in kHz
}

// ------------------------------------------------------------------------------------------------------
// I2C Configure I2C
// result is true:successfull otherwise false
bool _Chip_I2C_Configure(void *pPeri, uint32_t Speed_kHz, bool MasterMode, bool SlaveMode, bool MonitorMode, uint16_t Address)
{
	bool res = false;
	
	SYS_ASSERT(pPeri != NULL);
	if(pPeri == NULL) return(false);
	
//	26.7.1.1 Rate calculations
//		SCL high time (in I2C function clocks) = (CLKDIV + 1) * (MSTSCLHIGH + 2)
//		SCL low time (in I2C function clocks) = (CLKDIV + 1) * (MSTSCLLOW + 2)
//		Nominal SCL rate = I2C function clock rate / (SCL high time + SCL low time)

	if (MasterMode == true)
	{
		res = _Chip_I2C_Set_BusSpeed(pPeri, Speed_kHz);
		if (Speed_kHz > 400) ((_CHIP_I2C_T*)pPeri)->CFG = (1 << 5);					// Set HS Cabable and clear Master, Slave and Monitor enable
		else ((_CHIP_I2C_T*)pPeri)->CFG = 0;										// clear all enable
		
		((_CHIP_I2C_T*)pPeri)->CFG |= 0x00000008;									// EVENTTIMEOUT enable
	}
	
	return(res);
}
























// ************************************************************************************************
// UART  Functions 
// ************************************************************************************************
static const IRQn_Type s_USART_IRQS[] = USART_IRQS;



// ------------------------------------------------------------------------------------------------
// Return periphery index
int32_t _Chip_UART_Get_PeriIndex(void *pPeri)										// Return Index from pointer
{
#if _CHIP_UART_COUNT >= 3
	if((_CHIP_UART_T*) pPeri == USART2) return(2);
#endif	
#if _CHIP_UART_COUNT >= 2
	if((_CHIP_UART_T*) pPeri == USART1) return(1);
#endif	
#if _CHIP_UART_COUNT >= 1
	if((_CHIP_UART_T*) pPeri == USART0) return(0);
#endif	
	return(-1);
}


// ------------------------------------------------------------------------------------------------
// ADR DETECTION Control
bool _Chip_UART_Set_AdrDet(void* pPeri, bool NewVal)
{
	if(NewVal)
	{
		((_CHIP_UART_T *)pPeri)->CFG |= 1 << 19;									// AUTOADDR bit set
		((_CHIP_UART_T *)pPeri)->CTL |= 1 << 2;										// ADDRDET bit set
	}
	else 
	{
		((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 19);									// AUTOADDR bit clear
		((_CHIP_UART_T *)pPeri)->CTL &= ~(1 << 2);									// ADDRDET bit clear
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Write New Address 
// result is true:successfull otherwise false
bool _Chip_UART_Set_Address(void* pPeri, uint8_t NewAddress)
{
	((_CHIP_UART_T *)pPeri)->ADDR = NewAddress;										// Save address

	if(NewAddress)
	{
		_Chip_UART_Set_AdrDet(pPeri, true);											// activate ADRDET
	}
	else
	{
		_Chip_UART_Set_AdrDet(pPeri, false);										// deactivate ADRDET
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Read Address 
uint8_t _Chip_UART_Get_Address(void* pPeri)
{
	return(((_CHIP_UART_T *)pPeri)->ADDR & 0x00ff);
}


// ------------------------------------------------------------------------------------------------
// Read UART status
uint32_t _Chip_UART_Get_Peri_Status(void *pPeri)
{
	return(((_CHIP_UART_T*)pPeri)->STAT);
}


// ------------------------------------------------------------------------------------------------
// Clear UART status - clear by disable and reenable selected interrupts.
inline bool _Chip_UART_Clear_Peri_Status(void *pPeri, uint32_t NewValue)
{
	((_CHIP_UART_T*)pPeri)->STAT = NewValue;
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Get UART interrupt status
inline uint32_t _Chip_UART_Get_IRQ_Status(void *pPeri)
{
	return( (((_CHIP_UART_T *)pPeri)->INTSTAT) & (((_CHIP_UART_T *)pPeri)->INTENSET) );	// status masked with Enabled
}


// ------------------------------------------------------------------------------------------------
// Clear interrupt status - clear by disable and reenable selected interrupts.
inline bool _Chip_UART_Clear_IRQ_Status(void *pPeri,uint32_t IRQBitMask)
{
	return(true);																	// Doesn't support
}

// ------------------------------------------------------------------------------------------------
inline void _Chip_UART_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState)	// Enable/Disable UARTs Interrupts 
{
	if(NewState) ((_CHIP_UART_T*)pPeri)->INTENSET = IRQBitMask;
	else ((_CHIP_UART_T*)pPeri)->INTENCLR = IRQBitMask;
}

// ------------------------------------------------------------------------------------------------------
// Write char to OutBuff
inline void _Chip_UART_Put_Data(void *pPeri, uint32_t WrData)
{
	((_CHIP_UART_T *)pPeri)->TXDAT = WrData & 0x1FF;								// write data to UART - 9-bit value
}

// ------------------------------------------------------------------------------------------------------
// Read char from InBuff
inline uint32_t _Chip_UART_Get_Data(void *pPeri)
{
	return(((_CHIP_UART_T *)pPeri)->RXDAT & 0x1ff);									// read data from UART - 9-bit value
}	


// ------------------------------------------------------------------------------------------------
// Flush Uart Tx
void _Chip_UART_Flush_Tx(void *pPeri)
{

}

// ------------------------------------------------------------------------------------------------
// Flush Uart Rx
inline void _Chip_UART_Flush_Rx(void *pPeri)
{
																					// Not yet implemented	
}







	
// ------------------------------------------------------------------------------------------------------
// UART(Num) Initialization
// return is pointyer to periphery, otherwise NULL
void *_Chip_UART_Init(int32_t PeriIndex)
{
	void *ResPtr = NULL;
	// Check:	
	SYS_ASSERT((PeriIndex < _CHIP_UART_COUNT) && (PeriIndex >= 0));
	if((PeriIndex < 0) || (PeriIndex >= _CHIP_UART_COUNT)) return(false);			// check or available periphery for this MCU

#if _CHIP_UART_COUNT >= 3
	if(PeriIndex == 2)
	{
	// Power:
	// nothing to power up/down
		
	// Clock:		
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_UART2(1);					// enable clock

	// Reset:
		NVIC_DisableIRQ( (IRQn_Type) (USART2_IRQn));								// Disable NVIC interrupt
		SYSCON->PRESETCTRL0 &= ~SYSCON_PRESETCTRL0_UART2_RST_N(1);__nop();			// generate reset
		SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_UART2_RST_N(1);					// release reset
	
	// Feedback?
		// nothing
		
		NVIC_ClearPendingIRQ(USART2_IRQn);											// clear pending interrupt flag
		NVIC_DisableIRQ((IRQn_Type) (USART2_IRQn));									// Enable interrupt
		ResPtr = (void *) USART2;													// return address of periphery
	}
#endif

#if _CHIP_UART_COUNT >= 2
	if(PeriIndex == 1)
	{
	// Power:
	// nothing to power up/down
		
	// Clock:		
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_UART1(1);					// enable clock

	// Reset:
		NVIC_DisableIRQ( (IRQn_Type) (USART1_IRQn));								// Disable NVIC interrupt
		SYSCON->PRESETCTRL0 &= ~SYSCON_PRESETCTRL0_UART1_RST_N(1);__nop();			// generate reset
		SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_UART1_RST_N(1);					// release reset

	// Clear IRQ:
		NVIC_ClearPendingIRQ(USART1_IRQn);											// clear pending interrupt flag

	// Disable IRQ
		NVIC_DisableIRQ(USART1_IRQn);												// Enable interrupt
	
	// Feedback?
		// nothing

		ResPtr = (void *) USART1;													// return address of periphery
	}
#endif

#if _CHIP_UART_COUNT >= 1
	if(PeriIndex == 0)
	{
	// Power:
	// nothing to power up/down
		
	// Clock:		
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_UART0(1);					// enable clock

	// Reset:
		NVIC_DisableIRQ( (IRQn_Type) (USART0_IRQn));								// Disable NVIC interrupt
		SYSCON->PRESETCTRL0 &= ~SYSCON_PRESETCTRL0_UART0_RST_N(1);__nop();			// generate reset
		SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_UART0_RST_N(1);					// release reset

	// Clear IRQ:
		NVIC_ClearPendingIRQ(USART0_IRQn);											// clear pending interrupt flag

	// Disable IRQ
		NVIC_DisableIRQ(USART0_IRQn);												// Enable interrupt
		
	// Feedback?
		// nothing
		
		ResPtr = (void *) USART0;													// return address of periphery
	}
#endif

	return(ResPtr);
}


// ------------------------------------------------------------------------------------------------
// UART Interrupt - enable/disable
 bool _Chip_UART_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	int32_t PeriIndex = _Chip_UART_Get_PeriIndex(pPeri);

	// Check:
	SYS_ASSERT( pPeri != NULL);														// check
	
	if(NewState == true)
	{
		NVIC_ClearPendingIRQ (s_USART_IRQS[PeriIndex]);								// Clear NVIC interrupt flag
		NVIC_EnableIRQ(s_USART_IRQS[PeriIndex]);									// Enable NVIC interrupt
	}
	else
	{
		NVIC_DisableIRQ(s_USART_IRQS[PeriIndex]);									// Enable NVIC interrupt
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Enable/Disable UART
bool _Chip_UART_Enable(void *pPeri, bool NewState)
{	
	// Check:
	SYS_ASSERT(pPeri != NULL);
	int32_t PeriIndex = _Chip_UART_Get_PeriIndex(pPeri);
	if((PeriIndex < 0) || (PeriIndex >= _CHIP_UART_COUNT)) return(false);			// check or available periphery for this MCU
	
	if(NewState == true)
	{
		uint16_t waitfor = 5000;
		do
		{
			if(waitfor) waitfor --;
			else 
			{
				if((((_CHIP_UART_T *)pPeri)->CFG & 0x01) == NewState) return(true);	// Change was accepted?
				else return(false);
			}
		}while((((_CHIP_UART_T *)pPeri)->STAT & (USART_STAT_RXIDLE(1) | USART_STAT_TXIDLE(1))) != (USART_STAT_RXIDLE(1) | USART_STAT_TXIDLE(1)));			// wait for ones
		
		((_CHIP_UART_T *)pPeri)->CFG |= 0x01;										// Enable UART
		_Chip_UART_Enable_IRQ(pPeri, USART_INTENSET_RXRDYEN(1), true);				// Enable RX interrupt (Tx will enabled in send routine!
		NVIC_EnableIRQ(s_USART_IRQS[PeriIndex]);										// Enable NVIC interrupt
		NVIC_ClearPendingIRQ(s_USART_IRQS[PeriIndex]);								// Clear pending NVIC interrupt
	}
	else
	{
		((_CHIP_UART_T *)pPeri)->CFG &= ~0x01;										// Disable UART
		_Chip_UART_Enable_IRQ(pPeri, USART_INTENSET_TXRDYEN(1) | USART_INTENSET_RXRDYEN(1), false);	// Disable RX and Tx peripherial interrupt
		NVIC_DisableIRQ(s_USART_IRQS[PeriIndex]);										// Disable NVIC interrupt
	}

	NVIC_ClearPendingIRQ(s_USART_IRQS[PeriIndex]);									// clear interrupt pending flag

	if((((_CHIP_UART_T *)pPeri)->CFG & 0x01) == NewState) return(true);				// Change was accepted?
	return(false);
}









// ------------------------------------------------------------------------------------------------------
// Baud Rate settings. In case of overflow, Input: UART CLK Selector must be set before !!!!!
// result is true:successfull otherwise false
 bool _Chip_UART_Set_Baud(void* pPeri, uint32_t NewBaud)
{
	SYS_ASSERT(pPeri != NULL);
	
    uint32_t best_diff, best_osrval, best_brgval;
    uint32_t diff, brgval, osrval, baudrate;
	uint32_t srcClock_Hz;
	bool bCanDiv = false;
	
	srcClock_Hz = _Chip_Clock_Get_MainClk_Rate();									// Get Main clock rate
	
    // check arguments
    if ((pPeri == NULL) || (NewBaud == 0U) || (srcClock_Hz == 0U))
    {
        return (false);
    }
	
	
	int32_t PeriIndex = _Chip_UART_Get_PeriIndex(pPeri);

#if defined (CONF_CHIP_ID_LPC845M301JHI33) || \
	defined (CONF_CHIP_ID_LPC845M301JBD48) || \
	defined (CONF_CHIP_ID_LPC845M301JHI48) || \
	defined (CONF_CHIP_ID_LPC845M301JBD64)
	volatile uint32_t *UartCLKDIV_Reg = &(SYSCON->FRG[PeriIndex].FRGDIV);			// map clk reg
	uint32_t CLKDIV_Mask = SYSCON_FCLKSEL_SEL_MASK;
#else
	volatile uint32_t *UartCLKDIV_Reg = &SYSCON->UARTCLKDIV;						
	uint32_t CLKDIV_Mask = SYSCON_UARTCLKDIV_DIV_MASK;
#endif	
	
	
	if(!(*UartCLKDIV_Reg & CLKDIV_Mask))											// Divider was NOT set previous.Clock disabled
	{
IncreaseDIV:		
		*UartCLKDIV_Reg = (*UartCLKDIV_Reg & CLKDIV_Mask) + 1;						// increase divider (decrease clock rate)
		bCanDiv = true;
	}
	// init variables:
    best_diff = (uint32_t)-1;
	best_osrval = 0xf;
	best_brgval = (uint32_t)-1;
    diff = 0U;
	brgval = 0xffffffffU;
	osrval = 0U;
	baudrate = 0U;
	
	srcClock_Hz = _Chip_Clock_Get_UARTClk_Rate(PeriIndex);

			
    for (osrval = best_osrval; osrval >= 4; osrval--)
        {
            brgval = (srcClock_Hz / ((osrval + 1) * NewBaud)) - 1;
            if (brgval > 0xFFFF)
            {
                continue;
            }
            baudrate = srcClock_Hz / ((osrval + 1) * (brgval + 1));
            diff = NewBaud < baudrate ? baudrate - NewBaud : NewBaud - baudrate;
            if (diff < best_diff)
            {
                best_diff = diff;
                best_osrval = osrval;
                best_brgval = brgval;
            }
        }

        /* value over range */
        if (best_brgval > 0xFFFF)
        {
			if (bCanDiv) goto IncreaseDIV;											// Can I change DIVider?
            else return(false);
        }

        ((_CHIP_UART_T *)pPeri)->OSR = best_osrval;
        ((_CHIP_UART_T *)pPeri)->BRG = best_brgval;
		if(brgval > 0xFFFF)
			return(false);
    return (true);
}














#if defined (CONF_CHIP_ID_LPC845M301JHI33) || \
	defined (CONF_CHIP_ID_LPC845M301JBD48) || \
	defined (CONF_CHIP_ID_LPC845M301JHI48) || \
	defined (CONF_CHIP_ID_LPC845M301JBD64)
// ------------------------------------------------------------------------------------------------------
// Baud Rate settings. Get interface speed in baud
// result is true:successfull otherwise false
uint32_t _Chip_UART_Get_Baud(void* pPeri)
{
	return (0);
}
#else
// ------------------------------------------------------------------------------------------------------
// Baud Rate settings. Get interface speed in baud
// result is true:successfull otherwise false
uint32_t _Chip_UART_Get_Baud(void* pPeri)
{
    uint32_t srcClock_Hz = _Chip_Clock_Get_MainClk_Rate() / (SYSCON->UARTCLKDIV & SYSCON_UARTCLKDIV_DIV_MASK);
    uint32_t osrval, brgval, baudrate;

    // check arguments
    if ((pPeri == NULL) || (srcClock_Hz == 0U))
    {
        return (0);
    }

	osrval = ((_CHIP_UART_T *)pPeri)->OSR;
	brgval = ((_CHIP_UART_T *)pPeri)->BRG;
	
    // If synchronous master mode is enabled, only configure the BRG value.
    if ((((_CHIP_UART_T *)pPeri)->CFG & USART_CFG_SYNCEN_MASK) != 0U)
    {
        if ((((_CHIP_UART_T *)pPeri)->CFG & USART_CFG_SYNCMST_MASK) != 0U)
        {
			
			baudrate = srcClock_Hz / (brgval + 1U);
            //brgval    = srcClock_Hz / NewBaud;
            ((_CHIP_UART_T *)pPeri)->BRG = brgval - 1U;
        }
    }
    else
    {
		baudrate = srcClock_Hz / ((osrval + 1U) * (brgval + 1U));
    }

    return (baudrate);
}
#endif

// ------------------------------------------------------------------------------------------------------
// Write configuration into UART
// DataLen: 7,8,9
// Parity: 0-none, 2-Odd, 3-Even
// Stopbits: 0-1, 1-2
// result is true:successfull otherwise false
bool _Chip_UART_Set_Conf(void* pPeri, uint8_t DataLen, uint8_t Parity, uint8_t StopBits)
{
	uint32_t Config;
	
	SYS_ASSERT(pPeri != NULL);
	
	Config = (((DataLen - 7) << 2) & (3 << 2)) |  (Parity << 4) | (StopBits & (1 << 6));
	((_CHIP_UART_T*)pPeri)->CFG &= ~0x01;											// disable uart
	((_CHIP_UART_T*)pPeri)->CFG &= ~(0x7c);											// clear old settings
	((_CHIP_UART_T*)pPeri)->CFG |= Config;											// write new one
	
	return(true);
}

//// ------------------------------------------------------------------------------------------------------
//// Configure RTS as flow control for UART
//// result is true:successfull otherwise false
//// LPC8xx hasn't special configure for RTS. Only PINASSIGN select RTS for usable or not. This has been done by Signal Init (Must be called from master driver).
//bool _Chip_UART_Set_RTS_Configure(void* pPeri, uint8_t sRTS_Port, uint8_t sRTS_Pin)
//{
//	bool res = false;
//	
//	SYS_ASSERT(pPeri != NULL);
//	
//	uint32_t OldSyscon = SYSCON->SYSAHBCLKCTRL0;									// save syscon state
//	int32_t PeriIndex = _Chip_UART_Get_PeriIndex(pPeri);							// vrat index periferie
//	
//	if(pPeri)																		// check for valid periphery pointer
//	{
//		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_SWM(1);					// enable SWM clock
//		if((SWM0->PINASSIGN0 & SWM_PINASSIGN0_U0_RTS_O_MASK) != (SWM_PINASSIGN0_U0_RTS_O(0xff))) res = true;			// RTS0 is configured yet
//		SYSCON->SYSAHBCLKCTRL0 |= OldSyscon;									// restore SysCon state
//		res = true;
//	}
//	return(res);
//}


//// ------------------------------------------------------------------------------------------------------
//// Configure CTS as flow control for UART
//// result is true:successfull otherwise false
//// LPC8xx hasn't special configure for RTS. Only PINASSIGN select RTS for usable or not. This has been done by Signal Init (Must be called from master driver).
//bool _Chip_UART_Set_CTS_Configure(void* pPeri, uint8_t sCTS_Port, uint8_t sCTS_Pin)
//{
//	bool res = false;
//	
//	SYS_ASSERT(pPeri != NULL);
//	
//	uint32_t OldSyscon = SYSCON->SYSAHBCLKCTRL0;									// save syscon state
//	int32_t PeriIndex = _Chip_UART_Get_PeriIndex(pPeri);							// vrat index periferie
//	
//	if(pPeri)																		// check for valid periphery pointer
//	{
//		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_SWM(1);					// enable SWM clock
//		if((SWM0->PINASSIGN0 & SWM_PINASSIGN0_U0_CTS_I_MASK) != (SWM_PINASSIGN0_U0_CTS_I(0xff))) res = true;			// RTS0 is configured yet
//		SYSCON->SYSAHBCLKCTRL0 |= OldSyscon;									// restore SysCon state
//		((_CHIP_UART_T*)pPeri)->CFG |= USART_CFG_CTSEN(1);						// Enable CTS0
//		res = true;
//	}
//	return(res);
//}

//// ------------------------------------------------------------------------------------------------------
//// Configure DIRDE as flow control for RS485
//// result is true:successfull otherwise false
//// LPC8xx used RTS as OutputEnable
//bool _Chip_UART_Set_DIRDE_Configure(void* pPeri, uint8_t DIRDE_Port, uint8_t DIRDE_Pin, bool DIRDE_H_Active)
//{
//	bool res = false;
//	int32_t PeriIndex = _Chip_UART_Get_PeriIndex(pPeri);						// vrat index periferie
//	
//	((_CHIP_UART_T*)pPeri)->CFG |= USART_CFG_OESEL(1);							// Enable OE for UART0
//	
//	if(DIRDE_H_Active) ((_CHIP_UART_T*)pPeri)->CFG |= USART_CFG_OEPOL(1);		// OE pactive in High state
//	else ((_CHIP_UART_T*)pPeri)->CFG &= ~USART_CFG_OEPOL(1);					// OE pactive in Low state
//	res = true;
//	
//	return(res);
//}

//// ------------------------------------------------------------------------------------------------------
//// Configure RE as receive enable control in RS485 mode
//// result is true:successfull otherwise false
//// LPC8xx hasn't special configure for RE. 
//bool _Chip_UART_Set_RE_Configure(void* pPeri, uint8_t RE_Port, uint8_t RE_Pin, bool RE_H_Active)
//{
//	return(false);																	// LPC8xx hasn't RE dedicated pin
//}

// ------------------------------------------------------------------------------------------------------
// Configure RTS as flow control for UART
// result is true:successfull otherwise false
bool _Chip_UART_Set_RTS_Conf(void *pPeri, uint8_t sRTS_Port, uint8_t sRTS_Pin)
{
	((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 18);										// OETA - RS485 OFF
	((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 20);										// Output Disable. Use only RTS

	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Configure CTS as flow control for UART
// result is true:successfull otherwise false
bool _Chip_UART_Set_CTS_Conf(void *pPeri, uint8_t sCTS_Port, uint8_t sCTS_Pin)
{
	((_CHIP_UART_T *)pPeri)->CFG |= 1 << 9;											// Enable CTS
//	else ((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 9);									// Disable CTS	

	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Configure DIRDE as flow control for RS485
// result is true:successfull otherwise false
// LPC17xx used RTS or DTR as OutputEnable
bool _Chip_UART_Set_DIRDE_Conf(void *pPeri, uint8_t DIRDE_Port, uint8_t DIRDE_Pin, bool DIRDE_H_Active)
{
	((_CHIP_UART_T *)pPeri)->CFG |= 1 << 18;										// OETA - RS485 turnaround
	
	((_CHIP_UART_T *)pPeri)->CFG |= (1 << 20);										// Output Enable select by RTS
	if(DIRDE_H_Active == 1) ((_CHIP_UART_T *)pPeri)->CFG |= 1 << 21;				// OEPOLarity depends on Signal Active state active High
	else ((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 21);								// OEPOLarity depends on Signal Active state active Low

	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Configure RE as receive enable control in RS485 mode
// result is true:successfull otherwise false
bool _Chip_UART_Set_RE_Conf(void *pPeri, uint8_t RE_Port, uint8_t RE_Pin, bool RE_H_Active)
{
	return(false);																	// LPC546xx hasn't RE dedicated pin
}
























// ******************************************************************************************************
// SPI Functions
// ******************************************************************************************************

static const IRQn_Type s_SPI_IRQS[] = SPI_IRQS;


// ------------------------------------------------------------------------------------------------
// SPI - Get peripharial index by ptr
int32_t _Chip_SPI_Get_PeriIndex(void *pPeri)							// Return Index from pointer
{	
#if _CHIP_SPI_COUNT == 3
	if((_CHIP_SPI_T*) pPeri == SPI2) return(2);
#elif _CHIP_SPI_COUNT == 2	
	if((_CHIP_SPI_T*) pPeri == SPI1) return(1);
#elif _CHIP_SPI_COUNT == 1	
	if((_CHIP_SPI_T*) pPeri == SPI0) return(0);
#endif	
	return(-1);																		// otherwise error
}


// ------------------------------------------------------------------------------------------------------
// SPI(Num) Initialization
// result is true:successfull otherwise false
void *_Chip_SPI_Init(int32_t PeriIndex)
{
	void *ResPtr = NULL;
	// Check:	
	SYS_ASSERT((PeriIndex < _CHIP_SPI_COUNT) && (PeriIndex >= 0));
	if((PeriIndex < 0) || (PeriIndex >= _CHIP_SPI_COUNT)) return(false);			// check or available periphery for this MCU
	
#if _CHIP_SPI_COUNT >= 2
	if(PeriIndex == 1)
	{
	// Power:
	// nothing to power up/down
		
	// Clock:		
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_SPI1(1);					// enable clock

	// Reset:
		NVIC_DisableIRQ( (IRQn_Type) (s_SPI_IRQS[PeriIndex]));						// Disable NVIC interrupt
		SYSCON->PRESETCTRL0 &= ~SYSCON_PRESETCTRL0_SPI1_RST_N(1);__nop();			// generate reset
		SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_SPI1_RST_N(1);					// release reset

		// Clear IRQ:
		NVIC_ClearPendingIRQ(SPI1_IRQn);											// clear pending interrupt flag

		// Disable IRQ
		NVIC_DisableIRQ(SPI1_IRQn);													// Enable NVIC interrupt
		
	// Feedback?
		// nothing
		ResPtr = (void *) SPI1;														// return address of periphery
	}
#endif

#if _CHIP_SPI_COUNT >= 1
	if(PeriIndex == 0)
	{
	// Power:
	// nothing to power up/down
		
	// Clock:		
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_SPI0(1);					// enable clock

	// Reset:
		NVIC_DisableIRQ( (IRQn_Type) (s_SPI_IRQS[PeriIndex]));						// Disable NVIC interrupt
		SYSCON->PRESETCTRL0 &= ~SYSCON_PRESETCTRL0_SPI0_RST_N(1);__nop();			// generate reset
		SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_SPI0_RST_N(1);					// release reset

		// Clear IRQ:
		NVIC_ClearPendingIRQ(SPI0_IRQn);											// clear pending interrupt flag

		// Disable IRQ
		NVIC_DisableIRQ(SPI0_IRQn);													// Enable NVIC interrupt

		
	// Feedback?
		// nothing
		ResPtr = (void *) SPI0;														// return address of periphery
	}
#endif
	return(ResPtr);
}


// ------------------------------------------------------------------------------------------------------
// SPI Enable/disable
// result is true:successfull otherwise false
bool _Chip_SPI_Enable(void *pPeri, bool NewState)
{
	SYS_ASSERT(pPeri != NULL);
	
	if(pPeri == NULL ) return(false);
	
	if(NewState)
	{
		((_CHIP_SPI_T*) pPeri)->CFG &= ~(I2C_CFG_MSTEN(1) | I2C_CFG_SLVEN(1) | I2C_CFG_MONEN(1));// disable all functions
	}
	else
	{
		((_CHIP_SPI_T*) pPeri)->CFG |= I2C_CFG_MSTEN(1) | I2C_CFG_SLVEN(1) | I2C_CFG_MONEN(1); // enable all functions
	}
	return(true);
}


// ------------------------------------------------------------------------------------------------
// SPI - Set clock divisor
void _Chip_SPI_Set_ClockDiv(void *pPeri, uint32_t clkdiv)
{
	if ((clkdiv >= 1) && (clkdiv <= 0x10000)) ((_CHIP_SPI_T*) pPeri)->DIV = clkdiv - 1;	// write to CLK DIV register
	else ((_CHIP_SPI_T*) pPeri)->DIV = 0;
}

// ------------------------------------------------------------------------------------------------
// SPI Set interface speed in Hz
// result is true:successfull otherwise false
bool _Chip_SPI_Set_BusSpeed(void *pPeri, uint32_t Speed_Hz)
{
	uint32_t CLKDiv;
	
	CLKDiv = (_Chip_Clock_Get_SysClk_Rate() / Speed_Hz) + 1;						// +1: speed maximal to Speed_Hz or LOWer!
	if(CLKDiv > 0x10000) {sys_Err("Cannot set SPI Divider!"); return(false);} 
	_Chip_SPI_Set_ClockDiv((_CHIP_SPI_T*) pPeri, CLKDiv);							// set new peripherial divider
	return(true);
}


// ------------------------------------------------------------------------------------------------
// SPI Get SPI interface speed in Hz
uint32_t _Chip_SPI_Get_BusSpeed(void *pPeri)										// Return Current bus speed
{
	return(_Chip_Clock_Get_SysClk_Rate() / (((_CHIP_SPI_T*) pPeri)->DIV + 1));		// return in Hz
}

// ------------------------------------------------------------------------------------------------------
// SPI Configure
// result is true:successfull otherwise false
bool _Chip_SPI_Configure(void *pPeri, uint32_t Speed_Hz, bool MasterMode, bool SlaveMode, bool LSB_First, bool CPHA, bool CPOL)
{
	SYS_ASSERT(pPeri != NULL);
	bool res = false;
	
	if(pPeri == NULL) return(false);
	if((MasterMode == true) && (SlaveMode == true)) return(false);					// only one mode set check....
	if((MasterMode == false) && (SlaveMode == false)) return(false);
	
	((_CHIP_SPI_T*)pPeri)->CFG = 0;													// clear old config
	
	if(CPHA == true) ((_CHIP_SPI_T*) pPeri)->CFG |= 1 << 4;							// set CPHA 1
	if(CPOL == true) ((_CHIP_SPI_T*) pPeri)->CFG |= 1 << 5;							// set CPOL 1
	
	res = _Chip_SPI_Set_BusSpeed (pPeri, Speed_Hz);									// set speed
	if (MasterMode == true)
	{
		((_CHIP_SPI_T*) pPeri)->CFG |= (1 << 2);									// set MASTER	
	}
	else
	{
		((_CHIP_SPI_T*) pPeri)->CFG &= ~(1 << 2);									// clear MASTER
	}
	
	if(LSB_First) ((_CHIP_SPI_T*) pPeri)->CFG |= (1 << 3);							// set LSBF
	else ((_CHIP_SPI_T*) pPeri)->CFG &= ~(1 << 3);									// clear LSBF
	
	((_CHIP_SPI_T*) pPeri)->DLY = 0x00002222;										// add a minimal time to delay registers 
	
	return(res);																	// set speed

}

// ------------------------------------------------------------------------------------------------
// SPI interrupt enable/disable 
bool _Chip_SPI_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	SYS_ASSERT(pPeri != NULL);

	int32_t PeriIndex = _Chip_SPI_Get_PeriIndex(pPeri);
	
	if(PeriIndex < 0) return (false);

	if(NewState == true)
	{
		NVIC_ClearPendingIRQ (SPI0_IRQn);											// Clear NVIC interrupt flag
		NVIC_EnableIRQ(SPI0_IRQn);													// Enable NVIC interrupt
	}
	else
	{
		NVIC_DisableIRQ(SPI0_IRQn);													// Disable NVIC interrupt
	}
	return(true);
}


// ------------------------------------------------------------------------------------------------
// SPI - write data into SPI datareg
void _Chip_SPI_Put_Data(void *pPeri, uint32_t NewValue, uint8_t NumOfBits, uint32_t CtrlFlags, uint8_t SSel)
{
//	SSel = SSel & 0x0f;																// maximum SSEL
//	((_CHIP_SPI_T *)pPeri)->TXCTL |= (0x000f0000);									// clear SSEL
//	((_CHIP_SPI_T *)pPeri)->TXCTL &= ~(0x0f000000);									// clear LEN
//	
//	// SSEL are inverted:
//	((_CHIP_SPI_T *)pPeri)->TXCTL |= ((0x0f - SSel) << 16) | (((NumOfBits - 1) & 0x0f) << 24);
//	((_CHIP_SPI_T *)pPeri)->TXDAT = (uint32_t) NewValue;
	
	((_CHIP_SPI_T *)pPeri)->TXDATCTL = (NewValue & 0xffff) | ((~SSel & 0x0f) << 16) | ((NumOfBits & 0x0f) << 24) | (CtrlFlags << 20);
}

// ------------------------------------------------------------------------------------------------
// SPI - Read Data from datareg
uint32_t _Chip_SPI_Get_Data(void *pPeri)
{
	return(((_CHIP_SPI_T *)pPeri)->RXDAT & 0xffff);
}

// ------------------------------------------------------------------------------------------------
// Read SPI interrupt status
uint32_t _Chip_SPI_Get_IRQ_Status(void *pPeri)
{
	return(((_CHIP_SPI_T *)pPeri)->INTSTAT);
}

// ------------------------------------------------------------------------------------------------
// Clear SPI interrupt status
bool _Chip_SPI_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
	((_CHIP_SPI_T *)pPeri)->INTENCLR |= NewValue;
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Read SPI status
uint32_t _Chip_SPI_Get_Peri_Status(void *pPeri)
{
	return(((_CHIP_SPI_T*)pPeri)->STAT);
}


// ------------------------------------------------------------------------------------------------
// clear peri status
bool _Chip_SPI_Clear_Peri_Status(void *pPeri, uint32_t BitMask)
{
	((_CHIP_SPI_T*)pPeri)->STAT |= BitMask;
	return true;
}

// ------------------------------------------------------------------------------------------------
// Enable/disable selected interrupts
bool _Chip_SPI_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_SPI_T*)pPeri)->INTENSET = IRQBitMask;
	else ((_CHIP_SPI_T*)pPeri)->INTENCLR = IRQBitMask;
	
	return(true);

}

// ------------------------------------------------------------------------------------------------
// SPI - Create End Of Transfer condition 
void _Chip_SPI_SendEOT(void *pPeri)
{
	((_CHIP_SPI_T *)pPeri)->TXCTL |= (1 << 20);	
}

// ------------------------------------------------------------------------------------------------
// SPI - Create End Of Frame condition 
void _Chip_SPI_SendEOF(void *pPeri)
{
	((_CHIP_SPI_T *)pPeri)->TXCTL |= (1 << 21);
}

// ------------------------------------------------------------------------------------------------
// SPI - Create Receive Ignore condition 
void _Chip_SPI_ReceiveIgnore(void *pPeri)
{
	((_CHIP_SPI_T *)pPeri)->TXCTL |= (1 << 22);
}

// ------------------------------------------------------------------------------------------------
// Flush SPI Tx
void _Chip_SPI_Flush_Tx(void *pPeri)
{
																					// Not yet implemented	
}

// ------------------------------------------------------------------------------------------------
// Flush SPI Rx
void _Chip_SPI_Flush_Rx(void *pPeri)
{
																					// Not yet implemented	
}











// ******************************************************************************************************
// USB Functions
// ******************************************************************************************************
// - not used -













// ******************************************************************************************************
// ADC Functions
// ******************************************************************************************************
bool _Chip_ADC_Repeat_Mode;


// ------------------------------------------------------------------------------------------------
// return ptr to periphary
int32_t _Chip_ADC_Get_PeriIndex(void *pPeri)
{
#if _CHIP_ADC_COUNT == 3
	if((_CHIP_ADC_T*) pPeri == ADC2) return(2);
#elif _CHIP_ADC_COUNT == 2	
	if((_CHIP_ADC_T*) pPeri == ADC1) return(1);
#elif _CHIP_ADC_COUNT == 1	
	if((_CHIP_ADC_T*) pPeri == ADC0) return(0);
#endif	
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------------
// ADC(Num) Initialization and enable interrupt. Not enable peripahy!
// result is true:successfull otherwise false
void *_Chip_ADC_Init(int32_t PeriIndex)
{
	// Check:			
	SYS_ASSERT((PeriIndex < _CHIP_ADC_COUNT) && (PeriIndex >= 0));
	if(PeriIndex > 0) return(NULL);
	
	// Power:
	SYSCON->PDRUNCFG &= ~SYSCON_PDRUNCFG_ADC_PD(1);									// Power On 
		
	// Clock:		
	SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_ADC(1);							// enable ADC clock
	
	// Reset:
	SYSCON->PRESETCTRL0 &= ~SYSCON_PRESETCTRL0_ADC_RST_N(1);__nop();				// generate reset
	SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_ADC_RST_N(1);							// release reset

	// Clear IRQ:
	NVIC_ClearPendingIRQ(ADC0_SEQA_IRQn);											// clear pending interrupt flag
	NVIC_ClearPendingIRQ(ADC0_SEQB_IRQn);											// clear pending interrupt flag
	NVIC_ClearPendingIRQ(ADC0_THCMP_IRQn);											// clear pending interrupt flag
	NVIC_ClearPendingIRQ(ADC0_OVR_IRQn);											// clear pending interrupt flag

	// Disable IRQ
	NVIC_DisableIRQ(ADC0_SEQA_IRQn);													// Enable NVIC interrupt
	NVIC_DisableIRQ(ADC0_SEQB_IRQn);													// Enable NVIC interrupt
	NVIC_DisableIRQ(ADC0_THCMP_IRQn);												// Enable NVIC interrupt
	NVIC_DisableIRQ(ADC0_OVR_IRQn);													// Enable NVIC interrupt
	
	// Feedback?
	// nothing

	return(ADC0);
}

// ------------------------------------------------------------------------------------------------------
// Read conversion result validity flag
bool _Chip_ADC_Get_DataValidFlag_CH( void *pPeri, uint8_t Channel)
{
	return((((_CHIP_ADC_T*) pPeri)->DAT[Channel] & ADC_DAT_DATAVALID_MASK) >> ADC_DAT_DATAVALID_SHIFT);
}

// ------------------------------------------------------------------------------------------------------
// Read conversion result on seleected channel
uint32_t _Chip_ADC_Get_Data_CH( void *pPeri, uint8_t Channel)
{
	return((((_CHIP_ADC_T*) pPeri)->DAT[Channel] & ADC_DAT_RESULT_MASK) >> ADC_DAT_RESULT_SHIFT);
}

// ------------------------------------------------------------------------------------------------------
// Read active channel on ADC
// read from SWM configuration for each ADC Channel
uint32_t _Chip_ADC_Get_ChannelMask(void *pPeri)
{
	uint32_t res = 0;
	SYS_ASSERT(pPeri != NULL);
	
	int32_t PeriIndex = _Chip_ADC_Get_PeriIndex(pPeri);								// return peripherial index
	bool SWM_Was_Enabled = SYSCON->SYSAHBCLKCTRL0 & SYSCON_SYSAHBCLKCTRL0_SWM_MASK ? true:false;
		
	SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_SWM(1);							// enable SWM clock

	switch(PeriIndex)
	{
		case 1:
		{
			for(uint8_t bit=13 ; bit<=23 ; bit++ )									// depend on each MCU!
			{
				if(_Chip_SWM_IsFixedPinEnabled((uint8_t) bit) == true )	// fixed pin function is enabled
				{
					res |= (1 << (bit - 13));
				}
			}
		}
			
		default:			// ADC = 0
		{
			for(uint8_t bit=0 ; bit<=11 ; bit++ )									// depend on each MCU!
			{
				if(_Chip_SWM_IsFixedPinEnabled((uint8_t)bit + 13) == true )			// fixed pin function is enabled
				{
					res |= (1 << bit);
				}
			}
		}
	}
	
	if(SWM_Was_Enabled == false) SYSCON->SYSAHBCLKCTRL0 &= ~SYSCON_SYSAHBCLKCTRL0_SWM(1); // disable SWM clock if has been disabled
	return(res);
}


// ------------------------------------------------------------------------------------------------
// ADC Interrupt - enable/disable
bool _Chip_ADC_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	SYS_ASSERT(pPeri != NULL);														// check

	uint8_t PeriIndex = _Chip_ADC_Get_PeriIndex(pPeri);								// return peripherial index
	
	switch(PeriIndex)
	{
		case 0:
		{
			if(NewState == true)
			{
				NVIC_ClearPendingIRQ(ADC0_SEQA_IRQn);										// clear pending NVIC interrupt flag
				NVIC_ClearPendingIRQ(ADC0_SEQB_IRQn);										// clear pending NVIC interrupt flag
				NVIC_ClearPendingIRQ(ADC0_THCMP_IRQn);										// clear pending NVIC interrupt flag
				NVIC_ClearPendingIRQ(ADC0_OVR_IRQn);										// clear pending NVIC interrupt flag
				
				// Enable IRQ:
				NVIC_EnableIRQ(ADC0_SEQA_IRQn);												// Enable NVIC interrupt
				NVIC_EnableIRQ(ADC0_SEQB_IRQn);												// Enable NVIC interrupt
				NVIC_EnableIRQ(ADC0_THCMP_IRQn);											// Enable NVIC interrupt		
				NVIC_EnableIRQ(ADC0_OVR_IRQn);												// Enable NVIC interrupt
			}
			else
			{
				NVIC_DisableIRQ(ADC0_SEQA_IRQn);											// Disable NVIC interrupt
				NVIC_DisableIRQ(ADC0_SEQB_IRQn);											// Disable NVIC interrupt
				NVIC_DisableIRQ(ADC0_THCMP_IRQn);											// Disable NVIC interrupt
				NVIC_DisableIRQ(ADC0_OVR_IRQn);												// Disable NVIC interrupt
			}
			return(true);
		}
		default: return(false);
	}
}


// ------------------------------------------------------------------------------------------------
// Power down / up
bool _Chip_ADC_Power(void *pPeri, bool NewState)
{
	int32_t PeriIndex = _Chip_ADC_Get_PeriIndex(pPeri);
	
	if(NewState) SYSCON->PDRUNCFG &= ~(1 << (10 + PeriIndex));						// Enable Power for periphery
	else  SYSCON->PDRUNCFG |= (1 << (10 + PeriIndex));								// Disable Power for periphery
	
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Enable/Disable ADC
// result is true:successfull otherwise false
bool _Chip_ADC_Enable_CH(void* pPeri, uint32_t ChannelBitMask, bool NewState)
{
	SYS_ASSERT(pPeri != NULL);
	
	if(pPeri == NULL ) return(false);
	int32_t PeriIndex = _Chip_ADC_Get_PeriIndex(pPeri);								// return peripherial index
	if(PeriIndex == -1) return(false);
	if(NewState == true)
	{
		NVIC_ClearPendingIRQ(ADC0_SEQA_IRQn);										// clear pending interrupt flag
		NVIC_ClearPendingIRQ(ADC0_SEQB_IRQn);										// clear pending interrupt flag
		NVIC_ClearPendingIRQ(ADC0_THCMP_IRQn);										// clear pending interrupt flag
		NVIC_ClearPendingIRQ(ADC0_OVR_IRQn);										// clear pending interrupt flag
		
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[ADC_SEQA_IDX] |= ADC_SEQ_CTRL_SEQ_ENA(1) | ADC_SEQ_CTRL_START(1);	// Enable and Start Sequencer A
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[ADC_SEQB_IDX] |= ADC_SEQ_CTRL_SEQ_ENA(1) | ADC_SEQ_CTRL_START(1);	// Enable and Start Sequencer B
	}
	else
	{
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[ADC_SEQA_IDX] &= ~(ADC_SEQ_CTRL_SEQ_ENA(1));	// Disable Sequencer A
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[ADC_SEQB_IDX] &= ~(ADC_SEQ_CTRL_SEQ_ENA(1));	// Disable Sequencer B				
	}

	return (true);
}

// ------------------------------------------------------------------------------------------------------
// ADC[x](ChannelBitMask) Configure whole ADC
// result is true:successfull otherwise false
bool _Chip_ADC_Configure_CH (void *pPeri, uint32_t ChannelBitMask, uint32_t Speed_kHz, bool RepeatMode, bool DMAMode)
{
	//int8_t i = _CHIP_ADC_CHANNELS_COUNT-1;
	uint32_t adc_clk_div, Reg_tmp;
	
	SYS_ASSERT(pPeri != NULL);														// check
	SYS_ASSERT(ChannelBitMask > 0);													// check

	
	((_CHIP_ADC_T *)pPeri)->INTEN = 0;												// diasable all interrupt requests
	((_CHIP_ADC_T *)pPeri)->CTRL = 0;												// Clear control  and stop ADC

	// HW Autocalibration:
	Reg_tmp = ADC_CTRL_CALMODE(1);													// Set CALMODE bit
	adc_clk_div = (uint32_t) (_Chip_Clock_Get_SysClk_Rate() / 500000);
	Reg_tmp |= ADC_CTRL_CLKDIV(adc_clk_div);										// conversion speed set to ~500kHz
	((_CHIP_ADC_T *)pPeri)->CTRL = Reg_tmp;											// one-cycle write
	{}while((((_CHIP_ADC_T *)pPeri)->CTRL & ADC_CTRL_CALMODE_MASK) == ADC_CTRL_CALMODE_MASK);	// Wait for CALMODE bit is set
		// cca 290 usec long
		
	// next configure ADC
	((_CHIP_ADC_T *)pPeri)->CTRL = 0;												// Clear control  and stop ADC
	((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[0] = (ChannelBitMask & 0x0fff);				// enable channels

//	if(((_CHIP_ADC_T *)pPeri)->CTRL & (1 << 8)) adc_rate = _Chip_Clock_Get_ADCClk_Rate(); // if ASYNC mode selected, reat input freq
//	else clksrc_rate = _Chip_Clock_Get_SysClk_Rate();								// else system_clock as source
		

	adc_clk_div = _Chip_Clock_Get_SysClk_Rate() / (Speed_kHz*1000);					// compute divider from parameter
	if(adc_clk_div > 30000000) adc_clk_div = 30000000;								// ADC Rate divider	
		
	if(adc_clk_div > 1) adc_clk_div -= 1;											// Because value in DIV reg is +1

		
	((_CHIP_ADC_T *)pPeri)->CTRL |= ADC_CTRL_CLKDIV(adc_clk_div & 0xff); 			// AD CLK must be lower or equal to 12.4 MHz. Src is PCLK.

	if(DMAMode)
	{
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[0] &= ~ADC_SEQ_CTRL_MODE(1);				// set interrupt for end of conversion of channel
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[1] &= ~ADC_SEQ_CTRL_MODE(1);				// set interrupt for end of conversion of channel
	}
	else
	{
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[0] |= ADC_SEQ_CTRL_MODE(1);				// set interrupt for end of conversion of whole sequence
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[1] |= ADC_SEQ_CTRL_MODE(1);				// set interrupt for end of conversion of whole sequence
	}
	
	((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[0] |= ADC_SEQ_CTRL_CHANNELS(ChannelBitMask);	// enable selected channels
	((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[1] |= ADC_SEQ_CTRL_CHANNELS(ChannelBitMask);	
		
	if(RepeatMode) 
	{
		_Chip_ADC_Repeat_Mode = true;
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[0] |= ADC_SEQ_CTRL_BURST(1);				// Set burst mode
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[0] &= ~ADC_SEQ_CTRL_SINGLESTEP(1);			// Clear single-step mode
	}
	else
	{
		_Chip_ADC_Repeat_Mode = false;
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[0] &= ~ADC_SEQ_CTRL_BURST(1);				// Clear burst mode
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[0] |= ADC_SEQ_CTRL_SINGLESTEP(1);			// Set single-step mode
	}
	
	return (true);
}

// ------------------------------------------------------------------------------------------------
// Get interrupt flags
uint32_t _Chip_ADC_Get_IRQ_Status_CH(_CHIP_ADC_T *pADC, uint32_t ChannelBitMask)
{
	return pADC->FLAGS & ChannelBitMask;
}


// ------------------------------------------------------------------------------------------------
// clear interrupt flags
bool _Chip_ADC_Clear_IRQ_Status_CH(_CHIP_ADC_T *pADC, uint32_t ChannelBitMask)
{
	pADC->FLAGS = ChannelBitMask;													// nothing to clear
	return(true);	
}

// ------------------------------------------------------------------------------------------------
// ADC Conversion start
void _Chip_ADC_Start_CH(void *pPeri, uint32_t ChannelBitMask)
{
	((_CHIP_ADC_T*)pPeri)->SEQ_CTRL[0] |= ADC_SEQ_CTRL_START(1);						// start immediatelly
	((_CHIP_ADC_T*)pPeri)->SEQ_CTRL[1] |= ADC_SEQ_CTRL_START(1);						// start immediatelly
}


// ------------------------------------------------------------------------------------------------
// ADC channel interrupt enable/disable for selected channel
// result is true:successfull otherwise false
bool _Chip_ADC_IRQ_Peri_Enable_CH(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState)
	{
		((_CHIP_ADC_T *)pPeri)->INTEN |= IRQBitMask;								// Set
	}
	else
	{
		((_CHIP_ADC_T *)pPeri)->INTEN &= ~IRQBitMask;								// clear
	}
	return(true);
}


// ------------------------------------------------------------------------------------------------------
// Enable/Disable on-chip temperature sensor on ADC
// result is true:successfull otherwise false
bool _Chip_Temp_Enable(bool NewState)
{
	return(false);																	// LPXC80x haven't temperature sensor
}




















// ******************************************************************************************************
// DAC
// ******************************************************************************************************
// ------------------------------------------------------------------------------------------------
// return ptr to periphary
int32_t _Chip_DAC_Get_PeriIndex(void *pPeri)
{
#if _CHIP_DAC_COUNT >= 2
	if((_CHIP_DAC_T*) pPeri == DAC1) return(1);
#elif _CHIP_DAC_COUNT >= 1
	if((_CHIP_DAC_T*) pPeri == DAC0) return(0);
#endif	
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------
// Initialize DAC
void *_Chip_DAC_Init(int32_t PeriIndex)
{
#if _CHIP_DAC_COUNT > 0	
	void *ResPtr = NULL;
#endif	

	// Check:	
	SYS_ASSERT((PeriIndex < _CHIP_DAC_COUNT) && (PeriIndex >= 0));
	if((PeriIndex < 0) || (PeriIndex >= _CHIP_DAC_COUNT)) return(false);			// check or available periphery for this MCU
	
	
#if _CHIP_DAC_COUNT >= 2
	if(PeriIndex == 1)
	{
		// Power:
		SYSCON->PDRUNCFG &= ~SYSCON_PDRUNCFG_DAC1(1);								// Enable system clock for DAC1
		// Clock:
		SYSCON->SYSAHBCLKCTRL1 |= SYSCON_SYSAHBCLKCTRL1_DAC1(1);					// Enable system clock for DAC1
		// Reset:
		SYSCON->PRESETCTRL1 &= ~SYSCON_PRESETCTRL1_DAC1_RST_N(1);__nop();			// Activate reset DAC1
		SYSCON->PRESETCTRL1 |= SYSCON_PRESETCTRL1_DAC1_RST_N(1);					// release reset
		// Feedback?
		// nothing
		NVIC_ClearPendingIRQ(DAC1_IRQn);											// clear pending interrupt flag
		NVIC_DisableIRQ((IRQn_Type) (DAC1_IRQn));									// Enable interrupt
		ResPtr = (void *) DAC1;														// return address of periphery
	}
	return(ResPtr);
#elif _CHIP_DAC_COUNT >= 1	
	if(PeriIndex == 0)
	{
		// Power:
		SYSCON->PDRUNCFG &= ~SYSCON_PDRUNCFG_DAC0(1);								// Enable system clock for DAC0
		// Clock:
		SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_DAC0(1);					// Enable system clock for DAC0
		// Reset:
		SYSCON->PRESETCTRL0 &= ~SYSCON_PRESETCTRL0_DAC0_RST_N(1);__nop();			// Activate reset DAC0
		SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_DAC0_RST_N(1);					// release reset
		// Feedback?
		// nothing
		NVIC_ClearPendingIRQ(DAC0_IRQn);											// clear pending interrupt flag
		NVIC_DisableIRQ((IRQn_Type) (DAC0_IRQn));									// Enable interrupt
		ResPtr = (void *) DAC0;														// return address of periphery
	}
	return(ResPtr);
#else	
	return(NULL);																	// otherwise error	
#endif	
}


// ------------------------------------------------------------------------------------------------------
// DAC Enable/Disable
bool _Chip_DAC_Enable(void *pPeri, bool NewState)
{
	uint32_t PeriIndex = _Chip_DAC_Get_PeriIndex(pPeri);
	
#if _CHIP_DAC_COUNT >= 2	
	if(PeriIndex == 1)
	{
		// Enable:
		if(NewState == true) ((_CHIP_DAC_T *)pPeri)->CTRL |= DAC_CTRL_CNT_ENA(1);
		else ((_CHIP_DAC_T *)pPeri)->CTRL &= ~DAC_CTRL_CNT_ENA(1);
		return(true);
	}
#elif _CHIP_DAC_COUNT >= 1	
	if(PeriIndex == 0)
	{
		// Enable:
		if(NewState == true) ((_CHIP_DAC_T *)pPeri)->CTRL |= DAC_CTRL_CNT_ENA(1);
		else ((_CHIP_DAC_T *)pPeri)->CTRL &= ~DAC_CTRL_CNT_ENA(1);
		return(true);
	}
#endif	
	return(false);																	// otherwise error		
}

// ------------------------------------------------------------------------------------------------------
// DAC Enable/Disable IRQ
bool _Chip_DAC_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	return(false);
}

// ------------------------------------------------------------------------------------------------
// DAC NVIC Interrupt - enable/disable
bool _Chip_DAC_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	uint32_t PeriIndex = _Chip_DAC_Get_PeriIndex(pPeri);
	
#if _CHIP_DAC_COUNT >= 2	
	if(PeriIndex == 1)
	{
		NVIC_ClearPendingIRQ(DAC1_IRQn);											// clear pending interrupt flag
		if(NewState == true) NVIC_EnableIRQ((IRQn_Type) (DAC1_IRQn));				// Enable interrupt
		else NVIC_DisableIRQ((IRQn_Type) (DAC1_IRQn));								// Disable interrupt
		return(true);
	}
#elif _CHIP_DAC_COUNT >= 1	
	if(PeriIndex == 0)
	{
		NVIC_ClearPendingIRQ(DAC0_IRQn);											// clear pending interrupt flag
		if(NewState == true) NVIC_EnableIRQ((IRQn_Type) (DAC0_IRQn));				// Enable interrupt
		else NVIC_DisableIRQ((IRQn_Type) (DAC0_IRQn));								// Disable interrupt
		return(true);
	}
#endif	
	return(NULL);																	// otherwise error		
}
	


// ------------------------------------------------------------------------------------------------------
// Write data to DAC
void _Chip_DAC_Put_Data(void *pPeri, uint32_t WrData)
{
#if _CHIP_DAC_COUNT >= 1
	((_CHIP_DAC_T *)pPeri)->CR = (WrData & 0x0000ffff) << 5;
#endif	
}


// ------------------------------------------------------------------------------------------------------
// Get DAC Peri Status register
uint32_t _Chip_DAC_Get_Peri_Status(void *pPeri)
{
	return(0);
}

// ------------------------------------------------------------------------------------------------------
// Clear DAC Peri Status register
void _Chip_DAC_Clear_Peri_Status(void *pPeri, uint32_t IRQBitMask)
{

}

// ------------------------------------------------------------------------------------------------------
// Get DAC Interrupt Status register
uint32_t _Chip_DAC_Get_IRQ_Status(void *pPeri)
{
	return(0);
}

// ------------------------------------------------------------------------------------------------------
// Clear DAC Interrupt Status register
void _Chip_DAC_Clear_IRQ_Status(void *pPeri, uint32_t IRQBitMask)
{
	
}

// ------------------------------------------------------------------------------------------------------
// Configuration
bool _Chip_DAC_Configure(void *pPeri)
{
	return(false);
}


























// ******************************************************************************************************
// TIMER Functions - Timerx Or MRTx
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------
// return index to periphery Timer(pointer to peri)
int32_t _Chip_MRT_Get_PeriIndex(void *pPeri)
{
#if _CHIP_MRT_COUNT >= 3
	if((_CHIP_MRT_T*) pPeri == MRT2) return(2);
#elif _CHIP_MRT_COUNT >= 2	
	if((_CHIP_MRT_T*) pPeri == MRT1) return(1);
#elif _CHIP_ADC_COUNT >= 1	
	if((_CHIP_MRT_T*) pPeri == MRT0) return(0);
#endif	
	return(-1);																		// otherwise error
}	

// ------------------------------------------------------------------------------------------------------
// MRT(Num) Initialization
// result is true:successfull otherwise false
void *_Chip_MRT_Init(int32_t PeriIndex)
{
	void *ResPtr = NULL;
	
	// Check:
	SYS_ASSERT((PeriIndex < _CHIP_MRT_COUNT) && (PeriIndex >= 0));	
	if(PeriIndex != 0) return(NULL);

	
#if _CHIP_MRT_COUNT >= 1
	if(PeriIndex == 0)
	{	
		
		if((SYSCON->SYSAHBCLKCTRL0 & (1UL << 10)) == 0 )							// enabled clock ?
		{
		// Power:
		// Nothing to power On/Off
		
		// Clock:
			SYSCON->SYSAHBCLKCTRL0 |= (1UL << 10);									// enable clock
		
		// Reset:
			NVIC_DisableIRQ(MRT0_IRQn);												// Disable NVIC interrupt
			SYSCON->PRESETCTRL0 &= ~SYSCON_PRESETCTRL0_MRT0_RST_N(1);__nop();		// generate reset
			SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_MRT0_RST_N(1);				// release reset
		}
		
		// Clear IRQ:
		NVIC_ClearPendingIRQ(MRT0_IRQn);											// clear pending interrupt flag

		// Disable IRQ:
		NVIC_DisableIRQ(MRT0_IRQn);													// Enable interrupt
		ResPtr = (void *) MRT0;
	}
#endif
	return(ResPtr);
}


// ------------------------------------------------------------------------------------------------------
// MRT(Num) Enable/disable
// result is true:successfull otherwise false
// If All chanels are disable, Disable Global MRT IRQ also
bool _Chip_MRT_Enable_CH(void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
	bool res = false;
	
	SYS_ASSERT(pPeri != NULL);
	
	if(ChannelBitMask == 0) return(false);											// nothing to do!

	NVIC_ClearPendingIRQ(MRT0_IRQn);												// clear pending interrupt flag
	
	res = _Chip_MRT_Enable_IRQ_CH(pPeri, ChannelBitMask, NewState);
	_Chip_MRT_Reset_Tmr_CH(pPeri, ChannelBitMask);										// reload value and start counting
	
//	for(uint8_t Ch=0; Ch < MRT_CHANNEL_INTVAL_COUNT; Ch ++)
//	{
//		if(ChannelBitMask & (1 << Ch)) res |= _Chip_MRT_Enable_CHIRQ(pPeri, 1 << Ch, NewState);
//	}

//	if(NewState)
//	{
//		NVIC_EnableIRQ((IRQn_Type) (MRT0_IRQn));									// Enable Global Timer interrupt
//	}
//	else
//	{
//		bool EnableSome = false;
//		for(uint8_t Ch=0; Ch < MRT_CHANNEL_INTVAL_COUNT; Ch ++)
//		{
//			if((_CHIP_MRT_T*)pPeri)->CHANNEL[Ch].CTRL & MRT_CHANNEL_CTRL_INTEN(1)) EnableSome |= true;
//		}
//		if(EnableSome == false) NVIC_DisableIRQ((IRQn_Type) (MRT0_IRQn));			// Disable Global Timer interrupt
//	}
//	

	return(res);																	// nothing to check. return Seccess
}

// ------------------------------------------------------------------------------------------------
// NVIC Interrupt - enable/disable
bool _Chip_MRT_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	SYS_ASSERT(pPeri != NULL);
	
	if(NewState == true)
	{
		NVIC_ClearPendingIRQ (MRT0_IRQn);											// Clear NVIC interrupt flag
		NVIC_EnableIRQ(MRT0_IRQn);													// Enable NVIC interrupt
	}
	else
	{
		NVIC_DisableIRQ(MRT0_IRQn);													// Enable NVIC interrupt
	}
	return(true);
}


// ------------------------------------------------------------------------------------------------------
// MRT(Channel) Configure
// Channel 0-3
// if repeat==true, autoreload timer will executed, otherwise oneshot only
// result is true:successfull otherwise false
bool _Chip_MRT_Configure (void *pPeri, uint8_t Channel, uint32_t RateHz, bool RepeatMode)
{
	bool res = false;
	uint32_t reg;
	
	SYS_ASSERT(pPeri != NULL && (Channel < _CHIP_MRT_CHANNELS_COUNT));

	if (Channel >= _CHIP_MRT_CHANNELS_COUNT) return(false);							// LPC8xx have only channels 0-3
	
	uint32_t tmp = _Chip_Clock_Get_SysClk_Rate();									// Get System Clock
	tmp = tmp / RateHz;
	if(tmp <= 0x00ffffff)															// Cannot set Rate! Out of Range!!
	{
		//tmp = tmp | ((uint32_t) 1 << 31);
		((_CHIP_MRT_T*)pPeri)->CHANNEL[Channel].INTVAL = (tmp - 1); 
	
		reg = ((_CHIP_MRT_T*)pPeri)->CHANNEL[Channel].CTRL & ~0x06;					// read all nor mode
		if(RepeatMode == true) 	((_CHIP_MRT_T*)pPeri)->CHANNEL[Channel].CTRL = reg | (0x00 << 1);	
		else					((_CHIP_MRT_T*)pPeri)->CHANNEL[Channel].CTRL = reg | (0x01 << 1);
		res = _Chip_MRT_Clear_IRQ_Status_CH(pPeri, 1 << Channel);
	}

	//res = _Chip_MRT_Enable(pPeri, 1 << Channel, res);								// enable or disable interrupt for selected channel, depends on previous settings
	return (res);
}

// ------------------------------------------------------------------------------------------------------
// MRT(Channel) - write reload value
// Channel 0-3
void _Chip_MRT_Set_TmrVal(void *pPeri, uint8_t Channel, uint32_t TmrValue) 			// write a new reload value
{
	((_CHIP_MRT_T*)pPeri)->CHANNEL[Channel].INTVAL = (TmrValue | (1UL << 31UL));	// save new value and force reload
}

// ------------------------------------------------------------------------------------------------------
// MRT(Channel) - reset timer counter
// Channel 0-3
void _Chip_MRT_Reset_Tmr_CH(void *pPeri, uint32_t ChannelBitMask) 						// reload value
{
	
	if(ChannelBitMask & 0x01){((_CHIP_MRT_T*)pPeri)->CHANNEL[0].INTVAL |= (1UL << 31);}	// Force reload
	if(ChannelBitMask & 0x02){((_CHIP_MRT_T*)pPeri)->CHANNEL[1].INTVAL |= (1UL << 31);}	// Force reload
	if(ChannelBitMask & 0x04){((_CHIP_MRT_T*)pPeri)->CHANNEL[2].INTVAL |= (1UL << 31);}	// Force reload
	if(ChannelBitMask & 0x08){((_CHIP_MRT_T*)pPeri)->CHANNEL[3].INTVAL |= (1UL << 31);}	// Force reload
}

// ------------------------------------------------------------------------------------------------------
// MRT(Channel) - write reload value
// Channel 0-3
uint32_t _Chip_MRT_Get_TmrVal(void *pPeri, uint8_t Channel)							// read a reload value
{
	return(((_CHIP_MRT_T*)pPeri)->CHANNEL[Channel].INTVAL);							// return value
}


// ------------------------------------------------------------------------------------------------
// Timer Clear channel interrupt Flag for selected channel
// result is true:successfull otherwise false
bool _Chip_MRT_Clear_IRQ_Status_CH(void *pPeri, uint32_t ChannelBitMask)
{
	bool res = false;
	
	if(ChannelBitMask & 0x01){((_CHIP_MRT_T*)pPeri)->CHANNEL[0].STAT |= 1;res = true;}	// Clear Int Stat for channel
	if(ChannelBitMask & 0x02){((_CHIP_MRT_T*)pPeri)->CHANNEL[1].STAT |= 1;res = true;}	// Clear Int Stat for channel
	if(ChannelBitMask & 0x04){((_CHIP_MRT_T*)pPeri)->CHANNEL[2].STAT |= 1;res = true;}	// Clear Int Stat for channel
	if(ChannelBitMask & 0x08){((_CHIP_MRT_T*)pPeri)->CHANNEL[3].STAT |= 1;res = true;}	// Clear Int Stat for channel
	return(res);
}

// ------------------------------------------------------------------------------------------------
// Timer Get channel interrupt Flag for all channels
// result is bit position for active IRQ
uint32_t _Chip_MRT_Get_IRQ_Status_CH(void *pPeri)
{
	uint32_t Result = 0;
	if((((_CHIP_MRT_T*)pPeri)->CHANNEL[0].STAT & ((_CHIP_MRT_T*)pPeri)->CHANNEL[0].CTRL) & 0x01) Result |= 0x01;	// read INTFLAG for channel 0
	if((((_CHIP_MRT_T*)pPeri)->CHANNEL[1].STAT & ((_CHIP_MRT_T*)pPeri)->CHANNEL[1].CTRL) & 0x01) Result |= 0x02;	// read INTFLAG for channel 1
	if((((_CHIP_MRT_T*)pPeri)->CHANNEL[2].STAT & ((_CHIP_MRT_T*)pPeri)->CHANNEL[2].CTRL) & 0x01) Result |= 0x04;	// read INTFLAG for channel 2
	if((((_CHIP_MRT_T*)pPeri)->CHANNEL[3].STAT & ((_CHIP_MRT_T*)pPeri)->CHANNEL[3].CTRL) & 0x01) Result |= 0x08;	// read INTFLAG for channel 3
	return(Result);
}

// ------------------------------------------------------------------------------------------------
// Timer channel interrupt enable/disable for selected channel
// result is true:successfull otherwise false
bool _Chip_MRT_Enable_IRQ_CH(void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
	bool res = false;
	if(NewState)																	// Enable channel interrupt
	{
		if(ChannelBitMask & 0x01){((_CHIP_MRT_T*)pPeri)->CHANNEL[0].CTRL |= 1;res = true;}	// enable timer 0 interrupt
		if(ChannelBitMask & 0x02){((_CHIP_MRT_T*)pPeri)->CHANNEL[1].CTRL |= 1;res = true;}	// enable timer 1 interrupt
		if(ChannelBitMask & 0x04){((_CHIP_MRT_T*)pPeri)->CHANNEL[2].CTRL |= 1;res = true;}	// enable timer 2 interrupt
		if(ChannelBitMask & 0x08){((_CHIP_MRT_T*)pPeri)->CHANNEL[3].CTRL |= 1;res = true;}	// enable timer 3 interrupt		
	}
	else 																			// disable channel interrupt
	{
		if(ChannelBitMask & 0x01){((_CHIP_MRT_T*)pPeri)->CHANNEL[0].CTRL &= ~1;res = true;}// disable timer 0 interrupt
		if(ChannelBitMask & 0x02){((_CHIP_MRT_T*)pPeri)->CHANNEL[1].CTRL &= ~1;res = true;}// disable timer 1 interrupt
		if(ChannelBitMask & 0x04){((_CHIP_MRT_T*)pPeri)->CHANNEL[2].CTRL &= ~1;res = true;}// disable timer 2 interrupt
		if(ChannelBitMask & 0x08){((_CHIP_MRT_T*)pPeri)->CHANNEL[3].CTRL &= ~1;res = true;}// disable timer 3 interrupt
	}
	
	return(res);
}


//// ------------------------------------------------------------------------------------------------------
//// MRT(Num) Initialization
//// result is true:successfull otherwise false
//// Index is 0 for LPC15xx, Channels are 0-3
//void *_Chip_Timer_Init(uint8_t PeriIndex)
//{
//	SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_MRT(1); 						// enable MRT clock
//	SYSCON->PRESETCTRL0 &= ~SYSCON_PRESETCTRL0_MRT_RST_N(1);						// generate reset
//	SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_MRT_RST_N(1);							// release reset
//	
//	return(MRT0);																	// nothing to check. return Seccess
//}

//// ------------------------------------------------------------------------------------------------------
//// Timer channel interrupt enable/disable for selected channel
//// result is true:successfull otherwise false
//bool _Chip_Timer_Enable_CHIRQ(void *pPeri, uint8_t ChannelNum, bool NewState)
//{
//	if(ChannelNum > (MRT_CHANNEL_INTVAL_COUNT-1)) return(false);
//	if(NewState)((_CHIP_MRT_T*)pPeri)->CHANNEL[ChannelNum].CTRL |= MRT_CHANNEL_CTRL_INTEN(1);// enable timer interrupt for selected channel
//	else ((_CHIP_MRT_T*)pPeri)->CHANNEL[ChannelNum].CTRL &= ~MRT_CHANNEL_CTRL_INTEN(1);	// disable timer interrupt for selected channel
//	return(true);
//}

//// ------------------------------------------------------------------------------------------------------
//// Timer Clear channel interrupt Flag for selected channel
//// result is true:successfull otherwise false
//bool _Chip_Timer_Clear_CHIntStatus(void *pPeri, uint32_t ChannelMask, uint32_t NewValue)
//{
//	//if(Channel > (MRT_CHANNEL_INTVAL_COUNT-1)) return(false);
//	for(uint8_t i=0; i< MRT_CHANNEL_INTVAL_COUNT; i++)
//	{
//		if(ChannelMask & (1 << i)) ((_CHIP_MRT_T*)pPeri)->CHANNEL[i].STAT |= MRT_CHANNEL_STAT_INTFLAG(1); // Clear Int Stat for selected channel
//	}
//	return(true);
//}

//// ------------------------------------------------------------------------------------------------------
//// Timer Get channel interrupt Flag for selected channel
//// result is bit position for active IRQ
//uint32_t _Chip_Timer_Get_IntStatus(void *pPeri)
//{
//	uint32_t res=0;

//	for(uint8_t i=0; i< MRT_CHANNEL_INTVAL_COUNT; i++)
//	{
//		if(((_CHIP_MRT_T*)pPeri)->CHANNEL[i].STAT & MRT_CHANNEL_STAT_INTFLAG_MASK) res |= (1<<i);
//	}
//		
//	return(res);
//}








// ******************************************************************************************************
// DMA Functions
// ******************************************************************************************************
uint32_t DMA_CH_XFERLEN[_CHIP_DMA_CHANNELS_COUNT];


// ------------------------------------------------------------------------------------------------
// return pointer to periphery DMA(PeriIndex)
int32_t _Chip_DMA_Get_PeriIndex(void *pPeri)
{
#if _CHIP_DMA_COUNT >= 3
	if((_CHIP_DMA_T *) pPeri == DMA2) return(2);
#elif _CHIP_DMA_COUNT >= 2
	if((_CHIP_DMA_T *) pPeri == DMA1) return(1);
#elif _CHIP_DMA_COUNT >= 1
	if((_CHIP_DMA_T *) pPeri == DMA0) return(0);
#endif	
	return(-1);																		// otherwise error
}


// ------------------------------------------------------------------------------------------------------
// DMA(Num) Initialization
// result is true:successfull otherwise false
void *_Chip_DMA_Init(int32_t PeriIndex)
{
	// Check:
	SYS_ASSERT((PeriIndex < _CHIP_DMA_COUNT) && (PeriIndex >= 0));
	
	// Power:
	// Nothing to power On/Off
	
	// Clock:
	SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_DMA(1);							// Enable system clock for DMA
	
	// Reset:
	SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_DMA_RST_N(1);__nop();					// Activate reset
	SYSCON->PRESETCTRL0 &= ~(SYSCON_PRESETCTRL0_DMA_RST_N(1));						// Deactivate reset
	
	// Clear IRQ:
	DMA0->COMMON[0].ENABLECLR |= 0xffffffff;											// disable all channels
	NVIC_ClearPendingIRQ(DMA0_IRQn);												// clear pending interrupt flag

	// Disable IRQ
	NVIC_DisableIRQ(DMA0_IRQn);														// Enable DMA interrupt
	
	return((void *) DMA0);															// return
}

// ------------------------------------------------------------------------------------------------------
// DMA(Num) Enable/disable
// result is true:successfull otherwise false
bool _Chip_DMA_Enable_CH(void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
	if(pPeri == NULL ) return(false);

	if(NewState)
	{
		((_CHIP_DMA_T*)pPeri)->COMMON[0].ENABLESET |= ChannelBitMask;				// enable selected channels
		if(ChannelBitMask == UINT32_MAX) NVIC_EnableIRQ(DMA0_IRQn);					// enable IRQ
	}
	else
	{
		((_CHIP_DMA_T*)pPeri)->COMMON[0].ENABLECLR |= ChannelBitMask;				// disable selected channels
		if(ChannelBitMask == UINT32_MAX) NVIC_DisableIRQ(DMA0_IRQn);				// disable IRQ
	}
	return(true);																	// nothing to check. return Seccess
}

// ------------------------------------------------------------------------------------------------------
// DMA[x](Channel) Configure
// Channel 0-17
// CHAL_DMA_Xfer_t - structure with xfer parameters
// result is true:successfull otherwise false

bool _Chip_DMA_Configure (void *pPeri, uint8_t Channel, _Chip_DMA_Xfer_t *Xfer )
{
	return(false);
}


// ------------------------------------------------------------------------------------------------
// DMA Clear channel interrupt state for selected BitMasked channels
// result is true:successfull otherwise false
bool _Chip_DMA_Clear_IRQ_Status_CH(void *pPeri, uint32_t ChannelBitMask)
{
	((_CHIP_DMA_T*)pPeri)->COMMON[0].INTA |= (ChannelBitMask & 0x0003ffff);
	return(true);
}

// ------------------------------------------------------------------------------------------------
// DMA Get channel interrupt Flag for all channels
// result is bit position for active IRQ
uint32_t _Chip_DMA_Get_CHIRQ_Status(void *pPeri)
{
	return(((_CHIP_DMA_T*)pPeri)->COMMON[0].INTA);
}


// ------------------------------------------------------------------------------------------------
// DMA - Get channel control word
uint32_t _Chip_DMA_Get_CHControl(void *pPeri, uint32_t Channel)
{
	return(((_CHIP_DMA_T*)pPeri)->CHANNEL[Channel].CTLSTAT | DMA_CH_XFERLEN[Channel]);
}


// ------------------------------------------------------------------------------------------------
// DMA channel interrupt enable/disable for selected channel
// result is true:successfull otherwise false
bool _Chip_DMA_Enable_IRQ_CH(void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
	bool res = false;
		
	if(NewState)
	{
		((_CHIP_DMA_T*)pPeri)->COMMON[0].INTENSET |= ChannelBitMask & 0x0003ffff;	// enable interrupt for channels
	}
	else 
	{
		((_CHIP_DMA_T*)pPeri)->COMMON[0].INTENCLR |= ChannelBitMask & 0x0003ffff;	// disable interrupt for channels
	}
	
	return(res);
}	








// ************************************************************************************************
// PWM Functions 
// ************************************************************************************************
// - not used -







// ******************************************************************************************************
// EEPROM Functions
// ******************************************************************************************************
 
_Chip_Emul_EEPROM_t	Emul_EEPROM;

union onepage
{
	uint32_t word[128/4];
	uint8_t	 byte[128];
} ;

// ------------------------------------------------------------------------------------------------------
// EMUL_EEPROM Init
void *_Chip_Emulated_EEPROM_Init(uint32_t PeriIndex)
{
	Emul_EEPROM.pPeri = &Emul_EEPROM;
	Emul_EEPROM.Enabled = true;
	Emul_EEPROM.Busy = false;
	Emul_EEPROM.Init = true;
	return(Emul_EEPROM.pPeri);
}

// ------------------------------------------------------------------------------------------------------
// EMUL_EEPROM Enable/Disable
// Return True if Ena/Disa was successfull, otherwise false
bool _Chip_Emulated_EEPROM_Enable(void *pPeri, bool NewState)
{
	if (((_Chip_Emul_EEPROM_t *)pPeri)->pPeri != pPeri) return (false);
	
	if(((_Chip_Emul_EEPROM_t *)pPeri)->Init == true) ((_Chip_Emul_EEPROM_t *)pPeri)->Enabled = NewState;
	else return(false);
	
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// EMUL_EEPROM Enable/Disable interrupt
// Not used
bool _Chip_Emulated_EEPROM_IRQ_Enable(void *pPeri, bool NewState)
{
	if (((_Chip_Emul_EEPROM_t *)pPeri)->pPeri != pPeri) return (false);
	
	return(true);
}


/*
// ------------------------------------------------------------------------------------------------
// Write data to EMUL_EEPROM
// non interrupt access = blocking 
// Read PAGE from memory, modify dst bytes and write PAGE back to memory
bool _Chip_Emulated_EEPROM_Write(void *pPeri, uint32_t EEDst, uint8_t *pInBuff, size_t byteswr)
{
	bool res = false;
	uint32_t wrPage;
	uint32_t wrOffset;
    uint8_t	 byteshift, elSize;														// align to elSize - shift left or right
	uint32_t wrSize, wroted, nextwrite;
	uint8_t	 Buffer[_CHIP_EMUL_EEPROM_PAGESIZE] 		ATTR_ALIGNED(4);
	
	
	if((byteswr % 4) == 0) elSize = 4;												// find maximal transfer size
	else if ((byteswr % 2) == 0) elSize = 2;
	else elSize = 1;
	
	
	
	if(EEDst > _CHIP_EMUL_EEPROM_PAGESIZE) wrPage =  EEDst / _CHIP_EMUL_EEPROM_PAGESIZE;				// require read from page 1-... ?
	else wrPage = 0;																// will start read from page 0
	wrOffset = EEDst - (wrPage * _CHIP_EMUL_EEPROM_PAGE_NUM);						// select offset in page
	byteshift = (wrOffset % elSize);
	wrOffset -= byteshift;															// align to elSize
	wrSize = byteswr;																// read length. Must not exceeds PAGE_SIZE
	wroted = 0;
	
	
	do 
	{
		nextwrite = (((wrSize+byteshift) - wroted) > _CHIP_EMUL_EEPROM_PAGESIZE) ? (_CHIP_EMUL_EEPROM_PAGESIZE): (wrSize+byteshift) - wroted;	// remains part of total writed bytes
		// Read
		memcpy(&Buffer[0], (const void*) (_CHIP_EMUL_EEPROM_START + (wrPage * _CHIP_EMUL_EEPROM_PAGESIZE)), _CHIP_EMUL_EEPROM_PAGESIZE);		// Read Flash (emulated eeprom) into Buffer
		// Modify
		memcpy(&Buffer[0+byteshift], pInBuff, nextwrite-byteshift);					// save only needed count			// rewrite original content with new one
//		dbgprint("\r\n\t(_chip_lpc8xx) EEPROM Wr[%p], Page:0x%X, Offset:0x%X, 0x%X next bytes", pPeri, wrPage, wrOffset+byteshift, nextwrite);
		// Write
		
		// Prepare the Sector for erase
		res |= _Chip_IAP_PrepareSector(wrPage/_CHIP_EMUL_EEPROM_PAGESPERSECTOR, wrPage/_CHIP_EMUL_EEPROM_PAGESPERSECTOR);

		// Erase the Page
		res |= _Chip_IAP_ErasePage(wrPage, wrPage);									// erase selected page

		// Prepare the Sector for writing
		res  |= _Chip_IAP_PrepareSector(wrPage/_CHIP_EMUL_EEPROM_PAGESPERSECTOR, wrPage/_CHIP_EMUL_EEPROM_PAGESPERSECTOR);

		// Write data to address with page size
		res |= _Chip_IAP_CopyRamToFlash(_CHIP_EMUL_EEPROM_START + (wrPage * _CHIP_EMUL_EEPROM_SIZE), (uint32_t *) &Buffer[0], _CHIP_EMUL_EEPROM_PAGESIZE);

	
	
		//Chip_EEPROM_WritePageRegister(pPeri, wrOffset, &Buffer[0], elSize, nextwrite);
		//Chip_EEPROM_EraseProgramPage(pPeri, wrPage);

		wroted += nextwrite;
		wroted -= byteshift;														// byteshift correction
		wrPage ++;																	// increase to next page
		wrOffset = 0;																// next page must read from start
	}while (byteswr != wroted);
		
	return (res);
}

// ------------------------------------------------------------------------------------------------------
// Read data from EEPROM
// non interrupt access = blocking 
// EEPROM with Page access - read page which contain required data and then copy their to dst buffer
bool _Chip_Emulated_EEPROM_Read(void *pPeri, uint32_t EESrc, uint8_t *pOutBuff, size_t bytesrd)
{
	bool res = false;
	uint32_t rdPage;
	uint32_t rdOffset;
    uint8_t	 byteshift, elSize;														// align to elSize - shift left or right
	uint32_t readed;
//	uint32_t rdSize;
//	uint8_t	 Buffer[_CHIP_EMUL_EEPROM_PAGESIZE];
	
	
	if((bytesrd % 4) == 0) elSize = 4;												// find maximal transfer size
	else if ((bytesrd % 2) == 0) elSize = 2;
	else elSize = 1;
	
	if(EESrc > _CHIP_EMUL_EEPROM_PAGESIZE) rdPage =  EESrc / _CHIP_EMUL_EEPROM_PAGESIZE;				// require read from page 1-... ?
	else rdPage = 0;																// will start read from page 0
	rdOffset = EESrc - (rdPage * _CHIP_EMUL_EEPROM_PAGE_NUM);						// select offset in page
	byteshift = (rdOffset % elSize);
	rdOffset -= byteshift;															// align to elSize
	//rdSize = bytesrd;																// read length. Must not exceeds PAGE_SIZE
	readed = 0;
		
	do 
	{
//		readed += Chip_EEPROM_ReadPage(pPeri, rdOffset, rdPage, &Buffer[0], elSize, ((rdSize+byteshift > _CHIP_EMUL_EEPROM_PAGESIZE) ? (_CHIP_EMUL_EEPROM_PAGESIZE): (rdSize+byteshift)));
//		dbgprint("\r\n\t(_chip_lpc8xx) EEPROM Rd[%p], Page:0x%X, Offset:0x%X, 0x%X bytes total", pPeri, rdPage, rdOffset+byteshift, readed);
//		
//		readed -= byteshift;														// byuteshift correction
//		memcpy(pOutBuff, &Buffer[0+byteshift], readed);								// save only needed count
//		pOutBuff += readed;
//		rdPage ++;																	// increase to next page
//		rdSize = bytesrd - readed;													// remains part of total read bytes
//		rdOffset = 0;																// next page must read from start
	}while (bytesrd != readed);

	res = true;
	return (res);
}

*/


// ------------------------------------------------------------------------------------------------
// Write data to EMUL_EEPROM
// non interrupt access = blocking 
// Read PAGE from memory, modify dst bytes and write PAGE back to memory
// EEDst is relative to _CHIP_EMUL_EEPROM_START. Starting from 0, 1, 2 .... etc
bool _Chip_Emulated_EEPROM_Write(void *pPeri, uint32_t EEDst, uint8_t *pInBuff, size_t byteswr)
{
	bool res = false;
	uint32_t wrPage;
	uint32_t wrOffset;
    uint8_t	 byteshift, elSize;														// align to elSize - shift left or right
	uint32_t wrSize, wroted, nextwrite;
	uint8_t	 Buffer[_CHIP_EMUL_EEPROM_PAGESIZE] 		ATTR_ALIGNED(4);
	
	
	if((byteswr % 4) == 0) elSize = 4;												// find maximal transfer size
	else if ((byteswr % 2) == 0) elSize = 2;
	else elSize = 1;
	
	
	//EEDst += _CHIP_EMUL_EEPROM_START;												// address space starting at _CHIP_EMUL_EEPROM_START !!
	//if(EEDst > _CHIP_EMUL_EEPROM_PAGESIZE) wrPage =  EEDst / _CHIP_EMUL_EEPROM_PAGESIZE;				// require read from page 1-... ?
	//else wrPage = 0;																// will start read from page 0
	wrPage =  (EEDst + _CHIP_EMUL_EEPROM_START)/ _CHIP_EMUL_EEPROM_PAGESIZE;		// calculate flash page number
	
	//wrOffset = EEDst - (wrPage * _CHIP_EMUL_EEPROM_PAGE_NUM);						// select offset in page
	wrOffset = (EEDst + _CHIP_EMUL_EEPROM_START) - (wrPage * _CHIP_EMUL_EEPROM_PAGESIZE);	 // offset in the page
	
	byteshift = (wrOffset % elSize);												// align to element size
	wrOffset -= byteshift;															// apply align
	wrSize = byteswr;																// read length. Must not exceeds PAGE_SIZE
	wroted = 0;
	
	do 
	{
		nextwrite = (((wrSize+byteshift) - wroted) > _CHIP_EMUL_EEPROM_PAGESIZE) ? (_CHIP_EMUL_EEPROM_PAGESIZE): (wrSize+byteshift) - wroted;	// remains part of total writed bytes
		// Read
//		memcpy(&Buffer[0], (const void*) (_CHIP_EMUL_EEPROM_START + (wrPage * _CHIP_EMUL_EEPROM_PAGESIZE)), _CHIP_EMUL_EEPROM_PAGESIZE);		// Read Flash (emulated eeprom) into Buffer
		memcpy(&Buffer[0], (const void*) (wrPage * _CHIP_EMUL_EEPROM_PAGESIZE), _CHIP_EMUL_EEPROM_PAGESIZE);		// Read Flash (emulated eeprom) into Buffer
		// Modify
		memcpy(&Buffer[0+wrOffset], pInBuff, nextwrite-byteshift);					// save only needed count			// rewrite original content with new one
//		dbgprint("\r\n\t(_chip_lpc8xx) EEPROM Wr[%p], Page:0x%X, Offset:0x%X, 0x%X next bytes", pPeri, wrPage, wrOffset+byteshift, nextwrite);
		// Write
		
		// Prepare the Sector for erase
		res |= _Chip_IAP_PrepareSector(wrPage/_CHIP_EMUL_EEPROM_PAGESPERSECTOR, wrPage/_CHIP_EMUL_EEPROM_PAGESPERSECTOR);

		// Erase the Page
		res |= _Chip_IAP_ErasePage(wrPage, wrPage);									// erase selected page

		// Prepare the Sector for writing
		res  |= _Chip_IAP_PrepareSector(wrPage/_CHIP_EMUL_EEPROM_PAGESPERSECTOR, wrPage/_CHIP_EMUL_EEPROM_PAGESPERSECTOR);

		// Write data to address with page size
//		res |= _Chip_IAP_CopyRamToFlash(_CHIP_EMUL_EEPROM_START + (wrPage * _CHIP_EMUL_EEPROM_SIZE), (uint32_t *) &Buffer[0], _CHIP_EMUL_EEPROM_PAGESIZE);
		res |= _Chip_IAP_CopyRamToFlash(wrPage * _CHIP_EMUL_EEPROM_PAGESIZE, (uint32_t *) &Buffer[0], _CHIP_EMUL_EEPROM_PAGESIZE);
	
	
		//Chip_EEPROM_WritePageRegister(pPeri, wrOffset, &Buffer[0], elSize, nextwrite);
		//Chip_EEPROM_EraseProgramPage(pPeri, wrPage);

		wroted += nextwrite;
		wroted -= byteshift;														// byteshift correction
		wrPage ++;																	// increase to next page
		wrOffset = 0;																// next page must read from start
	}while (byteswr != wroted);
		
	return (res);
}

// ------------------------------------------------------------------------------------------------------
// Read data from EEPROM
// non interrupt access = blocking 
// EEPROM with Page access - read page which contain required data and then copy their to dst buffer
// EESrc is relative to _CHIP_EMUL_EEPROM_START. Starting from 0, 1, 2 .... etc
bool _Chip_Emulated_EEPROM_Read(void *pPeri, uint32_t EESrc, uint8_t *pOutBuff, size_t bytesrd)
{
	bool res = false;
	//uint32_t rdPage;
	//uint32_t rdOffset;
    //uint8_t	 byteshift, elSize;														// align to elSize - shift left or right
	//uint32_t readed;
//	uint32_t rdSize;
//	uint8_t	 Buffer[_CHIP_EMUL_EEPROM_PAGESIZE];
	
	
	//if((bytesrd % 4) == 0) elSize = 4;												// find maximal transfer size
	//else if ((bytesrd % 2) == 0) elSize = 2;
	//else elSize = 1;
	
	//if(EESrc > _CHIP_EMUL_EEPROM_PAGESIZE) rdPage =  EESrc / _CHIP_EMUL_EEPROM_PAGESIZE;				// require read from page 1-... ?
	//else rdPage = 0;																// will start read from page 0
	//rdOffset = EESrc - (rdPage * _CHIP_EMUL_EEPROM_PAGE_NUM);						// select offset in page
	//byteshift = (rdOffset % elSize);
	//rdOffset -= byteshift;															// align to elSize
	//rdSize = bytesrd;																// read length. Must not exceeds PAGE_SIZE
	//readed = 0;
		
	// Read
	memcpy(pOutBuff, (const void*) (_CHIP_EMUL_EEPROM_START + EESrc), bytesrd);		// Read Flash (emulated eeprom) into Buffer
//		dbgprint("\r\n\t(_chip_lpc8xx) EEPROM Rd[%p], Page:0x%X, Offset:0x%X, 0x%X bytes total", pPeri, rdPage, rdOffset+byteshift, readed);
		
	res = true;
	return (res);
}

































// ******************************************************************************************************
// SCT Functions 
// ******************************************************************************************************


// ------------------------------------------------------------------------------------------------
// Return Index from pointer
int32_t _Chip_SCT_Get_PeriIndex(void *pPeri)
{
#if _CHIP_SCT_COUNT == 3
	if((_CHIP_SCT_T *) pPeri == SCT2) return(2);
#elif _CHIP_SCT_COUNT == 2
	if((_CHIP_SCT_T *) pPeri == SCT1) return(1);
#elif _CHIP_SCT_COUNT == 1
	if((_CHIP_SCT_T *) pPeri == SCT0) return(0);
#endif	
return(-1);																			// otherwise error
}

// ------------------------------------------------------------------------------------------------------
// SCT(Num) Initialization
// result is true:successfull otherwise false
void* _Chip_SCT_Init(int32_t PeriIndex)
{
	void *ResPtr = NULL;

	// Check:	
	SYS_ASSERT((PeriIndex < _CHIP_SCT_COUNT) && (PeriIndex >= 0));

	if((PeriIndex < 0) || (PeriIndex >= _CHIP_SCT_COUNT)) return(NULL);				// check or available periphery for this MCU

	// Power:
	// Nothing to power On/Off	
	
	// Clock:
	SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_SCT(1);							// Enable system clock for SCT

	// Reset:
		SYSCON->PRESETCTRL0 &= ~SYSCON_PRESETCTRL0_SCT_RST_N(1);__nop();			// generate reset
		SYSCON->PRESETCTRL0 |= SYSCON_PRESETCTRL0_SCT_RST_N(1);						// release reset

	// Feedback?
	// SCT without any feedback

	// Clear IRQ:
	NVIC_ClearPendingIRQ(SCT0_IRQn);												// clear pending interrupt flag

	// Disable IRQ
	NVIC_DisableIRQ(SCT0_IRQn);														// Enable interrupt
		
	ResPtr = (void *) SCT0;															// return address of periphery		
	
	
	return(ResPtr);
}

// ------------------------------------------------------------------------------------------------------
// SCT(Num) Enable/disable
// result is true:successfull otherwise false
bool _Chip_SCT_Enable(void *pPeri, bool NewState)
{
	SYS_ASSERT(pPeri != NULL);
	if(pPeri == NULL ) return(false);
	
	if(NewState)
	{
		((_CHIP_SCT_T*)pPeri)->CTRL &= ~((SCT_CTRL_HALT_L(1)) | (SCT_CTRL_HALT_H(1)));// Halt SCT
	}
	else
	{
		((_CHIP_SCT_T*)pPeri)->CTRL |= (SCT_CTRL_HALT_L(1)) | (SCT_CTRL_HALT_H(1));	// unHalt SCT
	}	
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// SCT(PeriIndex) Configure
bool _Chip_SCT_Configure(void *pPeri)
{
	//_CHIP_SCT_T *pPeri = _Chip_SCT_Get_pPeri(PeriIndex);
	
	//hmmmmmm, co s tymto ?????????
	
	return(true);
}



// ******************************************************************************************************
// SWM Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Assign movable pin function to physical pin in Switch Matrix
// RegNumBlock - cislo cieloveho registra PINASSIGN a cislo byte v 32-bit registri pre dan� pin (0-3)
// RegVal - hodnota registra PINASSIGN. Urcuje zvoleny PIO
// Ex: _Chip_SWM_MovablePinAssign(0x42, 0x10)
//		- zapise do registra PINASSIGN[4] na bity 16-23 novu hodnotu 0x10
void _Chip_SWM_MovablePinAssign(uint8_t Reg_NumBlock, uint8_t NewRegVal)
{
	uint32_t RegVal;
	uint8_t RegIdx = (Reg_NumBlock >> 4) & 0x0f;									// extrahuj cislo cieloveho PINASSIGN registra
	uint8_t RegBlock = Reg_NumBlock & 0x0f;											// extrahuj cislo bloku v uint32 registri PINASSIGN
	uint8_t BitShift = RegBlock * 8;													// vypocitaj pocet bitov pre posun hodnoty na zaklade cieloveho bloku
	
	RegVal = SWM0->PINASSIGN_DATA[RegIdx];											// precitaj povodnu hodnotu cieloveho registra PINASSIGN
	RegVal = RegVal & (~(0xFF << BitShift));											// znuluj (nastav na 1) povodne nastavenie zvoleneho pinu
	RegVal = RegVal | (NewRegVal << BitShift);										// napln novu hodnotu a posun ju na spravne miesto (blok 1-4)
	
	SWM0->PINASSIGN_DATA[RegIdx] = RegVal;											// zapis novu hodnotu
	
}

// ------------------------------------------------------------------------------------------------------
// Enables a fixed function pin in the Switch Matrix
void _Chip_SWM_EnableFixedPin(uint8_t pin)
{
	uint32_t regOff, pinPos;

	pinPos = pin & 0x1F;															// read pin position [0-31]
//	regOff = 0;																		// LPC824 ahve only PINENABLE0 register!
	regOff = (pin >> 5) & 0x07;														// read register number [0-7]

	*(&SWM0->PINENABLE0 + (regOff)) &= ~(1 << pinPos);								// Set zero to enable fixed pin
}

// ------------------------------------------------------------------------------------------------------
// Disables a fixed function pin in the Switch Matrix
void _Chip_SWM_DisableFixedPin(uint8_t pin)
{
	uint32_t regOff, pinPos;

	pinPos = ((uint32_t) pin) & 0x1F;
	regOff = 0;

	*(&SWM0->PINENABLE0 + (regOff)) |= (1 << pinPos);								// Set high to disable fixed pin
}

// ------------------------------------------------------------------------------------------------------
// Enables or disables a fixed function pin in the Switch Matrix
void _Chip_SWM_FixedPinEnable(uint8_t pin, bool enable)
{
	if (enable) 
	{
		_Chip_SWM_EnableFixedPin(pin);
	}
	else 
	{
		_Chip_SWM_DisableFixedPin(pin);
	}
}

// ------------------------------------------------------------------------------------------------------
// Tests whether a fixed function pin is enabled or disabled in the Switch Matrix
bool _Chip_SWM_IsFixedPinEnabled(uint8_t pin)
{
	uint32_t regOff, pinPos;

	pinPos = ((uint32_t) pin) & 0x1F;
	regOff = ((uint32_t) pin) >> 7;

	return (bool) ((*(&SWM0->PINENABLE0 + (regOff)) & (1 << pinPos)) == 0);
}











// ******************************************************************************************************
// CRC Functions 
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// CRC(Num) Initialization
// result is true:successfull otherwise false
void* _Chip_CRC_Init(int32_t PeriIndex)
{
	void *ResPtr = NULL;

	// Check:	
	SYS_ASSERT((PeriIndex < _CHIP_CRC_COUNT) && (PeriIndex >= 0));

	if((PeriIndex < 0) || (PeriIndex >= _CHIP_CRC_COUNT)) return(NULL);				// check or available periphery for this MCU

	// Power:
	// Nothing to power On/Off	
	
	// Clock:
	SYSCON->SYSAHBCLKCTRL0 |= SYSCON_SYSAHBCLKCTRL0_CRC0(1);						// Enable system clock for CRC

	// Reset:
	// Nothing 
	
	// Feedback?
	// Without any feedback

	// Clear IRQ:
	// Nothing 

	// Disable IRQ
	// Nothing 
		
	ResPtr = (void *) CRC;															// return address of periphery		
	
	
	return(ResPtr);
}

#endif
