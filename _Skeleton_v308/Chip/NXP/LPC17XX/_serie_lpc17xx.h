// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: _serie_lpc17xx.h
// 	   Version: 2.0
//  Created on: 01.05.2014
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: MCU of series LPC8xx description/configuration file. Same for all MCUs of this family!
//              Special, device depents features, can be included outside of this file (in parent header)        
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:   01.05.2014  - v1.0: first revision. 
//              06.05.2018  - v2.0: chip manufacturer rename, skeleton system re-directories 


#ifndef __SERIE_LPC17XX_H_
#define __SERIE_LPC17XX_H_

#ifndef LOAD_SCATTER
    extern uint32_t SystemCoreClock;													// for IAP - System Clock Frequency (Core Clock)
    extern uint32_t	OscRateIn;
    extern uint32_t RTCOscRateIn;

    // for compatibility with mfg's header file
    typedef enum {ERROR = 0, SUCCESS = !ERROR} Status;
    typedef enum {RESET = 0, SET = !RESET} FlagStatus, IntStatus, SetState;
    typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;

    #define CORE_M3							1

#if defined( CONF_CHIP_ID_LPC1774FBD144 )
	#include    "chip_lpc177x_8x.h"													// CMSIS Peripheral Access Layer
#elif defined( CONF_CHIP_ID_LPC1774FBD208 )
	#include    "chip_lpc177x_8x.h"													// CMSIS Peripheral Access Layer
#elif defined( CONF_CHIP_ID_LPC1776FET180 )
	#include    "chip_lpc177x_8x.h"													// CMSIS Peripheral Access Layer
#elif defined( CONF_CHIP_ID_LPC1776FBD208 )
	#include    "chip_lpc177x_8x.h"													// CMSIS Peripheral Access Layer
#elif defined( CONF_CHIP_ID_LPC1777FBD208 )
	#include    "chip_lpc177x_8x.h"													// CMSIS Peripheral Access Layer	
#elif defined( CONF_CHIP_ID_LPC1778FBD144 )
	#include    "chip_lpc177x_8x.h"													// CMSIS Peripheral Access Layer
#elif defined( CONF_CHIP_ID_LPC1778FET180 )
	#include    "chip_lpc177x_8x.h"													// CMSIS Peripheral Access Layer
#elif defined( CONF_CHIP_ID_LPC1778FET208 )
	#include    "chip_lpc177x_8x.h"													// CMSIS Peripheral Access Layer
#elif defined( CONF_CHIP_ID_LPC1778FBD208 )
	#include    "chip_lpc177x_8x.h"													// CMSIS Peripheral Access Layer
#elif defined( CONF_CHIP_ID_LPC1785FBD208 )
	#include    "chip_lpc177x_8x.h"													// CMSIS Peripheral Access Layer
#elif defined( CONF_CHIP_ID_LPC1786FBD208 )
	#include    "chip_lpc177x_8x.h"													// CMSIS Peripheral Access Layer
#elif defined( CONF_CHIP_ID_LPC1787FBD208 )
	#include    "chip_lpc177x_8x.h"													// CMSIS Peripheral Access Layer
#elif defined( CONF_CHIP_ID_LPC1788FBD144 )
	#include    "chip_lpc177x_8x.h"													// CMSIS Peripheral Access Layer
#elif defined( CONF_CHIP_ID_LPC1788FET180 )
	#include    "chip_lpc177x_8x.h"													// CMSIS Peripheral Access Layer
#elif defined( CONF_CHIP_ID_LPC1788FET208 )
	#include    "chip_lpc177x_8x.h"													// CMSIS Peripheral Access Layer
#elif defined( CONF_CHIP_ID_LPC1788FBD208 )
	#include    "chip_lpc177x_8x.h"													// CMSIS Peripheral Access Layer
#else
	#error "Unknown selected device!"
#endif
#endif	// LOAD_SCATTER


// ******************************************************************************************************
// PUBLIC Values for all LPC17XX MCU's
// ******************************************************************************************************

#define	_CHIP_IRC_FREQUENCY				12000000UL									// Internal Osciltor: 12MHz

// for whole family LPC17xx:
#define  _CHIP_HAVE_GPIO				1											// General purpose IO
#define  _CHIP_HAVE_USB					1											// Universal Serial Bus
#define  _CHIP_HAVE_DMA					1											// Direct Memory Access
#define  _CHIP_HAVE_RTC					1											// Real Time Clock (RTC)
#define  _CHIP_HAVE_SYSTICK				1											// System Tick Timer is an integral part of the Cortex-M3. The System Tick Timer is intended to generate a fixed 10 millisecond interrupt for use by an operating system or other system management software.
#define  _CHIP_HAVE_WWDT				1											// Windowed Watchdog Timer (WWDT)
#define  _CHIP_HAVE_UART				1											// Universal asynchronous receiver-transmitter
#define  _CHIP_HAVE_SPI					1											// Serial Peripheral Interface
#define  _CHIP_HAVE_I2C					1											// A typical I2C-bus
#define  _CHIP_HAVE_CAN					1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
#define  _CHIP_HAVE_ADC					1											// Analog-to-Digital Converter (ADC)
#define  _CHIP_HAVE_DAC					1											// Digital-to-Analog Converter (DAC)
#define  _CHIP_HAVE_CRC					1											// Cyclic Redundancy Check (CRC) generator
#define	 _CHIP_HAVE_PWM				    1
#define	 _CHIP_HAVE_TIMER				1											// Timer 0 - 3

#define IAP_ENTRY_LOCATION        		0X1FFF1FF1UL								// Pointer to ROM IAP entry functions


// Specific parameters for each chip from this series:
#if defined( CONF_CHIP_ID_LPC1774FBD144 )
	// for LPC1774FBD144
    #define  CHIP_ID                    0x27011132                      			// Chip ID wroted in silicone
	#define  _CHIP_HAVE_ETH				0											// Ethernet
	#define  _CHIP_HAVE_QEI				0											// Quadrature Encoder Interface (QEI)

#elif defined( CONF_CHIP_ID_LPC1774FBD208 )
	// for LPC1774FBD208
    #define  CHIP_ID                    0x27011132                      			// Chip ID wroted in silicone
	#define  _CHIP_HAVE_ETH				0											// Ethernet
	#define  _CHIP_HAVE_QEI				0											// Quadrature Encoder Interface (QEI)
	
#elif defined( CONF_CHIP_ID_LPC1776FET180 )
	// for LPC1776FET180
    #define  CHIP_ID                    0x27191F43                      			// Chip ID wroted in silicone
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_QEI				1											// Quadrature Encoder Interface (QEI)	

#elif defined( CONF_CHIP_ID_LPC1776FBD208 )
	// for LPC1776FBD208
    #define  CHIP_ID                    0x27191F43                      			// Chip ID wroted in silicone
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_QEI				1											// Quadrature Encoder Interface (QEI)	

#elif defined( CONF_CHIP_ID_LPC1777FBD208 )
	// for LPC1777FBD208
    #define  CHIP_ID                    0x27193747                      			// Chip ID wroted in silicone
	#define  _CHIP_HAVE_ETH				0											// Ethernet
	#define  _CHIP_HAVE_QEI				1											// Quadrature Encoder Interface (QEI)	
	
#elif defined( CONF_CHIP_ID_LPC1778FBD144 )
	// for LPC1778FBD144
    #define  CHIP_ID                    0x27193F47                      			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x10000										// size of internal RAM (datasheet value) - 64kB
	#define	 _CHIP_IRAMSTART			0x10000000									// Start of RAM	
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0xFC0										// size of internal EEPROM (datasheet value) - 4kB	- 4032 bytes
	#define  _CHIP_ADCS					1											// Number of Analog-to-Digital Converters (ADC)
	#define	 _CHIP_ADC_CHANNELS_COUNT	8											// number of channels per ADC
	#define	 __LPC177X_8X__		
	#define  USB_CAN_BE_DEVICE														// for lpcusblib stack
	#define  USB_CAN_BE_HOST														// for lpcusblib stack
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_QEI				1											// Quadrature Encoder Interface (QEI)	
	
#elif defined( CONF_CHIP_ID_LPC1778FET180 )
	// for LPC1778FET180
    #define  CHIP_ID                    0x27193F47                      			// Chip ID wroted in silicone
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_QEI				1											// Quadrature Encoder Interface (QEI)	
	
#elif defined( CONF_CHIP_ID_LPC1778FET208 )
	// for LPC1778FET208
    #define  CHIP_ID                    0x27193F47                      			// Chip ID wroted in silicone
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_QEI				1											// Quadrature Encoder Interface (QEI)	

#elif defined( CONF_CHIP_ID_LPC1778FBD208 )
	// for LPC1778FBD208
    #define  CHIP_ID                    0x27193F47                      			// Chip ID wroted in silicone
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_QEI				1											// Quadrature Encoder Interface (QEI)	
	
#elif defined( CONF_CHIP_ID_LPC1785FBD208 )
	// for LPC1785FBD208
    #define  CHIP_ID                    0x281D1743                      			// Chip ID wroted in silicone
	#define  _CHIP_HAVE_ETH				0											// Ethernet	
	#define  _CHIP_HAVE_QEI				0											// Quadrature Encoder Interface (QEI)	
	
#elif defined( CONF_CHIP_ID_LPC1786FBD208 )
	// for LPC1786FBD208
    #define  CHIP_ID                    0x281D1F43                     				// Chip ID wroted in silicone
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_QEI				1											// Quadrature Encoder Interface (QEI)	

#elif defined( CONF_CHIP_ID_LPC1787FBD208 )
	// for LPC1787FBD208
    #define  CHIP_ID                    0x281D3747                     				// Chip ID wroted in silicone
	#define  _CHIP_HAVE_ETH				0											// Ethernet
	#define  _CHIP_HAVE_QEI				1											// Quadrature Encoder Interface (QEI)	

#elif defined( CONF_CHIP_ID_LPC1788FBD144 )
	// for LPC1788FBD144
    #define  CHIP_ID                    0x281D3F47                     				// Chip ID wroted in silicone
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_QEI				1											// Quadrature Encoder Interface (QEI)	
	
#elif defined( CONF_CHIP_ID_LPC1788FET180 )
	// for LPC1788FET180
    #define  CHIP_ID                    0x281D3F47                     				// Chip ID wroted in silicone
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_QEI				1											// Quadrature Encoder Interface (QEI)	

#elif defined( CONF_CHIP_ID_LPC1788FET208 )
	// for LPC1788FET208
    #define  CHIP_ID                    0x281D3F47                     				// Chip ID wroted in silicone
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_QEI				1											// Quadrature Encoder Interface (QEI)	

#elif defined( CONF_CHIP_ID_LPC1788FBD208 )
	// for PC1788FBD208
    #define  CHIP_ID                    0x281D3F47                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x10000										// size of internal RAM (datasheet value) - 64kB
	#define	 _CHIP_IRAMSTART			0x10000000									// Start of RAM	
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0xFC0										// size of internal EEPROM (datasheet value) - 4kB	- 4032 bytes
	#define  _CHIP_ADCS					1											// Number of Analog-to-Digital Converters (ADC)
	#define	 _CHIP_ADC_CHANNELS_COUNT	8											// number of channels per ADC
	#define	 __LPC177X_8X__															// LPSUSB lib requirement
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_QEI				1											// Quadrature Encoder Interface (QEI)	
	
	#define _CHIP_DMA_CHANNELS_COUNT	GPDMA_NUMBER_CHANNELS
#else
	#error "Unknown selected device!"
#endif


	#define	 IRQHANDLER_16				_CHIP_WDT0_IRQ_Handler
	#define	 IRQHANDLER_17				_CHIP_TIMER0_IRQ_Handler
	#define	 IRQHANDLER_18				_CHIP_TIMER1_IRQ_Handler
	#define	 IRQHANDLER_19				_CHIP_TIMER2_IRQ_Handler
	#define	 IRQHANDLER_20				_CHIP_TIMER3_IRQ_Handler
	#define	 IRQHANDLER_21				_CHIP_UART0_IRQ_Handler
	#define	 IRQHANDLER_22				_CHIP_UART1_IRQ_Handler
	#define	 IRQHANDLER_23				_CHIP_UART2_IRQ_Handler
	#define	 IRQHANDLER_24				_CHIP_UART3_IRQ_Handler
	#define	 IRQHANDLER_25				_CHIP_PWM1_IRQ_Handler
	#define	 IRQHANDLER_26				_CHIP_I2C0_IRQ_Handler
	#define	 IRQHANDLER_27				_CHIP_I2C1_IRQ_Handler
	#define	 IRQHANDLER_28				_CHIP_I2C2_IRQ_Handler
	#define  IRQHANDLER_29				_Chip_Default_IRQ_Handler						// this IRQ num is unused
	#define	 IRQHANDLER_30				_CHIP_SSP0_IRQ_Handler
	#define	 IRQHANDLER_31				_CHIP_SSP1_IRQ_Handler
	#define	 IRQHANDLER_32				_CHIP_PLL0_IRQ_Handler
	#define	 IRQHANDLER_33				_CHIP_RTC0_IRQ_Handler
	#define	 IRQHANDLER_34				_CHIP_EINT0_IRQ_Handler
	#define	 IRQHANDLER_35				_CHIP_EINT1_IRQ_Handler
	#define	 IRQHANDLER_36				_CHIP_EINT2_IRQ_Handler
	#define	 IRQHANDLER_37				_CHIP_EINT3_IRQ_Handler
	#define	 IRQHANDLER_38				_CHIP_ADC0_SEQA_IRQ_Handler
	#define	 IRQHANDLER_39				_CHIP_BOD0_IRQ_Handler
	#define	 IRQHANDLER_40				_CHIP_USB0_IRQ_Handler
	#define	 IRQHANDLER_41				_CHIP_CAN0_IRQ_Handler
	#define	 IRQHANDLER_42				_CHIP_DMA0_IRQ_Handler
	#define	 IRQHANDLER_43				_CHIP_I2S0_IRQ_Handler
	#define	 IRQHANDLER_44				_CHIP_ETH0_IRQ_Handler
	#define	 IRQHANDLER_45				_CHIP_SDC0_IRQ_Handler
	#define	 IRQHANDLER_46				_CHIP_MCPWM0_IRQ_Handler
	#define	 IRQHANDLER_47				_CHIP_QEI0_IRQ_Handler
	#define	 IRQHANDLER_48				_CHIP_PLL1_IRQ_Handler
	#define	 IRQHANDLER_49				_CHIP_USB0_Act_IRQ_Handler
	#define	 IRQHANDLER_50				_CHIP_CAN0_Act_IRQ_Handler
	#define	 IRQHANDLER_51				_CHIP_UART4_IRQ_Handler
	#define	 IRQHANDLER_52				_CHIP_SSP2_IRQ_Handler
	#define	 IRQHANDLER_53				_CHIP_LCD0_IRQ_Handler
	#define	 IRQHANDLER_54				_CHIP_GPIO_IRQ_Handler
	#define	 IRQHANDLER_55				_CHIP_PWM0_IRQ_Handler
	#define	 IRQHANDLER_56				_CHIP_EEPROM0_IRQ_Handler

#ifndef LOAD_SCATTER																// this file is included from C ? not from ASM or Scatter file!

// ----------------------------------------------------------------------------
// Validate the the user's selctions
// ----------------------------------------------------------------------------
#ifndef CONF_CHIP_OSCRATEIN
	#error "CONF_CHIP_OSCRATEIN not defined"
#endif
#ifndef CONF_CHIP_EXTRATEIN
	#error "CONF_CHIP_EXTRATEIN not defined"
#endif
#ifndef CONF_CHIP_RTCRATEIN
	#error "CONF_CHIP_RTCRATEIN not defined"
#endif

// ******************************************************************************************************
// PUBLIC Values for all LPC17XX MCU's
// ******************************************************************************************************

#define CPU_NONISR_EXCEPTIONS  			(15)										// Cortex M3 exceptions /without SP!/
#define CPU_IRQ_NUMOF 					(41)										// Vendor and family specific external interrupts. See users manual!

//	Load register's definitions:
#include "sysctl_17xx_40xx.h"
#include "clock_17xx_40xx.h"
#include "iocon_17xx_40xx.h"
#include "adc_17xx_40xx.h"
#include "can_17xx_40xx.h"
#include "crc_17xx_40xx.h"
#include "dac_17xx_40xx.h"
#include "eeprom_17xx_40xx.h"
#include "emc_17xx_40xx.h"
#include "enet_17xx_40xx.h"
#include "gpdma_17xx_40xx.h"
#include "gpio_17xx_40xx.h"
#include "gpioint_17xx_40xx.h"
#include "i2c_17xx_40xx.h"
#include "i2s_17xx_40xx.h"
#include "lcd_17xx_40xx.h"
#include "mcpwm_17xx_40xx.h"
#include "pmu_17xx_40xx.h"
#include "qei_17xx_40xx.h"
#include "rtc_17xx_40xx.h"
#include "sdc_17xx_40xx.h"
#include "sdmmc_17xx_40xx.h"
#include "ssp_17xx_40xx.h"
#include "timer_17xx_40xx.h"
#include "uart_17xx_40xx.h"
#include "usb_17xx_40xx.h"
#include "wwdt_17xx_40xx.h"
#include "fmc_17xx_40xx.h"

// harmonizing names
// Some manufacturers header files, using different peripherial structure names. Now, we harmonize it into one names.
// now used from chip header file:		Harmonization to:
typedef LPC_SYSCTL_T					_CHIP_SYSCON_T;
typedef LPC_WWDT_T						_CHIP_WWDT_T;
typedef LPC_TIMER_T						_CHIP_TIM_T;
typedef LPC_USART_T						_CHIP_UART_T;
typedef LPC_MCPWM_T						_CHIP_PWM_T;
typedef LPC_I2C_T						_CHIP_I2C_T;
typedef LPC_I2S_T						_CHIP_I2S_T;
typedef LPC_RTC_T						_CHIP_RTC_T;
typedef LPC_GPIOINT_T					_CHIP_GPIOINT_T;
typedef LPC_IOCON_T						_CHIP_IOCON_T;
typedef LPC_SSP_T						_CHIP_SSP_T;
typedef LPC_ADC_T						_CHIP_ADC_T;
typedef LPC_DAC_T						_CHIP_DAC_T;
//typedef LPC_CANAF_RAM_T
//typedef LPC_CANAF_T
//typedef LPC_CANCR_T
typedef LPC_CAN_T						_CHIP_CAN_T;
typedef LPC_MCPWM_T						_CHIP_MCPWM_T;
typedef LPC_QEI_T						_CHIP_QEI_T;
//typedef LPC_MCI_T
typedef LPC_GPDMA_T						_CHIP_DMA_T;     
//typedef LPC_GPDMACH_T
typedef LPC_ENET_T						_CHIP_ETH_T;
typedef LPC_LCD_T						_CHIP_LCD_T;       
typedef LPC_USB_T						_CHIP_USB_T;
typedef LPC_GPIO_T						_CHIP_GPIO_T;
typedef LPC_EMC_T						_CHIP_EMC_T;       
typedef LPC_CRC_T						_CHIP_CRC_T;
typedef LPC_EEPROM_T					_CHIP_EEPROM_T;
typedef LPC_SSP_T						_CHIP_SPI_T;


// ******************************************************************************************************
// IAP CHIP functions
// ******************************************************************************************************
#define _CHIP_IAP_PREWRRITE_CMD           50										// Prepare sector for write operation command
#define _CHIP_IAP_WRISECTOR_CMD           51										// Write Sector command
#define _CHIP_IAP_ERSSECTOR_CMD           52										// Erase Sector command
#define _CHIP_IAP_BLANK_CHECK_SECTOR_CMD  53										// Blank check sector
#define _CHIP_IAP_REPID_CMD               54										// Read PartID command
#define _CHIP_IAP_READ_BOOT_CODE_CMD      55										// Read Boot code version
#define _CHIP_IAP_COMPARE_CMD             56										// Compare two RAM address locations
#define _CHIP_IAP_REINVOKE_ISP_CMD        57										// Reinvoke ISP
#define _CHIP_IAP_READ_UID_CMD            58										// Read UID
//#define _CHIP_IAP_ERASE_PAGE_CMD          59										// Erase page
//#define _CHIP_IAP_EEPROM_WRITE            61										// EEPROM Write command
//#define _CHIP_IAP_EEPROM_READ             62										// EEPROM READ command

typedef void (*IAP_ENTRY_T)(unsigned int[], unsigned int[]);

static SK_SK_INLINE void _Chip_IAP_Entry(unsigned int cmd_param[], unsigned int status_result[])
{
	((IAP_ENTRY_T) IAP_ENTRY_LOCATION)(cmd_param, status_result);
}

// ******************************************************************************************************
// CHIP functions
// ******************************************************************************************************
// Public functions exactly for this series of MCU, but uniform with CHAL System:
extern bool _Chip_IAP_Read_ID(uint32_t *pDst);
extern bool _Chip_IAP_Read_SerialNum(uint32_t *pDst);
extern bool _Chip_IAP_Read_BootCodeVersion(uint32_t *pDst);

// Call System Init - in reset vector!
extern inline void _Chip_SystemInit(void)											// inserted into SystemInit
{
	SCnSCB->ACTLR |= 1 << SCnSCB_ACTLR_DISDEFWBUF_Pos;								// disable write buffering - DEBUG ONLY !!!! 

	SCB->SHCSR |= SCB_SHCSR_USGFAULTENA_Msk
		| SCB_SHCSR_BUSFAULTENA_Msk
		| SCB_SHCSR_MEMFAULTENA_Msk; 												// enable Usage-, Bus-, and MMU Fault	

}


// ------------------------------------------------------------------------------------------------
// get reset signal sources
extern inline void _Chip_Read_ResetSource(uint32_t *dst)							// Get reset sources (Chip_RST_SRC)
{
    *dst = LPC_SYSCON->RSID;														// format as CHAL_RST_SRC_t
}


// ******************************************************************************************************
// CLOCK functions
// ******************************************************************************************************
// PRIVATE CLK helpers functions. See Users Manual UM10470, page 22, Fig. 4
#define _Chip_Clock_Get_altpllclk_Rate(x)	(_Chip_Clock_Get_SysClk_Rate() * ((LPC_SYSCON->PLL[1].PLLCFG & 0x1F) + 1))	// Alt PLL out, also called as "alt_pll_clk"



// ------------------------------------------------------------------------------------------------
// Main PLL out, also called as "pll_clk"
#define _Chip_Clock_Get_pllclk_Rate(x)		(_Chip_Clock_Get_SysClk_Rate() * ((LPC_SYSCON->PLL[0].PLLCFG & 0x1F) + 1))


// ------------------------------------------------------------------------------------------------
// Peripherial clock source rate, also called as "pclk"
#define _Chip_Clock_Get_pclk_Rate(x)	((LPC_SYSCON->CCLKSEL & (1 << 8)) ?  (_Chip_Clock_Get_pllclk_Rate() / (LPC_SYSCON->PCLKSEL & 0x1f)): (_Chip_Clock_Get_SysClk_Rate() / (LPC_SYSCON->PCLKSEL & 0x1f)))


// ------------------------------------------------------------------------------------------------
// general clock source rate - MCU main crystal freq
extern inline uint32_t _Chip_Clock_Get_SourceClk_Rate(void)
{
	if(LPC_SYSCON->CLKSRCSEL & 1) return(CONF_CHIP_OSCRATEIN);						// selected extarnal oscillator
	else return(_CHIP_IRC_FREQUENCY);												// selected internal RC oscillator
}

// ------------------------------------------------------------------------------------------------
// RTC clock source rate - RTC input freq /32768/
extern inline uint32_t _Chip_Clock_Get_SourceRTC_Rate(void)
{
	return (0);
}

// ------------------------------------------------------------------------------------------------
// "sysclk"
extern inline uint32_t _Chip_Clock_Get_SysClk_Rate(void)
{
	return(_Chip_Clock_Get_SourceClk_Rate());										// same as selected Source clk
}

// ------------------------------------------------------------------------------------------------
// CPU core clock source rate, also called as "cclk"
extern inline uint32_t _Chip_Clock_Get_CoreClk_Rate(void)
{
	if(LPC_SYSCON->CCLKSEL & (1 << 8)) return(_Chip_Clock_Get_pllclk_Rate() / (LPC_SYSCON->CCLKSEL & 0x1f));
	else return(_Chip_Clock_Get_SysClk_Rate() / (LPC_SYSCON->CCLKSEL & 0x1f));
}

// ------------------------------------------------------------------------------------------------
// EMC clock source rate, also called as "emc_clk"
extern inline uint32_t _Chip_Clock_Get_EMCClk_Rate(void)
{
	return (0);
}

// ------------------------------------------------------------------------------------------------
// USB clock source rate, also called as "usb_clk"
extern inline uint32_t _Chip_Clock_Get_USBClk_Rate(void)
{
	return (0);
}

// ------------------------------------------------------------------------------------------------
// SPIFI clock source rate, also called as "spifi_clk"
extern inline uint32_t _Chip_Clock_Get_SPIFIClk_Rate(void)
{
	return (0);
}

// ------------------------------------------------------------------------------------------------
extern inline uint32_t _Chip_Clock_Get_ClkOut_Rate(void)
{
	return(0);
}


// ******************************************************************************************************
// GPIO functions
// ******************************************************************************************************

// For IOCON Block, Pin function
#define CHIP_PINSWMUNUSED				0x0000										// Not used pin of MCU - write 0xff into PINENABLE - default state
#define PFUN_GPIO						0x0000										// no alternate function, Pin as std GPIO
#define PFUN_ALT(y)						(y & 0x07)									// Write y value into IOCON bits 2:0

COMP_PACKED_BEGIN
typedef struct	_Chip_IO_Spec														// Additional GPIO structure based on chip specific requirements */
{
	uint16_t							IOCON_Reg;									// IOCON register hodnota. pouziva sa len 16 bit. Inak IOCON je 32 bitovy	 */
}_Chip_IO_Spec_t;
COMP_PACKED_END

extern void *_Chip_GPIO_Init( int32_t PeriIndex, uint32_t Pin);
extern bool _Chip_GPIO_DeInit( int32_t PeriIndex, uint32_t Pin);
extern void *_Chip_GPIO_IRQ_Conf( uint8_t Group, uint32_t Port, uint32_t Pin, uint8_t Sens);
extern bool _Chip_GPIO_IRQ_Enable( void *pPeri, uint32_t Port, uint32_t Pin, bool NewState);

static inline void *_Chip_Get_GPIO_Ptr(int32_t PeriIndex)							// vrat pointer na konkretnu GPIO periferiu - LPC15xx pouziva iba jeten ptr
{
	switch(PeriIndex)																// LPC17xx have 5 GPIOs
	{
		case 0: return(LPC_GPIO);
		case 1: return(LPC_GPIO1);
		case 2: return(LPC_GPIO2); 
		case 3: return(LPC_GPIO3); 
		case 4:	return(LPC_GPIO4); 
		case 5: return(LPC_GPIO5); 
		default: return(NULL);
	}	
}

// ------------------------------------------------------------------------------------------------
// Set Direction for a GPIO port 
extern inline void _Chip_GPIO_Set_PinDir(_CHIP_GPIO_T *pGPIO, uint8_t portNum, uint32_t bitValue, uint8_t out)
{
	if (out) pGPIO->DIR |= bitValue;
	else pGPIO->DIR &= ~bitValue;
}

// ------------------------------------------------------------------------------------------------
// Read Pin Value
extern inline bool _Chip_GPIO_GetPinState(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin)
{
	if(pGPIO->PIN & (1 << pin)) return(true);
	else return false ;
}

// ------------------------------------------------------------------------------------------------
// Clear Pin
extern inline void _Chip_GPIO_SetPinOutLow(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin)
{
	pGPIO->CLR = (1 << pin);
}

// ------------------------------------------------------------------------------------------------
// Set Pin
extern inline void _Chip_GPIO_SetPinOutHigh(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin)
{
	pGPIO->SET = (1 << pin);
}

// ------------------------------------------------------------------------------------------------
// Toggle Pin
extern inline void _Chip_GPIO_SetPinToggle(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin)
{
	if(pGPIO->PIN & (1 << pin)) pGPIO->CLR |= (1 << pin);
	else pGPIO->SET |= (1 << pin);
}

// ------------------------------------------------------------------------------------------------
// Enable/Disable PINT IRQ
extern inline bool _Chip_GPIO_PINT_IRQ_Enable(void *pPeri, uint32_t Port, uint32_t Pin, bool NewState)
{
	if(NewState == true) NVIC_EnableIRQ( (IRQn_Type) (GPIO_IRQn));					// Enable GINT0 interrupt
	else NVIC_DisableIRQ( (IRQn_Type) (GPIO_IRQn));									// Disable Pin Interrupt	
	return(true);
}




// ******************************************************************************************************
// I2C Functions
// ******************************************************************************************************
// Interrupt status flag defines:
#define	CHIP_I2C_IRQSTAT_MSARBLOSS			0x0001									// Master Arbitration Loss flag.
#define	CHIP_I2C_IRQSTAT_MSSTSTPERR			0x0002									// Master Start/Stop Error flag
#define	CHIP_I2C_IRQSTAT_MSSTATE_CHANGED	0x0080									// MSSTATE was changed - this flag mus be out of mask !!!
#define	CHIP_I2C_IRQSTAT_MSSTATE_MASK		0xFF00
#define CHIP_I2C_IRQSTAT_MSSTATE_RX			0x0100
#define CHIP_I2C_IRQSTAT_MSSTATE_TX			0x0200
#define CHIP_I2C_IRQSTAT_MSSTATE_NACK_ADR	0x1000
#define CHIP_I2C_IRQSTAT_MSSTATE_NACK_DATA	0x2000
// needs as CHAL_I2CM_Get_IntStatus bitwise result!


extern void *_Chip_I2C_Init(int32_t PeriIndex);										// init I2C by periphary number. Return is pointer to periphery if success
extern bool _Chip_I2CM_SetBusSpeed(void *pPeri, uint32_t Speed_kHz);				// Speed: 100kHz, 400kHz
extern uint32_t _Chip_I2C_Get_I2C_Src_ClockRate(void *pPeri);
extern bool _Chip_I2C_Configure(void *pPeri, uint32_t Speed_kHz, bool MasterMode, bool SlaveMode, bool MonitorMode);
extern bool _Chip_I2C_Enable(void* pPeri, bool NewState);
extern bool _Chip_I2C_IRQ_Enable(void *pPeri, bool NewState);

typedef struct 
{
	const uint8_t 						*pTxBuff;									// Pointer to array of bytes to be transmitted
	uint8_t 							*pRxBuff;									// Pointer memory where bytes received from I2C be stored
	uint16_t 							TxSz;										// Number of bytes in transmit array, if 0 only receive transfer will be carried on
	uint16_t 							RxSz;										// Number of bytes to received, if 0 only transmission we be carried on
	uint16_t 							Status;										// Status of the current I2C transfer
	uint8_t 							SlaveAddr;									// 7-bit I2C Slave address
} _Chip_I2CM_XFER_T;

// ------------------------------------------------------------------------------------------------------
// return periphery index
static inline int32_t _Chip_I2C_Get_PeriIndex(void *pPeri)							// Return Index from pointer
{
	if((_CHIP_I2C_T*) pPeri == LPC_I2C2) return(2);
	if((_CHIP_I2C_T*) pPeri == LPC_I2C1) return(1);
	if((_CHIP_I2C_T*) pPeri == LPC_I2C0) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------------
// I2C Set I2C interface speed in kHz
// result is true:successfull otherwise false
extern inline bool _Chip_I2C_Set_BusSpeed(void *pPeri, uint32_t Speed_kHz)			// Max Speed: 100kHz, 400kHz, 1MHz or 3.2 MHz
{
	uint32_t SCLValue;

	SCLValue = (_Chip_Clock_Get_pclk_Rate() / Speed_kHz) / 1000;					// to Hz
	((_CHIP_I2C_T*)pPeri)->SCLH = (uint32_t) (SCLValue >> 1);						// divide 2
	((_CHIP_I2C_T*)pPeri)->SCLL = (uint32_t) (SCLValue - ((_CHIP_I2C_T*)pPeri)->SCLH); // add residue part
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// I2C Get I2C interface speed in kHz
extern inline uint32_t _Chip_I2C_Get_BusSpeed(void *pPeri)							// Return Current bus speed
{
	uint32_t SCLL = ((_CHIP_I2C_T*)pPeri)->SCLL;
	return(_Chip_Clock_Get_pclk_Rate() / (((_CHIP_I2C_T*)pPeri)->SCLH  + (SCLL)) / 1000); //return in kHz
}

// ------------------------------------------------------------------------------------------------------
// Clear I2C MASTER status - clear by disable and reenable selected interrupts.
extern inline bool _Chip_I2C_Clear_Status(void *pPeri, uint32_t NewValue)
{
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Get I2C MASTER interrupt status
extern inline uint32_t _Chip_I2CM_GET_IRQStatus(void *pPeri)
{
	uint32_t rdstat = ((_CHIP_I2C_T*)pPeri)->STAT;									// read chip status
	uint32_t Result = CHIP_I2C_IRQSTAT_MSSTATE_CHANGED;								// LPC1788 dosn't have change flag.
	
	switch (rdstat & 0xf8)															// recode chip status to CHAL simples status. See UM10470, Table 512
	{
//		case 0x08: Result|= CHIP_I2C_IRQSTAT_MSSTATE_ST_WASSENT; break;				// Start has been sent now
		case 0x10: Result = 0; break;												// Repeated start has been sent
//		case 0x18: Result|= CHIP_I2C_IRQSTAT_MSSTATE_TX; ((_CHIP_I2C_T *)pPeri)->CONCLR = (1 << 5); break;	// SLA+W has been transmitted; ACK has been received.
		case 0x18: Result|= CHIP_I2C_IRQSTAT_MSSTATE_TX; break;						// SLA+W has been transmitted; ACK has been received.
		case 0x20: Result|= CHIP_I2C_IRQSTAT_MSSTATE_NACK_ADR; break;				// SLA+W has been transmitted; NOT ACK has been received.
		case 0x28: Result|= CHIP_I2C_IRQSTAT_MSSTATE_TX; break;						// Data byte in I2DAT has been transmitted; ACK has been received.
		case 0x30: Result|= CHIP_I2C_IRQSTAT_MSSTATE_NACK_ADR; break;				// Data byte in I2DAT has been transmitted; NOT ACK has been received.
		case 0x38: Result|= CHIP_I2C_IRQSTAT_MSARBLOSS; break;						// Arbitration lost in SLA+R/W or Data bytes.
		case 0x40: Result = 0; break;												// SLA+R has been transmitted, ACK has been received. Set for ACK.
		case 0x48: Result|= CHIP_I2C_IRQSTAT_MSSTATE_NACK_ADR; break;				// SLA+R has been transmitted, ACK has not been received.
		case 0x50: Result|= CHIP_I2C_IRQSTAT_MSSTATE_RX; break;						// Data has been received, ACK has been returned
		case 0x58: Result|= CHIP_I2C_IRQSTAT_MSSTATE_NACK_DATA; break;				// Data has been received, ACK has not been returned
		default:  Result = 0;
	}
	return(Result);
}

// ------------------------------------------------------------------------------------------------------
// Read I2C status
extern inline uint32_t _Chip_I2C_GET_Status(void *pPeri)
{
	return(_Chip_I2CM_GET_IRQStatus(pPeri));										// same status for LPC17xx
}

// ------------------------------------------------------------------------------------------------------
// Clear I2C MASTER interrupt status - clear by disable and reenable selected interrupts.
extern inline bool _Chip_I2CM_Clear_IRQStatus(void *pPeri, uint32_t NewValue)
{
	((_CHIP_I2C_T*)pPeri)->CONCLR = 0x00000002f;									// Clear IRQ Status
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// I2C - write data into I2C
extern inline void _Chip_I2CM_Put_Data(void *pPeri, uint32_t NewValue)
{
	//((_CHIP_I2C_T *)pPeri)->CONCLR = (1 << 5);									// also Clear Start Flag
	//((_CHIP_I2C_T *)pPeri)->CONSET = (1 << 6);									// I2EN set
	((_CHIP_I2C_T *)pPeri)->DAT = (uint32_t) NewValue & 0xff;						// write data to I2C Out reg
	((_CHIP_I2C_T *)pPeri)->CONCLR = (1 << 3);										// also Clear SI Flag
}

// ------------------------------------------------------------------------------------------------------
// I2C - Read Data 
extern inline uint32_t _Chip_I2CM_Get_Data(void *pPeri)
{
	//((_CHIP_I2C_T *)pPeri)->CONSET = (1 << 2);									// Set AA as ACK
	uint32_t tmp = ((_CHIP_I2C_T *)pPeri)->DAT;
	//((_CHIP_I2C_T *)pPeri)->CONCLR = (1 << 3);									// also Clear SI Flag
	return(tmp);
}

// ------------------------------------------------------------------------------------------------------
// I2C - Create START condition 
static inline void _Chip_I2CM_SendStart(void *pPeri)
{
	((_CHIP_I2C_T *)pPeri)->CONCLR = (1 << 2);										// Clear ACK Flag
	((_CHIP_I2C_T *)pPeri)->CONCLR = (1 << 3);										// Clear SI Flag
	((_CHIP_I2C_T *)pPeri)->CONSET = (1 << 5);										// generate I2C Start 
	while(1)
	{
		if(((_CHIP_I2C_T*)pPeri)->STAT ==  0x08) break;
		if(((_CHIP_I2C_T*)pPeri)->STAT ==  0x10) break;
	}
	((_CHIP_I2C_T *)pPeri)->CONCLR = (1 << 5);										// Clear START
}

// ------------------------------------------------------------------------------------------------------
// I2C - Create STOP condition
static inline void _Chip_I2CM_SendStop(void *pPeri)
{
	((_CHIP_I2C_T *)pPeri)->CONSET = (1 << 4);										// generate I2C Stop
	((_CHIP_I2C_T *)pPeri)->CONCLR = (1 << 3);										// also Clear SI Flag
}

// ------------------------------------------------------------------------------------------------------
// I2C - Master Continue
static inline void _Chip_I2CM_Continue(void *pPeri)
{
	((_CHIP_I2C_T *)pPeri)->CONSET = (1 << 2);										// generate ACK
	((_CHIP_I2C_T *)pPeri)->CONCLR = (1 << 3);										// also Clear SI Flag
}
	


// ******************************************************************************************************
// UART Functions
// ******************************************************************************************************
// Interrupt status flag defines:
#define	CHIP_UART_IRQSTAT_TXREADY			0x0001									// TX ready before transsmiting
#define	CHIP_UART_IRQSTAT_TXDONE			0x0002									// TX Data was Transsmited
#define	CHIP_UART_IRQSTAT_RXREADY			0x0004									// RX ready before receiving
#define	CHIP_UART_IRQSTAT_RXDONE			0x0008									// RX new data available

// ------------------------------------------------------------------------------------------------------
// Return periphery index
static inline int32_t _Chip_UART_Get_PeriIndex(void *pPeri)							// Return Index from pointer
{
	if(((_CHIP_UART_T*) pPeri) == LPC_UART4) return(4);
	if(((_CHIP_UART_T*) pPeri) == LPC_UART3) return(3);
	if(((_CHIP_UART_T*) pPeri) == LPC_UART2) return(2);
	if(((_CHIP_UART_T*) pPeri) == LPC_UART1) return(1);
	if(((_CHIP_UART_T*) pPeri) == LPC_UART0) return(0);
	return(-1);																		// otherwise error
}

extern void *_Chip_UART_Init(int32_t PeriIndex);
extern bool _Chip_UART_Enable(void *pPeri, bool NewState);
extern bool _Chip_UART_Set_Baud(void* pPeri, uint32_t NewBaud);
extern bool _Chip_UART_ConfigData(void* pPeri, uint8_t DataLen, uint8_t Parity, uint8_t StopBits);
extern bool _Chip_UART_RTS_Configure(void* pPeri, uint8_t sRTS_Port, uint8_t sRTS_Pin);
extern bool _Chip_UART_CTS_Configure(void* pPeri, uint8_t sCTS_Port, uint8_t sCTS_Pin);
extern bool _Chip_UART_DIRDE_Configure(void* pPeri, uint8_t DIRDE_Port, uint8_t DIRDE_Pin, bool DIRDE_H_Active);
extern bool _Chip_UART_RE_Configure(void* pPeri, uint8_t RE_Port, uint8_t RE_Pin, bool RE_H_Active);

// ------------------------------------------------------------------------------------------------------
// ADR DETECTION Control
extern inline bool _Chip_UART_Set_AdrDet(void* pPeri, bool NewVal)
{
	if(NewVal)((_CHIP_UART_T *)pPeri)->RS485CTRL |= 1 << 2;							// AADEN bit set
	else ((_CHIP_UART_T *)pPeri)->RS485CTRL &= ~(1 << 2);
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Write New Address 
// result is true:successfull otherwise false
extern inline bool _Chip_UART_Set_Address(void* pPeri, uint8_t NewAddress)
{
	if(NewAddress)
	{
		((_CHIP_UART_T *)pPeri)->RS485ADRMATCH = NewAddress;						// Save address
		((_CHIP_UART_T *)pPeri)->RS485CTRL |= 1 << 0;								// Multidrop mode set
		_Chip_UART_Set_AdrDet(pPeri, true);											// nahod ADRDET
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Get I2C MASTER interrupt status
extern inline uint32_t _Chip_UART_Get_IRQStatus(void *pPeri)
{
	return( (((_CHIP_UART_T *)pPeri)->IIR) & (((_CHIP_UART_T *)pPeri)->IER) );		// status masked with Enabled
	//return( (((_CHIP_UART_T *)pPeri)->STAT) & (((_CHIP_UART_T *)pPeri)->INTENSET) );	// status masked with Enabled
	// PRI UART1 TO vracia sly INTSTAT !!!
}

// ------------------------------------------------------------------------------------------------------
// Clear I2C MASTER interrupt status - clear by disable and reenable selected interrupts.
extern inline bool _Chip_UART_Clear_IRQStatus(void *pPeri, uint32_t NewValue)
{
	return(true);																	// IIR is cleared upon IID read
}


// ------------------------------------------------------------------------------------------------------
// Write char to OutBuff
extern inline void _Chip_UART_Put_Data(void *pPeri, uint16_t WrChar)
{
	((_CHIP_UART_T *)pPeri)->THR = WrChar & 0xFF;									// only 8-bit value
}

// ------------------------------------------------------------------------------------------------------
// Read char from InBuff
extern inline uint32_t _Chip_UART_Get_Data(void *pPeri)
{
	return(((_CHIP_UART_T *)pPeri)->RBR);
}	

// ------------------------------------------------------------------------------------------------------
// TX Interrupt enable/disable
extern inline void _Chip_UART_IRQTx_Enable (void *pPeri, bool NewState)			// Enable/Disable Tx Interrupts
{
	if(NewState)
	{
		((_CHIP_UART_T*)pPeri)->IER |= (1 << 1);									// Enable TX interrupt
	}
	else
	{
		((_CHIP_UART_T*)pPeri)->IER &= ~(1 << 1);									// Disable TX interrupt
	}
}

// ------------------------------------------------------------------------------------------------------
// RX Interrupt enable/disable
extern inline void _Chip_UART_IRQRx_Enable (void *pPeri, bool NewState)			// Enable/Disable Rx Interrupts 
{
	if(NewState)
	{
		((_CHIP_UART_T*)pPeri)->IER |= (1 << 0);									// Enable RX interrupt
	}
	else
	{
		((_CHIP_UART_T*)pPeri)->IER &= ~(1 << 0);									// Disable RX interrupt
	}
}

// ------------------------------------------------------------------------------------------------------
// UART Get interface speed in kHz
extern inline uint32_t _Chip_UART_Get_BusSpeed(void *pPeri)							// Return Current bus speed
{
	//uint32_t SCLL = ((_CHIP_I2C_T*)pPeri)->SCLL;
	//return(_Chip_Clock_Get_pclk_Rate() / (((_CHIP_I2C_T*)pPeri)->SCLH  + (SCLL)) / 1000); //return in kHz
	return(1); 																		//return in kHz
}





// ******************************************************************************************************
// SPI Functions
// ******************************************************************************************************
extern void *_Chip_SPI_Init(int32_t PeriIndex);										// init SPI by periphary number. Return is pointer to periphery if success
extern bool _Chip_SPI_Enable(void *pPeri, bool NewState);
extern bool _Chip_SPI_IRQ_Enable(void *pPeri, bool NewState);
extern bool _Chip_SPI_Configure(void *pPeri, uint32_t Speed_kHz, bool MasterMode, bool SlaveMode, bool LSB_First);

#define	CHIP_SPI_IRQSTAT_RXDONE							(1 << 0)					// Receiver Done Flag new Data available
#define	CHIP_SPI_IRQSTAT_TXRDY							(1 << 2)					// Transmitter Ready Flag
#define	CHIP_SPI_IRQSTAT_RXOV							(1 << 0)					// Receiver Overrun IRQ Flag
//#define	CHIP_SPI_IRQSTAT_TXUR							(1 << 3)					// Transmitter underrun IRQ Flag
//#define	CHIP_SPI_IRQSTAT_SSA							(1 << 4)					// Slave Select Assert
//#define	CHIP_SPI_IRQSTAT_SSD							(1 << 5)					// Slave Select Deassert

#define	CHIP_SPI_IRQEN_RXDONE							(1 << 0)					// Receiver Done IRQ enable - new data available
#define	CHIP_SPI_IRQEN_TXRDY							(1 << 1) 					// Transmitter Ready IRQ enable - ready before send
#define	CHIP_SPI_IRQEN_RXOV								(1 << 2)					// Receiver Overrun IRQ enable
#define	CHIP_SPI_IRQEN_TXUR								(1 << 3)					// Transmitter underrun IRQ enable
#define	CHIP_SPI_IRQEN_SSA								(1 << 4)					// Slave Select Assert IRQ enable
#define	CHIP_SPI_IRQEN_SSD								(1 << 5)					// Slave Select Deassert IRQ enable



extern void *_Chip_SPI_Init(int32_t PeriIndex);
extern uint32_t _Chip_SPI_Get_I2C_Src_ClockRate(void *pPeri);
extern bool _Chip_SPI_Configure(void *pPeri, uint32_t Speed_kHz, bool MasterMode, bool SlaveMode, bool LSB_First);
extern bool _Chip_SPI_Enable(void* pPeri, bool NewState);

// ------------------------------------------------------------------------------------------------------
// SPI - Get peripharial index by ptr
static inline int32_t _Chip_SPI_Get_PeriIndex(void *pPeri)							// Return Index from pointer
{
	if(((_CHIP_SPI_T*) pPeri) == LPC_SSP2) return(2);
	if(((_CHIP_SPI_T*) pPeri) == LPC_SSP1) return(1);
	if(((_CHIP_SPI_T*) pPeri) == LPC_SSP0) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------------
// SPI - Set clock divisor
static inline void Chip_SPI_SetClockDiv(_CHIP_SPI_T *pSPI, uint32_t clkdiv)
{
	if ((clkdiv >= 1) && (clkdiv <= 0x00000100)) pSPI->CR0 |= ((clkdiv - 1) << 8);	// write to CLK DIV register
	else pSPI->CR0 &= 0x0000ff00;
}

// ------------------------------------------------------------------------------------------------------
// SPI Get SPI interface speed in kHz
extern inline uint32_t _Chip_SPI_Get_BusSpeed(_CHIP_SPI_T *pSPI)					// Return Current bus speed
{
	return((_Chip_Clock_Get_pclk_Rate() / 1000) / ((pSPI->CR0 + 1) >> 8)); 			// return in kHz
}

// ------------------------------------------------------------------------------------------------------
// SPI - write data into I2C datareg
extern inline void _Chip_SPI_Put_Data(void *pPeri, uint32_t NewValue, uint8_t NumOfBits, uint8_t SSel)
{
	//((_CHIP_SPI_T *)pPeri)->TXCTL &= ~(0x0f0f0000);								// clear SSEL and Datalen bits
	((_CHIP_SPI_T *)pPeri)->CR0 &= ~(0x0000000f);									// clear SSEL and Datalen bits
	((_CHIP_SPI_T *)pPeri)->CR0 |= (NumOfBits & 0x0f);
	((_CHIP_SPI_T *)pPeri)->DR = (uint32_t) NewValue & 0x0000ffff;					// write data
}

// ------------------------------------------------------------------------------------------------------
// SPI - Read Data from datareg
extern inline uint32_t _Chip_SPI_Get_Data(void *pPeri)
{
	return(((_CHIP_SPI_T *)pPeri)->DR & 0x0000ffff);
}

// ------------------------------------------------------------------------------------------------------
// Read SPI interrupt status
extern inline uint32_t _Chip_SPI_Get_IRQStatus(void *pPeri)
{
	return(((_CHIP_SPI_T *)pPeri)->RIS);
}

// ------------------------------------------------------------------------------------------------------
// Clear SPI interrupt status
extern inline bool _Chip_SPI_Clear_IRQStatus(void *pPeri, uint32_t NewValue)
{
	((_CHIP_SPI_T *)pPeri)->ICR |= NewValue;
	return(true);
}

// ------------------------------------------------------------------------------------------------
// enable/disable interrupt generation from selectet SPI
extern inline bool _Chip_SPI_IRQ_Enable(void *pPeri, bool NewState)
{
	bool res = false;
	IRQn_Type PeriIRQ;

	switch(_Chip_SPI_Get_PeriIndex(pPeri))
	{
		case 2: 
			{
				PeriIRQ = SSP2_IRQn;												// select apropriate interrupt number
				res = true;
				break;
			}		
		case 1: 
			{
				PeriIRQ = SSP1_IRQn;												// select apropriate interrupt number
				res = true;
				break;
			}
		case 0: 
			{
				PeriIRQ = SSP0_IRQn;												// select apropriate interrupt number
				res = true;					
				break;
			}				
		default:return(false);														// Bad input
	}	
	
	if((NewState == true) && (res == true))
	{
		NVIC_ClearPendingIRQ ( (IRQn_Type) (PeriIRQ));								// Clear SPIx interrupt flag
		NVIC_EnableIRQ( (IRQn_Type) (PeriIRQ));										// Enable SPIx interrupt
	}
	else
	{
		NVIC_DisableIRQ( (IRQn_Type) (PeriIRQ));									// Disable SPIx interrupt
	}
	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// Enable/disable selected interrupts
extern inline bool _Chip_SPI_IRQMask_Enable(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_SPI_T*)pPeri)->IMSC |= IRQBitMask;
	else ((_CHIP_SPI_T*)pPeri)->IMSC &= ~IRQBitMask;
	
	return(true);

}

// ------------------------------------------------------------------------------------------------------
// SPI - Create End Of Transfer condition 
extern inline void _Chip_SPI_SendEOT(void *pPeri)
{
	
}

// ------------------------------------------------------------------------------------------------------
// SPI - Create End Of Frame condition 
extern inline void _Chip_SPI_SendEOF(void *pPeri)
{
	
}

// ------------------------------------------------------------------------------------------------------
// SPI - Create Receive Ignore condition 
extern inline void _Chip_SPI_ReceiveIgnore(void *pPeri)
{
	
}















// ******************************************************************************************************
// USB Functions
// ******************************************************************************************************
extern void *_Chip_USB_Init(int32_t PeriIndex);
extern bool _Chip_USB_Enable(void *pPeri, bool NewState);
extern bool _Chip_USB_Configure(void *pPeri, bool DeviceMode, bool HostMode, bool OTGMode);
extern uint32_t _Chip_USB_Get_IRQStatus(void *pPeri);

// ------------------------------------------------------------------------------------------------
// Power down / up
extern inline bool _Chip_USB_Power(void *pPeri, bool NewState)
{
	return(true);																	// Doesn't support power down mode for this periphery
}

// ******************************************************************************************************
// ETH Functions
// ******************************************************************************************************
extern void *_Chip_ETH_Init(int32_t PeriIndex);
extern bool _Chip_ETH_Enable(void *pPeri, bool NewState);
//extern bool _Chip_USB_Configure(void *pPeri, bool DeviceMode, bool HostMode, bool OTGMode);
//extern uint32_t _Chip_USB_Get_IRQStatus(void *pPeri);


// ******************************************************************************************************
// ADC Functions
// ******************************************************************************************************
extern void *_Chip_ADC_Init(int32_t PeriIndex);
extern bool _Chip_ADC_Enable(void* pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool _Chip_ADC_Configure (void *pPeri, uint32_t ChannelBitMask, uint32_t Speed_kHz, bool RepeatMode, bool DMAMode);
extern uint32_t _Chip_ADC_Get_ChannelMask(void *pPeri);
extern bool _Chip_Temp_Enable(bool NewState);

extern uint8_t _Chip_ADC_DMA_Mode_ChannelBitMask;


// ------------------------------------------------------------------------------------------------------
// return ptr to periphary
static inline int32_t _Chip_ADC_Get_PeriIndex(void *pPeri)
{
	if(((_CHIP_ADC_T*) pPeri) == LPC_ADC) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------------
// Power down / up
extern inline bool _Chip_ADC_Power(void *pPeri, bool NewState)
{
	return(true);																	// Doesn't support power down mode for this periphery
}

// ------------------------------------------------------------------------------------------------------
// Get interrupt flags
extern inline uint32_t _Chip_ADC_Get_IRQFlags(_CHIP_ADC_T *pADC)
{
	return pADC->STAT;
}

// ------------------------------------------------------------------------------------------------------
// clear interrupt flags
extern inline void _Chip_ADC_Clear_IRQFlags(_CHIP_ADC_T *pADC, uint32_t flags)
{
																					// nothing to clear
}

// ------------------------------------------------------------------------------------------------------
// ADC Conversion start
extern inline void _Chip_ADC_Start(void *pPeri, uint32_t ChannelBitMask)
{
	((_CHIP_ADC_T*)pPeri)->CR |= (1 << 24);											// start immediatelly
}

// ------------------------------------------------------------------------------------------------------
// ADC channel interrupt enable/disable for selected channel
// result is true:successfull otherwise false
extern bool _Chip_ADC_DMA_Mode;
extern inline bool _Chip_ADC_Enable_CHIRQ(void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
	bool res = false;
	if(ChannelBitMask == UINT32_MAX) return(true);
	
	if(NewState)
	{
		((_CHIP_ADC_T*)pPeri)->CR |= ChannelBitMask;								// enable channel[num]
		
		if(_Chip_ADC_DMA_Mode == 0) ((_CHIP_ADC_T*)pPeri)->INTEN |= ChannelBitMask;	// enable IRQ also for DMA access		
		//(~(_Chip_ADC_DMA_Mode_ChannelBitMask)) & 0xff; // Enable interrupt for non-DMA channels
		
		((_CHIP_ADC_T*)pPeri)->CR |= (1 << 24);										// start immediatelly
	}
	else 
	{
		((_CHIP_ADC_T*)pPeri)->CR &= ~(ChannelBitMask);								// disable channel[num]
		((_CHIP_ADC_T*)pPeri)->INTEN = 0;											// disable interrupts
	}
	
	return(res);
}

// ******************************************************************************************************
// DAC Functions
// ******************************************************************************************************
// ------------------------------------------------------------------------------------------------------
// return ptr to periphary
static inline int32_t _Chip_DAC_Get_PeriIndex(void *pPeri)
{
	if((_CHIP_DAC_T*) pPeri == LPC_DAC) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------------
// Power down / up
extern inline bool _Chip_DAC_Power(void *pPeri, bool NewState)
{
	return(true);																	// Doesn't support power down mode for this periphery
}


// ******************************************************************************************************
// TIMER Functions - Timerx Or MRTx
// ******************************************************************************************************
extern void *_Chip_Timer_Init(int32_t PeriIndex);
extern bool _Chip_Timer_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState);
//extern bool _Chip_Timer_IRQ_Enable(void *pPeri, bool NewState);
extern bool _Chip_Timer_Configure (void *pPeri, uint8_t Channel, uint32_t RateHz, bool RepeatMode);

// ------------------------------------------------------------------------------------------------------
// return index to periphery Timer(pointer to peri)
static inline int32_t _Chip_Timer_Get_PeriIndex(void *pPeri)
{
	if((_CHIP_TIM_T*) pPeri == LPC_TIMER3) return(3);
	if((_CHIP_TIM_T*) pPeri == LPC_TIMER2) return(2);
	if((_CHIP_TIM_T*) pPeri == LPC_TIMER1) return(1);
	if((_CHIP_TIM_T*) pPeri == LPC_TIMER0) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------------
// Timer Clear channel interrupt Flag for selected channel
// result is true:successfull otherwise false
extern inline bool _Chip_Timer_Clear_CHIRQ_Status(void *pPeri, uint32_t ChannelBitMask)
{
	bool res = false;
	
	((_CHIP_TIM_T*)pPeri)->IR |= (ChannelBitMask & 0x0f);
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// Timer Get channel interrupt Flag for all channels
// result is bit position for active IRQ
extern inline uint32_t _Chip_Timer_Get_CHIRQ_Status(void *pPeri)
{
	return(((_CHIP_TIM_T*)pPeri)->IR & 0x0f);
}

// ------------------------------------------------------------------------------------------------------
// Timer channel interrupt enable/disable for selected channel
// result is true:successfull otherwise false
extern inline bool _Chip_Timer_Enable_CHIRQ(void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
	bool res = false;
	uint32_t mask = 0;
	
	mask = ((ChannelBitMask & 0x0F) & (1 << 3)) << 6;								// move MR3 to MR3I
	mask |= ((ChannelBitMask & 0x0F) & (1 << 2)) << 4;								// move MR2 to MR2I
	mask |= ((ChannelBitMask & 0x0F) & (1 << 1)) << 2;								// move MR1 to MR1I
	mask |= ((ChannelBitMask & 0x0F) & (1 << 0)) << 0;								// move MR0 to MR0I
	
	if(NewState) ((_CHIP_TIM_T*)pPeri)->MCR |= mask;
	else ((_CHIP_TIM_T*)pPeri)->MCR &= ~(mask);
	res = true;
	
	return(res);
}





// ******************************************************************************************************
// DMA Functions
// ******************************************************************************************************
typedef struct
{
	uint32_t							SrcAdr;										// Source address
	uint16_t							SrcBSize;									// Source Burst size		  1/2/4 x Word
	uint8_t								SrcWSize;									// Source width: 8/16/32-bit
	uint8_t								SrcIncr;									// Source Increment
	uint32_t							SrcPeri;									// Source peripherial	
	uint32_t							DstAdr;										// Destination address
	uint16_t							DstBSize;									// Destination Burst size		  1/2/4 x Word
	uint8_t								DstWSize;									// Destination width: 8/16/32-bit
	uint8_t								DstIncr;									// Destination Increment
	uint32_t							DstPeri;									// Destination peripherial	
	uint8_t								XferIRQ;									// Select Interrupt generated after transfer
	uint32_t 							XferLen;									// length of transfer
	uint8_t								XferType;									// transfer type
	uint8_t								XferPrio;									// transfer priority
	uint32_t 							XferNextDescriptor;							// address of next linked descriptor
} _Chip_DMA_Xfer_t;

extern void *_Chip_DMA_Init(int32_t PeriIndex);
extern bool _Chip_DMA_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState);
//extern bool _Chip_DMA_IRQ_Enable(void *pPeri, bool NewState);
extern bool _Chip_DMA_Configure (void *pPeri, uint8_t Channel, _Chip_DMA_Xfer_t *Xfer);

// ------------------------------------------------------------------------------------------------------
// return pointer to periphery DMA(PeriIndex)
static inline int32_t _Chip_DMA_Get_PeriIndex(void *pPeri)
{
	if((_CHIP_DMA_T*) pPeri == LPC_GPDMA) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------------
// DMA Clear channel interrupt state for selected BitMasked channels
// result is true:successfull otherwise false
extern inline bool _Chip_DMA_Clear_CHIRQ_Status(void *pPeri, uint32_t ChannelBitMask)
{
	((_CHIP_DMA_T*)pPeri)->INTTCCLEAR |= ChannelBitMask;
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// DMA Get channel interrupt Flag for all channels
// result is bit position for active IRQ
extern inline uint32_t _Chip_DMA_Get_CHIRQ_Status(void *pPeri)
{
	return(((_CHIP_DMA_T*)pPeri)->INTSTAT & 0x0f);
}


// ------------------------------------------------------------------------------------------------------
// DMA - Get channel control word
extern uint32_t DMA_CH_XFERLEN[];
extern inline uint32_t _Chip_DMA_Get_CHControl(void *pPeri, uint32_t Channel)
{
	return(((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL | DMA_CH_XFERLEN[Channel]);
}


// ------------------------------------------------------------------------------------------------------
// DMA channel interrupt enable/disable for selected channel
// result is true:successfull otherwise false
extern inline bool _Chip_DMA_Enable_CHIRQ(void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
	bool res = false;
	if(ChannelBitMask == UINT32_MAX) return(true);
	
	for(uint8_t i=0; i < _CHIP_DMA_CHANNELS_COUNT; i++)								// ch 0 to 7
	{
		if((1 << i) & ChannelBitMask)												// selected channel in bit mask?
		{
			if(NewState)
			{
				((_CHIP_DMA_T*)pPeri)->CH[i].CONFIG &= ~(1 << 18);					// release GPDMA - clear HALT bit
				((_CHIP_DMA_T*)pPeri)->CH[i].CONFIG |= (1 << 0);					// enable GPDMA
			}
			else 
			{
				((_CHIP_DMA_T*)pPeri)->CH[i].CONFIG |= (1 << 18);					// Stop GPDMA - set HALT bit
				while(((_CHIP_DMA_T*)pPeri)->CH[i].CONFIG & (1 << 17));				// wait while active is set
				((_CHIP_DMA_T*)pPeri)->CH[i].CONFIG &= ~(1 << 0);					// disable GPDMA
			}
		}
	}
	
	return(res);
}	




// ******************************************************************************************************
// EEPROM Functions
// ******************************************************************************************************
extern void *_Chip_EEPROM_Init(int32_t PeriIndex);
extern bool _Chip_EEPROM_Enable(void *pPeri, bool NewState);
extern bool _Chip_EEPROM_IRQ_Enable(void *pPeri, bool NewState);
extern bool _Chip_EEPROM_Write(void *pPeri, uint32_t EEDst, uint8_t *pInBuff, size_t byteswr);
extern bool _Chip_EEPROM_Read(void *pPeri, uint32_t EESrc, uint8_t *pOutBuff, size_t bytesrd);

// ------------------------------------------------------------------------------------------------------
// Power down / up
extern inline bool _Chip_EEPROM_Power(void *pPeri, bool NewState)
{
	return(true);																	// Doesn't support power down mode for this periphery
}



// ******************************************************************************************************
// PWM Functions
// ******************************************************************************************************
extern void *_Chip_PWM_Init(int32_t PeriIndex);
extern bool _Chip_PWM_IRQ_Enable(void *pPeri, bool NewState);


typedef struct 							/*!< PWM Structure          */
{
	__IO uint32_t  IR;
	__IO uint32_t  TCR;
	__IO uint32_t  TC;
	__IO uint32_t  PR;
	__IO uint32_t  PC;
	__IO uint32_t  MCR;
	__IO uint32_t  MR0;
	__IO uint32_t  MR1;
	__IO uint32_t  MR2;
	__IO uint32_t  MR3;
	__IO uint32_t  CCR;
	__I  uint32_t  CR0;
	__I  uint32_t  CR1;
	__I  uint32_t  RESERVED0[3];
	__IO uint32_t  MR4;
	__IO uint32_t  MR5;
	__IO uint32_t  MR6;
	__IO uint32_t  PCR;
	__IO uint32_t  LER;
	__I  uint32_t  RESERVED1[3];
	__IO uint32_t  CTCR;
} LPC_PWM_T;


// ------------------------------------------------------------------------------------------------------
// Return Index from pointer
static inline int32_t _Chip_PWM_Get_PeriIndex(void *pPeri)							// Return Index from pointer
{
	if((LPC_PWM_T*) pPeri == LPC_PWM1) return(1);
	if((LPC_PWM_T*) pPeri == LPC_PWM0) return(0);
	return(-1);																		// otherwise error
}

#endif 		// ! LOAD_SCATTER
#endif		//__SERIE_LPC17XX_H_
