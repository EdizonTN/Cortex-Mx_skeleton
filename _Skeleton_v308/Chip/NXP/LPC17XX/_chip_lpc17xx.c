// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: _chip_lpc17xx.c
// 	   Version: 3.02
//		  Desc: MCU LPC17xx Chip description/configuration file. Same for all MCUs of this family!
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//				2024.09.26	- v3.02 - rename _CHIP_xxx_Handler to _Chip_xxx_Handler
//				2023.11.20	- v 3.01 - rename handler to sys_xxx_handler
//				2020.07.08	- rename Pld: _Chip_UART_IRQRx_Enable ---> _Chip_UART_IRQ_Peri_Enable
//							- rename Pld: _Chip_UART_IRQTx_Enable ---> _Chip_UART_IRQ_Peri_Enable, add parameter


#include "Skeleton.h"


#if defined (CONF_CHIP_ID_LPC1788FBD208) || 	\
	defined (CONF_CHIP_ID_LPC1788FET208) || 	\
	defined (CONF_CHIP_ID_LPC1788FET180) ||		\
	defined (CONF_CHIP_ID_LPC1788FBD144) ||		\
	defined (CONF_CHIP_ID_LPC1787FBD208) ||		\
	defined (CONF_CHIP_ID_LPC1786FBD208) ||		\
	defined (CONF_CHIP_ID_LPC1785FBD208) ||		\
	defined (CONF_CHIP_ID_LPC1778FBD208) ||		\
	defined (CONF_CHIP_ID_LPC1778FET208) ||		\
	defined (CONF_CHIP_ID_LPC1778FET180) ||		\
	defined (CONF_CHIP_ID_LPC1778FBD144) ||		\
	defined (CONF_CHIP_ID_LPC1777FBD208) ||		\
	defined (CONF_CHIP_ID_LPC1776FBD208) ||		\
	defined (CONF_CHIP_ID_LPC1776FET180) ||		\
	defined (CONF_CHIP_ID_LPC1774FBD208) ||		\
	defined (CONF_CHIP_ID_LPC1774FBD144)



//// Now, load original OpelLPC (or another) peripherial library.
//#include 	"sysctl_17xx_40xx.c"													// SysCon regs.
//#include 	"clock_17xx_40xx.c"														// CLOCK
//#include 	"iocon_17xx_40xx.c"														// IOCON
//#include 	"gpio_17xx_40xx.c"														// GPIO
//#include 	"iap.c"																	// IAP
//#include 	"eeprom_17xx_40xx.c"													// EEPROM
//#include 	"uart_17xx_40xx.c"														// UART
//#include 	"adc_17xx_40xx.c"														// ADC
//#include 	"gpdma_17xx_40xx.c"														// DMA
//#include 	"i2c_17xx_40xx.c"														// I2C

//ATTR_WEAK extern void 	__valid_user_code_checksum(void);


#if defined (__REDLIB__)
extern void __main(void);
#endif
extern int main(void);


void _Chip_Default_IRQ_Handler(void) 
{
	dbgprint("\r\n.Unhandlered exception! .\r\n");
	// NVIC->IABR[0-8];		// active interrupt flag
}

void sys_Reset_Handler					(void)  __WEAK_DEFAULT;
void sys_HardFault_Handler				(void)  __WEAK_DEFAULT;

void _Chip_WDT0_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_TIMER0_IRQ_Handler		(void)  __WEAK_DEFAULT;
void _Chip_TIMER1_IRQ_Handler		(void)  __WEAK_DEFAULT;
void _Chip_TIMER2_IRQ_Handler		(void)  __WEAK_DEFAULT;
void _Chip_TIMER3_IRQ_Handler		(void)  __WEAK_DEFAULT;
void _Chip_UART0_IRQ_Handler		(void)  __WEAK_DEFAULT;
void _Chip_UART1_IRQ_Handler		(void)  __WEAK_DEFAULT;
void _Chip_UART2_IRQ_Handler		(void)  __WEAK_DEFAULT;
void _Chip_UART3_IRQ_Handler		(void)  __WEAK_DEFAULT;
void _Chip_PWM1_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_I2C0_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_I2C1_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_I2C2_IRQ_Handler			(void)  __WEAK_DEFAULT;

void _Chip_SSP0_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_SSP1_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_PLL0_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_RTC0_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_EINT0_IRQ_Handler		(void)  __WEAK_DEFAULT;
void _Chip_EINT1_IRQ_Handler		(void)  __WEAK_DEFAULT;
void _Chip_EINT2_IRQ_Handler		(void)  __WEAK_DEFAULT;
void _Chip_EINT3_IRQ_Handler		(void)  __WEAK_DEFAULT;
void _Chip_ADC0_SEQA_IRQ_Handler	(void)  __WEAK_DEFAULT;
void _Chip_BOD0_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_USB0_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_CAN0_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_DMA0_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_I2S0_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_ETH0_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_SDC0_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_MCPWM0_IRQ_Handler		(void)  __WEAK_DEFAULT;
void _Chip_QEI0_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_PLL1_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_USB0_Act_IRQ_Handler		(void)  __WEAK_DEFAULT;
void _Chip_CAN0_Act_IRQ_Handler		(void)  __WEAK_DEFAULT;
void _Chip_UART4_IRQ_Handler		(void)  __WEAK_DEFAULT;
void _Chip_SSP2_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_LCD0_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_GPIO_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_PWM0_IRQ_Handler			(void)  __WEAK_DEFAULT;
void _Chip_EEPROM0_IRQ_Handler		(void)  __WEAK_DEFAULT;

extern const uint32_t *StatckInit;
// define Cortex-M base interrupt vectors 
const cortexm_base_t cortex_vector_base __SECTION_IRQVECTORS  = 
{
	CONF_STACK_START+CONF_STACK_SIZE,
    {
        [ 0] = sys_Reset_Handler,														/* entry point of the program */
        [ 1] = sys_NMI_Handler,															/* [-14] non maskable interrupt handler */
        [ 2] = sys_HardFault_Handler,													/* [-13] hard fault exception */
        [ 3] = sys_MemManage_Handler,													/* [-12] memory manage exception */
        [ 4] = sys_BusFault_Handler,													/* [-11] bus fault exception */
        [ 5] = sys_UsageFault_Handler,													/* [-10] usage fault exception */
        [ 6] = NULL,
		[ 7] = NULL,
		[ 8] = NULL,
		[ 9] = NULL,
        [10] = sys_SVC_Handler,															/* [-5] SW interrupt, in RIOT used for triggering context switches */
        [11] = sys_DebugMon_Handler,													/* [-4] debug monitor exception */
		[12] = NULL,
        [13] = sys_PendSV_Handler,														/* [-2] pendSV interrupt, in RIOT use to do the actual context switch */
        [14] = sys_SysTick_Handler,														/* [-1] SysTick interrupt, not used in RIOT */
    },
	{
		[ 0] = IRQHANDLER_16,														// WDT
		[ 1] = IRQHANDLER_17,														// Timer 0
		[ 2] = IRQHANDLER_18,														// Timer 1
		[ 3] = IRQHANDLER_19,														// Timer 2
		[ 4] = IRQHANDLER_20,														// Timer 3
		[ 5] = IRQHANDLER_21,														// UART 0
		[ 6] = IRQHANDLER_22,														// UART 1
		[ 7] = IRQHANDLER_23,														// UART 2
		[ 8] = IRQHANDLER_24,														// UART 3
		[ 9] = IRQHANDLER_25,														// PWM 1
		[10] = IRQHANDLER_26,														// I2C0
		[11] = IRQHANDLER_27,														// I2C1
		[12] = IRQHANDLER_28,														// I2C2
		[13] = IRQHANDLER_29,														//  --- Unused
		[14] = IRQHANDLER_30,														// SSP 0
		[15] = IRQHANDLER_31,														// SSP 1
		[16] = IRQHANDLER_32,														// PLL 0
		[17] = IRQHANDLER_33,														// RTC and Event Monitor/Recorder
		[18] = IRQHANDLER_34,														// EINT 0
		[19] = IRQHANDLER_35,														// EINT 1
		[20] = IRQHANDLER_36,														// EINT 2
		[21] = IRQHANDLER_37,														// EINT 3
		[22] = IRQHANDLER_38,														// ADC
		[23] = IRQHANDLER_39,														// BOD
		[24] = IRQHANDLER_40,														// USB
		[25] = IRQHANDLER_41,														// CAN
		[26] = IRQHANDLER_42,														// DMA
		[27] = IRQHANDLER_43,														// I2S
		[28] = IRQHANDLER_44,														// ETH
		[29] = IRQHANDLER_45,														// SD Card
		[30] = IRQHANDLER_46,														// MCPWM
		[31] = IRQHANDLER_47,														// QEI
		[32] = IRQHANDLER_48,														// PLL 1
		[33] = IRQHANDLER_49,														// USB Act. Int.
		[34] = IRQHANDLER_50,														// CAN Act. Int.
		[35] = IRQHANDLER_51,														// UART 4
		[36] = IRQHANDLER_52,														// SSP 2
		[37] = IRQHANDLER_53,														// LCD
		[38] = IRQHANDLER_54,														// GPIO
		[39] = IRQHANDLER_55,														// PWM 0
		[40] = IRQHANDLER_56														// EEPROM
	}
};



// ************************************************************************************************
// PUBLIC Functions
// ************************************************************************************************



// ************************************************************************************************
// PRIVATE Variables
// ************************************************************************************************
typedef void (*IAP)(unsigned int [],unsigned int[]);


// ******************************************************************************************************
// CHIP functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------
// Prepare sector for write operation - This command must be executed before executing "Copy RAM to flash" or "Erase Sector(s)" command. 
//					Successful execution of the "Copy RAM to flash" or "Erase Sector(s)" command causes relevant sectors to be protected again. 
//					The boot sector can not be prepared by this command. To prepare a single sector use the same "Start" and "End" sector numbers.
//return 	result[0] : Status Code
bool _Chip_IAP_PrepareSector(uint32_t strSector, uint32_t endSector)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_PREWRRITE_CMD;
	Command[1] = strSector;
	Command[2] = endSector;
	_Chip_IAP_Entry(Command, Result);

	if(Result[0] == 0) return(true);
	return (false);	
}


// ------------------------------------------------------------------------------------------------
// Copy RAM to flash - This command is used to program the flash memory. 
//					The affected sectors should be prepared first by calling "Prepare Sector for Write Operation" command. 
//					The affected sectors are automatically protected again once the copy command is successfully executed. 
//					The boot sector can not be written by this command. Also see Section 34.4.3 for the number of bytes that can be written.
//					Param3 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
//					Remark: All user code must be written in such a way that no master accesses the flash while this command is executed and the flash is programmed.
//return 	result[0] : Status Code
bool _Chip_IAP_CopyRamToFlash(uint32_t DstAdd, uint32_t *pSrc, uint32_t byteswrt)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_WRISECTOR_CMD;
	Command[1] = DstAdd;
	Command[2] = (uint32_t) pSrc;
	Command[3] = byteswrt;															// Should be 256 | 512 | 1024 | 4096.
	Command[4] = SystemCoreClock / 1000;
	_Chip_IAP_Entry(Command, Result);

	if(Result[0] == 0) return(true);
	return (false);	
}

//// ------------------------------------------------------------------------------------------------
//// Erase Page - This command is used to erase a page or multiple pages of on-chip flash memory. 
////				To erase a single page use the same "Start" and "End" page numbers.
////				Param2 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
////				Remark: All user code must be written in such a way that no master accesses the flash while this command is executed and the flash is erased.
////return 	result[0] : Status Code
//bool _Chip_IAP_ErasePage(uint32_t strPage, uint32_t endPage)
//{
//	uint32_t Command[5], Result[5];

//	Command[0] = _CHIP_IAP_ERASE_PAGE_CMD;
//	Command[1] = strPage;
//	Command[2] = endPage;
//	Command[3] = SystemCoreClock / 1000;
//	_Chip_IAP_Entry(Command, Result);

//	if(Result[0] == 0) return(true);
//	return (false);
//}

// ------------------------------------------------------------------------------------------------
// Blank check sector - This command is used to blank check a sector or multiple sectors of on-chip flash memory. 
//					To blank check a single sector use the same "Start" and "End" sector numbers.
// Result is True if sector/s is blank. Otherwise false
bool _Chip_IAP_BlankCheckSector(uint32_t strSector, uint32_t endSector)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_BLANK_CHECK_SECTOR_CMD;
	Command[1] = strSector;
	Command[2] = endSector;
	_Chip_IAP_Entry(Command, Result);

	if(Result[0] == 0) return(true);
	return (false);
}

// ------------------------------------------------------------------------------------------------
// Read_MCUChipID
// Read MCU ID 
// Readed ID is saved into memory at address dst
// Result is error code
bool _Chip_IAP_Read_ID(uint32_t *dst)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_REPID_CMD;
	_Chip_IAP_Entry(Command, Result);
	
	if(Result[0] == 0)																// Function call SUCCESS?
	{
		*dst = Result[1];															// save readed value
		return(true);
	}
	else
	{
		*dst = NULL;
		return (false);
	}
}

// ------------------------------------------------------------------------------------------------
// Read_MCUSerialNum
// Read MCU serial Number
// Readed SerialNumber is saved into memory at address dst
// Result is error code
bool _Chip_IAP_Read_SerialNum(uint32_t *dst)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_READ_UID_CMD;
	_Chip_IAP_Entry(Command, Result);
	
	if(Result[0] == 0)																// Function call SUCCESS?
	{
		*dst++ = Result[1];															// save readed value
		*dst++ = Result[2];															// save readed value
		*dst++ = Result[3];															// save readed value
		*dst = Result[4];															// save readed value
		return(true);
	}
	else
	{
		*dst = 0xFFFFFFFF;
		return (false);
	}
}

// ------------------------------------------------------------------------------------------------
// Read BootLoader version in MCU
// Result is success code
bool _Chip_IAP_Read_BootCodeVersion(uint32_t *dst)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_READ_BOOT_CODE_CMD;
	_Chip_IAP_Entry(Command, Result);
	
	if(Result[0] == 0)																// Function call SUCCESS?
	{
		*dst = Result[1];															// save readed value
		return(true);
	}
	else
	{
		*dst = NULL;
		return (false);
	}
}

// ****************************************************************************************************** 
// CLOCK Functions
// ****************************************************************************************************** 
uint32_t  SystemCoreClock;															// for IAP - System Clock Frequency (Core Clock)














// ************************************************************************************************
// GPIO Functions 
// ************************************************************************************************

// GPIO are same as other LPC family
// only initialization can be different


// ------------------------------------------------------------------------------------------------
// HW Initialization
// Initialize port and pin
// ex:  drv_GPIO_Init(sig_STATUS_0)
void *_Chip_GPIO_Init ( int32_t PeriIndex, uint32_t Pin)
{
	void *res = NULL;
	
	res = _Chip_Get_GPIO_Ptr(PeriIndex);
	if(res == NULL ) return (res);
	
	if(LPC_SYSCON->PCONP & (1 << 15))												// if GPIO is already initialized, skip it.
	{
		LPC_SYSCON->RSTCON[0] &= ~(1 << 15);										// dassert reset
	}
	else
	{
		LPC_SYSCON->PCONP |= (1 << 15);												// enable GPIO in PCONP
		LPC_SYSCON->RSTCON[0] |= (1 << 15);											// assert reset
		LPC_SYSCON->RSTCON[0] &= ~(1 << 15);										// dassert reset

	}
	
	return(res);
}

// ------------------------------------------------------------------------------------------------
// Hardware periphery Deinitialization
bool _Chip_GPIO_DeInit ( int32_t PeriIndex, uint32_t Pin)
{
	void *res = _Chip_Get_GPIO_Ptr(PeriIndex);
	if(res == NULL ) return (res);
	
	LPC_SYSCON->PCONP &= ~(1 << 15); 												// disable GPIO in PCONP - LPC17xx uses one bit for all GPIOs
	return(true);
}


// ------------------------------------------------------------------------------------------------
// Pin configure - for LPC17xx (without SWM Block)
// Index- target register
// 	0	- IOCON : bit 7-0
// 	1	- IOCON : bit 15-8
//	2	- not used
//  3 	- not used
// ex:

void _Chip_GPIO_Wr_Conf_Pin (uint32_t Port, uint32_t Pin, uint8_t Reg_Idx, const _Chip_IO_Spec_t *Reg_Val)		// zapis do registra Reg_Idx hodnotu Reg_Val
{
	uint32_t tmp = 0;

	//tmp = *((&LPC_IOCON->PIO0_0) + (Port * 32) + Pin);							// nacitaj povodnu hodnotu IOCON
	tmp = *(&LPC_IOCON->p[Port][Pin]);												// nacitaj povodnu hodnotu IOCON
	
	tmp &= 0xffff0000;																// zmaz spodne 2 byte
	tmp |= Reg_Val->IOCON_Reg;														// spodny - 0 byte nasav zadanou hodnotou

	LPC_IOCON->p[Port][Pin] = tmp;													// a zapis do IOCON
}

// ------------------------------------------------------------------------------------------------
// Pin configure - interrupt generation conditions - Pin change interrupt
void *_Chip_GPIO_PINT_Conf(uint8_t PINTSel, uint32_t Port, uint32_t Pin, uint8_t Sens)
{
	return(NULL);
}

// ------------------------------------------------------------------------------------------------
// Pin configure - interrupt generation conditions - Grouped interrupt
void *_Chip_GPIO_GINT_Conf(uint8_t Group, uint32_t Port, uint32_t Pin, uint8_t Sens)
{
	return(NULL);
}
	
// ------------------------------------------------------------------------------------------------
// Pin configure - interrupt generation conditions - Grouped interrupt
//	Sens (for whole group!):
// 	0x00	- level - low
// 	0x11	- level - high
// 	0x01	- edge - low to high
// 	0x10	- edge - high to low
// return is pointer to GINT
void *_Chip_GPIO_IRQ_Conf(uint8_t Group, uint32_t Port, uint32_t Pin, uint8_t Sens)
{
	void* res;
	if(Group > 1) return(NULL);
	
	//Chip_Clock_EnablePeriphClock(SYSCON_CLOCK_GINT);								// Run GINT

	//LPC_GINT[Group].CTRL &= ~GPIOGR_COMB;											// GPIO interrupt OR(0)/AND(1) mode bit - set OR condition
	
	switch(Sens)
	{
		case 0x00: 		// level - Low
			{
				return(NULL);
			}
		case 0x01:		// edge - rising
			{
				if(Port == 0) {LPC_GPIOINT->IO0.ENR |= (1 << Pin); LPC_GPIOINT->IO0.CLR |= (1 << Pin); res = &LPC_GPIOINT->IO0;}
				if(Port == 2) {LPC_GPIOINT->IO2.ENR |= (1 << Pin); LPC_GPIOINT->IO2.CLR |= (1 << Pin); res = &LPC_GPIOINT->IO2;}
				break;
			}
		case 0x10:		// edge - falling
			{
				if(Port == 0) {LPC_GPIOINT->IO0.ENF |= (1 << Pin); LPC_GPIOINT->IO0.CLR |= (1 << Pin); res = &LPC_GPIOINT->IO0;}
				if(Port == 2) {LPC_GPIOINT->IO2.ENF |= (1 << Pin); LPC_GPIOINT->IO2.CLR |= (1 << Pin); res = &LPC_GPIOINT->IO2;}
				break;
			}
		case 0x11: 		// level - high
			{
				return(NULL);
			}
		default: return(false);
				
	}
	
	NVIC_ClearPendingIRQ(GPIO_IRQn);											// clear pending interrupt flag
	return(res);
}


// ------------------------------------------------------------------------------------------------
// Pin configure - enable/disable interrupt generation from selectet Port/Pin
bool _Chip_GPIO_IRQ_Enable(void *pPeri, uint32_t Port, uint32_t Pin, bool NewState)
{
	if(NewState == true)
	{
		NVIC_EnableIRQ( (IRQn_Type) (GPIO_IRQn));									// Enable GPIO0 interrupt
	}
	else
	{
		NVIC_DisableIRQ( (IRQn_Type) (GPIO_IRQn));									// Disable GPIO0 interrupt
	}
	return(true);
}


// ************************************************************************************************
// PWM Functions
// ************************************************************************************************
// ------------------------------------------------------------------------------------------------------
// PWM(Num) Initialization
void *_Chip_PWM_Init(int32_t PeriIndex)
{
	switch(PeriIndex)
	{
		case 1: 
		{
			//Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_PWM1); 
			LPC_SYSCON->PCONP |= (1 << 6);											// enable GPIO in PCONP
			//Chip_SYSCTL_PeriphReset(SYSCTL_RESET_PWM1);
			LPC_SYSCON->RSTCON[0] |= (1 << 6);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 6);										// dassert reset			
			return(LPC_PWM1);
		}
		case 0: 
		{
			//Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_PWM0); 
			LPC_SYSCON->PCONP |= (1 << 5);											// enable GPIO in PCONP
			//Chip_SYSCTL_PeriphReset(SYSCTL_RESET_PWM0);
			LPC_SYSCON->RSTCON[0] |= (1 << 5);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 5);										// dassert reset
			return(LPC_PWM0);
		}			
	}
	return(NULL);
}

// ------------------------------------------------------------------------------------------------------
// PWM Enable/Disable interrupt
// Return True if Ena/Disa was successfull, otherwise false
bool _Chip_PWM_IRQ_Enable(void *pPeri, bool NewState)
{
	if(NewState) 
	{
		switch((intptr_t) pPeri)
		{
			case (intptr_t) LPC_PWM1: NVIC_EnableIRQ( (IRQn_Type) (PWM1_IRQn));		// Enable PWM interrupt
					break;
			case (intptr_t) LPC_PWM0: NVIC_EnableIRQ( (IRQn_Type) (PWM0_IRQn));		// Enable PWM interrupt
					break;
			default: return(false);
		}
	}
	else
	{		
		switch((intptr_t) pPeri)
		{
			case (intptr_t) LPC_PWM1: NVIC_DisableIRQ( (IRQn_Type) (PWM1_IRQn));						// Disable PWM interrupt
					break;
			case (intptr_t) LPC_PWM0: NVIC_DisableIRQ( (IRQn_Type) (PWM0_IRQn));						// Disable PWM interrupt
					break;
			default: return(false);
		}
	}
	return(true);
}


// ************************************************************************************************
// EEPROM Functions
// ************************************************************************************************
union onepage
{
	uint32_t word[128/4];
	uint8_t	 byte[128];
} ;

// ------------------------------------------------------------------------------------------------------
// EEPROM(Num) Initialization
void *_Chip_EEPROM_Init(int32_t PeriIndex)
{
	if(PeriIndex != 0) return(NULL);
	//LPC_SYSCON->PCONP |= (1 << 5);													// enable GPIO in PCONP
	//LPC_SYSCON->RSTCON[0] |= (1 << 5);												// assert reset
	//LPC_SYSCON->RSTCON[0] &= ~(1 << 5);												// dassert reset
	
	NVIC_DisableIRQ( (IRQn_Type) (EEPROM_IRQn));									// Disable EEPROM interrupt
	//Chip_EEPROM_Init(LPC_EEPROM);													// Call Init
	return(LPC_EEPROM);
}


// ------------------------------------------------------------------------------------------------------
// EEPROM Enable/Disable
// Return True if Ena/Disa was successfull, otherwise false
bool _Chip_EEPROM_Enable(void *pPeri, bool NewState)
{
	if(NewState) 
	{
		Chip_EEPROM_EnablePowerDown(LPC_EEPROM);									// enable EEPROM power
	}
	else
	{		
		Chip_EEPROM_DisablePowerDown(LPC_EEPROM);									// disable EEPROM power
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// EEPROM Enable/Disable interrupt
// Return True if Ena/Disa was successfull, otherwise false
bool _Chip_EEPROM_IRQ_Enable(void *pPeri, bool NewState)
{
	if(NewState) 
	{
		NVIC_EnableIRQ( (IRQn_Type) (EEPROM_IRQn));									// Enable EEPROM interrupt
	}
	else
	{		
		NVIC_DisableIRQ( (IRQn_Type) (EEPROM_IRQn));								// Disable EEPROM interrupt
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Write data to EEPROM
// non interrupt access = blocking 
// EEPROM with Page access - read page which contain required data and then copy their to dst buffer
bool _Chip_EEPROM_Write(void *pPeri, uint32_t EEDst, uint8_t *pInBuff, size_t byteswr)
{
	bool res = false;
	uint32_t wrPage;
	uint32_t wrOffset;
    uint8_t	 byteshift, elSize;														// align to elSize - shift left or right
	uint32_t wrSize, wroted, nextwrite;
	uint8_t	 Buffer[EEPROM_PAGE_SIZE];
	
	
	if((byteswr % 4) == 0) elSize = 4;												// find maximal transfer size
	else if ((byteswr % 2) == 0) elSize = 2;
	else elSize = 1;
	
	
	
	if(EEDst > EEPROM_PAGE_SIZE) wrPage =  EEDst / EEPROM_PAGE_SIZE;				// require read from page 1-... ?
	else wrPage = 0;																// will start read from page 0
	wrOffset = EEDst - (wrPage * EEPROM_PAGE_NUM);									// select offset in page
	byteshift = (wrOffset % elSize);
	wrOffset -= byteshift;															// align to elSize
	wrSize = byteswr;																// read length. Must not exceeds PAGE_SIZE
	wroted = 0;
	
	
	do 
	{
		nextwrite = (((wrSize+byteshift) - wroted) > EEPROM_PAGE_SIZE) ? (EEPROM_PAGE_SIZE): (wrSize+byteshift) - wroted;	// remains part of total writed bytes
		memcpy(&Buffer[0+byteshift], pInBuff, nextwrite-byteshift);							// save only needed count
		dbgprint("\r\n\t(_chip_lpc17xx) EEPROM Wr[%p], Page:0x%X, Offset:0x%X, 0x%X next bytes", pPeri, wrPage, wrOffset+byteshift, nextwrite);
//		Chip_EEPROM_WritePageRegister(pPeri, wrOffset, &Buffer[0], elSize, nextwrite);
//		Chip_EEPROM_EraseProgramPage(pPeri, wrPage);
		wroted += nextwrite;
		wroted -= byteshift;														// byuteshift correction
		wrPage ++;																	// increase to next page
		wrOffset = 0;																// next page must read from start
	}while (byteswr != wroted);
		
	return (res);
}

// ------------------------------------------------------------------------------------------------------
// Read data from EEPROM
// non interrupt access = blocking 
// EEPROM with Page access - read page which contain required data and then copy their to dst buffer
bool _Chip_EEPROM_Read(void *pPeri, uint32_t EESrc, uint8_t *pOutBuff, size_t bytesrd)
{
	bool res = false;
	uint32_t rdPage;
	uint32_t rdOffset;
    uint8_t	 byteshift, elSize;														// align to elSize - shift left or right
	uint32_t readed;
	//uint32_t rdSize;
	uint8_t	 Buffer[EEPROM_PAGE_SIZE];
	
	
	if((bytesrd % 4) == 0) elSize = 4;												// find maximal transfer size
	else if ((bytesrd % 2) == 0) elSize = 2;
	else elSize = 1;
	
	if(EESrc > EEPROM_PAGE_SIZE) rdPage =  EESrc / EEPROM_PAGE_SIZE;				// require read from page 1-... ?
	else rdPage = 0;																// will start read from page 0
	rdOffset = EESrc - (rdPage * EEPROM_PAGE_NUM);									// select offset in page
	byteshift = (rdOffset % elSize);
	rdOffset -= byteshift;															// align to elSize
	//rdSize = bytesrd;																// read length. Must not exceeds PAGE_SIZE
	readed = 0;
		
	do 
	{
		//dbgprint("\r\n\t(_chip_lpc17xx) EEPROM Rd[%p], Page:0x%X, Offset:0x%X, 0x%X Bytes", pPeri, rdPage, rdOffset+byteshift, rdSize);
//		readed += Chip_EEPROM_ReadPage(pPeri, rdOffset, rdPage, &Buffer[0], elSize, ((rdSize+byteshift > EEPROM_PAGE_SIZE) ? (EEPROM_PAGE_SIZE): (rdSize+byteshift)));
		dbgprint("\r\n\t(_chip_lpc17xx) EEPROM Rd[%p], Page:0x%X, Offset:0x%X, 0x%X bytes total", pPeri, rdPage, rdOffset+byteshift, readed);
		
		readed -= byteshift;														// byuteshift correction
		memcpy(pOutBuff, &Buffer[0+byteshift], readed);								// save only needed count
		pOutBuff += readed;
		rdPage ++;																	// increase to next page
//		rdSize = bytesrd - readed;													// remains part of total read bytes
		rdOffset = 0;																// next page must read from start
	}while (bytesrd != readed);

	res = true;
	return (res);
}




// ************************************************************************************************
// UART Functions
// ************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// UART(Num) Initialization
// return is pointyer to periphery, otherwise NULL
void *_Chip_UART_Init(int32_t PeriIndex)
{
	void *res = NULL;
	
	NVIC_DisableIRQ( (IRQn_Type) (UART0_IRQn + PeriIndex));							// Disable UART interrupt
	switch(PeriIndex)
	{
		case 4:
		{
			//Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_UART4);						// enable UART clock
			//Chip_SYSCTL_PeriphReset(SYSCTL_RESET_UART4);							// periphery reset
			LPC_SYSCON->PCONP |= (1 << 8);											// enable GPIO in PCONP
			LPC_SYSCON->RSTCON[0] |= (1 << 8);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 8);										// dassert reset				
			res = LPC_UART4;														// return pointer to periphery
			break;
		}
		case 3:
		{
			//Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_UART3);						// enable UART clock
			//Chip_SYSCTL_PeriphReset(SYSCTL_RESET_UART3);							// periphery reset
			LPC_SYSCON->PCONP |= (1 << 25);											// enable GPIO in PCONP
			LPC_SYSCON->RSTCON[0] |= (1 << 25);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 25);										// dassert reset							
			res = LPC_UART3;														// return pointer to periphery
			break;
		}		
		case 2:
		{
			//Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_UART2);						// enable UART clock
			//Chip_SYSCTL_PeriphReset(SYSCTL_RESET_UART2);							// periphery reset
			LPC_SYSCON->PCONP |= (1 << 24);											// enable GPIO in PCONP
			LPC_SYSCON->RSTCON[0] |= (1 << 24);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 24);										// dassert reset							
			res = LPC_UART2;														// return pointer to periphery
			break;
		}
		case 1:
		{
			//Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_UART1);						// enable UART clock
			//Chip_SYSCTL_PeriphReset(SYSCTL_RESET_UART1);							// periphery reset
			LPC_SYSCON->PCONP |= (1 << 4);											// enable GPIO in PCONP
			LPC_SYSCON->RSTCON[0] |= (1 << 4);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 4);										// dassert reset							
			res = LPC_UART1;														// return pointer to periphery
			break;
		}
		case 0:
		{
			//Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_UART0);						// enable UART clock
			//Chip_SYSCTL_PeriphReset(SYSCTL_RESET_UART0);							// periphery reset
			LPC_SYSCON->PCONP |= (1 << 3);											// enable GPIO in PCONP
			LPC_SYSCON->RSTCON[0] |= (1 << 3);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 3);										// dassert reset							
			res = LPC_UART0;														// return pointer to periphery
			break;
		}
		default:
		{
			res = NULL;																// bad index of periphery
		}
	}
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// Enable/Disable UART
bool _Chip_UART_Enable(void *pPeri, bool NewState)
{	
	if(pPeri == NULL ) return(false);
	
	_Chip_UART_IRQ_Peri_Enable(pPeri, NewState);										// Enable/Disable RX interrupt
	_Chip_UART_IRQTx_Enable(pPeri, NewState);										// Enable/Disable TX interrupt
	
	uint8_t PeriIndex = _Chip_UART_Get_PeriIndex(pPeri);
	
	if(NewState == true)
	{
		((_CHIP_UART_T *)pPeri)->TER1 |= (1 << 7);									// Enable Transmit UART
		
		switch(PeriIndex)
		{
			case 0:
			{
				NVIC_EnableIRQ(UART0_IRQn);											// Enable UART interrupt
				NVIC_ClearPendingIRQ(UART0_IRQn);									// Clear pending interrupt
				break;
			}
			case 1:
			{
				NVIC_EnableIRQ(UART1_IRQn);											// Enable UART interrupt
				NVIC_ClearPendingIRQ(UART1_IRQn);									// Clear pending interrupt
				break;
			}				
			case 2:
			{
				NVIC_EnableIRQ(UART2_IRQn);											// Enable UART interrupt
				NVIC_ClearPendingIRQ(UART2_IRQn);									// Clear pending interrupt
				break;
			}				
			case 3:
			{
				NVIC_EnableIRQ(UART3_IRQn);											// Enable UART interrupt
				NVIC_ClearPendingIRQ(UART3_IRQn);									// Clear pending interrupt
				break;
			}				
			case 4:
			{
				NVIC_EnableIRQ(UART4_IRQn);											// Enable UART interrupt
				NVIC_ClearPendingIRQ(UART4_IRQn);									// Clear pending interrupt
				break;
			}	
			default: return(false);
		}

	}
	else
	{
		((_CHIP_UART_T *)pPeri)->TER1 &= ~(1 << 7);									// Disable Transmit UART
		
		switch(PeriIndex)
		{
			case 0:
			{
				NVIC_DisableIRQ(UART0_IRQn);										// Enable UART interrupt
				break;
			}
			case 1:
			{
				NVIC_DisableIRQ(UART1_IRQn);										// Enable UART interrupt
				break;
			}				
			case 2:
			{
				NVIC_DisableIRQ(UART2_IRQn);										// Enable UART interrupt
				break;
			}				
			case 3:
			{
				NVIC_DisableIRQ(UART3_IRQn);										// Enable UART interrupt
				break;
			}				
			case 4:
			{
				NVIC_DisableIRQ(UART4_IRQn);										// Enable UART interrupt
				break;
			}				
			default: return(false);
		}
		
	}
	
	return(true);
}



// ------------------------------------------------------------------------------------------------------
// Nastav pomer pre Baud Rate
bool _Chip_UART_Set_Baud(void* pPeri, uint32_t NewBaud)
{
    bool res = false;

    uint32_t d, m, bestd, bestm, tmp;
    uint64_t best_divisor, divisor;
    uint32_t current_error, best_error;
    uint32_t recalcbaud;

    /* In the Uart IP block, baud rate is calculated using FDR and DLL-DLM registers
    * The formula is :
    * BaudRate= uClk * (mulFracDiv/(mulFracDiv+dividerAddFracDiv) / (16 * (DLL)
    * It involves floating point calculations. That's the reason the formulae are adjusted with
    * Multiply and divide method.*/
    
    /* The value of mulFracDiv and dividerAddFracDiv should comply to the following expressions:
    * 0 < mulFracDiv <= 15, 0 <= dividerAddFracDiv <= 15 */
    best_error = 0xFFFFFFFF; /* Worst case */
    bestd = 0;
    bestm = 0;
    best_divisor = 0;
    
    for (m = 1 ; m <= 15 ;m++)
    {
        for (d = 0 ; d < m ; d++)
        {
            divisor = ((uint64_t)_Chip_Clock_Get_pclk_Rate() << 28)*m / (NewBaud * (m + d));
            current_error = divisor & 0xFFFFFFFF;
            tmp = divisor>>32;
            if(current_error > ((uint32_t)1<<31))								// Adjust error
            {
                current_error = -current_error;
                tmp++;
            }
            if(tmp < 1 || tmp > 65536) continue;								// Out of range
            if( current_error < best_error)
            {
                best_error = current_error;
                best_divisor = tmp;
                bestd = d;
                bestm = m;
                if(best_error == 0) break;
            }
        } // end of inner for loop
        if (best_error == 0)
            break;
    } // end of outer for loop
    // can not find best match */
    if(best_divisor == 0) return ERROR;

    recalcbaud = (_Chip_Clock_Get_pclk_Rate() >> 4) * bestm / (best_divisor * (bestm + bestd));

    if(NewBaud > recalcbaud) 													// reuse best_error to evaluate baud error
        best_error = NewBaud - recalcbaud;
    else 
        best_error = recalcbaud - NewBaud;

    best_error = best_error * 100 / NewBaud;

    if (best_error < 5)
    {
        ((LPC_USART_T *)pPeri)->LCR |= UART_LCR_DLAB_EN;
        ((LPC_USART_T *)pPeri)->DLM = (((best_divisor) >> 8) & 0xFF);
        ((LPC_USART_T *)pPeri)->DLL = ((best_divisor) & 0xFF);
        ((LPC_USART_T *)pPeri)->LCR &= (~UART_LCR_DLAB_EN) & UART_LCR_BITMASK;				// Then reset DLAB bit
        ((LPC_USART_T *)pPeri)->FDR = (((uint32_t)((bestm<<4)&0xF0)) | ((uint32_t)(bestd&0x0F))) & (uint32_t)0xff;
        res = true;
    }
    return res;
}


//// ------------------------------------------------------------------------------------------------------
//// Baud Rate settings. In case of overflow, increased divider from MainCLK
//// result is true:successfull otherwise false
//bool _Chip_UART_Set_Baud_old(void* pPeri, uint32_t NewBaud)
//{
//	
//	if(pPeri == NULL ) return(false);
//	
//	Chip_UART_SetBaudFDR(pPeri, NewBaud);
//	Chip_UART_SetBaud(pPeri, NewBaud);												// pokus sa nastavir BaudRate
//	
//	return(true);
//}

// ------------------------------------------------------------------------------------------------------
// Write configuration into UART
// DataLen: 5,6,7,8
// Parity: 0-none, 1-Space, 2-Odd, 3-Even, 4-Mark
// Stopbits: 0-1, 1-2
bool _Chip_UART_ConfigData(void* pPeri, uint8_t DataLen, uint8_t Parity, uint8_t StopBits)
{
	uint32_t Config = (DataLen-5) | ((StopBits-1) << 2);
	//uint32_t OldCFG = ((_CHIP_UART_T*)pPeri)->CFG;
	
	if(Parity > 0)																	// Odd/Even?
	{
		Config |= (1 << 3);															// enable parity
		Config |= (Parity - 2) << 4;
	}
	
	((LPC_USART_T *)pPeri)->LCR &= ~0x03;											// clear char len
	if(DataLen == 9) 																// 9-bit is not supportet netive. We must emulate it
	{
		DataLen = 8;																// set datalen as 8
		Parity = 1;																	// use parity bit as nine data bit.
	}
	
	((LPC_USART_T *)pPeri)->LCR |= (DataLen - 5) & 0x03;							// write new char len
	
	if(StopBits == 2) ((LPC_USART_T *)pPeri)->LCR |= (1 << 2);
	else	((LPC_USART_T *)pPeri)->LCR &= ~(1 << 2);

	((LPC_USART_T *)pPeri)->LCR &= ~(3 << 4);										// clear parity settings
	switch(Parity)
	{
		case 4:		// Mark Parity (forced "1")										// write 0x02
			((LPC_USART_T *)pPeri)->LCR |= (1 << 3);								// enable parity checking
			((LPC_USART_T *)pPeri)->LCR |= (2 << 4);								// Forced 1
			break;		
		case 3:		// Even parity
			((LPC_USART_T *)pPeri)->LCR |= (1 << 4);								// write 0x01
			((LPC_USART_T *)pPeri)->LCR |= (1 << 3);								// enable parity checking
			break;
		case 2:		// Odd Parity
			((LPC_USART_T *)pPeri)->LCR |= (1 << 3);								// enable parity checking
			break;																	// write 0x00
		case 1:		// Space Parity (forced "0")									// write 0x03
			((LPC_USART_T *)pPeri)->LCR |= (1 << 3);								// enable parity checking
			((LPC_USART_T *)pPeri)->LCR |= (3 << 4);								// Forced 0
			break;
		default:	// None
			((LPC_USART_T *)pPeri)->LCR &= ~(1 << 3);								// disable parity checking
	}
	
	if(StopBits == 1) ((LPC_USART_T *)pPeri)->LCR &= ~(1 << 2);						// 1 stop bit
	else ((LPC_USART_T *)pPeri)->LCR |= (1 << 2);									// 2 stop bits
	
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Configure RTS as flow control for UART
// result is true:successfull otherwise false
bool _Chip_UART_RTS_Configure(void* pPeri, uint8_t sRTS_Port, uint8_t sRTS_Pin)
{
	((LPC_USART_T *)pPeri)->MCR |= (1 << 6);										// RTS Enable

	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Configure CTS as flow control for UART
// result is true:successfull otherwise false
bool _Chip_UART_CTS_Configure(void* pPeri, uint8_t sCTS_Port, uint8_t sCTS_Pin)
{
	((LPC_USART_T *)pPeri)->MCR |= (1 << 7);										// CTS Enable

	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Configure DIRDE as flow control for RS485
// result is true:successfull otherwise false
// LPC17xx used RTS or DTR as OutputEnable
bool _Chip_UART_DIRDE_Configure(void* pPeri, uint8_t DIRDE_Port, uint8_t DIRDE_Pin, bool DIRDE_H_Active)
{
	((LPC_USART_T *)pPeri)->RS485CTRL |= (1 << 4);									// Enable auto direction control
	if(DIRDE_H_Active) ((LPC_USART_T *)pPeri)->RS485CTRL |= (1 << 5);				// Polarity: High
	else ((LPC_USART_T *)pPeri)->RS485CTRL &= ~(1 << 5);							// Polarity: Low
	
	return(true);
}


// ------------------------------------------------------------------------------------------------------
// Configure RE as receive enable control in RS485 mode
// result is true:successfull otherwise false
bool _Chip_UART_RE_Configure(void* pPeri, uint8_t RE_Port, uint8_t RE_Pin, bool RE_H_Active)
{
	return(false);																	// LPC17xx hasn't RE dedicated pin
}




// ************************************************************************************************
// ADC Functions
// ************************************************************************************************
bool _Chip_ADC_DMA_Mode;
bool _Chip_ADC_Repeat_Mode;

// ------------------------------------------------------------------------------------------------------
// ADC(Num) Initialization (PowerUp, Enable CLK, Init)
// result is true:successfull otherwise false
void *_Chip_ADC_Init(int32_t PeriIndex)
{
	if(PeriIndex != 0) return( NULL );												// LPC17xx have only oone USB
	
	//Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_ADC);								// enable ADC clock
	//Chip_SYSCTL_PeriphReset(SYSCTL_RESET_ADC);									// periphery reset
	LPC_SYSCON->PCONP |= (1 << 12);													// enable GPIO in PCONP
	LPC_ADC->CR = (1 << 21);														// clear active channel, deactivate power down
	LPC_ADC->INTEN = 0;
	
	LPC_SYSCON->RSTCON[0] |= (1 << 12);												// assert reset
	LPC_SYSCON->RSTCON[0] &= ~(1 << 12);											// dassert reset
	_Chip_ADC_DMA_Mode = false;														// default - IRQ mode
	
	
	
	return(LPC_ADC);
}

// ------------------------------------------------------------------------------------------------------
// Enable/Disable ADC
// if Channel is UINT32_MAX, enable while periphery without channels, otherwise ena/disa selected channel
// result is true:successfull otherwise false
bool _Chip_ADC_Enable(void* pPeri, uint32_t ChannelBitMask, bool NewState)
{
	bool res = false;
	
	if(pPeri == NULL ) return(false);
	uint8_t PeriIndex = _Chip_ADC_Get_PeriIndex(pPeri);								// return peripherial index


	res = _Chip_ADC_Enable_CHIRQ(pPeri, ChannelBitMask, NewState);					// ena/disa specified channels
	
	if(ChannelBitMask == UINT32_MAX)												// works with all channels?
	{
		if(NewState)
		{
			LPC_ADC->CR |= (1 << 21);												// Enable ADC		
			NVIC_ClearPendingIRQ(ADC_IRQn);											// clear pending interrupt flag
			if(_Chip_ADC_Repeat_Mode == true)
			{
				((LPC_ADC_T *)pPeri)->CR &= ~(1 << 16);								// disable burst mode in DMA
				((LPC_ADC_T *)pPeri)->CR &= ~(7 << 24);								// disable SW and HW Start
				((LPC_ADC_T *)pPeri)->CR |= (1 << 16);								// Set burst mode in DMA. This will Start AD conversions
			}
			if(_Chip_ADC_DMA_Mode == true)
			{
				NVIC_DisableIRQ(ADC_IRQn);											// Disable ADC interrupt in DMA mode
			}
			else NVIC_EnableIRQ(ADC_IRQn);											// Enable ADC interrupt in IRQ mode
		}
		else
		{
			NVIC_DisableIRQ(ADC_IRQn);												// Disable ADC interrupt
			LPC_ADC->CR &= ~(1 << 21);												// Disable ADC - power down
		}
	}
	return (res);
}

// ------------------------------------------------------------------------------------------------------
// Read active channel on ADC
// read from IOCON.A configuration for each ADC Channel. Aif set as Analog, Channel is active. Otherwise inactive
extern uint32_t _Chip_ADC_Get_ChannelMask(void *pPeri)
{
	uint32_t res = 0;
	

	for(uint8_t bit=23 ; bit<=26 ; bit++ )											// P0.23 - AD0.0; P0.24 - AD0.1; P0.25 - AD0.2; P0.26 - AD0.3
	{
		if(((LPC_IOCON->p[0][bit]) & 0x03) == 1)									// Analog input mode
		{
			res |= (1 << (bit - 23));												// set bits 0 to 3
		}
	}
	
	for(uint8_t bit=30 ; bit<=31 ; bit++ )											// P1.30 - AD0.4; P1.31 - AD0.5
	{
		if(((LPC_IOCON->p[1][bit]) & 0x03) == 3)									// Analog input mode
		{
			res |= (1 << (bit - 26));												// set bits 4 to 5
		}
	}	

	for(uint8_t bit=12 ; bit<=13 ; bit++ )											// P0.12 - AD0.6; P0.13 - AD0.7
	{
		if(((LPC_IOCON->p[0][bit]) & 0x03) == 3)									// Analog input mode
		{
			res |= (1 << (bit - 6));												// set bits 6 to 7
		}
	}
	
	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// Enable/Disable on-chip temperature sensor. In LPC17xx is not implemented
// result is true:successfull otherwise false
bool _Chip_Temp_Enable(bool NewState)
{
	return(false);
}

// ------------------------------------------------------------------------------------------------------
// ADC[x](ChannelBitMask) Configure whole ADC
// result is true:successfull otherwise false
bool _Chip_ADC_Configure (void *pPeri, uint32_t ChannelBitMask, uint32_t Speed_kHz, bool RepeatMode, bool DMAMode)
{
	int8_t i = _CHIP_ADC_CHANNELS_COUNT-1;
	
	if(pPeri == NULL ) return(false);
	
	((LPC_ADC_T *)pPeri)->INTEN = 0;												// diasable all interrupt requests

	((LPC_ADC_T *)pPeri)->CR = 0;													// Clear control  and stop ADC
	
	((LPC_ADC_T *)pPeri)->CR |= (((_Chip_Clock_Get_pclk_Rate() / (Speed_kHz*1000)) & 0xff) << 8); // AD CLK must be lower or equal to 12.4 MHz. Src is PCLK.
	
	((LPC_ADC_T *)pPeri)->CR |= ChannelBitMask;										// select BitMasked channels.

	if(DMAMode) 																	// in DMA mode, set IRQ from last channel, ang global IRQ Disable
	{
		_Chip_ADC_DMA_Mode = true;													// never enable global IRQ !!!
		do
		{
			if(ChannelBitMask & (1 << i)) {((LPC_ADC_T *)pPeri)->INTEN |= (1<<i); break;} // find biggest ective channel and enable IRQ
		}while(i--);
	}
	else 
	{
		_Chip_ADC_DMA_Mode = false;
		((LPC_ADC_T *)pPeri)->INTEN |= ChannelBitMask;								// Enable IRQ for each channel
	}
	
	if(RepeatMode) 
	{
		_Chip_ADC_Repeat_Mode = true;
		((LPC_ADC_T *)pPeri)->CR &= ~(7 << 24);										// disable SW and HW Start
		((LPC_ADC_T *)pPeri)->CR |= (1 << 16);										// Set burst mode, otherwise single mode
	}
	
	
	return (true);
}



// ************************************************************************************************
// Timer 0-3

// ------------------------------------------------------------------------------------------------------
// Timer(Num) Initialization
// result is true:successfull otherwise false
// PeriIndex is 0-3 for LPC17xx, each have 4 channels
void *_Chip_Timer_Init(int32_t PeriIndex)
{

	switch(PeriIndex)
	{
		case 3: 
		{
			//Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_TIMER3); 
			//Chip_SYSCTL_PeriphReset(SYSCTL_RESET_TIMER3);
			LPC_SYSCON->PCONP |= (1 << 23);											// enable GPIO in PCONP
			LPC_SYSCON->RSTCON[0] |= (1 << 23);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 23);									// dassert reset
			return(LPC_TIMER3);
		}
		case 2: 
		{
			//Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_TIMER2); 
			//Chip_SYSCTL_PeriphReset(SYSCTL_RESET_TIMER2);
			LPC_SYSCON->PCONP |= (1 << 22);											// enable GPIO in PCONP
			LPC_SYSCON->RSTCON[0] |= (1 << 22);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 22);									// dassert reset			
			return(LPC_TIMER2);
		}			
		case 1: 
		{
			//Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_TIMER1);
			//Chip_SYSCTL_PeriphReset(SYSCTL_RESET_TIMER1);
			LPC_SYSCON->PCONP |= (1 << 2);											// enable GPIO in PCONP
			LPC_SYSCON->RSTCON[0] |= (1 << 2);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 2);										// dassert reset			
			return(LPC_TIMER1);
		}			
		case 0:
		{
			//Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_TIMER0);
			//Chip_SYSCTL_PeriphReset(SYSCTL_RESET_TIMER0);
			LPC_SYSCON->PCONP |= (1 << 1);											// enable GPIO in PCONP
			LPC_SYSCON->RSTCON[0] |= (1 << 1);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 1);										// dassert reset			
			return(LPC_TIMER0);
		}			
		default:
		{
			return(NULL);
		}			

	}
}



// ------------------------------------------------------------------------------------------------------
// MRT(Num) Enable/disable whole timer
// result is true:successfull otherwise false
bool _Chip_Timer_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
	bool res;
	uint8_t TimerIndex = _Chip_Timer_Get_PeriIndex(pPeri);
	
	if(pPeri == NULL ) return(false);
	NVIC_ClearPendingIRQ((IRQn_Type)(TIMER0_IRQn + TimerIndex));					// clear pending interrupt flag

	res = _Chip_Timer_Enable_CHIRQ(pPeri, 1 << TimerIndex, NewState);				// ena/disa specified channels
	
	if(ChannelBitMask == UINT32_MAX)												// works with all channels?
	{
		if(NewState)	
		{
			((_CHIP_TIM_T*)pPeri)->TCR |= (1 << 0);									// Run Timer
			((_CHIP_TIM_T*)pPeri)->TCR &= ~(1 << 1);								// Clear reset counter
			NVIC_ClearPendingIRQ((IRQn_Type)(TIMER0_IRQn + TimerIndex));			// clear pending interrupt flag
			NVIC_EnableIRQ((IRQn_Type)(TIMER0_IRQn + TimerIndex));					// Enable Timer interrupt
		}
		else
		{
			// ToDo: check active channel. If nothing active stop timer and disable complette:
			((_CHIP_TIM_T*)pPeri)->TCR &= ~(1 << 0);								// Stop Timer
			((_CHIP_TIM_T*)pPeri)->TCR &= ~(1 << 1);								// Clear reset counter		
			NVIC_DisableIRQ((IRQn_Type)(TIMER0_IRQn + TimerIndex));					// Disable Timer interrupt
		}
	}
	return(res);																	// nothing to check. return Seccess
}

// ------------------------------------------------------------------------------------------------------
// Timer[x](Channel) Configure
// Channel 0-3
// if repeat==true, autoreload timer will executed, otherwise oneshot only
// result is true:successfull otherwise false
bool _Chip_Timer_Configure (void *pPeri, uint8_t Channel, uint32_t RateHz, bool RepeatMode)
{
	bool res = false;
	uint32_t OldTCR = ((_CHIP_TIM_T*)pPeri)->TCR;
	
	if (Channel > 3) return(false);													// LPC15xx have only channels 0-3
	if(pPeri == NULL ) return(false);
	
	uint64_t tmp = _Chip_Clock_Get_SysClk_Rate();									// Get System Clock
	
	tmp = tmp / RateHz;
	
	if(tmp <= 0xffffffff)															// is able to set ?
	{
		((_CHIP_TIM_T*)pPeri)->TCR &= ~(1 << 0);									// disable timer counting
		//((_CHIP_TIM_T*)pPeri)->TCR |= (1 << 1);										// timer counter reset
		//((_CHIP_TIM_T*)pPeri)->CTCR = 0;											// timer counter reset
		((_CHIP_TIM_T*)pPeri)->MR[Channel] = tmp; 									// Match register load
		//((_CHIP_TIM_T*)pPeri)->PC = 0; 												// Prescale Counter value
		((_CHIP_TIM_T*)pPeri)->IR =	(1 << Channel);									// clear interruptflags
		res = true;
	}

	if(RepeatMode == true)															// auto reload
	{
	uint32_t mask = 0;
	
		mask = (2 << (Channel*3));													// move Channel bit to MRxR
		((_CHIP_TIM_T*)pPeri)->MCR |= mask;
	}
	
	if(OldTCR & 0x01) ((_CHIP_TIM_T*)pPeri)->TCR |= 1;								// re-enable Counter
	
	return (res);
}



// ************************************************************************************************
// DMA Functions 
// ************************************************************************************************


// ------------------------------------------------------------------------------------------------------
// DMA(Num) Initialization
// result is true:successfull otherwise false
void *_Chip_DMA_Init(int32_t PeriIndex)
{
	if(PeriIndex != 0) return(false);
	
	LPC_SYSCON->PCONP |= (1 << 29);													// enable GPIO in PCONP
	LPC_SYSCON->RSTCON[0] |= (1 << 29);												// assert reset
	LPC_SYSCON->RSTCON[0] &= ~(1 << 29);											// dassert reset
	LPC_GPDMA->CONFIG |= (1 << 0);													// enable DMA block
	
	LPC_GPDMA->INTTCCLEAR |= 0x000000ff;
	LPC_GPDMA->INTERRCLR  |= 0x000000ff;
	
	return(LPC_GPDMA);																// nothing to check. return Seccess
}

// ------------------------------------------------------------------------------------------------------
// DMA(Num) Enable/disable
// if Channel is UINT32_MAX, enable while periphery without channels, otherwise ena/disa selected channel
// result is true:successfull otherwise false
bool _Chip_DMA_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
	bool res;
	
	if(pPeri == NULL ) return(false);

	res = _Chip_DMA_Enable_CHIRQ(pPeri, ChannelBitMask, NewState);					// ena/disa specified channels
	
	if(ChannelBitMask == UINT32_MAX)												// works with all channels?
	{
		if(NewState)
		{
			NVIC_ClearPendingIRQ(DMA_IRQn);											// clear pending interrupt flag
		}
		else
		{
			NVIC_DisableIRQ((IRQn_Type) (DMA_IRQn));								// Enable DMA interrupt
		}
	}
	return(res);																	// return result
}

// ------------------------------------------------------------------------------------------------------
// DMA[x](Channel) Configure
// Channel 0-7
// CHAL_DMA_Xfer_t - structure with xfer parameters
// result is true:successfull otherwise false
uint32_t DMA_CH_XFERLEN[_CHIP_DMA_CHANNELS_COUNT];

bool _Chip_DMA_Configure (void *pPeri, uint8_t Channel, _Chip_DMA_Xfer_t *Xfer )
{
	if (Channel > 7) return(false);													// LPC17xx have only channels 0-7
	if(pPeri == NULL ) return(false);
	
	((_CHIP_DMA_T*)pPeri)->INTTCCLEAR |= 1<< Channel;								// clear interrupt status reg
	((_CHIP_DMA_T*)pPeri)->INTERRCLR |= 1<< Channel;								// clear error statusreg
	
	((_CHIP_DMA_T*)pPeri)->CH[Channel].SRCADDR = Xfer->SrcAdr;						// source address
	((_CHIP_DMA_T*)pPeri)->CH[Channel].DESTADDR = Xfer->DstAdr;						// destination address
	((_CHIP_DMA_T*)pPeri)->CH[Channel].LLI = Xfer->XferNextDescriptor;				// address of next linked descriptor
	((_CHIP_DMA_T*)pPeri)->CH[Channel].CONFIG = 0;									// clear and disable this DMA Channel
	((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL = 0;									// firstly, clear reg.
		
	
	// CONTROL FILL:
	switch(Xfer->SrcBSize)															// set Source Burst Size
	{
		case 256:((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (0x07 << 12); break;	// Burst size:256 change to 7 and shift to bit 12-14
		case 128:((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (0x06 << 12); break;	// Burst size:128 change to 6 and shift to bit 12-14
		case 64:((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (0x05 << 12); break;	// Burst size:64 change to 5 and shift to bit 12-14
		case 32:((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (0x04 << 12); break;	// Burst size:32 change to 4 and shift to bit 12-14
		case 16:((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (0x03 << 12); break;	// Burst size:16 change to 3 and shift to bit 12-14
		case 8:	((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (0x02 << 12); break;	// Burst size:8 change to 2 and shift to bit 12-14
		case 4:	((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (0x01 << 12); break;	// Burst size:4 change to 1 and shift to bit 12-14
		case 1:	((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (0x00 << 12); break;	// Burst size:1 change to 0 and shift to bit 12-14
		default: return(false);														// otherwise FAIL
	}
	
	switch(Xfer->DstBSize)															// set Detination Burst Size
	{
		case 256:((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (0x07 << 15); break;	// Burst size:256 change to 7 and shift to bit 15-17
		case 128:((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (0x06 << 15); break;	// Burst size:128 change to 6 and shift to bit 15-17
		case 64:((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (0x05 << 15); break;	// Burst size:64 change to 5 and shift to bit 15-17
		case 32:((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (0x04 << 15); break;	// Burst size:32 change to 4 and shift to bit 15-17
		case 16:((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (0x03 << 15); break;	// Burst size:16 change to 3 and shift to bit 15-17
		case 8:	((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (0x02 << 15); break;	// Burst size:8 change to 2 and shift to bit 15-17
		case 4:	((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (0x01 << 15); break;	// Burst size:4 change to 1 and shift to bit 15-17
		case 1:	((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (0x00 << 15); break;	// Burst size:1 change to 0 and shift to bit 15-17
		default: return(false);														// otherwise FAIL
	}
	
	((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= ((Xfer->SrcWSize/8) >> 1) << 18;	// set Source transfer width
	((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= ((Xfer->DstWSize/8) >> 1) << 21;	// set Destination transfer width
	
	if(Xfer->SrcIncr) ((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (1 << 26);		// Source increment?
	if(Xfer->DstIncr) ((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (1 << 27);		// Destination increment?
	
	if(Xfer->XferIRQ) ((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= (1UL << 31);	// Transfer count interrupt?
	
	// CONFIG FILL:
	if(Xfer->SrcPeri > 15) return(false);
	else ((_CHIP_DMA_T*)pPeri)->CH[Channel].CONFIG |= (Xfer->SrcPeri << 1);			// Source periphery
	
	if(Xfer->DstPeri > 15) return(false);
	else ((_CHIP_DMA_T*)pPeri)->CH[Channel].CONFIG |= (Xfer->DstPeri << 6);			// Destination periphery
	
	if(Xfer->XferType > 7) return(false);
	else ((_CHIP_DMA_T*)pPeri)->CH[Channel].CONFIG |= (Xfer->XferType << 11);		// Transfer type
	
	if(Xfer->XferIRQ) ((_CHIP_DMA_T*)pPeri)->CH[Channel].CONFIG |= (1 << 15);		// Terminal count interrupt mask
	
	
	// Lenght must be writen at least !!!!!
	DMA_CH_XFERLEN[Channel] = Xfer->XferLen & 0xfff;
	((_CHIP_DMA_T*)pPeri)->CH[Channel].CONTROL |= DMA_CH_XFERLEN[Channel];			// set atransfer size

	return (true);
}


// ************************************************************************************************
// I2C Functions 
// ************************************************************************************************
// ------------------------------------------------------------------------------------------------------
// I2C(Num) Initialization LPC17xx - 0/1/2
// result is true:successfull otherwise false
void *_Chip_I2C_Init(int32_t PeriIndex)
{
	switch(PeriIndex)
	{
		case 2: 
		{
			LPC_SYSCON->PCONP |= (1 << 26);											// enable GPIO in PCONP
			LPC_SYSCON->RSTCON[0] |= (1 << 26);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 26);									// dassert reset
            if(LPC_I2C2->STAT & 0x0001) return(NULL);			                    // I2C not ready? Missing PULLUP?
			NVIC_ClearPendingIRQ(I2C2_IRQn);										// clear pending in			
			return(LPC_I2C2);
		}
		case 1:
		{
			LPC_SYSCON->PCONP |= (1 << 19);											// enable GPIO in PCONP
			LPC_SYSCON->RSTCON[0] |= (1 << 19);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 19);									// dassert reset
            if(LPC_I2C1->STAT & 0x0001) return(NULL);			                    // I2C not ready? Missing PULLUP?
			NVIC_ClearPendingIRQ(I2C1_IRQn);										// clear pending in			
			return(LPC_I2C1);			
		}
		case 0:
		{
			LPC_SYSCON->PCONP |= (1 << 7);											// enable GPIO in PCONP
			LPC_SYSCON->RSTCON[0] |= (1 << 7);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 7);										// dassert reset
            if(LPC_I2C0->STAT & 0x0001) return(NULL);			                    // I2C not ready? Missing PULLUP?            
			NVIC_ClearPendingIRQ(I2C0_IRQn);										// clear pending in			
			return(LPC_I2C0);			
		}
		default: return(NULL);
	}
}


// ------------------------------------------------------------------------------------------------------
// I2C Enable/disable
// result is true:successfull otherwise false
bool _Chip_I2C_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	
	if(NewState)
	{
		((_CHIP_I2C_T*) pPeri)->CONSET |= (1 << 6);									// set I2EN
	}
	else
	{
		((_CHIP_I2C_T*) pPeri)->CONCLR |= (1 << 6);									// erase I2EN
	}
	return(true);
}



// ------------------------------------------------------------------------------------------------------
// I2C Configure I2C
// result is true:successfull otherwise false
bool _Chip_I2C_Configure(void *pPeri, uint32_t Speed_kHz, bool MasterMode, bool SlaveMode, bool MonitorMode)
{
	bool res = false;
	
	if(pPeri == NULL) return(res);
	
//	UM10470 - 22.8.11 Selecting the appropriate I2C data rate and duty cycle
//		I2Cbitfrequency = PCLKI2C / (I2CSCLH + I2CSCLL)

	if (MasterMode == true)
	{
		res = _Chip_I2C_Set_BusSpeed(pPeri, Speed_kHz);
		((_CHIP_I2C_T*)pPeri)->CONCLR = 0x000000ff;
	}
	return(res);
}


// ------------------------------------------------------------------------------------------------
// I2C Interrupt - enable/disable
bool _Chip_I2C_IRQ_Enable(void *pPeri, bool NewState)
{
	if(NewState == true)
	{
		switch(_Chip_I2C_Get_PeriIndex(pPeri)) 
		{
			case 2:
			{
				NVIC_ClearPendingIRQ ( (IRQn_Type) (I2C2_IRQn));						// Clear I2C2 interrupt flag
				NVIC_EnableIRQ( (IRQn_Type) (I2C2_IRQn));								// Enable I2C2 interrupt
				break;
			}
			case 1:
			{
				NVIC_ClearPendingIRQ ( (IRQn_Type) (I2C1_IRQn));						// Clear I2C1 interrupt flag
				NVIC_EnableIRQ( (IRQn_Type) (I2C1_IRQn));								// Enable I2C1 interrupt
				break;
			}
			default:
			{
				NVIC_ClearPendingIRQ ( (IRQn_Type) (I2C0_IRQn));						// Clear I2C0 interrupt flag
				NVIC_EnableIRQ( (IRQn_Type) (I2C0_IRQn));								// Enable I2C0 interrupt
				break;
			}
		}
	}
	else
	{
		switch(_Chip_I2C_Get_PeriIndex(pPeri)) 
		{
			case 2:
			{
				NVIC_DisableIRQ( (IRQn_Type) (I2C2_IRQn));								// Enable I2C2 interrupt
				break;
			}
			case 1:
			{
				NVIC_DisableIRQ( (IRQn_Type) (I2C1_IRQn));								// Enable I2C1 interrupt
				break;
			}
			default:
			{
				NVIC_DisableIRQ( (IRQn_Type) (I2C0_IRQn));								// Enable I2C0 interrupt
				break;
			}
		}	}
	return(true);
}






// ************************************************************************************************
// SPI Functions 
// ************************************************************************************************
// ------------------------------------------------------------------------------------------------------
// SSP/SPI(Num) Initialization
// result is true:successfull otherwise false
void *_Chip_SPI_Init(int32_t PeriIndex)
{
	switch(PeriIndex)
	{
		case 2: 
		{
			LPC_SYSCON->PCONP |= (1 << 20);											// enable GPIO in PCONP
			LPC_SYSCON->RSTCON[0] |= (1 << 20);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 20);									// dassert reset
			NVIC_ClearPendingIRQ(SSP2_IRQn);										// clear pending in			
			return(LPC_SSP2);
		}
		case 1:
		{
			LPC_SYSCON->PCONP |= (1 << 10);											// enable GPIO in PCONP
			LPC_SYSCON->RSTCON[0] |= (1 << 10);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 10);									// dassert reset
			NVIC_ClearPendingIRQ(SSP1_IRQn);										// clear pending in			
			return(LPC_SSP1);			
		}
		case 0:
		{
			LPC_SYSCON->PCONP |= (1 << 21);											// enable GPIO in PCONP
			LPC_SYSCON->RSTCON[0] |= (1 << 21);										// assert reset
			LPC_SYSCON->RSTCON[0] &= ~(1 << 21);									// dassert reset
			NVIC_ClearPendingIRQ(SSP0_IRQn);										// clear pending in			
			return(LPC_SSP0);			
		}
		default: return(NULL);
	}
}


// ------------------------------------------------------------------------------------------------------
// SPI Enable/disable
// result is true:successfull otherwise false
bool _Chip_SPI_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	
	if(NewState)
	{
		((_CHIP_SPI_T*) pPeri)->CR1 |= (1 << 1);									// SSP enable 
	}
	else
	{
		((_CHIP_SPI_T*) pPeri)->CR1 &= ~(1 << 1);									// disable all functions
	}
	return(true);
}


// ------------------------------------------------------------------------------------------------------
// SPI Configure
// result is true:successfull otherwise false
bool _Chip_SPI_Configure(void *pPeri, uint32_t Speed_kHz, bool MasterMode, bool SlaveMode, bool LSB_First)
{
	uint32_t CLKDiv;
	uint32_t PCLK = _Chip_Clock_Get_pclk_Rate();								// LPC17xx clocked by system clock (PCLK)

	
	if(pPeri == NULL) return(false);
	if((MasterMode == true) && (SlaveMode == true)) return(false);					// only one mode set check....
	if((MasterMode == false) && (SlaveMode == false)) return(false);
	
	if (MasterMode == true)
	{
		CLKDiv = (PCLK / 1000) / Speed_kHz;
		if(CLKDiv > 0x10000) {sys_Err("Cannot set SPI Divider!"); return(false);} 
		
		Chip_SPI_SetClockDiv((_CHIP_SPI_T*)pPeri, CLKDiv);							// set new peripherial divider
		((_CHIP_SPI_T*)pPeri)->CR1 &= ~(1 << 2);									// set MASTER	
	}
	else
	{
		((_CHIP_SPI_T*)pPeri)->CR1 |= (1 << 2);										// clear MASTER, set Slave
	}
		
	
	((_CHIP_SPI_T*)pPeri)->CR0 &= ~(3 << 4);										// Set SPI mode
	
	return(true);
}

 

// ************************************************************************************************
// USB Functions 
// ************************************************************************************************
// ------------------------------------------------------------------------------------------------------
// USB(Num) Initialization
// result is true:successfull otherwise false
void *_Chip_USB_Init(int32_t PeriIndex)
{
	if(PeriIndex != 0) return( NULL );												// LPC17xx have only oone USB

#if 1			// init z lpcUSBLib:

	//Chip_SYSCTL_PeriphReset(SYSCTL_RESET_USB);									// periphery reset
	LPC_SYSCON->PCONP |= (uint32_t)(1UL << 31);										// enable GPIO in PCONP
	LPC_SYSCON->RSTCON[0] |= (uint32_t)(1UL << 31);									// assert reset
	LPC_SYSCON->RSTCON[0] &= (uint32_t)~(1UL << 31);								// dassert reset

	
	
	
//	/* Enable PLL1 for 48MHz output */
//		Chip_Clock_EnablePLL(SYSCTL_USB_PLL, SYSCTL_PLL_ENABLE);
//	#if defined(__LPC175X_6X__)
//		while ((Chip_Clock_GetPLLStatus(SYSCTL_USB_PLL) & SYSCTL_PLL1STS_LOCKED) == 0);
//	#else
//		while ((Chip_Clock_GetPLLStatus(SYSCTL_USB_PLL) & SYSCTL_PLLSTS_LOCKED) == 0);
//	#endif


//#if defined(USB_CAN_BE_DEVICE)			
//	LPC_USB->USBClkCtrl = 0x12;														// Dev, PortSel, AHB clock enable
//	while ((LPC_USB->USBClkSt & 0x12) != 0x12) ;
//#endif

#endif	

	return(LPC_USB);
}	

// ------------------------------------------------------------------------------------------------------
// USB Enable/disable
// result is true:successfull otherwise false
bool _Chip_USB_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	
	if(NewState)
	{
		NVIC_ClearPendingIRQ(USB_IRQn);												// clear pending interrupt flag
		NVIC_EnableIRQ((IRQn_Type) (USB_IRQn));										// Enable USB interrupt
	}
	else
	{
		NVIC_DisableIRQ((IRQn_Type) (USB_IRQn));									// Disable USB interrupt
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Configure USB
// result is true:successfull otherwise false
bool _Chip_USB_Configure(void *pPeri, bool DeviceMode, bool HostMode, bool OTGMode)
{
	bool res = false;
	
	if(pPeri == NULL) return(res);
	

	if (DeviceMode == true)
	{
		LPC_SYSCTL->PLL[1].PLLCON   = 0x00;             							// PLL1 disable
		LPC_SYSCTL->USBCLKSEL = BSP_USBDIV_VAL-1;									// usb_clk - clock pre USB
		LPC_SYSCTL->USBCLKSEL |= (0x02 << 8);										// usb_clk - src pre clock pre USB. ideme z PLL1
		//	USBCLKSEL sa smie pripnut na PLL len ak je PLL zastaveny !!!!
				
		// nastav CLOCK USB do periferie musi ist 48MHz - ideme cez PLL1
		// Nastav teda PLL1:
		LPC_SYSCTL->PLL[1].PLLCFG   = (BSP_FREQ_PLL1_M_VAL -1) | ((BSP_FREQ_PLL1_P_VAL -1) << 5);	// nastav M a P pomer pre PLL1 - ideme na alt_pll_clk = 48MHz
		LPC_SYSCTL->PLL[1].PLLCON   = 0x01;             							// PLL1 Enable
		LPC_SYSCTL->PLL[1].PLLFEED  = 0xAA;
		LPC_SYSCTL->PLL[1].PLLFEED  = 0x55;
		while (!(LPC_SYSCTL->PLL[1].PLLSTAT & (1UL << 10)));						// Wait for PLOCK1

		LPC_USB->USBClkCtrl |= 0x12;													// Device clock enable
		while (!(LPC_USB->USBClkSt & 0x12)) ;
		return(true);
	}
	
	return(false);
}

// ------------------------------------------------------------------------------------------------------
// Read USB interrupt status
uint32_t _Chip_USB_Get_IRQStatus(void *pPeri)
{
	return(LPC_SYSCTL->USBIntSt);
}




// ************************************************************************************************
// ETH Functions 
// ************************************************************************************************
// ------------------------------------------------------------------------------------------------------
// ETH(Num) Initialization
// result is true:successfull otherwise false
void *_Chip_ETH_Init(int32_t PeriIndex)
{
	if(PeriIndex != 0) return( NULL );												// LPC17xx have only one ETH

#if 1			
	// this is a special Power-On sequence!
	LPC_SYSCON->PCONP |= (uint32_t)(1UL << 30);										// enable ETH in PCONP
	
	// if RX_CLK is without clock signal from PHY, MCU will freeze !!!!!!!!!!!!
	
	LPC_SYSCON->RSTCON[0] |=  (1 << 30);											// assert reset
	LPC_SYSCON->RSTCON[0] &= ~(1 << 30);											// dassert reset

#endif	

	return(LPC_ETHERNET);
}	

// ------------------------------------------------------------------------------------------------------
// ETH Enable/disable
// result is true:successfull otherwise false
bool _Chip_ETH_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	
	if(NewState)
	{
		NVIC_ClearPendingIRQ(ETHERNET_IRQn);										// clear pending interrupt flag
		NVIC_EnableIRQ((IRQn_Type) (ETHERNET_IRQn));								// Enable ETH interrupt
	}
	else
	{
		NVIC_DisableIRQ((IRQn_Type) (ETHERNET_IRQn));								// Disable ETH interrupt
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Configure ETH
// result is true:successfull otherwise false
//bool _Chip_ETH_Configure(void *pPeri, bool DeviceMode, bool HostMode, bool OTGMode)
//{
//	bool res = false;
//	
//	if(pPeri == NULL) return(res);
//	

////	if (DeviceMode == true)
////	{
////		LPC_SYSCTL->PLL[1].PLLCON   = 0x00;             							// PLL1 disable
////		LPC_SYSCTL->USBCLKSEL = BSP_USBDIV_VAL-1;									// usb_clk - clock pre USB
////		LPC_SYSCTL->USBCLKSEL |= (0x02 << 8);										// usb_clk - src pre clock pre USB. ideme z PLL1
////		//	USBCLKSEL sa smie pripnut na PLL len ak je PLL zastaveny !!!!
////				
////		// nastav CLOCK USB do periferie musi ist 48MHz - ideme cez PLL1
////		// Nastav teda PLL1:
////		LPC_SYSCTL->PLL[1].PLLCFG   = (BSP_FREQ_PLL1_M_VAL -1) | ((BSP_FREQ_PLL1_P_VAL -1) << 5);	// nastav M a P pomer pre PLL1 - ideme na alt_pll_clk = 48MHz
////		LPC_SYSCTL->PLL[1].PLLCON   = 0x01;             							// PLL1 Enable
////		LPC_SYSCTL->PLL[1].PLLFEED  = 0xAA;
////		LPC_SYSCTL->PLL[1].PLLFEED  = 0x55;
////		while (!(LPC_SYSCTL->PLL[1].PLLSTAT & (1UL << 10)));						// Wait for PLOCK1

////		LPC_USB->USBClkCtrl |= 0x12;													// Device clock enable
////		while (!(LPC_USB->USBClkSt & 0x12)) ;
////		return(true);
////	}
//	
//	return(false);
//}

// ------------------------------------------------------------------------------------------------------
// Read USB interrupt status
//uint32_t _Chip_ETH_GET_IRQStatus(void *pPeri)
//{
//	return(0);
//}

#endif
