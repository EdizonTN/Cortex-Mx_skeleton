// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: _chip_lpc546xx.c
// 	   Version: 3.22
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: MCU LPC546xx Chip description/configuration file. Same for all MCUs of this family!
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:	
//				2024.09.26	- v3.22 - rename _CHIP_xxx_Handler to _Chip_xxx_Handler
//				2023.11.20	- v3.21 - rename handler to sys_xxx_handler
//				2023.10.04	- v3.2 - move all extern inline into this .c file. Clean all code
//				2020.09.23	- Check peri pointer in manipulations
//				2020.08.07	- Error in _Chip_I2C_Set_BusSpeed corrected. scl changed t uint32_t
//				2020.08.04	- I2C access divided to master, slave, monitor and timeouts
//				2020.07.22	- MRT Config - if cannot send time, set closest possible time.
//				2020.07.08	- rename Pld: _Chip_UART_IRQRx_Enable ---> _Chip_UART_IRQ_Peri_Enable
//							- rename Pld: _Chip_UART_IRQTx_Enable ---> _Chip_UART_IRQ_Peri_Enable, add parameter
//				2020.07.01	- Calling of Ena/Disa NVIC IRQ rewriten
//							- rename Old: _Chip_xxx_IRQ_Enable  -----> _Chip_xxx_Enable_IRQ_NVIC
//							- rename Old: _Chip_xxx_IRQMask_Enable --> _Chip_xxx_IRQ_Peri_Enable
//							- rename Old: _Chip_xxx_IRQ_Enable_CHIRQ > _Chip_xxx_IRQ_Peri_Enable  also parameters renamed
//							- rename Old: _Chip_SPI_SetClockDiv -----> _Chip_SPI_Set_ClockDiv

#include "Skeleton.h"


#if	defined( CONF_CHIP_ID_LPC54605J256ET180 ) || \
	defined( CONF_CHIP_ID_LPC54605J512ET180 ) || \
	defined( CONF_CHIP_ID_LPC54605J256BD100 ) || \
	defined( CONF_CHIP_ID_LPC54605J512BD100 ) || \
	defined( CONF_CHIP_ID_LPC54605J256ET100 ) || \
	defined( CONF_CHIP_ID_LPC54605J512ET100 ) || \
	defined( CONF_CHIP_ID_LPC54606J256ET100 ) || \
	defined( CONF_CHIP_ID_LPC54606J512ET100 ) || \
	defined( CONF_CHIP_ID_LPC54606J256BD100 ) || \
	defined( CONF_CHIP_ID_LPC54606J512BD100 ) || \
	defined( CONF_CHIP_ID_LPC54606J256ET180 ) || \
	defined( CONF_CHIP_ID_LPC54606J512BD208 ) || \
	defined( CONF_CHIP_ID_LPC54607J256ET180 ) || \
	defined( CONF_CHIP_ID_LPC54607J512ET180 ) || \
	defined( CONF_CHIP_ID_LPC54607J256BD208 ) || \
	defined( CONF_CHIP_ID_LPC54608J512ET180 ) || \
	defined( CONF_CHIP_ID_LPC54608J512BD208 ) || \
	defined( CONF_CHIP_ID_LPC54616J512ET100 ) || \
	defined( CONF_CHIP_ID_LPC54616J512BD100 ) || \
	defined( CONF_CHIP_ID_LPC54616J256ET180 ) || \
	defined( CONF_CHIP_ID_LPC54616J512BD208 ) || \
	defined( CONF_CHIP_ID_LPC54618J512ET180 ) || \
	defined( CONF_CHIP_ID_LPC54618J512BD208 ) || \
	defined( CONF_CHIP_ID_LPC54628J512ET180 )



#if defined(COMP_TYPE_XPR)		// LPCXPRESSO/MCUXpresso
	#include	"XPR\crp.c"
#endif

ATTR_WEAK extern void 	__valid_user_code_checksum(void);

#if defined (__REDLIB__)
extern void __main(void);
#endif
extern int main(void);


void _Chip_Default_IRQ_Handler(void) 
{
	dbgprint("\r\n.Unhandlered exception! Vector: %d.\r\n", SCB->ICSR & 0xff);
}


void sys_Reset_Handler								(void) __WEAK_("_Chip_Default_IRQ_Handler");
void sys_HardFault_Handler							(void) __WEAK_("_Chip_Default_IRQ_Handler");

void _Chip_WDT0_BOD0_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_DMA0_IRQ_Handler						(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_GINT0_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_GINT1_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_PINT0_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_PINT1_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_PINT2_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_PINT3_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_UTICK0_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_MRT0_IRQ_Handler						(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_TIMER0_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_TIMER1_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_SCT0_IRQ_Handler						(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_TIMER3_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_FLEXCOMM0_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_FLEXCOMM1_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_FLEXCOMM2_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_FLEXCOMM3_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_FLEXCOMM4_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_FLEXCOMM5_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_FLEXCOMM6_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_FLEXCOMM7_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_ADC0_SEQA_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_ADC0_SEQB_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_ADC0_THCMP_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_DMIC0_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_HVAD0_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_USB0_NEEDCLK_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_USB0_IRQ_Handler						(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_RTC0_IRQ_Handler						(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_PINT4_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_PINT5_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_PINT6_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_PINT7_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_TIMER2_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_TIMER4_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_RIT0_IRQ_Handler						(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_SPIFI0_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_FLEXCOMM8_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_FLEXCOMM9_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_SDIO0_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_CAN00_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_CAN01_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_CAN10_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_CAN11_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_USB1_IRQ_Handler						(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_USB1_NEEDCLK_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_ETH0_IRQ_Handler						(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_ETH0_PMT_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_ETH0_MACLP_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_EEPROM0_IRQ_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_LCD0_IRQ_Handler						(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_SHA0_IRQ_Handler						(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_SMARDCARD0_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_SMARDCARD1_IRQ_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");

extern const uint32_t *StatckInit;

// define Cortex-M base interrupt vectors 
const cortexm_base_t cortex_vector_base __SECTION_IRQVECTORS  = 
{
	CONF_STACK_START + CONF_STACK_SIZE,
    {
        [ 0] = sys_Reset_Handler,														// entry point of the program 
        [ 1] = sys_NMI_Handler,															// [-14] non maskable interrupt handler
        [ 2] = sys_HardFault_Handler,													// [-13] hard fault exception
        [ 3] = sys_MemManage_Handler,													// [-12] memory manage exception
        [ 4] = sys_BusFault_Handler,													// [-11] bus fault exception
        [ 5] = sys_UsageFault_Handler,													// [-10] usage fault exception
        [ 6] = (isr_t)0x00000000, 													// LPC MCU Checksum - checksum.exe will write it here !!!!
		[ 7] = (isr_t)0xFFFFFFFF,													// ECRP
		[ 8] = (isr_t)0,															// reserved
		[ 9] = (isr_t)0,															// reserved
        [10] = sys_SVC_Handler,															// [-5] SW interrupt, use it, for triggering context switches
        [11] = sys_DebugMon_Handler,													// [-4] debug monitor exception
		[12] = (isr_t)0,															// reserved
        [13] = sys_PendSV_Handler,														// [-2] pendSV interrupt, use it, to do the actual context switch
        [14] = sys_SysTick_Handler,														// [-1] SysTick interrupt - Used for system counters and alarms
    },	
	{
		[ 0] = IRQHANDLER_16,
		[ 1] = IRQHANDLER_17,
		[ 2] = IRQHANDLER_18,
		[ 3] = IRQHANDLER_19,
		[ 4] = IRQHANDLER_20,
		[ 5] = IRQHANDLER_21,
		[ 6] = IRQHANDLER_22,
		[ 7] = IRQHANDLER_23,
		[ 8] = IRQHANDLER_24,
		[ 9] = IRQHANDLER_25,
		[10] = IRQHANDLER_26,
		[11] = IRQHANDLER_27,
		[12] = IRQHANDLER_28,
		[13] = IRQHANDLER_29,
		[14] = IRQHANDLER_30,
		[15] = IRQHANDLER_31,
		[16] = IRQHANDLER_32,
		[17] = IRQHANDLER_33,
		[18] = IRQHANDLER_34,
		[19] = IRQHANDLER_35,
		[20] = IRQHANDLER_36,
		[21] = IRQHANDLER_37,
		[22] = IRQHANDLER_38,
		[23] = IRQHANDLER_39,
		[24] = IRQHANDLER_40,
		[25] = IRQHANDLER_41,
		[26] = IRQHANDLER_42,
		[27] = IRQHANDLER_43,
		[28] = IRQHANDLER_44,
		[29] = IRQHANDLER_45,
		[30] = IRQHANDLER_46,
		[31] = IRQHANDLER_47,
		[32] = IRQHANDLER_48,
		[33] = IRQHANDLER_49,
		[34] = IRQHANDLER_50,
		[35] = IRQHANDLER_51,
		[36] = IRQHANDLER_52,
		[37] = IRQHANDLER_53,
		[38] = IRQHANDLER_54,
		[39] = IRQHANDLER_55,
		[40] = IRQHANDLER_56,
		[41] = IRQHANDLER_57,
		[42] = IRQHANDLER_58,
		[43] = IRQHANDLER_59,
		[44] = IRQHANDLER_60,
		[45] = IRQHANDLER_61,
		[46] = IRQHANDLER_62,
		[47] = IRQHANDLER_63,
		[48] = IRQHANDLER_64,
		[49] = IRQHANDLER_65,
		[50] = IRQHANDLER_66,
		[51] = IRQHANDLER_67,
		[52] = IRQHANDLER_68,
		[53] = IRQHANDLER_69,
		[54] = IRQHANDLER_70,
		[55] = IRQHANDLER_71,
		[56] = IRQHANDLER_72
	}
};


// ******************************************************************************************************
// PUBLIC Functions
// ******************************************************************************************************
void _Chip_CoreClockUpdate(void);


// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************




// ------------------------------------------------------------------------------------------------
// CHIP functions
// ------------------------------------------------------------------------------------------------

typedef void (*IAP)(unsigned int [],unsigned int[]);

// ------------------------------------------------------------------------------------------------
// init ITM Cortex Engine
void _Chip_SWO_Init(uint32_t Main_CPU_Clock)
{
// DEBUG INIT:
#if (CONF_DEBUG_ITM == 1) && (__CORTEX_M >= 3)										// debug with ITM support
	ITM_RxBuffer = ITM_RXBUFFER_EMPTY;  											//  CMSIS Debug Input

	SYSCON->ARMTRACECLKDIV = 1;														// ARM trace clock divider register set to 1 - run it.

	//CoreDebug->DEMCR |= 1 ;//CoreDebug_DEMCR_TRCENA_Msk;									// Enable Trace - Debug Exception and Monitor Control Registe

	TPI->SPPR = kSWO_ProtocolNrz;													// select SWO encoding protocol - 1: SWO Manchester protocol,  2: SWO UART/NRZ protocol
	TPI->ACPR = (Main_CPU_Clock / 2000000)-1;										// TPIU - Set the baud rate to 10MHz
	TPI->FFCR = 0x100;																// Configure FIFO
	
	ITM->LAR = 0xC5ACCE55;															// Enable write to ITM registers
	
	if(~(ITM->LSR & 0x02))															// Accesss unlocked?
	{
		ITM->TPR = 0;																// allow unprivilege access
		ITM->TCR = ITM_TCR_ITMENA_Msk | ITM_TCR_SYNCENA_Msk| ITM_TCR_TraceBusID_Msk | ITM_TCR_DWTENA_Msk
#if defined(BSP_DEBUG_SWO) && (BSP_DEBUG_SWO == 1)
		| ITM_TCR_SWOENA_Msk;
		ITM->TER = 1U << 0;															// ITM Port #0 enable for SWO
#else
		;
		ITM->TER = 0;																// ITM Port #0 disable for SWO
#endif		
		
	}
#endif //CONF_DEBUG_ITM
	
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// Call System Init - in reset vector!
inline void _Chip_SystemInit(void)															// inserted into SystemInit
{
	// FIRSTLY! Enable RAM banks that may be off by default at reset
	SYSCON->AHBCLKCTRLSET[0] |= SYSCON_AHBCLKCTRL_SRAM1_MASK | SYSCON_AHBCLKCTRL_SRAM2_MASK | SYSCON_AHBCLKCTRL_SRAM3_MASK;
	
#if ((__FPU_PRESENT == 1) && (__FPU_USED == 1))
	SCB->CPACR |= ((3UL << 10*2) | (3UL << 11*2));    								// set CP10, CP11 Full Access
#endif // ((__FPU_PRESENT == 1) && (__FPU_USED == 1))

}


// ------------------------------------------------------------------------------------------------
// get reset signal sources
void _Chip_Read_ResetSource(uint32_t *dst)											// Get reset sources (Chip_RST_SRC)
{
    *dst = SYSCON->SYSRSTSTAT;														// format as CHAL_RST_SRC_t
}





// ******************************************************************************************************
// IAP functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------
// Prepare sector for write operation - This command must be executed before executing "Copy RAM to flash" or "Erase Sector(s)" command. 
//					Successful execution of the "Copy RAM to flash" or "Erase Sector(s)" command causes relevant sectors to be protected again. 
//					The boot sector can not be prepared by this command. To prepare a single sector use the same "Start" and "End" sector numbers.
//return 	result[0] : Status Code
bool _Chip_IAP_PrepareSector(uint32_t strSector, uint32_t endSector)
{
	uint32_t cpuSR = __get_PRIMASK();
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_PREWRRITE_CMD;
	Command[1] = strSector;
	Command[2] = endSector;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__set_PRIMASK(cpuSR);
	
	if(Result[0] == 0) return(true);
	return (false);	
}

// ------------------------------------------------------------------------------------------------
// Copy RAM to flash - This command is used to program the flash memory. 
//					The affected sectors should be prepared first by calling "Prepare Sector for Write Operation" command. 
//					The affected sectors are automatically protected again once the copy command is successfully executed. 
//					The boot sector can not be written by this command. Also see Section 34.4.3 for the number of bytes that can be written.
//					Param3 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
//					Remark: All user code must be written in such a way that no master accesses the flash while this command is executed and the flash is programmed.
//return 	result[0] : Status Code
bool _Chip_IAP_CopyRamToFlash(uint32_t DstAdd, uint32_t *pSrc, uint32_t byteswrt)
{
	uint32_t cpuSR = __get_PRIMASK();
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_WRISECTOR_CMD;
	Command[1] = DstAdd;
	Command[2] = (uint32_t) pSrc;
	Command[3] = byteswrt;															// Should be 256 | 512 | 1024 | 4096.
	Command[4] = SystemCoreClock / 1000;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__set_PRIMASK(cpuSR);
	
	if(Result[0] == 0) return(true);
	return (false);	
}

// ------------------------------------------------------------------------------------------------
// Erase Page - This command is used to erase a page or multiple pages of on-chip flash memory. 
//				To erase a single page use the same "Start" and "End" page numbers.
//				Param2 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
//				Remark: All user code must be written in such a way that no master accesses the flash while this command is executed and the flash is erased.
//return 	result[0] : Status Code
bool _Chip_IAP_ErasePage(uint32_t strPage, uint32_t endPage)
{
	uint32_t cpuSR = __get_PRIMASK();
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_ERASE_PAGE_CMD;
	Command[1] = strPage;
	Command[2] = endPage;
	Command[3] = SystemCoreClock / 1000;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__set_PRIMASK(cpuSR);
	
	if(Result[0] == 0) return(true);
	return (false);
}

// ------------------------------------------------------------------------------------------------
// Blank check sector - This command is used to blank check a sector or multiple sectors of on-chip flash memory. 
//					To blank check a single sector use the same "Start" and "End" sector numbers.
// Result is True if sector/s is blank. Otherwise false
bool _Chip_IAP_BlankCheckSector(uint32_t strSector, uint32_t endSector)
{
	uint32_t cpuSR = __get_PRIMASK();
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_BLANK_CHECK_SECTOR_CMD;
	Command[1] = strSector;
	Command[2] = endSector;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__set_PRIMASK(cpuSR);
	
	if(Result[0] == 0) return(true);
	return (false);
}

// ------------------------------------------------------------------------------------------------
// Read_MCUChipID
// Read MCU ID 
// Readed ID is saved into memory at address dst
// Result is true for Success otherwise false
bool _Chip_IAP_Read_ID(uint32_t *pDst)
{
	uint32_t cpuSR = __get_PRIMASK();
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_REPID_CMD;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__set_PRIMASK(cpuSR);

	if(Result[0] == 0)
	{
		*pDst = Result[1];															// copy readed value to Dst
		return(true);
	}
	else
	{
		*pDst = NULL;
		return (false);
	}
}

// ------------------------------------------------------------------------------------------------
// Read_MCUSerialNum
// Read MCU serial Number
// Readed SerialNumber is saved into memory at address dst
// Result is true for Success otherwise false
bool _Chip_IAP_Read_SerialNum(uint32_t *pDst)
{
	uint32_t cpuSR = __get_PRIMASK();
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_READ_UID_CMD;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__set_PRIMASK(cpuSR);
	
	if(Result[0] == 0)																// Function call SUCCESS?
	{
		*pDst++ = Result[1];														// save readed value
		*pDst++ = Result[2];														// save readed value
		*pDst++ = Result[3];														// save readed value
		*pDst = Result[4];															// save readed value
		return(true);
	}
	else
	{
		*pDst = 0xFFFFFFFF;
		return (false);
	}
}
	
// ------------------------------------------------------------------------------------------------
// Read BootLoader version in MCU
// Result is true for Success otherwise false
bool _Chip_IAP_Read_BootCodeVersion(uint32_t *pDst)
{
	uint32_t cpuSR = __get_PRIMASK();
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_READ_BOOT_CODE_CMD;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__set_PRIMASK(cpuSR);
	
	if(Result[0] == 0)																// Function call SUCCESS?
	{
		*pDst = Result[1];															// save readed value
		return(true);
	}
	else
	{
		*pDst = NULL;
		return (false);
	}
}

// ------------------------------------------------------------------------------------------------
// Write data to EEPROM - Data is copied from the RAM address to the EEPROM address.
//			Param3 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
//			Remark: The top 64 bytes of the EEPROM memory are reserved and cannot be written to.
// return 	result[0] : Status Code
bool _Chip_IAP_Write_EEPROM(uint32_t EEDst, uint32_t RAMSrc, uint32_t byteswrt)
{
	uint32_t cpuSR = __get_PRIMASK();

	uint32_t Command[5], Result[5];
	
	Command[0] = _CHIP_IAP_EEPROM_WRITEPAGE_CMD;
	Command[1] = EEDst;
	Command[2] = RAMSrc;
	Command[3] = byteswrt;															// Write one page only
	Command[4] = SystemCoreClock / 1000;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__set_PRIMASK(cpuSR);

	if(Result[0] == 0) return(true);
	return (false);
}

// ------------------------------------------------------------------------------------------------
// Read data from EEPROM - Data is copied from the EEPROM address to the RAM address.
//			Param3 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
// return 	result[0] : Status Code
bool _Chip_IAP_Read_EEPROM_Page(uint32_t PageNum, uint32_t RAMDst)
{
	uint32_t cpuSR = __get_PRIMASK();
	
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_EEPROM_READPAGE_CMD;
	Command[1] = PageNum;
	Command[2] = RAMDst;
	Command[3] = SystemCoreClock / 1000;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__set_PRIMASK(cpuSR);

	if(Result[0] == 0) return(true);
	return (false);
}

// ------------------------------------------------------------------------------------------------
// STD CLOCK Functions 
// ------------------------------------------------------------------------------------------------
 uint32_t  SystemCoreClock;															// for IAP - System Clock Frequency (Core Clock)
#define	_Chip_Clock_Get_SysClk_Rate		_Chip_Clock_Get_MainClk_Rate

// ------------------------------------------------------------------------------------------------
// "mainclk"
// return clock source for core sysclk
uint32_t _Chip_Clock_Get_MainClk_Rate(void)
{
    uint32_t freq = 0U;

    switch (SYSCON->MAINCLKSELB)
    {
        case 0U:
            if (SYSCON->MAINCLKSELA == 0U)
            {
                freq = _Chip_Clock_Get_Fro12M_Rate();
            }
            else if (SYSCON->MAINCLKSELA == 1U)
            {
                freq = _Chip_Clock_Get_eOsc_Rate();
            }
            else if (SYSCON->MAINCLKSELA == 2U)
            {
                freq = _Chip_Clock_Get_Wdt_Inp_Rate();
            }
            else if (SYSCON->MAINCLKSELA == 3U)
            {
                freq = _Chip_Clock_Get_FroHf_Out_Rate();
            }
            else
            {
                /* Misra Compliance. */
            }
            break;
        case 2U:
            freq = _Chip_Clock_Get_Pll_Out_Rate();
            break;

        case 3U:
            freq = CONF_CHIP_RTCRATEIN;
            break;

        default:
            freq = 0;
            break;
    }

    return freq;
}


// ------------------------------------------------------------------------------------------------
// return clock frequency for watchdog "wdt_clk"
uint32_t _Chip_Clock_Get_Wdt_Inp_Rate(void)
{
#if defined(CONF_SYS_WATCHDOG_USED) && (CONF_SYS_WATCHDOG_USED == 1)	
    uint8_t freq_sel, div_sel;
	
	if ((SYSCON->PDRUNCFG[0] & SYSCON_PDRUNCFG_PDEN_WDT_OSC_MASK) != 0UL)
	{
		return 0U;
	}
	else
	{
		div_sel = (uint8_t)((SYSCON->WDTOSCCTRL & 0x1fUL) + 1UL) << 1U;
		freq_sel = wdtFreqLookup[((SYSCON->WDTOSCCTRL & SYSCON_WDTOSCCTRL_FREQSEL_MASK) >> SYSCON_WDTOSCCTRL_FREQSEL_SHIFT)];
		return ((uint32_t)freq_sel * 50000U) / ((uint32_t)div_sel);
	}
#else
		return(0);	
#endif	
}

// ------------------------------------------------------------------------------------------------
// return clock frequency of High-Freq output of FRO "fro_hf"
uint32_t _Chip_Clock_Get_FroHf_Out_Rate(void)
{
    if (((SYSCON->PDRUNCFG[0] & SYSCON_PDRUNCFG_PDEN_FRO_MASK) != 0UL) ||
        ((SYSCON->FROCTRL & SYSCON_FROCTRL_HSPDCLK_MASK) == 0UL))
    {
        return 0U;
    }

    if ((SYSCON->FROCTRL & SYSCON_FROCTRL_SEL_MASK) != 0UL)
    {
        return 96000000U;
    }
    else
    {
        return 48000000U;
    }
}


// ------------------------------------------------------------------------------------------------
// Return System PLL input clock frequency baed on SYSPLLCLKSEL
uint32_t _Chip_Clock_Get_Pll_In_Rate(void)
{
    uint32_t clkRate = 0U;

    switch ((SYSCON->SYSPLLCLKSEL & SYSCON_SYSPLLCLKSEL_SEL_MASK))
    {
        case 0x00U:
            clkRate = _Chip_Clock_Get_Fro12M_Rate();
            break;

        case 0x01U:
            clkRate = _Chip_Clock_Get_eOsc_Rate();
            break;

        case 0x02U:
            clkRate = _Chip_Clock_Get_Wdt_Inp_Rate();
            break;

        case 0x03U:
            clkRate = CONF_CHIP_RTCRATEIN;
            break;

        default:
            clkRate = 0U;
            break;
    }

    return clkRate;
}

// ------------------------------------------------------------------------------------------------
// return clock frequency of PLL output "pll_clk"
uint32_t _Chip_Clock_Get_Pll_Out_Rate(void)
{
    uint32_t prediv, postdiv, mMult, inPllRate;
    uint64_t workRate;

    inPllRate = _Chip_Clock_Get_Pll_In_Rate();										// Read Input rate to PLL
    // If the PLL is bypassed, PLL would not be used and the output of PLL module would just be the input clock
    if ((SYSCON->SYSPLLCTRL & (SYSCON_SYSPLLCTRL_BYPASS_MASK)) == 0U)
    {
        // PLL is not in bypass mode, get pre-divider, and M divider, post-divider. */
        //
        // 1. Pre-divider
        // Pre-divider is only available when the DIRECTI is disabled.
        if (0U == (SYSCON->SYSPLLCTRL & SYSCON_SYSPLLCTRL_DIRECTI_MASK))
        {
            prediv = _Chip_findPllPreDiv((SYSCON->SYSPLLCTRL & (SYSCON_SYSPLLCTRL_SELP_MASK|SYSCON_SYSPLLCTRL_SELI_MASK|SYSCON_SYSPLLCTRL_SELR_MASK)), SYSCON->SYSPLLNDEC);
        }
        else
        {
            prediv = 1U; 															// The pre-divider is bypassed
        }
        //
        // 2. Post-divider
        // Post-divider is only available when the DIRECTO is disabled.
        if (0U == (SYSCON->SYSPLLCTRL & SYSCON_SYSPLLCTRL_DIRECTO_MASK))
        {
            postdiv = _Chip_findPllPostDiv(SYSCON->SYSPLLCTRL, SYSCON->SYSPLLPDEC);
        }
        else
        {
            postdiv = 1U; 															// The post-divider is bypassed
        }
        inPllRate = inPllRate / prediv;												// Adjust input clock

        // MDEC used for rate
        mMult    = _Chip_findPllMMult(SYSCON->SYSPLLCTRL, SYSCON->SYSPLLMDEC);
        workRate = (uint64_t)inPllRate * (uint64_t)mMult;

        workRate = workRate / ((uint64_t)postdiv);
        workRate = workRate * 2U; 													// SYS PLL hardware cco is divide by 2 before to M-DIVIDER
    }
    else
    {
        workRate = (uint64_t)inPllRate;												// In bypass mode
    }
	return (workRate);
}


// ------------------------------------------------------------------------------------------------
// return clock frequency of USB PLL output "usb_pll_clk"
uint32_t _Chip_Clock_Get_UsbPll_Out_Rate(void)
{
   uint32_t nsel, psel, msel, inPllRate;
    uint64_t workRate;
	inPllRate = _Chip_Clock_Get_eOsc_Rate();
	msel      = (uint8_t)((SYSCON->USBPLLCTRL >> SYSCON_USBPLLCTRL_MSEL_SHIFT) & SYSCON_USBPLLCTRL_MSEL_MASK);
	psel      = (uint8_t)((SYSCON->USBPLLCTRL >> SYSCON_USBPLLCTRL_PSEL_SHIFT) & SYSCON_USBPLLCTRL_PSEL_MASK);
	nsel      = (uint8_t)((SYSCON->USBPLLCTRL >> SYSCON_USBPLLCTRL_NSEL_SHIFT) & SYSCON_USBPLLCTRL_NSEL_MASK);

    if (((SYSCON->USBPLLCTRL >> SYSCON_USBPLLCTRL_FBSEL_SHIFT) & SYSCON_USBPLLCTRL_FBSEL_MASK))
    {
        workRate = (inPllRate) * (msel + 1ULL) / (nsel + 1ULL);						// integer_mode: Fout = M*(Fin/N),  Fcco = 2*P*M*(Fin/N)
    }
    else
    {
        workRate = (inPllRate / (nsel + 1ULL)) * (msel + 1ULL) / (2ULL * (1ULL << (psel & 3ULL)));	// non integer_mode: Fout = M*(Fin/N)/(2*P), Fcco = M * (Fin/N)
    }

    return (uint32_t)workRate;
}

// ------------------------------------------------------------------------------------------------
// return clock frequency of AUDIO PLL input based on AUDPLLCLKSEL
uint32_t _Chip_Clock_Get_AudioPLL_In_Rate(void)
{
    uint32_t clkRate = 0U;

    switch ((SYSCON->AUDPLLCLKSEL & SYSCON_AUDPLLCLKSEL_SEL_MASK))
    {
        case 0x00U:
            clkRate = _Chip_Clock_Get_Fro12M_Rate();
            break;

        case 0x01U:
            clkRate = _Chip_Clock_Get_eOsc_Rate();
            break;

        default:
            clkRate = 0U;
            break;
    }

    return clkRate;
}

// ------------------------------------------------------------------------------------------------
// return clock frequency of AUDIO PLL output "audio_pll_clk"
uint32_t _Chip_Clock_Get_AudioPll_Out_Rate(void)
{
    uint32_t prediv, postdiv, mMult, inPllRate;
    uint64_t workRate;

    inPllRate = _Chip_Clock_Get_AudioPLL_In_Rate();
    if ((SYSCON->AUDPLLCTRL & (1UL << SYSCON_SYSPLLCTRL_BYPASS_SHIFT)) == 0U)
    {
        // PLL is not in bypass mode, get pre-divider, and M divider, post-divider. */
        // 1. Pre-divider
        // Pre-divider is only available when the DIRECTI is disabled.
        if (0U == (SYSCON->AUDPLLCTRL & SYSCON_AUDPLLCTRL_DIRECTI_MASK))
        {
            prediv = _Chip_findPllPreDiv(SYSCON->AUDPLLCTRL, SYSCON->AUDPLLNDEC);
        }
        else
        {
            prediv = 1U; 															// The pre-divider is bypassed
        }
         // 2. Post-divider
         // Post-divider is only available when the DIRECTO is disabled.
        if (0U == (SYSCON->AUDPLLCTRL & SYSCON_AUDPLLCTRL_DIRECTO_MASK))
        {
            postdiv = _Chip_findPllPostDiv(SYSCON->AUDPLLCTRL, SYSCON->AUDPLLPDEC);
        }
        else
        {
            postdiv = 1U; 															// The post-divider is bypassed
        }
        inPllRate = inPllRate / prediv;												// Adjust input clock

        mMult    = _Chip_findPllMMult(SYSCON->AUDPLLCTRL, SYSCON->AUDPLLMDEC);			// MDEC used for rate
        workRate = (uint64_t)inPllRate * (uint64_t)mMult;

        workRate = workRate / ((uint64_t)postdiv);
        workRate = workRate * 2U; 													// SYS PLL hardware cco is divide by 2 before to M-DIVIDER
    }
    else
    {
        workRate = (uint64_t)inPllRate;												// In bypass mode
    }

    return (uint32_t)workRate;
}


// ------------------------------------------------------------------------------------------------
// return MCU Source clock
uint32_t _Chip_Clock_Get_SourceClk_Rate(void)
{
    uint32_t freq = 0U;

    switch (SYSCON->MAINCLKSELB)
    {
        case 0U:
            if (SYSCON->MAINCLKSELA == 0U)
            {
                freq = _Chip_Clock_Get_Fro12M_Rate();
            }
            else if (SYSCON->MAINCLKSELA == 1U)
            {
                freq = _Chip_Clock_Get_eOsc_Rate();
            }
            else if (SYSCON->MAINCLKSELA == 2U)
            {
                freq = _Chip_Clock_Get_Wdt_Inp_Rate();
            }
            else if (SYSCON->MAINCLKSELA == 3U)
            {
                freq = _Chip_Clock_Get_FroHf_Out_Rate();
            }
            else
            {
                /* Misra Compliance. */
            }
            break;
        case 2U:
            if (SYSCON->SYSPLLCLKSEL == 0U)
            {
                freq = _Chip_Clock_Get_Fro12M_Rate();
            }
            else if (SYSCON->SYSPLLCLKSEL == 1U)
            {
                freq = _Chip_Clock_Get_eOsc_Rate();
            }
            else if (SYSCON->SYSPLLCLKSEL == 3U)
            {
                freq = CONF_CHIP_RTCRATEIN;
            }
            else
            {
                /* Misra Compliance. */
            }
            break;

        case 3U:
            freq = CONF_CHIP_RTCRATEIN;
            break;

        default:
            freq = 0;
            break;
    }

    return freq;
	
}

// ------------------------------------------------------------------------------------------------
// Get CPU Frequency rate
uint32_t _Chip_Clock_Get_CoreClk_Rate(void)
{
	
	return(_Chip_Clock_Get_MainClk_Rate() / (((SYSCON->AHBCLKDIV & SYSCON_AHBCLKDIV_DIV_MASK) >> SYSCON_AHBCLKDIV_DIV_SHIFT) + 1));
}

// ------------------------------------------------------------------------------------------------
// Get FRG input Clk based on FRGCLKSEL
uint32_t _Chip_Clock_Get_FRG_In_Rate(void)
{
    uint32_t freq = 0U;

    switch (SYSCON->FRGCLKSEL)
    {
        case 0U:
            freq = _Chip_Clock_Get_MainClk_Rate();
            break;
        case 1U:
            freq = _Chip_Clock_Get_Pll_Out_Rate();
            break;
        case 2U:
            freq = _Chip_Clock_Get_Fro12M_Rate();
            break;
        case 3U:
            freq = _Chip_Clock_Get_FroHf_Out_Rate();
            break;

        default:
            freq = 0;
            break;
    }

    return freq;
}

// ------------------------------------------------------------------------------------------------
// Get FRG Out Clk based on FRGCTRL
uint32_t _Chip_Clock_Get_Frg_Out_Rate(void)
{
    uint32_t freq = 0UL;

    if ((SYSCON->FRGCTRL & SYSCON_FRGCTRL_DIV_MASK) == SYSCON_FRGCTRL_DIV_MASK)
    {
        freq = (uint32_t)((uint64_t)_Chip_Clock_Get_FRG_In_Rate() * (SYSCON_FRGCTRL_DIV_MASK + 1UL)) /
               ((SYSCON_FRGCTRL_DIV_MASK + 1UL) +
                ((SYSCON->FRGCTRL & SYSCON_FRGCTRL_MULT_MASK) >> SYSCON_FRGCTRL_MULT_SHIFT));
    }
    else
    {
        freq = 0UL;
    }

    return freq;
}


// ------------------------------------------------------------------------------------------------
// return clock freq of CLKOUT pin
uint32_t _Chip_Clock_Get_ClkOut_Rate(void)
{
    uint32_t freq = 0U;

    switch (SYSCON->CLKOUTSELA)
    {
        case 0U:
            freq = _Chip_Clock_Get_MainClk_Rate();
            break;

        case 1U:
            freq = _Chip_Clock_Get_eOsc_Rate();
            break;

        case 2U:
            freq = _Chip_Clock_Get_Wdt_Inp_Rate();
            break;

        case 3U:
            freq = _Chip_Clock_Get_FroHf_Out_Rate();
            break;

        case 4U:
            freq = _Chip_Clock_Get_Pll_Out_Rate();
            break;

        case 5U:
            freq = _Chip_Clock_Get_UsbPll_Out_Rate();
            break;

        case 6U:
            freq = _Chip_Clock_Get_AudioPll_Out_Rate();
            break;

        case 7U:
            freq = CONF_CHIP_RTCRATEIN;
            break;

        default:
            freq = 0;
            break;
    }
    return freq / ((SYSCON->CLKOUTDIV & 0xffU) + 1U);
}



// ------------------------------------------------------------------------------------------------
// "USB clk"
// return USB rate
uint32_t _Chip_Clock_Get_UsbClk_Rate(void)						// Get USB0 rate
{
    uint32_t freq = 0U;

    switch (SYSCON->USB0CLKSEL)
    {
        case 0U:
            freq = _Chip_Clock_Get_FroHf_Out_Rate();
            break;
        case 1U:
            freq = _Chip_Clock_Get_Pll_Out_Rate();
            break;
        case 2U:
            freq = _Chip_Clock_Get_UsbPll_Out_Rate();
            break;
        case 7U:
            freq = 0U;
            break;

        default:
            freq = 0;
            break;
    }

    return freq / ((SYSCON->USB0CLKDIV & 0xffU) + 1U);

}

// ------------------------------------------------------------------------------------------------
// "SCT clk Out"
// return SCT rate 
int32_t _Chip_Clock_Get_SCT_Inp_Rate(void)							// Get SCT block input rate
{
    uint32_t freq = 0U;

    switch (SYSCON->SCTCLKSEL)
    {
        case 0U:
            freq = _Chip_Clock_Get_MainClk_Rate();
            break;
        case 1U:
            freq = _Chip_Clock_Get_Pll_Out_Rate();
            break;
        case 2U:
            freq = _Chip_Clock_Get_FroHf_Out_Rate();
            break;
        case 3U:
            freq = _Chip_Clock_Get_AudioPll_Out_Rate();
            break;
        case 7U:
            freq = 0U;
            break;

        default:
            freq = 0;
            break;
    }

    return freq / ((SYSCON->SCTCLKDIV & 0xffU) + 1U);
}


// ------------------------------------------------------------------------------------------------
// Get I2S clock "mclk_in"
//	Return Frequency of I2S MCLK Clock
uint32_t _Chip_Clock_Get_Mclk_Rate(void)
{
   uint32_t freq = 0U;

    switch (SYSCON->MCLKCLKSEL)
    {
        case 0U:
            freq = _Chip_Clock_Get_FroHf_Out_Rate();
            break;
        case 1U:
            freq = _Chip_Clock_Get_AudioPll_Out_Rate();
            break;
        case 7U:
            freq = 0;
            break;

        default:
            freq = 0;
            break;
    }

    return freq;
}

// ------------------------------------------------------------------------------------------------
// Get FLEXCOMM Clk "fcn_fclk"
// input is Flexcomm peri ID based on FCLKSEL
uint32_t _Chip_Clock_Get_FlexCommClk_Rate(uint32_t id)
{
    uint32_t freq = 0U;

    switch (SYSCON->FCLKSEL[id])
    {
        case 0U:
            freq = _Chip_Clock_Get_Fro12M_Rate();
            break;
        case 1U:
            freq = _Chip_Clock_Get_FroHf_Out_Rate() / ((SYSCON->FROHFCLKDIV & SYSCON_FROHFCLKDIV_DIV_MASK) + 1U);
            break;
        case 2U:
            freq = _Chip_Clock_Get_Pll_Out_Rate();
            break;
        case 3U:
            freq = _Chip_Clock_Get_Mclk_Rate();
            break;
        case 4U:
            freq = _Chip_Clock_Get_Frg_Out_Rate();
            break;
        case 7U:
            freq = 0;																// None - clock is disable (due to reduce power)
            break;
        default:
            freq = 0;
            break;
    }

    return freq;
}


// ------------------------------------------------------------------------------------------------
// Get DMIC Clk
// Return Frequency of dmic
uint32_t _Chip_Clock_Get_DmicClk_Rate(void)
{
    uint32_t freq = 0UL;

    switch (SYSCON->DMICCLKSEL)
    {
        case 0U:
            freq = _Chip_Clock_Get_Fro12M_Rate();
            break;
        case 1U:
            freq = _Chip_Clock_Get_FroHf_Out_Rate();
            break;
        case 2U:
            freq = _Chip_Clock_Get_Pll_Out_Rate();
            break;
        case 3U:
            freq = _Chip_Clock_Get_Mclk_Rate();
            break;
        case 4U:
            freq = _Chip_Clock_Get_MainClk_Rate();
            break;
        case 5U:
            freq = _Chip_Clock_Get_Wdt_Inp_Rate();
            break;
        default:
            freq = 0;
            break;
    }

    return freq / ((SYSCON->DMICCLKDIV & 0xffU) + 1U);
}


// ------------------------------------------------------------------------------------------------
// Get ADC Clk
// Return Frequency of ADC
uint32_t _Chip_Clock_Get_ADCClk_Rate(void)
{
    uint32_t freq = 0UL;

	if ADC_CTRL_CLKDIV_MASK
		
	if(ADC0->CTRL & ADC_CTRL_ASYNMODE_MASK) 
	{																				// Asynchronous mode
		switch (SYSCON->ADCCLKSEL)
		{
			case 0U:
				freq = _Chip_Clock_Get_FroHf_Out_Rate();
				break;
			case 1U:
				freq = _Chip_Clock_Get_Pll_Out_Rate();
				break;
			case 2U:
				freq = _Chip_Clock_Get_UsbPll_Out_Rate();
				break;
			case 3U:
				freq = _Chip_Clock_Get_AudioPll_Out_Rate();
				break;
			case 7U:
				freq = 0U;
				break;
			default:
				freq = 0;
				break;
		}
		freq = freq/((SYSCON->ADCCLKDIV & SYSCON_ADCCLKDIV_DIV_MASK) + 1U);			// Aplly divider from SYSCON
	}
	else																			// Synchronous mode
	{
		freq = _Chip_Clock_Get_SysClk_Rate()/((ADC0->CTRL & ADC_CTRL_CLKDIV_MASK) + 1U);	// else system_clock as source and apply divider from ADCCTRL
	}
    return (freq);
}


// ------------------------------------------------------------------------------------------------
// Get LCD Clk
uint32_t _Chip_Clock_Get_LCDClk_Rate(void)
{
    uint32_t freq = 0U;

    switch (SYSCON->LCDCLKSEL)
    {
        case 0U:
            freq = _Chip_Clock_Get_MainClk_Rate();
            break;
        case 1U:
            freq = CONF_CHIP_LCDRATEIN;
            break;
        case 2U:
            freq = _Chip_Clock_Get_FroHf_Out_Rate();
            break;
        case 3U:
            freq = 0U;
            break;

        default:
            freq = 0U;
            break;
    }

    return freq / ((SYSCON->LCDCLKDIV & 0xffU) + 1U);
}

// ------------------------------------------------------------------------------------------------
// Get Async APB clock Rate
uint32_t _Chip_Clock_Get_AsyncAPB_Rate(void)
{
    uint32_t freq = 0U;

    switch (ASYNC_SYSCON->ASYNCAPBCLKSELA)
    {
        case 0U:
            freq = _Chip_Clock_Get_MainClk_Rate();									// Get main_clk
            break;
        case 1U:
            freq = _Chip_Clock_Get_Fro12M_Rate();									// Get internal FRO oscillator 12MHz
            break;
        case 2U:
            freq = _Chip_Clock_Get_AudioPll_Out_Rate();								// Get Audio PLL Out clk rate
            break;
        case 3U:
            freq = _Chip_Clock_Get_FlexCommClk_Rate(6);								// Get FlexComm[6] rate
            break;

        default:
            freq = 0;
            break;
    }

    return freq;
}


// ------------------------------------------------------------------------------------------------
// Set the FLASH wait states for the passed frequency
void _Chip_SetFLASHAccessCyclesForFreq(uint32_t iFreq)
{
   uint32_t tmp = SYSCON->FLASHCFG & ~(SYSCON_FLASHCFG_FLASHTIM_MASK);

	
    if (iFreq <= 12000000U)
    {
	    SYSCON->FLASHCFG = tmp | ((uint32_t)0x00 << SYSCON_FLASHCFG_FLASHTIM_SHIFT);	// Don't alter lower bits
    }
    else if (iFreq <= 24000000U)
    {
	    SYSCON->FLASHCFG = tmp | ((uint32_t)0x01 << SYSCON_FLASHCFG_FLASHTIM_SHIFT);	// Don't alter lower bits
    }
    else if (iFreq <= 36000000U)
    {
	    SYSCON->FLASHCFG = tmp | ((uint32_t)0x02 << SYSCON_FLASHCFG_FLASHTIM_SHIFT);	// Don't alter lower bits
    }
    else if (iFreq <= 60000000U)
    {
	    SYSCON->FLASHCFG = tmp | ((uint32_t)0x03 << SYSCON_FLASHCFG_FLASHTIM_SHIFT);	// Don't alter lower bits
    }
    else if (iFreq <= 96000000U)
    {
	    SYSCON->FLASHCFG = tmp | ((uint32_t)0x04 << SYSCON_FLASHCFG_FLASHTIM_SHIFT);	// Don't alter lower bits
    }
    else if (iFreq <= 120000000U)
    {
	    SYSCON->FLASHCFG = tmp | ((uint32_t)0x05 << SYSCON_FLASHCFG_FLASHTIM_SHIFT);	// Don't alter lower bits
    }
    else if (iFreq <= 144000000U)
    {
	    SYSCON->FLASHCFG = tmp | ((uint32_t)0x06 << SYSCON_FLASHCFG_FLASHTIM_SHIFT);	// Don't alter lower bits
    }
	
    /* At 220 MHz the system clock/access time can be lower when compared to 180 MHz because the power library optimizes
     * the on-chip voltage regulator */
    else if ((iFreq <= 168000000U) || ((iFreq > 180000000U) && (iFreq <= 220000000U)))
    {
	    SYSCON->FLASHCFG = tmp | ((uint32_t)0x07 << SYSCON_FLASHCFG_FLASHTIM_SHIFT);	// Don't alter lower bits
    }
    else
    {
	    SYSCON->FLASHCFG = tmp | ((uint32_t)0x08 << SYSCON_FLASHCFG_FLASHTIM_SHIFT);	// Don't alter lower bits
    }
}


// ------------------------------------------------------------------------------------------------
// Find encoded NDEC value for raw N value, max N = NVALMAX
uint32_t _Chip_pllEncodeN(uint32_t N)
{
    uint32_t x, i;

    switch (N)																		// Find NDec
    {
        case 0U:
            x = 0x3FFU;
            break;

        case 1U:
            x = 0x302U;
            break;

        case 2U:
            x = 0x202U;
            break;

        default:
            x = 0x080U;
            for (i = N; i <= NVALMAX; i++)
            {
                x = (((x ^ (x >> 2U) ^ (x >> 3U) ^ (x >> 4U)) & 1U) << 7U) | ((x >> 1U) & 0x7FU);
            }
            break;
    }

    return x & (PLL_NDEC_VAL_M >> PLL_NDEC_VAL_P);
}

// ------------------------------------------------------------------------------------------------
// Find decoded N value for raw NDEC value
uint32_t _Chip_pllDecodeN(uint32_t NDEC)
{
    uint32_t n, x, i;

    switch (NDEC)																	// Find NDec
    {
        case 0x3FFU:
            n = 0U;
            break;

        case 0x302U:
            n = 1U;
            break;

        case 0x202U:
            n = 2U;
            break;

        default:
            x = 0x080U;
            n = 0xFFFFFFFF;
            for (i = NVALMAX; i >= 3U; i--)
            {
                if (n != 0xFFFFFFFF)
                {
                    break;
                }
                x = (((x ^ (x >> 2U) ^ (x >> 3U) ^ (x >> 4U)) & 1U) << 7U) | ((x >> 1U) & 0x7FU);
                if ((x & (PLL_NDEC_VAL_M >> PLL_NDEC_VAL_P)) == NDEC)
                {
                    n = i;															// Decoded value of NDEC
                }
            }
            break;
    }

    return n;
}

// ------------------------------------------------------------------------------------------------
// Find encoded PDEC value for raw P value, max P = PVALMAX
uint32_t _Chip_pllEncodeP(uint32_t P)
{
    uint32_t x, i;

    switch (P)																		// Find PDec
    {
        case 0U:
            x = 0x7FU;
            break;

        case 1U:
            x = 0x62U;
            break;

        case 2U:
            x = 0x42U;
            break;

        default:
            x = 0x10U;
            for (i = P; i <= PVALMAX; i++)
            {
                x = (((x ^ (x >> 2U)) & 1U) << 4U) | ((x >> 1U) & 0xFU);
            }
            break;
    }

    return x & (PLL_PDEC_VAL_M >> PLL_PDEC_VAL_P);
}

// ------------------------------------------------------------------------------------------------
// Find decoded P value for raw PDEC value
uint32_t _Chip_pllDecodeP(uint32_t PDEC)
{
    uint32_t p, x, i;

    switch (PDEC)																	// Find PDec
    {
        case 0x7FU:
            p = 0U;
            break;

        case 0x62U:
            p = 1U;
            break;

        case 0x42U:
            p = 2U;
            break;

        default:
            x = 0x10U;
            p = 0xFFFFFFFFU;
            for (i = PVALMAX; i >= 3U; i--)
            {
                if (p != 0XFFFFFFFFUL)
                {
                    break;
                }

                x = (((x ^ (x >> 2U)) & 1U) << 4U) | ((x >> 1U) & 0xFU);
                if ((x & (PLL_PDEC_VAL_M >> PLL_PDEC_VAL_P)) == PDEC)
                {
                    p = i;															// Decoded value of PDEC
                }
            }
            break;
    }

    return p;
}

// ------------------------------------------------------------------------------------------------
// Find encoded MDEC value for raw M value, max M = MVALMAX
uint32_t _Chip_pllEncodeM(uint32_t M)
{
    uint32_t i, x;

    switch (M)																		// Find MDec
    {
        case 0U:
            x = 0x1FFFFU;
            break;

        case 1U:
            x = 0x18003U;
            break;

        case 2U:
            x = 0x10003U;
            break;

        default:
            x = 0x04000U;
            for (i = M; i <= MVALMAX; i++)
            {
                x = (((x ^ (x >> 1U)) & 1U) << 14U) | ((x >> 1U) & 0x3FFFU);
            }
            break;
    }

    return x & (PLL_MDEC_VAL_M >> PLL_MDEC_VAL_P);
}

// ------------------------------------------------------------------------------------------------
// Find decoded M value for raw MDEC value
uint32_t _Chip_pllDecodeM(uint32_t MDEC)
{
    uint32_t m, i, x;

    switch (MDEC)																	// Find MDec
    {
        case 0x1FFFFU:
            m = 0U;
            break;

        case 0x18003U:
            m = 1U;
            break;

        case 0x10003U:
            m = 2U;
            break;

        default:
            x = 0x04000U;
            m = 0xFFFFFFFFU;
            for (i = MVALMAX; i >= 3U; i--)
            {
                if (m != 0xFFFFFFFFUL)
                {
                    break;
                }
                x = (((x ^ (x >> 1U)) & 1U) << 14U) | ((x >> 1U) & 0x3FFFU);
                if ((x & (PLL_MDEC_VAL_M >> PLL_MDEC_VAL_P)) == MDEC)
                {
                    m = i;															// Decoded value of MDEC
                }
            }
            break;
    }

    return m;
}

// ------------------------------------------------------------------------------------------------
// Find SELP, SELI, and SELR values for raw M divider value, max M = MVALMAX
void _Chip_pllFindSel(uint32_t M, uint32_t *pSelP, uint32_t *pSelI, uint32_t *pSelR)
{
    // bandwidth: compute selP from Multiplier
    if (M < 60U)
    {
        *pSelP = (M >> 1U) + 1U;
    }
    else
    {
        *pSelP = PVALMAX - 1U;
    }

    // bandwidth: compute selI from Multiplier
    if (M > 16384U)
    {
        *pSelI = 1U;
    }
    else if (M > 8192U)
    {
        *pSelI = 2U;
    }
    else if (M > 2048U)
    {
        *pSelI = 4U;
    }
    else if (M >= 501U)
    {
        *pSelI = 8U;
    }
    else if (M >= 60U)
    {
        *pSelI = 4U * (1024U / (M + 9U));
    }
    else
    {
        *pSelI = (M & 0x3CU) + 4U;
    }

    if (*pSelI > ((0x3FUL << SYSCON_SYSPLLCTRL_SELI_SHIFT) >> SYSCON_SYSPLLCTRL_SELI_SHIFT))
    {
        *pSelI = ((0x3FUL << SYSCON_SYSPLLCTRL_SELI_SHIFT) >> SYSCON_SYSPLLCTRL_SELI_SHIFT);
    }

    *pSelR = 0U;
}

// ------------------------------------------------------------------------------------------------
// Get predivider (N) from PLL NDEC setting
uint32_t _Chip_findPllPreDiv(uint32_t ctrlReg, uint32_t nDecReg)
{
    uint32_t preDiv = 1;

    if ((ctrlReg & (1UL << SYSCON_SYSPLLCTRL_DIRECTI_SHIFT)) == 0U)					// Direct input is not used?
    {
        preDiv = _Chip_pllDecodeN(nDecReg & 0x3FFU);										// Decode NDEC value to get (N) pre divider
        if (preDiv == 0U)
        {
            preDiv = 1U;
        }
    }

    // Adjusted by 1, directi is used to bypass
    return preDiv;
}

// ------------------------------------------------------------------------------------------------
// Get postdivider (P) from PLL PDEC setting
uint32_t _Chip_findPllPostDiv(uint32_t ctrlReg, uint32_t pDecReg)
{
    uint32_t postDiv = 1U;

    if ((ctrlReg & SYSCON_SYSPLLCTRL_DIRECTO_MASK) == 0U)							// Direct input is not used?
    {
        postDiv = 2U * _Chip_pllDecodeP(pDecReg & 0x7FU);									// Decode PDEC value to get (P) post divider
        if (postDiv == 0U)
        {
            postDiv = 2U;
        }
    }

    // Adjusted by 1, directo is used to bypass
    return postDiv;
}

// ------------------------------------------------------------------------------------------------
// Get multiplier (M) from PLL MDEC and BYPASS_FBDIV2 settings
uint32_t _Chip_findPllMMult(uint32_t ctrlReg, uint32_t mDecReg)
{
    uint32_t mMult = 1U;

    mMult = _Chip_pllDecodeM(mDecReg & 0x1FFFFU);											// Decode MDEC value to get (M) multiplier
    if (mMult == 0U)
    {
        mMult = 1U;
    }
    return mMult;
}


// ------------------------------------------------------------------------------------------------
// Convert the binary to fractional part
double _Chip_Binary2Fractional(uint32_t binaryPart)
{
    double fractional = 0.0;
    for (uint32_t i = 0; i <= 14UL; i++)
    {
        fractional += (double)(uint32_t)((binaryPart >> i) & 0x1UL) / (double)(uint32_t)(1UL << (15U - i));
    }
    return fractional;
}

// ------------------------------------------------------------------------------------------------
// Find greatest common divisor between m and n
uint32_t _Chip_FindGreatestCommonDivisor(uint32_t m, uint32_t n)
{
    uint32_t tmp;

    while (n != 0U)
    {
        tmp = n;
        n   = m % n;
        m   = tmp;
    }
    return m;
}

// ------------------------------------------------------------------------------------------------
// return frequency for watchdog "fro_12m"
uint32_t _Chip_Clock_Get_Fro12M_Rate(void)
{
    return ((SYSCON->PDRUNCFG[0] & SYSCON_PDRUNCFG_PDEN_FRO_MASK) != 0UL) ? 0U : 12000000U;
}

// ------------------------------------------------------------------------------------------------
// return frequency of external inp "clk_in"
// Frequency of External Quartz Clock. If no external Quartz clock is used returns 0.
uint32_t _Chip_Clock_Get_eOsc_Rate(void)
{
    return (CONF_CHIP_OSCRATEIN);
}

// ------------------------------------------------------------------------------------------------
// Set PLL frequency.
// Input clock have to active! 
//
// UM10912: page 167
// Check that the selected settings meet all of the PLL requirements:
//	� Fin is in the range of 32 kHz to 25 MHz.
//	� Fcco is in the range of 275 MHz to 550 MHz.
//	� Fout is in the range of 4.3 MHz to 550 MHz.
//	� The pre-divider is either bypassed, or N is in the range of 2 to 256.
//	� The post-divider is either bypassed, or P is in the range of 2 to 32.
//	� M is in the range of 3 to 32,768.
// Also note that PLL startup time becomes longer as Fref drops below 500 kHz. At 500
// kHz and above, startup time is up to 500 microseconds. Below 500 kHz, startup time
// can be estimated as 200 / Fref, or up to 6.1 milliseconds for Fref = 32 kHz. PLL
// accuracy and jitter is better with higher values of Fref
void _Chip_Clock_Set_PLLFreq(uint32_t pllN, uint32_t pllM, uint32_t pllP)
{
	uint32_t SELP, SELI, SELR;
	
    if ((SYSCON->SYSPLLCLKSEL & SYSCON_SYSPLLCLKSEL_SEL_MASK) == 0x01U)				
    {
        /* Turn on the ext clock if system pll input select clk_in */
        CLOCK_Enable_SysOsc(true);
    }

    POWER_SetPLL();																	// Enable power VD3 for PLLs
	
    /* Power off PLL during setup changes */
    POWER_EnablePD(kPDRUNCFG_PD_SYS_PLL0);

	// Set filter after Phase frequency detector:
	_Chip_pllFindSel(pllM, &SELP, &SELI, &SELR);
	
	//SELP = 16; SELI = 32; SELR = 0;
	SYSCON->SYSPLLCTRL = SYSCON_SYSPLLCTRL_SELI(SELI) | SYSCON_SYSPLLCTRL_SELP(SELP) | SYSCON_SYSPLLCTRL_SELR(SELR);
	
	// Set Input N divider
	SYSCON->SYSPLLNDEC = SYSCON_SYSPLLNDEC_NDEC(_Chip_pllEncodeN(pllN));
	SYSCON->SYSPLLNDEC = SYSCON_SYSPLLNDEC_NDEC(_Chip_pllEncodeN(pllN)) | SYSCON_SYSPLLNDEC_NREQ(1);

	// Set Output 2xP divider
    SYSCON->SYSPLLPDEC = SYSCON_SYSPLLPDEC_PDEC(_Chip_pllEncodeP(pllP));
	SYSCON->SYSPLLPDEC = SYSCON_SYSPLLPDEC_PDEC(_Chip_pllEncodeP(pllP)) | SYSCON_SYSPLLPDEC_PREQ(1);
	
	// Set feedback's M multiplier
	SYSCON->SYSPLLMDEC = SYSCON_SYSPLLMDEC_MDEC(_Chip_pllEncodeM(pllM));
	SYSCON->SYSPLLMDEC = SYSCON_SYSPLLMDEC_MDEC(_Chip_pllEncodeM(pllM)) | SYSCON_SYSPLLMDEC_MREQ(1);


	// Power ON and wait for Lock code here:
	// Set feedback's M multiplier - temporary set maximal value for fast PLL Lock after Power ON
	SYSCON->SYSPLLMDEC = 0x5dd2U | (1UL << 18U);

	POWER_DisablePD(kPDRUNCFG_PD_SYS_PLL0);
	
	SYSCON->SYSPLLMDEC = 0x5dd2U | (1UL << 18U)| SYSCON_SYSPLLMDEC_MREQ(1);			// Set mreq to activate


	

	// Delay for 72 uSec @ 12Mhz
	DelayChipUs(72, 12000000);

	// Set feedback's M multiplier
	SYSCON->SYSPLLMDEC = SYSCON_SYSPLLMDEC_MDEC(_Chip_pllEncodeM(pllM));
	SYSCON->SYSPLLMDEC = SYSCON_SYSPLLMDEC_MDEC(_Chip_pllEncodeM(pllM)) | SYSCON_SYSPLLMDEC_MREQ(1);

	// Power ON PLL
	POWER_DisablePD(kPDRUNCFG_PD_SYS_PLL0);

	// Wait for Lock - skoncime tu!!!! WHY ????
    while ((SYSCON->SYSPLLSTAT & SYSCON_SYSPLLSTAT_LOCK_MASK) == 0UL)				// Wait for PLL Lock
    {
    }

}



// ------------------------------------------------------------------------------------------------
// FLEXCOMM Functions 
// ------------------------------------------------------------------------------------------------
const IRQn_Type IRQ_FlexComm_Array[] = FLEXCOMM_IRQS;
static _CHIP_FLEXCOMM_T *pPeri_FLEX_USED[_CHIP_FLEXCOMM_COUNT] ;

int32_t _Chip_FlexComm_Get_Free(int32_t PeriIndex, _Chip_FlexComm_Peri_T ReqPeriType)
{
//	if(PeriIndex == -1)		// auto search free Flexcomm
//	{
//		for(uint8_t i=0; i < _CHIP_FLEXCOMM_COUNT; i++)
//		{
//			if(pPeri_FLEX_USED[i] == NULL) 
//			{
//				// I2S periphery only for Flexcomm 6 or 7
//				if((ReqPeriType == FLEXCOMM_PERIPH_I2S_TX) | (ReqPeriType == FLEXCOMM_PERIPH_I2S_RX))
//				{
//					if((i==6) | (i == 7)) PeriIndex = i; break;						// Assign Peri Index
//				}
//				else PeriIndex = i; break;											// Assign Peri Index
//			}
//		}
//	}
//	else
//	{	
//		uint8_t PeriTest = PeriIndex;												// start checking by entered value
//		PeriIndex = -1;																// default is not searched
//		// start test to entered flexcomm
//		uint8_t i = _CHIP_FLEXCOMM_COUNT - 1;
//		do
//		{
//			if(pPeri_FLEX_USED[PeriTest] == NULL) 
//			{
//				// I2S periphery only for Flexcomm 6 or 7
//				if((ReqPeriType == FLEXCOMM_PERIPH_I2S_TX) | (ReqPeriType == FLEXCOMM_PERIPH_I2S_RX))
//				{
//					if((i==6) | (i == 7)) PeriIndex = i; break;						// Assign Peri Index
//				}
//				else PeriIndex = i; break;											// Assign Peri Index
//			}
//			if(PeriTest == _CHIP_FLEXCOMM_COUNT) PeriTest = 0;						// rotate
//			else PeriTest ++;
//			i --;
//		}
//		while(i);
//	}

//	if(pPeri_FLEX_USED[PeriIndex]) return(-1);										// Flecomm occupied
//	else return(PeriIndex);
	return(PeriIndex);
}

// ------------------------------------------------------------------------------------------------------
// FlexComm(Num) - change mode of useability
// if Force=true, mode will changed in spite occupied another peri.
bool _Chip_FlexComm_Change_Mode(_CHIP_FLEXCOMM_T *pFlex, _Chip_FlexComm_Peri_T ReqPeriType, bool Lock)
{
	//_CHIP_FLEXCOMM_T pFlex = _Chip_FlexComm_Get_PeriIndex(FlexIndex);
	
    /* Flexcomm is locked to different peripheral type than expected  */
    if (((pFlex->PSELID & FLEXCOMM_PSELID_LOCK_MASK) != 0U) && ((pFlex->PSELID & FLEXCOMM_PSELID_PERSEL_MASK) != (uint32_t)ReqPeriType))
    {
        return (false);
    }

    if (Lock != 0)																	// Check if we are asked to lock
    {
        pFlex->PSELID = (uint32_t)ReqPeriType | FLEXCOMM_PSELID_LOCK_MASK;
    }
    else
    {
        pFlex->PSELID = (uint32_t)ReqPeriType;
    }

    return (true);
}

// ------------------------------------------------------------------------------------------------------
// Init
void *_Chip_FlexComm_Init(int32_t FlexIndex)
{
	void *ResPtr = NULL;

	// Check:
	SYS_ASSERT((FlexIndex < _CHIP_FLEXCOMM_COUNT) && (FlexIndex >= 0));				// check - in release will be ASSERT skipped
	
	switch(FlexIndex)
	{
		case 0: ResPtr = (void *)FLEXCOMM0; break;
		case 1: ResPtr = (void *)FLEXCOMM1; break;
		case 2: ResPtr = (void *)FLEXCOMM2; break;
		case 3: ResPtr = (void *)FLEXCOMM3; break;
		case 4: ResPtr = (void *)FLEXCOMM4; break;
		case 5: ResPtr = (void *)FLEXCOMM5; break;
		case 6: ResPtr = (void *)FLEXCOMM6; break;
		case 7: ResPtr = (void *)FLEXCOMM7; break;
		case 8: ResPtr = (void *)FLEXCOMM8; break;
		case 9: ResPtr = (void *)FLEXCOMM9; break;
		default: return(NULL);														// wrong Index
	}

	// Power:
	// nothing to power up/down
	
	// Clock:
	_Chip_FlexComm_Enable_Clock(FlexIndex); 										// enable clock
	
	// Reset:
	_Chip_FlexComm_Set_Reset(FlexIndex);											// generate reset
	_Chip_FlexComm_Clear_Reset(FlexIndex);											// release reset
	
	SYSCON->FCLKSEL[FlexIndex] = 0x04;												// select FRG Clock as input CLK
	
	pPeri_FLEX_USED[FlexIndex] = ResPtr;											// Save ptr as used flexcomm
	
	return((void*)ResPtr);															// return pointer to perihery
}



// ------------------------------------------------------------------------------------------------
// GPIO Functions 
// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------
// Get ptr from index
void *_Chip_Get_GPIO_Ptr(int32_t PeriIndex)							// vrat pointer na konkretnu GPIO periferiu
{
	return(GPIO);																	// LPC546xx have 5 GPIOs, but all in has one ptr
}

// ------------------------------------------------------------------------------------------------
// Set Direction for a GPIO port 
void _Chip_GPIO_Set_PinDir(_CHIP_GPIO_T *pGPIO, uint8_t portNum, uint32_t bitValue, uint8_t out)
{
	if (out) pGPIO->DIR[portNum] |= bitValue;
	else pGPIO->DIR[portNum] &= ~bitValue;
}

// ------------------------------------------------------------------------------------------------
// Read Pin Value
bool _Chip_GPIO_GetPinState(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin)
{
	if(pGPIO->B[port][pin]) return(true);
	else return false;
}

// ------------------------------------------------------------------------------------------------
// Clear Pin
void _Chip_GPIO_SetPinOutLow(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin)
{
	pGPIO->CLR[port] = (1 << pin);
}

// ------------------------------------------------------------------------------------------------
// Set Pin
void _Chip_GPIO_SetPinOutHigh(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin)
{
	pGPIO->SET[port] = (1 << pin);
}

// ------------------------------------------------------------------------------------------------
// Toggle Pin
void _Chip_GPIO_SetPinToggle(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin)
{
	pGPIO->NOT[port] = (1 << pin);
}

// ------------------------------------------------------------------------------------------------
// Enable/Disable PINT IRQ
bool _Chip_GPIO_PINT_Enable_IRQ_NVIC(void *pPeri, uint32_t Port, uint32_t Pin, bool NewState)
{
	ERR_BREAK;			// needs to complette this function
	return(false);
}

// ------------------------------------------------------------------------------------------------
// HW Initialization
// Initialize port and pin
void *_Chip_GPIO_Init ( int32_t PeriIndex, uint32_t Pin)
{
	void *res = NULL;

	SYS_ASSERT((PeriIndex <= 5) && (PeriIndex >= 0));								// check - in release will be ASSERT skipped
	
	switch(PeriIndex)
	{
		case 5:
			{
				if(!(SYSCON->AHBCLKCTRL[2] & SYSCON_AHBCLKCTRL_GPIO5(1)))			// CLK already enabled?
				{				
					SYSCON->AHBCLKCTRL[2] |= SYSCON_AHBCLKCTRL_GPIO5(1);			// Enable system clock for GPIO5
					SYSCON->PRESETCTRL[2] |= (SYSCON_PRESETCTRL_GPIO5_RST(1));__nop();	// Activate reset
					SYSCON->PRESETCTRL[2] &= ~(SYSCON_PRESETCTRL_GPIO5_RST(1));		// Deactivate reset
				}
				res = (void *) GPIO;
				break;
			}						
		case 4: 
			{
				if(!(SYSCON->AHBCLKCTRL[2] & SYSCON_AHBCLKCTRL_GPIO4(1)))			// CLK already enabled?
				{				
					SYSCON->AHBCLKCTRL[2] |= SYSCON_AHBCLKCTRL_GPIO4(1);			// Enable system clock for GPIO4
					SYSCON->PRESETCTRL[2] |= (SYSCON_PRESETCTRL_GPIO4_RST(1));__nop();	// Activate reset
					SYSCON->PRESETCTRL[2] &= ~(SYSCON_PRESETCTRL_GPIO4_RST(1));		// Deactivate reset
				}
				res = (void *) GPIO;			
				break;				
			}
		case 3: 
			{
				if(!(SYSCON->AHBCLKCTRL[0] & SYSCON_AHBCLKCTRL_GPIO3(1)))			// CLK already enabled?
				{				
					SYSCON->AHBCLKCTRL[0] |= SYSCON_AHBCLKCTRL_GPIO3(1);			// Enable system clock for GPIO3
					SYSCON->PRESETCTRL[0] |= (SYSCON_PRESETCTRL_GPIO3_RST(1));__nop();	// Activate reset
					SYSCON->PRESETCTRL[0] &= ~(SYSCON_PRESETCTRL_GPIO3_RST(1));		// Deactivate reset
				}
				res = (void *) GPIO;
				break;				
			}
		case 2: 
			{
				if(!(SYSCON->AHBCLKCTRL[0] & SYSCON_AHBCLKCTRL_GPIO2(1)))			// CLK already enabled?
				{				
					SYSCON->AHBCLKCTRL[0] |= SYSCON_AHBCLKCTRL_GPIO2(1);			// Enable system clock for GPIO2
					SYSCON->PRESETCTRL[0] |= (SYSCON_PRESETCTRL_GPIO2_RST(1));__nop();	// Activate reset
					SYSCON->PRESETCTRL[0] &= ~(SYSCON_PRESETCTRL_GPIO2_RST(1));		// Deactivate reset
				}
				res = (void *) GPIO;
				break;				
			}
		case 1: 
			{
				if(!(SYSCON->AHBCLKCTRL[0] & SYSCON_AHBCLKCTRL_GPIO1(1)))			// CLK already enabled?
				{				
					SYSCON->AHBCLKCTRL[0] |= SYSCON_AHBCLKCTRL_GPIO1(1);			// Enable system clock for GPIO1
					SYSCON->PRESETCTRL[0] |= (SYSCON_PRESETCTRL_GPIO1_RST(1));__nop();	// Activate reset
					SYSCON->PRESETCTRL[0] &= ~(SYSCON_PRESETCTRL_GPIO1_RST(1));		// Deactivate reset
				}
				res = (void *) GPIO;
				break;				
			}
		case 0: 
			{
				if(!(SYSCON->AHBCLKCTRL[0] & SYSCON_AHBCLKCTRL_GPIO0(1)))			// CLK already enabled?
				{
					SYSCON->AHBCLKCTRL[0] |= SYSCON_AHBCLKCTRL_GPIO0(1);			// Enable system clock for GPIO0
					SYSCON->PRESETCTRL[0] |= (SYSCON_PRESETCTRL_GPIO0_RST(1));__nop();	// Activate reset
					SYSCON->PRESETCTRL[0] &= ~(SYSCON_PRESETCTRL_GPIO0_RST(1));		// Deactivate reset
				}
				res = (void *) GPIO;
				break;
			}
	}
	return(res);
}

// ------------------------------------------------------------------------------------------------
// Hardware periphery Deinitialization
bool _Chip_GPIO_DeInit ( int32_t PeriIndex, uint32_t Pin)
{
	switch(PeriIndex)
	{
		case 5:
			{
				SYSCON->AHBCLKCTRL[2]  &= ~(SYSCON_AHBCLKCTRL_GPIO5(1));			// Disable system clock for GPIO5
				break;
			}						
		case 4: 
			{
				SYSCON->AHBCLKCTRL[2]  &= ~(SYSCON_AHBCLKCTRL_GPIO4(1));			// Disable system clock for GPIO4
				break;				
			}
		case 3: 
			{
				SYSCON->AHBCLKCTRL[0]  &= ~(SYSCON_AHBCLKCTRL_GPIO3(1));			// Disable system clock for GPIO3
				break;				
			}
		case 2: 
			{
				SYSCON->AHBCLKCTRL[0]  &= ~(SYSCON_AHBCLKCTRL_GPIO2(1));			// Disable system clock for GPIO2
				break;				
			}
		case 1: 
			{
				SYSCON->AHBCLKCTRL[0]  &= ~(SYSCON_AHBCLKCTRL_GPIO1(1));			// Disable system clock for GPIO1
				break;				
			}
		case 0: 
			{
				SYSCON->AHBCLKCTRL[0]  &= ~(SYSCON_AHBCLKCTRL_GPIO0(1));			// Disable system clock for GPIO0
				break;
			}
		default: return(false);
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Pin configure - for LPC546xx (without SWM Block)
// Index- target register
// 	0	- IOCON : bit 7-0
// 	1	- IOCON : bit 15-8
//	2	- not used
//  3 	- not used
// write into register Reg_Idx value of the Reg_Val
void _Chip_GPIO_Wr_Conf_Pin (uint32_t Port, uint32_t Pin, uint8_t Reg_Idx, const _Chip_IO_Spec_t *Reg_Val)
{
	uint32_t tmp = 0;
	uint32_t SaveIOCON = SYSCON->AHBCLKCTRL[0] & SYSCON_AHBCLKCTRL_IOCON_MASK;		// Read IOCON CLK state;

	SYSCON->AHBCLKCTRL[0] |= SYSCON_AHBCLKCTRL_IOCON(1);							// Enable system clock for IOCON
	
	tmp = IOCON->PIO[Port][Pin];													// Read original vaule of IOCON
	tmp &= 0xffff0000;																// clear lower 2 bytes
	tmp |= Reg_Val->IOCON_Reg;														// save - 0 byte 

	IOCON->PIO[Port][Pin] = tmp;													// and write to IOCON

	SYSCON->AHBCLKCTRL[0] = (SYSCON->AHBCLKCTRL[0] & ~SYSCON_AHBCLKCTRL_IOCON_MASK) | SaveIOCON;// return back clock for IOCON
}

// ------------------------------------------------------------------------------------------------
// Pin configure - interrupt generation conditions - Pin change interrupt
//	Sens (for whole group!):
// 	0x00	- level - low
// 	0x11	- level - high
// 	0x01	- edge - low to high
// 	0x10	- edge - high to low
// return is pointer to PINT
void *_Chip_GPIO_PINT_Conf(uint8_t PINTSel, uint32_t Port, uint32_t Pin, uint8_t Sens)
{
	// Check:
	if(PINTSel >= INPUTMUX_PINTSEL_COUNT) return(NULL);								// LPC546xx has PINTSEL 0 - 7 only
	
	// Power:
	// nothing to power up/down
	
	// Clock:
	SYSCON->AHBCLKCTRL[0] |= SYSCON_AHBCLKCTRL_PINT(1);								// Enable system clock for PINT

	// Reset:
	//SYSCON->PRESETCTRL[0] |= (SYSCON_PRESETCTRL_PINT_RST(1));__nop();				// Activate reset
	//SYSCON->PRESETCTRL[0] &= ~(SYSCON_PRESETCTRL_PINT_RST(1));					// Deactivate reset

	
	// Configure INMUX
	SYSCON->AHBCLKCTRL[0] |= SYSCON_AHBCLKCTRL_INPUTMUX(1);							// Enable system clock for MUX
	INPUTMUX->PINTSEL[PINTSel] = (Port * 32) + Pin;									// select port and pin to input MUX
	SYSCON->AHBCLKCTRL[0] &= ~(SYSCON_AHBCLKCTRL_INPUTMUX(1));						// Disable system clock for MUX
	
	// Configure PINT
	switch(Sens)
	{
		case 0x00: 						// low level interrupt
			{
				PINT->ISEL |= (1 << PINTSel);										// select LEVEL mode
				PINT->SIENR |= (1 << PINTSel);										// Enable level interrupt
				PINT->CIENF |= (1 << PINTSel);										// select low-level
				break;
			}
		case 0x01:						// Rising edge interrupt
			{
				PINT->ISEL &= ~(1 << PINTSel);										// select rising EDGE mode
				PINT->SIENR |= (1 << PINTSel);										// Enable rising edge interrupt
				PINT->CIENF |= (1 << PINTSel);										// Disable falling edge interrupt
				break;
			}
		case 0x10:						// Falling edge interrupt
			{
				PINT->ISEL &= ~(1 << PINTSel);										// select falling EDGE mode
				PINT->CIENR |= (1 << PINTSel);										// Disable rising edge interrupt
				PINT->SIENF |= (1 << PINTSel);										// Enable falling edge interrupt
				break;
			}
		case 0x22:						// Rising or Falling edge interrupt
			{
				PINT->ISEL &= ~(1 << PINTSel);										// select rising EDGE mode
				PINT->SIENR |= (1 << PINTSel);										// Enable rising edge interrupt
				PINT->SIENF |= (1 << PINTSel);										// Enable falling edge interrupt
				break;
			}
		case 0x11: 						// High level interrupt
			{
				PINT->ISEL |= (1 << PINTSel);										// select LEVEL mode
				PINT->SIENR |= (1 << PINTSel);										// Enable level interrupt
				PINT->SIENF |= (1 << PINTSel);										// select high level
				break;
			}
		default: return(NULL);
	}
	PINT->RISE |= (1 << PINTSel);													// clear rise flag
	PINT->FALL |= (1 << PINTSel);													// clear fall flag
	
	// Clear IRQ:
	NVIC_ClearPendingIRQ((IRQn_Type) ((uint32_t) PIN_INT0_IRQn + PINTSel));			// clear pending interrupt flag

	// Enable IRQ:
	NVIC_EnableIRQ((IRQn_Type) ((uint32_t) PIN_INT0_IRQn + PINTSel));				// Enable interrupt flag

	return((void *) PINT);
}

// ------------------------------------------------------------------------------------------------
// Pin configure - interrupt generation conditions - Grouped interrupt
//	Sens (for whole group!):
// 	0x00	- level - low
// 	0x11	- level - high
// 	0x01	- edge - low to high
// 	0x10	- edge - high to low
// return is pointer to GINT
void *_Chip_GPIO_GINT_Conf(uint8_t Group, uint32_t Port, uint32_t Pin, uint8_t Sens)
{
	_CHIP_GINT_T *pPeri;
	
	// Check:
	if(Group > 1) return(NULL);														// LPC546xx has group 0,1 only
	
	switch(Group)
	{
		case 1: pPeri = GINT1; break;
		case 0: pPeri = GINT0; break;
		default: return(NULL);
	}
	
	// Clock:
	SYSCON->AHBCLKCTRL[0] |= SYSCON_AHBCLKCTRL_GINT(1);								// Enable system clock for GINT

	// Reset:
	
	pPeri->CTRL &= ~(GINT_CTRL_COMB(1));											// GPIO interrupt OR(0)/AND(1) mode bit - set OR condition
	
	switch(Sens)
	{
		case 0x00: 
			{
				pPeri->PORT_POL[Port] &= ~(1 << Pin);								// set polarity to 0
				pPeri->CTRL |= GINT_CTRL_TRIG(1);									// select LEVEL mode
				break;
			}
		case 0x01:
			{
				pPeri->PORT_POL[Port] |= (1 << Pin);								// set polarity to 1
				pPeri->CTRL &= ~(GINT_CTRL_TRIG(1));								// select EDGE mode
				break;
			}
		case 0x10:
			{
				pPeri->PORT_POL[Port] &= ~(1 << Pin);								// set polarity to 0
				pPeri->CTRL &= ~(GINT_CTRL_TRIG(1));								// select EDGE mode
				break;
			}
		case 0x11: 
			{
				pPeri->PORT_POL[Port] |= (1 << Pin);								// set polarity to 1
				pPeri->CTRL |= GINT_CTRL_TRIG(1);									// select LEVEL mode
				break;
			}
		default: return(NULL);
	}

	// Clear IRQ:
	// Enable IRQ:
	
	pPeri->PORT_ENA[Port] |= 1 << Pin;												// Enable Pin Interrupt
	pPeri->CTRL |= 1 << Pin;														// clear pending Interrupt
	
	if(Group == 0)
	{
		NVIC_ClearPendingIRQ(GINT0_IRQn);											// clear pending interrupt flag
		NVIC_EnableIRQ(GINT0_IRQn);													// Enable GINT0 interrupt
	}
	else
	{
		NVIC_ClearPendingIRQ(GINT1_IRQn);											// clear pending interrupt flag
		NVIC_EnableIRQ(GINT1_IRQn);													// Enable GINT1 interrupt
	}
	
	return(pPeri);
}

// ------------------------------------------------------------------------------------------------
// Pin configure - enable/disable interrupt generation from selectet Port/Pin
bool _Chip_GPIO_Enable_IRQ_NVIC(void *pPeri, uint32_t Port, uint32_t Pin, bool NewState)
{
	if(NewState == true)
	{
		NVIC_ClearPendingIRQ (GINT0_IRQn);											// Clear NVIC interrupt flag
		NVIC_EnableIRQ(GINT0_IRQn);													// Enable NVIC interrupt
	}
	else
	{
		NVIC_DisableIRQ((IRQn_Type) (GINT0_IRQn));									// Disable NVIC interrupt
	}
	return(true);
}





// ------------------------------------------------------------------------------------------------
// I2C Functions 
// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------
// return periphery index 
int32_t _Chip_I2C_Get_PeriIndex(void *pPeri)										// Return Index from pointer
{
	if((_CHIP_I2C_T *) pPeri == I2C9) return(9);
	if((_CHIP_I2C_T*) pPeri == I2C8) return(8);
	if((_CHIP_I2C_T*) pPeri == I2C7) return(7);
	if((_CHIP_I2C_T*) pPeri == I2C6) return(6);
	if((_CHIP_I2C_T*) pPeri == I2C5) return(5);
	if((_CHIP_I2C_T*) pPeri == I2C4) return(4);
	if((_CHIP_I2C_T*) pPeri == I2C3) return(3);
	if((_CHIP_I2C_T*) pPeri == I2C2) return(2);
	if((_CHIP_I2C_T*) pPeri == I2C1) return(1);
	if((_CHIP_I2C_T*) pPeri == I2C0) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------------
// I2C(Num) Initialization
// result is true:successfull otherwise false
// PerinIndex is FLEXCOMM ID!
static uint8_t _Chip_I2C_FX[_CHIP_FLEXCOMM_COUNT];

void *_Chip_I2C_Init(int32_t PeriIndex)
{
	_CHIP_FLEXCOMM_T *FXPtr;

	// Check + Power + Clock + Reset:
	uint32_t FXIndex = _Chip_FlexComm_Get_Free(PeriIndex, FLEXCOMM_PERIPH_I2C);		// Get free flexcomm device for I2C
	FXPtr = _Chip_FlexComm_Init(FXIndex);											// Initialize FlexComm(id)
	
	if(_Chip_FlexComm_Change_Mode(FXPtr, FLEXCOMM_PERIPH_I2C, true) == false) return(NULL);	// Remode Flexcomm
	_Chip_I2C_FX[PeriIndex] = FXIndex;												// Store flexcomm index to this PeriIndex

	switch (PeriIndex)
	{
		case 9: if(I2C9->STAT & 0x0001) return((void *) I2C9); else return(NULL);
		case 8: if(I2C8->STAT & 0x0001) return((void *) I2C8); else return(NULL);
		case 7: if(I2C7->STAT & 0x0001) return((void *) I2C7); else return(NULL);
		case 6: if(I2C6->STAT & 0x0001) return((void *) I2C6); else return(NULL);
		case 5: if(I2C5->STAT & 0x0001) return((void *) I2C5); else return(NULL);
		case 4: if(I2C4->STAT & 0x0001) return((void *) I2C4); else return(NULL);
		case 3: if(I2C3->STAT & 0x0001) return((void *) I2C3); else return(NULL); 
		case 2: if(I2C2->STAT & 0x0001) return((void *) I2C2); else return(NULL);
		case 1: if(I2C1->STAT & 0x0001) return((void *) I2C1); else return(NULL);
		case 0: if(I2C0->STAT & 0x0001) return((void *) I2C0); else return(NULL);
		default: return(NULL);
	}

}

// ------------------------------------------------------------------------------------------------
// Set SCL Low and High level time
void _Chip_I2CM_Set_DutyCycle(void *pPeri, uint16_t sclH, uint16_t sclL)
{
	((_CHIP_I2C_T*)pPeri)->MSTTIME = (((sclH - 2) & 0x07) << 4) | ((sclL - 2) & 0x07);
}

// ------------------------------------------------------------------------------------------------
// Set clock divisor
void _Chip_I2C_Set_ClockDiv(void *pPeri, uint32_t clkdiv)
{
	if ((clkdiv >= 1) && (clkdiv <= 0x10000)) ((_CHIP_I2C_T*)pPeri)->CLKDIV = clkdiv - 1;
}


// ------------------------------------------------------------------------------------------------
// Read I2C status
uint32_t _Chip_I2C_Get_Peri_Status(void *pPeri)
{
	uint32_t Result = 0;
	uint32_t i2cstat = ((_CHIP_I2C_T*)pPeri)->STAT;						// read chip i2c status
	
	switch((i2cstat >> 1) & 0x07)
	{
		case 0x01: Result |= CHIP_I2C_MSSTATE_RX; break;				// RX Ready
		case 0x02: Result |= CHIP_I2C_MSSTATE_TX; break;				// TX Ready
		case 0x03: Result |= CHIP_I2C_MSSTATE_NACK_ADR; break;			// Addres non ACK
		case 0x04: Result |= CHIP_I2C_MSSTATE_NACK_DATA; break; 		// Data non ACK
		
	}
	if (i2cstat & (1 << 4)) Result |= CHIP_I2C_IRQSTAT_MSARBLOSS;		// Master Arbitration Loss Interrupt Status Bit
	if (i2cstat & (1 << 6)) Result |= CHIP_I2C_IRQSTAT_MSSTSTPERR;		// Start/Stop error

	return(Result);
}


// ------------------------------------------------------------------------------------------------
// Clear I2C status - clear by disable and reenable selected interrupts.
bool _Chip_I2C_Clear_Peri_Status(void *pPeri, uint32_t NewValue)
{
	((_CHIP_I2C_T*)pPeri)->STAT |= NewValue;
	return(true);
}


// ------------------------------------------------------------------------------------------------
// Read I2C MASTER interrupt status
uint32_t _Chip_I2CMST_Get_IRQ_Status(void *pPeri)
{
	uint32_t Result = 0;
	uint32_t rdstat = ((((_CHIP_I2C_T*)pPeri)->INTSTAT) & (((_CHIP_I2C_T *)pPeri)->INTENSET));	// read chip interrupt status
	
	switch (rdstat) // & 0xf8)														// recode chip status to CHAL simples status. See UM10470, Table 512
	{
		case (1 << 4): Result |= CHIP_I2C_IRQSTAT_MSARBLOSS; break;					// Master Arbitration Loss Interrupt Status Bit
		case (1 << 6): Result |= CHIP_I2C_IRQSTAT_MSSTSTPERR; break;				// Master Start Stop Error Interrupt Status Bit
		case (1 << 0): Result |= CHIP_I2C_IRQSTAT_MSSTATE_CHANGED; break;			// Master Pending Interrupt Status Bit
			
		default:  Result = 0;
	}
	return(Result);
}

// ------------------------------------------------------------------------------------------------
// Read I2C SLAVE interrupt status
uint32_t _Chip_I2CSLV_Get_IRQ_Status(void *pPeri)
{
	uint32_t Result = 0;
	uint32_t rdstat = ((((_CHIP_I2C_T*)pPeri)->INTSTAT) & (((_CHIP_I2C_T *)pPeri)->INTENSET));	// read chip interrupt status
	
	switch (rdstat) // & 0xf8)														// recode chip status to CHAL simples status. See UM10470, Table 512
	{
		case (1 << 15): Result |= CHIP_I2C_IRQSTAT_SLVDESEL; break;					// Slave Deselect Interrupt Status Bit
		case (1 << 11): Result |= CHIP_I2C_IRQSTAT_SLVNOTSTR; break;				// Slave not stretching Clock Interrupt Status Bit
		case (1 << 8): Result |= CHIP_I2C_IRQSTAT_SLVPENDING; break;				// Slave Pending Interrupt Status Bit
			
		default:  Result = 0;
	}
	return(Result);
}

// ------------------------------------------------------------------------------------------------
// Read I2C MONITOR interrupt status
uint32_t _Chip_I2CMON_Get_IRQ_Status(void *pPeri)
{
	uint32_t Result = 0;
	uint32_t rdstat = ((((_CHIP_I2C_T*)pPeri)->INTSTAT) & (((_CHIP_I2C_T *)pPeri)->INTENSET));	// read chip interrupt status
	
	switch (rdstat) // & 0xf8)														// recode chip status to CHAL simples status. See UM10470, Table 512
	{
		case (1 << 16): Result |= CHIP_I2C_IRQSTAT_MONRDY; break;					// Monitor Ready Interrupt Status Bit
		case (1 << 17): Result |= CHIP_I2C_IRQSTAT_MONOV; break;					// Monitor Overflow Interrupt Status Bit
		case (1 << 19): Result |= CHIP_I2C_IRQSTAT_MONIDLE; break;					// Monitor Idle Interrupt Status Bit
			
		default:  Result = 0;
	}
	return(Result);
}

// ------------------------------------------------------------------------------------------------
// Read I2C TIMEOUT interrupt status
uint32_t _Chip_I2CTimeout_Get_IRQ_Status(void *pPeri)
{
	uint32_t Result = 0;
	uint32_t rdstat = ((((_CHIP_I2C_T*)pPeri)->INTSTAT) & (((_CHIP_I2C_T *)pPeri)->INTENSET));	// read chip interrupt status
	
	switch (rdstat) // & 0xf8)														// recode chip status to CHAL simples status. See UM10470, Table 512
	{
		case (1 << 25): Result |= CHIP_I2C_IRQSTAT_SCLTIMEOUT; break;				// SCL Timeout Interrupt Status Bit
		case (1 << 24): Result |= CHIP_I2C_IRQSTAT_EVENTTIMEOUT; break;				// Event Timeout Interrupt Status Bit
			
		default:  Result = 0;
	}
	return(Result);
}

// ------------------------------------------------------------------------------------------------
// Clear I2C MASTER interrupt status
bool _Chip_I2CMST_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Clear I2C SLAVE interrupt status
bool _Chip_I2CSLV_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Clear I2C MONITOR interrupt status
bool _Chip_I2CMON_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Clear I2C Timeout interrupt status
bool _Chip_I2CTimeout_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
	((_CHIP_I2C_T*)pPeri)->STAT |= (1<<24);											// clear ststus flags
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Enable/disable selected MASTER interrupts
bool _Chip_I2CMST_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_I2C_T*)pPeri)->INTENSET = IRQBitMask & 0x7f;
	else ((_CHIP_I2C_T*)pPeri)->INTENCLR = IRQBitMask  & 0x7f;
	
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Enable/disable selected SLAVE interrupts
bool _Chip_I2CSLV_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_I2C_T*)pPeri)->INTENSET = IRQBitMask & 0x8900;
	else ((_CHIP_I2C_T*)pPeri)->INTENCLR = IRQBitMask  & 0x8900;
	
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Enable/disable selected MONITOR interrupts
bool _Chip_I2CMON_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_I2C_T*)pPeri)->INTENSET = IRQBitMask & 0x000B0000;
	else ((_CHIP_I2C_T*)pPeri)->INTENCLR = IRQBitMask  & 0x000B0000;
	
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Enable/disable selected Timeout interrupts
bool _Chip_I2CTimeout_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_I2C_T*)pPeri)->INTENSET = IRQBitMask & 0x03000000;
	else ((_CHIP_I2C_T*)pPeri)->INTENCLR = IRQBitMask  & 0x03000000;
	
	return(true);
}
// ------------------------------------------------------------------------------------------------
// I2C - write data into I2C in MASTER morde 
void _Chip_I2CMST_Put_Data(void *pPeri, uint32_t NewValue)
{
	((_CHIP_I2C_T *)pPeri)->MSTDAT = (uint32_t) NewValue;							// write data to I2C
}

// ------------------------------------------------------------------------------------------------
// I2C - Read Data in MASTER morde 
uint32_t _Chip_I2CMST_Get_Data(void *pPeri)
{
	return(((_CHIP_I2C_T *)pPeri)->MSTDAT);											// read data from I2C
}



// ------------------------------------------------------------------------------------------------
// I2C - Create START condition 
void _Chip_I2CMST_Set_Start(void *pPeri)								// create start impulse
{
	((_CHIP_I2C_T *)pPeri)->MSTCTL = I2C_MSTCTL_MSTSTART(1);
}


// ------------------------------------------------------------------------------------------------
// I2C - Create STOP condition
void _Chip_I2CMST_Set_Stop(void *pPeri)
{
	((_CHIP_I2C_T *)pPeri)->MSTCTL = I2C_MSTCTL_MSTSTOP(1);							// create stop impulse
}

// ------------------------------------------------------------------------------------------------
// I2C - Master Continue
void _Chip_I2CMST_Set_Cont(void *pPeri)
{
	((_CHIP_I2C_T *)pPeri)->MSTCTL = I2C_MSTCTL_MSTCONTINUE(1);						// ACK
}

// ------------------------------------------------------------------------------------------------
// I2C - Reset comm.
void _Chip_I2CMST_Reset(void *pPeri)
{	// LPC546xx has no normal reset. Better solution is disable and reenable I2C periphery.
	((_CHIP_I2C_T *)pPeri)->CFG &= ~(1 << 0);										// Disable I2C
	((_CHIP_I2C_T *)pPeri)->CFG |= (1 << 0);										// Enable I2C
}


//// ------------------------------------------------------------------------------------------------------
//// I2C Enable/disable
//// result is true:successfull otherwise false
//bool _Chip_I2C_Enable(void *pPeri, bool NewState)
//{
//	if(pPeri == NULL ) return(false);
//	
//	if(NewState)
//	{
//		((_CHIP_I2C_T*) pPeri)->CFG |= I2C_CFG_MSTEN(1) | I2C_CFG_SLVEN(1) | I2C_CFG_MONEN(1);	// enable all functions
//	}
//	else
//	{
//		((_CHIP_I2C_T*) pPeri)->CFG &= ~(I2C_CFG_MSTEN(1) | I2C_CFG_SLVEN(1) | I2C_CFG_MONEN(1));	// disable all functions
//	}
//	return(true);
//}

// ------------------------------------------------------------------------------------------------------
// I2C Master Enable/disable
// result is true:successfull otherwise false
bool _Chip_I2CMST_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	
	if(NewState)
	{
		((_CHIP_I2C_T*) pPeri)->CFG |= I2C_CFG_MSTEN(1);							// enable master functions
	}
	else
	{
		((_CHIP_I2C_T*) pPeri)->CFG &= ~I2C_CFG_MSTEN(1);							// disable Master functions
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// I2C Slave Enable/disable
// result is true:successfull otherwise false
bool _Chip_I2CSLV_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	
	if(NewState)
	{
		((_CHIP_I2C_T*) pPeri)->CFG |= I2C_CFG_SLVEN(1);							// enable Slave functions
	}
	else
	{
		((_CHIP_I2C_T*) pPeri)->CFG &= ~I2C_CFG_SLVEN(1);							// disable Slave functions
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// I2C Monitor Enable/disable
// result is true:successfull otherwise false
bool _Chip_I2CMON_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	
	if(NewState)
	{
		((_CHIP_I2C_T*) pPeri)->CFG |= I2C_CFG_MONEN(1);							// enable Monitor functions
	}
	else
	{
		((_CHIP_I2C_T*) pPeri)->CFG &= ~I2C_CFG_MONEN(1);							// disable Monitor functions
	}
	return(true);
}


///*!
// * brief Sets the I2C bus frequency for master transactions.
// *
// * The I2C master is automatically disabled and re-enabled as necessary to configure the baud
// * rate. Do not call this function during a transfer, or the transfer is aborted.
// *
// * param base The I2C peripheral base address.
// * param srcClock_Hz I2C functional clock frequency in Hertz.
// * param baudRate_Bps Requested bus frequency in bits per second.
// */
//// ------------------------------------------------------------------------------------------------
//// I2C Set I2C interface speed in kHz
//void _Chip_I2C_SetBusSpeed(void *pPeri, uint32_t Speed_kHz)
//{
//    uint32_t scl, divider;
//    uint32_t err, best_err;
//    uint32_t best_scl = 0U;
//    uint32_t best_div = 0U;
//	uint32_t srcClock_Hz = _Chip_Clock_Get_FlexCommClk_Rate(_Chip_I2C_FX[_Chip_FlexComm_Get_PeriIndex(pPeri)]);

//	Speed_kHz *= 1000;																// change to Hz
//	
//    /*
//     * RFT1717/RFT1437: workaround for hardware bug when using DMA
//     * I2C peripheral clock frequency has to be fixed at 8MHz
//     * source clock is 32MHz or 48MHz so divider is a round integer value
//     */
//    best_div = srcClock_Hz / 8000000U;
//    best_scl = 8000000U / (2U * Speed_kHz);

//    if (((8000000U / (2U * best_scl)) - Speed_kHz) > (Speed_kHz - (8000000U / (2U * (best_scl + 1U)))))
//    {
//        best_scl = best_scl + 1U;
//    }

//    /*
//     * If master SCL frequency does not fit in workaround range, fallback to
//     * usual baudrate computation method
//     */
//    if ((best_scl > 9U) || ((best_scl < 2U)))
//    {

//        best_err = 0U;

//        for (scl = 9U; scl >= 2U; scl--)
//        {
//            /* calculated ideal divider value for given scl, round up the result */
//            divider = ((srcClock_Hz * 10U) / (Speed_kHz * scl * 2U) + 5U) / 10U;

//            /* adjust it if it is out of range */
//            divider = (divider > 0x10000U) ? 0x10000U : divider;

//            /* calculate error */
//            err = srcClock_Hz - (Speed_kHz * scl * 2U * divider);
//            if ((err < best_err) || (best_err == 0U))
//            {
//                best_div = divider;
//                best_scl = scl;
//                best_err = err;
//            }

//            if ((err == 0U) || (divider >= 0x10000U))
//            {
//                /* either exact value was found
//                   or divider is at its max (it would even greater in the next iteration for sure) */
//                break;
//            }
//        }
//    }
//    ((_CHIP_I2C_T*) pPeri)->CLKDIV  = I2C_CLKDIV_DIVVAL(best_div - 1U);
//    ((_CHIP_I2C_T*) pPeri)->MSTTIME = I2C_MSTTIME_MSTSCLLOW(best_scl - 2U) | I2C_MSTTIME_MSTSCLHIGH(best_scl - 2U);
//}


// ------------------------------------------------------------------------------------------------
// I2C Interrupt - enable/disable
bool _Chip_I2CMST_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	int32_t PeriIndex = _Chip_I2C_Get_PeriIndex(pPeri);
	
	if(PeriIndex < 0) return (false);
	
	if(NewState == true)
	{
		NVIC_ClearPendingIRQ (IRQ_FlexComm_Array[_Chip_I2C_FX[PeriIndex]]);			// Clear NVIC interrupt flag
		NVIC_EnableIRQ(IRQ_FlexComm_Array[_Chip_I2C_FX[PeriIndex]]);				// Enable NVIC interrupt
	}
	else
	{
		NVIC_DisableIRQ(IRQ_FlexComm_Array[_Chip_I2C_FX[PeriIndex]]);				// Enable NVIC interrupt
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------
// I2C Slave Interrupt - enable/disable
bool _Chip_I2CSLV_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	return(_Chip_I2CMST_Enable_IRQ_NVIC(pPeri, NewState));							// this MCU used same IRQ for all modes
}

// ------------------------------------------------------------------------------------------------
// I2C Monitor Interrupt - enable/disable
bool _Chip_I2CMON_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	return(_Chip_I2CMST_Enable_IRQ_NVIC(pPeri, NewState));							// this MCU used same IRQ for all modes
}

// ------------------------------------------------------------------------------------------------
// I2C Interrupt - enable/disable
bool _Chip_I2CTimeout_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	return(_Chip_I2CMST_Enable_IRQ_NVIC(pPeri, NewState));							// this MCU used same IRQ for all modes

}

// ------------------------------------------------------------------------------------------------
// I2C Set I2C interface speed in kHz
// result is true:successfull otherwise false
bool _Chip_I2C_Set_BusSpeed(void *pPeri, uint32_t Speed_kHz)						// Max Speed: 100kHz, 400kHz, 1MHz
{
	uint32_t scl;
	uint32_t CLKDiv;
	uint32_t FlexId = _Chip_I2C_Get_PeriIndex(pPeri);
	uint32_t clkin = _Chip_Clock_Get_FlexCommClk_Rate(FlexId);						// get flexcommclk freq
	
	CLKDiv = 1;																		// start at divider = 1
	
	if(clkin > 48000000) return(false);												// se UM10912,p.436: Remark: The Flexcomm Interface function clock frequency should not be above 48 MHz.
	
	do
	{ 
		_Chip_I2C_Set_ClockDiv((_CHIP_I2C_T*)pPeri, CLKDiv);						// set new peripherial divider
		//((_CHIP_I2C_T*)pPeri)->CLKDIV = (CLKDiv - 1) & 0x0000ffff;
		
		scl = clkin / ((((_CHIP_I2C_T*)pPeri)->CLKDIV + 1) * (Speed_kHz * 1000));
		CLKDiv ++;
		if(CLKDiv > 65535) return(false);
	}while((scl < 1) || (scl > 8));													// recalculate while scl is out of range
	_Chip_I2C_Set_ClockDiv((_CHIP_I2C_T*)pPeri, ++CLKDiv);							// set new peripherial divider set smaller speed because Speed_kHz haven't be reached!
	_Chip_I2CM_Set_DutyCycle((_CHIP_I2C_T*)pPeri, (scl >> 1), (scl - (scl >> 1)));
	return(true);
}


// ------------------------------------------------------------------------------------------------
// I2C Get I2C interface speed in kHz
uint32_t _Chip_I2C_Get_BusSpeed(void *pPeri)										// Return Current bus speed
{
	uint32_t FlexId = _Chip_I2C_Get_PeriIndex(pPeri);

	uint32_t SCLL = (((_CHIP_I2C_T*)pPeri)->MSTTIME & 0x000007) + 2;				// get SCL Low
	uint32_t SCLH = ((((_CHIP_I2C_T*)pPeri)->MSTTIME & 0x000070) >> 4) + 2;			// get SCL High
	uint32_t res = (_Chip_Clock_Get_FlexCommClk_Rate(FlexId) / ((_CHIP_I2C_T*)pPeri)->CLKDIV + 1) / (SCLL + SCLH); // get in Hz
	return(res / 1000); //return in kHz
}


// ------------------------------------------------------------------------------------------------------
// I2C Configure I2C
// result is true:successfull otherwise false
bool _Chip_I2C_Configure(void *pPeri, uint32_t Speed_kHz, bool MasterMode, bool SlaveMode, bool MonitorMode, uint16_t Address)
{
	bool res = false;
	
	if(pPeri == NULL) return(false);
	
//	26.7.1.1 Rate calculations
//		SCL high time (in I2C function clocks) = (CLKDIV + 1) * (MSTSCLHIGH + 2)
//		SCL low time (in I2C function clocks) = (CLKDIV + 1) * (MSTSCLLOW + 2)
//		Nominal SCL rate = I2C function clock rate / (SCL high time + SCL low time)

	if (MasterMode == true)
	{
		res = _Chip_I2C_Set_BusSpeed(pPeri, Speed_kHz);
		if (Speed_kHz > 400) ((_CHIP_I2C_T*)pPeri)->CFG = (1 << 5);					// Set HS Cabable and clear Master, Slave and Monitor enable
		else ((_CHIP_I2C_T*)pPeri)->CFG = 0;										// clear all enable
		
		((_CHIP_I2C_T*)pPeri)->CFG |= 0x00000008;									// EVENTTIMEOUT enable
	}
	
	return(res);
}


// ************************************************************************************************
// UART  Functions 
// ************************************************************************************************
static uint8_t _Chip_UART_FX[_CHIP_FLEXCOMM_COUNT];

// ------------------------------------------------------------------------------------------------
// return periphery index 
int32_t _Chip_UART_Get_PeriIndex(void *pPeri)							// Return Index from pointer
{
	if((_CHIP_UART_T *) pPeri == USART9) return(9);
	if((_CHIP_UART_T*) pPeri == USART8) return(8);
	if((_CHIP_UART_T*) pPeri == USART7) return(7);
	if((_CHIP_UART_T*) pPeri == USART6) return(6);
	if((_CHIP_UART_T*) pPeri == USART5) return(5);
	if((_CHIP_UART_T*) pPeri == USART4) return(4);
	if((_CHIP_UART_T*) pPeri == USART3) return(3);
	if((_CHIP_UART_T*) pPeri == USART2) return(2);
	if((_CHIP_UART_T*) pPeri == USART1) return(1);
	if((_CHIP_UART_T*) pPeri == USART0) return(0);
	return(-1);																		// otherwise error
}
// ------------------------------------------------------------------------------------------------
// ADR DETECTION Control
bool _Chip_UART_Set_AdrDet(void* pPeri, bool NewState)
{
	if(NewState)
	{
		((_CHIP_UART_T *)pPeri)->CFG |= USART_CFG_AUTOADDR(1);						// AUTOADDR bit set
		((_CHIP_UART_T *)pPeri)->CTL |= USART_CTL_ADDRDET(1);						// ADDRDET bit set
	}
	else 
	{
		((_CHIP_UART_T *)pPeri)->CFG &= ~(USART_CFG_AUTOADDR(1));					// AUTOADDR bit clear
		((_CHIP_UART_T *)pPeri)->CTL &= ~(USART_CTL_ADDRDET(1));					// ADDRDET bit clear
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Write New Address 
// result is true:successfull otherwise false
bool _Chip_UART_Set_Address(void* pPeri, uint8_t NewAddress)
{
	((_CHIP_UART_T *)pPeri)->ADDR = NewAddress;										// Save address
	
	if(NewAddress)
	{
		_Chip_UART_Set_AdrDet(pPeri, true);											// activate ADRDET
	}
	else
	{
		_Chip_UART_Set_AdrDet(pPeri, false);										// deactivate ADRDET
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Read Address 
uint8_t _Chip_UART_Get_Address(void* pPeri)
{
	return(((_CHIP_UART_T *)pPeri)->ADDR & 0x00ff);
}

// ------------------------------------------------------------------------------------------------
// Read UART status
uint32_t _Chip_UART_Get_Peri_Status(void *pPeri)
{
	return(((_CHIP_UART_T*)pPeri)->STAT);
}


// ------------------------------------------------------------------------------------------------
// Clear UART status - clear by disable and reenable selected interrupts.
bool _Chip_UART_Clear_Peri_Status(void *pPeri, uint32_t NewValue)
{
	((_CHIP_UART_T*)pPeri)->STAT |= NewValue;
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Get UART interrupt status
uint32_t _Chip_UART_Get_IRQ_Status(void *pPeri)
{
	return( (((_CHIP_UART_T *)pPeri)->INTSTAT) & (((_CHIP_UART_T *)pPeri)->INTENSET) );	// status masked with Enabled
}

// ------------------------------------------------------------------------------------------------
uint32_t _Chip_UARTFIFO_Get_IRQ_Status(void *pPeri)
{
	return( (((_CHIP_UART_T *)pPeri)->FIFOINTSTAT) & (((_CHIP_UART_T *)pPeri)->FIFOINTENSET) );	// status masked with Enabled
}

// ------------------------------------------------------------------------------------------------
// Clear UART interrupt status - clear 
bool _Chip_UART_Clear_IRQ_Status(void *pPeri,uint32_t IRQBitMask)
{
	((_CHIP_UART_T*)pPeri)->STAT |= IRQBitMask;
	return(true);																	// intstatus is cleared by STAT reg
}

// ------------------------------------------------------------------------------------------------
bool _Chip_UARTFIFO_Clear_IRQ_Status(void *pPeri, uint32_t IRQBitMask)
{
	((_CHIP_UART_T*)pPeri)->FIFOSTAT |= IRQBitMask;
	return(true);
}

// ------------------------------------------------------------------------------------------------
void _Chip_UART_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState)		// Enable/Disable UARTs Interrupts 
{
	if(NewState) ((_CHIP_UART_T*)pPeri)->INTENSET = IRQBitMask;
	else ((_CHIP_UART_T*)pPeri)->INTENCLR = IRQBitMask;
}

// ------------------------------------------------------------------------------------------------
void _Chip_UARTFIFO_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState)	// Enable/Disable UARTs FIFO Interrupts 
{
	if(NewState) ((_CHIP_UART_T*)pPeri)->FIFOINTENSET = IRQBitMask;
	else ((_CHIP_UART_T*)pPeri)->FIFOINTENCLR = IRQBitMask;
}

// ------------------------------------------------------------------------------------------------
// Read UART FIFO status
uint32_t _Chip_UARTFIFO_Get_Peri_Status(void *pPeri)
{
	return(((_CHIP_UART_T*)pPeri)->FIFOSTAT);
}


// ------------------------------------------------------------------------------------------------
// Clear UART FIFO status - clear by disable and reenable selected interrupts.
bool _Chip_UARTFIFO_Clear_Peri_Status(void *pPeri, uint32_t NewValue)
{
	((_CHIP_UART_T*)pPeri)->FIFOSTAT |= NewValue;
	return(true);
}


// ------------------------------------------------------------------------------------------------
// Write char to OutBuff
void _Chip_UART_Put_Data(void *pPeri, uint32_t WrData)
{
	((_CHIP_UART_T *)pPeri)->FIFOWR = WrData & USART_FIFOWR_TXDATA_MASK;			// write data to UART - 9-bit value
}

// ------------------------------------------------------------------------------------------------
// Read char from InBuff
uint32_t _Chip_UART_Get_Data(void *pPeri)
{
	return(((_CHIP_UART_T *)pPeri)->FIFORD & 0x1ff);								// read data from UART - 9-bit value
}

// ------------------------------------------------------------------------------------------------
// Read actaul value of FIFO Tx threshold
uint32_t _Chip_UARTFIFO_Get_TxTHLevel(void *pPeri)
{
	return(((((_CHIP_UART_T *)pPeri)->FIFOTRIG >> 8) & 0x0f) + 1);					// read TXLVL 
}

// ------------------------------------------------------------------------------------------------
// Read actaul value of FIFO Rx threshold
uint32_t _Chip_UARTFIFO_Get_RxTHLevel(void *pPeri)
{
	return(((((_CHIP_UART_T *)pPeri)->FIFOTRIG >> 16) & 0x0f) + 1);					// read RXLVL 
}

// ------------------------------------------------------------------------------------------------
// Write new value of FIFO Tx Threshold
void _Chip_UARTFIFO_Set_TxTHLevel(void *pPeri, uint32_t NewValue)
{
	uint32_t RegVal = ((_CHIP_UART_T *)pPeri)->FIFOTRIG;
	RegVal &= ~(0x0f << 8);															// Clear old setting
	
	((_CHIP_UART_T *)pPeri)->FIFOTRIG &= ~(0x0f << 8);								// Clear old setting
	
	if(NewValue) ((_CHIP_UART_T *)pPeri)->FIFOTRIG |= RegVal | ((NewValue-1 & 0x0f) << 8) | USART_FIFOTRIG_TXLVLENA(1);	// write new level and enable
	else ((_CHIP_UART_T *)pPeri)->FIFOTRIG &= ~USART_FIFOTRIG_TXLVLENA(1);			// NewValue = 0. Disable TH

}

// ------------------------------------------------------------------------------------------------
// Write new value of FIFO Rx Threshold
void _Chip_UARTFIFO_Set_RxTHLevel(void *pPeri, uint32_t NewValue)
{
	uint32_t RegVal = ((_CHIP_UART_T *)pPeri)->FIFOTRIG;
	RegVal &= ~(0x0f << 16);														// Clear old setting
	
	((_CHIP_UART_T *)pPeri)->FIFOTRIG &= ~(0x0f << 16);								// Clear old setting
	
	if(NewValue) ((_CHIP_UART_T *)pPeri)->FIFOTRIG |= RegVal | ((NewValue-1 & 0x0f) << 16) | USART_FIFOTRIG_RXLVLENA(1);	// write new level and enable
	else ((_CHIP_UART_T *)pPeri)->FIFOTRIG &= ~USART_FIFOTRIG_RXLVLENA(1);			// NewValue = 0. Disable TH
}

// ------------------------------------------------------------------------------------------------
// Flush Uart Tx
void _Chip_UART_Flush_Tx(void *pPeri)
{
	((_CHIP_UART_T *)pPeri)->FIFOCFG |= USART_FIFOCFG_EMPTYTX_MASK;						// Flush Tx FIFO
}

// ------------------------------------------------------------------------------------------------
// Flush Uart Rx
void _Chip_UART_Flush_Rx(void *pPeri)
{
	((_CHIP_UART_T *)pPeri)->FIFOCFG |= USART_FIFOCFG_EMPTYRX_MASK;						// Flush Rx FIFO
}



// ------------------------------------------------------------------------------------------------------
// UART(Num) Initialization
// return is pointer to periphery, otherwise NULL
void *_Chip_UART_Init(int32_t PeriIndex)
{
	_CHIP_UART_T * pPeri;
	_CHIP_FLEXCOMM_T *FXPtr;

	uint32_t FXIndex = _Chip_FlexComm_Get_Free(PeriIndex, FLEXCOMM_PERIPH_USART);	// Get free flexcomm device for UART
	FXPtr = _Chip_FlexComm_Init(FXIndex);											// Initialize FlexComm(id)
	
	// Check + Power + Clock + Reset
	if(_Chip_FlexComm_Change_Mode(FXPtr, FLEXCOMM_PERIPH_USART, true) == false) return(NULL);// Remode Flexcomm
	_Chip_UART_FX[PeriIndex] = FXIndex;												// Store flexcomm index to this PeriIndex

	switch (PeriIndex)
	{
		case 9: pPeri = USART9; break;
		case 8: pPeri = USART8; break;
		case 7: pPeri = USART7; break;
		case 6: pPeri = USART6; break;
		case 5: pPeri = USART5; break;
		case 4: pPeri = USART4; break;
		case 3: pPeri = USART3; break;
		case 2: pPeri = USART2; break;
		case 1: pPeri = USART1; break;
		case 0: pPeri = USART0; break;
		default: pPeri = NULL;
	}
	
	
	((_CHIP_UART_T *)pPeri)->FIFOCFG = USART_FIFOCFG_EMPTYTX_MASK | USART_FIFOCFG_EMPTYRX_MASK;			// clear FIFOs
	((_CHIP_UART_T *)pPeri)->FIFOTRIG = (1 << 8) | USART_FIFOTRIG_TXLVLENA_MASK | USART_FIFOTRIG_RXLVLENA_MASK;	// set trigger for 1 char and enable both
	return(pPeri);
}

// ------------------------------------------------------------------------------------------------
// UART Interrupt - enable/disable
bool _Chip_UART_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	int32_t PeriIndex = _Chip_UART_Get_PeriIndex(pPeri);

	// Check:
	SYS_ASSERT( pPeri != NULL);														// check
	
	if(NewState == true)
	{
		NVIC_ClearPendingIRQ (IRQ_FlexComm_Array[_Chip_UART_FX[PeriIndex]]);		// Clear NVIC interrupt flag
		NVIC_EnableIRQ(IRQ_FlexComm_Array[_Chip_UART_FX[PeriIndex]]);				// Enable NVIC interrupt
	}
	else
	{
		NVIC_DisableIRQ(IRQ_FlexComm_Array[_Chip_UART_FX[PeriIndex]]);				// Enable NVIC interrupt
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Enable/Disable UART
bool _Chip_UART_Enable(void *pPeri, bool NewState)
{	
//	// Check:
	SYS_ASSERT(pPeri != NULL);														// check
	//if(pPeri == NULL ) return(false);

	int32_t PeriIndex = _Chip_FlexComm_Get_PeriIndex(pPeri);

	//_Chip_UART_Enable_IRQ_NVIC(pPeri, NewState);									// Interrupt
	
	if(NewState == true)
	{
		uint16_t waitfor = 5000;
		do
		{
			if(waitfor) waitfor --;
			else 
			{
				if((((_CHIP_UART_T *)pPeri)->CFG & 0x01) == NewState) return(true);	// Change was accepted?
				else return(false);
			}
		}while((((_CHIP_UART_T *)pPeri)->STAT & (USART_STAT_RXIDLE(1) | USART_STAT_TXIDLE(1))) != (USART_STAT_RXIDLE(1) | USART_STAT_TXIDLE(1)));			// wait for ones
		
		((_CHIP_UART_T *)pPeri)->CFG |= USART_CFG_ENABLE_MASK;						// Enable UART
		((_CHIP_UART_T *)pPeri)->FIFOCFG |= USART_FIFOCFG_ENABLERX_MASK | USART_FIFOCFG_ENABLETX_MASK;		// Enable UART FIFO Rx and Tx
		
		//_Chip_UART_Enable_IRQ(pPeri, USART_FIFOCFG_ENABLERX_MASK, true);			// Enable RX interrupt
		_Chip_UARTFIFO_Enable_IRQ(pPeri, USART_FIFOINTENSET_TXLVL_MASK | USART_FIFOINTENSET_RXLVL_MASK, true);	// Enable RX and Rx FIFO Level Interrupt
		
	}
	else
	{
		((_CHIP_UART_T *)pPeri)->CFG &= ~USART_CFG_ENABLE_MASK;						// Disable UART
		((_CHIP_UART_T *)pPeri)->FIFOCFG &= ~(USART_FIFOCFG_ENABLETX_MASK | USART_FIFOCFG_ENABLERX_MASK);
		//_Chip_UART_Enable_IRQ(pPeri, USART_FIFOCFG_ENABLERX_MASK, false);			// Disable RX interrupt
		//_Chip_UART_Enable_IRQ(pPeri, USART_FIFOCFG_ENABLETX_MASK, false);			// Disable TX interrupt
		_Chip_UARTFIFO_Enable_IRQ(pPeri, USART_FIFOINTENSET_TXLVL_MASK | USART_FIFOINTENSET_RXLVL_MASK, false);	// Disable RX and Rx FIFO Level Interrupt
	}


	//if((((_CHIP_UART_T *)pPeri)->CFG & USART_CFG_ENABLE_MASK) == NewState) return(true);	// Change was accepted?
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Baud Rate settings. In case of overflow, increased divider from MainCLK
// result is true:successfull otherwise false
bool _Chip_UART_Set_Baud(void* pPeri, uint32_t NewBaud)
{
    uint32_t srcClock_Hz = _Chip_Clock_Get_FlexCommClk_Rate(_Chip_FlexComm_Get_PeriIndex(pPeri));
	uint32_t best_diff = (uint32_t)-1, best_osrval = 0xf, best_brgval = (uint32_t)-1;
    uint32_t osrval, brgval, diff, baudrate;

    // check arguments
    if ((pPeri == NULL) || (NewBaud == 0U) || (srcClock_Hz == 0U))
    {
        return (false);
    }

    // If synchronous master mode is enabled, only configure the BRG value.
    if ((((_CHIP_UART_T *)pPeri)->CFG & USART_CFG_SYNCEN_MASK) != 0U)
    {
        if ((((_CHIP_UART_T *)pPeri)->CFG & USART_CFG_SYNCMST_MASK) != 0U)
        {
            brgval    = srcClock_Hz / NewBaud;
            ((_CHIP_UART_T *)pPeri)->BRG = brgval - 1U;
        }
    }
    else
    {
        /*
         * Smaller values of OSR can make the sampling position within a data bit less accurate and may
         * potentially cause more noise errors or incorrect data.
         */
        for (osrval = best_osrval; osrval >= 8U; osrval--)
        {
            brgval = (((srcClock_Hz * 10U) / ((osrval + 1U) * NewBaud)) - 5U) / 10U;
            if (brgval > 0xFFFFU)
            {
                continue;
            }
            baudrate = srcClock_Hz / ((osrval + 1U) * (brgval + 1U));
            diff     = NewBaud < baudrate ? baudrate - NewBaud : NewBaud - baudrate;
            if (diff < best_diff)
            {
                best_diff   = diff;
                best_osrval = osrval;
                best_brgval = brgval;
            }
        }

        /* value over range */
        if (best_brgval > 0xFFFFU)
        {
            return (false);
        }

        ((_CHIP_UART_T *)pPeri)->OSR = best_osrval;
        ((_CHIP_UART_T *)pPeri)->BRG = best_brgval;
    }

    return (true);
}

// ------------------------------------------------------------------------------------------------------
// Baud Rate settings. Get interface speed in baud
// result is true:successfull otherwise false
uint32_t _Chip_UART_Get_Baud(void* pPeri)
{
    uint32_t srcClock_Hz = _Chip_Clock_Get_FlexCommClk_Rate(_Chip_FlexComm_Get_PeriIndex(pPeri));
    uint32_t osrval, brgval, baudrate;

    // check arguments
    if ((pPeri == NULL) || (srcClock_Hz == 0U))
    {
        return (0);
    }

	osrval = ((_CHIP_UART_T *)pPeri)->OSR;
	brgval = ((_CHIP_UART_T *)pPeri)->BRG;

	
	
    // If synchronous master mode is enabled, only configure the BRG value.
    if ((((_CHIP_UART_T *)pPeri)->CFG & USART_CFG_SYNCEN_MASK) != 0U)
    {
        if ((((_CHIP_UART_T *)pPeri)->CFG & USART_CFG_SYNCMST_MASK) != 0U)
        {
			
			baudrate = srcClock_Hz / (brgval + 1U);
            //brgval    = srcClock_Hz / NewBaud;
            ((_CHIP_UART_T *)pPeri)->BRG = brgval - 1U;
        }
    }
    else
    {
		baudrate = srcClock_Hz / ((osrval + 1U) * (brgval + 1U));
    }

    return (baudrate);
}

// ------------------------------------------------------------------------------------------------------
// Write configuration into UART
// DataLen: 7,8,9
// Parity: 0-none, 2-Odd, 3-Even
// Stopbits: 0-1, 1-2
//bool _Chip_UART_ConfigData(void* pPeri, uint8_t DataLen, uint8_t Parity, uint8_t StopBits, const CHAL_Signal_t *sCTS, const CHAL_Signal_t *sRTS)
bool _Chip_UART_Set_Conf(void *pPeri, uint8_t DataLen, uint8_t Parity, uint8_t StopBits)
{
	uint32_t Config;
	
	Config = (((DataLen - 7) << 2) & (3 << 2)) |  ((Parity << 4) & (3 << 4)) | (StopBits & (1 << 6));
	((_CHIP_UART_T *)pPeri)->CFG &= ~0x01;											// disable uart
	((_CHIP_UART_T *)pPeri)->CFG &= ~(0x7c);										// clear old settings
	((_CHIP_UART_T *)pPeri)->CFG |= Config;											// write new one
	
//	if(sCTS) ((_CHIP_UART_T *)pPeri)->CFG |= 1 << 9;								// Enable CTS
//	else ((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 9);									// Disable CTS	
	
	return(true);
}

//// ------------------------------------------------------------------------------------------------------
//// Write configuration into UART for RS485 mode
//bool _Chip_UART_RS485_ConfigData(void* pPeri, uint8_t Address, const CHAL_Signal_t *sDIR)
//{
//	((_CHIP_UART_T *)pPeri)->CFG |= 1 << 18;										// OETA - RS485 turnaround
//	
//	if(sDIR) 
//	{
//		((_CHIP_UART_T *)pPeri)->CFG |= (1 << 20);									// Output Enable selectby RTS
//		if(sDIR->Std.Act == 1) ((_CHIP_UART_T *)pPeri)->CFG |= 1 << 21;				// OEPOLarity depends on Signal Active state active High
//		else ((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 21);							// OEPOLarity depends on Signal Active state active Low
//	}
//	else ((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 20);								// only RTS

//	if(Address)
//	{
//		((_CHIP_UART_T *)pPeri)->ADDR = Address;									// Save address
//		((_CHIP_UART_T *)pPeri)->CFG |= 1 << 19;									// AUTOADDR set
//	}
//	
//	return(true);
//}


// ------------------------------------------------------------------------------------------------------
// Configure RTS as flow control for UART
// result is true:successfull otherwise false
bool _Chip_UART_Set_RTS_Conf(void *pPeri, uint8_t sRTS_Port, uint8_t sRTS_Pin)
{
	((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 18);										// OETA - RS485 OFF
	((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 20);										// Output Disable. Use only RTS

	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Configure CTS as flow control for UART
// result is true:successfull otherwise false
bool _Chip_UART_Set_CTS_Conf(void *pPeri, uint8_t sCTS_Port, uint8_t sCTS_Pin)
{
	((_CHIP_UART_T *)pPeri)->CFG |= 1 << 9;											// Enable CTS
//	else ((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 9);									// Disable CTS	

	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Configure DIRDE as flow control for RS485
// result is true:successfull otherwise false
// LPC17xx used RTS or DTR as OutputEnable
bool _Chip_UART_Set_DIRDE_Conf(void *pPeri, uint8_t DIRDE_Port, uint8_t DIRDE_Pin, bool DIRDE_H_Active)
{
	((_CHIP_UART_T *)pPeri)->CFG |= 1 << 18;										// OETA - RS485 turnaround
	
	((_CHIP_UART_T *)pPeri)->CFG |= (1 << 20);										// Output Enable select by RTS
	if(DIRDE_H_Active == 1) ((_CHIP_UART_T *)pPeri)->CFG |= 1 << 21;				// OEPOLarity depends on Signal Active state active High
	else ((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 21);								// OEPOLarity depends on Signal Active state active Low

	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Configure RE as receive enable control in RS485 mode
// result is true:successfull otherwise false
bool _Chip_UART_Set_RE_Conf(void *pPeri, uint8_t RE_Port, uint8_t RE_Pin, bool RE_H_Active)
{
	return(false);																	// LPC546xx hasn't RE dedicated pin
}



// ******************************************************************************************************
// SPI Functions
// ******************************************************************************************************
static uint8_t _Chip_SPI_FX[_CHIP_FLEXCOMM_COUNT];

// ------------------------------------------------------------------------------------------------
// SPI - Get peripharial index by ptr
// return periphery index 
int32_t _Chip_SPI_Get_PeriIndex(void *pPeri)							// Return Index from pointer
{
	if((_CHIP_SPI_T *) pPeri == SPI9) return(9);
	if((_CHIP_SPI_T*) pPeri == SPI8) return(8);
	if((_CHIP_SPI_T*) pPeri == SPI7) return(7);
	if((_CHIP_SPI_T*) pPeri == SPI6) return(6);
	if((_CHIP_SPI_T*) pPeri == SPI5) return(5);
	if((_CHIP_SPI_T*) pPeri == SPI4) return(4);
	if((_CHIP_SPI_T*) pPeri == SPI3) return(3);
	if((_CHIP_SPI_T*) pPeri == SPI2) return(2);
	if((_CHIP_SPI_T*) pPeri == SPI1) return(1);
	if((_CHIP_SPI_T*) pPeri == SPI0) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------
// SPI(Num) Initialization
// return is pointer to periphery, otherwise NULL
void *_Chip_SPI_Init(int32_t PeriIndex)
{
	_CHIP_FLEXCOMM_T *FXPtr;

	// Check + Power + Clock + Reset:
	uint32_t FXIndex = _Chip_FlexComm_Get_Free(PeriIndex, FLEXCOMM_PERIPH_SPI);		// Get free flexcomm device for I2C
	FXPtr = _Chip_FlexComm_Init(FXIndex);											// Initialize FlexComm(id)
	
	if(_Chip_FlexComm_Change_Mode(FXPtr, FLEXCOMM_PERIPH_SPI, true) == false) return(NULL);	// Remode Flexcomm
	_Chip_SPI_FX[PeriIndex] = FXIndex;												// Store flexcomm index to this PeriIndex

	switch (PeriIndex)
	{
		case 9: return(SPI9);
		case 8: return(SPI8);
		case 7: return(SPI7);
		case 6: return(SPI6);
		case 5: return(SPI5);
		case 4: return(SPI4);
		case 3: return(SPI3);
		case 2: return(SPI2);
		case 1: return(SPI1);
		case 0: return(SPI0);
		default: return(NULL);
	}
}

// ------------------------------------------------------------------------------------------------------
// SPI Enable/disable
// result is true:successfull otherwise false
bool _Chip_SPI_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);	
	
	//_Chip_SPI_Enable_IRQ_NVIC(pPeri, NewState);											// Interrupt
	
	if(NewState == true) 
	{
		((_CHIP_SPI_T *)pPeri)->CFG |= (1 << 0);									// SPI Enable
	}
	else
	{		
		((_CHIP_SPI_T *)pPeri)->CFG &= ~(1 << 0);									// Disable
	}
	
	return (true);
}

// ------------------------------------------------------------------------------------------------
// SPI - Set clock divisor
void _Chip_SPI_Set_ClockDiv(_CHIP_SPI_T *pPeri, uint32_t clkdiv)
{
	if ((clkdiv >= 1) && (clkdiv <= 0x10000)) pPeri->DIV = clkdiv - 1;				// write to CLK DIV register
	else pPeri->DIV = 0;
}

// ------------------------------------------------------------------------------------------------
// SPI Set interface speed in Hz
// result is true:successfull otherwise false
bool _Chip_SPI_Set_BusSpeed(void *pPeri, uint32_t Speed_Hz)
{
	uint32_t CLKDiv;
	
	CLKDiv = (_Chip_Clock_Get_SysClk_Rate() / Speed_Hz) + 1;						// +1: speed maximal to Speed_Hz or LOWer!
	if(CLKDiv > 0x10000) {sys_Err("Cannot set SPI Divider!"); return(false);} 
	_Chip_SPI_Set_ClockDiv((_CHIP_SPI_T*) pPeri, CLKDiv);							// set new peripherial divider
	return(true);
}

// ------------------------------------------------------------------------------------------------
// SPI Get SPI interface speed in kHz
uint32_t _Chip_SPI_Get_BusSpeed(_CHIP_SPI_T *pPeri)									// Return Current bus speed
{
	int32_t FlexId = _Chip_SPI_FX[_Chip_FlexComm_Get_PeriIndex(pPeri)];

	return((_Chip_Clock_Get_FlexCommClk_Rate(FlexId) / 1000) / (pPeri->DIV + 1));	// return in kHz
}

// ------------------------------------------------------------------------------------------------------
// SPI Configure
// result is true:successfull otherwise false
bool _Chip_SPI_Configure(void *pPeri, uint32_t Speed_Hz, bool MasterMode, bool SlaveMode, bool LSB_First, bool CPHA, bool CPOL)
{
	int32_t FlexId = _Chip_SPI_FX[_Chip_SPI_Get_PeriIndex(pPeri)];					// get used Flexcomm ID for this periphery
	uint32_t CLKDiv;
	uint32_t MainCLK = _Chip_Clock_Get_FlexCommClk_Rate(FlexId);					// get clock

	
	if(pPeri == NULL) return(false);
	if((MasterMode == true) && (SlaveMode == true)) return(false);					// only one mode set check....
	if((MasterMode == false) && (SlaveMode == false)) return(false);

	((_CHIP_SPI_T*)pPeri)->CFG = 0;													// clear old config
	
	if(CPHA == true) ((_CHIP_SPI_T*) pPeri)->CFG |= 1 << 4;							// set CPHA 1
	if(CPOL == true) ((_CHIP_SPI_T*) pPeri)->CFG |= 1 << 5;							// set CPOL 1

	
	if (MasterMode == true)
	{
		CLKDiv = (MainCLK / 1000) / Speed_Hz;
		if(CLKDiv > 0x10000) {sys_Err("Cannot set SPI Divider!"); return(false);} 
		
		_Chip_SPI_Set_ClockDiv((_CHIP_SPI_T*)pPeri, CLKDiv);						// set new peripherial divider
		((_CHIP_SPI_T*)pPeri)->CFG |= (1 << 2);										// set MASTER	
	}
	else
	{
		((_CHIP_SPI_T*)pPeri)->CFG &= ~(1 << 2);									// clear MASTER
	}
	
	if(LSB_First) ((_CHIP_SPI_T*)pPeri)->CFG |= (1 << 3);							// set LSBF
	else ((_CHIP_SPI_T*)pPeri)->CFG &= ~(1 << 3);									// clear LSBF
	return(true);
}

// ------------------------------------------------------------------------------------------------
// SPI interrupt enable/disable 
bool _Chip_SPI_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	int32_t PeriIndex = _Chip_SPI_Get_PeriIndex(pPeri);
	
	if(PeriIndex < 0) return (false);

	if(NewState == true)
	{
		NVIC_ClearPendingIRQ (IRQ_FlexComm_Array[_Chip_SPI_FX[PeriIndex]]);			// Clear NVIC interrupt flag
		NVIC_EnableIRQ(IRQ_FlexComm_Array[_Chip_SPI_FX[PeriIndex]]);				// Enable NVIC interrupt
	}
	else
	{
		NVIC_DisableIRQ(IRQ_FlexComm_Array[_Chip_SPI_FX[PeriIndex]]);				// Disable NVIC interrupt
	}
	return(true);
}


// ------------------------------------------------------------------------------------------------
// SPI - write data into SPI datareg
void _Chip_SPI_Put_Data(void *pPeri, uint32_t NewValue, uint8_t NumOfBits, uint32_t CtrlFlags, uint8_t SSel)
{
	((_CHIP_SPI_T *)pPeri)->FIFOWR = (NewValue & 0xffff) | ((~SSel & 0x0f) << 16) | ((NumOfBits & 0x0f) << 24) | (CtrlFlags << 20);
}

// ------------------------------------------------------------------------------------------------
// SPI - Read Data from datareg
uint32_t _Chip_SPI_Get_Data(void *pPeri)
{
	return(((_CHIP_SPI_T *)pPeri)->FIFORD & 0xffff);
}

// ------------------------------------------------------------------------------------------------
// Read SPI interrupt status
uint32_t _Chip_SPI_Get_IRQ_Status(void *pPeri)
{
	return(((_CHIP_SPI_T *)pPeri)->INTSTAT);
}

// ------------------------------------------------------------------------------------------------
// Clear SPI interrupt status
bool _Chip_SPI_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
	((_CHIP_SPI_T *)pPeri)->INTENCLR |= NewValue;
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Read SPI status
uint32_t _Chip_SPI_Get_Peri_Status(void *pPeri)
{
	return(((_CHIP_SPI_T*)pPeri)->STAT);
}


// ------------------------------------------------------------------------------------------------
// clear peri status
bool _Chip_SPI_Clear_Peri_Status(void *pPeri, uint32_t BitMask)
{
	((_CHIP_SPI_T*)pPeri)->STAT |= BitMask;
	return true;
}

// ------------------------------------------------------------------------------------------------
// Enable/disable selected interrupts
bool _Chip_SPI_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_SPI_T*)pPeri)->INTENSET = IRQBitMask;
	else ((_CHIP_SPI_T*)pPeri)->INTENCLR = IRQBitMask;
	
	return(true);

}

// ------------------------------------------------------------------------------------------------
// SPI - Create End Of Transfer condition 
void _Chip_SPI_SendEOT(void *pPeri)
{
	((_CHIP_SPI_T *)pPeri)->FIFOWR |= (1 << 20);	
}

// ------------------------------------------------------------------------------------------------
// SPI - Create End Of Frame condition 
void _Chip_SPI_SendEOF(void *pPeri)
{
	((_CHIP_SPI_T *)pPeri)->FIFOWR |= (1 << 21);
}

// ------------------------------------------------------------------------------------------------
// SPI - Create Receive Ignore condition 
void _Chip_SPI_ReceiveIgnore(void *pPeri)
{
	((_CHIP_SPI_T *)pPeri)->FIFOWR |= (1 << 22);
}

// ------------------------------------------------------------------------------------------------
// Flush SPI Tx
void _Chip_SPI_Flush_Tx(void *pPeri)
{
																					// Not yet implemented	
}

// ------------------------------------------------------------------------------------------------
// Flush SPI Rx
void _Chip_SPI_Flush_Rx(void *pPeri)
{
																					// Not yet implemented	
}

// ------------------------------------------------------------------------------------------------
// SPIFIFO Functions 
// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------
// Enable/disable selected interrupts
bool _Chip_SPIFIFO_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_SPI_T*)pPeri)->FIFOINTENSET = IRQBitMask;
	else ((_CHIP_SPI_T*)pPeri)->FIFOINTENCLR = IRQBitMask;
	
	return(true);

}

// ------------------------------------------------------------------------------------------------
// Read SPIFIFO status
uint32_t _Chip_SPIFIFO_Get_Peri_Status(void *pPeri)
{
	return(((_CHIP_SPI_T*)pPeri)->FIFOSTAT);
}


// ------------------------------------------------------------------------------------------------
// clear SPIFIFO status
bool _Chip_SPIFIFO_Clear_Peri_Status(void *pPeri, uint32_t BitMask)
{
	((_CHIP_SPI_T*)pPeri)->FIFOSTAT |= BitMask;
	return true;
}

// ------------------------------------------------------------------------------------------------
// Read SPIFIFO interrupt status
uint32_t _Chip_SPIFIFO_Get_IRQ_Status(void *pPeri)
{
	return(((_CHIP_SPI_T *)pPeri)->FIFOINTSTAT);
}

// ------------------------------------------------------------------------------------------------
// Clear SPIFIFO interrupt status
bool _Chip_SPIFIFO_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
	((_CHIP_SPI_T *)pPeri)->FIFOINTENCLR |= NewValue;
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Read actaul value of FIFO Tx threshold
uint32_t _Chip_SPIFIFO_Get_TxTHLevel(void *pPeri)
{
	return(((((_CHIP_SPI_T *)pPeri)->FIFOTRIG >> 8) & 0x0f) + 1);					// read TXLVL 
}

// ------------------------------------------------------------------------------------------------
// Read actaul value of FIFO Rx threshold
uint32_t _Chip_SPIFIFO_Get_RxTHLevel(void *pPeri)
{
	return(((((_CHIP_SPI_T *)pPeri)->FIFOTRIG >> 16) & 0x0f) + 1);					// read RXLVL 
}

// ------------------------------------------------------------------------------------------------
// Write new value of FIFO Tx Threshold
void _Chip_SPIFIFO_Set_TxTHLevel(void *pPeri, uint32_t NewValue)
{
	uint32_t RegVal = ((_CHIP_SPI_T *)pPeri)->FIFOTRIG;
	RegVal &= ~(0x0f << 8);															// Clear old setting
	RegVal |= ((NewValue & 0x0f) << 8); 											// Set a new setting
	((_CHIP_SPI_T *)pPeri)->FIFOTRIG = RegVal - 1;
}

// ------------------------------------------------------------------------------------------------
// Write new value of FIFO Rx Threshold
void _Chip_SPIFIFO_Set_RxTHLevel(void *pPeri, uint32_t NewValue)
{
	uint32_t RegVal = ((_CHIP_SPI_T *)pPeri)->FIFOTRIG;
	RegVal &= ~(0x0f << 16);														// Clear old setting
	RegVal |= ((NewValue & 0x0f) << 16); 											// Set a new setting
	((_CHIP_SPI_T *)pPeri)->FIFOTRIG = RegVal - 1;
}


// ******************************************************************************************************
// USB Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------
// return USB periphery index
int32_t _Chip_USB_Get_PeriIndex(void *pPeri)							// Return Index from pointer
{
	if((_CHIP_USBHSD_T*) pPeri == USBHSD) return(1);
	if((_CHIP_USB_T*) pPeri == USB0) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------
// Power down / up
bool _Chip_USB_Power(void *pPeri, bool NewState)
{

	switch(_Chip_USB_Get_PeriIndex(pPeri))
	{
		case 0:
		{
			if(NewState) SYSCON->PDRUNCFG[0] &= !(SYSCON_PDRUNCFG_PDEN_USB0_PHY(1));// Enable Power for periphery
			else  SYSCON->PDRUNCFG[0] |= (SYSCON_PDRUNCFG_PDEN_USB0_PHY(1));		// Disable Power for periphery
			break;
		}
		case 1:
		{
			if(NewState) SYSCON->PDRUNCFG[1] &= !(SYSCON_PDRUNCFG_PDEN_USB1_PHY(1));// Enable Power for periphery
			else  SYSCON->PDRUNCFG[1] |= (SYSCON_PDRUNCFG_PDEN_USB1_PHY(1));		// Disable Power for periphery
			break;
		}		
		default: return(false);
	}
	
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Read USB interrupt status
uint32_t _Chip_USB_Get_IRQStatus(void *pPeri)
{
	return(USB0->INTSTAT);
}

// ------------------------------------------------------------------------------------------------------
// USB(Num) Initialization
// return is pointer to periphery, otherwise NULL
void *_Chip_USB_Init(int32_t PeriIndex)
{
	void *ResPtr;
	
	// Check:
	SYS_ASSERT((PeriIndex <= 1) && (PeriIndex >= 0));								// check - in release will be ASSERT skipped
	//if(PeriIndex > 1) return( NULL );												// LPC465xx have two USB

	switch(PeriIndex)
	{
		case 1: ResPtr = (void *) USBHSD; break;
		case 0: ResPtr = (void *) USB0; break;
		default: return(NULL);
	}
	// Power:
	//SYSCON->PDRUNCFG &= ~SYSCON_PDRUNCFG_PDEN_xxx(1);								// Disable power down for xxx
	
	// Clock:
	//SYSCON->AHBCLKCTRLn |= SYSCON_AHBCLKCTRL_xxx(1);								// Enable system clock for xxx
	
	// Reset:
	//SYSCON->PRESETCTRLn |= SYSCON_PRESETCTRL_xxx_RST(1);__nop();					// Activate reset xxx
		
	// prepis z drv_USBD.c
	return (ResPtr);
}	

// ------------------------------------------------------------------------------------------------------
// USB Enable/disable
// result is true:successfull otherwise false
bool _Chip_USB_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	
	if(NewState)
	{
		NVIC_ClearPendingIRQ(USB0_IRQn);											// clear pending interrupt flag
		NVIC_EnableIRQ((IRQn_Type) (USB0_IRQn));									// Enable USB interrupt
	}
	else
	{
		NVIC_DisableIRQ((IRQn_Type) (USB0_IRQn));									// Disable USB interrupt
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Configure USB
// result is true:successfull otherwise false
bool _Chip_USB_Configure(void *pPeri, bool DeviceMode, bool HostMode, bool OTGMode)
{
	// prepis z drv_USBD.c
	
	return(true);
}

// ******************************************************************************************************
// ADC Functions
// ******************************************************************************************************
bool _Chip_ADC_DMA_Mode;
bool _Chip_ADC_Repeat_Mode;
bool _Chip_ADC_Temp_Sensor;

// ------------------------------------------------------------------------------------------------
// return ptr to periphary
int32_t _Chip_ADC_Get_PeriIndex(void *pPeri)
{
	if(((_CHIP_ADC_T*) pPeri) == ADC0) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------
// Power down / up
bool _Chip_ADC_Power(void *pPeri, bool NewState)
{
	int32_t PeriIndex = _Chip_ADC_Get_PeriIndex(pPeri);
	
	if(PeriIndex != 0) return(false);
	if(NewState) SYSCON->PDRUNCFG[0] &= ~SYSCON_PDRUNCFG_PDEN_ADC0_MASK;			// Enable Power for periphery
	else  SYSCON->PDRUNCFG[0] |= SYSCON_PDRUNCFG_PDEN_ADC0_MASK;					// Disable Power for periphery
	
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Get interrupt flags
uint32_t _Chip_ADC_Get_CHIRQ_Status(_CHIP_ADC_T *pADC)
{
	return pADC->FLAGS;
}


// ------------------------------------------------------------------------------------------------
// clear interrupt flags
bool _Chip_ADC_Clear_CHIRQ_Status(_CHIP_ADC_T *pADC, uint32_t ChannelBitMask)
{
	pADC->FLAGS = ChannelBitMask;													// clear
	return(true);
}

// ------------------------------------------------------------------------------------------------
// ADC Conversion start
void _Chip_ADC_Start(void *pPeri, uint32_t ChannelBitMask)
{
	((_CHIP_ADC_T *)pPeri)->STARTUP |= ADC_STARTUP_ADC_ENA_MASK;					// Run ADC
	((_CHIP_ADC_T *)pPeri)->STARTUP |= ADC_STARTUP_ADC_INIT_MASK;					// 
}

// ------------------------------------------------------------------------------------------------
// ADC channel interrupt enable/disable for selected channel
// result is true:successfull otherwise false
extern bool _Chip_ADC_DMA_Mode;
bool _Chip_ADC_IRQ_Peri_Enable(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState)
	{
		((_CHIP_ADC_T *)pPeri)->INTEN |= IRQBitMask;								// Set
	}
	else
	{
		((_CHIP_ADC_T *)pPeri)->INTEN &= ~IRQBitMask;								// clear
	}
	return(true);
}


// ------------------------------------------------------------------------------------------------------
// ADC(Num) Initialization (PowerUp, Enable CLK, Init)
// return is pointer to periphery, otherwise NULL
void *_Chip_ADC_Init(int32_t PeriIndex)
{
	// Check:
	SYS_ASSERT(PeriIndex == 0);
	//if(PeriIndex != 0) return( NULL );											// Check for valid index of periphery

	// Power:
	_Chip_ADC_Power(ADC0, true);													// Power Up

	// Clock:
	SYSCON->AHBCLKCTRL[0] |= SYSCON_AHBCLKCTRL_ADC0(1);								// Enable system clock for ADC

	// Reset:
	SYSCON->PRESETCTRL[0] |= (SYSCON_PRESETCTRL_ADC0_RST(1));__nop();				// Activate reset
	SYSCON->PRESETCTRL[0] &= ~(SYSCON_PRESETCTRL_ADC0_RST(1));						// Deactivate reset
	_Chip_ADC_Temp_Sensor = false;
	_Chip_ADC_Repeat_Mode = false;
	_Chip_ADC_DMA_Mode = false;
	
	return((void *) ADC0);
}

// ------------------------------------------------------------------------------------------------------
// Enable/Disable ADC
// if Channel is UINT32_MAX, enable while periphery without channels, otherwise ena/disa selected channel
// result is true:successfull otherwise false
bool _Chip_ADC_Enable(void * pPeri, uint32_t ChannelBitMask, bool NewState)
{
	SYS_ASSERT(pPeri != NULL);														// check
	int32_t PeriIndex = _Chip_ADC_Get_PeriIndex(pPeri);								// return peripherial index
	
	if(NewState == true)
	{
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[0] |= ADC_SEQ_CTRL_SEQ_ENA(1) | ADC_SEQ_CTRL_START(1);	// Enable and Start Sequencer A
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[1] |= ADC_SEQ_CTRL_SEQ_ENA(1) | ADC_SEQ_CTRL_START(1);	// Enable and Start Sequencer B
		_Chip_ADC_Power(pPeri, true);												// Power Up
	}
	else
	{
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[0] &= ~ADC_SEQ_CTRL_SEQ_ENA(1);			// Disable Sequencer A
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[1] &= ~ADC_SEQ_CTRL_SEQ_ENA(1);			// Disable Sequencer B
		_Chip_ADC_Power(pPeri, false);												// Power Down
	}
	return (true);
}

// ------------------------------------------------------------------------------------------------
// ADC Interrupt - enable/disable
bool _Chip_ADC_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	SYS_ASSERT(pPeri != NULL);														// check. for ADC 0
	
	if(NewState == true)
	{
		NVIC_ClearPendingIRQ(ADC0_SEQA_IRQn);										// clear pending NVIC interrupt flag
		NVIC_ClearPendingIRQ(ADC0_SEQB_IRQn);										// clear pending NVIC interrupt flag
		NVIC_ClearPendingIRQ(ADC0_THCMP_IRQn);										// clear pending NVIC interrupt flag
		
		// Enable IRQ:
		NVIC_EnableIRQ(ADC0_SEQA_IRQn);												// Enable NVIC interrupt
		NVIC_EnableIRQ(ADC0_SEQB_IRQn);												// Enable NVIC interrupt
		NVIC_EnableIRQ(ADC0_THCMP_IRQn);											// Enable NVIC interrupt		
	}
	else
	{
		NVIC_DisableIRQ(ADC0_SEQA_IRQn);											// Disable NVIC interrupt
		NVIC_DisableIRQ(ADC0_SEQB_IRQn);											// Disable NVIC interrupt
		NVIC_DisableIRQ(ADC0_THCMP_IRQn);											// Disable NVIC interrupt
	}
	
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Read active channel on ADC
// read from IOCON.A configuration for each ADC Channel. Aif set as Analog, Channel is active. Otherwise inactive
//User manual Rev. 2.4 � 11 November 2019 1057 of 1149,  NXP Semiconductors UM10912
//Table 1025.ADC0 pin description
//Function Connect to Description
//ADC0_0 PIO0_10 Analog input channel 0.
//ADC0_1 PIO0_11 Analog input channel 1.
//ADC0_2 PIO0_12 Analog input channel 2.
//ADC0_3 PIO0_15 Analog input channel 3.
//ADC0_4 PIO0_16 Analog input channel 4.
//ADC0_5 PIO0_31 Analog input channel 5.
//ADC0_6 PIO1_0 Analog input channel 6.
//ADC0_7 PIO2_0 Analog input channel 7.
//ADC0_8 PIO2_1 Analog input channel 8.
//ADC0_9 PIO3_21 Analog input channel 9.
//ADC0_10 PIO3_22 Analog input channel 10.
//ADC0_11 PIO0_23 Analog input channel 11.
uint32_t _Chip_ADC_Get_ChannelMask(void *pPeri)
{
	uint32_t res = 0;
	SYS_ASSERT(pPeri != NULL);														// check

	for(uint8_t bit=10 ; bit<=12 ; bit++ )											// P0.10 - AD0.0; P0.11 - AD0.1; P0.12 - AD0.2
	{
		if(((IOCON->PIO[0][bit]) & IOCON_PIO_DIGIMODE_MASK) == 0)					// Analog input mode
		{
			res |= (1 << (bit - 10));												// set bits 0 to 2
		}
	}
	
	for(uint8_t bit=15 ; bit<=16 ; bit++ )											// P0.15 - AD0.3; P0.16 - AD0.4
	{
		if(((IOCON->PIO[0][bit]) & IOCON_PIO_DIGIMODE_MASK) == 0)					// Analog input mode
		{
			res |= (8 << (bit - 15));												// set bits 3 to 4
		}
	}	

	if(((IOCON->PIO[0][31]) & IOCON_PIO_DIGIMODE_MASK) == 0)						// Analog input mode
	{
		res |= (1 << (5));															// set bit 5
	}
	
	if(((IOCON->PIO[1][0]) & IOCON_PIO_DIGIMODE_MASK) == 0)							// Analog input mode
	{
		res |= (1 << (6));															// set bit 6
	}

	if(((IOCON->PIO[2][0]) & IOCON_PIO_DIGIMODE_MASK) == 0)							// Analog input mode
	{
		res |= (1 << (7));															// set bit 7
	}

	if(((IOCON->PIO[2][1]) & IOCON_PIO_DIGIMODE_MASK) == 0)							// Analog input mode
	{
		res |= (1 << (8));															// set bit 8
	}
	
	if(((IOCON->PIO[3][21]) & IOCON_PIO_DIGIMODE_MASK) == 0)						// Analog input mode
	{
		res |= (1 << (9));															// set bit 9
	}
	
	if(((IOCON->PIO[3][22]) & IOCON_PIO_DIGIMODE_MASK) == 0)						// Analog input mode
	{
		res |= (1 << (10));															// set bit 10
	}	
	
	if(((IOCON->PIO[0][23]) & IOCON_PIO_DIGIMODE_MASK) == 0)						// Analog input mode
	{
		res |= (1 << (11));															// set bit 11
	}	
	
	if(_Chip_ADC_Temp_Sensor) res |= 0x01;											// enable channel 0 for temperature sensor
	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// Enable/Disable on-chip temperature sensor on ADC0.CH0
// result is true:successfull otherwise false
bool _Chip_Temp_Enable(bool NewState)
{
	if(NewState == true)
	{
		SYSCON->PDRUNCFG[0] &= !(SYSCON_PDRUNCFGSET_PDEN_TS_MASK);					// Enable Power for periphery
		ADC0->INSEL = 0x03;															// select Temp. sensor
		_Chip_ADC_Temp_Sensor = true;
	}
	else
	{
		SYSCON->PDRUNCFG[0] |= (SYSCON_PDRUNCFGCLR_PDEN_TS_MASK);					// Disable Power for periphery
		ADC0->INSEL = 0x00;															// select ADC0_IN0
		_Chip_ADC_Temp_Sensor = false;
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// ADC[x](ChannelBitMask) Configure whole ADC
// result is true:successfull otherwise false
bool _Chip_ADC_Configure (void *pPeri, uint32_t ChannelBitMask, uint32_t Speed_kHz, bool RepeatMode, bool DMAMode)
{
//	int8_t i = _CHIP_ADC_CHANNELS_COUNT-1;
	uint32_t clksrc_rate;
	
	SYS_ASSERT(pPeri != NULL);														// check
	SYS_ASSERT(ChannelBitMask > 0);													// check
	
	((_CHIP_ADC_T *)pPeri)->INTEN = 0;												// diasable all interrupt requests
	((_CHIP_ADC_T *)pPeri)->CTRL = 0;												// Clear control  and stop ADC

	((_CHIP_ADC_T *)pPeri)->CTRL = (((_CHIP_ADC_T *)pPeri)->CTRL & ADC_CTRL_CLKDIV_MASK); // clear DIV 
	((_CHIP_ADC_T *)pPeri)->CTRL &= ~ADC_CTRL_ASYNMODE_MASK;						// set synchronous mode
	((_CHIP_ADC_T *)pPeri)->CTRL |= 0x03 << ADC_CTRL_RESOL_SHIFT;					// set 12-bit rosolution
	((_CHIP_ADC_T *)pPeri)->CTRL |= ADC_CTRL_BYPASSCAL_MASK;						// bypass calibration

	clksrc_rate = _Chip_Clock_Get_ADCClk_Rate();									// Get Clock Rate
	
	uint16_t DIVider;
	
	DIVider = (clksrc_rate / (Speed_kHz * 1000)) - 1;
	
	if(ADC0->CTRL & ADC_CTRL_ASYNMODE_MASK) 
	{																				// Asynchronous mode
		SYS_ASSERT(DIVider <= SYSCON_ADCCLKDIV_DIV_MASK);							// check
		SYSCON->ADCCLKDIV |= DIVider & SYSCON_ADCCLKDIV_DIV_MASK;
	}
	else
	{
		SYS_ASSERT(DIVider <= ADC_CTRL_CLKDIV_MASK);								// check
		ADC0->CTRL |= DIVider & ADC_CTRL_CLKDIV_MASK;
	}
	
	//((_CHIP_ADC_T *)pPeri)->CTRL |= ADC_CTRL_CLKDIV((clksrc_rate / (Speed_kHz*1000)) & 0xff); // AD CLK must be lower or equal to 12.4 MHz. Src is PCLK.

	// Autocalibration:
	((_CHIP_ADC_T *)pPeri)->CALIB |= ADC_CALIB_CALIB_MASK;							// Set CALMODE bit
	((_CHIP_ADC_T *)pPeri)->STARTUP |= ADC_STARTUP_ADC_ENA_MASK;					// Run ADC
	{}while(((_CHIP_ADC_T *)pPeri)->CALIB & ADC_CALIB_CALIB_MASK);					// Wait for CALMODE bit is zero

	// next configure ADC - use Sequencer A by default
	((_CHIP_ADC_T *)pPeri)->STARTUP &= ~ADC_STARTUP_ADC_ENA_MASK;					// Stop ADC
	((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[0] = (ChannelBitMask & 0x0fff);				// enable channels

		
	//((_CHIP_ADC_T *)pPeri)->CTRL |= ADC_SEQ_CTRL_MODE_EOS;						// end of sequence result
	//((_CHIP_ADC_T *)pPeri)->CTRL |= ADC_SEQ_CTRL_HWTRIG_POLPOS | ADC0_SEQ_CTRL_HWTRIG_SCT2_OUT3;		// ADC startuje SCT2 Out 3 na nabeznu hranu HW Trig.
	((_CHIP_ADC_T *)pPeri)->CTRL |= ADC_SEQ_CTRL_SEQ_ENA_MASK;						// enable sequencer of ADC0
	((_CHIP_ADC_T *)pPeri)->INTEN |= ADC_IRQEN_SEQA_IRQEN_MASK;						// enable interrupt of Sequencer A
		
	if(DMAMode) 																	// in DMA mode, set IRQ from last active channel, and global IRQ Disable
	{
		_Chip_ADC_DMA_Mode = true;													// never enable global IRQ !!!
		//_Chip_ADC_Enable_IRQ_NVIC(pPeri, false);											// for DMA transfer disable NVIC IRQ

		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[0] &= ~(ADC_SEQ_CTRL_START_MASK);			// disable SW and HW Start
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[0] &= ~ADC_SEQ_CTRL_BURST_MASK;			// Set burst mode, otherwise single mode		
	}
	else 
	{
		_Chip_ADC_DMA_Mode = false;
	}
	
	if(RepeatMode) 
	{
		_Chip_ADC_Repeat_Mode = true;
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[0] &= ~(ADC_SEQ_CTRL_START_MASK);			// disable SW and HW Start
		((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[0] |= ADC_SEQ_CTRL_BURST_MASK;				// Set burst mode, otherwise single mode
	}
	
	
	return (true);
}


// ******************************************************************************************************
// Timer	- MRT for LPC546xx
// LPC546xx has one timer with 4 channels
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------
// return index to periphery MRT(pointer to peri)
int32_t _Chip_MRT_Get_PeriIndex(void *pPeri)
{
	if((_CHIP_MRT_T*) pPeri == MRT0) return(0);
	return(-1);																		// otherwise error
}	

// ------------------------------------------------------------------------------------------------
// Timer Clear channel interrupt Flag for selected channel
// result is true:successfull otherwise false
bool _Chip_MRT_Clear_CHIRQ_Status(void *pPeri, uint32_t ChannelBitMask)
{
	bool res = false;
	
	if(ChannelBitMask && 0x01){((_CHIP_MRT_T*)pPeri)->CHANNEL[0].STAT |= MRT_CHANNEL_STAT_INTFLAG_MASK;res = true;}	// Clear Int Stat for channel
	if(ChannelBitMask && 0x02){((_CHIP_MRT_T*)pPeri)->CHANNEL[1].STAT |= MRT_CHANNEL_STAT_INTFLAG_MASK;res = true;}	// Clear Int Stat for channel
	if(ChannelBitMask && 0x04){((_CHIP_MRT_T*)pPeri)->CHANNEL[2].STAT |= MRT_CHANNEL_STAT_INTFLAG_MASK;res = true;}	// Clear Int Stat for channel
	if(ChannelBitMask && 0x08){((_CHIP_MRT_T*)pPeri)->CHANNEL[3].STAT |= MRT_CHANNEL_STAT_INTFLAG_MASK;res = true;}	// Clear Int Stat for channel
	return(res);
}

// ------------------------------------------------------------------------------------------------
// MRT Get channel interrupt Flag for all channels
// result is bit position for active IRQ
uint32_t _Chip_MRT_Get_CHIRQ_Status(void *pPeri)
{
	uint32_t Result = 0;
	if(((_CHIP_MRT_T*)pPeri)->CHANNEL[0].STAT && MRT_CHANNEL_STAT_INTFLAG_MASK) Result |= 0x01;// read INTFLAG for channel 0
	if(((_CHIP_MRT_T*)pPeri)->CHANNEL[1].STAT && MRT_CHANNEL_STAT_INTFLAG_MASK) Result |= 0x02;// read INTFLAG for channel 1
	if(((_CHIP_MRT_T*)pPeri)->CHANNEL[2].STAT && MRT_CHANNEL_STAT_INTFLAG_MASK) Result |= 0x04;// read INTFLAG for channel 2
	if(((_CHIP_MRT_T*)pPeri)->CHANNEL[3].STAT && MRT_CHANNEL_STAT_INTFLAG_MASK) Result |= 0x08;// read INTFLAG for channel 3
	return(Result);
}

// ------------------------------------------------------------------------------------------------
// MRT channel interrupt enable/disable for selected channel
// result is true:successfull otherwise false
bool _Chip_MRT_IRQ_Peri_Enable(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	bool res = false;
	if(NewState)																	// Enable channel interrupt
	{
		if(IRQBitMask && 0x01){((_CHIP_MRT_T*)pPeri)->CHANNEL[0].CTRL |= MRT_CHANNEL_CTRL_IRQEN_MASK;res = true;}	// enable timer 0 interrupt
		if(IRQBitMask && 0x02){((_CHIP_MRT_T*)pPeri)->CHANNEL[1].CTRL |= MRT_CHANNEL_CTRL_IRQEN_MASK;res = true;}	// enable timer 1 interrupt
		if(IRQBitMask && 0x04){((_CHIP_MRT_T*)pPeri)->CHANNEL[2].CTRL |= MRT_CHANNEL_CTRL_IRQEN_MASK;res = true;}	// enable timer 2 interrupt
		if(IRQBitMask && 0x08){((_CHIP_MRT_T*)pPeri)->CHANNEL[3].CTRL |= MRT_CHANNEL_CTRL_IRQEN_MASK;res = true;}	// enable timer 3 interrupt		
	}
	else 																			// disable channel interrupt
	{
		if(IRQBitMask && 0x01){((_CHIP_MRT_T*)pPeri)->CHANNEL[0].CTRL &= !(MRT_CHANNEL_CTRL_IRQEN_MASK);res = true;}// disable timer 0 interrupt
		if(IRQBitMask && 0x02){((_CHIP_MRT_T*)pPeri)->CHANNEL[1].CTRL &= !(MRT_CHANNEL_CTRL_IRQEN_MASK);res = true;}// disable timer 1 interrupt
		if(IRQBitMask && 0x04){((_CHIP_MRT_T*)pPeri)->CHANNEL[2].CTRL &= !(MRT_CHANNEL_CTRL_IRQEN_MASK);res = true;}// disable timer 2 interrupt
		if(IRQBitMask && 0x08){((_CHIP_MRT_T*)pPeri)->CHANNEL[3].CTRL &= !(MRT_CHANNEL_CTRL_IRQEN_MASK);res = true;}// disable timer 3 interrupt
	}
	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// MRT(Num) Initialization
// return is pointer to periphery, otherwise NULL
// Index is 0 for LPC15xx, Channels are 0-3
void *_Chip_MRT_Init(int32_t PeriIndex)
{
	// Check:
	SYS_ASSERT(PeriIndex == 0);														// check - in release will be ASSERT skipped
	//if(PeriIndex != 0) return(NULL);
	
	// Power:
	// Nothing to power On/Off
	
	// Clock:
	SYSCON->AHBCLKCTRL[1] |= SYSCON_AHBCLKCTRL_MRT(1);								// Enable system clock for MRT
	
	// Reset:
	SYSCON->PRESETCTRL[1] |= SYSCON_PRESETCTRL_MRT_RST(1);__nop();					// Activate reset
	SYSCON->PRESETCTRL[1] &= ~(SYSCON_PRESETCTRL_MRT_RST(1));						// Deactivate reset
	
	return((void*) MRT0);																	// Return
}

// ------------------------------------------------------------------------------------------------------
// MRT(Num) Enable/disable
// result is true:successfull otherwise false
// if ChannelBitMask is set, selected channel are enabled/disabled
// 		otherwise whole timer as periphery is switched
bool _Chip_MRT_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
	bool res = false;
	
	if(pPeri == NULL ) return(false);
	
	if(NewState)	
	{
		if(ChannelBitMask)
		{
			if(ChannelBitMask && 0x01){((_CHIP_MRT_T*)pPeri)->CHANNEL[0].CTRL |= 1;}	// enable timer channel 0 interrupt
			if(ChannelBitMask && 0x02){((_CHIP_MRT_T*)pPeri)->CHANNEL[1].CTRL |= 1;}	// enable timer channel 1 interrupt
			if(ChannelBitMask && 0x04){((_CHIP_MRT_T*)pPeri)->CHANNEL[2].CTRL |= 1;}	// enable timer channel 2 interrupt
			if(ChannelBitMask && 0x08){((_CHIP_MRT_T*)pPeri)->CHANNEL[3].CTRL |= 1;}	// enable channel timer 3 interrupt
		}
	}
	else
	{
		if(ChannelBitMask)
		{
			if(ChannelBitMask && 0x01){((_CHIP_MRT_T*)pPeri)->CHANNEL[0].CTRL &= ~1;}	// disable timer channel 0 interrupt
			if(ChannelBitMask && 0x02){((_CHIP_MRT_T*)pPeri)->CHANNEL[1].CTRL &= ~1;}	// disable timer channel 1 interrupt
			if(ChannelBitMask && 0x04){((_CHIP_MRT_T*)pPeri)->CHANNEL[2].CTRL &= ~1;}	// disable timer channel 2 interrupt
			if(ChannelBitMask && 0x08){((_CHIP_MRT_T*)pPeri)->CHANNEL[3].CTRL &= ~1;}	// disable timer channel timer 3 interrupt
		}
		if( (((_CHIP_MRT_T*)pPeri)->CHANNEL[0].CTRL & 0x01) && 	\
			(((_CHIP_MRT_T*)pPeri)->CHANNEL[1].CTRL & 0x01) &&	\
			(((_CHIP_MRT_T*)pPeri)->CHANNEL[2].CTRL & 0x01) &&	\
			(((_CHIP_MRT_T*)pPeri)->CHANNEL[3].CTRL & 0x01) ) __nop();
	}
	res = true;

	return(res);																	// nothing to check. return Seccess
}

// ------------------------------------------------------------------------------------------------
// NVIC Interrupt - enable/disable
bool _Chip_MRT_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	
	if(NewState == true)
	{
		NVIC_ClearPendingIRQ (MRT0_IRQn);											// Clear NVIC interrupt flag
		NVIC_EnableIRQ(MRT0_IRQn);													// Enable NVIC interrupt
	}
	else
	{
		NVIC_DisableIRQ(MRT0_IRQn);													// Enable NVIC interrupt
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// MRT(Channel) Configure
// Channel 0-3
// if repeat==true, autoreload timer will executed, otherwise oneshot only
// result is true:successfull otherwise false
bool _Chip_MRT_Configure (void *pPeri, uint8_t Channel, uint32_t RateHz, bool RepeatMode)
{
	bool res = false;
	uint32_t reg;
	
	if (Channel > 3) return(false);													// LPC465xx have only channels 0-3
	if(pPeri == NULL ) return(false);
	
	//Chip_MRT_SetInterval(pMRT, (Chip_Clock_GetSystemClockRate() / RateHz) | MRT_INTVAL_LOAD);
	uint32_t tmp = _Chip_Clock_Get_SysClk_Rate();									// Get System Clock
	tmp = tmp / RateHz;
	if(tmp > 0x00ffffff) tmp = 0x00ffffff;

	tmp = tmp | ((uint32_t) 1 << 31);
	((_CHIP_MRT_T*)pPeri)->CHANNEL[Channel].INTVAL = tmp; 
	
	reg = ((_CHIP_MRT_T*)pPeri)->CHANNEL[Channel].CTRL & ~0x06;					// read all nor mode
	if(RepeatMode == true) 	((_CHIP_MRT_T*)pPeri)->CHANNEL[Channel].CTRL = reg | (0x00 << 1);	
	else					((_CHIP_MRT_T*)pPeri)->CHANNEL[Channel].CTRL = reg | (0x01 << 1);
	//Chip_MRT_IntClear(pMRT);
	res = _Chip_MRT_Clear_CHIRQ_Status(pPeri, 1 << Channel);

	if(res == true) res = _Chip_MRT_Enable(pPeri, 1 << Channel, true);				// enable interrupt for channel
	else 			res = _Chip_MRT_Enable(pPeri, 1 << Channel, false);				// disable interrupt for channel

	return (res);
}

// ******************************************************************************************************
// CTIMER Functions - like Timer
// ******************************************************************************************************
static const uint32_t CTIM_IRQ[] = {CTIMER0_IRQn, CTIMER1_IRQn, CTIMER2_IRQn, CTIMER3_IRQn, CTIMER4_IRQn}; // quick decone IRQ

// ------------------------------------------------------------------------------------------------
// return index to periphery CTimer(pointer to peri)
int32_t _Chip_CTimer_Get_PeriIndex(void *pPeri)
{
	if((_CHIP_CTIMER_T*) pPeri == CTIMER0) return(0);
	if((_CHIP_CTIMER_T*) pPeri == CTIMER1) return(1);
	if((_CHIP_CTIMER_T*) pPeri == CTIMER2) return(2);
	if((_CHIP_CTIMER_T*) pPeri == CTIMER3) return(3);
	if((_CHIP_CTIMER_T*) pPeri == CTIMER4) return(4);
	return(-1);																		// otherwise error
}	


// ------------------------------------------------------------------------------------------------
// CTIMER Clear channel interrupt Flag for selected channel
// result is true:successfull otherwise false
bool _Chip_Timer_Clear_CHIRQ_Status(void *pPeri, uint32_t ChannelBitMask)
{
	bool res = false;
	
	if(ChannelBitMask && 0x01){((_CHIP_CTIMER_T*)pPeri)->IR |= (CTIMER_IR_MR0INT_MASK | CTIMER_IR_CR0INT_MASK);res = true;}	// Clear Int Stat for channel
	if(ChannelBitMask && 0x02){((_CHIP_CTIMER_T*)pPeri)->IR |= (CTIMER_IR_MR1INT_MASK | CTIMER_IR_CR1INT_MASK);res = true;}	// Clear Int Stat for channel
	if(ChannelBitMask && 0x04){((_CHIP_CTIMER_T*)pPeri)->IR |= (CTIMER_IR_MR2INT_MASK | CTIMER_IR_CR2INT_MASK);res = true;}	// Clear Int Stat for channel
	if(ChannelBitMask && 0x08){((_CHIP_CTIMER_T*)pPeri)->IR |= (CTIMER_IR_MR3INT_MASK | CTIMER_IR_CR3INT_MASK);res = true;}	// Clear Int Stat for channel
	return(res);
}

// ------------------------------------------------------------------------------------------------
// CTIMER Get channel interrupt Flag for all channels
// result is bit position for active IRQ
uint32_t _Chip_Timer_Get_CHIRQ_Status(void *pPeri)
{
	uint32_t Result;
	if(((_CHIP_CTIMER_T*)pPeri)->IR & (CTIMER_IR_MR0INT_MASK | CTIMER_IR_CR0INT_MASK)) Result |= 0x01;// read INTFLAG for channel 0
	if(((_CHIP_CTIMER_T*)pPeri)->IR & (CTIMER_IR_MR1INT_MASK | CTIMER_IR_CR1INT_MASK)) Result |= 0x02;// read INTFLAG for channel 1
	if(((_CHIP_CTIMER_T*)pPeri)->IR & (CTIMER_IR_MR2INT_MASK | CTIMER_IR_CR2INT_MASK)) Result |= 0x04;// read INTFLAG for channel 2
	if(((_CHIP_CTIMER_T*)pPeri)->IR & (CTIMER_IR_MR3INT_MASK | CTIMER_IR_CR3INT_MASK)) Result |= 0x08;// read INTFLAG for channel 3
	return(Result);
}

// ------------------------------------------------------------------------------------------------
// CTIMER channel interrupt enable/disable for selected channel
// result is true:successfull otherwise false
bool _Chip_Timer_IRQ_Peri_Enable(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	bool res = false;
	uint32_t mask = 0;
	
	mask = ((IRQBitMask & 0x0F) & (1 << 3)) << 6;									// move MR3 to MR3I
	mask |= ((IRQBitMask & 0x0F) & (1 << 2)) << 4;									// move MR2 to MR2I
	mask |= ((IRQBitMask & 0x0F) & (1 << 1)) << 2;									// move MR1 to MR1I
	mask |= ((IRQBitMask & 0x0F) & (1 << 0)) << 0;									// move MR0 to MR0I
	
	if(NewState) ((_CHIP_CTIMER_T*)pPeri)->MCR |= mask;
	else ((_CHIP_CTIMER_T*)pPeri)->MCR &= ~(mask);
	res = true;
	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// Timer(Num) Initialization - like CTimer
// return is pointer to periphery, otherwise NULL
// PeriIndex is 0-2 for sync timers CT0 - CT2, 3-4 for async timer CT3 and CT4
void *_Chip_Timer_Init(int32_t PeriIndex)
{
	void*	Result = NULL;
	
	// Check:
	SYS_ASSERT((PeriIndex <= 4) && (PeriIndex >= 0));								// check - in release will be ASSERT skipped
	//if(PeriIndex > 4) return(NULL);
	
	// Power:
	// Nothing to power On/Off
	
	switch(PeriIndex)
	{
		case 4:
		{
			// Power:
			// Nothing to power On/Off				
			// Clock:
			ASYNC_SYSCON->ASYNCAPBCLKCTRL |= ASYNC_SYSCON_ASYNCAPBCLKCTRL_CTIMER4_MASK;	// Enable system clock for CTIMER
			// Reset:
			ASYNC_SYSCON->ASYNCPRESETCTRL |= ASYNC_SYSCON_ASYNCPRESETCTRL_CTIMER4_MASK;	// activate reset for CTIMER
			__nop();
			ASYNC_SYSCON->ASYNCPRESETCTRL &= ~ASYNC_SYSCON_ASYNCPRESETCTRL_CTIMER4_MASK;	// deactivate reset for CTIMER
			Result = CTIMER4;
			break;
		}		
		case 3:
		{
			// Power:
			// Nothing to power On/Off				
			// Clock:
			ASYNC_SYSCON->ASYNCAPBCLKCTRL |= ASYNC_SYSCON_ASYNCAPBCLKCTRL_CTIMER3_MASK;	// Enable system clock for CTIMER
			// Reset:
			ASYNC_SYSCON->ASYNCPRESETCTRL |= ASYNC_SYSCON_ASYNCPRESETCTRL_CTIMER3_MASK;	// activate reset for CTIMER
			__nop();
			ASYNC_SYSCON->ASYNCPRESETCTRL &= ~ASYNC_SYSCON_ASYNCPRESETCTRL_CTIMER3_MASK;	// deactivate reset for CTIMER
			Result = CTIMER3;
			break;
		}		
		case 2:
		{
			// Power:
			// Nothing to power On/Off				
			// Clock:
			SYSCON->AHBCLKCTRL[1] |= SYSCON_AHBCLKCTRL_CTIMER2_MASK;				// Enable system clock for CTIMER
			// Reset:
			SYSCON->PRESETCTRL[1] |= SYSCON_PRESETCTRL_CTIMER2_RST_MASK;__nop();	// Activate reset
			SYSCON->PRESETCTRL[1] &= ~(SYSCON_PRESETCTRL_CTIMER2_RST_MASK);			// Deactivate reset
			Result = CTIMER2;
			break;
		}			
		case 1:
		{
			// Power:
			// Nothing to power On/Off				
			// Clock:
			SYSCON->AHBCLKCTRL[1] |= SYSCON_AHBCLKCTRL_CTIMER1_MASK;				// Enable system clock for CTIMER
			// Reset:
			SYSCON->PRESETCTRL[1] |= SYSCON_PRESETCTRL_CTIMER1_RST_MASK;__nop();	// Activate reset
			SYSCON->PRESETCTRL[1] &= ~(SYSCON_PRESETCTRL_CTIMER1_RST_MASK);			// Deactivate reset
			Result = CTIMER1;
			break;
		}			
		default:	// CTIMER0
		{
			// Power:
			// Nothing to power On/Off				
			// Clock:
			SYSCON->AHBCLKCTRL[1] |= SYSCON_AHBCLKCTRL_CTIMER0_MASK;				// Enable system clock for CTIMER
			// Reset:
			SYSCON->PRESETCTRL[1] |= SYSCON_PRESETCTRL_CTIMER0_RST_MASK;__nop();	// Activate reset
			SYSCON->PRESETCTRL[1] &= ~(SYSCON_PRESETCTRL_CTIMER0_RST_MASK);			// Deactivate reset
			Result = CTIMER0;
		}				
	}

	return(Result);
}

// ------------------------------------------------------------------------------------------------
// CTIMER Interrupt - enable/disable
bool _Chip_Timer_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	int32_t PeriIndex = _Chip_CTimer_Get_PeriIndex(pPeri);
	
	if(NewState)
	{
		NVIC_ClearPendingIRQ((IRQn_Type) CTIM_IRQ[PeriIndex]);						// Clear pending NVIC interrupt flag
		NVIC_EnableIRQ(((IRQn_Type) CTIM_IRQ[PeriIndex]));							// Enable NVIC interrupt
	}
	else
	{
		NVIC_DisableIRQ((IRQn_Type) (CTIM_IRQ[PeriIndex]));							// Disable NVIC interrupt
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// CTimer(Num) Enable/disable
// result is true:successfull otherwise false
bool _Chip_Timer_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
	bool res = false;
	int32_t PeriIndex = _Chip_CTimer_Get_PeriIndex(pPeri);
	
	if((pPeri == NULL ) || (PeriIndex < 0)) return(false);							// check
	
	res = _Chip_Timer_IRQ_Peri_Enable(pPeri, ChannelBitMask, NewState);				// Ena/Disa Channel
	
	if(NewState)	
	{
		((_CHIP_CTIMER_T*)pPeri)->TCR |= (1 << 0);									// Run Timer
		((_CHIP_CTIMER_T*)pPeri)->TCR &= ~(1 << 1);									// Clear reset counter
	}
	else
	{
		// ToDo: check active channel. If nothing active stop timer and disable complette:
		((_CHIP_CTIMER_T*)pPeri)->TCR &= ~(1 << 0);									// Stop Timer
		((_CHIP_CTIMER_T*)pPeri)->TCR &= ~(1 << 1);									// Clear reset counter		
	}

	return(res);																	// nothing to check. return Seccess
}

// ------------------------------------------------------------------------------------------------------
// CTimer[x](Channel) Configure
// Channel 0-3
// if repeat==true, autoreload timer will executed, otherwise oneshot only
// result is true:successfull otherwise false
bool _Chip_Timer_Configure (void *pPeri, uint8_t Channel, uint32_t RateHz, bool RepeatMode)
{
	uint64_t ClkIn;
	uint32_t Div = 0;
	uint64_t checkVal = 0;
	int32_t PeriIndex = _Chip_CTimer_Get_PeriIndex(pPeri);
	//bool res = false;
	
	
	if (Channel > 3) return(false);													// LPC546xx have only channels 0-3
	if(pPeri == NULL ) return(false);

	if(PeriIndex > 2) ClkIn = _Chip_Clock_Get_AsyncAPB_Rate();						// Get async clock source rate
	else ClkIn = _Chip_Clock_Get_MainClk_Rate();									// Get Main System Clock rate
	
	do
	{
		Div ++;
		checkVal = (ClkIn / Div) / RateHz;
	}while(checkVal > 0xffffffff);
	
	((_CHIP_CTIMER_T*)pPeri)->PC = 0;												// Prescale Counter value reset
	((_CHIP_CTIMER_T*)pPeri)->PR = Div - 1;											// Prescale Register value set to Max
	
	((_CHIP_CTIMER_T*)pPeri)->TCR &= ~(1 << 0);										// disable timer counting
	((_CHIP_CTIMER_T*)pPeri)->TCR |= (1 << 1);										// timer counter reset
	((_CHIP_CTIMER_T*)pPeri)->CTCR = 0;												// timer counter reset
	((_CHIP_CTIMER_T*)pPeri)->MR[Channel] = checkVal;								// Match register load
	((_CHIP_CTIMER_T*)pPeri)->IR =	(1 << Channel);									// clear interruptflags

	if(RepeatMode == true)															// auto reload
	{
		uint32_t mask = 0;
		mask = (2 << (Channel*3));													// move Channel bit to MRxR
		((_CHIP_CTIMER_T*)pPeri)->MCR |= mask;
	}
	return (true);
}


// ******************************************************************************************************
// DMA Functions
// ******************************************************************************************************

// DMA SRAM table - this can be optionally used with the Chip_DMA_SetSRAMBase()
// function if a DMA SRAM table is needed. This table is correctly aligned for
// the DMA controller.
ATTR_ALIGNED(512) _Chip_DMA_CHDesc_t _Chip_DMA_Table[_CHIP_DMA_CHANNELS_COUNT];
uint32_t DMA_CH_XFERLEN[_CHIP_DMA_CHANNELS_COUNT];

// ------------------------------------------------------------------------------------------------
// return pointer to periphery DMA(PeriIndex)
int32_t _Chip_DMA_Get_PeriIndex(void *pPeri)
{
	if((_CHIP_DMA_T*) pPeri == DMA0) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------
// DMA Clear channel interrupt state for selected BitMasked channels
// result is true:successfull otherwise false
bool _Chip_DMA_Clear_CHIRQ_Status(void *pPeri, uint32_t ChannelBitMask)
{
	((_CHIP_DMA_T*)pPeri)->COMMON[0].INTA |= DMA_COMMON_INTA_IA(ChannelBitMask);
	return(true);
}

// ------------------------------------------------------------------------------------------------
// DMA Get channel interrupt Flag for all channels
// result is bit position for active IRQ
uint32_t _Chip_DMA_Get_CHIRQ_Status(void *pPeri)
{
	return(((_CHIP_DMA_T*)pPeri)->COMMON[0].INTA);
}


// ------------------------------------------------------------------------------------------------
// DMA - Get channel control word
uint32_t _Chip_DMA_Get_CHControl(void *pPeri, uint32_t Channel)
{
	return(((_CHIP_DMA_T*)pPeri)->CHANNEL[Channel].CTLSTAT | DMA_CH_XFERLEN[Channel]);
}


// ------------------------------------------------------------------------------------------------
// DMA channel interrupt enable/disable for selected channel
// result is true:successfull otherwise false
bool _Chip_DMA_IRQ_Peri_Enable(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	bool res = false;
		
	if(NewState)
	{
		((_CHIP_DMA_T*)pPeri)->COMMON[0].INTENSET = DMA_COMMON_INTENSET_INTEN(IRQBitMask);	// enable interrupt for channels
	}
	else 
	{
		((_CHIP_DMA_T*)pPeri)->COMMON[0].INTENCLR = DMA_COMMON_INTENCLR_CLR(IRQBitMask);	// disable interrupt for channels
	}
	
	return(res);
}	



// ------------------------------------------------------------------------------------------------------
// DMA(Num) Initialization
// return is pointer to periphery, otherwise NULL
void *_Chip_DMA_Init(int32_t PeriIndex)
{
	// Check:
	SYS_ASSERT(PeriIndex == 0);														// check - in release will be ASSERT skipped
	//if(PeriIndex != 0) return(false);												// LPC546xx have only one DMA
	
	// Power:
	// Nothing to power On/Off
	
	// Clock:
	SYSCON->AHBCLKCTRL[0] |= SYSCON_AHBCLKCTRL_DMA(1);								// Enable system clock for DMA
	
	// Reset:
	SYSCON->PRESETCTRL[0] |= SYSCON_PRESETCTRL_DMA0_RST(1);__nop();					// Activate reset
	SYSCON->PRESETCTRL[0] &= ~(SYSCON_PRESETCTRL_DMA0_RST(1));						// Deactivate reset
	
//	// Clear IRQ:
//	DMA0->COMMON[0].ENABLECLR |= 0xffffffff;										// disable all channels
//	NVIC_ClearPendingIRQ(DMA0_IRQn);												// clear pending interrupt flag
//	
//	// Disable IRQ
//	NVIC_DisableIRQ(DMA0_IRQn);														// Enable DMA interrupt
	return((void *) DMA0);															// return
}

// ------------------------------------------------------------------------------------------------------
// DMA(Num) Enable/disable
// result is true:successfull otherwise false
bool _Chip_DMA_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
	if(pPeri == NULL ) return(false);

	if(NewState)
	{
		((_CHIP_DMA_T*)pPeri)->COMMON[0].ENABLESET |= ChannelBitMask;				// enable selected channels
		if(ChannelBitMask == UINT32_MAX) NVIC_EnableIRQ(DMA0_IRQn);					// enable IRQ
	}
	else
	{
		((_CHIP_DMA_T*)pPeri)->COMMON[0].ENABLECLR |= ChannelBitMask;				// disable selected channels
		if(ChannelBitMask == UINT32_MAX) NVIC_DisableIRQ(DMA0_IRQn);				// disable IRQ
	}
	return(true);																	// nothing to check. return Seccess
}

// ------------------------------------------------------------------------------------------------------
// DMA[x](Channel) Configure
// Channel 0-17
// CHAL_DMA_Xfer_t - structure with xfer parameters
// result is true:successfull otherwise false
bool _Chip_DMA_Configure (void *pPeri, uint8_t Channel, _Chip_DMA_Xfer_t *Xfer )
{
	return(false);
}


// ******************************************************************************************************
// EEPROM Functions
// ******************************************************************************************************
union onepage
{
	uint32_t word[_CHIP_EEPROM_PAGESIZE/4];
	uint8_t	 byte[_CHIP_EEPROM_PAGESIZE];
};


uint32_t EEPROM_VIRTUAL_PERI;

// ------------------------------------------------------------------------------------------------
// Power down / up
bool _Chip_EEPROM_Power(void *pPeri, bool NewState)
{
	if(NewState) SYSCON->PDRUNCFG[1] &= !(SYSCON_PDRUNCFGSET_PDEN_EEPROM(1));		// Enable Power for periphery
	else  SYSCON->PDRUNCFG[1] |= SYSCON_PDRUNCFGSET_PDEN_EEPROM(1);					// Disable Power for periphery
	
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// EEPROM Initialization
// return is pointer to periphery, otherwise NULL
void *_Chip_EEPROM_Init(int32_t PeriIndex)
{
	uint32_t Sys_CLK = _Chip_Clock_Get_CoreClk_Rate();
	_CHIP_EEPROM_T * pPeri;
	
	// Check:
	SYS_ASSERT(PeriIndex == 0);														// check - in release will be ASSERT skipped

	pPeri = EEPROM;
	
	// Power:
	SYSCON->PDRUNCFG[1] &= ~SYSCON_PDRUNCFG_PDEN_EEPROM(1);							// Disable power down for EEPROM

	// Clock:
	SYSCON->AHBCLKCTRL[0] |= SYSCON_AHBCLKCTRL_EEPROM(1);							// Enable system clock for EEPROM

	// Reset
	SYSCON->PRESETCTRL[0] |= (SYSCON_PRESETCTRL_EEPROM_RST(1));__nop();				// Activate reset
	SYSCON->PRESETCTRL[0] &= ~(SYSCON_PRESETCTRL_EEPROM_RST(1));					// Deactivate reset

	DelayChipUs(100, Sys_CLK);														// Delay for 100 uSec
	
	//Set eeprom clock divider
	uint32_t clockDiv = Sys_CLK / (uint32_t)_CHIP_EEPROM_INTERNAL_FREQ;				// 

	if ((Sys_CLK % (uint32_t)_CHIP_EEPROM_INTERNAL_FREQ) > ((uint32_t)_CHIP_EEPROM_INTERNAL_FREQ / 2UL))
    {
        clockDiv += 1UL;
    }
    pPeri->CLKDIV = clockDiv - 1UL;													// set divider
	
	pPeri->AUTOPROG = 0x0001;														// disable autoprog

   /* Set time delay parameter */
//	config->writeWaitPhase1 = 0x5U;
//	config->writeWaitPhase2 = 0x9U;
//	config->writeWaitPhase3 = 0x3U;
//	config->readWaitPhase1  = 0xFU;
//	config->readWaitPhase2  = 0x8U;
	
    pPeri->RWSTATE = EEPROM_RWSTATE_RPHASE1(0xFU - 1UL) | EEPROM_RWSTATE_RPHASE2(0x8U - 1UL);
    pPeri->WSTATE = EEPROM_WSTATE_PHASE1(0x5U - 1UL) | EEPROM_WSTATE_PHASE2(0x9U - 1UL) | EEPROM_WSTATE_PHASE3(0x3U - 1UL);
    pPeri->WSTATE |= EEPROM_WSTATE_LCK_PARWEP(false);

    /* Clear the remaining write operation  */
    pPeri->CMD = EEPROM_CMD_CMD(0x6);												// erase/program page cmd
    while ((pPeri->INTSTAT & EEPROM_INTENSET_PROG_SET_EN_MASK) == 0UL)				// wait for program finish status
    {
    }
	
	
	
	
	
	//	// Clear IRQ
//	NVIC_ClearPendingIRQ(EEPROM_IRQn);												// clear pending interrupt flag

//	// Disable IRQ
//	NVIC_DisableIRQ(EEPROM_IRQn);													// Enable EEPROM interrupt
	return((void *) EEPROM);
}

// ------------------------------------------------------------------------------------------------------
// EEPROM Enable/Disable
bool _Chip_EEPROM_Enable(void *pPeri, bool NewState)
{
	return(true);
}

// ------------------------------------------------------------------------------------------------
// NVIC Interrupt - enable/disable
bool _Chip_EEPROM_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	// Check:
	SYS_ASSERT(pPeri != NULL);														// check
	
	if(NewState == true)
	{
		NVIC_ClearPendingIRQ (EEPROM_IRQn);											// Clear NVIC interrupt flag
		NVIC_EnableIRQ(EEPROM_IRQn);												// Enable NVIC interrupt
	}
	else
	{
		NVIC_DisableIRQ(EEPROM_IRQn);												// Enable NVIC interrupt
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Write data to EEPROM
// LPC546xx have minimal vrite size:4 (word), aligned to 4, and maximum write size: 128 byte (one page)
bool _Chip_EEPROM_Write(void *pPeri, uint32_t EEDst, uint8_t *pInBuff, size_t byteswrt)
{
	uint32_t* pDestination;
	uint32_t* pSource;
	uint32_t TotalBytes = byteswrt;
//	uint32_t Address;
	uint32_t Alignn;
	bool res = false;
	size_t	i = 0;	
	
//	union onepage	SrcBuff;
//	union onepage	DstBuff;

	if(!pPeri) return(false);
	
	EEDst += _CHIP_IEEPROM_START;													// compute eeprom base address + offset 
	
	Alignn = EEDst % 4;																// check for align

	pDestination = (uint32_t *) ((uint32_t)(EEDst - Alignn));
	pSource = (uint32_t *) ((uint32_t)( pInBuff - Alignn));

	
//	Address = (uint32_t) pInBuff;
//	Alignn = Address % 4;
	
	while(TotalBytes)
	{
//		if(Alignn)																	// Source address is not arranged to 4
//		{					// rozsekaj pbuf na tmp buff
//			// copy pbuff to aligned buff - one page
//			while((i < sizeof(SrcBuff.byte)) && TotalBytes)
//			{
//				SrcBuff.byte[i++] = (uint8_t) *pInBuff++;
//				TotalBytes --;
//			}
//			Source = (uint32_t) &SrcBuff.word[0];
//		}
//		else 			// prenes cely buf na jeden zatah
//		{	
			i = byteswrt;
//			Source = (uint32_t) &pInBuff;
//		}
		
		((_CHIP_EEPROM_T*) pPeri)->INTSTATCLR = EEPROM_INTENSET_PROG_SET_EN_MASK;	// clear status
		
        if (((_CHIP_EEPROM_T*) pPeri)->AUTOPROG != EEPROM_AUTOPROG_AUTOPROG(0))	// Auto program disabled?
        {
            ((_CHIP_EEPROM_T*) pPeri)->AUTOPROG = EEPROM_AUTOPROG_AUTOPROG(1);		// set word autorprog
        }

		for (size_t cc = 0; cc < i; cc++) 
			pDestination[cc] = *pSource;
		
		
        /* Check if manual program erase is needed. */
        if (((_CHIP_EEPROM_T*) pPeri)->AUTOPROG != EEPROM_AUTOPROG_AUTOPROG(1))	// Auto program triggered after 1 word is written
        {
            ((_CHIP_EEPROM_T*) pPeri)->CMD = EEPROM_CMD_CMD(0x6);					// erase/program page cmd
        }
		
		while ((((_CHIP_EEPROM_T*) pPeri)->INTSTAT & EEPROM_INTENSET_PROG_SET_EN_MASK) == 0UL)										// wait for program finish status
		{
		}
		
		res = true;


		if(res == false) break;														// error - exit
		else TotalBytes -= i;														// minus correctly writes
	}
	return (res);

//	if(pPeri) return(_Chip_IAP_Write_EEPROM(EEDst, (uint32_t) pInBuff, (uint32_t) byteswrt));
//	else return(false);
}

// ------------------------------------------------------------------------------------------------------
// Read data from EEPROM - using IAP
// WARNING! LPC546xx can read mnimal one page (128 bytes) only!

bool _Chip_EEPROM_Read(void *pPeri, uint32_t EESrc, uint8_t *pOutBuff, size_t bytesrd)
{
	union onepage	EE_Page;
	bool res;
	uint8_t Alignn, CurrentPage;
	
//Pri normalnom behu tu aplikacia zhavaruje pri pokuse o zmenu adresy. 	Asi mi sem vleti prerusenie
	
	SYS_ASSERT(pPeri != 0);															// check - in release will be ASSERT skipped
	SYS_ASSERT(bytesrd != 0);														// check - in release will be ASSERT skipped

	Alignn = EESrc % 4;																// check for align
	CurrentPage = EESrc/_CHIP_EEPROM_PAGESIZE;										// select start page
	
	if(bytesrd)																		// read max one word
	{
		res = _Chip_IAP_Read_EEPROM_Page( CurrentPage, (uint32_t) &EE_Page);	// read 1 page (128 bytes) to uint32 dst

		if(bytesrd > _CHIP_EEPROM_PAGESIZE) 										// read more than one page?
		{
			memcpy( &pOutBuff[CurrentPage * _CHIP_EEPROM_PAGESIZE], &EE_Page.byte[Alignn], _CHIP_EEPROM_PAGESIZE - Alignn);	// copy with align to destination array
			bytesrd -= _CHIP_EEPROM_PAGESIZE;
			CurrentPage ++;															// select next page
		}
		else
		{			
			memcpy( &pOutBuff[CurrentPage * _CHIP_EEPROM_PAGESIZE], &EE_Page.byte[Alignn], bytesrd);	// copy with align to destination array
			bytesrd = 0;
		}
	}
	return(res);
}
//ZMENA ADRESY SPOSOBI BREAK. Asi este kombinacia write a read eeprom



// ******************************************************************************************************
// SCT Functions 
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------
// Return Index from pointer
int32_t _Chip_SCT_Get_PeriIndex(void *pPeri)
{
	if((_CHIP_SCT_T*) pPeri == SCT0) return(0);
	return(-1);																		// otherwise error
}
// ------------------------------------------------------------------------------------------------------
// SCT(Num) Initialization
// return is pointer to periphery, otherwise NULL
void *_Chip_SCT_Init(int32_t PeriIndex)
{
	// Check:
	SYS_ASSERT(PeriIndex == 0);														// check - in release will be ASSERT skipped
	//if(PeriIndex != 0) return(NULL);
	
	// Power:
	// Nothing to power On/Off
	
	// Clock:
	SYSCON->AHBCLKCTRL[1] |= SYSCON_AHBCLKCTRL_SCT0(1);								// Enable system clock for SCT

	// Reset:
	SYSCON->PRESETCTRL[1] |= SYSCON_PRESETCTRL_SCT0_RST(1);__nop();					// Activate reset
	SYSCON->PRESETCTRL[1] &= ~(SYSCON_PRESETCTRL_SCT0_RST(1));						// Deactivate reset

	// Configure
	SYSCON->SCTCLKSEL = SYSCON_SCTCLKSEL_SEL(1);									// Select PLL as clk source
	
	//Chip_Clock_SetupSCTPLL(1, 3);
	//SYSCON->PDRUNCFG &= !(1 << 24);												// Power Up
	
	//while (!Chip_Clock_IsSCTPLLLocked()) {}										// Wait for PLL to lock

	return(SCT0);
}

// ------------------------------------------------------------------------------------------------------
// SCT(Num) Enable/disable
// result is true:successfull otherwise false
bool _Chip_SCT_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	if(NewState)
	{
		((_CHIP_SCT_T*)pPeri)->CTRL &= ~(SCT_CTRL_HALT_L(1) | SCT_CTRL_HALT_H(1));	// Activate SCT
	}
	else
	{
		((_CHIP_SCT_T*)pPeri)->CTRL |= (SCT_CTRL_HALT_L(1) | SCT_CTRL_HALT_H(1));	// Deactivate SCT
	}	
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// SCT(PeriIndex) Configure
bool _Chip_SCT_Configure(void *pPeri)
{
	//_CHIP_SCT_T *pPeri = _Chip_SCT_Get_pPeri(PeriIndex);
	return(true);
}



// ******************************************************************************************************
// ETH Functions 
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// ETH(Num) Initialization
// return is pointer to periphery, otherwise NULL
void *_Chip_ETH_Init(int32_t PeriIndex)
{
		// Check:
	SYS_ASSERT(PeriIndex == 0);														// check - in release will be ASSERT skipped
	//if(PeriIndex != 0) return( NULL );											// LPC546xx have only one ETH

#if 1	
	// Power:
	// this is a special Power-On sequence!
	
	// Clock:
	SYSCON->AHBCLKCTRL[2] |= SYSCON_AHBCLKCTRL_ETH(1);								// Enable system clock for ETH

	// if RX_CLK is without clock signal from PHY, MCU will freeze !!!!!!!!!!!!
	
	// Reset:
	SYSCON->PRESETCTRL[2] |=  SYSCON_PRESETCTRL_ETH_RST(1);							// assert reset
	SYSCON->PRESETCTRL[2] &= ~(SYSCON_PRESETCTRL_ETH_RST(1));						// dassert reset
#endif	

	return((void *) ENET);
}	

// ------------------------------------------------------------------------------------------------------
// ETH Enable/disable
// result is true:successfull otherwise false
bool _Chip_ETH_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	
	if(NewState)
	{
		NVIC_ClearPendingIRQ(ETHERNET_IRQn);										// clear pending interrupt flag
		NVIC_EnableIRQ((IRQn_Type) (ETHERNET_IRQn));								// Enable ETH interrupt
	}
	else
	{
		NVIC_DisableIRQ((IRQn_Type) (ETHERNET_IRQn));								// Disable ETH interrupt
	}
	return(true);
}



// ******************************************************************************************************
// LCD Functions 
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// LCD(Num) Initialization
// return is pointer to periphery, otherwise NULL
void *_Chip_LCD_Init(int32_t PeriIndex)
{
	// Check:
	SYS_ASSERT(PeriIndex == 0);														// check - in release will be ASSERT skipped
	//if(PeriIndex != 0) return( NULL );											// Check for valid index of periphery

	// Power:
	// nothing
	
	// Clock:
	SYSCON->AHBCLKCTRL[2] |= SYSCON_AHBCLKCTRL_LCD(1);								// Enable system clock for LCD
	SYSCON->LCDCLKSEL = 0;															// select main clock for LCD
	SYSCON->LCDCLKDIV = 0;															// Set divider to 1 and run it
	
	// Reset:
	SYSCON->PRESETCTRL[2] |= SYSCON_PRESETCTRL_LCD_RST(1);__nop();					// Activate reset LCD
	SYSCON->PRESETCTRL[2] &= ~SYSCON_PRESETCTRL_LCD_RST(1);							// Deactivate reset
		
	return(LCD);																	// return pointer to perihery
}


// ------------------------------------------------------------------------------------------------------
// LCD Enable/disable
// result is true:successfull otherwise false
bool _Chip_LCD_Enable(void *pPeri, bool NewState)
{
	if(NewState == true) 
	{
		NVIC_ClearPendingIRQ(LCD_IRQn);												// clear pending interrupt flag
		//((_CHIP_LCD_T *)pPeri)->CTRL |= (1 << 0);									// LCD Enable
		LCD->CTRL |= (1 << 0);														// LCD Enable
	}
	else
	{		
		//((_CHIP_LCD_T *)pPeri)->CTLR &= ~(1 << 0);								// LCD Disable
		LCD->CTRL &= ~(1 << 0);														// LCD Disable
	}
	return (true);
}


// ------------------------------------------------------------------------------------------------------
// LCD Configure
// result is true:successfull otherwise false
//bool _Chip_LCD_Configure(void *pPeri, uint32_t Speed_kHz, bool MasterMode, bool SlaveMode)
bool _Chip_LCD_Configure(void *pPeri)
{
	return(true);
}





// ************** MUSTER ******************

//void *_Chip_xxx_Init(int32_t PeriIndex)
//{
	// Check:
	//SYS_ASSERT(pPeri != NULL);													// check - in release will be ASSERT skipped

	// Power:
	//SYSCON->PDRUNCFG &= ~SYSCON_PDRUNCFG_PDEN_xxx(1);								// Disable power down for xxx
	
	// Clock:
	//SYSCON->AHBCLKCTRLn |= SYSCON_AHBCLKCTRL_xxx(1);								// Enable system clock for xxx
	
	// Reset:
	//SYSCON->PRESETCTRLn |= SYSCON_PRESETCTRL_xxx_RST(1);__nop();					// Activate reset xxx
	
	//return(xxx);																	// return pointer to perihery
//}


//bool _Chip_xxx_Enable(void * pPeri, uint32_t ChannelBitMask, bool NewState)
//{
//	// Check:
//	SYS_ASSERT(pPeri != NULL);														// check
//	int32_t PeriIndex = _Chip_ADC_Get_PeriIndex(pPeri);								// return peripherial index
//	
//	if(NewState == true)
//	{
//		((_CHIP_XXX_T *)pPeri)->CTRL |= bit(1);										// Enable and Start periphery
//	}
//	else
//	{
//		((_CHIP_XXX_T *)pPeri)->CTRL &= ~bit(1);									// Disable periphery
//	}
//	return (true);
//}


//bool _Chip_xxx_Enable_IRQ_NVIC(void *pPeri, bool NewState)
//{
//	// Check:
//	SYS_ASSERT(pPeri != NULL);														// check
//	
//	if(NewState == true)
//	{
//		NVIC_ClearPendingIRQ (xxx_IRQn);											// Clear NVIC interrupt flag
//		NVIC_EnableIRQ(xxx_IRQn);													// Enable NVIC interrupt
//	}
//	else
//	{
//		NVIC_DisableIRQ((IRQn_Type) (xxx_IRQn));									// Disable NVIC interrupt
//	}
//	return(true);
//}

#endif

