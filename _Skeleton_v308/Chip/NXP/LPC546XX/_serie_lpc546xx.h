// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: _serie_lpc546xx.h
// 	   Version: 3.2
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: MCU of series LPC546xx description/configuration file. Same for all MCUs of this family!
//              Special, device depents features, can be included outside of this file (in parent header)        
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:	
//				2023.10.04	- v3.2 - move all extern inline into this .c file. Clean all code
//				2020.10.01	- added _Chip_UART_Get_Address
//				2020.08.04	- I2C access divided to master, slave, monitor and timeouts
//				2020.07.15	- Added UART FLUSH commands: _Chip_UART_Flush_.x
//				2020.07.14	- Added FIFO TH level control - _Chip_UARTFIFO_.et_Y.THLevel
//				2020.07.02	- variable Result was not initialized in GetIRQStatus.
//				2020.07.01	- Calling of Ena/Disa NVIC IRQ rewriten
//							- rename Old: _Chip_xxx_IRQ_Enable  ----->  _Chip_xxx_Enable_IRQ_NVIC
//							- rename Old: _Chip_xxx_IRQMask_Enable --> 	_Chip_xxx_IRQ_Peri_Enable
//							- rename Old: _Chip_xxx_IRQ_Enable_CHIRQ -> _Chip_xxx_IRQ_Peri_Enable  also parameters renamed
//							- rename Old: _Chip_xxx_Get_Status ------> _Chip_xxx_Get_Peri_Status

#ifndef __SERIE_546XX_H_
#define __SERIE_546XX_H_


// Configuration
#ifndef CONF_SYS_WATCHDOG_USED
	#define	CONF_SYS_WATCHDOG_USED			0										// Default Off
	#warning "Watchdog OFF!"
#endif


#ifndef LOAD_SCATTER
    extern uint32_t SystemCoreClock;												// for IAP - System Clock Frequency (Core Clock)
    extern uint32_t	OscRateIn;
    extern uint32_t RTCOscRateIn;
		
	// for compatibility with mfg's header file
    typedef enum {ERROR = 0, SUCCESS = !ERROR} Status;
    typedef enum {RESET = 0, SET = !RESET} FlagStatus, IntStatus, SetState;
    typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;
	
#if   defined( CONF_CHIP_ID_LPC54605J256ET180 ) || \
	  defined( CONF_CHIP_ID_LPC54605J512ET180 ) || \
	  defined( CONF_CHIP_ID_LPC54605J256BD100 ) || \
	  defined( CONF_CHIP_ID_LPC54605J512BD100 ) || \
	  defined( CONF_CHIP_ID_LPC54605J256ET100 ) || \
	  defined( CONF_CHIP_ID_LPC54605J512ET100 )
    #include    "LPC54605.h" 														// Peripheral Access Layer Header File - provided by manufacturer
#elif defined( CONF_CHIP_ID_LPC54606J256ET100 ) || \
	  defined( CONF_CHIP_ID_LPC54606J512ET100 ) || \
	  defined( CONF_CHIP_ID_LPC54606J256BD100 ) || \
	  defined( CONF_CHIP_ID_LPC54606J512BD100 ) || \
	  defined( CONF_CHIP_ID_LPC54606J256ET180 ) || \
	  defined( CONF_CHIP_ID_LPC54606J512BD208 )
    #include    "LPC54606.h" 														// Peripheral Access Layer Header File - provided by manufacturer
#elif defined( CONF_CHIP_ID_LPC54607J256ET180 ) || \
	  defined( CONF_CHIP_ID_LPC54607J512ET180 ) || \
	  defined( CONF_CHIP_ID_LPC54607J256BD208 )
    #include    "LPC54607.h" 														// Peripheral Access Layer Header File - provided by manufacturer
#elif defined( CONF_CHIP_ID_LPC54608J512ET180 ) || \
	  defined( CONF_CHIP_ID_LPC54608J512BD208 )
    #include    "LPC54608.h" 														// Peripheral Access Layer Header File - provided by manufacturer
#elif defined( CONF_CHIP_ID_LPC54616J512ET100 ) || \
	  defined( CONF_CHIP_ID_LPC54616J512BD100 ) || \
	  defined( CONF_CHIP_ID_LPC54616J256ET180 ) || \
	  defined( CONF_CHIP_ID_LPC54616J512BD208 )
    #include    "LPC54616.h" 														// Peripheral Access Layer Header File - provided by manufacturer
#elif defined( CONF_CHIP_ID_LPC54618J512ET180 ) || \
	  defined( CONF_CHIP_ID_LPC54618J512BD208 )
    #include    "LPC54618.h" 														// Peripheral Access Layer Header File - provided by manufacturer
#elif defined( CONF_CHIP_ID_LPC54628J512ET180 )
    #include    "LPC54628.h" 														// Peripheral Access Layer Header File - provided by manufacturer
#else
	#error "Unknown selected device!"
#endif

#endif


// ------------------------------------------------------------------------------------------------
// PUBLIC Const Values for all LPC546XX MCU's
// ------------------------------------------------------------------------------------------------

#define	_CHIP_IRC_FREQUENCY				12000000UL									// Internal Oscillator(fro): 12MHz

// for whole family LPC546xx:
#define  _CHIP_ADC_COUNT				1											// Number of Analog-to-Digital Converter (ADC)
#define  _CHIP_CRC_COUNT				1											// Number of Cyclic Redundancy Check (CRC) generator engine

// define CTIMER as TIMER!!
#define  _CHIP_TIMER_COUNT				5											// Number of Standard counter/timer

#define  _CHIP_DMA_COUNT				1											// Number of Direct Memory Access peripheries
#define  _CHIP_HAVE_DMIC				1											// Digital microphone interface
#define  _CHIP_HAVE_EEPROM				1											// Digital microphone interface
#define  _CHIP_HAVE_EMC					1											// External memory controller
#define  _CHIP_HAVE_FMC					1											// Flash Controller
#define  _CHIP_HAVE_I2C					1											// A typical I2C-bus
#define  _CHIP_HAVE_I2S					1											// Inter-IC Sound interface
#define  _CHIP_HAVE_MRT					1											// Timer - MRT
#define  _CHIP_HAVE_OTPC				1											// One Time Program memory controller
#define  _CHIP_HAVE_RIT					1											// Repetitive Interrupt Timer
#define  _CHIP_HAVE_RTC					1											// Real Time Clock (RTC)
#define  _CHIP_HAVE_SCT					1											// State-Configurable Timers
#define  _CHIP_HAVE_SDIF				1											// SD/MMC Controller
#define  _CHIP_HAVE_SMARDCARD			1											// SmardCard Interface
#define  _CHIP_HAVE_SPIFI				1											// SPI Flash Interface
#define	 _CHIP_LCD_COUNT				1											// Number of LCDs

#define	 _CHIP_SPIFIFO_COUNT			1											// Number of SPI FIFOs
#define	 _CHIP_UARTFIFO_COUNT			1											// Number of UART FIFOs

#define  _CHIP_HAVE_USB					1											// Universal Serial Bus
#define  _CHIP_HAVE_UTICK				1											// mikro Tick Timer
#define  _CHIP_HAVE_WWDT				1											// Windowed Watchdog Timer (WWDT)
#define  _CHIP_HAVE_RNG					1											// Random Number Generator
#define  _CHIP_HAVE_SYSTICK				1											// System Tick Timer is an integral part of the Cortex-M3. The System Tick Timer is intended to generate a fixed 10 millisecond interrupt for use by an operating system or other system management software.
#define  _CHIP_HAVE_TEMP				1											// Temperature sensor


#define  _CHIP_SPI_COUNT				_CHIP_FLEXCOMM_COUNT						// Number of Serial Peripheral Interface Blocks = FlexCount
#define  _CHIP_UART_COUNT				_CHIP_FLEXCOMM_COUNT						// Universal asynchronous receiver-transmitter = FlexCount

#define IAP_ENTRY_LOCATION        		0x03000205									// Pointer to ROM IAP entry functions

#define	 _CHIP_DMA_CHANNELS_COUNT		DMA_CHANNEL_XFERCFG_COUNT					// number of channels per DMA
#define  _CHIP_MAXIMUM_CPU_CLK_FREQ		(220000000UL)								// maximum clock freq: 220 MHz

#define	_CHIP_EEPROM_INTERNAL_FREQ 		(1500000)									// EEPROM internal clock freqency
#define	_CHIP_EEPROM_PAGESIZE 			(128)										// EEPROM page size


// Specific parameters for each chip from this series:
#if defined( CONF_CHIP_ID_LPC54605J256ET180 )
	// for LPC54605J256ET180
    #define  CHIP_ID                    0x7F954605                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		10											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			5											// Number of General purpose IO blocks

	#define  _CHIP_HAVE_CAN				0											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				0											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54605J512ET180 )
	// for LPC54605J512ET180
    #define  CHIP_ID                    0xFFF54605                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		9											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			5											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				0											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				0											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54605J256BD100 )
	// for LPC54605J256BD100
    #define  CHIP_ID                    0x7F954605                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		9											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			2											// Number of General purpose IO blocks

	#define  _CHIP_HAVE_CAN				0											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				0											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54605J512BD100 )
	// for LPC54605J512BD100
    #define  CHIP_ID                    0xFFF54605                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		9											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			2											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				0											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				0											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54605J256ET100 )
	// for LPC54605J256ET100
    #define  CHIP_ID                    0x7F954605                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		9											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			2											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				0											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				0											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54605J512ET100 )
	// for LPC54605J512ET100
    #define  CHIP_ID                    0xFFF54605                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		9											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			2											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				0											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				0											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54606J256ET100 )
	// for LPC54606J256ET100
    #define  CHIP_ID                    0x7F954606                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		9											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			2											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54606J512ET100 )
	// for LPC54606J512ET100
    #define  CHIP_ID                    0xFFF54606                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		9											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			2											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54606J256BD100 )
	// for LPC54606J256BD100
    #define  CHIP_ID                    0x7F954606                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		9											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			2											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54606J512BD100 )
	// for LPC54606J512BD100
    #define  CHIP_ID                    0xFFF54606                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		9											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			2											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54606J256ET180 )
	// for LPC54606J256ET180
    #define  CHIP_ID                    0x7F954606                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		10											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			5											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54606J512BD208 )
	// for LPC54606J512BD208
    #define  CHIP_ID                    0xFFF54606                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		10											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			6											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54607J256ET180 )
	// for LPC54607J256ET180
    #define  CHIP_ID                    0x7F954607                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		10											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			5											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				0											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				0											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54607J512ET180 )
	// for LPC54607J512ET180
    #define  CHIP_ID                    0xFFF54607                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		10											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			5											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				0											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				0											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54607J256BD208 )
	// for LPC54607J256BD208
    #define  CHIP_ID                    0x7F954607                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		10											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			6											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				0											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				0											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54608J512ET180 )
	// for LPC54608J512ET180
    #define  CHIP_ID                    0xFFF54608                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		10											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			5											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54608J512BD208 )
	// for LPC54608J512BD208
    #define  CHIP_ID                    0xFFF54608                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		10											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			6											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54616J512ET100 )
	// for LPC54616J512ET100
    #define  CHIP_ID                    0xFFF54616                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		9											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			2											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54616J512BD100 )
	// for LPC54616J512BD100
    #define  CHIP_ID                    0xFFF54616                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		9											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			2											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54616J256ET180 )
	// for LPC54616J256ET180
    #define  CHIP_ID                    0x7F954616                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		10											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			5											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54616J512BD208 )
	// for LPC54616J512BD208
    #define  CHIP_ID                    0xFFF54616                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		10											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			6											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54618J512ET180 )
	// for LPC54618J512ET180
    #define  CHIP_ID                    0xFFF54618                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		10											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			5											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm

#elif defined( CONF_CHIP_ID_LPC54618J512BD208 )
	// for LPC54618J512BD208
    #define  CHIP_ID                    0xFFF54618                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x8000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x04000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		10											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			6											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				1											// Ethernet
	#define  _CHIP_HAVE_SHA				0											// Secure Hash Algorithm
	
#elif defined( CONF_CHIP_ID_LPC54628J512ET180 )
	// for LPC54628J512ET180
    #define  CHIP_ID                    0xFFF54628                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x28000										// size of internal RAM (datasheet value) - up to 200kB
	#define	 _CHIP_IRAMSTART			0x20000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x80000										// size of internal FLASH (datasheet value) - 512kB
	#define	 _CHIP_IEEPROM				0x4000										// size of internal EEPROM (datasheet value) - 16kB	
	#define	 _CHIP_IEEPROM_START		0x40108000									// internal EEPROM start address
	#define	 _CHIP_ADC_CHANNELS_COUNT	12											// number of channels per ADC
	#define  _CHIP_FLEXCOMM_COUNT		10											// Number of Flexcomm interface serial communication
	#define  _CHIP_GPIO_COUNT			5											// Number of General purpose IO blocks
	
	#define  _CHIP_HAVE_CAN				1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
	#define  _CHIP_HAVE_ETH				1											// Ethernet
 	#define  _CHIP_HAVE_SHA				1											// Secure Hash Algorithm
	

#else
	#error "Unknown selected device!"
#endif

// Define _CHIP_CTIMER0_IRQ_Handler as _CHIP_TIMER0_IRQ_Handler !!!

// ------------------------------------------------------------------------------------------------
// Interrupt handlers assign
// ------------------------------------------------------------------------------------------------
	#define	 IRQHANDLER_16				_CHIP_WDT0_BOD0_IRQ_Handler					// IRQ 0
	#define	 IRQHANDLER_17				_CHIP_DMA0_IRQ_Handler						// IRQ 1
	#define	 IRQHANDLER_18				_CHIP_GINT0_IRQ_Handler						// IRQ 2
	#define	 IRQHANDLER_19				_CHIP_GINT1_IRQ_Handler						// IRQ 3
	#define	 IRQHANDLER_20				_CHIP_PINT0_IRQ_Handler						// IRQ 4
	#define	 IRQHANDLER_21				_CHIP_PINT1_IRQ_Handler						// IRQ 5
	#define	 IRQHANDLER_22				_CHIP_PINT2_IRQ_Handler						// IRQ 6
	#define	 IRQHANDLER_23				_CHIP_PINT3_IRQ_Handler						// IRQ 7
	#define	 IRQHANDLER_24				_CHIP_UTICK0_IRQ_Handler					// IRQ 8
	#define	 IRQHANDLER_25				_CHIP_MRT0_IRQ_Handler						// IRQ 9
	#define	 IRQHANDLER_26				_CHIP_TIMER0_IRQ_Handler					// IRQ 10
	#define	 IRQHANDLER_27				_CHIP_TIMER1_IRQ_Handler					// IRQ 11
	#define	 IRQHANDLER_28				_CHIP_SCT0_IRQ_Handler						// IRQ 12
	#define  IRQHANDLER_29				_CHIP_TIMER3_IRQ_Handler					// IRQ 13
	#define	 IRQHANDLER_30				_CHIP_FLEXCOMM0_IRQ_Handler					// IRQ 14
	#define	 IRQHANDLER_31				_CHIP_FLEXCOMM1_IRQ_Handler					// IRQ 15
	#define	 IRQHANDLER_32				_CHIP_FLEXCOMM2_IRQ_Handler					// IRQ 16
	#define	 IRQHANDLER_33				_CHIP_FLEXCOMM3_IRQ_Handler					// IRQ 17
	#define	 IRQHANDLER_34				_CHIP_FLEXCOMM4_IRQ_Handler					// IRQ 18
	#define	 IRQHANDLER_35				_CHIP_FLEXCOMM5_IRQ_Handler					// IRQ 19
	#define	 IRQHANDLER_36				_CHIP_FLEXCOMM6_IRQ_Handler					// IRQ 20
	#define	 IRQHANDLER_37				_CHIP_FLEXCOMM7_IRQ_Handler					// IRQ 21
	#define	 IRQHANDLER_38				_CHIP_ADC0_SEQA_IRQ_Handler					// IRQ 22
	#define	 IRQHANDLER_39				_CHIP_ADC0_SEQB_IRQ_Handler					// IRQ 23
	#define	 IRQHANDLER_40				_CHIP_ADC0_THCMP_IRQ_Handler				// IRQ 24
	#define	 IRQHANDLER_41				_CHIP_DMIC0_IRQ_Handler						// IRQ 25
	#define	 IRQHANDLER_42				_CHIP_HVAD0_IRQ_Handler						// IRQ 26
	#define	 IRQHANDLER_43				_CHIP_USB0_NEEDCLK_IRQ_Handler				// IRQ 27
	#define	 IRQHANDLER_44				_CHIP_USB0_IRQ_Handler						// IRQ 28
	#define	 IRQHANDLER_45				_CHIP_RTC0_IRQ_Handler						// IRQ 29
	#define	 IRQHANDLER_46				_Chip_Default_IRQ_Handler					// IRQ 30	reserved
	#define	 IRQHANDLER_47				_Chip_Default_IRQ_Handler					// IRQ 31	reserved
	#define	 IRQHANDLER_48				_CHIP_PINT4_IRQ_Handler						// IRQ 32
	#define	 IRQHANDLER_49				_CHIP_PINT5_IRQ_Handler						// IRQ 33
	#define	 IRQHANDLER_50				_CHIP_PINT6_IRQ_Handler						// IRQ 34
	#define	 IRQHANDLER_51				_CHIP_PINT7_IRQ_Handler						// IRQ 35
	#define	 IRQHANDLER_52				_CHIP_TIMER2_IRQ_Handler					// IRQ 36
	#define	 IRQHANDLER_53				_CHIP_TIMER4_IRQ_Handler					// IRQ 37
	#define	 IRQHANDLER_54				_CHIP_RIT0_IRQ_Handler						// IRQ 38
	#define	 IRQHANDLER_55				_CHIP_SPIFI0_IRQ_Handler					// IRQ 39
	#define	 IRQHANDLER_56				_CHIP_FLEXCOMM8_IRQ_Handler					// IRQ 40
	#define	 IRQHANDLER_57				_CHIP_FLEXCOMM9_IRQ_Handler					// IRQ 41
	#define	 IRQHANDLER_58				_CHIP_SDIO0_IRQ_Handler						// IRQ 42
	#define	 IRQHANDLER_59				_CHIP_CAN00_IRQ_Handler						// IRQ 43
	#define	 IRQHANDLER_60				_CHIP_CAN01_IRQ_Handler						// IRQ 44
	#define	 IRQHANDLER_61				_CHIP_CAN10_IRQ_Handler						// IRQ 45
	#define	 IRQHANDLER_62				_CHIP_CAN11_IRQ_Handler						// IRQ 46
	#define	 IRQHANDLER_63				_CHIP_USB1_IRQ_Handler						// IRQ 47
	#define	 IRQHANDLER_64				_CHIP_USB1_NEEDCLK_IRQ_Handler				// IRQ 48
	#define	 IRQHANDLER_65				_CHIP_ETH0_IRQ_Handler						// IRQ 49
	#define	 IRQHANDLER_66				_CHIP_ETH0_PMT_IRQ_Handler					// IRQ 50
	#define	 IRQHANDLER_67				_CHIP_ETH0_MACLP_IRQ_Handler				// IRQ 51
	#define	 IRQHANDLER_68				_CHIP_EEPROM0_IRQ_Handler					// IRQ 52
	#define	 IRQHANDLER_69				_CHIP_LCD0_IRQ_Handler						// IRQ 53
	#define	 IRQHANDLER_70				_CHIP_SHA0_IRQ_Handler						// IRQ 54
	#define	 IRQHANDLER_71				_CHIP_SMARDCARD0_IRQ_Handler				// IRQ 55
	#define	 IRQHANDLER_72				_CHIP_SMARDCARD1_IRQ_Handler				// IRQ 56
#ifndef LOAD_SCATTER

// ------------------------------------------------------------------------------------------------
// Validate the the user's selctions
// ------------------------------------------------------------------------------------------------
#ifndef CONF_CHIP_OSCRATEIN
	#error "CONF_CHIP_OSCRATEIN not defined"
#endif
//#ifndef CONF_CHIP_EXTRATEIN
//	#error "CONF_CHIP_EXTRATEIN not defined"
//#endif
#ifndef CONF_CHIP_RTCRATEIN
	#error "CONF_CHIP_RTCRATEIN not defined"
#endif

// ------------------------------------------------------------------------------------------------
// PUBLIC Values for all LPC546XX MCU's
// ------------------------------------------------------------------------------------------------

#define CPU_NONISR_EXCEPTIONS   	    (15)										// Cortex M3 exceptions /without SP!/
#define CPU_IRQ_NUMOF 				    (NUMBER_OF_INT_VECTORS)						// Vendor and family specific external interrupts. See users manual!


// harmonizing names
// Some manufacturers header files, using different peripherial structure names. Now, we harmonize it into one names.
// now used from chip header file:		Harmonization to:
typedef ADC_Type						_CHIP_ADC_T;
typedef ASYNC_SYSCON_Type				_CHIP_ASSYSCON_T;
typedef CAN_Type						_CHIP_CAN_T;
typedef CRC_Type						_CHIP_CRC_T;
typedef CTIMER_Type						_CHIP_CTIMER_T;
#define _CHIP_TIMER_T					_CHIP_CTIMER_T
typedef DMA_Type						_CHIP_DMA_T;
typedef DMIC_Type						_CHIP_DMIC_T;
typedef EEPROM_Type						_CHIP_EEPROM_T;
typedef EMC_Type						_CHIP_EMC_T;
typedef ENET_Type						_CHIP_ETH_T;
typedef FLEXCOMM_Type					_CHIP_FLEXCOMM_T;
typedef FMC_Type						_CHIP_FMC_T;
typedef GINT_Type						_CHIP_GINT_T;
typedef GPIO_Type						_CHIP_GPIO_T;
typedef I2C_Type						_CHIP_I2C_T;
typedef I2S_Type						_CHIP_I2S_T;
typedef INPUTMUX_Type					_CHIP_INMUX_T;
typedef IOCON_Type						_CHIP_IOCON_T;
typedef LCD_Type						_CHIP_LCD_T;
typedef MRT_Type						_CHIP_MRT_T;
typedef OTPC_Type						_CHIP_OTPC_T;
typedef PINT_Type						_CHIP_PINT_T;
typedef RIT_Type						_CHIP_RIT_T;
typedef RTC_Type						_CHIP_RTC_T;
typedef SCT_Type						_CHIP_SCT_T;
typedef SDIF_Type						_CHIP_SDIF_T;
typedef SMARTCARD_Type					_CHIP_SMARTCARD_T;
typedef SPI_Type						_CHIP_SPI_T;
typedef SPIFI_Type						_CHIP_SPIFI_T;
typedef SYSCON_Type						_CHIP_SYSCON_T;
typedef USART_Type						_CHIP_UART_T;
typedef USB_Type						_CHIP_USB_T;
typedef USBFSH_Type						_CHIP_USBFSH_T;
typedef USBHSD_Type						_CHIP_USBHSD_T;
typedef USBHSH_Type						_CHIP_USBHSH_T;
typedef UTICK_Type						_CHIP_UTICK_T;
typedef WWDT_Type						_CHIP_WWDT_T;
typedef OTP_API_Type					_CHIP_OTPAPI_T;
  
typedef SysTick_Type					_CHIP_SYSTICK_T;

#if defined( CONF_CHIP_ID_LPC54628J512ET180 )
typedef SHA_Type						_CHIP_SHA_T;
#endif


// ******************************************************************************************************
// CHIP functions
// ******************************************************************************************************

enum	_swo_protocol 
{
  kSWO_ProtocolManchester = 1U,
  kSWO_ProtocolNrz = 2U
};

// Public functions exactly for this series of MCU, but uniform with CHAL System:
extern void _Chip_SWO_Init(uint32_t Main_CPU_Clock);
extern void _Chip_SystemInit(void);
extern void _Chip_Read_ResetSource(uint32_t *dst);

// xxxxx_lib_powr.lib - Power Library API:
void POWER_SetVoltageForFreq(uint32_t freq);										// to choose normal regulation and set the voltage for the desired operating frequency.
uint32_t POWER_GetLibVersion(void);													// to return the library version.
void POWER_EnterDeepSleep(uint64_t exclude_from_pd);								// to enter deep sleep mode
void POWER_EnterSleep(void);														// to enter different power mode.
void POWER_SetUsbPhy(void);															// to power the USB PHY.
void POWER_SetPLL(void);															// to power the PLLs.






// ******************************************************************************************************
// IAP CHIP functions
// ******************************************************************************************************

// IAP command definitions
#define _CHIP_IAP_PREWRRITE_CMD         50											// Prepare sector for write operation command
#define _CHIP_IAP_WRISECTOR_CMD         51											// Write Sector command from RAM to FLASH
#define _CHIP_IAP_ERSSECTOR_CMD         52											// Erase Sector command
#define _CHIP_IAP_BLANK_CHECK_SECTOR_CMD  53										// Blank check sector
#define _CHIP_IAP_REPID_CMD             54											// Read PartID command
#define _CHIP_IAP_READ_BOOT_CODE_CMD    55											// Read Boot code version
#define _CHIP_IAP_COMPARE_CMD           56											// Compare two RAM address locations
#define _CHIP_IAP_REINVOKE_ISP_CMD      57											// Reinvoke ISP
#define _CHIP_IAP_READ_UID_CMD          58											// Read UID
#define _CHIP_IAP_ERASE_PAGE_CMD        59											// Erase page
#define _CHIP_IAP_READ_SIG_CMD			70											// Read Signature
#define _CHIP_IAP_EXTREAD_SIG_CMD		73											// Extended Read Signature
#if defined(FSL_FEATURE_IAP_HAS_FAIM_FUNCTION) && FSL_FEATURE_IAP_HAS_FAIM_FUNCTION
 #define  _CHIP_IAP_READ_FAIMPAGE_CMD	80	 										// Read FAIM page
 #define  _CHIP_IAP_WRITE_FAIMPAGE_CMD	81 											// Write FAIM page
#else
 #define  _CHIP_IAP_EEPROM_READPAGE_CMD	80 											// Read EEPROM page
 #define  _CHIP_IAP_EEPROM_WRITEPAGE_CMD 81											// Write EEPROM page
#endif 																				// FSL_FEATURE_IAP_HAS_FAIM_FUNCTION


// IAP_ENTRY API function type
typedef void (*IAP_ENTRY_T)(unsigned int[], unsigned int[]);

static inline void _Chip_IAP_Entry(unsigned int cmd_param[], unsigned int status_result[])
{
	((IAP_ENTRY_T) IAP_ENTRY_LOCATION)(cmd_param, status_result);
}

// Public functions exactly for this series of MCU
extern bool _Chip_IAP_Read_ID(uint32_t *pDst);
extern bool _Chip_IAP_Read_SerialNum(uint32_t *pDst);
extern bool _Chip_IAP_Read_BootCodeVersion(uint32_t *pDst);






// ------------------------------------------------------------------------------------------------
// CLOCK functions
// ------------------------------------------------------------------------------------------------
#if defined(CONF_SYS_WATCHDOG_USED) && (CONF_SYS_WATCHDOG_USED == 1)
static const uint8_t wdtFreqLookup[32] = {0,  60, 105, 140, 175, 210, 240, 270, 300, 325, 350, 375, 400, 420, 440, 460};
#endif
										  
#define NVALMAX 						(0x100U)
#define PVALMAX 						(0x20U)
#define MVALMAX 						(0x8000U)
#define PLL_MAX_N_DIV 					0x100U
#define USB_PLL_MAX_N_DIV 				0x100U

#define PLL_MDEC_VAL_P 					(0U)                          				// MDEC is in bits  16 downto 0
#define PLL_MDEC_VAL_M 					(0x1FFFFUL << PLL_MDEC_VAL_P) 				// NDEC is in bits  9 downto 0
#define PLL_NDEC_VAL_P 					(0U)                          				// NDEC is in bits  9:0
#define PLL_NDEC_VAL_M 					(0x3FFUL << PLL_NDEC_VAL_P)
#define PLL_PDEC_VAL_P 					(0U) 										// PDEC is in bits 6:0
#define PLL_PDEC_VAL_M 					(0x7FUL << PLL_PDEC_VAL_P)

// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------
//
__ASM static inline void DelayLoop(uint32_t count)
{
loop
    SUBS R0, R0, #1
    CMP  R0, #0
    BNE  loop
    BX   LR
}

// ------------------------------------------------------------------------------------------------
//
static inline void DelayChipUs(uint32_t delay_us, uint32_t coreClock_Hz)
{
    SYS_ASSERT(delay_us != 0);
	SYS_ASSERT(coreClock_Hz != 0);
	
	uint64_t count = (((uint64_t)(delay_us) * (coreClock_Hz)) / 1000000U);
    SYS_ASSERT(count <= UINT32_MAX);

    /* Divide value may be different in various environment to ensure delay is precise.
     * Every loop count includes three instructions, due to Cortex-M7 sometimes executes
     * two instructions in one period, through test here set divide 2. Other M cores use
     * divide 4. By the way, divide 2 or 4 could let odd count lost precision, but it does
     * not matter because other instructions outside while loop is enough to fill the time.
     */
#if (__CORTEX_M == 7)
    count = count / 2U;
#else
    count = count / 4U;
#endif
    DelayLoop((uint32_t)count);
}

extern uint32_t _Chip_Clock_Get_MainClk_Rate(void);
extern uint32_t _Chip_Clock_Get_Wdt_Inp_Rate(void);
extern uint32_t _Chip_Clock_Get_FroHf_Out_Rate(void);
extern uint32_t _Chip_Clock_Get_Pll_In_Rate(void);
extern uint32_t _Chip_Clock_Get_Pll_Out_Rate(void);
extern uint32_t _Chip_Clock_Get_UsbPll_Out_Rate(void);
extern uint32_t _Chip_Clock_Get_AudioPLL_In_Rate(void);
extern uint32_t _Chip_Clock_Get_AudioPll_Out_Rate(void);
extern uint32_t _Chip_Clock_Get_SourceClk_Rate(void);
extern uint32_t _Chip_Clock_Get_CoreClk_Rate(void);
extern uint32_t _Chip_Clock_Get_FRG_In_Rate(void);
extern uint32_t _Chip_Clock_Get_Frg_Out_Rate(void);
extern uint32_t _Chip_Clock_Get_ClkOut_Rate(void);
extern uint32_t _Chip_Clock_Get_UsbClk_Rate(void);	
extern int32_t _Chip_Clock_Get_SCT_Inp_Rate(void);
extern uint32_t _Chip_Clock_Get_Mclk_Rate(void);
extern uint32_t _Chip_Clock_Get_FlexCommClk_Rate(uint32_t id);
extern uint32_t _Chip_Clock_Get_DmicClk_Rate(void);
extern uint32_t _Chip_Clock_Get_ADCClk_Rate(void);
extern uint32_t _Chip_Clock_Get_LCDClk_Rate(void);
extern uint32_t _Chip_Clock_Get_AsyncAPB_Rate(void);
extern uint32_t _Chip_Clock_Get_Wdt_Inp_Rate(void);
extern void _Chip_SetFLASHAccessCyclesForFreq(uint32_t iFreq);
extern uint32_t _Chip_pllEncodeN(uint32_t N);
extern uint32_t _Chip_pllDecodeN(uint32_t NDEC);
extern uint32_t _Chip_pllEncodeP(uint32_t P);
extern uint32_t _Chip_pllDecodeP(uint32_t PDEC);
extern uint32_t _Chip_pllEncodeM(uint32_t M);
extern uint32_t _Chip_pllDecodeM(uint32_t MDEC);
void _Chip_pllFindSel(uint32_t M, uint32_t *pSelP, uint32_t *pSelI, uint32_t *pSelR);
extern uint32_t _Chip_findPllPreDiv(uint32_t ctrlReg, uint32_t nDecReg);
extern uint32_t _Chip_findPllPostDiv(uint32_t ctrlReg, uint32_t pDecReg);
extern uint32_t _Chip_findPllMMult(uint32_t ctrlReg, uint32_t mDecReg);
extern double _Chip_Binary2Fractional(uint32_t binaryPart);
extern uint32_t _Chip_FindGreatestCommonDivisor(uint32_t m, uint32_t n);
extern uint32_t _Chip_Clock_Get_Fro12M_Rate(void);
extern uint32_t _Chip_Clock_Get_eOsc_Rate(void);
extern void _Chip_Clock_Set_PLLFreq(uint32_t pllN, uint32_t pllM, uint32_t pllP);	








#define MAKE_PD_BITS(reg, slot) (((reg) << 8) | (slot))
#define PDRCFG0 0x0U
#define PDRCFG1 0x1U

typedef enum pd_bits
{
    kPDRUNCFG_LP_REG = MAKE_PD_BITS(PDRCFG0, 2U),
    kPDRUNCFG_PD_FRO_EN = MAKE_PD_BITS(PDRCFG0, 4U),
    kPDRUNCFG_PD_TS = MAKE_PD_BITS(PDRCFG0, 6U),
    kPDRUNCFG_PD_BOD_RESET = MAKE_PD_BITS(PDRCFG0, 7U),
    kPDRUNCFG_PD_BOD_INTR = MAKE_PD_BITS(PDRCFG0, 8U),
    kPDRUNCFG_PD_VD2_ANA = MAKE_PD_BITS(PDRCFG0, 9U),
    kPDRUNCFG_PD_ADC0 = MAKE_PD_BITS(PDRCFG0, 10U),
    kPDRUNCFG_PD_RAM0 = MAKE_PD_BITS(PDRCFG0, 13U),
    kPDRUNCFG_PD_RAM1 = MAKE_PD_BITS(PDRCFG0, 14U),
    kPDRUNCFG_PD_RAM2 = MAKE_PD_BITS(PDRCFG0, 15U),
    kPDRUNCFG_PD_RAM3 = MAKE_PD_BITS(PDRCFG0, 16U),
    kPDRUNCFG_PD_ROM = MAKE_PD_BITS(PDRCFG0, 17U),
    kPDRUNCFG_PD_VDDA = MAKE_PD_BITS(PDRCFG0, 19U),
    kPDRUNCFG_PD_WDT_OSC = MAKE_PD_BITS(PDRCFG0, 20U),
    kPDRUNCFG_PD_USB0_PHY = MAKE_PD_BITS(PDRCFG0, 21U),
    kPDRUNCFG_PD_SYS_PLL0 = MAKE_PD_BITS(PDRCFG0, 22U),
    kPDRUNCFG_PD_VREFP = MAKE_PD_BITS(PDRCFG0, 23U),
    kPDRUNCFG_PD_FLASH_BG = MAKE_PD_BITS(PDRCFG0, 25U),
    kPDRUNCFG_PD_VD3 = MAKE_PD_BITS(PDRCFG0, 26U),
    kPDRUNCFG_PD_VD4 = MAKE_PD_BITS(PDRCFG0, 27U),
    kPDRUNCFG_PD_VD5 = MAKE_PD_BITS(PDRCFG0, 28U),
    kPDRUNCFG_PD_VD6 = MAKE_PD_BITS(PDRCFG0, 29U),
    kPDRUNCFG_REQ_DELAY = MAKE_PD_BITS(PDRCFG0, 30U),
    kPDRUNCFG_FORCE_RBB = MAKE_PD_BITS(PDRCFG0, 31U),

    kPDRUNCFG_PD_USB1_PHY = MAKE_PD_BITS(PDRCFG1, 0U),
    kPDRUNCFG_PD_USB_PLL = MAKE_PD_BITS(PDRCFG1, 1U),
    kPDRUNCFG_PD_AUDIO_PLL = MAKE_PD_BITS(PDRCFG1, 2U),
    kPDRUNCFG_PD_SYS_OSC = MAKE_PD_BITS(PDRCFG1, 3U),
    kPDRUNCFG_PD_EEPROM = MAKE_PD_BITS(PDRCFG1, 5U),
    kPDRUNCFG_PD_rng = MAKE_PD_BITS(PDRCFG1, 6U),

    /*
    This enum member has no practical meaning,it is used to avoid MISRA issue,
    user should not trying to use it.
    */
    kPDRUNCFG_ForceUnsigned = (int)0x80000000U,
} pd_bit_t;


// ------------------------------------------------------------------------------------------------
// disable PDRUNCFG bit in the Syscon. Note that disabling the bit powers up the peripheral
// static inline void POWER_DisablePD(pd_bit_t en)
//{
//    /* PDRUNCFGCLR */
//    SYSCON->PDRUNCFGCLR[((uint32_t)en >> 8UL)] = (1UL << ((uint32_t)en & 0xffU));
//}



static inline void POWER_EnablePD(pd_bit_t en)
{
    /* PDRUNCFGSET */
    SYSCON->PDRUNCFGSET[((uint32_t)en >> 8UL)] = (1UL << ((uint32_t)en & 0xffU));
}

static inline void POWER_DisablePD(pd_bit_t en)
{
    /* PDRUNCFGCLR */
    SYSCON->PDRUNCFGCLR[((uint32_t)en >> 8UL)] = (1UL << ((uint32_t)en & 0xffU));
}

__STATIC_INLINE void CLOCK_Enable_SysOsc(bool enable)
{
    if (enable)
    {
        SYSCON->PDRUNCFGCLR[0] |= SYSCON_PDRUNCFG_PDEN_VD2_ANA_MASK;
        SYSCON->PDRUNCFGCLR[1] |= SYSCON_PDRUNCFG_PDEN_SYSOSC_MASK;
    }

    else
    {
        SYSCON->PDRUNCFGSET[0] = SYSCON_PDRUNCFG_PDEN_VD2_ANA_MASK;
        SYSCON->PDRUNCFGSET[1] = SYSCON_PDRUNCFG_PDEN_SYSOSC_MASK;
    }
}








// ******************************************************************************************************
// GPIO Functions
// ******************************************************************************************************

// For IOCON Block, Pin function
#define CHIP_PINSWMUNUSED				0x0000										// Not used pin of MCU - write 0xff into PINENABLE - default state
#define PFUN_GPIO						0x0000										// no alternate function, Pin as std GPIO
#define PFUN_ALT(y)						(y & 0x07)									// Write y value into IOCON bits 2:0

COMP_PACKED_BEGIN
typedef struct	_chip_IO_Spec														// Additional GPIO structure based on chip specific requirements */
{
	uint16_t							IOCON_Reg;									// IOCON register hodnota. pouziva sa len 16 bit. Inak IOCON je 32 bitovy	 */
}_Chip_IO_Spec_t;
COMP_PACKED_END


extern void *_Chip_GPIO_Init ( int32_t PeriIndex, uint32_t Pin);
extern bool _Chip_GPIO_DeInit ( int32_t PeriIndex, uint32_t Pin);
extern void _Chip_GPIO_Wr_Conf_Pin (uint32_t Port, uint32_t Pin, uint8_t Reg_Idx, const _Chip_IO_Spec_t *Reg_Val);
extern void *_Chip_GPIO_PINT_Conf(uint8_t PINTSel, uint32_t Port, uint32_t Pin, uint8_t Sens);
extern void *_Chip_Get_GPIO_Ptr(int32_t PeriIndex);
extern void _Chip_GPIO_Set_PinDir(GPIO_Type *pGPIO, uint8_t portNum, uint32_t bitValue, uint8_t out);
extern bool _Chip_GPIO_GetPinState(GPIO_Type *pGPIO, uint8_t port, uint8_t pin);
extern void _Chip_GPIO_SetPinOutLow(GPIO_Type *pGPIO, uint8_t port, uint8_t pin);
extern void _Chip_GPIO_SetPinOutHigh(GPIO_Type *pGPIO, uint8_t port, uint8_t pin);
extern void _Chip_GPIO_SetPinToggle(GPIO_Type *pGPIO, uint8_t port, uint8_t pin);
extern void *_Chip_GPIO_PINT_Conf(uint8_t PINTSel, uint32_t Port, uint32_t Pin, uint8_t Sens);
extern void *_Chip_GPIO_GINT_Conf(uint8_t Group, uint32_t Port, uint32_t Pin, uint8_t Sens);
extern bool _Chip_GPIO_Enable_IRQ_NVIC(void *pPeri, uint32_t Port, uint32_t Pin, bool NewState);

//extern void *_Chip_GPIO_Init ( int32_t PeriIndex, uint32_t Pin);
//extern bool _Chip_GPIO_DeInit ( int32_t PeriIndex, uint32_t Pin);
//extern void *_Chip_GPIO_IRQ_Conf( uint8_t Group, uint32_t Port, uint32_t Pin, uint8_t Sens);



// ------------------------------------------------------------------------------------------------
// I2C Functions
// ------------------------------------------------------------------------------------------------
// Interrupt status flag defines:
#define	CHIP_I2C_IRQSTAT_MSTPENDING			(1 << 0)								// Master Pending flag
#define	CHIP_I2C_IRQSTAT_MSARBLOSS			(1 << 4)								// Master Arbitration Loss flag.
#define	CHIP_I2C_IRQSTAT_MSSTSTPERR			(1 << 6)								// Master Start/Stop Error flag
#define	CHIP_I2C_IRQSTAT_MSSTATE_CHANGED	5										// MSSTATE was changed - this flag mus be out of mask !!!
// namiesto MSTCHANGED by sa mal pouzit MSTPENDING
#define	CHIP_I2C_MSSTATE_MASK				0x0e
#define CHIP_I2C_MSSTATE_RX					(1 << 1)
#define CHIP_I2C_MSSTATE_TX					(2 << 1)
#define CHIP_I2C_MSSTATE_NACK_ADR			(3 << 1)
#define CHIP_I2C_MSSTATE_NACK_DATA			(4 << 1)
#define CHIP_I2C_IRQSTAT_MONRDY				(1 << 16)								// Monitor Ready Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_MONOV				(1 << 17)								// Monitor Overflow Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_MONIDLE			(1 << 19)								// Monitor Idle Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_SLVDESEL			(1 << 15)								// Slave Deselect Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_SLVNOTSTR			(1 << 11)								// Slave not stretching Clock Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_SLVPENDING			(1 << 8)								// Slave Pending Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_EVENTTIMEOUT		(1 << 24)								// Event Timeout Interrupt Flag
#define CHIP_I2C_IRQSTAT_SCLTIMEOUT			(1 << 25)								// SCL Timeout Interrupt Flag

typedef struct 
{
	const uint8_t 						*pTxBuff;									// Pointer to array of bytes to be transmitted
	uint8_t 							*pRxBuff;									// Pointer memory where bytes received from I2C be stored
	uint16_t 							TxSz;										// Number of bytes in transmit array, if 0 only receive transfer will be carried on
	uint16_t 							RxSz;										// Number of bytes to received, if 0 only transmission we be carried on
	uint16_t 							Status;										// Status of the current I2C transfer
	uint8_t 							SlaveAddr;									// 7-bit I2C Slave address
} _Chip_I2CM_XFER_T;


extern int32_t 	_Chip_I2C_Get_PeriIndex(void *pPeri);
extern void*	_Chip_I2C_Init(int32_t PeriIndex);										// init I2C by periphary number. Return is pointer to periphery if success
extern uint32_t _Chip_I2C_Get_BusSpeed(void *pPeri);								// Return Current bus speed
extern bool 	_Chip_I2C_Set_BusSpeed(void *pPeri, uint32_t Speed_kHz);				// Max Speed: 100kHz, 400kHz, 1MHz,...
extern bool 	_Chip_I2C_Configure(void *pPeri, uint32_t Speed_kHz, bool MasterMode, bool SlaveMode, bool MonitorMode, uint16_t Address);
extern uint32_t _Chip_I2C_Get_Peri_Status(void *pPeri);
extern bool 	_Chip_I2C_Clear_Peri_Status(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_I2CMST_Get_IRQ_Status(void *pPeri);
	
extern bool 	_Chip_I2CMST_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_I2CSLV_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_I2CMON_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_I2CMST_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_I2CSLV_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_I2CMON_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_I2CTimeout_Enable_IRQ_NVIC(void *pPeri, bool NewState);

extern uint32_t _Chip_I2CSLV_Get_IRQ_Status(void *pPeri);
extern uint32_t _Chip_I2CMON_Get_IRQ_Status(void *pPeri);
extern uint32_t _Chip_I2CTimeout_Get_IRQ_Status(void *pPeri);
extern bool 	_Chip_I2CMST_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	_Chip_I2CSLV_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	_Chip_I2CMON_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	_Chip_I2CTimeout_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	_Chip_I2CMST_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	_Chip_I2CSLV_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	_Chip_I2CMON_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	_Chip_I2CTimeout_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern void 	_Chip_I2CMST_Put_Data(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_I2CMST_Get_Data(void *pPeri);
extern void 	_Chip_I2CMST_Set_Start(void *pPeri);								// create start impulse
extern void 	_Chip_I2CMST_Set_Stop(void *pPeri);
extern void 	_Chip_I2CMST_Set_Cont(void *pPeri);
extern void 	_Chip_I2CMST_Reset(void *pPeri);

//extern void *_Chip_I2C_Init(int32_t PeriIndex);										// init I2C by periphary number. Return is pointer to periphery if success
//extern uint32_t _Chip_I2C_Get_BusSpeed(void *pPeri);								// Return Current bus speed
//extern bool _Chip_I2C_Set_BusSpeed(void *pPeri, uint32_t Speed_kHz);				// Max Speed: 100kHz, 400kHz, 1MHz,...
//extern bool _Chip_I2C_Configure(void *pPeri, uint32_t Speed_kHz, bool MasterMode, bool SlaveMode, bool MonitorMode, uint16_t Address);
////extern bool _Chip_I2C_Enable(void *pPeri, bool NewState);
//extern bool _Chip_I2CMST_Enable(void *pPeri, bool NewState);
//extern bool _Chip_I2CSLV_Enable(void *pPeri, bool NewState);
//extern bool _Chip_I2CMON_Enable(void *pPeri, bool NewState);
//extern bool _Chip_I2CMST_Enable_IRQ_NVIC(void *pPeri, bool NewState);
//extern bool _Chip_I2CSLV_Enable_IRQ_NVIC(void *pPeri, bool NewState);
//extern bool _Chip_I2CMON_Enable_IRQ_NVIC(void *pPeri, bool NewState);
//extern bool _Chip_I2CTimeout_Enable_IRQ_NVIC(void *pPeri, bool NewState);


// ------------------------------------------------------------------------------------------------
// UART Functions
// ------------------------------------------------------------------------------------------------

#define	CHIP_UART_IRQSTAT_TXIDLE				USART_IRQSTAT_TXIDLE_MASK

#define	CHIP_UART_PERISTAT_TXIDLE				CHIP_UART_IRQSTAT_TXIDLE			// same as IRQ Stat

#define	CHIP_UARTFIFO_IRQSTAT_TXRDY				USART_FIFOINTSTAT_TXLVL_MASK
#define	CHIP_UARTFIFO_IRQSTAT_TXERR				USART_FIFOINTSTAT_TXERR_MASK
#define	CHIP_UARTFIFO_IRQSTAT_RXRDY				USART_FIFOINTSTAT_RXLVL_MASK
#define	CHIP_UARTFIFO_IRQSTAT_RXERR				USART_FIFOINTSTAT_RXERR_MASK

#define	CHIP_UARTFIFO_STAT_RXNOEMPTY			USART_FIFOSTAT_RXNOTEMPTY_MASK

extern int32_t 	_Chip_UART_Get_PeriIndex(void *pPeri);
extern bool 	_Chip_UART_Set_AdrDet(void* pPeri, bool NewVal);
extern bool 	_Chip_UART_Set_Address(void* pPeri, uint8_t NewAddress);
extern uint8_t 	_Chip_UART_Get_Address(void* pPeri);
extern uint32_t _Chip_UART_Get_Peri_Status(void *pPeri);
extern bool 	_Chip_UART_Clear_Peri_Status(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_UART_Get_IRQ_Status(void *pPeri);
extern bool 	_Chip_UART_Clear_IRQ_Status(void *pPeri,uint32_t IRQBitMask);
extern void 	_Chip_UART_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState);
extern void 	_Chip_UART_Put_Data(void *pPeri, uint32_t WrData);
extern uint32_t _Chip_UART_Get_Data(void *pPeri);
extern void 	_Chip_UART_Flush_Tx(void *pPeri);
extern void 	_Chip_UART_Flush_Rx(void *pPeri);
extern void*	_Chip_UART_Init(int32_t PeriIndex);
extern bool 	_Chip_UART_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_UART_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_UART_Set_Baud(void* pPeri, uint32_t NewBaud);
extern uint32_t _Chip_UART_Get_Baud(void* pPeri);
extern bool 	_Chip_UART_Set_Conf(void* pPeri, uint8_t DataLen, uint8_t Parity, uint8_t StopBits);
extern bool 	_Chip_UART_Set_RTS_Conf(void *pPeri, uint8_t sRTS_Port, uint8_t sRTS_Pin);
extern bool 	_Chip_UART_Set_CTS_Conf(void *pPeri, uint8_t sCTS_Port, uint8_t sCTS_Pin);
extern bool 	_Chip_UART_Set_DIRDE_Conf(void *pPeri, uint8_t DIRDE_Port, uint8_t DIRDE_Pin, bool DIRDE_H_Active);
extern bool 	_Chip_UART_Set_RE_Conf(void *pPeri, uint8_t RE_Port, uint8_t RE_Pin, bool RE_H_Active);
extern uint32_t _Chip_UARTFIFO_Get_IRQ_Status(void *pPeri);
extern bool 	_Chip_UART_Clear_IRQ_Status(void *pPeri,uint32_t IRQBitMask);
extern bool 	_Chip_UARTFIFO_Clear_IRQ_Status(void *pPeri, uint32_t IRQBitMask);
extern void 	_Chip_UART_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState);
extern void 	_Chip_UARTFIFO_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState);
extern uint32_t _Chip_UARTFIFO_Get_Peri_Status(void *pPeri);
extern bool 	_Chip_UARTFIFO_Clear_Peri_Status(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_UARTFIFO_Get_TxTHLevel(void *pPeri);
extern uint32_t _Chip_UARTFIFO_Get_RxTHLevel(void *pPeri);
extern void 	_Chip_UARTFIFO_Set_TxTHLevel(void *pPeri, uint32_t NewValue);
extern void 	_Chip_UARTFIFO_Set_RxTHLevel(void *pPeri, uint32_t NewValue);



//extern void *_Chip_UART_Init(int32_t PeriIndex);
//extern bool _Chip_UART_Enable(void *pPeri, bool NewState);
//extern bool _Chip_UART_Enable_IRQ_NVIC(void *pPeri, bool NewState);
//extern bool _Chip_UART_Set_Baud(void* pPeri, uint32_t NewBaud);
//extern uint32_t _Chip_UART_Get_Baud(void* pPeri);
//extern bool _Chip_UART_Set_Conf(void* pPeri, uint8_t DataLen, uint8_t Parity, uint8_t StopBits);
//extern bool _Chip_UART_Set_RTS_Conf(void* pPeri, uint8_t sRTS_Port, uint8_t sRTS_Pin);
//extern bool _Chip_UART_Set_CTS_Conf(void* pPeri, uint8_t sCTS_Port, uint8_t sCTS_Pin);
//extern bool _Chip_UART_Set_DIRDE_Conf(void* pPeri, uint8_t DIRDE_Port, uint8_t DIRDE_Pin, bool DIRDE_H_Active);
//extern bool _Chip_UART_Set_RE_Conf(void* pPeri, uint8_t RE_Port, uint8_t RE_Pin, bool RE_H_Active);





























































// ------------------------------------------------------------------------------------------------
// FLEXCOMMs Functions
// ------------------------------------------------------------------------------------------------

typedef enum
{
    FLEXCOMM_PERIPH_NONE,   														// No peripheral
    FLEXCOMM_PERIPH_USART,  														// USART peripheral
    FLEXCOMM_PERIPH_SPI,    														// SPI Peripheral
    FLEXCOMM_PERIPH_I2C,    														// I2C Peripheral
    FLEXCOMM_PERIPH_I2S_TX, 														// I2S TX Peripheral
    FLEXCOMM_PERIPH_I2S_RX, 														// I2S RX Peripheral
} _Chip_FlexComm_Peri_T;

// ------------------------------------------------------------------------------------------------
// return periphery index (due to FlexComm, return ptr is Flexcomm ptr!)
static inline int32_t _Chip_FlexComm_Get_PeriIndex(void *pPeri)						// Return Index from pointer
{
	if((_CHIP_FLEXCOMM_T*) pPeri == FLEXCOMM9) return(9);
	if((_CHIP_FLEXCOMM_T*) pPeri == FLEXCOMM8) return(8);
	if((_CHIP_FLEXCOMM_T*) pPeri == FLEXCOMM7) return(7);
	if((_CHIP_FLEXCOMM_T*) pPeri == FLEXCOMM6) return(6);
	if((_CHIP_FLEXCOMM_T*) pPeri == FLEXCOMM5) return(5);
	if((_CHIP_FLEXCOMM_T*) pPeri == FLEXCOMM4) return(4);
	if((_CHIP_FLEXCOMM_T*) pPeri == FLEXCOMM3) return(3);
	if((_CHIP_FLEXCOMM_T*) pPeri == FLEXCOMM2) return(2);
	if((_CHIP_FLEXCOMM_T*) pPeri == FLEXCOMM1) return(1);
	if((_CHIP_FLEXCOMM_T*) pPeri == FLEXCOMM0) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------
// Set reset to FlexComm[i]
extern inline void _Chip_FlexComm_Set_Reset(int32_t PeriIndex)
{
	switch(PeriIndex)
	{
		case 0:
		case 1:
		case 2:
		case 3:	
		case 4:
		case 5:
		case 6:
		case 7:	SYSCON->PRESETCTRLSET[1] |= ((1 << 11)<< PeriIndex); break;			// Assert Reset for FC 0-7
		case 8:	SYSCON->PRESETCTRLSET[2] |= (1 << 14); break;						// Assert Reset for FC 8
		case 9:	SYSCON->PRESETCTRLSET[2] |= (1 << 15); break;						// Assert Reset for FC 9
	}
}

// ------------------------------------------------------------------------------------------------
// Clear reset to FlexComm[i]
extern inline void _Chip_FlexComm_Clear_Reset(int32_t PeriIndex)
{
	switch(PeriIndex)
	{
		case 0:
		case 1:
		case 2:
		case 3:	
		case 4:
		case 5:
		case 6:
		case 7:	SYSCON->PRESETCTRLCLR[1] |= ((1 << 11)<< PeriIndex); break;			// Assert Reset for FC 0-7
		case 8:	SYSCON->PRESETCTRLCLR[2] |= (1 << 14); break;						// Assert Reset for FC 8
		case 9:	SYSCON->PRESETCTRLCLR[2] |= (1 << 15); break;						// Assert Reset for FC 9
	}
}

// ------------------------------------------------------------------------------------------------
// Enable Peri Clock for FlexComm[i]
extern inline void _Chip_FlexComm_Enable_Clock(int32_t PeriIndex)
{
	switch(PeriIndex)
	{
		case 0:
		case 1:
		case 2:
		case 3:	
		case 4:
		case 5:
		case 6:
		case 7:	SYSCON->AHBCLKCTRLSET[1] |= ((1 << 11)<< PeriIndex); break;			// Enable Clock for FC 0-7
		case 8:	SYSCON->AHBCLKCTRLSET[2] |= (1 << 14); break;						// Enable Clock for FC 8
		case 9:	SYSCON->AHBCLKCTRLSET[2] |= (1 << 15); break;						// Enable Clock for FC 9
	}
}

// ------------------------------------------------------------------------------------------------
// Disable Peri Clock for FlexComm[i]
extern inline void _Chip_FlexComm_Disable_Clock(int32_t PeriIndex)
{
	switch(PeriIndex)
	{
		case 0:
		case 1:
		case 2:
		case 3:	
		case 4:
		case 5:
		case 6:
		case 7:	SYSCON->AHBCLKCTRLCLR[1] |= ((1 << 11)<< PeriIndex); break;			// DIsable Clock for FC 0-7
		case 8:	SYSCON->AHBCLKCTRLCLR[2] |= (1 << 14); break;						// Disable Clock for FC 8
		case 9:	SYSCON->AHBCLKCTRLCLR[2] |= (1 << 15); break;						// Disable Clock for FC 9
	}
}


// ******************************************************************************************************
// SPI Functions
// ******************************************************************************************************

//#define	CHIP_SPI_IRQSTAT_RXDONE			SPI_FIFOINTSTAT_RXLVL_MASK					// Receiver Done Flag new Data available
//#define	CHIP_SPI_IRQSTAT_TXRDY			SPI_FIFOINTSTAT_TXLVL_MASK					// Transmitter Ready Flag
//#define	CHIP_SPI_IRQSTAT_RXOV			SPI_FIFOINTSTAT_RXERR_MASK					// Receiver Overrun IRQ Flag
//#define	CHIP_SPI_IRQSTAT_TXUR			SPI_FIFOINTSTAT_TXERR_MASK					// Transmitter underrun IRQ Flag
#define	CHIP_SPI_IRQSTAT_SSA			SPI_IRQSTAT_SSA_MASK						// Slave Select Assert
#define	CHIP_SPI_IRQSTAT_SSD			SPI_IRQSTAT_SSD_MASK						// Slave Select Deassert
#define CHIP_SPI_IRQSTAT_MSTIDLE		SPI_IRQSTAT_MSTIDLE_MASK

#define	CHIP_SPI_IRQEN_RXDONE			SPI_FIFOINTENSET_RXLVL_MASK					// Receiver Done IRQ enable - new data available
#define	CHIP_SPI_IRQEN_TXRDY			SPI_FIFOINTENSET_TXLVL_MASK 					// Transmitter Ready IRQ enable - ready before send
#define	CHIP_SPI_IRQEN_RXOV				SPI_FIFOINTENSET_RXERR_MASK					// Receiver Overrun IRQ enable
#define	CHIP_SPI_IRQEN_TXUR				SPI_FIFOINTENSET_TXERR_MASK					// Transmitter underrun IRQ enable
#define	CHIP_SPI_IRQEN_SSA								(1 << 4)					// Slave Select Assert IRQ enable
#define	CHIP_SPI_IRQEN_SSD								(1 << 5)					// Slave Select Deassert IRQ enable
#define CHIP_SPI_IRQEN_MSTIDLE							(1 << 8)					// Master Idle IRQ enable

#define CHIP_SPIFIFO_IRQSTAT_RXDONE		SPI_FIFOINTSTAT_RXLVL_MASK					// number of received chars meet the level preset
#define CHIP_SPIFIFO_IRQSTAT_RXOV		SPI_FIFOINTSTAT_RXERR_MASK					// some error during receiving
#define CHIP_SPIFIFO_IRQSTAT_TXRDY		SPI_FIFOINTSTAT_TXLVL_MASK
#define CHIP_SPIFIFO_IRQSTAT_TXUR		SPI_FIFOINTSTAT_TXERR_MASK

#define CHIP_SPIFIFO_IRQEN_RXDONE		SPI_FIFOINTENSET_RXLVL_MASK
#define CHIP_SPIFIFO_IRQEN_RXOV			SPI_FIFOINTENSET_RXERR_MASK
#define CHIP_SPIFIFO_IRQEN_TXRDY		SPI_FIFOINTENSET_TXLVL_MASK
#define CHIP_SPIFIFO_IRQEN_TXUR			SPI_FIFOINTENSET_TXERR_MASK

extern void *_Chip_SPI_Init(int32_t PeriIndex);
extern bool	_Chip_SPI_Enable(void *pPeri, bool NewState);
extern void _Chip_SPI_Set_ClockDiv(_CHIP_SPI_T *pPeri, uint32_t clkdiv);
extern bool	_Chip_SPI_Set_BusSpeed(void *pPeri, uint32_t Speed_Hz);
extern uint32_t _Chip_SPI_Get_BusSpeed(_CHIP_SPI_T *pPeri);
extern bool	_Chip_SPI_Configure(void *pPeri, uint32_t Speed_Hz, bool MasterMode, bool SlaveMode, bool LSB_First, bool CPHA, bool CPOL);
extern bool	_Chip_SPI_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern int32_t _Chip_SPI_Get_PeriIndex(void *pPeri);
extern void	_Chip_SPI_Put_Data(void *pPeri, uint32_t NewValue, uint8_t NumOfBits, uint32_t CtrlFlags, uint8_t SSel);
extern uint32_t _Chip_SPI_Get_Data(void *pPeri);
extern uint32_t _Chip_SPI_Get_IRQ_Status(void *pPeri);
extern bool _Chip_SPI_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_SPI_Get_Peri_Status(void *pPeri);
extern bool _Chip_SPI_Clear_Peri_Status(void *pPeri, uint32_t BitMask);
extern bool _Chip_SPI_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern void _Chip_SPI_SendEOT(void *pPeri);
extern void _Chip_SPI_SendEOF(void *pPeri);
extern void _Chip_SPI_ReceiveIgnore(void *pPeri);
extern void _Chip_SPI_Flush_Tx(void *pPeri);
extern void _Chip_SPI_Flush_Rx(void *pPeri);


extern bool _Chip_SPIFIFO_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern uint32_t _Chip_SPIFIFO_Get_Peri_Status(void *pPeri);
extern bool _Chip_SPIFIFO_Clear_Peri_Status(void *pPeri, uint32_t BitMask);
extern uint32_t _Chip_SPIFIFO_Get_IRQ_Status(void *pPeri);
extern bool _Chip_SPIFIFO_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_SPIFIFO_Get_TxTHLevel(void *pPeri);
extern uint32_t _Chip_SPIFIFO_Get_RxTHLevel(void *pPeri);
extern void _Chip_SPIFIFO_Set_TxTHLevel(void *pPeri, uint32_t NewValue);
extern void _Chip_SPIFIFO_Set_RxTHLevel(void *pPeri, uint32_t NewValue);

//extern void *_Chip_SPI_Init(int32_t PeriIndex);
//extern uint32_t _Chip_SPI_Get_SPI_Src_ClockRate(void *pPeri);
//extern bool _Chip_SPI_Configure(void *pPeri, uint32_t Speed_Hz, bool MasterMode, bool SlaveMode, bool LSB_First, bool CPHA, bool CPOL);
//extern bool _Chip_SPI_Enable(void* pPeri, bool NewState);
//extern void Chip_SPI_SetClockDiv(_CHIP_SPI_T *pSPI, uint32_t clkdiv);
//extern bool _Chip_SPI_Set_BusSpeed(void *pPeri, uint32_t Speed_Hz);					// Set bus speed
//extern uint32_t _Chip_SPI_Get_BusSpeed(_CHIP_SPI_T *pSPI);							// Return Current bus speed
//extern bool _Chip_SPI_Enable_IRQ_NVIC(void *pPeri, bool NewState);



// ******************************************************************************************************
// USB Functions
// ******************************************************************************************************
extern void *_Chip_USB_Init(int32_t PeriIndex);
extern bool _Chip_USB_Enable(void *pPeri, bool NewState);
extern bool _Chip_USB_Configure(void *pPeri, bool DeviceMode, bool HostMode, bool OTGMode);
extern uint32_t _Chip_USB_Get_IRQStatus(void *pPeri);
extern int32_t _Chip_USB_Get_PeriIndex(void *pPeri);
extern bool _Chip_USB_Power(void *pPeri, bool NewState);
extern uint32_t _Chip_USB_Get_IRQStatus(void *pPeri);


// ******************************************************************************************************
// ADC Functions
// ******************************************************************************************************
static int32_t _Chip_ADC_Get_PeriIndex(void *pPeri);
extern bool _Chip_ADC_Power(void *pPeri, bool NewState);
extern uint32_t _Chip_ADC_Get_CHIRQ_Status(_CHIP_ADC_T *pADC);
extern bool _Chip_ADC_Clear_CHIRQ_Status(_CHIP_ADC_T *pADC, uint32_t ChannelBitMask);
extern void _Chip_ADC_Start(void *pPeri, uint32_t ChannelBitMask);
extern bool _Chip_ADC_IRQ_Peri_Enable(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern void *_Chip_ADC_Init(int32_t PeriIndex);
extern bool _Chip_ADC_Enable(void* pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool _Chip_ADC_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool _Chip_ADC_Configure (void *pPeri, uint32_t ChannelBitMask, uint32_t Speed_kHz, bool RepeatMode, bool DMAMode);
extern uint32_t _Chip_ADC_Get_ChannelMask(void *pPeri);
extern bool _Chip_Temp_Enable(bool NewState);
extern uint8_t _Chip_ADC_DMA_Mode_ChannelBitMask;


// ******************************************************************************************************
// TIMER Functions - MRTx
// LPC546xx has one MRT timer with 4 independent channels
// ******************************************************************************************************
extern void *_Chip_MRT_Init(int32_t PeriIndex);
extern bool _Chip_MRT_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool _Chip_MRT_Configure (void *pPeri, uint8_t Channel, uint32_t RateHz, bool RepeatMode);
static int32_t _Chip_MRT_Get_PeriIndex(void *pPeri);
extern bool _Chip_MRT_Clear_CHIRQ_Status(void *pPeri, uint32_t ChannelBitMask);
extern uint32_t _Chip_MRT_Get_CHIRQ_Status(void *pPeri);
extern bool _Chip_MRT_IRQ_Peri_Enable(void *pPeri, uint32_t IRQBitMask, bool NewState);





// ******************************************************************************************************
// CTIMER Functions - CTimerx like Timer...
// LPC546xx has four 32-bits CTimer with 4 match channels
// ******************************************************************************************************
extern void *_Chip_Timer_Init(int32_t PeriIndex);
extern bool _Chip_Timer_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool _Chip_Timer_Configure (void *pPeri, uint8_t Channel, uint32_t RateHz, bool RepeatMode);
extern bool _Chip_Timer_Enable_IRQ_NVIC(void *pPeri, bool NewState);
static int32_t _Chip_CTimer_Get_PeriIndex(void *pPeri);
extern bool _Chip_Timer_Clear_CHIRQ_Status(void *pPeri, uint32_t ChannelBitMask);
extern uint32_t _Chip_Timer_Get_CHIRQ_Status(void *pPeri);
extern bool _Chip_Timer_IRQ_Peri_Enable(void *pPeri, uint32_t IRQBitMask, bool NewState);

// ******************************************************************************************************
// DMA Functions
// ******************************************************************************************************
/* Support definitions for setting the configuration of a DMA channel. You
   will need to get more information on these options from the User manual. */
#define _CHIP_DMA_CFG_PERIPHREQEN		(DMA_CHANNEL_CFG_PERIPHREQEN(1))			// Enables Peripheral DMA requests
#define _CHIP_DMA_CFG_HWTRIGEN			(DMA_CHANNEL_CFG_HWTRIGEN(1))				// Use hardware triggering via imput mux
#define _CHIP_DMA_CFG_TRIGPOL_LOW		(DMA_CHANNEL_CFG_TRIGPOL(0))				// Hardware trigger is active low or falling edge
#define _CHIP_DMA_CFG_TRIGPOL_HIGH		(DMA_CHANNEL_CFG_TRIGPOL(1))				// Hardware trigger is active high or rising edge
#define _CHIP_DMA_CFG_TRIGTYPE_EDGE		(DMA_CHANNEL_CFG_TRIGTYPE(0))				// Hardware trigger is edge triggered
#define _CHIP_DMA_CFG_TRIGTYPE_LEVEL	(DMA_CHANNEL_CFG_TRIGTYPE(1))				// Hardware trigger is level triggered
#define _CHIP_DMA_CFG_TRIGBURST_SNGL	(DMA_CHANNEL_CFG_TRIGBURST(0))				// Single transfer. Hardware trigger causes a single transfer
#define _CHIP_DMA_CFG_TRIGBURST_BURST	(DMA_CHANNEL_CFG_TRIGBURST(1))				// Burst transfer (see UM)
#define _CHIP_DMA_CFG_BURSTPOWER_1		(DMA_CHANNEL_CFG_BURSTPOWER(0))				// Set DMA burst size to 1 transfer
#define _CHIP_DMA_CFG_BURSTPOWER_2		(DMA_CHANNEL_CFG_BURSTPOWER(1))				// Set DMA burst size to 2 transfers
#define _CHIP_DMA_CFG_BURSTPOWER_4		(DMA_CHANNEL_CFG_BURSTPOWER(2))				// Set DMA burst size to 4 transfers
#define _CHIP_DMA_CFG_BURSTPOWER_8		(DMA_CHANNEL_CFG_BURSTPOWER(3))				// Set DMA burst size to 8 transfers
#define _CHIP_DMA_CFG_BURSTPOWER_16		(DMA_CHANNEL_CFG_BURSTPOWER(4))				// Set DMA burst size to 16 transfers
#define _CHIP_DMA_CFG_BURSTPOWER_32		(DMA_CHANNEL_CFG_BURSTPOWER(5))				// Set DMA burst size to 32 transfers
#define _CHIP_DMA_CFG_BURSTPOWER_64		(DMA_CHANNEL_CFG_BURSTPOWER(6))				// Set DMA burst size to 64 transfers
#define _CHIP_DMA_CFG_BURSTPOWER_128	(DMA_CHANNEL_CFG_BURSTPOWER(7))				// Set DMA burst size to 128 transfers
#define _CHIP_DMA_CFG_BURSTPOWER_256	(DMA_CHANNEL_CFG_BURSTPOWER(8))				// Set DMA burst size to 256 transfers
#define _CHIP_DMA_CFG_BURSTPOWER_512	(DMA_CHANNEL_CFG_BURSTPOWER(9))				// Set DMA burst size to 512 transfers
#define _CHIP_DMA_CFG_BURSTPOWER_1024	(DMA_CHANNEL_CFG_BURSTPOWER(10))			// Set DMA burst size to 1024 transfers
#define _CHIP_DMA_CFG_BURSTPOWER(n)		(DMA_CHANNEL_CFG_BURSTPOWER(n))				// Set DMA burst size to 2^n transfers, max n=10
#define _CHIP_DMA_CFG_SRCBURSTWRAP		(DMA_CHANNEL_CFG_SRCBURSTWRAP(1))			// Source burst wrapping is enabled for this DMA channel
#define _CHIP_DMA_CFG_DSTBURSTWRAP		(DMA_CHANNEL_CFG_DSTBURSTWRAP(1))			// Destination burst wrapping is enabled for this DMA channel
#define _CHIP_DMA_CFG_CHPRIORITY(p)		(DMA_CHANNEL_CFG_CHPRIORITY(p))				// Sets DMA channel priority, min 0 (highest), max 3 (lowest)

/* DMA channel transfer configuration registers definitions */
#define _CHIP_DMA_XFERCFG_CFGVALID		(DMA_CHANNEL_XFERCFG_CFGVALID(1))			// Configuration Valid flag
#define _CHIP_DMA_XFERCFG_RELOAD		(DMA_CHANNEL_XFERCFG_RELOAD(1))				// Indicates whether the channels control structure will be reloaded when the current descriptor is exhausted
#define _CHIP_DMA_XFERCFG_SWTRIG		(DMA_CHANNEL_XFERCFG_SWTRIG(1))				// Software Trigger
#define _CHIP_DMA_XFERCFG_CLRTRIG		(DMA_CHANNEL_XFERCFG_CLRTRIG(1))			// Clear Trigger
#define _CHIP_DMA_XFERCFG_SETINTA		(DMA_CHANNEL_XFERCFG_SETINTA(1))			// Set Interrupt flag A for this channel to fire when descriptor is complete
#define _CHIP_DMA_XFERCFG_SETINTB		(DMA_CHANNEL_XFERCFG_SETINTB(1))			// Set Interrupt flag B for this channel to fire when descriptor is complete
#define _CHIP_DMA_XFERCFG_WIDTH_8		(DMA_CHANNEL_XFERCFG_WIDTH(0))				// 8-bit transfers are performed
#define _CHIP_DMA_XFERCFG_WIDTH_16		(DMA_CHANNEL_XFERCFG_WIDTH(1))				// 16-bit transfers are performed
#define _CHIP_DMA_XFERCFG_WIDTH_32		(DMA_CHANNEL_XFERCFG_WIDTH(2))				// 32-bit transfers are performed
#define _CHIP_DMA_XFERCFG_SRCINC_0		(DMA_CHANNEL_XFERCFG_SRCINC(0))				// DMA source address is not incremented after a transfer
#define _CHIP_DMA_XFERCFG_SRCINC_1		(DMA_CHANNEL_XFERCFG_SRCINC(1))				// DMA source address is incremented by 1 (width) after a transfer
#define _CHIP_DMA_XFERCFG_SRCINC_2		(DMA_CHANNEL_XFERCFG_SRCINC(2))				// DMA source address is incremented by 2 (width) after a transfer
#define _CHIP_DMA_XFERCFG_SRCINC_4		(DMA_CHANNEL_XFERCFG_SRCINC(3))				// DMA source address is incremented by 4 (width) after a transfer
#define _CHIP_DMA_XFERCFG_DSTINC_0		(DMA_CHANNEL_XFERCFG_DSTINC(0))				// DMA destination address is not incremented after a transfer
#define _CHIP_DMA_XFERCFG_DSTINC_1		(DMA_CHANNEL_XFERCFG_DSTINC(1))				// DMA destination address is incremented by 1 (width) after a transfer
#define _CHIP_DMA_XFERCFG_DSTINC_2		(DMA_CHANNEL_XFERCFG_DSTINC(2))				// DMA destination address is incremented by 2 (width) after a transfer
#define _CHIP_DMA_XFERCFG_DSTINC_4		(DMA_CHANNEL_XFERCFG_DSTINC(3))				// DMA destination address is incremented by 4 (width) after a transfer
#define _CHIP_DMA_XFERCFG_XFERCOUNT(n)	(DMA_CHANNEL_XFERCFG_XFERCOUNT(n))			// DMA transfer count in 'transfers', between (0)1 and (1023)1024

typedef struct
{
	uint32_t							SrcAdr;										// Source address
	uint16_t							SrcBSize;									// Source Burst size		  1/2/4 x Word
	uint8_t								SrcWSize;									// Source width: 8/16/32-bit
	uint8_t								SrcIncr;									// Source Increment
	uint32_t							SrcPeri;									// Source peripherial	
	uint32_t							DstAdr;										// Destination address
	uint16_t							DstBSize;									// Destination Burst size		  1/2/4 x Word
	uint8_t								DstWSize;									// Destination width: 8/16/32-bit
	uint8_t								DstIncr;									// Destination Increment
	uint32_t							DstPeri;									// Destination peripherial	
	uint8_t								XferIRQ;									// Select Interrupt generated after transfer
	uint32_t 							XferLen;									// length of transfer
	uint8_t								XferType;									// transfer type
	uint8_t								XferPrio;									// transfer priority
	uint32_t 							XferNextDescriptor;							// address of next linked descriptor
} _Chip_DMA_Xfer_t;

// DMA channel source/address/next descriptor
typedef struct {
	uint32_t  xfercfg;																// Transfer configuration (only used in linked lists and ping-pong configs)
	uint32_t  source;																// DMA transfer source end address
	uint32_t  dest;																	// DMA transfer desintation end address
	uint32_t  next;																	// Link to next DMA descriptor, must be 16 byte aligned
} _Chip_DMA_CHDesc_t;

// DMA SRAM table - this can be optionally used with the Chip_DMA_SetSRAMBase()
//   function if a DMA SRAM table is needed.
extern _Chip_DMA_CHDesc_t _Chip_DMA_Table[_CHIP_DMA_CHANNELS_COUNT];
extern void *_Chip_DMA_Init(int32_t PeriIndex);
extern bool _Chip_DMA_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool _Chip_DMA_Configure (void *pPeri, uint8_t Channel, _Chip_DMA_Xfer_t *Xfer);
extern int32_t _Chip_DMA_Get_PeriIndex(void *pPeri);
extern bool _Chip_DMA_Clear_CHIRQ_Status(void *pPeri, uint32_t ChannelBitMask);
extern uint32_t _Chip_DMA_Get_CHIRQ_Status(void *pPeri);
extern uint32_t _Chip_DMA_Get_CHControl(void *pPeri, uint32_t Channel);
extern bool _Chip_DMA_IRQ_Peri_Enable(void *pPeri, uint32_t IRQBitMask, bool NewState);


// ******************************************************************************************************
// EEPROM Functions
// ******************************************************************************************************
extern void *_Chip_EEPROM_Init(int32_t PeriIndex);
extern bool _Chip_EEPROM_Enable(void *pPeri, bool NewState);
extern bool _Chip_EEPROM_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool _Chip_EEPROM_Write(void *pPeri, uint32_t EEDst, uint8_t *pInBuff, size_t byteswr);
extern bool _Chip_EEPROM_Read(void *pPeri, uint32_t EESrc, uint8_t *pOutBuff, size_t bytesrd);
extern bool _Chip_EEPROM_Power(void *pPeri, bool NewState);


// ******************************************************************************************************
// SCT Functions
// ******************************************************************************************************
extern void* _Chip_SCT_Init(int32_t PeriIndex);
extern bool _Chip_SCT_Enable(void *pPeri, bool NewState);
extern bool _Chip_SCT_Configure(void *pPeri);
extern int32_t _Chip_SCT_Get_PeriIndex(void *pPeri);


// ******************************************************************************************************
// ETH Functions
// ******************************************************************************************************
extern void *_Chip_ETH_Init(int32_t PeriIndex);
extern bool _Chip_ETH_Enable(void *pPeri, bool NewState);


// ******************************************************************************************************
// LCD Functions
// ******************************************************************************************************
extern void *_Chip_LCD_Init(int32_t PeriIndex);
extern bool _Chip_LCD_Enable(void *pPeri, bool NewState);
extern bool _Chip_LCD_Configure(void *pPeri);

// ------------------------------------------------------------------------------------------------
// LCD interrupt enable/disable with mask
// result is true:successfull otherwise false
extern inline bool _Chip_LCD_IRQ_Peri_Enable(void *pPeri, uint32_t IRQMask, bool NewState)
{
	bool res = false;
	
	if(NewState) ((_CHIP_LCD_T *)pPeri)->INTMSK |= (IRQMask & 0x0000001e);
	else ((_CHIP_LCD_T *)pPeri)->INTMSK &= ~(IRQMask & 0x0000001e);
	res = true;
	
	return(res);
}

// ------------------------------------------------------------------------------------------------
// Get interrupt flags
extern inline uint32_t _Chip_LCD_Get_IRQ_Status(void *pPeri)
{
	return ((_CHIP_LCD_T *)pPeri)->INTSTAT;
}

// ------------------------------------------------------------------------------------------------
// clear interrupt flags
extern inline void _Chip_LCD_Clear_IRQ_Status(void *pPeri, uint32_t flags)
{
	((_CHIP_LCD_T *)pPeri)->INTCLR |= flags;										// Clear selected in ints flags
}

#endif	// ! LOAD_SCATTER
#endif	//__SERIE_546XX_H_
