// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: i2c_15xx.c
// 	   Version: 1.0
//  Created on: 01.05.2014
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: MCU LPC15xx Chip description/configuration file. Same for all MCUs of this family!
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//


// Initializes the LPC_I2C peripheral
//void Chip_I2C_Init(LPC_I2C_T *pI2C)
//{
//	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_I2C0);								// Enable I2C clock
//	Chip_SYSCTL_PeriphReset(RESET_I2C0);											// Peripheral reset control to I2C
//}

// Shuts down the I2C controller block
void Chip_I2C_DeInit(_CHIP_I2C_T *pI2C)
{
	Chip_Clock_DisablePeriphClock(SYSCON_CLOCK_I2C0);								// Disable I2C clock
}



// ------------------------------------------------------------------------------------------------------
// Set up bus speed for LPC_I2C interface
bool Chip_I2CM_SetBusSpeed(_CHIP_I2C_T *pI2C, uint32_t busSpeed)
{
	uint32_t scl;
	
	scl = Chip_Clock_GetMainClockRate() / (Chip_I2C_GetClockDiv(pI2C) * busSpeed);
	Chip_I2CM_SetDutyCycle(pI2C, (scl >> 1), (scl - (scl >> 1)));
	
	return(true);
}

/* Master transfer state change handler handler */
uint32_t Chip_I2CM_XferHandler(uint32_t *pPeri, I2CM_XFER_T *xfer)
{
	_CHIP_I2C_T *pI2C = (_CHIP_I2C_T*) pPeri;
	
	uint32_t status = Chip_I2CM_GetStatus(pI2C);
	/* Master Lost Arbitration */
	if (status & I2C_STAT_MSTRARBLOSS) {
		/* Set transfer status as Arbitration Lost */
		xfer->status = I2CM_STATUS_ARBLOST;
		/* Clear Status Flags */
		Chip_I2CM_ClearStatus(pI2C, I2C_STAT_MSTRARBLOSS);
	}
	/* Master Start Stop Error */
	else if (status & I2C_STAT_MSTSTSTPERR) {
		/* Set transfer status as Bus Error */
		xfer->status = I2CM_STATUS_BUS_ERROR;
		/* Clear Status Flags */
		Chip_I2CM_ClearStatus(pI2C, I2C_STAT_MSTSTSTPERR);
	}
	/* Master is Pending */
	else if (status & I2C_STAT_MSTPENDING) {
		/* Branch based on Master State Code */
		switch (Chip_I2CM_GetMasterState(pI2C)) {
		/* Master idle */
		case I2C_STAT_MSTCODE_IDLE:
			/* Do Nothing */
			break;

		/* Receive data is available */
		case I2C_STAT_MSTCODE_RXREADY:
			/* Read Data */
			*xfer->rxBuff++ = pI2C->MSTDAT;
			xfer->rxSz--;
			if (xfer->rxSz) {
				/* Set Continue if there is more data to read */
				Chip_I2CM_MasterContinue(pI2C);
			}
			else {
				/* Set transfer status as OK */
				xfer->status = I2CM_STATUS_OK;
				/* No data to read send Stop */
				Chip_I2CM_SendStop(pI2C);
			}
			break;

		/* Master Transmit available */
		case I2C_STAT_MSTCODE_TXREADY:
			if (xfer->txSz) {
				/* If Tx data available transmit data and continue */
				pI2C->MSTDAT = *xfer->txBuff++;
				xfer->txSz--;
				Chip_I2CM_MasterContinue(pI2C);
			}
			else {
				/* If receive queued after transmit then initiate master receive transfer*/
				if (xfer->rxSz) {
					/* Write Address and RW bit to data register */
					Chip_I2CM_WriteByte(pI2C, (xfer->slaveAddr << 1) | 0x1);
					/* Enter to Master Transmitter mode */
					Chip_I2CM_SendStart(pI2C);
				}
				else {
					/* If no receive queued then set transfer status as OK */
					xfer->status = I2CM_STATUS_OK;
					/* Send Stop */
					Chip_I2CM_SendStop(pI2C);
				}
			}
			break;

		case I2C_STAT_MSTCODE_NACKADR:
			/* Set transfer status as NACK on address */
			xfer->status = I2CM_STATUS_NAK_ADR;
			Chip_I2CM_SendStop(pI2C);
			break;

		case I2C_STAT_MSTCODE_NACKDAT:
			/* Set transfer status as NACK on data */
			xfer->status = I2CM_STATUS_NAK_DAT;
			Chip_I2CM_SendStop(pI2C);
			break;

		default:
			/* Default case should not occur*/
			xfer->status = I2CM_STATUS_ERROR;
			break;
		}
	}
	else {
		/* Default case should not occur */
		xfer->status = I2CM_STATUS_ERROR;
	}
	return xfer->status != I2CM_STATUS_BUSY;
}

/* Transmit and Receive data in master mode */
void Chip_I2CM_Xfer(_CHIP_I2C_T *pI2C, I2CM_XFER_T *xfer)
{
	/* set the transfer status as busy */
	xfer->status = I2CM_STATUS_BUSY;
	/* Clear controller state. */
	Chip_I2CM_ClearStatus(pI2C, I2C_STAT_MSTRARBLOSS | I2C_STAT_MSTSTSTPERR);
	/* Write Address and RW bit to data register */
	Chip_I2CM_WriteByte(pI2C, (xfer->slaveAddr << 1) | (xfer->txSz == 0));
	/* Enter to Master Transmitter mode */
	Chip_I2CM_SendStart(pI2C);
}

/* Transmit and Receive data in master mode */
uint32_t Chip_I2CM_XferBlocking(uint32_t *pPeri, I2CM_XFER_T *xfer)
{
	uint32_t ret = 0;
	/* start transfer */
	Chip_I2CM_Xfer((_CHIP_I2C_T *) pPeri, xfer);

	while (ret == 0) {
		/* wait for status change interrupt */
		while (!Chip_I2CM_IsMasterPending((_CHIP_I2C_T *) pPeri)) {}
		/* call state change handler */
		ret = Chip_I2CM_XferHandler(pPeri, xfer);
	}
	return ret;
}


/* Slave transfer state change handler */
uint32_t Chip_I2CS_XferHandler(_CHIP_I2C_T *pI2C, const I2CS_XFER_T *xfers)
{
	uint32_t done = 0;

	uint8_t data;
	uint32_t state;

	/* transfer complete? */
	if ((Chip_I2C_GetPendingInt(pI2C) & I2C_INTENSET_SLVDESEL) != 0) {
		Chip_I2CS_ClearStatus(pI2C, I2C_STAT_SLVDESEL);
		xfers->slaveDone();
	}
	else {
		/* Determine the current I2C slave state */
		state = Chip_I2CS_GetSlaveState(pI2C);

		switch (state) {
		case I2C_STAT_SLVCODE_ADDR:		/* Slave address received */
			/* Get slave address that needs servicing */
			data = Chip_I2CS_GetSlaveAddr(pI2C, Chip_I2CS_GetSlaveMatchIndex(pI2C));

			/* Call address callback */
			xfers->slaveStart(data);
			break;

		case I2C_STAT_SLVCODE_RX:		/* Data byte received */
			/* Get received data */
			data = Chip_I2CS_ReadByte(pI2C);
			done = xfers->slaveRecv(data);
			break;

		case I2C_STAT_SLVCODE_TX:		/* Get byte that needs to be sent */
			/* Get data to send */
			done = xfers->slaveSend(&data);
			Chip_I2CS_WriteByte(pI2C, data);
			break;
		}
	}

	if (done == 0) {
		Chip_I2CS_SlaveContinue(pI2C);
	}
	else {
		Chip_I2CS_SlaveNACK(pI2C);
	}

	return done;
}

