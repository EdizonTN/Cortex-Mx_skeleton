// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: iap_15xx.c
// 	   Version: 1.0
//  Created on: 01.05.2014
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//	 Desc: IAP
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//


//#include "Skeleton.h"

// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************


// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************

// ******************************************************************************************************
// PUBLIC Functions
// ******************************************************************************************************



// ------------------------------------------------------------------------------------------------
// Prepare sector for write operation - This command must be executed before executing "Copy RAM to flash" or "Erase Sector(s)" command. 
//					Successful execution of the "Copy RAM to flash" or "Erase Sector(s)" command causes relevant sectors to be protected again. 
//					The boot sector can not be prepared by this command. To prepare a single sector use the same "Start" and "End" sector numbers.
//return 	result[0] : Status Code
bool Chip_IAP_PreSectorForReadWrite(uint32_t strSector, uint32_t endSector)
{
	uint32_t Command[5], Result[5];

	Command[0] = IAP_PREWRRITE_CMD;
	Command[1] = strSector;
	Command[2] = endSector;
	iap_entry(Command, Result);

	//return result[0];
	if(Result[0] == IAP_CMD_SUCCESS) return(true);
	return (false);	
}


// ------------------------------------------------------------------------------------------------
// Copy RAM to flash - This command is used to program the flash memory. 
//					The affected sectors should be prepared first by calling "Prepare Sector for Write Operation" command. 
//					The affected sectors are automatically protected again once the copy command is successfully executed. 
//					The boot sector can not be written by this command. Also see Section 34.4.3 for the number of bytes that can be written.
//					Param3 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
//					Remark: All user code must be written in such a way that no master accesses the flash while this command is executed and the flash is programmed.
//return 	result[0] : Status Code
bool Chip_IAP_CopyRamToFlash(uint32_t DstAdd, uint32_t *pSrc, uint32_t byteswrt)
{
	uint32_t Command[5], Result[5];

	Command[0] = IAP_WRISECTOR_CMD;
	Command[1] = DstAdd;
	Command[2] = (uint32_t) pSrc;
	Command[3] = byteswrt;															// Should be 256 | 512 | 1024 | 4096.
	Command[4] = SystemCoreClock / 1000;
	iap_entry(Command, Result);

	//return result[0];
	if(Result[0] == IAP_CMD_SUCCESS) return(true);
	return (false);	
}

// ------------------------------------------------------------------------------------------------
// Erase sector - This command is used to erase a sector or multiple sectors of on-chip flash memory. 
//				The boot sector can not be erased by this command. To erase a single sector use the same "Start" and "End" sector numbers.
//				Param2 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
//				Remark: All user code must be written in such a way that no master accesses the flash while this command is executed and the flash is erased.
//return 	result[0] : Status Code
bool Chip_IAP_EraseSector(uint32_t strSector, uint32_t endSector)
{
	uint32_t Command[5], Result[5];

	Command[0] = IAP_ERSSECTOR_CMD;
	Command[1] = strSector;
	Command[2] = endSector;
	Command[3] = SystemCoreClock / 1000;
	iap_entry(Command, Result);

	//return result[0];
	if(Result[0] == IAP_CMD_SUCCESS) return(true);
	return (false);
}

// ------------------------------------------------------------------------------------------------
// Blank check sector - This command is used to blank check a sector or multiple sectors of on-chip flash memory. 
//					To blank check a single sector use the same "Start" and "End" sector numbers.
//return 	result[0] : Status Code
//			result[1] : Offset of the first non blank word location if the status code is SECTOR_NOT_BLANK.
//			result[2] : Contents of non blank word location.
bool Chip_IAP_BlankCheckSector(uint32_t strSector, uint32_t endSector)
{
	uint32_t Command[5], Result[5];

	Command[0] = IAP_BLANK_CHECK_SECTOR_CMD;
	Command[1] = strSector;
	Command[2] = endSector;
	iap_entry(Command, Result);

	//return 	result[0]
	if(Result[0] == IAP_CMD_SUCCESS) return(true);
	return (false);
}


// ------------------------------------------------------------------------------------------------
// Read part identification number - This command is used to read the part identification number.
// return 	result[0] : Status Code
//			result[1] : Part Identification Number.
bool Chip_IAP_Read_PID(uint32_t *pDst)
{
	uint32_t Command[5], Result[5];

	Command[0] = IAP_REPID_CMD;
	iap_entry(Command, Result);
	if(Result[0] == IAP_CMD_SUCCESS)
	{
		*pDst = Result[1];															// copy readed value to Dst
		return(true);
	}
	return (false);
}


// ------------------------------------------------------------------------------------------------
// Read boot code version number - This command is used to read the boot code version number.
// return 	result[0] : Status Code
//			result[1] : Boot Code version. <byte1(Major)>.<byte0(Minor)>
bool Chip_IAP_Read_BootCode(uint32_t *pDst)
{
	uint32_t Command[5], Result[5];

	Command[0] = IAP_READ_BOOT_CODE_CMD;
	iap_entry(Command, Result);

	//return result[1] & 0xffff;
	if(Result[0] == IAP_CMD_SUCCESS) 
	{
		*pDst = Result[1];															// copy readed value to Dst
		return(true);
	}
	return (false);
}


// ------------------------------------------------------------------------------------------------
// IAP compare - This command is used to compare the memory contents at two locations.
//				!!!!!!   The result may not be correct when the source or destination includes any of the first 512 bytes starting from address zero. 
//				The first 512 bytes can be re-mapped to RAM. !!!!!!!!
// return 	result[0] : Status Code
//			result[1] : Offset of the first mismatch if the status code is COMPARE_ERROR.
bool Chip_IAP_Compare(uint32_t dstAdd, uint32_t srcAdd, uint32_t bytescmp)
{
	uint32_t Command[5], Result[5];

	Command[0] = IAP_COMPARE_CMD;
	Command[1] = dstAdd;
	Command[2] = srcAdd;
	Command[3] = bytescmp;
	iap_entry(Command, Result);

	//return result[0];
	if(Result[0] == IAP_CMD_SUCCESS) return(true);
	return (false);
}


// ------------------------------------------------------------------------------------------------
// Reinvoke ISP - This command is used to invoke the boot loader in ISP mode. It maps boot vectors and configures the peripherals for ISP. 
//				Which peripheral (UART, C_CAN,UART, or USB) is selected for ISP mode depends on the parameter written with this command.
// 				This command may be used when a valid user program is present in the internal flash memory and the ISP entry pins are not accessible to force the ISP mode.
// 				Before calling this command, enable the clocks to the GPIO0/1/2 blocks in the SYSAHBCLKCTRL0 register.
//				ISPMode: ISP mode. 1 = UART ISP, 2 = USB ISP, 3 = C_CAN ISP.
// return 	result[0] : always true
bool Chip_IAP_ReinvokeISP(uint8_t ISPMode)
{
	uint32_t Command[5], Result[5];

	Command[0] = IAP_REINVOKE_ISP_CMD;
	Command[1] = ISPMode;
	iap_entry(Command, Result);

	return(true);
}


// ------------------------------------------------------------------------------------------------
// Read the unique ID - This command is used to read the unique ID.
// return 	result[0] : Status Code
//			result[1] : The first 32-bit word (at the lowest address).
//			result[2] : The second 32-bit word.
//			result[3] : The third 32-bit word.
//			result[4] : The fourth 32-bit word.
bool Chip_IAP_Read_UID(uint32_t* pDst)
{
	uint32_t Command[5], Result[5];
	uint32_t i;
	
	Command[0] = IAP_READ_UID_CMD;
	iap_entry(Command, Result);

	//return result[0];
	if(Result[0] == IAP_CMD_SUCCESS) 
	{
		for (i=1; i<=4; i++)
			*(pDst++) = Result[i];
		return(true);
	}
	return(false);	
}


// ------------------------------------------------------------------------------------------------
// Erase page - This command is used to erase a page or multiple pages of on-chip flash memory.
//				To erase a single page use the same "start" and "end" page numbers.
//				Param2 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
//				Remark: All user code must be written in such a way that no master accesses the flash while this command is executed and the flash is erased.
// return 	result[0] : Status Code
bool Chip_IAP_ErasePage(uint32_t strPage, uint32_t endPage)
{
	uint32_t Command[5], Result[5];

	Command[0] = IAP_ERASE_PAGE_CMD;
	Command[1] = strPage;
	Command[2] = endPage;
	Command[3] = SystemCoreClock / 1000;
	iap_entry(Command, Result);

	//return result[0];
	if(Result[0] == IAP_CMD_SUCCESS) return(true);
	else return (false);	
}


// ------------------------------------------------------------------------------------------------
// Write data to EEPROM - Data is copied from the RAM address to the EEPROM address.
//			Param3 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
//			Remark: The top 64 bytes of the EEPROM memory are reserved and cannot be written to.
// return 	result[0] : Status Code
bool Chip_IAP_EEPROM_Write(uint32_t EEDst, uint32_t RAMSrc, uint32_t byteswrt)
{
	uint32_t Command[5], Result[5];
	
	Command[0] = IAP_EEPROM_WRITE;
	Command[1] = EEDst;
	Command[2] = RAMSrc;
	Command[3] = byteswrt;															// Write one page only
	Command[4] = SystemCoreClock / 1000;
	__disable_irq();
	iap_entry(Command, Result);
	__enable_irq();

	//return result[0];
	if(Result[0] == IAP_CMD_SUCCESS) return(true);
	return (false);
}

// ------------------------------------------------------------------------------------------------
// Read data from EEPROM - Data is copied from the EEPROM address to the RAM address.
//			Param3 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
// return 	result[0] : Status Code
bool Chip_IAP_EEPROM_Read(uint32_t EESrc, uint32_t RAMDst, uint32_t bytesrd)
{
	uint32_t Command[5], Result[5];

	Command[0] = IAP_EEPROM_READ;
	Command[1] = EESrc;
	Command[2] = RAMDst;
	Command[3] = bytesrd;
	Command[4] = SystemCoreClock / 1000;
	__disable_irq();
	iap_entry(Command, Result);
	__enable_irq();
	//return result[0];
	if(Result[0] == IAP_CMD_SUCCESS) return(true);
	return (false);
}
