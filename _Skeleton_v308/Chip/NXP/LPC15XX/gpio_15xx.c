// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: gpio_15xx.c
// 	   Version: 1.0
//  Created on: 01.05.2014
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//	  Desc: GPIO Access
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//


//#include "Skeleton.h"

// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************


// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************

// ******************************************************************************************************
// PUBLIC Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------
// De-Initialize GPIO block
bool Chip_GPIO_DeInit(uint8_t PeriIndex)
{
	switch(PeriIndex)
	{
		case 1: Chip_Clock_DisablePeriphClock(SYSCON_CLOCK_GPIO1); break;
		case 2: Chip_Clock_DisablePeriphClock(SYSCON_CLOCK_GPIO2); break;
		case 0: Chip_Clock_DisablePeriphClock(SYSCON_CLOCK_GPIO0); break;
		default: return(false);
	}
	
	// ak su vypnute vsetky GPIO, vypni aj MUX
	if(( Chip_Clock_GetPeriphClock(SYSCON_CLOCK_GPIO0) & Chip_Clock_GetPeriphClock(SYSCON_CLOCK_GPIO1) & Chip_Clock_GetPeriphClock(SYSCON_CLOCK_GPIO2)) > 0) __nop();
	else Chip_Clock_DisablePeriphClock(SYSCON_CLOCK_MUX);
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Set a GPIO direction
void Chip_GPIO_WriteDirBit(LPC_GPIO_PORT_Type *LPC_GPIO, uint32_t port, uint8_t bit, bool setting)
{
	if (setting) {
		LPC_GPIO->DIR[port] |= 1UL << bit;
	}
	else {
		LPC_GPIO->DIR[port] &= ~(1UL << bit);
	}
}

// ------------------------------------------------------------------------------------------------
// Set Direction for a GPIO port 
void Chip_GPIO_SetDir(LPC_GPIO_PORT_Type *LPC_GPIO, uint8_t portNum, uint32_t bitValue, uint8_t out)
{
	if (out) {
		LPC_GPIO->DIR[portNum] |= bitValue;
	}
	else {
		LPC_GPIO->DIR[portNum] &= ~bitValue;
	}
}

// ------------------------------------------------------------------------------------------------
// Set GPIO direction for a single GPIO pin
void Chip_GPIO_SetPinDIR(LPC_GPIO_PORT_Type *LPC_GPIO, uint8_t port, uint8_t pin, bool output)
{
	if (output) {
		Chip_GPIO_SetPinDIROutput(LPC_GPIO, port, pin);
	}
	else {
		Chip_GPIO_SetPinDIRInput(LPC_GPIO, port, pin);
	}
}

// ------------------------------------------------------------------------------------------------
// Set GPIO direction for a all selected GPIO pins to an input or output
void Chip_GPIO_SetPortDIR(LPC_GPIO_PORT_Type *LPC_GPIO, uint8_t port, uint32_t pinMask, bool outSet)
{
	if (outSet) {
		Chip_GPIO_SetPortDIROutput(LPC_GPIO, port, pinMask);
	}
	else {
		Chip_GPIO_SetPortDIRInput(LPC_GPIO, port, pinMask);
	}
}


// ------------------------------------------------------------------------------------------------
// Initialize GPIOGP block - Group Interrupt Block
// result is true:successfull otherwise false
bool Chip_GPIOGP_Init(uint8_t PeriIndex)
{
	Chip_Clock_EnablePeriphClock(SYSCON_CLOCK_GINT);
	Chip_SYSCON_PeriphReset(RESET_GINT);
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Set selected pins for the group and port to low level trigger
STATIC INLINE void Chip_GPIOGP_SelectLowLevel(LPC_GINT_T *pGPIOGPINT, uint8_t group, uint8_t port, uint32_t pinMask)
{
	pGPIOGPINT[group].PORT_POL[port] &= ~pinMask;
}

// ------------------------------------------------------------------------------------------------
// Enable selected pins for the group interrupt
STATIC INLINE void Chip_GPIOGP_EnableGroupPins(LPC_GINT_T *pGPIOGPINT, uint8_t group, uint8_t port, uint32_t pinMask)
{
	pGPIOGPINT[group].PORT_ENA[port] |= pinMask;
}

// ------------------------------------------------------------------------------------------------
// Selected GPIO group functionality for trigger on all matching pins in group (AND mode)
STATIC INLINE void Chip_GPIOGP_SelectAndMode(LPC_GINT_T *pGPIOGPINT, uint8_t group)
{
	pGPIOGPINT[group].CTRL |= GPIOGR_COMB;
}

// ------------------------------------------------------------------------------------------------
// Selected GPIO group functionality edge trigger mode
STATIC INLINE void Chip_GPIOGP_SelectEdgeMode(LPC_GINT_T *pGPIOGPINT, uint8_t group)
{
	pGPIOGPINT[group].CTRL &= ~GPIOGR_TRIG;
}

