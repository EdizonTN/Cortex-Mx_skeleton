// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: _serie_lpc11xx.h
// 	   Version: 1.0
//  Created on: 2023.05.01
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: MCU of series LPC11xx description/configuration file. Same for all MCUs of this family!
//              Special, device depents features, can be included outside of this file (in parent header)
// ******************************************************************************
// Usage:
// 
// ToDo:	viz _Chip_UART_Set_AdrDet
// 
// Changelog:   
//				2023.05.01 - v1.0 	-  first revision. 




// ************************************************************** CHIP CONFIGURATION *******************
// In your project, please define next constants:
//#define CONF_CHIP_FROFREQ_VAL 		2             								// 0 = 18 MHz
//                                   												// 1 = 24 MHz (reset value)
//                                   												// 2 = 30 MHz

//#define CONF_CHIP_MAINCLK_SEL 		0      										// 00 = fro (reset value)
//                                   												// 01 = external_clk
//                                   												// 10 = lposc_clk
//                                   												// 11 = fro_div

//#define CONF_CHIP_SYSAHBCLKDIV_VAL 	1         									// 0x00 = system_ahb_clk disabled (use with caution)
//                                  												// 0x01 = divide_by_1 (reset value)
//                                   												// 0x02 = divide_by_2
//                                   												// 0xFF = divide_by_255

//#define CONF_CHIP_CLKIN_VAL 			12000000     								// External Clock (CLKIN) frequency [Hz] must be in the range of  1 MHz to  25 MHz     

//#define EXT_CLOCK_FORCE_ENABLE 		0   										// Force config. and enable of external_clk for use by other than main_clk
//                                   												// 0 = external_clk will be configured and enabled only if needed by main_clk or sys_pll0_clk.
//                                   												// 1 = external_clk will be configured and enabled (available for other, e.g. clock out).
// *******************************************************************************************************








#ifndef __SERIE_LPC11XX_H_
#define __SERIE_LPC11XX_H_

#ifndef LOAD_SCATTER
    extern uint32_t SystemCoreClock;												// -//- pre IAP - System Clock Frequency (Core Clock)
    extern uint32_t OscRateIn;														// 
    extern uint32_t ExtRateIn;														// 
    extern uint32_t RTCOscRateIn;

    // for compatibility with mfg's header file
    typedef enum {ERROR = 0, SUCCESS = !ERROR} Status;
    typedef enum {RESET = 0, SET = !RESET} FlagStatus, IntStatus, SetState;
    typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;


    #define CORE_M0							1
	
	// Call CMSIS Peripheral Access Layer:
	#if   defined (CONF_CHIP_ID_LPC1114FDH28_102)
		#include    "LPC1100L.h" 													// CMSIS Peripheral Access Layer
	#elif defined (CONF_CHIP_ID_LPC1114FHN33_203)
		#include    "LPC1100XL.h" 													// CMSIS Peripheral Access Layer
	#elif defined (CONF_CHIP_ID_LPC1114FBD48_301)
		#include    "LPC11xx.h" 													// CMSIS Peripheral Access Layer
	#else
		#error "Unknown selected device!"
	#endif
#endif


// ************************************************************************************************
// PUBLIC Varioables for all LPC11XX MCU's
// ************************************************************************************************

#define	_CHIP_IRC_FREQUENCY				12000000UL									// Internal Oscilator: 12MHz

// for whole family LPC111x:

// Specific parameters for each chip from this series:
#if defined (CONF_CHIP_ID_LPC1114FBD48_301)


	#define IAP_ENTRY_LOCATION        	0X1FFF1FF1UL								// Pointer to ROM IAP entry functions
	//#define ROM_DRIVER_BASE 			(0x0F001FF8UL)	

	#define  _CHIP_I2C_COUNT			1											// A typical I2C-bus
	#define  _CHIP_WDT_COUNT			1											// Watchdog Timer (WDT)
	#define  _CHIP_WWDT_COUNT			0											// Windowed Watchdog Timer (WWDT)
	#define  _CHIP_UART_COUNT			1											// Universal asynchronous receiver-transmitter	
	#define  _CHIP_CTIMER_COUNT			4											// 16-bit and 32-bit counter/timer (have same register definitions)
	#define  _CHIP_ADC_COUNT			1											// Number of Analog-to-Digital Converters (ADC)
	#define	 _CHIP_ADC_CHANNELS_COUNT	8											// number of channels per ADC
	#define	 _CHIP_PMU_COUNT			1											// Power monitor unit
	#define  _CHIP_FLASHCTRL_COUNT		1											// Flash control unit
	#define  _CHIP_SPI_COUNT			2											// Serial Peripheral Interface
	#define  _CHIP_CAN_COUNT			0											// A typical CAN-bus
	#define  _CHIP_IOCON_COUNT			1											// IO Control
	#define  _CHIP_SYSCON_COUNT			1											// System Control
	#define  _CHIP_GPIO_COUNT			4											// General Purpose Input/Output
	#define	 _CHIP_GPIO_PINPERPORT		12											// each port has maximal 12 pins (0-11)
	
    #define  CHIP_ID                    0x0444102B                     				// Chip ID wroted in silicone
	#define  CHIP_ID1                   0x2540102B                     				// Chip ID wroted in silicone - alternate ID
	
	#define	 _CHIP_IRAM					0x2000										// size of internal RAM (datasheet value) - 8 kB
	#define	 _CHIP_IRAMSTART			0x10000000									// Start of RAM
	#define	 _CHIP_IFLASH				0x8000										// size of internal FLASH (datasheet value) - 32kB
				// GPIO Port.PIN		0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.10, 0.11, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 1.10, 1.11, 2.0, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 2.10, 2.11, 3.0, 3.1, 3.2, 3.3, 3.4, 3.5
	#define  _CHIP_IOCON_REMAP			{ 3,   4,   7,  11,  12,  13,  19,  20,  24,  25,   26,   29,  30,  31,  32,  36,  37,  40,  41,  42,   5,  14,   27,   38,   2,  10,  23,  35,  16,  17,   0,   8,   9,  21,   22,   28,  33,  34,  39,  43,  15,  18};	// IOCON registers is not in fully ascending in memory!!!
	#define	 _CHIP_GPIO_LAST_PIN		11											// Last GPIO PIN

	#define	 IRQHANDLER_16				_CHIP_WAKEUP0_IRQ_Handler
	#define	 IRQHANDLER_17				_CHIP_WAKEUP1_IRQ_Handler
	#define	 IRQHANDLER_18				_CHIP_WAKEUP2_IRQ_Handler
	#define	 IRQHANDLER_19				_CHIP_WAKEUP3_IRQ_Handler
	#define	 IRQHANDLER_20				_CHIP_WAKEUP4_IRQ_Handler
	#define	 IRQHANDLER_21				_CHIP_WAKEUP5_IRQ_Handler
	#define	 IRQHANDLER_22				_CHIP_WAKEUP6_IRQ_Handler
	#define	 IRQHANDLER_23				_CHIP_WAKEUP7_IRQ_Handler
	#define	 IRQHANDLER_24				_CHIP_WAKEUP8_IRQ_Handler
	#define	 IRQHANDLER_25				_CHIP_WAKEUP9_IRQ_Handler
	#define	 IRQHANDLER_26				_CHIP_WAKEUP10_IRQ_Handler
	#define	 IRQHANDLER_27				_CHIP_WAKEUP11_IRQ_Handler
	#define	 IRQHANDLER_28				_CHIP_WAKEUP12_IRQ_Handler
	#define  IRQHANDLER_29				_CHIP_CAN0_IRQ_Handler
	#define	 IRQHANDLER_30				_CHIP_SPI1_IRQ_Handler
	#define	 IRQHANDLER_31				_CHIP_I2C0_IRQ_Handler
	#define	 IRQHANDLER_32				_CHIP_TIMER0_16_IRQ_Handler
	#define	 IRQHANDLER_33				_CHIP_TIMER1_16_IRQ_Handler
	#define	 IRQHANDLER_34				_CHIP_TIMER0_32_IRQ_Handler
	#define	 IRQHANDLER_35				_CHIP_TIMER1_32_IRQ_Handler
	#define	 IRQHANDLER_36				_CHIP_SPI0_IRQ_Handler
	#define	 IRQHANDLER_37				_CHIP_UART0_IRQ_Handler
	#define	 IRQHANDLER_38				_Chip_Default_IRQ_Handler					// this IRQ num is unused
	#define	 IRQHANDLER_39				_Chip_Default_IRQ_Handler					// this IRQ num is unused
	#define	 IRQHANDLER_40				_CHIP_ADC0_IRQ_Handler
	#define	 IRQHANDLER_41				_CHIP_WDT0_IRQ_Handler
	#define	 IRQHANDLER_42				_CHIP_BOD0_IRQ_Handler
	#define	 IRQHANDLER_43				_Chip_Default_IRQ_Handler					// this IRQ num is unused
	#define	 IRQHANDLER_44				_CHIP_EINT3_IRQ_Handler
	#define	 IRQHANDLER_45				_CHIP_EINT2_IRQ_Handler
	#define	 IRQHANDLER_46				_CHIP_EINT1_IRQ_Handler
	#define	 IRQHANDLER_47				_CHIP_EINT0_IRQ_Handler


#elif defined (CONF_CHIP_ID_LPC1112FDH20_102)
	#define  _CHIP_I2C_COUNT			0											// A typical I2C-bus
#elif defined (CONF_CHIP_ID_LPC11Cxx)
	#define  _CHIP_CAN_COUNT			1											// A typical CAN-bus
#else
 #error CONF_CHIP_ID_XXXXXXX not defined!
#endif


#ifndef LOAD_SCATTER


//----------------------------------------------------------------------------
// Validate the the user's selctions
//----------------------------------------------------------------------------
#define CHECK_RANGE(val, min, max)		((val < min) || (val > max))
#define CHECK_RSVD(val, mask)			(val & mask)

#if (CHECK_RANGE((CONF_CHIP_FROFREQ_VAL), 0, 2))
   #error "CONF_CHIP_FROFREQ_VAL: Value out of range."
#endif

#if (CHECK_RSVD((CONF_CHIP_MAINCLK_SEL),  ~0x00000003))
   #error "MAINCLKSEL: Invalid values of reserved bits!"
#endif

#if (CHECK_RANGE((CONF_CHIP_SYSAHBCLKDIV_VAL), 0, 255))
   #error "SYSAHBCLKDIV: Value out of range!"
#endif

#if (CHECK_RANGE(CONF_CHIP_CLKIN_VAL, 1000000, 25000000)) && (CONF_CHIP_CLKIN_VAL != 0)
   #error "CLKIN frequency is out of bounds"
#endif



// ******************************************************************************************************
// PUBLIC Values for all LPC11XX MCU's
// ******************************************************************************************************

#define CPU_NONISR_EXCEPTIONS   	    (15)									    // Cortex M0 exceptions /without SP!/
#define CPU_IRQ_NUMOF 				    (32)										// Vendor and family specific external interrupts. See users manual!


// harmonizing names
// Some manufacturers header files, using different peripherial structure names. Now, we harmonize it into one names.
// now used from chip header file:		Harmonization to:
#if defined(_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
typedef LPC_I2C_TypeDef    				_CHIP_I2C_T;
#endif

#if defined(_CHIP_WDT_COUNT) && (_CHIP_WDT_COUNT > 0)
typedef LPC_WDT_TypeDef        			_CHIP_WDT_T;
#endif

#if defined(_CHIP_WWDT_COUNT) && (_CHIP_WWDT_COUNT > 0)
typedef WWDT_Type           			_CHIP_WWDT_T;
#endif

#if defined(_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
typedef LPC_UART_TypeDef          		_CHIP_UART_T;
#endif

#if defined(_CHIP_CTIMER_COUNT) && (_CHIP_CTIMER_COUNT > 0)
typedef LPC_TMR_TypeDef	            	_CHIP_TIMER_T;
#endif

#if defined(_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)
typedef LPC_ADC_TypeDef	            	_CHIP_ADC_T;
#endif

#if defined(_CHIP_PMU_COUNT) && (_CHIP_PMU_COUNT > 0)
typedef LPC_PMU_TypeDef	            	_CHIP_PMU_T;
#endif

#if defined(_CHIP_FLASHCTRL_COUNT) && (_CHIP_FLASHCTRL_COUNT > 0)
typedef LPC_FLASHCTRL_Type		       	_CHIP_FLASHCTRL_T;
#endif

#if defined(_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
typedef LPC_SSP_TypeDef        			_CHIP_SPI_T;
#endif

#if defined(_CHIP_CAN_COUNT) && (_CHIP_CAN_COUNT > 0)
typedef LPC_CAN_TypeDef        			_CHIP_CAN_T;
#endif

#if defined(_CHIP_IOCON_COUNT) && (_CHIP_IOCON_COUNT > 0)
typedef LPC_IOCON_TypeDef        		_CHIP_IOCON_T;
#endif

#if defined(_CHIP_SYSCON_COUNT) && (_CHIP_SYSCON_COUNT > 0)
typedef LPC_SYSCON_TypeDef        		_CHIP_SYSCON_T;
#endif

#if defined(_CHIP_GPIO_COUNT) && (_CHIP_GPIO_COUNT > 0)
typedef LPC_GPIO_TypeDef  				_CHIP_GPIO_T;
#endif



#define I2C0					LPC_I2C
#define WDT0					LPC_WDT
#define UART0					LPC_UART
/*#define LPC_TMR16B0           ((LPC_TMR_TypeDef    *) LPC_CT16B0_BASE)
#define LPC_TMR16B1           ((LPC_TMR_TypeDef    *) LPC_CT16B1_BASE)
#define LPC_TMR32B0           ((LPC_TMR_TypeDef    *) LPC_CT32B0_BASE)
#define LPC_TMR32B1           ((LPC_TMR_TypeDef    *) LPC_CT32B1_BASE) */
#define ADC0					LPC_ADC
#define PMU0					LPC_PMU
#define FLASHCTRL0				LPC_FLASHCTRL
#define SPI0					LPC_SSP0
#define SPI1					LPC_SSP1
#define CAN0					LPC_CAN
#define IOCON0					LPC_IOCON
#define SYSCON0					LPC_SYSCON
#define GPIO0					LPC_GPIO0
#define GPIO1					LPC_GPIO1
#define GPIO2					LPC_GPIO2
#define GPIO3					LPC_GPIO3





#define		SYSAHBCLKCTRL0				SYSAHBCLKCTRL
#define		PRESETCTRL0					PRESETCTRL


/*! @name PDRUNCFG - Power configuration register */
/*! @{ */
#define SYSCON_PDRUNCFG_IRCOUT_PD_MASK           (0x1U)
#define SYSCON_PDRUNCFG_IRCOUT_PD_SHIFT          (0U)
#define SYSCON_PDRUNCFG_IRCOUT_PD(x)             (((uint32_t)(((uint32_t)(x)) << SYSCON_PDRUNCFG_IRCOUT_PD_SHIFT)) & SYSCON_PDRUNCFG_IRCOUT_PD_MASK)
#define SYSCON_PDRUNCFG_IRC_PD_MASK              (0x2U)
#define SYSCON_PDRUNCFG_IRC_PD_SHIFT             (1U)
#define SYSCON_PDRUNCFG_IRC_PD(x)                (((uint32_t)(((uint32_t)(x)) << SYSCON_PDRUNCFG_IRC_PD_SHIFT)) & SYSCON_PDRUNCFG_IRC_PD_MASK)
#define SYSCON_PDRUNCFG_FLASH_PD_MASK            (0x4U)
#define SYSCON_PDRUNCFG_FLASH_PD_SHIFT           (2U)
#define SYSCON_PDRUNCFG_FLASH_PD(x)              (((uint32_t)(((uint32_t)(x)) << SYSCON_PDRUNCFG_FLASH_PD_SHIFT)) & SYSCON_PDRUNCFG_FLASH_PD_MASK)
#define SYSCON_PDRUNCFG_BOD_PD_MASK              (0x8U)
#define SYSCON_PDRUNCFG_BOD_PD_SHIFT             (3U)
#define SYSCON_PDRUNCFG_BOD_PD(x)                (((uint32_t)(((uint32_t)(x)) << SYSCON_PDRUNCFG_BOD_PD_SHIFT)) & SYSCON_PDRUNCFG_BOD_PD_MASK)
#define SYSCON_PDRUNCFG_ADC_PD_MASK              (0x10U)
#define SYSCON_PDRUNCFG_ADC_PD_SHIFT             (4U)
#define SYSCON_PDRUNCFG_ADC_PD(x)                (((uint32_t)(((uint32_t)(x)) << SYSCON_PDRUNCFG_ADC_PD_SHIFT)) & SYSCON_PDRUNCFG_ADC_PD_MASK)
#define SYSCON_PDRUNCFG_SYSOSC_PD_MASK           (0x20U)
#define SYSCON_PDRUNCFG_SYSOSC_PD_SHIFT          (5U)
#define SYSCON_PDRUNCFG_SYSOSC_PD(x)             (((uint32_t)(((uint32_t)(x)) << SYSCON_PDRUNCFG_SYSOSC_PD_SHIFT)) & SYSCON_PDRUNCFG_SYSOSC_PD_MASK)
#define SYSCON_PDRUNCFG_WDTOSC_PD_MASK           (0x40U)
#define SYSCON_PDRUNCFG_WDTOSC_PD_SHIFT          (6U)
#define SYSCON_PDRUNCFG_WDTOSC_PD(x)             (((uint32_t)(((uint32_t)(x)) << SYSCON_PDRUNCFG_WDTOSC_PD_SHIFT)) & SYSCON_PDRUNCFG_WDTOSC_PD_MASK)
#define SYSCON_PDRUNCFG_SYSPLL_PD_MASK           (0x80U)
#define SYSCON_PDRUNCFG_SYSPLL_PD_SHIFT          (7U)
#define SYSCON_PDRUNCFG_SYSPLL_PD(x)             (((uint32_t)(((uint32_t)(x)) << SYSCON_PDRUNCFG_SYSPLL_PD_SHIFT)) & SYSCON_PDRUNCFG_SYSPLL_PD_MASK)
#define SYSCON_PDRUNCFG_ACMP_MASK                (0x8000U)
#define SYSCON_PDRUNCFG_ACMP_SHIFT               (15U)
#define SYSCON_PDRUNCFG_ACMP(x)                  (((uint32_t)(((uint32_t)(x)) << SYSCON_PDRUNCFG_ACMP_SHIFT)) & SYSCON_PDRUNCFG_ACMP_MASK)
/*! @} */

/*! @name WDTOSCCTRL - Watchdog oscillator control */
/*! @{ */
#define SYSCON_WDTOSCCTRL_DIVSEL_MASK            (0x1FU)
#define SYSCON_WDTOSCCTRL_DIVSEL_SHIFT           (0U)
#define SYSCON_WDTOSCCTRL_DIVSEL(x)              (((uint32_t)(((uint32_t)(x)) << SYSCON_WDTOSCCTRL_DIVSEL_SHIFT)) & SYSCON_WDTOSCCTRL_DIVSEL_MASK)
#define SYSCON_WDTOSCCTRL_FREQSEL_MASK           (0x1E0U)
#define SYSCON_WDTOSCCTRL_FREQSEL_SHIFT          (5U)
#define SYSCON_WDTOSCCTRL_FREQSEL(x)             (((uint32_t)(((uint32_t)(x)) << SYSCON_WDTOSCCTRL_FREQSEL_SHIFT)) & SYSCON_WDTOSCCTRL_FREQSEL_MASK)
/*! @} */
/*! @name MAINCLKSEL - Main clock source select */
/*! @{ */
#define SYSCON_MAINCLKSEL_SEL_MASK               (0x3U)
#define SYSCON_MAINCLKSEL_SEL_SHIFT              (0U)
#define SYSCON_MAINCLKSEL_SEL(x)                 (((uint32_t)(((uint32_t)(x)) << SYSCON_MAINCLKSEL_SEL_SHIFT)) & SYSCON_MAINCLKSEL_SEL_MASK)
/*! @} */

/*! @name MAINCLKUEN - Main clock source update enable */
/*! @{ */
#define SYSCON_MAINCLKUEN_ENA_MASK               (0x1U)
#define SYSCON_MAINCLKUEN_ENA_SHIFT              (0U)
#define SYSCON_MAINCLKUEN_ENA(x)                 (((uint32_t)(((uint32_t)(x)) << SYSCON_MAINCLKUEN_ENA_SHIFT)) & SYSCON_MAINCLKUEN_ENA_MASK)
/*! @} */

/*! @name SYSPLLCLKSEL - System PLL clock source select register */
/*! @{ */
#define SYSCON_SYSPLLCLKSEL_SEL_MASK             (0x3U)
#define SYSCON_SYSPLLCLKSEL_SEL_SHIFT            (0U)
#define SYSCON_SYSPLLCLKSEL_SEL(x)               (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSPLLCLKSEL_SEL_SHIFT)) & SYSCON_SYSPLLCLKSEL_SEL_MASK)
/*! @} */

/*! @name SYSPLLCLKUEN - System PLL clock source update enable register */
/*! @{ */
#define SYSCON_SYSPLLCLKUEN_ENA_MASK             (0x1U)
#define SYSCON_SYSPLLCLKUEN_ENA_SHIFT            (0U)
#define SYSCON_SYSPLLCLKUEN_ENA(x)               (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSPLLCLKUEN_ENA_SHIFT)) & SYSCON_SYSPLLCLKUEN_ENA_MASK)
/*! @} */

/*! @name SYSAHBCLKCTRL - System clock control */
/*! @{ */
#define SYSCON_SYSAHBCLKCTRL0_SYS_MASK            (0x1U)
#define SYSCON_SYSAHBCLKCTRL0_SYS_SHIFT           (0U)
#define SYSCON_SYSAHBCLKCTRL0_SYS(x)              (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_SYS_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_SYS_MASK)
#define SYSCON_SYSAHBCLKCTRL0_ROM_MASK            (0x2U)
#define SYSCON_SYSAHBCLKCTRL0_ROM_SHIFT           (1U)
#define SYSCON_SYSAHBCLKCTRL0_ROM(x)              (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_ROM_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_ROM_MASK)
#define SYSCON_SYSAHBCLKCTRL0_RAM0_1_MASK         (0x4U)
#define SYSCON_SYSAHBCLKCTRL0_RAM0_1_SHIFT        (2U)
#define SYSCON_SYSAHBCLKCTRL0_RAM0_1(x)           (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_RAM0_1_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_RAM0_1_MASK)
#define SYSCON_SYSAHBCLKCTRL0_FLASHREG_MASK       (0x8U)
#define SYSCON_SYSAHBCLKCTRL0_FLASHREG_SHIFT      (3U)
#define SYSCON_SYSAHBCLKCTRL0_FLASHREG(x)         (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_FLASHREG_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_FLASHREG_MASK)
#define SYSCON_SYSAHBCLKCTRL0_FLASH_MASK          (0x10U)
#define SYSCON_SYSAHBCLKCTRL0_FLASH_SHIFT         (4U)
#define SYSCON_SYSAHBCLKCTRL0_FLASH(x)            (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_FLASH_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_FLASH_MASK)
#define SYSCON_SYSAHBCLKCTRL0_I2C0_MASK           (0x20U)
#define SYSCON_SYSAHBCLKCTRL0_I2C0_SHIFT          (5U)
#define SYSCON_SYSAHBCLKCTRL0_I2C0(x)             (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_I2C0_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_I2C0_MASK)
#define SYSCON_SYSAHBCLKCTRL0_GPIO_MASK           (0x40U)
#define SYSCON_SYSAHBCLKCTRL0_GPIO_SHIFT          (6U)
#define SYSCON_SYSAHBCLKCTRL0_GPIO(x)             (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_GPIO_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_GPIO_MASK)
#define SYSCON_SYSAHBCLKCTRL0_CT16B0_MASK         (0x80U)
#define SYSCON_SYSAHBCLKCTRL0_CT16B0_SHIFT        (7U)
#define SYSCON_SYSAHBCLKCTRL0_CT16B0(x)           (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_CT16B0_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_CT16B0_MASK)
#define SYSCON_SYSAHBCLKCTRL0_CT16B1_MASK         (0x100U)
#define SYSCON_SYSAHBCLKCTRL0_CT16B1_SHIFT        (8U)
#define SYSCON_SYSAHBCLKCTRL0_CT16B1(x)           (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_CT16B1_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_CT16B1_MASK)
#define SYSCON_SYSAHBCLKCTRL0_CT32B0_MASK         (0x200U)
#define SYSCON_SYSAHBCLKCTRL0_CT32B0_SHIFT        (9U)
#define SYSCON_SYSAHBCLKCTRL0_CT32B0(x)           (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_CT32B0_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_CT32B0_MASK)
#define SYSCON_SYSAHBCLKCTRL0_CT32B1_MASK         (0x400U)
#define SYSCON_SYSAHBCLKCTRL0_CT32B1_SHIFT        (10U)
#define SYSCON_SYSAHBCLKCTRL0_CT32B1(x)           (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_CT32B1_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_CT32B1_MASK)
#define SYSCON_SYSAHBCLKCTRL0_SPI0_MASK           (0x800U)
#define SYSCON_SYSAHBCLKCTRL0_SPI0_SHIFT          (11U)
#define SYSCON_SYSAHBCLKCTRL0_SPI0(x)             (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_SPI0_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_SPI0_MASK)
#define SYSCON_SYSAHBCLKCTRL0_UART0_MASK          (0x1000U)
#define SYSCON_SYSAHBCLKCTRL0_UART0_SHIFT         (12U)
#define SYSCON_SYSAHBCLKCTRL0_UART0(x)            (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_UART0_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_UART0_MASK)
#define SYSCON_SYSAHBCLKCTRL0_ADC0_MASK           (0x2000U)
#define SYSCON_SYSAHBCLKCTRL0_ADC0_SHIFT          (13U)
#define SYSCON_SYSAHBCLKCTRL0_ADC0(x)             (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_ADC0_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_ADC0_MASK)

#define SYSCON_SYSAHBCLKCTRL0_WDT0_MASK           (0x8000U)
#define SYSCON_SYSAHBCLKCTRL0_WDT0_SHIFT          (15U)
#define SYSCON_SYSAHBCLKCTRL0_WDT0(x)             (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_WDT0_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_WDT0_MASK)
#define SYSCON_SYSAHBCLKCTRL0_IOCON_MASK          (0x10000U)
#define SYSCON_SYSAHBCLKCTRL0_IOCON_SHIFT         (16U)
#define SYSCON_SYSAHBCLKCTRL0_IOCON(x)            (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_IOCON_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_IOCON_MASK)
#define SYSCON_SYSAHBCLKCTRL0_CAN_MASK            (0x20000U)
#define SYSCON_SYSAHBCLKCTRL0_CAN_SHIFT           (17U)
#define SYSCON_SYSAHBCLKCTRL0_CAN(x)              (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_CAN_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_CAN_MASK)
#define SYSCON_SYSAHBCLKCTRL0_SPI1_MASK           (0x40000U)
#define SYSCON_SYSAHBCLKCTRL0_SPI1_SHIFT          (18U)
#define SYSCON_SYSAHBCLKCTRL0_SPI1(x)             (((uint32_t)(((uint32_t)(x)) << SYSCON_SYSAHBCLKCTRL0_SPI1_SHIFT)) & SYSCON_SYSAHBCLKCTRL0_SPI1_MASK)
/*! @} */

/*! @name PRESETCTRL - Peripheral reset control register */
/*! @{ */
#define SYSCON_PRESETCTRL0_SPI0_RST_N_MASK        (0x1U)
#define SYSCON_PRESETCTRL0_SPI0_RST_N_SHIFT       (0U)
#define SYSCON_PRESETCTRL0_SPI0_RST_N(x)          (((uint32_t)(((uint32_t)(x)) << SYSCON_PRESETCTRL0_SPI0_RST_N_SHIFT)) & SYSCON_PRESETCTRL0_SPI0_RST_N_MASK)
#define SYSCON_PRESETCTRL0_I2C0_RST_N_MASK        (0x2U)
#define SYSCON_PRESETCTRL0_I2C0_RST_N_SHIFT       (1U)
#define SYSCON_PRESETCTRL0_I2C0_RST_N(x)          (((uint32_t)(((uint32_t)(x)) << SYSCON_PRESETCTRL0_I2C0_RST_N_SHIFT)) & SYSCON_PRESETCTRL0_I2C0_RST_N_MASK)
#define SYSCON_PRESETCTRL0_SPI1_RST_N_MASK        (0x4U)
#define SYSCON_PRESETCTRL0_SPI1_RST_N_SHIFT       (2U)
#define SYSCON_PRESETCTRL0_SPI1_RST_N(x)          (((uint32_t)(((uint32_t)(x)) << SYSCON_PRESETCTRL0_SPI1_RST_N_SHIFT)) & SYSCON_PRESETCTRL0_SPI1_RST_N_MASK)
#define SYSCON_PRESETCTRL0_CAN0_RST_N_MASK        (0x8U)
#define SYSCON_PRESETCTRL0_CAN0_RST_N_SHIFT       (3U)
#define SYSCON_PRESETCTRL0_CAN0_RST_N(x)          (((uint32_t)(((uint32_t)(x)) << SYSCON_PRESETCTRL0_UART0_RST_N_SHIFT)) & SYSCON_PRESETCTRL0_UART0_RST_N_MASK)


#define SYSCON_PRESETCTRL0_UARTFRG_RST_N_MASK     (0x4U)
#define SYSCON_PRESETCTRL0_UARTFRG_RST_N_SHIFT    (2U)
#define SYSCON_PRESETCTRL0_UARTFRG_RST_N(x)       (((uint32_t)(((uint32_t)(x)) << SYSCON_PRESETCTRL0_UARTFRG_RST_N_SHIFT)) & SYSCON_PRESETCTRL0_UARTFRG_RST_N_MASK)
#define SYSCON_PRESETCTRL0_UART0_RST_N_MASK       (0x8U)
#define SYSCON_PRESETCTRL0_UART0_RST_N_SHIFT      (3U)
#define SYSCON_PRESETCTRL0_UART0_RST_N(x)         (((uint32_t)(((uint32_t)(x)) << SYSCON_PRESETCTRL0_UART0_RST_N_SHIFT)) & SYSCON_PRESETCTRL0_UART0_RST_N_MASK)
/*! @} */

/*! @name CTRL - ADC Control register. Contains the clock divide value, resolution selection, sampling time selection, and mode controls. */
/*! @{ */
#define ADC_CTRL_CLKDIV_MASK                     (0xFF00U)
#define ADC_CTRL_CLKDIV_SHIFT                    (8U)
#define ADC_CTRL_CLKDIV(x)                       (((uint32_t)(((uint32_t)(x)) << ADC_CTRL_CLKDIV_SHIFT)) & ADC_CTRL_CLKDIV_MASK)
#define ADC_CTRL_LPWRMODE_MASK                   (0x400U)
#define ADC_CTRL_LPWRMODE_SHIFT                  (10U)
#define ADC_CTRL_LPWRMODE(x)                     (((uint32_t)(((uint32_t)(x)) << ADC_CTRL_LPWRMODE_SHIFT)) & ADC_CTRL_LPWRMODE_MASK)
#define ADC_CTRL_CALMODE_MASK                    (0x40000000U)
#define ADC_CTRL_CALMODE_SHIFT                   (30U)
#define ADC_CTRL_CALMODE(x)                      (((uint32_t)(((uint32_t)(x)) << ADC_CTRL_CALMODE_SHIFT)) & ADC_CTRL_CALMODE_MASK)
/*! @} */




















// ******************************************************************************************************
// CHIP functions
// ******************************************************************************************************

enum	_swo_protocol 
{
  kSWO_ProtocolManchester = 1U,
  kSWO_ProtocolNrz = 2U
};

// Public functions exactly for this series of MCU, but uniform with CHAL System:
extern void _Chip_SWO_Init(uint32_t Main_CPU_Clock);
extern void _Chip_SystemInit(void);
extern void _Chip_Read_ResetSource(uint32_t *dst);




// ******************************************************************************************************
// IAP CHIP functions
// ******************************************************************************************************
#define _CHIP_IAP_PREWRRITE_CMD           50										// Prepare sector for write operation command
#define _CHIP_IAP_WRISECTOR_CMD           51										// Write Sector command
#define _CHIP_IAP_ERSSECTOR_CMD           52										// Erase Sector command
#define _CHIP_IAP_BLANK_CHECK_SECTOR_CMD  53										// Blank check sector
#define _CHIP_IAP_REPID_CMD               54										// Read PartID command
#define _CHIP_IAP_READ_BOOT_CODE_CMD      55										// Read Boot code version
#define _CHIP_IAP_COMPARE_CMD             56										// Compare two RAM address locations
#define _CHIP_IAP_REINVOKE_ISP_CMD        57										// Reinvoke ISP
#define _CHIP_IAP_READ_UID_CMD            58										// Read UID
#define _CHIP_IAP_ERASE_PAGE_CMD          59										// Erase page

typedef void (*IAP_ENTRY_T)(unsigned int[], unsigned int[]);

extern inline void _Chip_IAP_Entry(unsigned int cmd_param[], unsigned int status_result[])
{
	((IAP_ENTRY_T) IAP_ENTRY_LOCATION)(cmd_param, status_result);
}

// Public functions exactly for this series of MCU
extern bool _Chip_IAP_Read_ID(uint32_t *pDst);
extern bool _Chip_IAP_Read_SerialNum(uint32_t *pDst);
extern bool _Chip_IAP_Read_BootCodeVersion(uint32_t *pDst);
extern bool _Chip_IAP_PrepareSector(uint32_t strSector, uint32_t endSector);

 
// ******************************************************************************************************
// CLOCK functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------
// "mainclk"
// return clock source for core sysclk
extern uint32_t	_Chip_Clock_Get_MainClk_Rate(void);
extern uint32_t _Chip_Clock_Get_SysClk_Rate(void);									// read system clock
extern uint32_t _Chip_Clock_Get_SourceClk_Rate(void);								// Get input source clock rate.
extern uint32_t _Chip_Clock_Get_CoreClk_Rate(void);
extern uint32_t _Chip_Clock_Get_ClkOut_Rate(void);

extern uint32_t  SystemCoreClock;													// for IAP - System Clock Frequency (Core Clock)
extern uint32_t _Chip_Clock_Get_SysPLL_Out_Rate(void);								// Get system pll output rate
extern uint32_t _Chip_Clock_Get_Wdt_Inp_Rate(void);
extern uint32_t _Chip_Clock_Get_SysPLL_In_Rate(void);								// Get system pll input rate
extern uint32_t _Chip_Clock_Get_ADCClk_Rate(void);










// ******************************************************************************************************
// GPIO Functions
// ******************************************************************************************************
#define PINUNUSED						0x0000										// Not used pin of MCU - write 0xff into PINENABLE - default state
#define PFUN_GPIO						0x0000										// no alternate function, Pin as std GPIO
#define PFUN_ALT(y)						(y & 0x07)									// Write y value into IOCON bits 2:0

#define IOCON_MODE_PULLDOWN     		(0x1 << 3)									// Selects pull-down function
#define IOCON_MODE_PULLUP       		(0x2 << 3)									// Selects pull-up function
#define IOCON_MODE_REPEATER     		(0x3 << 3)									// Selects pin repeater function
#define IOCON_HYS_EN            		(0x1 << 5)									// Enables hysteresis

#define	IOCON_ADMODE					(0x0 << 7)									// Analog input mode
#define	IOCON_DIGMODE					(0x1 << 7)									// Digital function mode

#define IOCON_I2CMODE_STD				(0x0 << 8)									// Standard/Fast mode I2C
#define IOCON_I2CMODE_GPIO				(0x1 << 8)									// deactivate I2C and set as GPIO
#define IOCON_I2CMODE_FASTPLUS			(0x2 << 8)									// Fast-mode+ I2C
#define IOCON_OPENDRAIN_EN      		(0x1 << 10)									// Enables open-drain function

COMP_PACKED_BEGIN
typedef struct	_chip_IO_Spec														// Additional GPIO structure based on chip specific requirements */
{
	uint16_t							IOCON_Reg;									// IOCON register hodnota. pouziva sa len 16 bit. Inak IOCON je 32 bitovy	 */
}_Chip_IO_Spec_t;
COMP_PACKED_END

extern void*	_Chip_GPIO_Init ( int32_t PeriIndex, uint32_t Pin);
extern bool 	_Chip_GPIO_DeInit ( int32_t PeriIndex, uint32_t Pin);
extern void 	_Chip_GPIO_Wr_Conf_Pin (uint32_t Port, uint32_t Pin, uint8_t Reg_Idx,  _Chip_IO_Spec_t *SWMRegs);
extern void*	_Chip_GPIO_PINT_Conf(uint8_t PINTSel, uint32_t Port, uint32_t Pin, uint8_t Sens);
extern void*	_Chip_Get_GPIO_Ptr(int32_t PeriIndex);
extern void 	_Chip_GPIO_Set_PinDir(_CHIP_GPIO_T *pGPIO, uint8_t portNum, uint32_t bitValue, uint8_t out);
extern bool 	_Chip_GPIO_GetPinState(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin);
extern void 	_Chip_GPIO_SetPinOutLow(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin);
extern void 	_Chip_GPIO_SetPinOutHigh(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin);
extern void 	_Chip_GPIO_SetPinToggle(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin);











// ******************************************************************************************************
// I2C Functions
// ******************************************************************************************************

// Interrupt status flag defines:
#define	CHIP_I2C_IRQSTAT_MSTPENDING		(1 << 0)									// Master Pending flag
#define	CHIP_I2C_IRQSTAT_MSARBLOSS		(1 << 4)									// Master Arbitration Loss flag.
#define	CHIP_I2C_IRQSTAT_MSSTSTPERR		(1 << 6)									// Master Start/Stop Error flag
#define	CHIP_I2C_IRQSTAT_MSSTATE_CHANGED	5										// MSSTATE was changed - this flag mus be out of mask !!!
// namiesto MSTCHANGED by sa mal pouzit MSTPENDING
#define	CHIP_I2C_MSSTATE_MASK			0x0e
#define CHIP_I2C_MSSTATE_RX				(1 << 1)
#define CHIP_I2C_MSSTATE_TX				(2 << 1)
#define CHIP_I2C_MSSTATE_NACK_ADR		(3 << 1)
#define CHIP_I2C_MSSTATE_NACK_DATA		(4 << 1)
#define CHIP_I2C_IRQSTAT_MONRDY			(1 << 16)									// Monitor Ready Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_MONOV			(1 << 17)									// Monitor Overflow Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_MONIDLE		(1 << 19)									// Monitor Idle Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_SLVDESEL		(1 << 15)									// Slave Deselect Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_SLVNOTSTR		(1 << 11)									// Slave not stretching Clock Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_SLVPENDING		(1 << 8)									// Slave Pending Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_EVENTTIMEOUT	(1 << 24)									// Event Timeout Interrupt Flag
#define CHIP_I2C_IRQSTAT_SCLTIMEOUT		(1 << 25)									// SCL Timeout Interrupt Flag


typedef struct 
{
	const uint8_t 						*pTxBuff;									// Pointer to array of bytes to be transmitted
	uint8_t 							*pRxBuff;									// Pointer memory where bytes received from I2C be stored
	uint16_t 							TxSz;										// Number of bytes in transmit array, if 0 only receive transfer will be carried on
	uint16_t 							RxSz;										// Number of bytes to received, if 0 only transmission we be carried on
	uint16_t 							Status;										// Status of the current I2C transfer
	uint8_t 							SlaveAddr;									// 7-bit I2C Slave address
} _Chip_I2CM_XFER_T;


extern int32_t 	_Chip_I2C_Get_PeriIndex(void *pPeri);
extern void*	_Chip_I2C_Init(int32_t PeriIndex);										// init I2C by periphary number. Return is pointer to periphery if success
extern uint32_t _Chip_I2C_Get_BusSpeed(void *pPeri);								// Return Current bus speed
extern bool 	_Chip_I2C_Set_BusSpeed(void *pPeri, uint32_t Speed_kHz);				// Max Speed: 100kHz, 400kHz, 1MHz,...
extern bool 	_Chip_I2C_Configure(void *pPeri, uint32_t Speed_kHz, bool MasterMode, bool SlaveMode, bool MonitorMode, uint16_t Address);
extern uint32_t _Chip_I2C_Get_Peri_Status(void *pPeri);
extern bool 	_Chip_I2C_Clear_Peri_Status(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_I2CMST_Get_IRQ_Status(void *pPeri);
	
extern bool 	_Chip_I2CMST_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_I2CSLV_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_I2CMON_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_I2CMST_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_I2CSLV_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_I2CMON_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_I2CTimeout_Enable_IRQ_NVIC(void *pPeri, bool NewState);

extern uint32_t _Chip_I2CSLV_Get_IRQ_Status(void *pPeri);
extern uint32_t _Chip_I2CMON_Get_IRQ_Status(void *pPeri);
extern uint32_t _Chip_I2CTimeout_Get_IRQ_Status(void *pPeri);
extern bool 	_Chip_I2CMST_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	_Chip_I2CSLV_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	_Chip_I2CMON_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	_Chip_I2CTimeout_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	_Chip_I2CMST_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	_Chip_I2CSLV_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	_Chip_I2CMON_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	_Chip_I2CTimeout_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern void 	_Chip_I2CMST_Put_Data(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_I2CMST_Get_Data(void *pPeri);
extern void 	_Chip_I2CMST_Set_Start(void *pPeri);								// create start impulse
extern void 	_Chip_I2CMST_Set_Stop(void *pPeri);
extern void 	_Chip_I2CMST_Set_Cont(void *pPeri);
extern void 	_Chip_I2CMST_Reset(void *pPeri);







// ******************************************************************************************************
// UART Functions
// ******************************************************************************************************

// UART Interrupt STAT register definitions
#define CHIP_UART_IRQSTAT_RXRDY			USART_IRQSTAT_RXRDY_MASK					// Receiver ready
#define CHIP_UART_IRQSTAT_TXRDY			USART_IRQSTAT_TXRDY_MASK					// Transmitter ready for data
#define CHIP_UART_IRQSTAT_TXIDLE		USART_IRQSTAT_TXIDLE_MASK					// Transmitter idle
//#define CHIP_UART_IRQSTAT_RXIDLE		USART_IRQSTAT_RXIDLE_MASK					// Receiver idle

#define	CHIP_UART_PERISTAT_RXIDLE		USART_STAT_RXIDLE_MASK						// Periphery status bit
#define	CHIP_UART_PERISTAT_TXIDLE		USART_STAT_TXIDLE_MASK						// Periphery status bit
		




extern int32_t 	_Chip_UART_Get_PeriIndex(void *pPeri);
extern bool 	_Chip_UART_Set_AdrDet(void* pPeri, bool NewVal);
extern bool 	_Chip_UART_Set_Address(void* pPeri, uint8_t NewAddress);
extern uint8_t 	_Chip_UART_Get_Address(void* pPeri);
extern uint32_t _Chip_UART_Get_Peri_Status(void *pPeri);
extern bool 	_Chip_UART_Clear_Peri_Status(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_UART_Get_IRQ_Status(void *pPeri);
extern bool 	_Chip_UART_Clear_IRQ_Status(void *pPeri,uint32_t IRQBitMask);
extern void 	_Chip_UART_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState);
extern void 	_Chip_UART_Put_Data(void *pPeri, uint32_t WrData);
extern uint32_t _Chip_UART_Get_Data(void *pPeri);
extern void 	_Chip_UART_Flush_Tx(void *pPeri);
extern void 	_Chip_UART_Flush_Rx(void *pPeri);
extern void*	_Chip_UART_Init(int32_t PeriIndex);
extern bool 	_Chip_UART_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_UART_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_UART_Set_Baud(void* pPeri, uint32_t NewBaud);
extern uint32_t _Chip_UART_Get_Baud(void* pPeri);
extern bool 	_Chip_UART_Set_Conf(void* pPeri, uint8_t DataLen, uint8_t Parity, uint8_t StopBits);
extern bool 	_Chip_UART_Set_RTS_Conf(void *pPeri, uint8_t sRTS_Port, uint8_t sRTS_Pin);
extern bool 	_Chip_UART_Set_CTS_Conf(void *pPeri, uint8_t sCTS_Port, uint8_t sCTS_Pin);
extern bool 	_Chip_UART_Set_DIRDE_Conf(void *pPeri, uint8_t DIRDE_Port, uint8_t DIRDE_Pin, bool DIRDE_H_Active);
extern bool 	_Chip_UART_Set_RE_Conf(void *pPeri, uint8_t RE_Port, uint8_t RE_Pin, bool RE_H_Active);










// ******************************************************************************************************
// SPI Functions
// ******************************************************************************************************

#define	CHIP_SPI_IRQSTAT_RXDONE							(1 << 0)					// Receiver Done Flag new Data available
#define	CHIP_SPI_IRQSTAT_TXRDY							(1 << 1)					// Transmitter Ready Flag
#define	CHIP_SPI_IRQSTAT_RXOV							(1 << 2)					// Receiver Overrun IRQ Flag
#define	CHIP_SPI_IRQSTAT_TXUR							(1 << 3)					// Transmitter underrun IRQ Flag
#define	CHIP_SPI_IRQSTAT_SSA							(1 << 4)					// Slave Select Assert
#define	CHIP_SPI_IRQSTAT_SSD							(1 << 5)					// Slave Select Deassert

#define	CHIP_SPI_IRQEN_RXDONE							(1 << 0)					// Receiver Done IRQ enable - new data available
#define	CHIP_SPI_IRQEN_TXRDY							(1 << 1) 					// Transmitter Ready IRQ enable - ready before send
#define	CHIP_SPI_IRQEN_RXOV								(1 << 2)					// Receiver Overrun IRQ enable
#define	CHIP_SPI_IRQEN_TXUR								(1 << 3)					// Transmitter underrun IRQ enable
#define	CHIP_SPI_IRQEN_SSA								(1 << 4)					// Slave Select Assert IRQ enable
#define	CHIP_SPI_IRQEN_SSD								(1 << 5)					// Slave Select Deassert IRQ enable


extern void*	_Chip_SPI_Init(int32_t PeriIndex);
extern bool 	_Chip_SPI_Enable(void *pPeri, bool NewState);
extern void 	_Chip_SPI_Set_ClockDiv(void *pPeri, uint32_t clkdiv);
extern bool 	_Chip_SPI_Set_BusSpeed(void *pPeri, uint32_t Speed_Hz);
extern uint32_t _Chip_SPI_Get_BusSpeed(void *pPeri);
extern bool 	_Chip_SPI_Configure(void *pPeri, uint32_t Speed_Hz, bool MasterMode, bool SlaveMode, bool LSB_First, bool CPHA, bool CPOL);
extern bool 	_Chip_SPI_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern int32_t 	_Chip_SPI_Get_PeriIndex(void *pPeri);
extern void 	_Chip_SPI_Put_Data(void *pPeri, uint32_t NewValue, uint8_t NumOfBits, uint32_t CtrlFlags, uint8_t SSel);
extern uint32_t _Chip_SPI_Get_Data(void *pPeri);
extern uint32_t _Chip_SPI_Get_IRQ_Status(void *pPeri);
extern bool 	_Chip_SPI_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_SPI_Get_Peri_Status(void *pPeri);
extern bool 	_Chip_SPI_Clear_Peri_Status(void *pPeri, uint32_t BitMask);
extern bool 	_Chip_SPI_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern void 	_Chip_SPI_SendEOT(void *pPeri);
extern void 	_Chip_SPI_SendEOF(void *pPeri);
extern void 	_Chip_SPI_ReceiveIgnore(void *pPeri);
extern void 	_Chip_SPI_Flush_Tx(void *pPeri);
extern void 	_Chip_SPI_Flush_Rx(void *pPeri);





// ******************************************************************************************************
// USB Functions
// ******************************************************************************************************
// - not used -







// ******************************************************************************************************
// ADC Functions
// ******************************************************************************************************

typedef enum 
{
	ADC_SEQA_IDX,
	ADC_SEQB_IDX
} ADC_SEQ_IDX_T;


extern int32_t 	_Chip_ADC_Get_PeriIndex(void *pPeri);
extern void*	_Chip_ADC_Init(int32_t PeriIndex);
extern bool 	_Chip_ADC_Enable(void* pPeri, uint32_t ChannelBitMask, bool NewState);
extern uint32_t _Chip_ADC_Get_ChannelMask(void *pPeri);
extern bool 	_Chip_ADC_Configure (void *pPeri, uint32_t ChannelBitMask, uint32_t Speed_kHz, bool RepeatMode, bool DMAMode);
extern bool 	_Chip_ADC_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_ADC_Power(void *pPeri, bool NewState);
extern uint32_t _Chip_ADC_Get_CHIRQ_Status(_CHIP_ADC_T *pADC);
extern bool 	_Chip_ADC_Clear_CHIRQ_Status(_CHIP_ADC_T *pADC, uint32_t ChannelBitMask);
extern void 	_Chip_ADC_Start(void *pPeri, uint32_t ChannelBitMask);
extern bool 	_Chip_ADC_IRQ_Peri_Enable(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	_Chip_Temp_Enable(bool NewState);















// ******************************************************************************************************
// TIMER Functions - MRTx
// ******************************************************************************************************
extern void*	_Chip_MRT_Init(int32_t PeriIndex);
extern bool 	_Chip_MRT_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool 	_Chip_MRT_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_MRT_Configure (void *pPeri, uint8_t Channel, uint32_t RateHz, bool RepeatMode);
extern void 	_Chip_MRT_Set_TmrVal(void *pPeri, uint8_t Channel, uint32_t TmrValue);
extern void 	_Chip_MRT_Reset_Tmr(void *pPeri, uint32_t ChannelBitMask);
extern uint32_t _Chip_MRT_Get_TmrVal(void *pPeri, uint8_t Channel);
extern int32_t 	_Chip_MRT_Get_PeriIndex(void *pPeri);
extern bool 	_Chip_MRT_Clear_CHIRQ_Status(void *pPeri, uint32_t ChannelBitMask);
extern uint32_t _Chip_MRT_Get_CHIRQ_Status(void *pPeri);
extern bool 	_Chip_MRT_Enable_CHIRQ(void *pPeri, uint32_t ChannelBitMask, bool NewState);

	
	
	
	







// ******************************************************************************************************
// DMA Functions
// ******************************************************************************************************
typedef struct
{
	uint32_t							SrcAdr;										// Source address
	uint16_t							SrcBSize;									// Source Burst size		  1/2/4 x Word
	uint8_t								SrcWSize;									// Source width: 8/16/32-bit
	uint8_t								SrcIncr;									// Source Increment
	uint32_t							SrcPeri;									// Source peripherial	
	uint32_t							DstAdr;										// Destination address
	uint16_t							DstBSize;									// Destination Burst size		  1/2/4 x Word
	uint8_t								DstWSize;									// Destination width: 8/16/32-bit
	uint8_t								DstIncr;									// Destination Increment
	uint32_t							DstPeri;									// Destination peripherial	
	uint8_t								XferIRQ;									// Select Interrupt generated after transfer
	uint32_t 							XferLen;									// length of transfer
	uint8_t								XferType;									// transfer type
	uint8_t								XferPrio;									// transfer priority
	uint32_t 							XferNextDescriptor;							// address of next linked descriptor
} _Chip_DMA_Xfer_t;

// DMA channel source/address/next descriptor
typedef struct 
{
	uint32_t  xfercfg;																// Transfer configuration (only used in linked lists and ping-pong configs)
	uint32_t  source;																// DMA transfer source end address
	uint32_t  dest;																	// DMA transfer desintation end address
	uint32_t  next;																	// Link to next DMA descriptor, must be 16 byte aligned
} _Chip_DMA_CHDesc_t;

// DMA SRAM table - this can be optionally used with the Chip_DMA_SetSRAMBase()
//   function if a DMA SRAM table is needed.

extern void*	_Chip_DMA_Init(int32_t PeriIndex);
extern bool 	_Chip_DMA_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool 	_Chip_DMA_Configure (void *pPeri, uint8_t Channel, _Chip_DMA_Xfer_t *Xfer );
extern int32_t 	_Chip_DMA_Get_PeriIndex(void *pPeri);
extern bool 	_Chip_DMA_Clear_CHIRQ_Status(void *pPeri, uint32_t ChannelBitMask);
extern uint32_t _Chip_DMA_Get_CHIRQ_Status(void *pPeri);
extern uint32_t _Chip_DMA_Get_CHControl(void *pPeri, uint32_t Channel);
extern bool 	_Chip_DMA_Enable_CHIRQ(void *pPeri, uint32_t ChannelBitMask, bool NewState);






// ************************************************************************************************
// PWM Functions 
// ************************************************************************************************
// - not used -









// ******************************************************************************************************
// EEPROM Functions
// ******************************************************************************************************

// Emulated EEPROM 
typedef struct	Emul_EEPROM
{
	void								*pPeri;										// ptr to emulated Peripherial
	bool								Init;										// flag - initialization
	bool								Enabled;									// flag - enable/disabled
	bool								Busy;										// flag - busy, In used - wait for release
	uint32_t							OnePage[];									// page array in RAM - needs to read/modify/write access
} _Chip_Emul_EEPROM_t;

extern _Chip_Emul_EEPROM_t	Emul_EEPROM;

extern void*	_Chip_Emulated_EEPROM_Init(uint32_t PeriIndex);
extern bool 	_Chip_Emulated_EEPROM_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_Emulated_EEPROM_IRQ_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_Emulated_EEPROM_Write(void *pPeri, uint32_t EEDst, uint8_t *pInBuff, size_t byteswr);
extern bool 	_Chip_Emulated_EEPROM_Read(void *pPeri, uint32_t EESrc, uint8_t *pOutBuff, size_t bytesrd);










// ------------------------------------------------------------------------------------------------
// SCT Functions
// ------------------------------------------------------------------------------------------------

extern int32_t _Chip_SCT_Get_PeriIndex(void *pPeri);
extern void* _Chip_SCT_Init(int32_t PeriIndex);
extern bool _Chip_SCT_Enable(void *pPeri, bool NewState);
extern bool _Chip_SCT_Configure(void *pPeri);
	




// ******************************************************************************************************
// SWM Functions
// ******************************************************************************************************
extern void _Chip_SWM_MovablePinAssign(uint8_t Reg_NumBlock, uint8_t NewRegVal);
extern void _Chip_SWM_EnableFixedPin(uint8_t pin);
extern void _Chip_SWM_DisableFixedPin(uint8_t pin);
extern void _Chip_SWM_FixedPinEnable(uint8_t pin, bool enable);
extern bool _Chip_SWM_IsFixedPinEnabled(uint8_t pin);

// ******************************************************************************************************
// CRC Functions
// ******************************************************************************************************
extern void* _Chip_CRC_Init(int32_t PeriIndex);

#endif 	// ! LOAD_SCATTER

#endif	//__SERIE_LPC11XX_H_
