// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: Module_Manager.c
// 	   Version: 3.11
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: 
//
// Notice:
//
// Usage:
//			
// ToDo:
//
// Changelog:
//				2023.10.29	- v3.12 - fix modmgr_Load (load each second drivers)
//				2022.12.09	- v3.11 - add optimalization (if-else) switches
//				2022.11.28	- v3.1 - Add switch for extended functionality - CONF_USE_MOD_MANAGER_EXT
//				2021.07.12	- retry operation for init and enable module changed

#include "Skeleton.h"

#if defined(CONF_USE_MODDRV_MANAGER) && (CONF_USE_MODDRV_MANAGER == 1)
#define MOD_MANAGER_OBJECTNAME			"Module Manager"


// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************
_mod_Api_t				mod_pVoid_Api	__SECTION_ZEROINIT;							// Create a Void Api f-cions. Use in your own modules as empty.

// ******************************************************************************************************
// PRIVATE Variables and initialization
// ******************************************************************************************************
#if defined(CONF_USE_MOD_MANAGER_EXT) && (CONF_USE_MOD_MANAGER_EXT == 1)
_ColList_t 				modmgr_List		__SECTION_ZEROINIT;							// create module list and set to zero (uninitialized)
#endif

// ******************************************************************************************************
// Functions prototypes
// ******************************************************************************************************
bool modmgr_Load (_mod_If_t *pModIf, _drv_If_t **pRequiredDrvs);					// zavedenie modulu a jeho driverov do systemu
bool modmgr_Release (_mod_If_t *pModIf);											// uvolnenie modulu a jeho driverov zo systemu
bool modmgr_Enable (_mod_If_t *pModIf);												// Enable modulu
bool modmgr_Disable (_mod_If_t *pModIf);											// Disable modulu
bool modmgr_Write_Data (_mod_If_t *pModIf, _drv_If_t *pDstDrv, sys_Buffer_t *pbuff, uint32_t buflen, _XFerType_t XferType);	// zapis data s moznostou priamo do drivera
uint32_t modmgr_Read_Data (_mod_If_t *pModIf, _drv_If_t *pSrcDrv, sys_Buffer_t *pbuff, uint32_t maxlen, _XFerType_t XferType); // Citaj data s moznostou vyberu drivera
bool modmgr_Check_SetFlag(_mod_If_t *pModIf, struct _If_Status CheckedFlagMask);	


// ------------------------------------------------------------------------------------------------------
// Check module for selected flags (only with TRUE mark!)
// In case Flags in driver is not set, will called set routine (Load, Init, Enable) and check again (max 3x)
// If all flags from CheckedFlags are set, return is true, otherwise false.
// ------------------------------------------------------------------------------------------------------
#if defined(CONF_USE_MOD_MANAGER_EXT) && (CONF_USE_MOD_MANAGER_EXT == 1)
bool modmgr_Check_SetFlag(_mod_If_t *pModIf, struct _If_Status CheckedFlagMask)
{
	bool res = true;
	_If_Status_t OldStat = pModIf->Sys.Stat;										// save old status

	// If we need Check module for flag LOADED
	if(CheckedFlagMask.Loaded == true) 
	{
		uint16_t trycount = 3;														// How many times you try to call Load if need
		do
		{
			if(pModIf->Sys.Stat.Loaded == false) 									// ok, module has not set this flag, call load it
			{
				res = modmgr_Load (pModIf, (_drv_If_t **) pModIf->pRequiredDrvs );	// load module and all required drivers
			}
			if(pModIf->Sys.Stat.Loaded == true) { break;}							// ok, Module is Loaded
		}while(--trycount);															// try again
		if(pModIf->Sys.Stat.Loaded == false) 										// Module is still not Loaded ! 
		{
			return(false);															// exit
		}
	}

	
	// If we need Check module for flag INITIALIZED
	if((CheckedFlagMask.Initialized == true) || (OldStat.Initialized == true && pModIf->Sys.Stat.Initialized == false))
	{
		uint16_t trycount = CONF_MOD_MNGR_INIT_RETRY;								// How many times you try to call Initialize if need
		do
		{
			if(pModIf->Sys.Stat.Initialized == false) 								// ok, module has not set this flag, call load it
			{
				if(pModIf->pAPI->Init == NULL) res = modmgr_Init(pModIf);			// Initialize module
				else res = pModIf->pAPI->Init(pModIf);								// Initialize module using user function
			}
			if(pModIf->Sys.Stat.Initialized == true) { break;}						// ok, Module is Initialized
		}while(trycount--);															// Try again
		if(pModIf->Sys.Stat.Initialized == false) 									// Module is still not Initialized ! 
		{
			return(false);															// exit
		}
	}
	
	// If we need Check module for flag ENABLED
	// Performed init or load and Enabled has been set before.....
	if(((OldStat.Enabled == true) && pModIf->Sys.Stat.Enabled == false))
	{
		uint16_t trycount = CONF_MOD_MNGR_ENAB_RETRY;								// How many times you try to call Enable if need
		do
		{
			if(pModIf->Sys.Stat.Enabled == false) 									// ok, module has not set this flag, call enable it
			{
				if(pModIf->pAPI->Enable == NULL) res = modmgr_Enable (pModIf);		// Enabled module - all channel
				else res = pModIf->pAPI->Enable(pModIf);							// Enable module using user function
			}
			if(pModIf->Sys.Stat.Enabled == true) break;								// ok, Module is Enabled
		}while(trycount--);															// try again
		if(pModIf->Sys.Stat.Enabled == false) 										// Module is still not Enabled ! 
		{
			return(false);															// exit
		}
	}

	// Check status flag ENABLE - Nothing to do! Only return state...
	if(CheckedFlagMask.Enabled == true) 
	{
		if(pModIf->Sys.Stat.Enabled == false) 										// Module is still not Enabled ! 
			return(false);															// exit
	}
	return(res);
}
#endif			// CONF_USE_MOD_MANAGER_EXT

// ------------------------------------------------------------------------------------------------------
// Manager Routine
// Load module into list, Run HW and SW initialization. Load appropriate drivers also.
//	Input: pointer to module structure. MUST be filled with minimal information..
// ------------------------------------------------------------------------------------------------------
bool modmgr_Load (_mod_If_t *pModIf, _drv_If_t **pRequiredDrvs)
{
	bool res = true;

#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
	FN_DEBUG_ENTRY(pModIf->Name);
#endif	
	
	
// check for function are preset correctly:
//	if(pModIf->pAPI->Init == NULL) pModIf->pAPI->Init = (bool (*) (_mod_If_t*)) &sys_Empty;
//	if(pModIf->pAPI->Write_Data == NULL) pModIf->pAPI->Write_Data = (bool (*)( _mod_If_t*, _drv_If_t*, void *, uint32_t)) &sys_Empty;
//	if(pModIf->pAPI->Restart == NULL) pModIf->pAPI->Restart = (bool (*) (_mod_If_t*, bool)) &sys_Empty;
//	if(pModIf->pAPI->Release == NULL) pModIf->pAPI->Release = (bool (*) (_mod_If_t*)) &sys_Empty;
//	if(pModIf->pAPI->Enable == NULL) pModIf->pAPI->Enable = (bool (*) (_mod_If_t*)) &sys_Empty;
//	if(pModIf->pAPI->Disable == NULL) pModIf->pAPI->Disable = (bool (*) (_mod_If_t*)) &sys_Empty;
//	if(pModIf->pAPI->Write_Data == NULL) pModIf->pAPI->Write_Data = (bool (*) (_mod_If_t*, _drv_If_t*, void*, uint32_t)) &sys_Empty;
//	if(pModIf->pAPI->Read_Data == NULL) pModIf->pAPI->Read_Data = (uint32_t (*) (_mod_If_t*, _drv_If_t*, void*, uint32_t)) &sys_Empty;

	pModIf->Sys.Stat.Initialized = false;											// clear all flags.
	pModIf->Sys.Stat.Enabled = false;
	
#if (CONF_SYS_COLLECTIONS_USED == 1)
#if defined(CONF_USE_MOD_MANAGER_EXT) && (CONF_USE_MOD_MANAGER_EXT == 1)
	sys_DynCol_Add_Item( &modmgr_List, pModIf);											// add loaded module to global list
#endif	
#endif		// if (CONF_SYS_COLLECTIONS_USED == 1)



	// Now, Load all drivers from function argumet 
	
	uint8_t i = 0;
	do
	{
		{																			// try to load driver
			res = drvmgr_Load (pModIf, (_drv_If_t *) pRequiredDrvs[i++]);			// call loading of driver
		}
//		i ++;
	} while(pRequiredDrvs[i]);														// repeat until driver[i] exists
	
	
	pModIf->Sys.Stat.Loaded = res;													// set result
	
#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
    FN_DEBUG_EXIT(pModIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif		
	return(res);
}


// ------------------------------------------------------------------------------------------------
// Manager Routine
// Release selected module from list and all their associated drivers (if can!)
//		Ak je nastavena pUsr_Api->Release, vola sa ta.
// ------------------------------------------------------------------------------------------------
bool modmgr_Release(_mod_If_t *pModIf)
{
	bool res = false;

#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
	FN_DEBUG_ENTRY(pModIf->Name);
#endif	

	
	if(pModIf == NULL) 
	{
		goto ReleaseExit;
	}

	
	// check for module loaded
#if defined(CONF_USE_MOD_MANAGER_EXT) && (CONF_USE_MOD_MANAGER_EXT == 1)
	res = modmgr_Check_SetFlag(pModIf, (struct _If_Status) {.Loaded = true, .Initialized = false, .Enabled = false});
#else
	res = true;
#endif	

#if (CONF_SYS_COLLECTIONS_USED == 1)		
	if(pModIf->Sys.DrvLinked.pFirst == NULL) res = false;							// Check: Associated Loaded driver is valid?
#endif
	
	if(res == true)
	{

#if (CONF_SYS_COLLECTIONS_USED == 1)		
		_ColListRecord_t *tmpListRecord = NULL;
		do
		{
			tmpListRecord = sys_DynCol_Get_Record(&pModIf->Sys.DrvLinked, tmpListRecord);	// temporary pointer to parent module list and get next to tmpListRecord
			if (tmpListRecord == NULL) break;

			_drv_If_t *pDrv_If = tmpListRecord->pValue;								// create driver ptr
			
			if( pDrv_If->Sys.Stat.Loaded == true)									// Driver is Loaded ? 
			{
				if(tmpListRecord->pHead->Count > 1 )								// if current driver is linked to another module also, we cannot release it. Only erase link to pModIf.
				{
					res = sys_DynCol_Remove_Item(&pModIf->Sys.DrvLinked, pDrv_If);	// remove driver from module LoadedDriver List
					if(res == true) res = sys_DynCol_Remove_Item(&pDrv_If->Sys.ParentModLinked, pModIf);		// remove parent module from driver List
				}
				else
				{
					// Complette Release code here:
					if((pModIf->pAPI->Release != (void *) sys_Empty) && (pModIf->pAPI->Release != NULL)) // Have an user functions? 
					{
						res = pModIf->pAPI->Release(pModIf);						// Call a user release
					}
					else
					{																// no User function - use default write sequence
						res = drvmgr_Release(pModIf, pDrv_If);						// Release driver
					}
				}
					// --------------------
			}
		}while(tmpListRecord);	
#endif		// if (CONF_SYS_COLLECTIONS_USED == 1)



		if(res == true) 															// If all drivers are released, release module from system
		{ 
#if (CONF_SYS_COLLECTIONS_USED == 1)			
#if defined(CONF_USE_MOD_MANAGER_EXT) && (CONF_USE_MOD_MANAGER_EXT == 1)
			res = sys_DynCol_Remove_Item( &modmgr_List, pModIf);					// remove module from module list
#endif			
#endif		// if (CONF_SYS_COLLECTIONS_USED == 1)			
			
			if (res == true)														// module was released
			{
				pModIf->Sys.Stat.Enabled = false; 									// clearerr all flags
				pModIf->Sys.Stat.Initialized = false; 
				pModIf->Sys.Stat.Loaded = false; 
			}
		} 					
	}

ReleaseExit:	
#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
    FN_DEBUG_EXIT(pModIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ------------------------------------------------------------------------------------------------
// GENERIC Routine
// inicializacia periferie - vola sa vo vsetkych pripojenych driveroch. 
//		Ak je nastavena pUsr_Api->Init, vola sa ta.
// ------------------------------------------------------------------------------------------------
bool modmgr_Init(_mod_If_t *pModIf)
{
	bool res = false;

#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
	FN_DEBUG_ENTRY(pModIf->Name);
#endif	
	
	if(pModIf == NULL) 
	{
		goto InitExit;
	}

	
	// check for module loaded:
#if defined(CONF_USE_MOD_MANAGER_EXT) && (CONF_USE_MOD_MANAGER_EXT == 1)
	res = modmgr_Check_SetFlag(pModIf, (struct _If_Status) {.Loaded = true, .Initialized = false, .Enabled = false});
#else
	res = true;
#endif	

#if (CONF_SYS_COLLECTIONS_USED == 1)	
	if(pModIf->Sys.DrvLinked.pFirst == NULL) res = false;							// Check: Associated Loaded driver is valid?
#endif



	if(res == true)
	{	
			// mod_usr_xxx?
		if((pModIf->pAPI->Init != (void *) &sys_Empty) && (pModIf->pAPI->Init != NULL))
		{
			res = pModIf->pAPI->Init (pModIf);		// if set, Call a module user init function
		}
		else
		{
#if (CONF_SYS_COLLECTIONS_USED == 1)			
// GENERIC CODE HERE. YOU CAN COPY TO USR FUNCTIONS:
			_ColListRecord_t *tmpListRecord = NULL;
			do
			{
				tmpListRecord = sys_DynCol_Get_Record(&pModIf->Sys.DrvLinked, tmpListRecord);	// temporary pointer to parent module list and get next to tmpListRecord
				if (tmpListRecord == NULL) break;
				
				_drv_If_t *pDrv_If = tmpListRecord->pValue;							// create driver ptr
				if(pDrv_If != NULL)
				{
					// Extended Initialization code here:
					if(drvmgr_Init(pModIf, pDrv_If) == false) {res = false; break;}	// Init driver failed - whole module not init!
					// --------------------
				}
			}while(tmpListRecord);	
// END OF GENERIC CODE
#else		// if (CONF_SYS_COLLECTIONS_USED == 1)
			uint8_t i = 0;
			do
			{
					// Simple Initialization code here:
					if(drvmgr_Init(pModIf,(_drv_If_t *) pModIf->pRequiredDrvs[i++]) == false) {res = false; break;}	// Init driver failed - whole module not init!
					// --------------------
			}while(pModIf->pRequiredDrvs[i] != NULL);
#endif		// if (CONF_SYS_COLLECTIONS_USED == 1)			
		}
		if(res == true) { pModIf->Sys.Stat.Initialized = true; } 					// If all is OK, clear init flags
	}

	
	
	
	
	
InitExit:	
#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
    FN_DEBUG_EXIT(pModIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}



// ------------------------------------------------------------------------------------------------
// GENERIC Routine
// Enable modulu - prebehne vsetky jeho priradene drivery a povoli ich pre pouzitie
//		Ak je nastavena pUsr_Api->Enable, vola sa ta.
// ------------------------------------------------------------------------------------------------
bool modmgr_Enable (_mod_If_t *pModIf)								
{
	bool res = false;
	
#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
	FN_DEBUG_ENTRY(pModIf->Name);
#endif	
	
	if(pModIf == NULL) 
	{
		goto EnableExit;
	}


	// check for module loaded and initialized:
#if defined(CONF_USE_MOD_MANAGER_EXT) && (CONF_USE_MOD_MANAGER_EXT == 1)
	res = modmgr_Check_SetFlag(pModIf, (struct _If_Status) {.Loaded = true, .Initialized = true, .Enabled = false});
#else
	res = true;
#endif	

#if (CONF_SYS_COLLECTIONS_USED == 1)		
	if(pModIf->Sys.DrvLinked.pFirst == NULL) res = false;							// Check: Associated Loaded driver is valid?
#endif
	
	if(res == true)
	{	
		// mod_usr_xxx?
		if((pModIf->pAPI->Enable != (void *) &sys_Empty) && (pModIf->pAPI->Enable != NULL))
		{																			// if set, Call a module user init function
			pModIf->pAPI->Enable (pModIf);
		}
		else
		{
#if (CONF_SYS_COLLECTIONS_USED == 1)
// GENERIC CODE HERE. YOU CAN COPY TO USR FUNCTIONS:
			_ColListRecord_t *tmpListRecord = NULL;
			// check all associated driver for Loaded flag.
			do
			{
				tmpListRecord = sys_DynCol_Get_Record(&pModIf->Sys.DrvLinked, tmpListRecord);	// temporary pointer to parent module list and get next to tmpListRecord
				if (tmpListRecord == NULL) break;
				_drv_If_t *pDrv_If = tmpListRecord->pValue;	 						// create driver ptr
				// Simple enable code here:
				if(drvmgr_Enable(pModIf, pDrv_If, UINT32_MAX) == false) res = false;// Enable driver
				// --------------------

			}while(tmpListRecord);			
// END OF GENERIC CODE			
#else
			uint8_t i = 0;
			do
			{
					// Simple Initialization code here:
					if(drvmgr_Enable(pModIf,(_drv_If_t *) pModIf->pRequiredDrvs[i++], UINT32_MAX) == false) {res = false;}	// Enable driver
					// --------------------
			}while(pModIf->pRequiredDrvs[i] != NULL);
#endif		// if (CONF_SYS_COLLECTIONS_USED == 1)				
			
		}
		
		if(res == true) { pModIf->Sys.Stat.Enabled = true; } 						// If all is OK, Set enable flag
	}
	
EnableExit:	
#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
    FN_DEBUG_EXIT(pModIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// GENERIC Routine
// Disable modulu - prebehne vsetky jeho priradene drivery a zakaze ich pre pouzitie (ak nie su pouzite aj v inom module)
//		Ak je nastavena pUsr_Api->Disable, vola sa ta.
// ------------------------------------------------------------------------------------------------------
bool modmgr_Disable (_mod_If_t *pModIf)								
{
	bool res = false;
	
#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
	FN_DEBUG_ENTRY(pModIf->Name);
#endif	
	
	if(pModIf == NULL) 
	{
		goto DisableExit;
	}

	
#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
		dbgprint("\r\n("MOD_MANAGER_OBJECTNAME") ID[%s]: Set Disable", pModIf->Name);
#endif

	// check for module loaded and initialized:
#if defined(CONF_USE_MOD_MANAGER_EXT) && (CONF_USE_MOD_MANAGER_EXT == 1)
	res = modmgr_Check_SetFlag(pModIf, (struct _If_Status) {.Loaded = true, .Initialized = true, .Enabled = false});
#else
	res = true;
#endif	

#if (CONF_SYS_COLLECTIONS_USED == 1)	
	if(pModIf->Sys.DrvLinked.pFirst == NULL) res = false;							// Check: Associated Loaded driver is valid?
#endif		// if (CONF_SYS_COLLECTIONS_USED == 1)	
	
	
	
	if(res == true)
	{	
			// mod_usr_xxx?
		if((pModIf->pAPI->Disable != (void *) &sys_Empty) && (pModIf->pAPI->Disable != NULL))
		{																			// if set, Call a module user init function
			pModIf->pAPI->Disable (pModIf);
		}
		else
		{
#if (CONF_SYS_COLLECTIONS_USED == 1)
			_ColListRecord_t *tmpListRecord = NULL;
			// check all associated driver for Loaded flag.
			do
			{
				tmpListRecord = sys_DynCol_Get_Record(&pModIf->Sys.DrvLinked, tmpListRecord);	// temporary pointer to parent module list and get next to tmpListRecord
				if (tmpListRecord == NULL) break;
				_drv_If_t *pDrv_If = tmpListRecord->pValue;							// create driver ptr	
				// Simple enable code here:
				if(drvmgr_Disable(pModIf, pDrv_If, UINT32_MAX) == false) res = false;	// Disable driver
				// --------------------
			}while(tmpListRecord);			
#else
			uint8_t i = 0;
			do
			{
					// Simple Initialization code here:
					if(drvmgr_Disable(pModIf,(_drv_If_t *) pModIf->pRequiredDrvs[i++], UINT32_MAX) == false) {res = false;}	// Disable driver
					// --------------------
			}while(pModIf->pRequiredDrvs[i] != NULL);
#endif		// if (CONF_SYS_COLLECTIONS_USED == 1)			
		}
		
		if(res == true) { pModIf->Sys.Stat.Enabled = false; } 						// If all is OK, Clear enable flag
	}

DisableExit:	
#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
    FN_DEBUG_EXIT(pModIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// GENERIC Routine
// pDstDrv - generic pointer na dst driver. If is set, then data write directly to this driver only
// pbuff - value/data buffer
// buflen - Data Count to write (uint8_t)
// zpisuje data do vsetkych pripojenych driverov. 
//		Ak je nastavena pUsr_Api->Write_Data, vola sa ta.
// ------------------------------------------------------------------------------------------------------
bool modmgr_Write_Data (_mod_If_t *pModIf, _drv_If_t *pDstDrv_If, sys_Buffer_t *pbuff, uint32_t WrLen, _XFerType_t XferType) 	// zapis data
{
	bool res = false;

#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
	FN_DEBUG_ENTRY(pModIf->Name);
	FN_DEBUG_FMTPRINT(" >> pSrc: %p, BuffByteLen: %d, XFerType: 0x%.1X", pbuff, WrLen, XferType);
#endif	
	
	if(pModIf == NULL) 
	{
		goto WriteExit;
	}
	
	if(pDstDrv_If == NULL)															// Direct write to driver ?
	{																				// No, write to all aprropriate drivers
		__nop();
	}
	else
	{
		if(drvmgr_CheckParentMod(pModIf, pDstDrv_If) == false)						// mame parent v zozname?
		{
			return(false);							
		}
	}

	// check for module loaded:
#if defined(CONF_USE_MOD_MANAGER_EXT) && (CONF_USE_MOD_MANAGER_EXT == 1)	
	res = modmgr_Check_SetFlag(pModIf, (struct _If_Status) {.Loaded = true, .Initialized = true, .Enabled = true});
#else
	res = true;
#endif
	
	if(res == true)
	{	
			// mod_usr_xxx?
		if((pModIf->pAPI->Write_Data != (void *) &sys_Empty) && (pModIf->pAPI->Write_Data != NULL))
		{																			// if set, Call a module user init function
			res = pModIf->pAPI->Write_Data(pModIf, pDstDrv_If, pbuff, WrLen, XferType);
		}
		else
		{
			if(pDstDrv_If == NULL)																
			{																		// No Direct Write, write to all aprropriate drivers
#if (CONF_SYS_COLLECTIONS_USED == 1)
				if (pModIf->Sys.DrvLinked.pFirst == NULL) 							// Check: Associated Loaded driver is valid?
				{	
					return(false);													// Exit
				}
				// Generic write - to all associated drivers
				_ColListRecord_t *tmpListRecord = NULL;
				
				do
				{
					tmpListRecord = sys_DynCol_Get_Record(&pModIf->Sys.DrvLinked, tmpListRecord);	// temporary pointer to parent module list and get next to tmpListRecord
					if (tmpListRecord == NULL) break;
					_drv_If_t *pDrv_If = tmpListRecord->pValue;						// create driver ptr	

					// Write code here:
					res = drvmgr_Write_Data(pModIf, pDrv_If, pbuff, WrLen, XferType);// == buflen) res = true;	// Write data to driver. if writed count = bufle, result = true
					//else res = false;
					// --------------------

				}while(tmpListRecord);
#else
			uint8_t i = 0;
			do
			{
					// Simple Write code here:
					res = drvmgr_Write_Data(pModIf,(_drv_If_t *) pModIf->pRequiredDrvs[i++], pbuff, WrLen, XferType);	// Write Data to driver
					// --------------------
			}while(pModIf->pRequiredDrvs[i] != NULL);
#endif		//if (CONF_SYS_COLLECTIONS_USED == 1)
				
			}
			else		///////// DIRECT ACCESS:
			{																		// Yes, write direct to selected driver
				// Generic write - direct to destination driver

				// Write code here:
				res = drvmgr_Write_Data(pModIf, pDstDrv_If, pbuff, WrLen, XferType);			// Write data to driver
				// --------------------
			}
		}
	}
		
WriteExit:	
#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
    FN_DEBUG_EXIT(pModIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif		
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// GENERIC Routine
// pSrcDrv_If - if is set, Direct read will be applied. Otherwise read will be from all associated drivers till pbuff have free space. Starting from first.
// pbuff - destination buffer for readed data
// buflen - buffer length (uint8_t)
// return - num of transfered bytes
// 
//		Ak je nastavena pUsr_Api->Read_Data, vola sa ta.
// 		
// ------------------------------------------------------------------------------------------------------
uint32_t modmgr_Read_Data (_mod_If_t *pModIf, _drv_If_t *pSrcDrv_If, sys_Buffer_t *pbuff, uint32_t RdLen, _XFerType_t XferType)			// Citaj data
{
	bool res = false;
	uint32_t Result = 0;

#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
	FN_DEBUG_ENTRY(pModIf->Name);
	FN_DEBUG_FMTPRINT(" >> pDst: %p, MaxLen: %d, XFerType: 0x%.1X", pbuff, RdLen, XferType);	
#endif	
	
	if(pModIf == NULL) 
	{
		goto ReadExit;
	}

	if(pSrcDrv_If == NULL)															// Direct write to driver ?
	{																				// No, write to all aprropriate drivers
		__nop();
	}
	else
	{
		if(drvmgr_CheckParentMod(pModIf, pSrcDrv_If) == false) return(res);			// mame parent v zozname?
	}

	// check for module loaded:
#if defined(CONF_USE_MOD_MANAGER_EXT) && (CONF_USE_MOD_MANAGER_EXT == 1)	
	res = modmgr_Check_SetFlag(pModIf, (struct _If_Status) {.Loaded = true, .Initialized = true, .Enabled = true});
#else
	res = true;
#endif
	
	if(res == true)
	{	
		if((pModIf->pAPI->Read_Data != (void *) &sys_Empty) && (pModIf->pAPI->Read_Data != NULL))
		{																			// if set, Call a module user read function
			Result = pModIf->pAPI->Read_Data(pModIf, pSrcDrv_If, pbuff, RdLen, XferType);	
		}
		else
		{
			if(pSrcDrv_If == NULL)																
			{																		// No Direct Write, write to all aprropriate drivers
#if (CONF_SYS_COLLECTIONS_USED == 1)
				if (pModIf->Sys.DrvLinked.pFirst == NULL) 							// Check: Associated Loaded driver is valid?
				{	
					return(0);														// Exit
				}
				// Generic Read - to all associated drivers. Read from all drivers from first, till buffer have free space 
				_ColListRecord_t *tmpListRecord = NULL;
				do
				{
					tmpListRecord = sys_DynCol_Get_Record(&pModIf->Sys.DrvLinked, tmpListRecord);	// temporary pointer to parent module list and get next to tmpListRecord
					if (tmpListRecord == NULL) break;
					_drv_If_t *pDrv_If = tmpListRecord->pValue;						// create driver ptr	
					
					// Read code here:
					Result += drvmgr_Read_Data(pModIf, pDrv_If, pbuff, RdLen - Result, XferType);// Read data from driver
					// --------------------
					if(Result >= RdLen) break;										// if have ull buffer - exit
				}while(tmpListRecord);				
#else
				uint8_t i = 0;
				do
				{
						// Simple Read code here:
						Result += drvmgr_Read_Data(pModIf,(_drv_If_t *) pModIf->pRequiredDrvs[i++], pbuff, RdLen - Result, XferType);	// Read Data from driver
						// --------------------
				}while(pModIf->pRequiredDrvs[i] != NULL);
#endif		// if (CONF_SYS_COLLECTIONS_USED == 1)				
			}
			else		///////// DIRECT READ:
			{																		// Yes, read direct from selected driver
				// Generic Read - direct from source driver
				// Write code here:
				Result = drvmgr_Read_Data(pModIf, pSrcDrv_If, pbuff, RdLen, XferType);		// Read data to driver
				// --------------------
			}
		}
	}
	
ReadExit:
#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
    FN_DEBUG_EXIT(pModIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT("Xfer: %d", Result);											// dbg exit return print
#endif	
	return(Result);
}





// ------------------------------------------------------------------------------------------------------
// GENERIC Routine
// ------------------------------------------------------------------------------------------------------
#if defined(CONF_USE_MOD_MANAGER_EXT) && (CONF_USE_MOD_MANAGER_EXT == 1)
void modmgr_Event_Error (_mod_If_t pModIf)											// CallBack - chyba - modul/zariadenie nepripravene
{
#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
	dbgprint("\r\n("MOD_MANAGER_OBJECTNAME") ID[%s]: EVENT-Error", pModIf.Name );
#endif		
}


// ------------------------------------------------------------------------------------------------------
// GENERIC Routine
// ------------------------------------------------------------------------------------------------------
void modmgr_Event_StateChanged (_mod_If_t pModIf)									// CallBack - zmenil sa stav modulu, prisli data, alebo interrupt z asociovaneho drivera
{
#if defined(CONF_DEBUG_MOD_MANAGER) && (CONF_DEBUG_MOD_MANAGER == 1)
	dbgprint("\r\n("MOD_MANAGER_OBJECTNAME") ID[%s]: EVENT-StateChanged", pModIf.Name );
#endif		
}
#endif		// #if defined(CONF_USE_MOD_MANAGER_EXT) && (CONF_USE_MOD_MANAGER_EXT == 1)

#endif // CONF_USE_MODDRV_MANAGER
