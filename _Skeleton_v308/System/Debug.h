// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: Debug.h
// 	   Version: 1.0
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: Debug subsystem.
//		 Idea from: http://www.hitex.co.uk/fileadmin/uk-files/pdf/ARM%20Seminar%20Presentations%202013/
//							Feabhas%20Developing%20a%20Generic%20Hard%20Fault%20handler%20for%20ARM.pdf
// or: https://www.segger.com/downloads/application-notes/AN00016
//
// Notice:
//
// Usage:
//			
// ToDo:
//			- ako ID pri debug print pouzi makra: __MODULE__ a __func__
// 			- SWO Trace for MCUXpresso:
// 				https://www.nxp.com/docs/en/quick-reference-guide/MCUXpresso_IDE_SWO_Trace.pdf
//
// Changelog:
//				2022.11.08	- changed dbgprint macro

#ifndef __Debug_H_
#define __Debug_H_
#include 	<stdio.h>																// Standard IO declarations

#if defined(CONF_USE_DEBUG) && (CONF_USE_DEBUG == 1)



// ******************************************************************************************************
// CONFIGURATION
// ******************************************************************************************************
#define 	DEBUG_MAX_CALLBACKS_PER_CHANNELS			3							// maximalny pocet volani debugu out na jeden kanal [stdout]
#define 	SYS_syscall 								0x900001					// ToDo: netusim preco zrovna 0x900001 ....



// ******************************************************************************************************
// PRIVATE
// ******************************************************************************************************
COMP_PACKED_BEGIN
typedef struct
{
	void				(*fnPtr)();													// pointer to callback function
	uint8_t 			Active;														// active current callback?
}sysCallback_Type;
COMP_PACKED_END

COMP_PACKED_BEGIN
typedef struct 
{
	sysCallback_Type 	CallBackFn[DEBUG_MAX_CALLBACKS_PER_CHANNELS];
	uint32_t			Count;														// number of installed callbacks
	uint32_t			CountMax;													// allowed maximum of installed callbacks
}DebugCallBackChannel_Type;
COMP_PACKED_END

COMP_PACKED_BEGIN
typedef struct
{
 	DebugCallBackChannel_Type Callback_STDOUT;
 	DebugCallBackChannel_Type Callback_STDERR;
 	char				*ptr_Message;												// pointer to message's buffer
	char				SendChar;
 	uint16_t			Message_Lenght;												// message length
}Debug_st;
COMP_PACKED_END 

#if CONF_DEBUG_ITM																	// ladime cez ITM blok
//  ITM Stimulus  debug
#define ITM_Port8(n)    (*((volatile unsigned char *)(0xE0000000+4*n)))
#define ITM_Port16(n)   (*((volatile unsigned short*)(0xE0000000+4*n)))
#define ITM_Port32(n)   (*((volatile unsigned long *)(0xE0000000+4*n)))
#define DEMCR           (*((volatile unsigned long *)(0xE000EDFC)))
#define TRCENA          0x01000000
#endif


// ******************************************************************************************************
// Export
// ******************************************************************************************************
#if defined(CONF_USE_DEBUG) && (CONF_USE_DEBUG == 1)

 #if defined(CONF_LOG_SIZE) && (CONF_LOG_SIZE)
	extern uint32_t	LastLogChar;
	extern char dbg_LogArea[];
 #endif	
 
	extern void Debug_Init(void);
	extern volatile Debug_st _DEBUG_pIf;
	extern int Debug_Register_CallbackFunction(int file, void * fnPtr);
	extern void HardFault_Handler_C(unsigned int * hardfault_args); 					// prints 8 regs saved on the stack and so on

	#define FN_DEBUG_ENTRY(StringID)		dbgprint("\r\n %08lld:%s:%s:", (uint64_t) sys_System.Cnts->SysTickCount, __MODULE__, __FUNCTION__);\
											dbgprint(" ID[%s]", StringID);										
	#define FN_DEBUG_EXIT(StringID)			dbgprint("\r\n %08lld:%s:%s:", (uint64_t) sys_System.Cnts->SysTickCount, __MODULE__, __FUNCTION__);\
											dbgprint(" ID[%s]: ", StringID);
	#define FN_DEBUG_FMTPRINT(...)			dbgprint(__VA_ARGS__);
	#define FN_DEBUG_RES_BOOL(a)			(a==true? "Done":"Fail")


	#endif		// CONF_USE_DEBUG

	#ifdef dbgprint
	#undef dbgprint																	// undefine printf
	#endif
 #if defined(CONF_LOG_SIZE) && (CONF_LOG_SIZE)
	#define dbgprint(...) 	LastLogChar += sprintf(&dbg_LogArea[LastLogChar], __VA_ARGS__); // redirect printout
 #else	
	#define dbgprint(...)					printf(__VA_ARGS__);
 #endif
#else
	#define	dbgprint(...)					do { if (CONF_USE_DEBUG) printf(__VA_ARGS__); } while (0)
#endif	// CONF_USE_DEBUG
#endif /* __Debug_H_ */
