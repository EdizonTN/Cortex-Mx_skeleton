// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
// 	   Version: 3.00
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Board Support Package - Configuration header file example
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
// 

#ifndef __BSP_NXP_OM13056_CONFIG_H_
#define __BSP_NXP_OM13056_CONFIG_H_

// ************************************************************************************************
// Configuration - Chip MCU:
// ************************************************************************************************
//#define LPC1549JBD64					1											// Used MCU. KEIL uVision define this chip via config dialog.

#define BSP_TYPE_STRING					"OM13056"		      						// Hardware board name - string
#define BSP_VERSION_MAJOR				2											// board version major - integer
#define BSP_VERSION_MINOR				03											// board version minor - integer

//	Select manufacturer and Chip codename:
#define	CONF_CHIP_ID_LPC1549JBD64		1											// MCU definition

// Link with Chip.h settings	- don't touch!
#define CONF_CHIP_OSCRATEIN				BSP_EXQUARTZFREQ
#define CONF_CHIP_EXTRATEIN				BSP_EXRATEIN
#define CONF_CHIP_RTCRATEIN				BSP_RTCQUARTZFREQ

// If hardware allow SWD, set it here:
#define	BSP_DEBUG_SWD					1											// Serial Wire Debug Interface. zablokujes ULINK debugger !!
#define	BSP_DEBUG_SWO					1											// use SWO output also


// Used clock rate:
#define	BSP_EXQUARTZFREQ				(12000000)									// external crystal frequency. If not used, set to zero.
#define BSP_RTCQUARTZFREQ				(0)											// external RTC crystal frequency. If not used, set to zero.
#define	BSP_EXRATEIN					(0)											// external signal frequency. If not used, set to zero

#define BSP_UART_UPCLK					Core.MainClock								// perihery clock for UART


// Chip Clock settings
#define BSP_FREQ_PLL_M_VAL				5											// 5 PLL multiplier value (not bit value but real)
#define	BSP_FREQ_PLL_P_VAL				1											// 1 PLL divider value (not bit value but real)
#define	BSP_SYSAHBCLKDIV				1											// 1 AHB divider



// ************************************************************************************************
// Private declaration - for compatibility with other systems - 'don't change it
// ************************************************************************************************
#define BSP_VERSION_STRING				STR(BSP_VERSION_MAJOR) "." STR(BSP_VERSION_MINOR)	// concatenate integer values into string

#endif 		// __BSP_NXP_OM13056_CONFIG_H_
