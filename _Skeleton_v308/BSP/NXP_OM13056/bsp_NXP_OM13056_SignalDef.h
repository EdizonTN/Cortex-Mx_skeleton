// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: bsp_NXP_OM13056_SignalDef.h
// 	   Version: 3.0
//  Created on: 01.05.2014
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Pin Signal definition file for NXP OM13056 board
// ******************************************************************************
// Usage:
//
// ToDo:
//
// Changelog:
//

#ifndef __BSP_NXP_OM13056_SIGNALDEF_H_
#define __BSP_NXP_OM13056_SIGNALDEF_H_

#include <Chip\Chip_HAL.h>


// OM13056 uses LPC1549JBD64
//   			64-pin LQFP package

// ------------------------------------------------------------------------------------------------
// IO Pin Signal human readable declaration. 
//	Will be good, if signals will have it same name as is in schematic - for quick knowing
// Or assign some logical names. Ex Button1, I2C_SDA, LCD_PCLK, or similar
// ------------------------------------------------------------------------------------------------

//	----------------------------------- Description of the naming and defining:
//		Readable name:						(32* Port.Number) + Pin.Number			// Comment



#define	sig_PIO0_0_SCT0_OUT3				Sig_Array[(32* 0) + 0]						// P0_0 - PIO0_0-SCT0_OUT3
#define	sig_PIO0_1_SCT0_OUT4				Sig_Array[(32* 0) + 1]						// P0_1 - PIO0_1-SCT0_OUT4
#define	sig_PIO0_2_QEI_SCT0_IN				Sig_Array[(32* 0) + 2]						// P0_2 - PIO0_2-QEI-SCT0_IN
#define	sig_PIO0_3_SCT1_OUT4_GRN			Sig_Array[(32* 0) + 3]						// P0_3 - PIO0_3-SCT1_OUT4-GRN
#define	sig_PIO0_4_ADC0_4					Sig_Array[(32* 0) + 4]						// P0_4 - PIO0_4-ADC0_4
#define	sig_PIO0_5_ADC0_3					Sig_Array[(32* 0) + 5]						// P0_5 - PIO0_5-ADC0_3
#define	sig_PIO0_6_ADC0_2_SCT0_OUT0			Sig_Array[(32* 0) + 6]						// P0_6 - PIO0_6-ADC0_2-SCT0_OUT0
#define	sig_PIO0_7_ADC0_1					Sig_Array[(32* 0) + 7]						// P0_7 - PIO0_7-ADC0_1
#define	sig_PIO0_8_ADC0_0					Sig_Array[(32* 0) + 8]						// P0_8 - PIO0_8-ADC0_0
#define	sig_PIO0_9_ADC1_1					Sig_Array[(32* 0) + 9]						// P0_9 - PIO0_9-ADC1_1
#define	sig_PIO0_10_ADC1_2					Sig_Array[(32* 0) + 10]						// P0_10 - PIO0_10-ADC1_2
#define	sig_PIO0_11_CAN_RD					Sig_Array[(32* 0) + 11]						// P0_11 - PIO0_11-CAN_RD
#define	sig_PIO0_12_AIN_CTRL				Sig_Array[(32* 0) + 12]						// P0_12 - PIO0_12-AIN_CTRL
#define	sig_PIO0_13_ISP_RX					Sig_Array[(32* 0) + 13]						// P0_13 - PIO0_13-ISP_RX
#define	sig_PIO0_14							Sig_Array[(32* 0) + 14]						// P0_14 - PIO0_14
#define	sig_PIO0_15							Sig_Array[(32* 0) + 15]						// P0_15 - PIO0_15
#define	sig_PIO0_16							Sig_Array[(32* 0) + 16]						// P0_16 - PIO0_16
#define	sig_PIO0_17_QEI_SCT0_IN				Sig_Array[(32* 0) + 17]						// P0_17 - PIO0_17-QEI-SCT0_IN
#define	sig_PIO0_18_ISP_TX					Sig_Array[(32* 0) + 18]						// P0_18 - PIO0_18-ISP_TX
#define	sig_SWCLK_PIO0_19					Sig_Array[(32* 0) + 19]						// P0_19 - SWCLK-PIO0_19
#define	sig_IF_SWDIO						Sig_Array[(32* 0) + 20]						// P0_20 - IF_SWDIO
#define	sig_RESET_PIO0_21					Sig_Array[(32* 0) + 21]						// P0_21 - RESET-PIO0_21
#define	sig_PIO0_22_I2C_SCL					Sig_Array[(32* 0) + 22]						// P0_22 - PIO0_22-I2C_SCL
#define	sig_PIO0_23_I2C_SDA					Sig_Array[(32* 0) + 23]						// P0_23 - PIO0_23-I2C_SDA
#define	sig_PIO0_24_SCT0_OUT6				Sig_Array[(32* 0) + 24]						// P0_24 - PIO0_24-SCT0_OUT6
#define	sig_PIO0_25_BREAK_CTRL_RED			Sig_Array[(32* 0) + 25]						// P0_25 - PIO0_25-BREAK_CTRL-RED
#define	sig_PIO0_26_SCT0_OUT2				Sig_Array[(32* 0) + 26]						// P0_26 - PIO0_26-SCT0_OUT2
#define	sig_PIO0_27_CURR_TRIP				Sig_Array[(32* 0) + 27]						// P0_27 - PIO0_27-CURR_TRIP
#define	sig_PIO0_28_CURR_TRIP_RST			Sig_Array[(32* 0) + 28]						// P0_28 - PIO0_28-CURR_TRIP_RST
#define	sig_PIO0_29_SCT0_OUT1				Sig_Array[(32* 0) + 29]						// P0_29 - PIO0_29-SCT0_OUT1
#define	sig_PIO0_30_QEI_SCT0_IN				Sig_Array[(32* 0) + 30]						// P0_30 - PIO0_30-QEI-SCT0_IN
#define	sig_PIO0_31_CAN_TD					Sig_Array[(32* 0) + 31]						// P0_31 - PIO0_31-CAN_TD


#define	sig_PIO1_0_SCT_OUT					Sig_Array[(32* 1) + 0]						// P1_0 - PIO1_0-SCT_OUT
#define	sig_PIO1_1_BREAK_STS1_BLUE			Sig_Array[(32* 1) + 1]						// P1_1 - PIO1_1-BREAK_STS1-BLUE
#define	sig_PIO1_2_OLED_EN_SWO				Sig_Array[(32* 1) + 2]						// P1_2 - PIO1_2-OLED_EN-SWO
#define	sig_PIO1_3_ADC1_5					Sig_Array[(32* 1) + 3]						// P1_3 - PIO1_3-ADC1_5
#define	sig_PIO1_4_JOY_L					Sig_Array[(32* 1) + 4]						// P1_4 - PIO1_4-JOY_L
#define	sig_PIO1_5_JOY_C					Sig_Array[(32* 1) + 5]						// P1_5 - PIO1_5-JOY_C
#define	sig_PIO1_6_ACMP_I2_JOY_R			Sig_Array[(32* 1) + 6]						// P1_6 - PIO1_6-ACMP_I2-JOY_R
#define	sig_PIO1_7_JOY_U					Sig_Array[(32* 1) + 7]						// P1_7 - PIO1_7-JOY_U
#define	sig_PIO1_8_ACMP3_I3_JOY_D			Sig_Array[(32* 1) + 8]						// P1_8 - PIO1_8-ACMP3_I3-JOY_D
#define	sig_PIO1_9_UART_RS485_U1_TX_ISP0	Sig_Array[(32* 1) + 9]						// P1_9 - PIO1_9-UART_RS485-U1_TX-ISP0
#define	sig_PIO1_10_RS485_RTS_U1_RX			Sig_Array[(32* 1) + 10]						// P1_10 - PIO1_10-RS485_RTS-U1-RX
#define	sig_PIO1_11_USB_VBUS_ISP1			Sig_Array[(32* 1) + 11]						// P1_11 - PIO1_11-USB-VBUS-ISP1

#define	sig_PIO2_12_USB_DP					Sig_Array[(32* 2) + 12]						// P2_12 - PIO2_12-USB_DP
#define	sig_PIO2_13_USB_DM					Sig_Array[(32* 2) + 13]						// P2_13 - PIO2_13-USB_DM

#define	sig_virtual							Sig_Array[(32* 2) +14]						// virtual signal
#define	sig_last							Sig_Array[(32* 2) +15]						// virtual signal

	
// Buttons

// LED

// Accelerometer, MMA8652FCR1

// NOR FLASH QuadSPI (MX25R6435FM2IL0/W25Q128JVFIM)

// SD Card Slot

// LCD Display (RK043FN02H-CT) Connection

// LCD Display Touch Pad (RK043FN02H-CT) Connection (I2C signals - same as Accelerometer)

// Ethernet PHY (LAN8720A-CP)

// Audio Codec (WM8904CGEFL/V) Connection (I2C signals - same as Accelerometer)

// DMIC Digital Microphone


// SWD pindef
#define BSP_sig_SWDCLK			sig_SWCLK_PIO0_19
#define BSP_sig_SWDIO			sig_IF_SWDIO
#define BSP_sig_SWO				sig_PIO1_2_OLED_EN_SWO



// Preset Value for signals - config and value will be set after reset:
						// Port; Pin; Active; {Direction; IOCON reg				 			 		}   - for MCU without SWM Block!
						// Port; Pin; Active; {Direction; IOCON reg (16 bit); SWM reg (2x8 bits)	} 	- for MCU with SWM Block!

#define _SP0_0_FILL			{ 0,  0, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_1_FILL			{ 0,  1, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_2_FILL			{ 0,  2, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_3_FILL			{ 0,  3, 1, { CHAL_IO_Output, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO for LED Green
#define _SP0_4_FILL			{ 0,  4, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_5_FILL			{ 0,  5, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_6_FILL			{ 0,  6, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_7_FILL			{ 0,  7, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_8_FILL			{ 0xff,  0, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Used for SWDO
#define _SP0_9_FILL			{ 0,  9, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_10_FILL		{ 0,  10, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_11_FILL		{ 0,  11, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_12_FILL		{ 0,  12, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_13_FILL		{ 0,  13, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_14_FILL		{ 0,  14, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_15_FILL		{ 0,  15, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_16_FILL		{ 0,  16, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_17_FILL		{ 0,  17, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_18_FILL		{ 0,  18, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_19_FILL		{ 0xff,  0, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// used for SWCLK
#define _SP0_20_FILL		{ 0xff,  0, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// used for SWDIO
#define _SP0_21_FILL		{ 0,  21, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_22_FILL		{ 0,  22, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_23_FILL		{ 0,  23, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_24_FILL		{ 0,  24, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_25_FILL		{ 0,  25, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_26_FILL		{ 0,  26, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_27_FILL		{ 0,  27, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_28_FILL		{ 0,  28, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_29_FILL		{ 0,  29, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_30_FILL		{ 0,  30, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_31_FILL		{ 0,  31, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO

#define _SP1_0_FILL			{ 1,  0, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_1_FILL			{ 1,  1, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_2_FILL			{ 1,  2, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_3_FILL			{ 1,  3, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_4_FILL			{ 1,  4, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_5_FILL			{ 1,  5, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_6_FILL			{ 1,  6, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_7_FILL			{ 1,  7, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_8_FILL			{ 1,  8, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_9_FILL			{ 1,  9, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_10_FILL		{ 1,  10, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_11_FILL		{ 1,  11, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO

#define _SP2_12_FILL		{ 2,  12, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_13_FILL		{ 2,  13, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO


// And declare as extern
extern const CHAL_Signal_t 				Sig_Array[];				// array of the used signals
//extern const CHAL_Signal_t				*sig_last;

#endif	//__BSP_NXP_OM13098_SIGNALDEF_H_
