// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: bsp_NXP_OM13098_SignalDef.h
// 	   Version: 3.0
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Pin Signal definition file for NXP OM13098 board
// ******************************************************************************
// Usage:
//
// ToDo:
//
// Changelog:
//

#ifndef __BSP_NXP_OM13098_SIGNALDEF_H_
#define __BSP_NXP_OM13098_SIGNALDEF_H_

#include <Chip\Chip_HAL.h>


// OM13098 uses LPC54628J512ET180
//   			180-pin TFBGA package

// ------------------------------------------------------------------------------------------------
// IO Pin Signal human readable declaration. 
//	Will be good, if signals will have it same name as is in schematic - for quick knowing
// Or assign some logical names. Ex Button1, I2C_SDA, LCD_PCLK, or similar
// ------------------------------------------------------------------------------------------------

//	----------------------------------- Description of the naming and defining:
//		Readable name:						(32* Port.Number) + Pin.Number			// Comment


// DEBUG/FLASH
#define sig_SWO							Sig_Array[(32* 0) + 10]						// P0.10 - SWO
#define sig_SWCLK						Sig_Array[(32* 0) + 11]						// P0.11 - SWCLK
#define sig_SWDIO						Sig_Array[(32* 0) + 12]						// P0.12 - SWDIO

// Buttons
#define	sig_PB_ISP0						Sig_Array[(32* 0) + 4]						// P0_4-ISP0, Button, SW2, ISP0
#define	sig_PB_ISP1						Sig_Array[(32* 0) + 5]						// P0_5-ISP1, Button, SW3, ISP1
#define	sig_PB_ISP2						Sig_Array[(32* 0) + 6]						// P0_6-ISP2, Button, SW4, ISP2
#define	sig_PB_USER						Sig_Array[(32* 1) + 1]						// P1_1-USER_PB-USB1_OVRCURn, Button, SW5, USER

// LED
#define	sig_LED_1						Sig_Array[(32* 3) + 14]						// P3_14-CT3MAT1-USR_LED1, LED1, D12
#define	sig_LED_2						Sig_Array[(32* 3) + 3]						// P3_3-USR_LED2, LED2, D11
#define	sig_LED_3						Sig_Array[(32* 2) + 2]						// P2_2-CT1MAT1-USR_LED3, LED3, D9

// Accelerometer, MMA8652FCR1
#define sig_ACC_INTR					Sig_Array[(32* 3) + 4]						// P3_4-ACCL_INTR, U19, INT out

// I2C is commonly used by more devices (Accelerometer, Displat Capacitive Touch Pad, Audio Codes)
#define	sig_I2C_SCL						Sig_Array[(32* 3) + 24]						// P3_24-FC2_SCLX, U19, I2C SCL
#define	sig_I2C_SDA						Sig_Array[(32* 3) + 23]						// P3_23-FC2_SDAX, U19, I2C SDA

// NOR FLASH QuadSPI (MX25R6435FM2IL0/W25Q128JVFIM)
#define sig_SPIFI_CSn					Sig_Array[(32* 0) + 23]						// P0_23-SPIFI_CSn-MCLK, U27/U26A1, MX25R6435FM2IL0/W25Q128JVFIM, Pin nCS
#define sig_SPIFI_CLK					Sig_Array[(32* 0) + 26]						// P0_26-SPIFI_CLK, U27/U26A1, MX25R6435FM2IL0/W25Q128JVFIM, Pin SCK
#define sig_SPIFI_IO0					Sig_Array[(32* 0) + 24]						// P0_24-SPIFI_IO0, U27/U26A1, MX25R6435FM2IL0/W25Q128JVFIM, Pin IO0
#define sig_SPIFI_RSTn					Sig_Array[(32* 2) + 12]						// P2_12-SPIFI_RSTn, U26A1, W25Q128JVFIM, Pin nRST
#define sig_SPIFI_IO3					Sig_Array[(32* 0) + 27]						// P0_27-SPIFI_IO3, U27/U26A1, MX25R6435FM2IL0/W25Q128JVFIM, Pin IO3
#define sig_SPIFI_IO2					Sig_Array[(32* 0) + 28]						// P0_28-SPIFI_IO2-USB0_OCURRn, U27/U26A1, MX25R6435FM2IL0/W25Q128JVFIM, Pin IO2
#define sig_SPIFI_IO1					Sig_Array[(32* 0) + 25]						// P0_25-SPIFI_IO1, U27/U26A1, MX25R6435FM2IL0/W25Q128JVFIM, Pin IO1

// SD Card Slot
#define sig_SDC_PWEN					Sig_Array[(32* 2) + 5]						// P2_5-SD_POW_EN, SDCard Slot, Power Enable (Indicated by LED D7)
#define sig_SDC_WPn						Sig_Array[(32* 3) + 15]						// P3_15-SD_WPn, SDCard Slot, Pin WPn
#define sig_SDC_CDn						Sig_Array[(32* 2) + 10]						// P2_10-SD_CDn, SDCard Slot, Pin CDn
#define sig_SDC_D2						Sig_Array[(32* 2) + 8]						// P2_8-SD_D2, SDCard Slot, Pin D2
#define sig_SDC_D1						Sig_Array[(32* 2) + 7]						// P2_7-SD_D1, SDCard Slot, Pin D1
#define sig_SDC_D0						Sig_Array[(32* 2) + 6]						// P2_6-SD_D0, SDCard Slot, Pin D0
#define sig_SDC_CLK						Sig_Array[(32* 2) + 3]						// P2_3-SD_CLK, SDCard Slot, Pin CLK
#define sig_SDC_CMD						Sig_Array[(32* 2) + 4]						// P2_4-SD_CMD, SDCard Slot, Pin CMD
#define sig_SDC_D3						Sig_Array[(32* 2) + 9]						// P2_9-SD_D3, SDCard Slot, Pin D3

// LCD Display (RK043FN02H-CT) Connection
#define sig_LCD_BRIGHT					Sig_Array[(32* 3) + 31]						// P3_31-SCT0_OUT5_CT4MAT2, LCD, Backlight PWM
#define sig_LCD_RED3					Sig_Array[(32* 2) + 21]						// P2_21-LCD_VD3, LCD, Pin Red 3
#define sig_LCD_RED4					Sig_Array[(32* 2) + 22]						// P2_22-LCD_VD4, LCD, Pin Red 4
#define sig_LCD_RED5					Sig_Array[(32* 2) + 23]						// P2_23-LCD_VD5, LCD, Pin Red 5
#define sig_LCD_RED6					Sig_Array[(32* 2) + 24]						// P2_24-LCD_VD6, LCD, Pin Red 6
#define sig_LCD_RED7					Sig_Array[(32* 2) + 25]						// P2_25-LCD_VD7, LCD, Pin Red 7
#define sig_LCD_GRN2					Sig_Array[(32* 2) + 28]						// P2_28-LCD_VD10, LCD, Pin Green 2
#define sig_LCD_GRN3					Sig_Array[(32* 2) + 29]						// P2_29-LCD_VD11, LCD, Pin Green 3
#define sig_LCD_GRN4					Sig_Array[(32* 2) + 30]						// P2_30-LCD_VD12, LCD, Pin Green 4
#define sig_LCD_GRN5					Sig_Array[(32* 2) + 31]						// P2_31-LCD_VD13, LCD, Pin Green 5
#define sig_LCD_GRN6					Sig_Array[(32* 3) + 0]						// P3_0-LCD_VD14, LCD, Pin Green 6
#define sig_LCD_GRN7					Sig_Array[(32* 3) + 1]						// P3_1-LCD_VD15, LCD, Pin Green 7
#define sig_LCD_BLU3					Sig_Array[(32* 3) + 5]						// P3_5-LCD_VD19, LCD, Pin Blue 3
#define sig_LCD_BLU4					Sig_Array[(32* 3) + 6]						// P3_6-LCD_VD20, LCD, Pin Blue 4
#define sig_LCD_BLU5					Sig_Array[(32* 3) + 7]						// P3_7-LCD_VD21, LCD, Pin Blue 5
#define sig_LCD_BLU6					Sig_Array[(32* 3) + 8]						// P3_8-LCD_VD22, LCD, Pin Blue 6
#define sig_LCD_BLU7					Sig_Array[(32* 3) + 9]						// P3_9-LCD_VD23, LCD, Pin Blue 7
#define sig_LCD_PCLK					Sig_Array[(32* 2) + 13]						// P2_13-LCD_DCLK, LCD, Pin CLK (PixelClock)
#define sig_LCD_DISP					Sig_Array[(32* 2) + 11]						// P2_11-LCD_PWR, LCD, Pin DISP
#define sig_LCD_HSYNC					Sig_Array[(32* 2) + 16]						// P2_16-LCD_LP, LCD, Pin HSync
#define sig_LCD_VSYNC					Sig_Array[(32* 2) + 14]						// P2_14-LCD_FP, LCD, Pin VSync
#define sig_LCD_DE						Sig_Array[(32* 2) + 15]						// P2_15-LCD_AC_ENAB_M, LCD, Pin DE

// LCD Display Touch Pad (RK043FN02H-CT) Connection (I2C signals - same as Accelerometer)
#define sig_LCD_TS_RST					Sig_Array[(32* 2) + 27]						// P2_27-CT_RSTn, LCD Touch Screen, Pin RST
#define sig_LCD_TS_INTR					Sig_Array[(32* 4) + 0]						// P4_0-CT_INTR, LCD Touch Screen, Pin INT

// Ethernet PHY (LAN8720A-CP)
#define sig_ETH_TXD0					Sig_Array[(32* 4) + 8]						// P4_8-ENET_TXD0, ETH, Pin TXD0
#define sig_ETH_TXD1					Sig_Array[(32* 0) + 17]						// P0_17-ENET_TXD1, ETH, Pin TXD1
#define sig_ETH_TXEN					Sig_Array[(32* 4) + 13]						// P4_13-ENET_TX_EN, ETH, Pin TXEN
#define sig_ETH_RXD0					Sig_Array[(32* 4) + 11]						// P4_11-ENET_RXD0, ETH, Pin RXD0
#define sig_ETH_RXD1					Sig_Array[(32* 4) + 12]						// P4_12-ENET_RXD1, ETH, Pin RXD1
#define sig_ETH_CRSDV					Sig_Array[(32* 4) + 10]						// P4_10-ENET_CRSDV, ETH, Pin CRS_DV
#define sig_ETH_MDIO					Sig_Array[(32* 4) + 16]						// P4_16-ENET_MDIO, ETH, Pin MDIO
#define sig_ETH_MDC						Sig_Array[(32* 4) + 15]						// P4_15-ENET_MDC, ETH, Pin MDC
#define sig_ETH_RSTn					Sig_Array[(32* 2) + 26]						// P2_26-ENET_PHY_RSTn, ETH, Pin RSTn
#define sig_ETH_RXCLK					Sig_Array[(32* 4) + 14]						// P4_14-ENET_RX_CLK, ETH, Pin REFCLKO

// Audio Codec (WM8904CGEFL/V) Connection (I2C signals - same as Accelerometer)
#define sig_ETH_I2S_TX_SCK				Sig_Array[(32* 4) + 1]						// P4_1-FC6_I2S_TX_SCK, AudioCodec, Pin BCLK
#define sig_ETH_I2S_TX_WS				Sig_Array[(32* 4) + 3]						// P4_3-FC6_I2S_TX_WS, AudioCodec, Pin WS
#define sig_ETH_I2S_TX_DAT				Sig_Array[(32* 4) + 2]						// P4_2-FC6_I2S_TX_DATA, AudioCodec, Pin DAO
#define sig_ETH_I2S_RX_DAT				Sig_Array[(32* 2) + 19]						// P2_19-FC7_I2S_RX_DAT, AudioCodec, Pin DAI
#define sig_ETH_I2S_RX_SCK				Sig_Array[(32* 2) + 18]						// P2_18-FC7_I2S_RX_SCK, AudioCodec, Pin BCLK
#define sig_ETH_I2S_RX_WS				Sig_Array[(32* 2) + 20]						// P2_20-FC7_I2S_RX_WS, AudioCodec, Pin WS
#define sig_ETH_I2S_MCLK				Sig_Array[(32* 3) + 11]						// P3_11-MCLK-PMOD2_GPIO, AudioCodec, Pin MCLK
#define sig_ETH_I2S_CLKOUT				Sig_Array[(32* 3) + 12]						// P3_12-CLKOUT, AudioCodec, Pin ALT_MCLKOUT

// DMIC Digital Microphone
#define sig_ETH_DMIC_CLK				Sig_Array[(32* 1) + 2]						// P1_2-PDM1_CLK, Digital Mic, Pin CLK
#define sig_ETH_DMIC_DAT				Sig_Array[(32* 1) + 3]						// P1_3-PDM1_DATA, Digital Mic, Pin DATA

// UART-FC0
#define sig_UART0_RX					Sig_Array[(32* 0) + 29]						// P0_29-UART - FC 0 - Rx
#define sig_UART0_TX					Sig_Array[(32* 0) + 30]						// P0_30-UART - FC 0 - Tx

// UART-FC3
#define sig_SPI3_SCK					Sig_Array[(32* 0) + 0]						// P0_0-UART - FC 3 - SCK
#define sig_SPI3_SSEL					Sig_Array[(32* 0) + 1]						// P0_1-UART - FC 3 - SSEL
#define sig_SPI3_MISO					Sig_Array[(32* 0) + 2]						// P0_2-UART - FC 3 - MISO
#define sig_SPI3_MOSI					Sig_Array[(32* 0) + 3]						// P0_3-UART - FC 3 - MOSI

//#define	sig_virtual						Sig_Array[(sizeof(Sig_Array)/sizeof(Sig_Array[0]))-2]
#define	sig_virtual						Sig_Array[(32*4)+17]



// Preset Value for signals - config and value will be set after reset:
						// Port; Pin; Active; {Direction; IOCON reg				 			 		}   - for MCU without SWM Block!
						// Port; Pin; Active; {Direction; IOCON reg (16 bit); SWM reg (2x8 bits)	} 	- for MCU with SWM Block!

#define _SP0_0_FILL			{ 0,  0, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// Active in 0, Used as ISP_FC3_SCK
#define _SP0_1_FILL			{ 0,  1, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// Active in 0, Used as ISP_FC3_SSEL0
#define _SP0_2_FILL			{ 0,  2, 0, { CHAL_IO_Input, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// Active in 0, Used as ISP_FC3_MISO
#define _SP0_3_FILL			{ 0,  3, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// Active in 0, Used as ISP_FC3_MOSI
#define _SP0_4_FILL			{ 0,  4, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_5_FILL			{ 0,  5, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_6_FILL			{ 0,  6, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_7_FILL			{ 0,  7, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_8_FILL			{ 0,  8, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_9_FILL			{ 0,  9, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_10_FILL		{ 0xff, 0, 0, { CHAL_IO_Output, IOCON_PIO_DIGIMODE(1) | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_11_FILL		{ 0xff, 0, 0, { CHAL_IO_Output, IOCON_PIO_DIGIMODE(1) | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_12_FILL		{ 0xff, 0, 0, { CHAL_IO_Output, IOCON_PIO_DIGIMODE(1) | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_13_FILL		{ 0,  13, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_14_FILL		{ 0,  14, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_15_FILL		{ 0,  15, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_16_FILL		{ 0,  16, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_17_FILL		{ 0,  17, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_18_FILL		{ 0,  18, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_19_FILL		{ 0,  19, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_20_FILL		{ 0,  20, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_21_FILL		{ 0,  21, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_22_FILL		{ 0,  22, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_23_FILL		{ 0,  23, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_24_FILL		{ 0,  24, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_25_FILL		{ 0,  25, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_26_FILL		{ 0,  26, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_27_FILL		{ 0,  27, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_28_FILL		{ 0,  28, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_29_FILL		{ 0,  29, 0, { CHAL_IO_Input, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// Active in 0, Used as FC0 RXD
#define _SP0_30_FILL		{ 0,  30, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// Active in 0, Used as FC0 TXD
#define _SP0_31_FILL		{ 0,  31, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO

#define _SP1_0_FILL			{ 1,  0, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_1_FILL			{ 1,  1, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_2_FILL			{ 1,  2, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_3_FILL			{ 1,  3, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_4_FILL			{ 1,  4, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_5_FILL			{ 1,  5, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_6_FILL			{ 1,  6, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_7_FILL			{ 1,  7, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_8_FILL			{ 1,  8, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_9_FILL			{ 1,  9, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_10_FILL		{ 1,  10, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_11_FILL		{ 1,  11, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_12_FILL		{ 1,  12, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_13_FILL		{ 1,  13, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_14_FILL		{ 1,  14, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_15_FILL		{ 1,  15, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_16_FILL		{ 1,  16, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_17_FILL		{ 1,  17, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_18_FILL		{ 1,  18, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_19_FILL		{ 1,  19, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_20_FILL		{ 1,  20, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_21_FILL		{ 1,  21, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_22_FILL		{ 1,  22, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_23_FILL		{ 1,  23, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_24_FILL		{ 1,  24, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_25_FILL		{ 1,  25, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_26_FILL		{ 1,  26, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_27_FILL		{ 1,  27, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_28_FILL		{ 1,  28, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_29_FILL		{ 1,  29, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_30_FILL		{ 1,  30, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_31_FILL		{ 1,  31, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO

#define _SP2_0_FILL			{ 2,  0, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_1_FILL			{ 2,  1, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_2_FILL			{ 2,  2, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_3_FILL			{ 2,  3, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_4_FILL			{ 2,  4, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_5_FILL			{ 2,  5, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_6_FILL			{ 2,  6, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_7_FILL			{ 2,  7, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_8_FILL			{ 2,  8, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_9_FILL			{ 2,  9, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_10_FILL		{ 2,  10, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_11_FILL		{ 2,  11, 0, { CHAL_IO_Input, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P2_11-LCD_PWR, LCD, Pin DISP
#define _SP2_12_FILL		{ 2,  12, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_13_FILL		{ 2,  13, 0, { CHAL_IO_Input, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P2_13-LCD_DCLK, LCD, Pin CLK (PixelClock)
#define _SP2_14_FILL		{ 2,  14, 0, { CHAL_IO_Input, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P2_14-LCD_FP, LCD, Pin VSync
#define _SP2_15_FILL		{ 2,  15, 0, { CHAL_IO_Input, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P2_15-LCD_AC_ENAB_M, LCD, Pin DE
#define _SP2_16_FILL		{ 2,  16, 0, { CHAL_IO_Input, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P2_16-LCD_LP, LCD, Pin HSync
#define _SP2_17_FILL		{ 2,  17, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}				// Active in 0, Used as GPIO
#define _SP2_18_FILL		{ 2,  18, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}				// Active in 0, Used as GPIO
#define _SP2_19_FILL		{ 2,  19, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}				// Active in 0, Used as GPIO
#define _SP2_20_FILL		{ 2,  20, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}				// Active in 0, Used as GPIO
#define _SP2_21_FILL		{ 2,  21, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P2_21-LCD_VD3, LCD, Pin Red 3
#define _SP2_22_FILL		{ 2,  22, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P2_22-LCD_VD4, LCD, Pin Red 4
#define _SP2_23_FILL		{ 2,  23, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P2_23-LCD_VD5, LCD, Pin Red 5
#define _SP2_24_FILL		{ 2,  24, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P2_24-LCD_VD6, LCD, Pin Red 6
#define _SP2_25_FILL		{ 2,  25, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P2_25-LCD_VD7, LCD, Pin Red 7
#define _SP2_26_FILL		{ 2,  26, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_27_FILL		{ 2,  27, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_28_FILL		{ 2,  28, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P2_28-LCD_VD10, LCD, Pin Green 2
#define _SP2_29_FILL		{ 2,  29, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P2_29-LCD_VD11, LCD, Pin Green 3
#define _SP2_30_FILL		{ 2,  30, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P2_30-LCD_VD12, LCD, Pin Green 4
#define _SP2_31_FILL		{ 2,  31, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P2_31-LCD_VD13, LCD, Pin Green 5

#define _SP3_0_FILL			{ 3,  0, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P3_0-LCD_VD14, LCD, Pin Green 6
#define _SP3_1_FILL			{ 3,  1, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P3_1-LCD_VD15, LCD, Pin Green 7
#define _SP3_2_FILL			{ 3,  2, 0, { CHAL_IO_Output, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO for LED
#define _SP3_3_FILL			{ 3,  3, 0, { CHAL_IO_Output, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO for LED
#define _SP3_4_FILL			{ 3,  4, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_5_FILL			{ 3,  5, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P3_5-LCD_VD19, LCD, Pin Blue 3
#define _SP3_6_FILL			{ 3,  6, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P3_6-LCD_VD20, LCD, Pin Blue 4
#define _SP3_7_FILL			{ 3,  7, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P3_7-LCD_VD21, LCD, Pin Blue 5
#define _SP3_8_FILL			{ 3,  8, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P3_8-LCD_VD22, LCD, Pin Blue 6
#define _SP3_9_FILL			{ 3,  9, 0, { CHAL_IO_Output, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// P3_9-LCD_VD23, LCD, Pin Blue 7
#define _SP3_10_FILL		{ 3,  10, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_11_FILL		{ 3,  11, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_12_FILL		{ 3,  12, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_13_FILL		{ 3,  13, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_14_FILL		{ 3,  14, 0, { CHAL_IO_Output, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO for LED
#define _SP3_15_FILL		{ 3,  15, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_16_FILL		{ 3,  16, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_17_FILL		{ 3,  17, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_18_FILL		{ 3,  18, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_19_FILL		{ 3,  19, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_20_FILL		{ 3,  20, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_21_FILL		{ 3,  21, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_22_FILL		{ 3,  22, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_23_FILL		{ 3,  23, 0, { CHAL_IO_Input, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// Active in 0, Used as GPIO
#define _SP3_24_FILL		{ 3,  24, 0, { CHAL_IO_Input, 	IOCON_PIO_DIGIMODE(1) | PFUN_ALT(1)}}	// Active in 0, Used as GPIO
#define _SP3_25_FILL		{ 3,  25, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_26_FILL		{ 3,  26, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_27_FILL		{ 3,  27, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_28_FILL		{ 3,  28, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_29_FILL		{ 3,  29, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_30_FILL		{ 3,  30, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
//#define _SP3_31_FILL		{ 3,  31, 0, { CHAL_IO_Output, IOCON_PIO_DIGIMODE(1) | PFUN_ALT(2)}}	// P3_31-SCT0_OUT5_CT4MAT2, LCD, Backlight PWM
#define _SP3_31_FILL		{ 3,  31, 0, { CHAL_IO_Output,  0x0000 | PFUN_GPIO}}	// P3_31-SCT0_OUT5_CT4MAT2, LCD, Backlight PWM

#define _SP4_0_FILL			{ 4,  0, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP4_1_FILL			{ 4,  1, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP4_2_FILL			{ 4,  2, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP4_3_FILL			{ 4,  3, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP4_4_FILL			{ 4,  4, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP4_5_FILL			{ 4,  5, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP4_6_FILL			{ 4,  6, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP4_7_FILL			{ 4,  7, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP4_8_FILL			{ 4,  8, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP4_9_FILL			{ 4,  9, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP4_10_FILL		{ 4,  10, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP4_11_FILL		{ 4,  11, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP4_12_FILL		{ 4,  12, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP4_13_FILL		{ 4,  13, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP4_14_FILL		{ 4,  14, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP4_15_FILL		{ 4,  15, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP4_16_FILL		{ 4,  16, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO


// And declare as extern
extern const CHAL_Signal_t 				Sig_Array[];				// array of the used signals
extern const CHAL_Signal_t				*sig_last;

#endif		//__BSP_NXP_OM13098_SIGNALDEF_H_
