// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: bsp_NXP_OM13098_Config.h
// 	   Version: 1.01
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Board Support Package - Configuration header file example
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
// 

#ifndef __BSP_NXP_OM13098_CONFIG_H_
#define __BSP_NXP_OM13098_CONFIG_H_

// ************************************************************************************************
// Configuration - Chip MCU:
// ************************************************************************************************

#define BSP_TYPE_STRING					"OM13098"		      						// Hardware board name - string
#define BSP_VERSION_MAJOR				1											// board version major - integer
#define BSP_VERSION_MINOR				01											// board version minor - integer

//	Select manufacturer and Chip codename:
//#define	CONF_CHIP_SERIES_LPC		1											// MCU Manufacturer serie
#define	CONF_CHIP_ID_LPC54628J512ET180	1											// MCU definition

// Link with Chip.h settings	- don't touch!
#define CONF_CHIP_OSCRATEIN				BSP_EXQUARTZFREQ
#define CONF_CHIP_EXTRATEIN				BSP_EXRATEIN
#define CONF_CHIP_RTCRATEIN				BSP_RTCQUARTZFREQ
#define CONF_CHIP_LCDRATEIN				BSP_LCDCLKRATEIN							// external LCD CLK input

// If hardware allow SWD, set it here:
#define BSP_ALLOW_SWO					0
#define	BSP_DEBUG_SWD					1											// Serial Wire Debug Interface. zablokujes ULINK debugger !!
#define	BSP_DEBUG_SWO					1											// use SWO output also

// Used clock rate:
#define	BSP_EXQUARTZFREQ				(12000000)									// external crystal frequency. If not used, set to zero.
#define BSP_RTCQUARTZFREQ				(0)											// external RTC crystal frequency. If not used, set to zero.
#define	BSP_EXRATEIN					(0)											// external signal frequency. If not used, set to zero
#define BSP_LCDCLKRATEIN				(0)											// external LCD Clock source not connected


// ************************************************************************************************
// Private declaration - for compatibility with other systems - 'don't change it
// ************************************************************************************************
#define BSP_VERSION_STRING				STR(BSP_VERSION_MAJOR) "." STR(BSP_VERSION_MINOR)	// concatenate integer values into string


// -----------------------------------------------------------------------------------------------
// Hardware configuration
// -----------------------------------------------------------------------------------------------


// 					Main MCU: LPC54608FET180
// ExRAM		  																	// W9812G6JB-6I, SDRAM 128Mb 3.3v, B4 pkg, 167Mhz

// ExFLASH																			// MX25L12835FM2I-10G, 128Mb serial FLASH Quad SPI, 8p SOIC 0.209", 3.3V

// ExFLASH																			// W25Q128JVFIM, Quad SPI Flash; Reset, Hold, SOIC16 0.30" wide
// 																						- alt.: MT25QL128ABA8ESF-0SIT

// LCD																				// RK043FN02H-CT, 4.3 inch TFT 480*272 pixels with LED backlight and capacitive touch panel
#define BSP_LCD_WIDTH					480											// X resolution
#define	BSP_LCD_HEIGHT					272											// Y resolution
#define	BSP_LCD_BPP						2											// Bits Per Pixel

#define BSP_LCDTS_I2C_ADR			 	(0x38<<1)									//0b0111000

// U19, Accelerometer 																// MMA8652FCR1, 3-axis, 12-bit
#define BSP_ACCEL_I2C_ADR			 	(0x1d<<1)									//0b0011101

// Current sensor																	// MAX9634TEUK+T
// ETH PHY																			// LAN8720A-CP, 10/100 1Port Ethernet RMII PHY 24p QFN-E
#define BSP_ETH_PHY_ADR			 		0x00										// Address

// AUDIO Codec																		// WM8904CGEFL/V, ST, I2S, I2C, 33p QFN 
#define BSP_AUDIO_I2C_ADR			 	(0x1a<<1)									//0b0011010

// LED Boost converter																// AP5724WG-7

// UART Communication - bridge over DAP CDC
#define	BSP_UART_ECOMM					0

#endif 		// __BSP_NXP_OM13098_CONFIG_H_
