// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: bsp_NXP_OM13098.h
// 	   Version: 3.0
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Board support package file
//							hardware specific settings and declaration
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
// 

#ifndef __BSP_NXP_OM13098_H_
#define __BSP_NXP_OM13098_H_

#include "bsp_NXP_OM13098_Config.h"													// load chip/board configuration settings

#ifndef LOAD_SCATTER

#define	CHIPDIRFILE 		<CONCAT(CONF_SKELETON_DIR_REL,/Chip/Chip.h)>			// Chip functions and declarations of peripherials for selected chip
#include	CHIPDIRFILE

// ************************************************************************************************
// Export
// ************************************************************************************************
extern void bsp_Init(void);															// Board Hardware initialization
extern void bsp_Chip_Init(void);													// MCU Chip init based on board HW
extern void bsp_Chip_Set_SystemClocking(void);

extern void bsp_Debug_Prepare(void);												// Debug init if needed
//extern void bsp_CONF_DEBUG_ITM_Init(void);										// Debug init if needed
extern void bsp_USB_Wr_Clocking(void);
extern void bsp_Set_SystemPinMux(void);
extern void bsp_Set_SystemPinDefault(void);

// ************************************************************************************************
// Connection
// ************************************************************************************************

// here you can declare hardware connection. Ex. LED_Status_Port	as P1.0, ....
#include 	"bsp_NXP_OM13098_SignalDef.h"
#endif		// LOAD_SCATTER
#endif 		// __BSP_NXP_OM13098_H_
