// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: bsp_EA_LPCXPRESSO_LPC1114_Config.h
// 	   Version: 1.00
//  Created on: 01.05.2023
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Board Support Package - Configuration header file for EA_LPCXPRESSO_LPC1114
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
// 

#ifndef __BSP_EA_LPCXPRESSO_LPC1114_CONFIG_H_
#define __BSP_EA_LPCXPRESSO_LPC1114_CONFIG_H_

// ************************************************************************************************
// Configuration - Chip MCU:
// ************************************************************************************************

#define BSP_TYPE_STRING					"LPCXPRESSO_LPC1114"		      			// Hardware board name - string
#define BSP_VERSION_MAJOR				1											// board version major - integer
#define BSP_VERSION_MINOR				00											// board version minor - integer

//	Select manufacturer and Chip codename:
#define	CONF_CHIP_ID_LPC1114FBD48_301	1											// MCU definition

// Link with Chip.h settings	- don't touch!
#define CONF_CHIP_OSCRATEIN				BSP_EXQUARTZFREQ
#define CONF_CHIP_EXTRATEIN				BSP_EXRATEIN
#define CONF_CHIP_RTCRATEIN				BSP_RTCQUARTZFREQ

// If hardware allow SWD, set it here:
#define	BSP_DEBUG_SWD					1											// Serial Wire Debug Interface. zablokujes ULINK debugger !!
#define	BSP_DEBUG_SWO					1											// use SWO output also


// Used clock rate:
#define	BSP_EXQUARTZFREQ				(12000000)									// external crystal frequency. If not used, set to zero.
#define BSP_RTCQUARTZFREQ				(0)											// external RTC crystal frequency. If not used, set to zero.
#define	BSP_EXRATEIN					(0)											// external signal frequency. If not used, set to zero

#define BSP_UART_UPCLK					Core.MainClock								// perihery clock for UART


// Chip Clock settings
#define BSP_FREQ_PLL_M_VAL				5											// 5 PLL multiplier value (not bit value but real)
#define	BSP_FREQ_PLL_P_VAL				1											// 1 PLL divider value (not bit value but real)
#define	BSP_SYSAHBCLKDIV				1											// 1 AHB divider



// ************************************************************************************************
// Private declaration - for compatibility with other systems - 'don't change it
// ************************************************************************************************
#define BSP_VERSION_STRING				STR(BSP_VERSION_MAJOR) "." STR(BSP_VERSION_MINOR)	// concatenate integer values into string

#endif 		// __BSP_EA_LPCXPRESSO_LPC1114_CONFIG_H_
