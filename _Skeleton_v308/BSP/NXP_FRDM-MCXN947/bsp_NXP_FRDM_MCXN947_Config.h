// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: bsp_NXP_FRDM_MCXN947_Config.h
// 	   Version: 1.00
//        Date: 2024.09.09
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Board Support Package - Configuration header file
// ******************************************************************************
// Info: 
//
// Usage:
// 
// ToDo:
// 
// Changelog:
// 

#ifndef __BSP_NXP_FRDM_MCXN947_CONFIG_H_
#define __BSP_NXP_FRDM_MCXN947_CONFIG_H_

// ************************************************************************************************
// Configuration - Chip MCU:
// ************************************************************************************************
#define BSP_TYPE_STRING					"NXP_FRDM_MCXN947"      					// Hardware board name - string
#define BSP_VERSION_MAJOR				1											// board version major - integer
#define BSP_VERSION_MINOR				00											// board version minor - integer

// Link with Chip.h settings	- don't touch!
#define CONF_CHIP_OSCRATEIN				BSP_EXQUARTZFREQ
#define CONF_CHIP_EXTRATEIN				BSP_EXRATEIN
#define CONF_CHIP_RTCRATEIN				BSP_RTCQUARTZFREQ
#define	CONF_CHIP_FRORATE				30000000									// 18 MHz. (18M/24M/30M)

// If hardware allow SWD, set it here:
#define	BSP_DEBUG_SWD					1											// Serial Wire Debug Interface. zablokujes ULINK debugger !!

// Used clock rate:
#define	BSP_EXQUARTZFREQ				(0)											// external crystal frequency. If not used, set to zero.
#define BSP_RTCQUARTZFREQ				(0)											// external RTC crystal frequency. If not used, set to zero.
#define	BSP_EXRATEIN					(0)											// external signal frequency. If not used, set to zero

//#define BSP_UART_UPCLK					Core.MainClock								// perihery clock for UART

// Chip Clock settings		FREQ = OSC * [M+1] / [2* P+1]
#define BSP_FREQ_PLL_M_VAL				1											// 1 PLL multiplier value (not bit value but real) 0,1,2,3...31 = 1,2..32
#define	BSP_FREQ_PLL_P_VAL				0											// 1 PLL divider value (not bit value but real): 0,1,2,3 = 1,2,4,8
#define	BSP_SYSAHBCLKDIV				1											// 1 AHB divider 1,2,..255 = 1,2,...255



// ************************************************************************************************
// Configuration depends on hardware assembly
// ************************************************************************************************
//	Select manufacturer and Chip codename - select same also in Keil Options dialog !!!!
#define	CONF_CHIP_ID_MCXN947VDFT



// uncomment appropriate define - which resistor are physsically assembled:
#define	BSP_CONF_ASSEMBLED_R5

// RS485 Connect to Uart number:
#define BSP_RS485_UART					1

// Display + DIP SITCH + LED STATUS
#define BSP_I2C_ADR_IC4					0x0A
#define BSP_I2C_ADR_IC5					0x2A
#define BSP_I2C_ADR_IC6					0x32
#define BSP_I2C_ADR_ALL					0xEC											// I2C address for ALL PCA9955B chips

// IC4, IC5, IC6 - PCA9955 bit coding:
//  Segment:	A	B	C	D	E	F	G	DP
//		Bit:	2	0	3	5	7	4	6	1
//				10	8	11	13	15	12	14	9
//
//	DIP Switch:	A	B	C	D
//	IC4-Bit:	9	10	11	12
//
//	LED Status
//	IC4-Bit:	8


// ************************************************************************************************
// Private declaration - for compatibility with other systems - 'don't change it
// ************************************************************************************************
#define BSP_VERSION_STRING				STR(BSP_VERSION_MAJOR) "." STR(BSP_VERSION_MINOR)	// concatenate integer values into string

#endif 		// __BSP_NXP_FRDM_MCXN947_CONFIG_H_
