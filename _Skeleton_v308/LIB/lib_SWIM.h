// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: lib_SWIM.h
// 	   Version: 3.0
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: SWIM graphic library - header file
// ******************************************************************************
// Notice:
//
// Usage:
//			
// ToDo:
//
// Changelog:
//				2024.06.25 - v3.1	- add ATTR_ALIGNED(8) to tructure definition - Cortex-M0 aligned access
//

#ifndef __LIB_SWIM_H_
#define __LIB_SWIM_H_

#include "Skeleton.h"

#include "Swim\Include\lpc_swim_font.h"
#include "Swim\Include\lpc_swim.h"
//#include "Swim\Include\lpc_rom8x8.h"
#include "Swim\Include\lpc_rom8x16.h"
//#include "Swim\Include\lpc_winfreesystem14x16.h"
//#include "Swim\Include\winFidonetFGHITerminal10x188x18.h"
#include "Swim\Include\lpc_x5x7.h"
//#include "Swim\Include\lpc_x6x13.h"

// ******************************************************************************************************
// DEFAULT CONFIGURATION
// ******************************************************************************************************
#ifndef CONF_DEBUG_LIB_SWIM
 #define CONF_DEBUG_LIB_SWIM			0											// Default debug is off
#endif


// ******************************************************************************************************
// PUBLIC Defines
// ******************************************************************************************************

// ******************************************************************************************************
// PRIVATE Defines
// ******************************************************************************************************


// Library interface structure:
COMP_PACKED_BEGIN
ATTR_ALIGNED(8)																		// For Cortex-M0 access !!!!
typedef struct
{
#if defined(CONF_DEBUG_LIB_SWIM) && (CONF_DEBUG_LIB_SWIM == 1)
	const char				Name[CONF_DEBUG_STRING_NAME_SIZE];						// interface name - text - only for ID
#endif	
	struct 	_If_Status					Stat;										// status of the interface
			uint32_t					WidthPx;									// LCD Width in pixels
			uint32_t					HeightPx;									// LCD Height in pixels
	SWIM_WINDOW_T 						MainWindow;
	COLOR_T 							clr;
	COLOR_T 							*fblog;										// ptr to display framebuffer

} lib_SWIM_If_t;
COMP_PACKED_END




// SWIM window structure - created for each floating win.
COMP_PACKED_BEGIN
typedef struct
{
#if defined(CONF_DEBUG_LIB_SWIM) && (CONF_DEBUG_LIB_SWIM == 1)
	const char				Name[CONF_DEBUG_STRING_NAME_SIZE];						// win name - text - only for ID
#endif	
	SWIM_WINDOW_T 						ThisWin;									// win structure
	lib_SWIM_If_t 						*pRoot_If;									// ptr to root main win
} lib_SWIM_WIN_If_t;
COMP_PACKED_END






// ******************************************************************************************************
// PUBLIC 
// ******************************************************************************************************
extern 	lib_SWIM_If_t  lib_SWIM_If;													// SWIM Interface
extern 	lib_SWIM_WIN_If_t  lib_SWIM_WIN_If;											// SWIM WIN Interface
extern bool lib_SWIM_Init(lib_SWIM_If_t* pLib_If, uint8_t *LCD_FB, uint32_t LCD_H_Size, uint32_t LCD_V_Size, uint8_t LCD_BPP); // Reset and init SWIM
extern lib_SWIM_WIN_If_t *lib_SWIM_Create_Win(lib_SWIM_If_t* pLib_If, int32_t xwin_min, int32_t ywin_min, int32_t xwin_max, int32_t ywin_max, int32_t border_width, COLOR_T pcolor, COLOR_T bkcolor, COLOR_T fcolor);
extern void lib_SWIM_Print_XY(lib_SWIM_WIN_If_t *Win, int32_t x, int32_t y, char* msg,...);


	
#endif  //__LIB_SWIM_H_
