// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: lib_EKermit.c
// 	   Version: 1.0
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: Kermit protocol v1.7, based on http://www.kermitproject.org/ek.html
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
//

#include "Skeleton.h"

#if defined(CONF_USE_LIB_EKERMIT) && (CONF_USE_LIB_EKERMIT == 1)

#define	LIB_EKERMIT_OBJECTNAME		"lib_EKermit"

#include "EKermit\kermit.c"

/*
  Sample prototypes for i/o functions.
  The functions are defined in a platform-specific i/o module.
  The function names are unknown to the Kermit module.
  The names can be changed but not their calling conventions.
  The following prototypes are keyed to unixio.c.
*/
int devopen(char *);																// Communications device/path
int devsettings(char *);
int devrestore(void);
int devclose(void);
int pktmode(short);

//int readpkt(struct k_data *, UCHAR *, int); 										// Communications i/o functions
int readpkt(struct lib_EKermit_If *EK_If, UCHAR *p, int len);
//int tx_data(struct k_data *, UCHAR *, int);
int tx_data(struct lib_EKermit_If *EK_If, UCHAR *p, int n);

int inchk(struct lib_EKermit_If *EK_If);

int openfile(struct lib_EKermit_If *EK_If, UCHAR * s, int mode);					// File i/o functions
int writefile(struct lib_EKermit_If *EK_If, UCHAR * s, int n);
int readfile(struct lib_EKermit_If *EK_If);
int closefile(struct lib_EKermit_If *EK_If, UCHAR c, int mode);
ULONG fileinfo(struct lib_EKermit_If *EK_If, UCHAR * filename, UCHAR * buf, int buflen, short * type, short mode);

int mem_prepare(struct lib_EKermit_If *EK_If, UCHAR * s, int mode);
int readmem (struct lib_EKermit_If *EK_If);
int writemem(struct lib_EKermit_If *EK_If, UCHAR * s, int n);


#ifdef F_CRC
int check = 3;																		// Block check
#else
int check = 1;
#endif 

#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
int errorrate = 0;																	// Simulated error rate
int seed = 1234;																	// Random number generator seed
#endif						


// ------------------------------------------------------------------------------------------------------
// parameter je funkcia, ktora poskytuje data 
// vstupom su adresa zaciatku prenosu v pamati a tlzka v bytes
// CLI_send_file (pCLI_If, &lib_CLI_Read_ieeprom_data, StartAddr, Len)
// ------------------------------------------------------------------------------------------------------
bool kermit_send_file   (uint8_t *FileName, bool Remote, bool Binary, fn_read_tx_data_t fn_data_source, uint32_t StartAddress, size_t Length, _drv_If_t *InDrv, _drv_If_t *OutDrv)
{
	xArgs_t	xARGS;																	// create new arguments structure
	bool res = false;
	
	xARGS.TotalLength	= Length;													// save total transfer length
	xARGS.Binary 		= Binary;													// set data type for transfer
	xARGS.Action		= xSend;													// Action - send data
	xARGS.FileName		= FileName;													// save filename
	xARGS.WriteDrv		= InDrv;
	xARGS.ReadDrv		= OutDrv;
	xARGS.Remote		= Remote;													// remote access ?
	xARGS.StartAddress  = StartAddress;												// for memory read

//	tmp_Buff = sys_malloc(16);														// allocate memory for temporary buffer
//	if(tmp_Buff == NULL) return(false);												// allocation problem - exit

	xARGS.readf_buffrdcount = 0;													// premenna urcujuca kde prave citame
	xARGS.fn_blockread = fn_data_source;											// uloz citaciu fciu do argumentov

	res = kTransfer(&xARGS);														// Activate data dransfer
		
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// stiahnutie dat z PC a zapis do EEPROM
// od adresy StartAddress o dlzke Length
// ------------------------------------------------------------------------------------------------------
bool kermit_receive_file (uint8_t *FileName, bool Remote, bool Binary, fn_write_rx_data_t fn_data_drain, uint32_t StartAddress, size_t Length, _drv_If_t *InDrv, _drv_If_t *OutDrv)
{
	xArgs_t	xARGS;																	// create new arguments structure
	bool res = false;
	
	xARGS.TotalLength	= Length;													// save total transfer length
	xARGS.Binary 		= Binary;													// set data type for transfer
	xARGS.Action		= xRecv;													// Action - send data
	xARGS.FileName		= FileName;													// save filename
	xARGS.WriteDrv		= InDrv;
	xARGS.ReadDrv		= OutDrv;
	xARGS.Remote		= Remote;													// remote access ?
	xARGS.StartAddress  = StartAddress;												// for memory read

//	tmp_Buff = sys_malloc(16);														// allocate memory for temporary buffer
//	if(tmp_Buff == NULL) return(false);												// allocation problem - exit

	xARGS.readf_buffrdcount = 0;													// premenna urcujuca kde prave citame
	xARGS.fn_blockwrite = fn_data_drain;											// uloz citaciu fciu do argumentov

	res = kTransfer(&xARGS);														// Activate data dransfer
		
	return(res);
}


// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************
//// ------------------------------------------------------------------------------------------------------
////
//// ------------------------------------------------------------------------------------------------------
//static inline void doexit(void) 
//{
//	if(i_buf) sys_free(i_buf);
//	//if(o_buf) sys_free(o_buf);
//    devrestore();                       											// Restore device
//    devclose();                         											// Close device
//}

// ------------------------------------------------------------------------------------------------------
//  kTransfer - kermit transfer data
// 	action: 0-no action, A_SEND - send file(s), A_RECV - receive file(s)
//	parity: PAR_NONE, PAR_SPACE, PAR_EVEN, PAR_ODD, PAR_MARK
//	xmode: xfer mode: 0 = automatic, 1 = manual 	
//	ftype: BINARY, TEXT
//	remote: 1 = Remote, 0 = Local mode
//	keep: Keep incompletely received files
//	fn_data_xfer: ptr to function to read/write buffer or read/write file (Data source/drain)
//	FileName: File to send/receive
//	Return: true if transfer was succesfully done, otherwise false
// ------------------------------------------------------------------------------------------------------
bool kTransfer(xArgs_t *Arguments)
{
    int status, rx_len;
    UCHAR *inbuf;
    short r_slot;
	static lib_EKermit_If_t EK_If;
	UCHAR **cmlist[5];																// Create pointer to file list // max 4 record  !!!!
	//UCHAR *o_buf;																	// ptr to File output buffer
	//UCHAR *i_buf;																	// ptr to File input buffer
	UCHAR *x_buf;																	// ptr to File transfer i/o buffer
    status = X_OK;                      											// Initial kermit status

    if (!Arguments->Action) return false;											// Nothing to do
      
	// THE REAL STUFF IS FROM HERE DOWN

	
	////////////// tuto to SPADNE !!! na nasledujucom riadku.
	/// Pracuj vsade s EK_If a nie s k strukturou!
//    if (!devopen("dummy"))		/* Open the communication device */
//		{doexit(); return(FAILURE);}
//    if (!devsettings("dummy"))		/* Perform any needed settings */
//		{doexit(); return(FAILURE);}

#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
	dbgprint("\r\n("LIB_EKERMIT_OBJECTNAME") Initializing...");
#endif						
	cmlist[0] = (UCHAR **) Arguments->FileName;										// add filename to list
	cmlist[1] = NULL;																// clear next
	
//	o_buf = sys_malloc(OBUFLEN+8);													// allocate memory for buffer
//	if(o_buf == NULL) {doexit(); return(FAILURE);}									// allocation problem - exit

	EK_If.xArgs = *Arguments;														//  Fill in parameters for this run
    //k.xfermode = xmode;															// Text/binary automatic/manual
    //k.remote = remote;															// Remote vs local
    
	if(Arguments->Binary == true) EK_If.k.Binary = 1;								// binary mode
	else EK_If.k.Binary = 0;														// text mode
	
	
	
    EK_If.k.parity = PAR_NONE;//parity;                  							// Communications parity
    EK_If.k.bct = (check == 5) ? 3 : check;											// Block check type
    //k.ikeep = keep;																// Keep incompletely received files
    
	EK_If.k.filelist = (UCHAR **) &cmlist;											// List of files to send (if any)
    EK_If.k.cancel = 0;																// Not canceled yet

	/*  Fill in the i/o pointers  */
//	EK_If.k.rIdx = 0;
	if(Arguments->Action == xRecv)
	{
		x_buf = sys_malloc(OBUFLEN+8);												// allocate memory for buffer
		if(x_buf == NULL) goto ret_err;												// allocation problem - exit

		EK_If.k.obuf = x_buf;														// File output buffer
		EK_If.k.obuflen = OBUFLEN;													// File output buffer length
		EK_If.k.obufpos = 0;														// File output buffer position
	}
	else
	{
		x_buf = sys_malloc(IBUFLEN+8);												// allocate memory for buffer
		if(x_buf == NULL) goto ret_err;												// allocation problem - exit
		
		EK_If.k.zinbuf = x_buf;														// File input buffer
		EK_If.k.zinlen = IBUFLEN;													// File input buffer length
		EK_If.k.zincnt = 0;															// File input buffer position
	}
	

	/* Fill in function pointers */
    EK_If.k.rxd    = readpkt;														// for reading packets
    EK_If.k.txd    = tx_data;														// for sending packets
    EK_If.k.ixd    = inchk;															// for checking connection
    EK_If.k.openf  = mem_prepare;              										// for opening files/mem access
    EK_If.k.finfo  = fileinfo;                										// for getting file info
    //EK_If.k.readf  = readfile;													// for reading files/mem
	EK_If.k.readf  = readmem;														// for reading files/mem
    EK_If.k.writef = writemem;               										// for writing to output file
    EK_If.k.closef = closefile;               										// for closing files

	EK_If.k.bctf   = (check == 5) ? 1 : 0;											// Force Type 3 Block Check (16-bit CRC) on all packets, or not


	tx_data(( struct lib_EKermit_If *) &EK_If, (uint8_t *) "\r\nKERMIT transfer ready. Waiting for data 10 sec.\r\n", 56);


	// Initialize Kermit protocol
    status = kermit( &EK_If, K_INIT, 0, 0, "");


#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
	dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") Init Status: %d", status);
	dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") Version: %s", EK_If.k.version);
#endif	

    if (status == X_ERROR) goto ret_err;
    //if (Arguments->Action == A_SEND) status = kermit(&EK_If, K_SEND, &EK_If.k, 0, 0, "", &EK_If.r);
	if (Arguments->Action == A_SEND) status = kermit(&EK_If, K_SEND, 0, 0, "");

// Now we read a packet ourselves and call Kermit with it.  Normally, Kermit
// would read its own packets, but in the embedded context, the device must be
// free to do other things while waiting for a packet to arrive.  So the real
// control program might dispatch to other types of tasks, of which Kermit is
// only one.  But in order to read a packet into Kermit's internal buffer, we
// have to ask for a buffer address and slot number.
// To interrupt a transfer in progress, set k.cancel to I_FILE to interrupt
// only the current file, or to I_GROUP to cancel the current file and all
// remaining files.  To cancel the whole operation in such a way that the
// both Kermits return an error status, call Kermit with K_ERROR.
	  
    while (status != X_DONE) 
	{

// Here we block waiting for a packet to come in (unless readpkt times out).
// Another possibility would be to call inchk() to see if any bytes are waiting
// to be read, and if not, go do something else for a while, then come back
// here and check again.

		inbuf = getrslot( &EK_If, &r_slot);	/* Allocate a window slot */
		rx_len = EK_If.k.rxd( &EK_If, inbuf, P_PKTLEN); /* Try to read a packet */
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
		dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> main packet: %p, rx_len: %d", &(EK_If.k.ipktbuf[0][r_slot]),rx_len);
#endif	


// For simplicity, kermit() ACKs the packet immediately after verifying it was
// received correctly.  If, afterwards, the control program fails to handle the
// data correctly (e.g. can't open file, can't write data, can't close file),
// then it tells Kermit to send an Error packet next time through the loop.

		if (rx_len < 1) 																// No data was read
		{               
			freerslot( &EK_If, r_slot);														// So free the window slot
			if (rx_len < 0) goto ret_err;             					// If there was a fatal error -> give up
			/* This would be another place to dispatch to another task */
			/* while waiting for a Kermit packet to show up. */
		}
		
		/* Handle the input */
		switch (status = kermit( &EK_If, K_RUN, r_slot, rx_len, "")) 
		{
			case X_OK:
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
				/*
					This shows how, after each packet, you get the protocol state, file name,
					date, size, and bytes transferred so far.  These can be used in a
					file-transfer progress display, log, etc.
				*/
				dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> NAME: %s", EK_If.r.filename[0] ? (char *)EK_If.r.filename : "(NULL)");
				dbgprint(", DATE: %s", EK_If.r.filedate[0] ? (char *)EK_If.r.filedate : "(NULL)");
				dbgprint(", SIZE: %ld", EK_If.r.filesize);
				dbgprint(", STATE: %d", EK_If.r.status);
				dbgprint(", SOFAR: %ld", EK_If.r.sofar);	
#endif	
				/* Maybe do other brief tasks here... */
				continue;																	// Keep looping
		  case X_DONE:
				break;																		// Finished
		  case X_ERROR:
				goto ret_err;												// Failed
		}
    }
    
	return(true);

ret_err:
	if(x_buf) sys_free(x_buf);
	//if(o_buf) sys_free(o_buf);
    devrestore();                       /* Restore device */
    devclose();                         /* Close device */	
	tx_data(( struct lib_EKermit_If *) &EK_If, (uint8_t *) "\r\nKERMIT transfer closed.\r\n", 32);
	return(false);
}


// ------------------------------------------------------------------------------------------------------
//  D E V O P E N  --  Open communications device
// 		Call with: string pointer to device name.  This routine should get the
//  	current device settings and save them so devclose() can restore them.
//  	It should open the device.  If the device is a serial port, devopen()
//  	set the speed, stop bits, flow control, etc.
//  Returns: 0 on failure, 1 on success.
// ------------------------------------------------------------------------------------------------------
int devopen(char *device) 
{
    //ttyfd = 0;
    return(1);
}

// ------------------------------------------------------------------------------------------------------
//  P K T M O D E  --  Put communications device into or out of packet mode 
//		Call with: 0 to put in normal (cooked) mode, 1 to put in packet (raw) mode.
// 		For a "dumb i/o device" like an i/o port that does not have a login attached
//  	to it, this routine can usually be a no-op.
//  Returns: 0 on failure, 1 on success.
// ------------------------------------------------------------------------------------------------------
int pktmode(short on) 
{
    //if (ttyfd < 0)                      /* Device must be open */
    //  return(0);
    //system(on ? "stty raw -echo" : "stty sane"); /* Crude but effective */
    return(1);
}


// ------------------------------------------------------------------------------------------------------
// D E V S E T T I N G S 
// ------------------------------------------------------------------------------------------------------
int devsettings(char * s) 
{
    /* Get current device settings, save them for devrestore() */
    /* Parse string s, do whatever it says, e.g. "9600;8N1" */
    //if (!pktmode(ON))			/* And put device in packet mode */
    //  return(0);
    return(1);
}

// ------------------------------------------------------------------------------------------------------
//  D E V R E S T O R E 
// ------------------------------------------------------------------------------------------------------
int devrestore(void) 
{
    /* Put device back as we found it */
    //pktmode(OFF);
    return(1);
}


// ------------------------------------------------------------------------------------------------------
//  D E V C L O S E  --  Closes the current open communications device 
//  	Call with: nothing
//		Closes the device and puts it back the way it was found by devopen().
//		Returns: 0 on failure, 1 on success.
// ------------------------------------------------------------------------------------------------------
int devclose(void) 
{
    //ttyfd = -1;
    return(1);
}

// ------------------------------------------------------------------------------------------------------
// I N C H K  --  Check if input waiting
//		Check if input is waiting to be read, needed for sliding windows.  This
//		sample version simply looks in the stdin buffer (which is not portable
//		even among different Unixes).  If your platform does not provide a way to
//		look at the device input buffer without blocking and without actually
//		reading from it, make this routine return -1.  On success, returns the
//		numbers of characters waiting to be read, i.e. that can be safely read
//		without blocking.
// ------------------------------------------------------------------------------------------------------
int inchk(struct lib_EKermit_If *EK_If)
{
#ifdef _IO_file_flags			/* Linux */
    if (ttyfd < 0)                      /* Device must be open */
      return(0);
    return((int) ((stdin->_IO_read_end) - (stdin->_IO_read_ptr)));
#else
#ifdef AIX				/* AIX */
    if (ttyfd < 0)
      return(0);
    return(stdin->_cnt);
#else
#ifdef SunOS				/* Solaris and SunOS */
    if (ttyfd < 0)
      return(0);
    return(stdin->_cnt);
#else  
#ifdef HPUX				/* HPUX */
    if (ttyfd < 0)
      return(0);
    return(stdin->__cnt);
#else
    return(-1);
#endif // HPUX
#endif // SunOS
#endif // AIX 
#endif // _IO_file_flags
}


// ------------------------------------------------------------------------------------------------------
//  R E A D P K T  --  Read a Kermit packet from the communications device  */
//  Call with:
//    k   - Kermit struct pointer
//    p   - pointer to read buffer
//    len - length of read buffer
//  When reading a packet, this function looks for start of Kermit packet
//  (k->r_soh), then reads everything between it and the end of the packet
//  (k->r_eom) into the indicated buffer.  Returns the number of bytes read, or:
//     0   - timeout or other possibly correctable error;
//    -1   - fatal error, such as loss of connection, or no buffer to read into.
// ------------------------------------------------------------------------------------------------------
int readpkt(struct lib_EKermit_If *EK_If, UCHAR *p, int len) 
{
    int x, n;		//max
    short flag;
    UCHAR c;
	bool res = false;
//		Timeout not implemented in this sample.
//		It should not be needed.  All non-embedded Kermits that are capable of
//		making connections are also capable of timing out, and only one Kermit
//		needs to time out.  NOTE: This simple example waits for SOH and then
//		reads everything up to the negotiated packet terminator.  A more robust
//		version might be driven by the value of the packet-length field.

//#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
//	UCHAR	p2;
//#endif

#ifdef F_CTRLC
    short ccn;
    ccn = 0;
#endif // F_CTRLC

    //if (ttyfd < 0 || !p) {		/* Device not open or no buffer */
	if (!p) 						/* no buffer */
	{
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
		dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> readpkt FAIL");
#endif			
		return(-1);
    }
    flag = n = 0;                       /* Init local variables */

//#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
//    p2 = *p;
//#endif

    while (1) 
	{
        //x = getchar();                  /* Replace this with real i/o */
		
		//res = EK_If->xArgs.WriteDrv->pAPI_STD->Read_Data(NULL, EK_If->xArgs.WriteDrv, &x, 1, XFer_Blocking);						// read char from I/O
		res = EK_If->xArgs.WriteDrv->pAPI_STD->Read_Data(NULL, EK_If->xArgs.WriteDrv, EK_If->xArgs.pDstBuff, 1, XFer_Blocking);		// read char from I/O
		if(res == true)
		{
			sys_Buffer_Read_Element(EK_If->xArgs.pDstBuff, (uint8_t *) &x, 1);
			
			c = (EK_If->k.parity) ? x & 0x7f : x & 0xff; /* Strip parity */

#ifdef F_CTRLC
		/* In remote mode only: three consecutive ^C's to quit */
			if (EK_If->k.remote && c == (UCHAR) 3) 
			{
				if (++ccn > 2) 
				{
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
					dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> readpkt ^C^C^C");
#endif				
					return(-1);
				}
			}
			else 
			{
				ccn = 0;
			}
#endif /* F_CTRLC */

			if (!flag && c != EK_If->k.r_soh)	/* No start of packet yet */
			  continue;                     /* so discard these bytes. */
			if (c == EK_If->k.r_soh) 			/* Start of packet */
			{
				flag = 1;                   /* Remember */
				continue;                   /* But discard. */
			} 
			else if (c == EK_If->k.r_eom || c == '\012')	/* Packet terminator  ||  1.3: For HyperTerminal */
			{
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
				*p = KERMIT_NUL;                   /* Terminate for printing */
				dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> RPKT");
#endif
				return(n);
			} 
			else 
			{                        /* Contents of packet */
				if (n++ > EK_If->k.r_maxlen)	/* Check length */
				  return(0);
				else
				  *p++ = x & 0xff;
			}
		}
	}
//#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
//	dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> READPKT FAIL (end)");
//#endif				
    //return(-1);
}


// ------------------------------------------------------------------------------------------------------
//  T X _ D A T A  --  Writes n bytes of data to communication device.
//  Call with:
//    k = pointer to Kermit struct.
//    p = pointer to data to transmit.
//    n = length.
//  Returns:
//    X_OK on success.
//    X_ERROR on failure to write - i/o error.
// ------------------------------------------------------------------------------------------------------
int tx_data(struct lib_EKermit_If *EK_If, UCHAR *p, int n)
{
    int x;
    int max;
	bool res;
	
    max = 10;                           											// Loop breaker
    while (n > 0)                      												// Keep trying till done
	{
        //x = write(ttyfd,p,n);
		//res = EK_If->xArgs.WriteDrv->pAPI_STD->Write_Data(NULL, EK_If->xArgs.WriteDrv, p, n, XFer_Blocking);	// po zapise sa n nezmeni . takze neviem kolko byte sa zapisalo
		sys_Buffer_Write_Element(EK_If->xArgs.pSrcBuff, p, 1);		
		res = EK_If->xArgs.WriteDrv->pAPI_STD->Write_Data(NULL, EK_If->xArgs.WriteDrv, EK_If->xArgs.pSrcBuff, n, XFer_Blocking);	// po zapise sa n nezmeni . takze neviem kolko byte sa zapisalo
		
		
		
		
		
        //debug(DB_MSG,"tx_data write",0,x);
        if (res == false || --max < 1) return(X_ERROR);      						// Errors are fatal
        if (res == true) x = n;
		n -= x;																		// decrement write len by wrtten count
		p += x;																		// increment buff position
    }
    return(X_OK);                       											// Success
}




// ------------------------------------------------------------------------------------------------------
//  Prepare memxfer
//  Call with:
//    Pointer to filename.
//    Size in bytes.
//    Creation date in format yyyymmdd hh:mm:ss, e.g. 19950208 14:00:00
//    Mode: 1 = read, 2 = create, 3 = append.
//  Returns:
//    X_OK on success.
//    X_ERROR on failure, including rejection based on name, size, or date.    
// ------------------------------------------------------------------------------------------------------
int mem_prepare(struct lib_EKermit_If *EK_If, UCHAR * s, int mode)
{
    switch (mode) 
	{
		case 1:				// Read
		{
			EK_If->k.s_first   = 1;													// Set up for getkpt
			EK_If->k.zinbuf[0] = '\0';												// Initialize buffer
			EK_If->k.zinptr    = EK_If->k.zinbuf;									// Set up buffer pointer
			EK_If->k.zincnt    = 0;													// and count
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
		dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> MEM_PREPARE for read ok");
#endif				
			return(X_OK);
		}
		case 2:				// Write
		{
//        	ofile = creat(s,0644);
//			if (ofile < 0) 
//			{
//	    		debug(DB_LOG,"openfile write error",s,0);
//	    		return(X_ERROR);
//			}
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
			dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> MEM_PREPARE for write ok");
#endif				
			return(X_OK);
		}
#ifdef COMMENT
		case 3:				/* Append (not used) */
			//ofile = open(s,O_WRONLY|O_APPEND);
			if (ofile < 0) 
			{
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
				dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") ID[%s]: -> openfile append error");
#endif				
				return(X_ERROR);
			}
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
			dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") ID[%s]: -> openfile append ok");
#endif				
			return(X_OK);
#endif /* COMMENT */
		default:
			return(X_ERROR);
    }
}


// ------------------------------------------------------------------------------------------------
// Write to Memory area specified in EK_If.xARGS
// ------------------------------------------------------------------------------------------------------
int writemem(struct lib_EKermit_If *EK_If, UCHAR * s, int n) 
{
    int rc;
    rc = X_OK;
	bool res = false;
	
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
	dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> writefile binary");
#endif				

    if (EK_If->k.Binary) 			/* Binary mode, just write it */
	{
		if(EK_If->xArgs.TotalLength)
		{
			res = EK_If->xArgs.fn_blockwrite ( s, n, &EK_If->xArgs.StartAddress, (uint32_t*) &EK_If->xArgs.TotalLength);
			if (res == true) 
			{
				EK_If->xArgs.writef_buffwrcount += n;
				rc = X_OK;
			}
			else rc = X_ERROR;
		} else rc = X_OK;
	}
	else
	{				/* Text mode, skip CRs */
		UCHAR * p, * q;
		int i;
		q = s;

		while (1) 
		{
			for (p = q, i = 0; ((*p) && (*p != (UCHAR)13)); p++, i++) ;
			if (i > 0)
			//if (write(ofile,q,i) != i) rc = X_ERROR;
			if (!*p) break;
			q = p+1;
		}
    }
    return(rc);
}


// ------------------------------------------------------------------------------------------------
// Read from Memory area specified in EK_If.xARGS
// ------------------------------------------------------------------------------------------------------
int readmem(struct lib_EKermit_If *EK_If)
{
	bool res = false;
	
    if (!EK_If->k.zinptr) 
	{
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
		dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> readfile ZINPTR NOT SET");
#endif				
		return(X_ERROR);
	}
	if (EK_If->k.zincnt < 1) 														// Nothing in pRDBuffer - must refill - call fn_blockread!
	{
		if (EK_If->k.Binary)														// Binary - just read raw buffers
		{
			uint32_t bytesread = 0;
			EK_If->k.dummy = 0;
			//	element transfered = fread(dst, elm size, elm count, stream)
			//     EK_If->k.zincnt = fread(EK_If->k.zinbuf, 1, EK_If->k.zinlen, ifile);
			
			if(EK_If->xArgs.readf_buffrdcount >= EK_If->xArgs.TotalLength) return(-1);					// EOF !!!
			
//			toto vychadza zaporne cislo, tym padom to nevie porovnat !!!!
			if(EK_If->xArgs.TotalLength <= EK_If->k.zinlen) bytesread = EK_If->xArgs.TotalLength;		// nacitaj vsetko na 1x 
			else bytesread = EK_If->k.zinlen;															// napln komplet buffer
			EK_If->k.zincnt = bytesread;																// Kolko ideme citat
			
			
			// nacitaj blok z pamate - picovina, neviem kolko byte som naozaj nacital
			res = EK_If->xArgs.fn_blockread ( EK_If->k.zinbuf, EK_If->k.zinlen, &EK_If->xArgs.StartAddress, (uint32_t*) &bytesread);			
			EK_If->xArgs.readf_buffrdcount += EK_If->k.zincnt;
			
			if(res == false) EK_If->k.zincnt = 0;									// chyba citania
			else EK_If->k.zincnt -= bytesread;										// ak nenacital vsetko ale nenastala chyba, odpocitaj to co este nenacital. ostane to v bytesread.
						
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
			dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> readmem read ok, zincnt: %d", EK_If->k.zincnt);
#endif	
		}
		if (EK_If->k.zincnt == 0)		/* Check for EOF */
			return(-1);
		EK_If->k.zinptr = EK_If->k.zinbuf;		/* Not EOF - reset pointer */
    }
    (EK_If->k.zincnt)--;			/* Return first byte. */
    return(*(EK_If->k.zinptr)++ & 0xff);
}



// ------------------------------------------------------------------------------------------------------
//  O P E N F I L E  --  Open output file 
//  Call with:
//    Pointer to filename.
//    Size in bytes.
//    Creation date in format yyyymmdd hh:mm:ss, e.g. 19950208 14:00:00
//    Mode: 1 = read, 2 = create, 3 = append.
//  Returns:
//    X_OK on success.
//    X_ERROR on failure, including rejection based on name, size, or date.    
// ------------------------------------------------------------------------------------------------------
int openfile(struct lib_EKermit_If *EK_If, UCHAR * s, int mode)
{
    switch (mode) {
      case 1:				/* Read */
	EK_If->k.s_first   = 1;		/* Set up for getkpt */
	EK_If->k.zinbuf[0] = '\0';		/* Initialize buffer */
	EK_If->k.zinptr    = EK_If->k.zinbuf;	/* Set up buffer pointer */
	EK_If->k.zincnt    = 0;		/* and count */
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
	dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> openfile read ok");
#endif				
	  
	return(X_OK);

      case 2:				/* Write (create) */
//        ofile = creat(s,0644);
//	if (ofile < 0) {
//	    debug(DB_LOG,"openfile write error",s,0);
//	    return(X_ERROR);
//	}
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
	dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> openfile write ok");
#endif				
	  
	return(X_OK);

#ifdef COMMENT
      case 3:				/* Append (not used) */
        ofile = open(s,O_WRONLY|O_APPEND);
	if (ofile < 0) 
	{
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
		dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") ID[%s]: -> openfile append error");
#endif				
	    return(X_ERROR);
	}
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
	dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") ID[%s]: -> openfile append ok");
#endif				
	return(X_OK);
#endif /* COMMENT */

      default:
        return(X_ERROR);
    }
}

// ------------------------------------------------------------------------------------------------------
//  F I L E I N F O  --  Get info about existing file

//  Call with:
//    Pointer to filename
//    Pointer to buffer for date-time string
//    Length of date-time string buffer (must be at least 18 bytes)
//    Pointer to int file type:
//       0: Prevailing type is text.
//       1: Prevailing type is binary.
//    Transfer mode (0 = auto, 1 = manual):
//       0: Figure out whether file is text or binary and return type.
//       1: (nonzero) Don't try to figure out file type. 
//  Returns:
//    X_ERROR on failure.
//    0L or greater on success == file length.
//    Date-time string set to yyyymmdd hh:mm:ss modtime of file.
//    If date can't be determined, first byte of buffer is set to NUL.
//    Type set to 0 (text) or 1 (binary) if mode == 0.
// ------------------------------------------------------------------------------------------------------
#ifdef F_SCAN
#define SCANBUF 1024
#define SCANSIZ 49152
#endif /* F_SCAN */
ULONG fileinfo(struct lib_EKermit_If *EK_If, UCHAR * filename, UCHAR * buf, int buflen, short * type, short mode)
{
//    struct stat statbuf;
//    struct tm * timestamp, * localtime();

////#ifdef F_SCAN
////    FILE * fp;				/* File scan pointer */
////    char inbuf[SCANBUF];		/* and buffer */
////#endif /* F_SCAN */

//    if (!buf) return(X_ERROR);
//    buf[0] = '\0';
//    if (buflen < 18) return(X_ERROR);
//    if (stat(filename,&statbuf) < 0) return(X_ERROR);
//    timestamp = localtime(&(statbuf.st_mtime));
//    sprintf(buf,"%04d%02d%02d %02d:%02d:%02d", timestamp->tm_year + 1900, timestamp->tm_mon + 1, timestamp->tm_mday, timestamp->tm_hour, timestamp->tm_min, timestamp->tm_sec);
//#ifdef F_SCAN
///*
//  Here we determine if the file is text or binary if the transfer mode is
//  not forced.  This is an extremely crude sample, which diagnoses any file
//  that contains a control character other than HT, LF, FF, or CR as binary.
//  A more thorough content analysis can be done that accounts for various
//  character sets as well as various forms of Unicode (UTF-8, UTF-16, etc).
//  Or the diagnosis could be based wholly or in part on the filename.
//  etc etc.  Or the implementation could skip this entirely by not defining
//  F_SCAN and/or by always calling this routine with type set to -1.
//*/
//    if (!mode) 			/* File type determination requested */
//	{
//		int isbinary = 1;
//		fp = fopen(filename,"r");	/* Open the file for scanning */
//		if (fp) 
//		{
//			int n = 0, count = 0;
//			char c, * p;

//			debug(DB_LOG,"fileinfo scan ",filename,0);

//			isbinary = 0;
//			while (count < SCANSIZ && !isbinary)  /* Scan this much */
//			{
//				n = fread(inbuf,1,SCANBUF,fp);
//				if (n == EOF || n == 0) break;
//				count += n;
//				p = inbuf;
//				while (n--) 
//				{
//					c = *p++;
//					if (c < 32 || c == 127) 
//					{
//						if (c !=  9 &&	/* Tab */
//						c != 10 &&	/* LF */
//						c != 12 &&	/* FF */
//						c != 13) 	/* CR */
//						{
//							isbinary = 1;
//							debug(DB_MSG,"fileinfo BINARY",0,0);
//							break;
//						}
//					}
//				}
//			}
//			fclose(fp);
//			*type = isbinary;
//		}
//    }
//#endif /* F_SCAN */

    return((ULONG) 0); //(statbuf.st_size));
}


// ------------------------------------------------------------------------------------------------------
//  R E A D F I L E  --  Read data from a file
// ------------------------------------------------------------------------------------------------------
int readfile(struct lib_EKermit_If *EK_If)
{
    if (!EK_If->k.zinptr) 
	{
#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
		dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> readfile ZINPTR NOT SET");
#endif				
		return(X_ERROR);
    }
//    if (k->zincnt < 1) {		/* Nothing in buffer - must refill */
//	if (k->binary) {		/* Binary - just read raw buffers */
//	    k->dummy = 0;
//	    k->zincnt = fread(k->zinbuf, 1, k->zinlen, ifile);
//	    debug(DB_LOG,"readfile binary ok zincnt",0,k->zincnt);

//	} else {			/* Text mode needs LF/CRLF handling */
//	    int c;			/* Current character */
//	    for (k->zincnt = 0; (k->zincnt < (k->zinlen - 2)); (k->zincnt)++) {
//		if ((c = getc(ifile)) == EOF)
//		  break;
//		if (c == '\n')		/* Have newline? */
//		  k->zinbuf[(k->zincnt)++] = '\r'; /* Insert CR */
//		k->zinbuf[k->zincnt] = c;
//	    }
//#ifdef DEBUG
//	    k->zinbuf[k->zincnt] = '\0';
//	    debug(DB_LOG,"readfile text ok zincnt",0,k->zincnt);
//#endif /* DEBUG */
//	}
//	k->zinbuf[k->zincnt] = '\0';	/* Terminate. */
//	if (k->zincnt == 0)		/* Check for EOF */
//	  return(-1);
//	k->zinptr = k->zinbuf;		/* Not EOF - reset pointer */
//    }
//    (k->zincnt)--;			/* Return first byte. */

//    debug(DB_LOG,"readfile exit zincnt",0,k->zincnt);
//    debug(DB_LOG,"readfile exit zinptr",0,k->zinptr);
    return(*(EK_If->k.zinptr)++ & 0xff);
}


// ------------------------------------------------------------------------------------------------------
//  W R I T E F I L E  --  Write data to file
//  Call with:
//    Kermit struct
//    String pointer
//    Length
//  Returns:
//    X_OK on success
//    X_ERROR on failure, such as i/o error, space used up, etc
// ------------------------------------------------------------------------------------------------------
int writefile(struct lib_EKermit_If *EK_If, UCHAR * s, int n) 
{
    int rc;
    rc = X_OK;

//#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
//	dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> writefile binary");
//#endif				

//    if (k->binary) 			/* Binary mode, just write it */
//	{
//		if (write(ofile,s,n) != n)  rc = X_ERROR;
//    } 
//	else
//	{				/* Text mode, skip CRs */
//		UCHAR * p, * q;
//		int i;
//		q = s;

//		while (1) 
//		{
//			for (p = q, i = 0; ((*p) && (*p != (UCHAR)13)); p++, i++) ;
//			if (i > 0)
//			if (write(ofile,q,i) != i) rc = X_ERROR;
//			if (!*p) break;
//			q = p+1;
//		}
//    }
    return(rc);
}


// ------------------------------------------------------------------------------------------------------
//  C L O S E F I L E  --  Close output file
//  Mode = 1 for input file, mode = 2 or 3 for output file.

//  For output files, the character c is the character (if any) from the Z
//  packet data field.  If it is D, it means the file transfer was canceled
//  in midstream by the sender, and the file is therefore incomplete.  This
//  routine should check for that and decide what to do.  It should be
//  harmless to call this routine for a file that that is not open.
// ------------------------------------------------------------------------------------------------------
int closefile(struct lib_EKermit_If *EK_If, UCHAR c, int mode) 
{
    int rc = X_OK;			/* Return code */

//    switch (mode) 
//	{
//	case 1:				/* Closing input file */
//			if (!ifile)	break;		/* If not not open */ /* do nothing but succeed */
//#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
//	dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> closefile (input)");
//#endif				

//			if (fclose(ifile) < 0)	rc = X_ERROR;
//			break;
//	case 2:				/* Closing output file */
//	case 3:
//			if (ofile < 0) break;			/* If not open */ /* do nothing but succeed */
//#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
//			dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> closefile (output) name :%s",k->filename);
//			dbgprint("\r\n\t("LIB_EKERMIT_OBJECTNAME") -> closefile (output) keep");
//#endif				
//			if (close(ofile) < 0) 		/* Try to close */
//			{
//				rc = X_ERROR;
//			} 
//			else if ((k->ikeep == false) &&	/* Don't keep incomplete files */
//					(c == 'D')) 	/* This file was incomplete */
//			{
//				if (k->filename) 
//				{
//					debug(DB_LOG,"deleting incomplete",k->filename,0);
//					unlink(k->filename);	/* Delete it. */
//				}
//			}
//			break;
//	default:
//			rc = X_ERROR;
//    }
    return(rc);
}

#if defined(CONF_DEBUG_LIB_EKERMIT) && (CONF_DEBUG_LIB_EKERMIT == 1)
// ------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
int xerror() 
{
    unsigned int x;
    extern int errorrate;															// Fix this - NO EXTERNS
    if (!errorrate)
      return(0);
    x = sys_rand() % 100;															// Fix this - NO C LIBRARY
//    debug(DB_LOG,"RANDOM",0,x);
//    debug(DB_LOG,"ERROR",0,(x < errorrate));
	return(x < errorrate);
}
#endif

#endif			// CONF_USE_LIB_EKERMIT
