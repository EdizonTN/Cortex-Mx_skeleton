// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: lib_CLI.h
// 	   Version: 2.0
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: Command Line Interface library
//		 Process CLI on some char in/out device
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
//



#ifndef __LIB_CLI_H_
#define __LIB_CLI_H_

#include "Skeleton.h"

#define SYSTMR_MS_TO_TICKS(delay_in_ms)    ((delay_in_ms * CONF_SYSTICK_FREQ) / 1000)

/// ----------------------------------------------------- Timeout
/// Timer state
typedef enum
{
    SYSTMR_STOPPED = 0,
    SYSTMR_STARTED,
    SYSTMR_EXPIRED,
} systmr_state_t;

/// Structure to track state of a timer
typedef struct
{
    systmr_state_t state;           ///< State of timer: STOPPED, STARTED or EXPIRED
    volatile uint_fast64_t start_tick;      ///< Tick when timer started
    volatile uint_fast64_t delay_in_ticks;  ///< Timer delay, used for subsequent timer restarts/resets
} systmr_t;
/// ----------------------------------------------------- end Timeout	







// ******************************************************************************************************
// CONFIGURATION
// ******************************************************************************************************
#ifndef CONF_DEBUG_LIB_CLI
 #define CONF_DEBUG_LIB_CLI				0											// Default debug is off
#endif

#ifndef CONF_CLI_USE_XMODEM
 #define CONF_CLI_USE_XMODEM            0											// use XMODEM transfer protocol?
#endif

#ifndef CONF_CLI_USE_YMODEM
 #define CONF_CLI_USE_YZMODEM           0											// use YMODEM transfer protocol?
#endif

// Check that all project specific options have been correctly specified
#ifndef CLI_CFG_ARGV_MAX
 #define CLI_CFG_ARGV_MAX            	16											// maximalny pocet argumentov fcii volanych cez CLI
#endif

#ifndef CLI_CFG_LINE_LENGTH_MAX
 #define CLI_CFG_LINE_LENGTH_MAX     	64											// dlzka command line v CLI
#endif
#if (CLI_CFG_LINE_LENGTH_MAX > 255)
#error "CLI_CFG_LINE_LENGTH_MAX must be less than 256"
#endif

#ifndef CLI_CFG_TREE_DEPTH_MAX
 #define CLI_CFG_TREE_DEPTH_MAX      	2											// maximalna hlbka menu a submenu
#endif
#if (CLI_CFG_TREE_DEPTH_MAX < 1) || (CLI_CFG_TREE_DEPTH_MAX > CLI_CFG_ARGV_MAX)
#error "CLI_CFG_LINE_LENGTH_MAX must be at least 1 and less than or equal to CLI_CFG_ARGV_MAX"
#endif

#ifndef CLI_CFG_HISTORY_SIZE
 #define CLI_CFG_HISTORY_SIZE        	64											// pocet poloziek v historii prikazov v CLI
#endif
#if (CLI_CFG_HISTORY_SIZE != 0) && (CLI_CFG_HISTORY_SIZE < CLI_CFG_LINE_LENGTH_MAX)
#error "CLI_CFG_HISTORY_SIZE must be equal or greater than CLI_CFG_LINE_LENGTH_MAX"
#endif
#if (CLI_CFG_HISTORY_SIZE > 65536)
#error "CLI_CFG_HISTORY_SIZE must be equal or less than 65536"
#endif


#ifndef CLI_CFG_NAME_STR_MAX_SIZE
 #define CLI_CFG_NAME_STR_MAX_SIZE   	16											//
#endif

#ifndef CLI_CFG_STARTUPNAME
 #define CLI_CFG_STARTUPNAME			" - Command line interface"
#endif

#ifndef CLI_CFG_PARAM_STR_MAX_SIZE
 #define CLI_CFG_PARAM_STR_MAX_SIZE  	42											//
#endif

#if (CLI_CFG_NAME_STR_MAX_SIZE != 0) && (CLI_CFG_PARAM_STR_MAX_SIZE == 0)
 #error "CLI_CFG_PARAM_STR_MAX_SIZE must also be specifed to reduce code size"
#endif
#if (CLI_CFG_NAME_STR_MAX_SIZE == 0) && (CLI_CFG_PARAM_STR_MAX_SIZE != 0)
 #error "CLI_CFG_NAME_STR_MAX_SIZE must also be specifed to reduce code size"
#endif







// ******************************************************************************************************
// PUBLIC Defines
// ******************************************************************************************************
typedef bool (*fn_write_rx_data_t)(uint8_t *pDataBuffer, size_t BufferSize, uint32_t *pWriteToAddress, size_t *pLength);
typedef bool (*fn_read_tx_data_t) (uint8_t *pDataBuffer, size_t BufferSize, uint32_t *pReadFromAddress, size_t *pLength);

// XMODEM - fixed 128 bytes data Block	
// YMODEM - filename + datasize + timestamp
// ZMODEM - YMODEM + sliding windows + checksum
	



// ******************************************************************************************************
// 												KERMIT file transfer protocol
#if defined(CONF_USE_LIB_EKERMIT) && (CONF_USE_LIB_EKERMIT == 1)
	#define CLI_send_file 				kermit_send_file
	#define CLI_receive_file 			kermit_receive_file
#endif


// ******************************************************************************************************
// 												YMODEM file transfer protocol
#if defined(CONF_CLI_USE_YMODEM) && (CONF_CLI_USE_YMODEM == 1)

/* Comment this out if you don't plan to use extra CRC32 - removes ~1K */
#define YMODEM_WITH_CRC32				0

#define YMODEM_PACKET_SEQNO_INDEX      	1
#define YMODEM_PACKET_SEQNO_COMP_INDEX 	2

//#define YMODEM_PACKET_HEADER           	3     										// start, block, block-complement
//#define YMODEM_PACKET_TRAILER          	2     										// CRC bytes
//#define YMODEM_PACKET_OVERHEAD         	(YMODEM_PACKET_HEADER + YMODEM_PACKET_TRAILER)
#define YMODEM_PACKET_SIZE             	128
#define YMODEM_PACKET_1K_SIZE          	1024
#define YMODEM_DATA_SIZE          		1024

#define YMODEM_PACKET_TIMEOUT          	1000

#define YMODEM_FILE_NAME_LENGTH 		64
#define YMODEM_FILE_SIZE_LENGTH 		16

/* ASCII control codes: */
#define YMODEM_SOH 						0x01     									// start of 128-byte data packet
#define YMODEM_STX 						0x02										// start of 1024-byte data packet
#define YMODEM_EOT 						0x04										// end of transmission
#define YMODEM_ACK 						0x06										// receive OK
#define YMODEM_NAK						0x15										// receiver error; retry
#define YMODEM_CAN 						0x18										// two of these in succession aborts transfer
#define YMODEM_CRC						0x43										// use in place of first NAK for CRC mode

#define YMODEM_MAX_ERRORS    			5											// Number of consecutive receive errors before giving up




typedef struct																		// XMODEM packet structure definition
{
    uint8_t  Start;
    uint8_t  Packet_nr;
    uint8_t  Packet_nr_inv;
    __packed uint8_t  PayLoad[YMODEM_DATA_SIZE];
    uint8_t  CRC16_hi8;
    uint8_t  CRC16_lo8;
} ymodem_packet_t;


static union ymodem_packet															// Packet buffer
{
    ymodem_packet_t 					Packet;
    uint8_t            					RAW[sizeof(ymodem_packet_t)];
} ymodem_packet_u;


#define CLI_send_file 		zmodem_send_file
#define CLI_receive_file 	zmodem_receive_file
#endif


	
// ******************************************************************************************************
// 												XMODEM file transfer protocol
#if defined(CONF_CLI_USE_XMODEM) && (CONF_CLI_USE_XMODEM == 1)
#ifndef XMODEM_CFG_STARTWAIT_MSEC
 #define XMODEM_CFG_STARTWAIT_MSEC      20000
#endif

#ifndef XMODEM_CFG_MAX_RETRIES
 #define XMODEM_CFG_MAX_RETRIES       	10
#endif

#ifndef XMODEM_CFG_MAX_RETRIES_START
 #define XMODEM_CFG_MAX_RETRIES_START 	10
#endif


// *************** XMODEM protocol definitions
#define XMODEM_DATA_SIZE         		128											// must be modulo of 128 !
#define XMODEM_TIMEOUT_MS        		1000

typedef struct																		// XMODEM packet structure definition
{
    uint8_t  Start;
    uint8_t  Packet_nr;
    uint8_t  Packet_nr_inv;
    __packed uint8_t  PayLoad[XMODEM_DATA_SIZE];
    uint8_t  CRC16_hi8;
    uint8_t  CRC16_lo8;
} xmodem_packet_t;

static union xmodem_packet															// Packet view
{
    xmodem_packet_t 					Struct;										// structure view
    uint8_t            					RAW[sizeof(xmodem_packet_t)];				// array view
} xmodem_packet_u;

typedef struct XModem
{
	union xmodem_packet					Packet;										// link to packet
	uint8_t 							packet_nr;									// Variable to keep track of current packet number
} XModem_t;


// XMODEM flow control characters
#define XMODEM_SOH               		0x01 										// Start of Header
#define XMODEM_EOT               		0x04 										// End of Transmission 
#define XMODEM_ACK               		0x06 										// Acknowledge 
#define XMODEM_NAK               		0x15 										// Not Acknowledge 
#define XMODEM_CAN  					0x18										// Canceled
#define XMODEM_C                 		0x43 										// ASCII 'C'


#define U16_HI8(data) ((uint8_t)(((data)>>8)&0xff))									// Extract the high 8 bits of a 16-bit value (Most Significant Byte)
#define U16_LO8(data) ((uint8_t)((data)&0xff))										// Extract the low 8 bits of a 16-bit value (Least Significant Byte)

/**
    Blocking function that receives a file using the XMODEM-CRC protocol.
    
    @param on_rx_data   Pointer to a function that will be called once a block of
                        data has been received.
   
    @retval true        File succesfully received
    @retval false       Timed out while trying to receive a file
 */
//bool xmodem_receive_file(xmodem_on_rx_data_t on_rx_data);

#define CLI_send_file 		xmodem_send_file
#define CLI_receive_file 	xmodem_receive_file
/**
    Blocking function that sends a file using the XMODEM-CRC protocol.
    
    @param on_tx_data   pointer to a function that will be called to supply
                        data to send. 

    @retval true        File succesfully sent
    @retval false       Timed out while trying to send a file
 */

//typedef bool (*fn_write_rx_data_t)(uint8_t *pDataBuffer, size_t BufferSize, uint32_t *pWriteToAddress, size_t *pLength);
//typedef bool (*fn_read_tx_data_t)(uint8_t *pDataBuffer, size_t BufferSize, uint32_t *pReadFromAddress, size_t *pLength);
#endif



COMP_PACKED_BEGIN
typedef struct CLI_If
{
#if defined(CONF_DEBUG_LIB_CLI) && (CONF_DEBUG_LIB_CLI == 1)	
	const 	char						Name[CONF_DEBUG_STRING_NAME_SIZE];			// interface name - text - only for ID
#endif
	bool 					Initialized;											// CLI is initialized?
	bool					Enabled;												// CLI is Enabled?
	_drv_If_t				*ConnectedDrv;											// which driver send and receive data?
	systmr_t 				tmr;													// timeout structure
	uint8_t					RxChar;													// last received character
#if defined(CONF_CLI_USE_XMODEM) && (CONF_CLI_USE_XMODEM == 1)
	//union xmodem_packet	XModem;													// Xmodem packet content
	XModem_t				XModem;
#elif defined(CONF_CLI_USE_YMODEM) && (CONF_CLI_USE_YMODEM == 1)	
	union YModem_t			YModem;													// Ymodem packet content
#endif
} lib_CLI_If_t;
COMP_PACKED_END



#if defined(CONF_CLI_USE_YMODEM) && (CONF_CLI_USE_YMODEM == 1)
extern unsigned long ymodem_receive_file(lib_CLI_If_t* const pCLI_If, unsigned char *buf, unsigned long length);
extern unsigned long ymodem_send_file(lib_CLI_If_t* const pCLI_If, unsigned char* buf, unsigned long size, char* filename);
#endif

#if defined(CONF_CLI_USE_XMODEM) && (CONF_CLI_USE_XMODEM == 1)
bool xmodem_send_file   (lib_CLI_If_t* const pCLI_If, fn_read_tx_data_t on_tx_data, uint32_t StartAddress, size_t Length);
bool xmodem_receive_file(lib_CLI_If_t* const pCLI_If, fn_write_rx_data_t on_rx_data, uint32_t StartAddress, size_t Length);
#endif

#if defined(CONF_CLI_USE_ZMODEM) && (CONF_CLI_USE_ZMODEM == 1)
bool zmodem_send_file   (lib_CLI_If_t* const pCLI_If, fn_read_tx_data_t on_tx_data, uint32_t StartAddress, size_t Length);
bool zmodem_receive_file(lib_CLI_If_t* const pCLI_If, fn_write_rx_data_t on_rx_data, uint32_t StartAddress, size_t Length);
#endif



// ******************************************************************************************************
// VT100
// ******************************************************************************************************
#define VT100_CHAR_BEL      			0x07
#define VT100_CHAR_BS       			0x08
#define VT100_CHAR_TAB      			0x09
#define VT100_CHAR_CR       			0x0D
#define VT100_CHAR_LF       			0x0A
#define VT100_CHAR_ESC      			0x1B
#define VT100_CHAR_DEL      			0x7F
#define VT100_CHAR_QUESTMARK   			'?'

typedef enum																		// VT100 Terminal receive state
{
    VT100_CHAR_NORMAL,          													// A normal key has been pressed and must be used
    VT100_CHAR_INVALID,         													// An invalid key code has been sent and must be discarded
    VT100_ESC_SEQ_BUSY,         													// Busy with escape sequence; data must be discarded
    VT100_ESC_SEQ_ARROW_UP,     													// Up Arrow has been pressed
    VT100_ESC_SEQ_ARROW_DN,     													// Down Arrow has been pressed
    VT100_ESC_SEQ_ARROW_LEFT,   													// Left Arrow has been pressed
    VT100_ESC_SEQ_ARROW_RIGHT,  													// Right Arrow has been pressed
} vt100_state_t;

extern void vt100_init(lib_CLI_If_t* const pCLI_If);														// Initialise module
extern vt100_state_t vt100_on_rx_char(char data);
extern void vt100_clr_screen(lib_CLI_If_t* const pCLI_If);													// Send 'clear screen' command to terminal
extern void vt100_erase_line(lib_CLI_If_t* const pCLI_If);													// Send 'erase line' command to terminal
extern void vt100_del_chars(lib_CLI_If_t* const pCLI_If, uint8_t nr_of_chars);

#define VT100_SET_DEFAULT_TEXT			"[0m"
#define VT100_SET_BLACK_TEXT			"[30m"
#define VT100_SET_RED_TEXT				"[31m"
#define VT100_SET_GREEN_TEXT			"[32m"
#define VT100_SET_YELLOW_TEXT			"[33m"
#define VT100_SET_BLUE_TEXT				"[34m"
#define VT100_SET_MAGENTA_TEXT			"[35m"
#define VT100_SET_CYAN_TEXT				"[36m"
#define VT100_SET_WHITE_TEXT			"[37m"

#define VT100_CURSOR_POS(R,C)			"["STR(R)";"STR(C)"H"
#define VT100_CURSOR_UP(n)				"["STR(n)"A"
#define VT100_CURSOR_DOWN(n)			"["STR(n)"B"
#define VT100_SCREEN_CLEAR				"[2J"
#define VT100_ERASE_LINE				"[2K"


// ********************** INTERFACE DECLARATIOJN ///

// ******************************************************************************************************
// PUBLIC 
// ******************************************************************************************************
extern bool lib_CLI_Init (lib_CLI_If_t* const pCLI_If, _drv_If_t* pOutDrv);			// Reset and init CLI - allocate memory also
extern bool lib_CLI_Input_Data(lib_CLI_If_t* const pCLI_If, uint8_t Src);				// input and process data



// ******************************************************************************************************
// PRIVATE Defines
// ******************************************************************************************************




extern bool lib_CLI_Input_Char_Wait(lib_CLI_If_t* const pCLI_If, void *DstData, uint8_t DataSize, bool PrintCountDown, const uint_fast64_t delay_in_ticks);
extern void lib_CLI_Output_Char(lib_CLI_If_t* const pCLI_If, const uint8_t Data);	// output char from CLI
extern void lib_CLI_Output_String(lib_CLI_If_t* const pCLI_If, const char *OutBuff);	// output data string from CLI. Must be terminated '\0'
extern void lib_CLI_Output_Data(lib_CLI_If_t* const pCLI_If, const char *OutBuff, size_t Length); // output binary data

extern 		lib_CLI_If_t  CLI_If;														// CLI Interface
extern uint_fast64_t sysclk_get_tick_count(void);

typedef const char* (*cli_handler_t)( lib_CLI_If_t* const pCLI_If, uint8_t argc, char* argv[]);	// Pointer to a function that will be called to handle a command

typedef struct																		// Command structure
{
    const char							*name;										// Command name
    uint8_t								argc_min;									// Minimum number of valid arguments
    uint8_t								argc_max;									// Maximum number of valid arguments
    const char 							*param; 									// Parameter string to be displayed when 'help' is executed
    const char 							*help;										// Help string to be displayed when 'help' is executed
} cli_cmd_t;

typedef struct																		// Command group structure
{
    const char 							*name; 										// Command name
    const struct cli_cmd_list_item_s 	*list; 										// Pointer to command list array
} cli_group_t;

typedef struct cli_cmd_list_item_s													// Command list item declaration
{
    const char 							*name; 										// Command name
	cli_handler_t   					handler;									// Function to be called when command is executed
    union
    {
        const cli_cmd_t					*cmd;  										// pointer to command (if handler != NULL)
        const cli_group_t 				*group;										// pointer to group   (if handler == NULL)
    } cmdgr;
} cli_cmd_list_item_t;

typedef union																		// ARGV converted value types
{
    uint8_t								u8;
    uint16_t							u16;
    uint32_t							u32;
    int8_t								s8;
    int16_t								s16;
    int32_t								s32;
    float								f;
    double								d;
} cli_argv_val_t;

extern cli_argv_val_t cli_argv_val;
extern void cli_init(lib_CLI_If_t* const pCLI_If, const char* startup);
extern void cli_on_rx_char(lib_CLI_If_t* const pCLI_If, char data);
extern const char* cli_cmd_help_fn(lib_CLI_If_t* const pCLI_If, uint8_t argc, char* argv[]);
extern const char* cli_cmd_cmdlist_fn(lib_CLI_If_t* const pCLI_If, uint8_t argc, char* argv[]);

//	timeout functions
extern bool systmr_has_expired(systmr_t *systmr);
extern void systmr_start(systmr_t *systmr, const uint_fast64_t delay_in_ticks);

uint8_t cli_util_argv_to_option(uint8_t argv_index, const char* options);
extern bool cli_util_argv_to_u8(uint8_t argv_index, uint8_t min, uint8_t max);
extern bool cli_util_argv_to_u16(uint8_t argv_index, uint16_t min, uint16_t max);
extern bool cli_util_argv_to_u32(uint8_t argv_index, uint32_t min, uint32_t max);
extern bool cli_util_argv_to_s8(uint8_t argv_index, int8_t min, int8_t max);
extern bool cli_util_argv_to_s16(uint8_t argv_index, int16_t min, int16_t max);
extern bool cli_util_argv_to_s32(uint8_t argv_index, int32_t min, int32_t max);
extern bool cli_util_argv_to_float(uint8_t argv_index);
extern bool cli_util_argv_to_double(uint8_t argv_index);
extern void cli_util_disp_buf(lib_CLI_If_t* const pCLI_If, const uint8_t * data, size_t nr_of_bytes, uint32_t address);

#define CLI_MENUITEM_CREATE(cli_cmd_name, name_str, nr_arg_min, nr_arg_max, param_str, help_str) \
    static const char cli_cmd_name ## _name[]  ATTR_PGM = name_str; \
    static const char cli_cmd_name ## _param[] ATTR_PGM = param_str; \
    static const char cli_cmd_name ## _help[]  ATTR_PGM = help_str; \
    const cli_cmd_t cli_cmd_name ATTR_PGM = \
    { \
        .name     = cli_cmd_name ## _name, \
        .argc_min = nr_arg_min, \
        .argc_max = nr_arg_max, \
        .param    = cli_cmd_name ## _param, \
        .help     = cli_cmd_name ## _help, \
    };

#define CLI_SUBMENU_CREATE(cli_submenu_name, name_str) \
    static const char                cli_submenu_name ## _str[] ATTR_PGM = name_str; \
    static const cli_cmd_list_item_t cli_submenu_name ## _list[] ATTR_PGM; \
    const cli_group_t cli_submenu_name ATTR_PGM = \
    { \
        .name = cli_submenu_name ## _str, \
        .list = cli_submenu_name ## _list, \
    }; \
    static const cli_cmd_list_item_t cli_submenu_name ## _list[] ATTR_PGM = \
    {

/// Macro to end a command list declaration
#define CLI_SUBMENU_END() \
        { \
            .handler = NULL, \
            .cmdgr.cmd     = NULL, \
        }, \
    };

/// Macro to start a command list declaration.
#define CLI_CMD_LIST_CREATE() \
    const cli_cmd_list_item_t cli_cmd_list[] ATTR_PGM = \
    {

#define CLI_CMD_ADD(cli_cmd_name, handler_fn) \
        { \
            .handler = handler_fn, \
            .cmdgr.cmd     = &cli_cmd_name, \
        },


#define CLI_SUBMENU_ADD(cli_submenu_name) \
        { \
            .handler = NULL, \
            .cmdgr.group   = &cli_submenu_name, \
        },

/// Macro to end a command list declaration
#define CLI_CMD_LIST_END() \
        { \
            .handler = NULL, \
            .cmdgr.cmd     = NULL, \
        }, \
    };
	
#endif  //__LIB_CLI_H_
