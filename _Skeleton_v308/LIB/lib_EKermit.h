// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: lib_EKermit.h
// 	   Version: 1.0
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: Kermit protocol v1.7, based on http://www.kermitproject.org/ek.html
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
//

#ifndef __LIB_EKERMIT_H_
#define __LIB_EKERMIT_H_

#include "Skeleton.h"

// ******************************************************************************************************
// CONFIGURATION
// ******************************************************************************************************

#define MINSIZE
//#define NO_LP																		// No Long packets
//#define NO_AT																		// No Attribute packets
//#define NO_CTRLC																	// No 3 consecutive Ctrl-C's to quit
//#define NO_SSW																	// No Simulated sliding windows
//#define NO_CRC																	// No CRC Check
//#define NO_SCAN																	// No Scan files for text/binary

#undef 	RECVONLY
#define	FN_MAX							16											// Filename Max. length
#define IBUFLEN  						128											// File input buffer size
#define OBUFLEN  						128											// File input buffer size

#define COMMENT							"Embedded kermit protocol"

#include "EKermit\kermit.h"


///* File Transfer Modes */
//enum xDataMode
//{
//	xBinary = 0,
//	xText	= 1
//};

/* File Transfer action */
enum xAction
{
	xNone	= 0,
	xSend	= 1,
	xRecv	= 2
};

typedef struct xArgs																// transfer arguments
{
	uint8_t		Action;																// none, write, read
	bool		Binary;																// Binary, Text,..
	size_t 		TotalLength;														// total length of transfer
	uint8_t 	*FileName;															// filename
	bool (*fn_blockread) (uint8_t *pDataBuffer, size_t BufferSize, uint32_t *pReadFromAddress, uint32_t *pLength);
	bool (*fn_blockwrite)(uint8_t *pDataBuffer, size_t BufferSize, uint32_t *pWriteToAddress, uint32_t *pLength);
	size_t		readf_buffrdcount;
	size_t		writef_buffwrcount;
	uint32_t 	StartAddress;
	_drv_If_t*	WriteDrv;
	_drv_If_t*	ReadDrv;
	sys_Buffer_t *pSrcBuff;
	sys_Buffer_t *pDstBuff;
	bool 		Remote;																// 0 = local, 1 = remote
} xArgs_t;

COMP_PACKED_BEGIN
typedef struct lib_EKermit_If
{
	//struct 		_If_Status	Stat;												// status struktura so stavom periferie
	struct		xArgs		xArgs;													// Transfer Arguments
	struct 		k_data 		k;                        								// Kermit data structure
	struct 		k_response 	r; 														// Kermit response structure
	//_ColList_t  RxFrames;															// prijate ramce - kolekcia
	//volatile 	uint8_t		Curr_Char;												// teraz spracovavany znak
	//const 		uint8_t		PKT_START[2];										// C_DLE + C_STX - inicializuju sa pre deklaracii
	//RS422Proto_Frame_st		PKT_FRAME[RS485PROTO_RTX_MAX_PACKET];				// Data v paketovej strukture
	//volatile 	uint8_t		FrameIndex;												// index prave spracovavaneho paketu
	//volatile 	uint8_t		Calc_CRC;												// pocitane CRC
	//const 		uint8_t		PKT_STOP[2];										// C_DLE + C_ETX
	//volatile 	uint16_t	PKT_Comm_Phase;											// faza prenosu telegramu. 0 - inactive
} lib_EKermit_If_t;
COMP_PACKED_END


//extern bool kermit_send_file    (uint8_t *FileName, bool Remote, bool Binary, fn_read_tx_data_t fn_data_source, uint32_t StartAddress, size_t Length, _drv_If_t *InDrv, _drv_If_t *OutDrv);
//extern bool kermit_receive_file (uint8_t *FileName, bool Remote, bool Binary, fn_write_rx_data_t fn_data_drain, uint32_t StartAddress, size_t Length, _drv_If_t *InDrv, _drv_If_t *OutDrv);
extern bool kTransfer(xArgs_t *Arguments);


#endif					// __LIB_EKERMIT_H_
