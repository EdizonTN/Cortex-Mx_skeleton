// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: lib_DFLT.h
// 	   Version: 2.01
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Digital Filters library - header file
// ******************************************************************************
// Notice:
//
// Usage:
//			
// ToDo:
//
// Changelog:
//				2024.01.15	- v2.01 - change typo
#ifndef __LIB_DFLT_H_
#define __LIB_DFLT_H_

#include "Skeleton.h"


// ************************************************************************************************
// DEFAULT CONFIGURATION
// ************************************************************************************************
#ifndef CONF_DEBUG_LIB_DFLT
#define CONF_DEBUG_LIB_DFLT				0											// Default debug is off
#endif

// ******************************************************************************************************
// PUBLIC Defines
// ******************************************************************************************************
typedef enum DigFilterTypes
{
	tNone		=	0,
	tSimpleAverage,
	tMovingAverage,
	tConstZero
}DFLT_Type_t;

typedef struct Priv
{
		uint32_t						*pMemBuff;									// mem buffer - samples aray
		uint32_t						LastSum;									// Last Sum of Samples
		uint8_t							MemPointer;
}_Private;


// library interface:
COMP_PACKED_BEGIN
typedef struct
{
#if defined(CONF_DEBUG_LIB_DFLT) && (CONF_DEBUG_LIB_DFLT == 1)	
	const 	char						Name[CONF_DEBUG_STRING_NAME_SIZE];			// interface name - text - only for ID
#endif
		bool							Init;										// Init flag
		bool							Reset;										// reset vhole array with provided value
		uint8_t							Samples;									// Number of samples for filter
		DFLT_Type_t						Type;										// filter type
		_Private						Priv;
} lib_DFLT_If_t;
COMP_PACKED_END


// ******************************************************************************************************
// PUBLIC 
// ******************************************************************************************************
extern bool lib_DFLT_Init( lib_DFLT_If_t* const pDFLT_If, DFLT_Type_t Type, uint8_t Samples, uint32_t InitVal); // Reset and init RB - allocate memory also
extern uint16_t lib_DFLT_Calculate( lib_DFLT_If_t* const pDFLT_If, uint16_t Input);

// ******************************************************************************************************
// PRIVATE Defines
// ******************************************************************************************************


#endif // __LIB_DFLT_H_

