// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: lib_DFLT.c
// 	   Version: 2.01
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Digital Filters library
// ******************************************************************************
// Notice:
//
// Usage:
//			
// ToDo:
//
// Changelog:
//				2024.01.15	- v2.01 - change typo

#include "Skeleton.h"
#include "math.h"
#if defined(CONF_USE_LIB_DFLT) && (CONF_USE_LIB_DFLT == 1)

#define	LIB_DFLT_OBJECTNAME		"lib_DigFilter"

// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
uint32_t LastSum;

// ******************************************************************************************************
// Globals
// ******************************************************************************************************

// ******************************************************************************************************
// PUBLIC Functions
// ******************************************************************************************************


// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************

// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------

#include <stdlib.h>

// ------------------------------------------------------------------------------------------------------
// Filter initialization
// select Type of filter, Number of Samples and Initialization Value
bool lib_DFLT_Init( lib_DFLT_If_t* const pDFLT_If, DFLT_Type_t Type, uint8_t Samples, uint32_t InitVal)
{
	bool res = false;
	uint32_t *pTmpMemBuff; 
	
#if defined(CONF_DEBUG_LIB_DIGFILTER) && (CONF_DEBUG_LIB_DIGFILTER == 1)
	FN_DEBUG_ENTRY(pDFLT_If->Name);													// dbg Entry print
#endif
	
    if(pDFLT_If)
	{
		if(Samples == 0) Samples = 1;
	
		pTmpMemBuff = sys_malloc(Samples*4);										// allocate memory
#if defined(CONF_DEBUG_LIB_DIGFILTER) && (CONF_DEBUG_LIB_DIGFILTER == 1)		
		if(pDFLT_If->pMemBuff == NULL) sys_Err ("lib_DFLT_Init ID[%s]: Not enought memory for allocating %d bytes!", pRB_If->Name, pTmpMemBuff);
#endif			
		if(pTmpMemBuff == NULL) return(false);
		sys_memset_4(pTmpMemBuff, InitVal, Samples);								// Fill with Init Value
		pDFLT_If->Priv.LastSum = InitVal * Samples;									// set as last average value of samples
		pDFLT_If->Priv.pMemBuff = pTmpMemBuff;										// set memory buffer area
		pDFLT_If->Priv.MemPointer = 0;
		pDFLT_If->Samples = Samples;
		pDFLT_If->Type = Type;
		pDFLT_If->Init = true;
		pDFLT_If->Reset= false;
        res = true;
    }

#if defined(CONF_DEBUG_LIB_DIGFILTER) && (CONF_DEBUG_LIB_DIGFILTER == 1)
	FN_DEBUG_EXIT(pModIf->Name);
#endif		
	
    return (res);	
}

// ------------------------------------------------------------------------------------------------------
// Filter Get sample total
uint16_t lib_DFLT_Calculate( lib_DFLT_If_t* const pDFLT_If, uint16_t Input)
{
	uint32_t Res = 0;
	if(pDFLT_If == NULL) return(0);
	if(pDFLT_If->Init == false) return(0);

	if(pDFLT_If->Reset == true) 
	{
		lib_DFLT_Init( pDFLT_If, pDFLT_If->Type, pDFLT_If->Samples, Input);	// reinit filter
		return (Input);
	}
	
	switch(pDFLT_If->Type)
	{
		case tSimpleAverage:
		{
			pDFLT_If->Priv.LastSum -= pDFLT_If->Priv.pMemBuff[pDFLT_If->Priv.MemPointer];	// odrataj prvy priemer z fronty
			pDFLT_If->Priv.LastSum += Input;												// pripocitaj aktualnu

			Res = pDFLT_If->Priv.LastSum / pDFLT_If->Samples;								// spriemeruj
			pDFLT_If->Priv.pMemBuff[pDFLT_If->Priv.MemPointer] = Input;						// odloz ako posledny priemer
			if(++ pDFLT_If->Priv.MemPointer >= pDFLT_If->Samples) pDFLT_If->Priv.MemPointer = 0;
			break;
		}
			
		case tMovingAverage:
		{
			pDFLT_If->Priv.LastSum += Input;												// pripocitaj aktualnu
			if(pDFLT_If->Priv.LastSum >= pDFLT_If->Priv.pMemBuff[pDFLT_If->Priv.MemPointer])
				pDFLT_If->Priv.LastSum -= pDFLT_If->Priv.pMemBuff[pDFLT_If->Priv.MemPointer];	// odrataj prvy priemer z fronty
			else 
				pDFLT_If->Priv.LastSum  = 0;
			
			Res = pDFLT_If->Priv.LastSum / pDFLT_If->Samples;								// spriemeruj
			
			pDFLT_If->Priv.pMemBuff[pDFLT_If->Priv.MemPointer] = Res;						// odloz ako posledny priemer
			
			if(++ pDFLT_If->Priv.MemPointer >= pDFLT_If->Samples) pDFLT_If->Priv.MemPointer = 0;
			break;
		}
		case tNone:	Res = Input; break;														// bez filtru
		default:
		{
			Res = 0;																		// deaktivuj
		}
	}
    return ((uint16_t)Res);	
}


#endif	// CONF_USE_LIB_DFLT
