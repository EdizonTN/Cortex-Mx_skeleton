; ******************************************************************************
; Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
;     Filename: startup_.s
; 	   Version: 3.0
;       Author: EdizonTN 
; Licenced under MIT License. More you can find at LICENSE file
; ******************************************************************************
; Info: STARTUP file - Stack and Heapo initialization, interrupt vectors, Reset vectors
;
; Notice:
;
; Usage: for CORTEX-M3 MCU
;
; ToDo:
; 
; Changelog:
; 


; Read Applications settings
#include "Application\App_Config.h"

; STACK Configuration
#if defined(CONF_STACK_SIZE)
Stack_Size      EQU     CONF_STACK_SIZE
#else
Stack_Size      EQU     0x00000100
#endif
                AREA    STACK, NOINIT, READWRITE, ALIGN=3
Stack_Mem       SPACE   Stack_Size
__initial_sp
StatckInit

; HEAP Configuration
#if defined(CONF_HEAP_SIZE)
Heap_Size       EQU     CONF_HEAP_SIZE
#else
Heap_Size       EQU     0x00000100
#endif
                AREA    HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem	    SPACE   Heap_Size
__heap_limit

                PRESERVE8
                THUMB


; Vector Table Mapped to Address 0 at Reset

                AREA    RESET, DATA, READONLY

; Especcialy for LPC microcontrollers:
;    	Code Read Protection  <0xFFFFFFFF=>CRP Disabled
;                             <0x12345678=>CRP Level 1
;                             <0x87654321=>CRP Level 2
;                             <0x43218765=>CRP Level 3 (ARE YOU SURE?)
;                             <0x4E697370=>NO ISP (ARE YOU SURE?)

				IF      :LNOT::DEF:NO_CRP
                AREA    |.ARM.__at_0x02FC|, CODE, READONLY
                DCD     0xFFFFFFFF
                ENDIF

                AREA    |.text|, CODE, READONLY

				ALIGN

					
#if (CONF_MICROLIB == 1)
				EXPORT  __initial_sp
                EXPORT  __heap_base
                EXPORT  __heap_limit

#else
	#if (CONF_USE_TWO_REGION_MODE == 1)                
				IMPORT  __use_two_region_memory
	#endif									
				EXPORT  __user_initial_stackheap

__user_initial_stackheap
                LDR     R0, =Stack_Mem            ; save Stack start adress
                LDR     R1, =Stack_Size           ; save Stack start size
                LDR     R2, =CONF_STACKFILLCONST  ; save FILL constant sample
Fill_Stack      STR     R2, [R0];				 , #4              ; fill
				ADDS	R0, R0, #4				  ; increment by 4
                SUBS    R1,#4                     ; ... whole Stack
                BNE     Fill_Stack                ;
				 
                LDR     R0, =Heap_Mem             ; save Heap start adress
                LDR     R1, =Heap_Size            ; save Heap start size
                LDR     R2, =CONF_HEAPFILLCONST   ; save FILL constant sample
Fill_Heap       STR     R2, [R0];				, #4              ; fill
				ADDS	R0, R0, #4					; increment by 4
                SUBS    R1,#4                     ; ... whole Heap
                BNE     Fill_Heap                 ;
				 
                LDR     R0, =  Heap_Mem
                LDR     R1, =(Stack_Mem + Stack_Size)
                LDR     R2, = (Heap_Mem +  Heap_Size)
                LDR     R3, = Stack_Mem
                BX      LR

                ALIGN
#endif
                END
