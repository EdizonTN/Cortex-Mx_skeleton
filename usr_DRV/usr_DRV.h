// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: usr_DRV.h
// 	   Version: 3.0
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: User's private Drivers include
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
// 


#ifndef __USR_DRV_H_
#define __USR_DRV_H_


// ******************************************************************************************************
// USER'S PRIVATE DRIVERS		- Load enabled drivers. Drivers are enabled in root's modules.
// ******************************************************************************************************
#if USE_DRV_ILI9806
	#include ".\usr_DRV\drv_ILI9806.h"									// Load custom driver
#endif

// insert other drivers here....

#endif	// __USR_DRV_H_
